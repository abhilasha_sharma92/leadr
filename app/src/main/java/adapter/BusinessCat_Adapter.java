package adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import leadr.com.leadr.R;
import modal.CategoryPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;


/**
 * Created by Abhilasha on 12/6/2017.
 */

public class BusinessCat_Adapter extends RecyclerView.Adapter<BusinessCat_Adapter.MyViewHolder> implements Filterable {

    private Activity ctx;
    private MyViewHolder holderItem;
    private OnItemClickListener listener = null;
    ArrayList<CategoryPojo> arr_cat = new ArrayList<>();
    public static ArrayList<CategoryPojo> arr_cat_filter = new ArrayList<>();
    GlobalConstant utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    public BusinessCat_Adapter(Activity ctx,ArrayList<CategoryPojo> arr_cat, OnItemClickListener listener) {
        this.ctx = ctx;
        this.listener = listener;
        this.arr_cat = arr_cat;
        this.arr_cat_filter = arr_cat;
        utils = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(ctx);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    arr_cat_filter = arr_cat;
                } else {
                    ArrayList<CategoryPojo> filteredList = new ArrayList<>();
                    for (CategoryPojo row : arr_cat) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if(sharedPreferenceLeadr.get_LANGUAGE().equals("en")) {
                            if (row.getCategory().toLowerCase().contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }else{
                            if (row.getHebrew().toLowerCase().contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }
                    }

                    arr_cat_filter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = arr_cat_filter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                arr_cat_filter = (ArrayList<CategoryPojo>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }


    public interface OnItemClickListener {
        void onItemClick(int i);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(ctx).inflate(R.layout.item_buss_categ, parent, false);
        holderItem = new MyViewHolder(itemView);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(position, listener);
    }


    @Override
    public int getItemCount() {
        return arr_cat_filter.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

            TextView  txt_cate;
            ImageView img_check;


        public MyViewHolder(View view) {
            super(view);

            txt_cate  = (TextView)view.findViewById(R.id.txt_cate);
            img_check = (ImageView) view.findViewById(R.id.img_check);

            if (view.getTag() != null) {
                view.setTag(holderItem);
            }


        }

        public void bind(final int i, final OnItemClickListener listener) {
            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")){
                txt_cate.setText(arr_cat_filter.get(i).getCategory());
            }else{
                txt_cate.setText(arr_cat_filter.get(i).getHebrew());
            }

            txt_cate.setTypeface(utils.OpenSans_Regular(ctx));
            if(arr_cat_filter.get(i).getChecked()){
                img_check.setVisibility(View.VISIBLE);
            }else{
                img_check.setVisibility(View.INVISIBLE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(i);
                }
            });

        }


    }
}


