package adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.BasePostprocessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import leadr.com.leadr.R;
import leadr.com.leadr.activity.ProfileOther_Activity;
import modal.CallDetails;
import modal.InboxPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;


/**
 * Created by Abhilasha on 12/6/2017.
 */

public class Inbox_Adapter extends RecyclerView.Adapter<Inbox_Adapter.MyViewHolder> {

    private Activity ctx;
    private MyViewHolder holderItem;
    private OnItemClickListener listener = null;
    ArrayList<InboxPojo> arr_inbox = new ArrayList<>();
    GlobalConstant utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    public Inbox_Adapter(Activity ctx,ArrayList<InboxPojo> arr_inbox, OnItemClickListener listener) {
        this.ctx = ctx;
        this.listener = listener;
        this.arr_inbox = arr_inbox;
        this.sharedPreferenceLeadr = new SharedPreferenceLeadr(ctx);
        utils = new GlobalConstant();
    }


    public interface OnItemClickListener {
        void onItemClick(int i);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(ctx).inflate(R.layout.item_inbox, parent, false);
        holderItem = new MyViewHolder(itemView);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(position, listener);
    }


    @Override
    public int getItemCount() {
        return arr_inbox.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

            TextView        txt_date;
            TextView        txt_name;
            TextView        txt_nmbr;
            TextView        txt_counter;
            LinearLayout    lnr_pro;
            SimpleDraweeView img_user;
            Button          btn_type;
            ProgressBar     progres_load_user;

        public MyViewHolder(View view) {
            super(view);

            txt_date = (TextView)view.findViewById(R.id.txt_date);
            txt_name = (TextView)view.findViewById(R.id.txt_name);
            txt_nmbr = (TextView)view.findViewById(R.id.txt_nmbr);
            txt_counter = (TextView)view.findViewById(R.id.txt_counter);
            lnr_pro  = (LinearLayout) view.findViewById(R.id.lnr_pro);
            img_user = (SimpleDraweeView)view.findViewById(R.id.img_user);
            btn_type = (Button)view.findViewById(R.id.btn_type);
            progres_load_user = (ProgressBar) view.findViewById(R.id.progres_load_user);

            if (view.getTag() != null) {
                view.setTag(holderItem);
            }
        }

        public void bind(final int i, final OnItemClickListener listener) {
            txt_date.setTypeface(utils.OpenSans_Regular(ctx));
            txt_name.setTypeface(utils.OpenSans_Regular(ctx));
            txt_nmbr.setTypeface(utils.OpenSans_Light(ctx));
            btn_type.setTypeface(utils.OpenSans_Regular(ctx));
            txt_counter.setTypeface(utils.OpenSans_Regular(ctx));


            try{
                if(!arr_inbox.get(i).getChat_cnt().equalsIgnoreCase("0")) {
                    txt_counter.setText(arr_inbox.get(i).getChat_cnt());
                    txt_counter.setVisibility(View.VISIBLE);
                }
            }catch (Exception e){
                txt_counter.setVisibility(View.GONE);
            }
            txt_nmbr.setText(arr_inbox.get(i).getMsg_from());
            txt_date.setText(arr_inbox.get(i).getMsg_to());
            txt_nmbr.setText(arr_inbox.get(i).getMsg());
            txt_date.setText(method_date_toseT(i));

            if(!sharedPreferenceLeadr.getUserId().equalsIgnoreCase(arr_inbox.get(i).getLead_user_id())) {
                btn_type.setText(arr_inbox.get(i).getMsg_from_type());
                if(arr_inbox.get(i).getFrom_user_name().equalsIgnoreCase("user deleted account")){
                    txt_name.setText(ctx.getResources().getString(R.string.usr_del));
                }else {
                    txt_name.setText(arr_inbox.get(i).getFrom_user_name());
                }
                try{
                    progres_load_user.setVisibility(View.VISIBLE);

                    DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                            ImageRequestBuilder.newBuilderWithSource(Uri.parse(arr_inbox.get(i).getFrom_user_thum())).
                                    setRotationOptions(RotationOptions.disableRotation())
                                    .setPostprocessor(new BasePostprocessor() {
                                        @Override
                                        public void process(Bitmap bitmap) {
                                        }
                                    })
                                    .build())
                            .setControllerListener(new BaseControllerListener<ImageInfo>() {
                                @Override
                                public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                                    progres_load_user.setVisibility(View.GONE);
                                }
                                @Override
                                public void onFailure(String id, Throwable throwable) {
                                    super.onFailure(id, throwable);
                                    progres_load_user.setVisibility(View.GONE);
                                }
                                @Override
                                public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                                    progres_load_user.setVisibility(View.GONE);
                                }
                            })
                            .build();
                    img_user.setController(controller);
                }catch (Exception e){progres_load_user.setVisibility(View.GONE);}
            }else{
                btn_type.setText(arr_inbox.get(i).getMsg_to_type());
                txt_name.setText(arr_inbox.get(i).getTo_user_name());
                try{
                    progres_load_user.setVisibility(View.VISIBLE);

                    DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                            ImageRequestBuilder.newBuilderWithSource(Uri.parse(arr_inbox.get(i).getTo_user_thum())).
                                    setRotationOptions(RotationOptions.disableRotation())
                                    .setPostprocessor(new BasePostprocessor() {
                                        @Override
                                        public void process(Bitmap bitmap) {
                                        }
                                    })
                                    .build())
                            .setControllerListener(new BaseControllerListener<ImageInfo>() {
                                @Override
                                public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                                    progres_load_user.setVisibility(View.GONE);
                                }
                                @Override
                                public void onFailure(String id, Throwable throwable) {
                                    super.onFailure(id, throwable);
                                    progres_load_user.setVisibility(View.GONE);
                                }
                                @Override
                                public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                                    progres_load_user.setVisibility(View.GONE);
                                }
                            })
                            .build();
                    img_user.setController(controller);
                }catch (Exception e){progres_load_user.setVisibility(View.GONE);}
            }

            if(btn_type.getText().toString().equalsIgnoreCase("I Sell")){
                btn_type.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.frame_chat_gray));
                btn_type.setText(ctx.getResources().getString(R.string.i_selll));
            }else if(btn_type.getText().toString().equalsIgnoreCase("I Sold")){
                btn_type.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.frame_chat_gray));
                btn_type.setText(ctx.getResources().getString(R.string.i_sold_inbx));
            }else if(btn_type.getText().toString().equalsIgnoreCase("I Buy")){
                btn_type.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.frame_chat_blue));
                btn_type.setText(ctx.getResources().getString(R.string.i_buy));
            }else if(btn_type.getText().toString().equalsIgnoreCase("I Bought")){
                btn_type.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.frame_chat_blue));
                btn_type.setText(ctx.getResources().getString(R.string.bought));
            }


           /* if(arr_inbox.get(i).getType().equalsIgnoreCase("buy")){
                btn_type.setText(ctx.getResources().getString(R.string.i_buy));
                btn_type.setBackgroundResource(R.drawable.frame_chat_blue);
            }else if(arr_inbox.get(i).getType().equalsIgnoreCase("sell")){
                btn_type.setText(ctx.getResources().getString(R.string.i_selll));
                btn_type.setBackgroundResource(R.drawable.frame_chat_gray);
            }else{
                btn_type.setVisibility(View.GONE);
            }*/

          /*  if(!arr_inbox.get(i).getThum_image().equals("")){
                if(!arr_inbox.get(i).getThum_image().equals("0")){
                    try{
                        Picasso.with(ctx).load(arr_inbox.get(i).getThum_image())
                                .into(img_user);
                    }catch (Exception e){}
                }else{
                    try{
                        Picasso.with(ctx).load(arr_inbox.get(i).getThum_image())
                                .into(img_user);
                    }catch (Exception e){}
                }
            }else{
                try{
                    Picasso.with(ctx).load(arr_inbox.get(i).getImage())
                            .into(img_user);
                }catch (Exception e){}
            }*/

           /* lnr_pro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   *//* Intent i_pro = new Intent(ctx, ProfileOther_Activity.class);
                    i_pro.putExtra("id",arr_inbox.get(i).getId());
                    ctx.startActivity(i_pro);*//*
                }
            });*/

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(i);
                }
            });



        }
        private String method_date_toseT(int position) {
            String date_return = "";
            try {
                //Dates to compare
                String CurrentDate=  CurrentDate();
                String CurrentTime=  CurrentTime();
                String FinalDate=  getOnlyDate(arr_inbox.get(position).getDate()+ " "+ arr_inbox.get(position).getTime());
                String get_time =  arr_inbox.get(position).getTime();
                String FinalTime=  getDate(arr_inbox.get(position).getDate() + " "+get_time);

                Date date1;
                Date date1_T;
                Date date2;
                Date date2_T;

                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
                SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

                //Setting dates
                date1 = dates.parse(CurrentDate);
                date1_T = times.parse(FinalTime);
                date2 = dates.parse(FinalDate);
                date2_T = times.parse(CurrentTime);

                //Comparing dates
                long difference = Math.abs(date1.getTime() - date2.getTime());
                long differenceDates = difference / (24 * 60 * 60 * 1000);
                long mills =   date2_T.getTime()-date1_T.getTime();

                String api_year = FinalDate.split("-")[0];
                String curr_year = CurrentDate.split("-")[0];
                String sub_year = "0";

                try{
                    sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
                }catch (Exception e){}

                //Convert long to String
                String dayDifference = Long.toString(differenceDates);

                if(sub_year.equals("1")){
                    date_return = ctx.getResources().getString(R.string.chat_m);
                }
                else if(sub_year.equals("2")){
                    date_return = ctx.getResources().getString(R.string.chat_y);
                }
                else if(Integer.valueOf(dayDifference)>0){
                    if(dayDifference.equals("1")){
                        date_return = dayDifference +ctx.getResources().getString(R.string.chat_d);
                    }else{
                        date_return = dayDifference + ctx.getResources().getString(R.string.chat_d);
                    }
                }else{
                    int hours = (int) (mills / (1000 * 60 * 60));
                    int minutes = (int) (mills / (1000 * 60));
                    int seconds = (int) (mills / (1000));
                    if(hours>0){
                        date_return = hours+""+ctx.getString(R.string.chat_hr);
                    }else if(minutes>0){
                        date_return = minutes+""+ctx.getString(R.string.chat_m);
                    }else{
                        date_return = seconds+ctx.getString(R.string.chat_s);
                    }
                }

            } catch (Exception exception) {
                date_return = ctx.getResources().getString(R.string.fewdays);
            }
            return date_return;
        }

    }



    private String getDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);

        }
        catch (Exception e)
        {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }


    String CurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    String CurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    private String getOnlyDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
            Log.e("time: ",TimeZone.getDefault()+"");
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);
        }
        catch (Exception e) {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }
}


