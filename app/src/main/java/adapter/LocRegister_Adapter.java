package adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import leadr.com.leadr.R;
import modal.LatlngPojo;


/**
 * Created by Abhilasha on 12/6/2017.
 */

public class LocRegister_Adapter extends RecyclerView.Adapter<LocRegister_Adapter.MyViewHolder> {

    private Activity ctx;
    private MyViewHolder holderItem;
    private OnItemClickListener listener = null;
    ArrayList<LatlngPojo> arr_place = new ArrayList<>();

    public LocRegister_Adapter(Activity ctx,ArrayList<LatlngPojo> arr_place, OnItemClickListener listener) {
        this.ctx = ctx;
        this.listener = listener;
        this.arr_place = arr_place;
    }


    public interface OnItemClickListener {
        void onItemClick(int i);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(ctx).inflate(R.layout.item_loc, parent, false);
        holderItem = new MyViewHolder(itemView);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(position, listener);
    }


    @Override
    public int getItemCount() {
        return arr_place.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView  txt_loc;
            ImageView img_remove;
            View vw_line;


        public MyViewHolder(View view) {
            super(view);

            txt_loc    = (TextView)view.findViewById(R.id.txt_loc);
            img_remove = (ImageView) view.findViewById(R.id.img_remove);
            vw_line    = (View) view.findViewById(R.id.vw_line);

            if (view.getTag() != null) {
                view.setTag(holderItem);
            }


        }

        public void bind(final int i, final OnItemClickListener listener) {

            txt_loc.setText(arr_place.get(i).getName());

            img_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arr_place.remove(arr_place.get(i));
                    notifyDataSetChanged();
                }
            });

            if(i==arr_place.size()-1){
                vw_line.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(i);
                }
            });

        }


    }
}


