package adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import leadr.com.leadr.R;
import leadr.com.leadr.activity.ChatActivity;
import modal.ChatPojo;
import modal.Message;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;


/**
 * Created by Abhilasha on 23-Jan-18.
 */

public class MessageListAdapter extends RecyclerView.Adapter<ChatAppMsgViewHolder> {

    private List<ChatPojo> msgDtoList = null;

    Activity ctx;

    String get_other_img = "";

    SharedPreferenceLeadr sharedPreferenceLeadr;
    GlobalConstant utils;

    public MessageListAdapter(List<ChatPojo> msgDtoList, String get_other_img, Activity ctx) {
        this.msgDtoList = msgDtoList;
        this.get_other_img = get_other_img;
        this.ctx = ctx;
        sharedPreferenceLeadr = new SharedPreferenceLeadr(ctx);
        utils = new GlobalConstant();
    }

    @Override
    public void onBindViewHolder(ChatAppMsgViewHolder holder, int position) {
        ChatPojo msgDto = this.msgDtoList.get(position);
        // If the message is a received message.
        holder.txt_time_other.setTypeface(utils.OpenSans_Regular(ctx));
        holder.leftMsgTextView.setTypeface(utils.OpenSans_Regular(ctx));
        holder.rightMsgTextView.setTypeface(utils.OpenSans_Regular(ctx));
        holder.txt_time_me.setTypeface(utils.OpenSans_Regular(ctx));

        if(msgDto.getMSGTYPE().equals("sent"))
        {
            // Show received message in left linearlayout.
            holder.leftMsgLayout.setVisibility(LinearLayout.VISIBLE);
            holder.leftMsgTextView.setText(msgDto.getMsg());

            Calendar now = Calendar.getInstance();
            int a = now.get(Calendar.AM_PM);
            String am_or_pm = "am";
            if(a == Calendar.AM){
                am_or_pm = "am";
            }else{
                am_or_pm = "pm";
            }
            holder.txt_time_other.setText(method_date_toseT(position)+" "+am_or_pm);
            // Remove left linearlayout.The value should be GONE, can not be INVISIBLE
            // Otherwise each iteview's distance is too big.
            holder.rightMsgLayout.setVisibility(LinearLayout.GONE);
            try{
                Picasso.with(ctx).load(get_other_img)
                        .into(holder.img_other);
            }catch (Exception e){}
        }
        // If the message is a sent message.
        else if(msgDto.getMSGTYPE().equals("receive"))
        {
            // Show sent message in right linearlayout.
            holder.rightMsgLayout.setVisibility(LinearLayout.VISIBLE);
            holder.rightMsgTextView.setText(msgDto.getMsg());

            Calendar now = Calendar.getInstance();
            int a = now.get(Calendar.AM_PM);
            String am_or_pm = "am";
            if(a == Calendar.AM){
                am_or_pm = "am";
            }else{
                am_or_pm = "pm";
            }
            holder.txt_time_me.setText(method_date_toseT(position)+" "+am_or_pm);
            // Remove left linearlayout.The value should be GONE, can not be INVISIBLE
            // Otherwise each iteview's distance is too big.
            holder.leftMsgLayout.setVisibility(LinearLayout.GONE);
            try{
                Picasso.with(ctx).load(sharedPreferenceLeadr.get_PROFILE_THUMB())
                        .into(holder.img_me);
            }catch (Exception e){}
        }
    }

    @Override
    public ChatAppMsgViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_message_received, parent, false);
        return new ChatAppMsgViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if(msgDtoList==null)
        {
            msgDtoList = new ArrayList<ChatPojo>();
        }
        return msgDtoList.size();
    }

    public void addMsg(ChatPojo item) {
        msgDtoList.add(item);
        notifyItemInserted(msgDtoList.size() - 1);
    }


    private String method_date_toseT(int position) {
        String FinalTime = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            SimpleDateFormat dates = new SimpleDateFormat("dd/MM/yyyy hh-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("dd/MM/yyyy hh-mm-ss");

            String FinalDate=  getOnlyDate(msgDtoList.get(position).getCurrent_date()+ " "+
                    msgDtoList.get(position).getCurrent_time());
            String get_time =  msgDtoList.get(position).getCurrent_time();
             FinalTime=  getDate(msgDtoList.get(position).getCurrent_date() + " "+get_time);
            
        } catch (Exception exception) {
        }
        return FinalTime;
    }


    private String getDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh-mm");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh-mm"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);

        }
        catch (Exception e)
        {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }


    String CurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh-mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    String CurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh-mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    private String getOnlyDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh-mm");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh-mm"); //this format changeable
            Log.e("time: ",TimeZone.getDefault()+"");
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);
        }
        catch (Exception e) {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }
}