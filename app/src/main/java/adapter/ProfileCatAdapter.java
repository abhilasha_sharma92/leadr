package adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import leadr.com.leadr.R;
import modal.CategoryPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;

/**
 * Created by Abhilasha on 17/1/2018.
 */

public class ProfileCatAdapter extends BaseAdapter {
    private List<CategoryPojo> data;
    private Activity context;
    GlobalConstant utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    public ProfileCatAdapter(List<CategoryPojo> data, Activity context) {
        this.data = data;
        this.context = context;
        sharedPreferenceLeadr = new SharedPreferenceLeadr(context);
        utils = new GlobalConstant();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view==null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.item_pro_categ, parent, false);

            TextView txt_cate = (TextView) view.findViewById(R.id.txt_cate);
            ImageView img_check = (ImageView) view.findViewById(R.id.img_check);
            View vw_line = (View) view.findViewById(R.id.vw_line);

            txt_cate.setTypeface(utils.OpenSans_Regular(context));

            img_check.setVisibility(View.GONE);

            if (sharedPreferenceLeadr.get_LANGUAGE().equals("it")) {
                txt_cate.setText(data.get(position).getHebrew());
            } else {
                txt_cate.setText(data.get(position).getCategory());
            }

            txt_cate.setTextColor(context.getResources().getColor(R.color.gray_cate));
            if(position == data.size()-1){
//                vw_line.setVisibility(View.INVISIBLE);
            }
        }


        return view;
    }


}