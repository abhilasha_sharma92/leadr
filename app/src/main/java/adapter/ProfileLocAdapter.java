package adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import leadr.com.leadr.R;
import modal.LatlngPojo;
import utils.GlobalConstant;

/**
 * Created by Abhilasha on 17/1/2018.
 */

public class ProfileLocAdapter extends BaseAdapter {
    private List<LatlngPojo> data;
    private Activity context;
    GlobalConstant utils;

    public ProfileLocAdapter(List<LatlngPojo> data, Activity context) {
        this.data = data;
        this.context = context;
        utils = new GlobalConstant();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view==null){
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.item_loc_pro_test, parent, false);

            TextView txt_loc         = (TextView)view.findViewById(R.id.txt_loc);
            View vw_line             = (View)view.findViewById(R.id.vw_line) ;
            ImageView img_remove     = (ImageView)view.findViewById(R.id.img_remove) ;

            txt_loc.setTypeface(utils.OpenSans_Regular(context));

            img_remove.setVisibility(View.GONE);
            try{
                if(!data.get(position).getCountryname().equals("")){
                    txt_loc.setText(data.get(position).getName());
                }else {
                    txt_loc.setText(data.get(position).getName());
                }

            }catch (Exception e){
                txt_loc.setText(data.get(position).getName());
            }
            txt_loc.setTextColor(context.getResources().getColor(R.color.gray_cate));
//            Drawable img = context.getResources().getDrawable( R.drawable.mapview );
//            img.setBounds( 0, 0, 0, 0 );
//            txt_loc.setCompoundDrawables( img, null, null, null );

            vw_line.setVisibility(View.GONE);

        }


        return view;
    }


}