package adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import leadr.com.leadr.R;
import modal.CallDetails;
import utils.GlobalConstant;


/**
 * Created by Abhilasha on 12/6/2017.
 */

public class CallLog_Adapter extends RecyclerView.Adapter<CallLog_Adapter.MyViewHolder> {

    private Activity ctx;
    private MyViewHolder holderItem;
    private OnItemClickListener listener = null;
    ArrayList<CallDetails> arr_call = new ArrayList<>();
    GlobalConstant utils;

    public CallLog_Adapter(Activity ctx, ArrayList<CallDetails> arr_call, OnItemClickListener listener) {
        this.ctx = ctx;
        this.listener = listener;
        this.arr_call = arr_call;
        utils = new GlobalConstant();
    }


    public interface OnItemClickListener {
        void onItemClick(int i);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(ctx).inflate(R.layout.item_call, parent, false);
        holderItem = new MyViewHolder(itemView);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(position, listener);
    }


    @Override
    public int getItemCount() {
        return arr_call.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

            TextView txt_date;
            TextView txt_name;
            TextView txt_nmbr;

        public MyViewHolder(View view) {
            super(view);

            txt_date = (TextView)view.findViewById(R.id.txt_date);
            txt_name = (TextView)view.findViewById(R.id.txt_name);
            txt_nmbr = (TextView)view.findViewById(R.id.txt_nmbr);

            if (view.getTag() != null) {
                view.setTag(holderItem);
            }


        }

        public void bind(final int i, final OnItemClickListener listener) {
            txt_date.setTypeface(utils.OpenSans_Regular(ctx));
            txt_name.setTypeface(utils.OpenSans_Regular(ctx));
            txt_nmbr.setTypeface(utils.OpenSans_Regular(ctx));

            txt_nmbr.setText(arr_call.get(i).getCaller_number());
            txt_name.setText(arr_call.get(i).getCaller_name());

            String time = arr_call.get(i).getCall_date();

            DateFormat inputFormat = new SimpleDateFormat("EE MMM dd HH:mm:ss zz yyy");
            Date d = null;
            try {
                d = inputFormat.parse(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try{

                DateFormat outputFormat = new SimpleDateFormat("HH:mm");

                txt_date.setText(outputFormat.format(d));
            }catch (Exception e){}

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(i);
                }
            });

        }


    }
}


