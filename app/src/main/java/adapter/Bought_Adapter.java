package adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import leadr.com.leadr.R;
import modal.BuyPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;


/**
 * Created by Abhilasha on 12/6/2017.
 */

public class Bought_Adapter extends RecyclerView.Adapter<Bought_Adapter.MyViewHolder> {

    private Activity      ctx;
    GlobalConstant        utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    private MyViewHolder holderItem;
    private OnItemClickListener          listener = null;
    private ArrayList<BuyPojo>            arr_bought = new ArrayList<>();


    public Bought_Adapter( Activity ctx, ArrayList<BuyPojo> arr_bought, OnItemClickListener listener) {
        this.ctx              = ctx;
        this.listener         = listener;
        this.arr_bought    = arr_bought;
        this.utils            = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(ctx);
    }


    public interface OnItemClickListener {
        void onItemClick( int i );
    }


    @Override
    public MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(ctx).inflate(R.layout.item_bought, parent, false);
        holderItem = new MyViewHolder(itemView);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder, int position) {
        holder.bind(position, listener);
    }


    @Override
    public int getItemCount() {
        return arr_bought.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        de.hdodenhof.circleimageview.CircleImageView img_user ;
        TextView txt_seller                ;
        TextView txt_ago                  ;
        TextView txt_client_phn            ;
        TextView txt_name_side             ;
        TextView txt_loc                   ;
        TextView txt_budget                ;
        final TextView txt_play_pause      ;
        final TextView txt_pause_audio    ;
        final TextView txt_secs            ;
        TextView txt_play                  ;
        TextView txt_des                  ;
        final TextView txt_status          ;
        TextView txt_read_more             ;
        TextView txt_lead_price            ;
        final RelativeLayout btn_buy       ;
        TextView txt_bought_ny             ;
        TextView txt_client_name           ;
        TextView txt_des_client            ;
        ImageView img_shadow               ;
        View vw_line                       ;
        final RelativeLayout rel_call      ;
        final LinearLayout lnr_pro         ;
        final SeekBar songProgressBar      ;
        final LinearLayout lnr_11          ;
        final  LinearLayout lnr_22        ;
        final  ImageView img_chat          ;
        final  RelativeLayout rel_no_show  ;
        final  TextView txt_noentry_nxt    ;
        final  TextView txt_noentry        ;
        final  TextView txt_srch           ;
        final  RelativeLayout btn_buy_last ;
        final  TextView txt_by_now         ;
        final FrameLayout frame_main     ;
        final  ImageView img_file_desc     ;
        final ProgressBar progress_one     ;
        final  ProgressBar progress_two    ;
        final  ImageView img_one           ;
        final  ImageView img_two           ;
        final  ProgressBar progres_load_user ;

        public MyViewHolder(View view) {
            super(view);

             img_user = (de.hdodenhof.circleimageview.CircleImageView)
                    view.findViewById(R.id.img_user);
             txt_seller                = (TextView)view.findViewById(R.id.txt_seller);
             txt_ago                   = (TextView)view.findViewById(R.id.txt_ago);
             txt_client_phn            = (TextView)view.findViewById(R.id.txt_client_phn);
             txt_name_side             = (TextView)view.findViewById(R.id.txt_name_side);
             txt_loc                   = (TextView)view.findViewById(R.id.txt_loc);
             txt_budget                = (TextView)view.findViewById(R.id.txt_budget);
              txt_play_pause      = (TextView)view.findViewById(R.id.txt_pause_audio);
              txt_pause_audio     = (TextView)view.findViewById(R.id.txt_play_pause);
              txt_secs            = (TextView)view.findViewById(R.id.txt_secs);
             txt_play                  = (TextView)view.findViewById(R.id.txt_play);
             txt_des                   = (TextView)view.findViewById(R.id.txt_des);
              txt_status          = (TextView)view.findViewById(R.id.txt_status);
             txt_read_more             = (TextView)view.findViewById(R.id.txt_read_more);
             txt_lead_price            = (TextView)view.findViewById(R.id.txt_lead_price);
              btn_buy       = (RelativeLayout)view.findViewById(R.id.btn_buy);
             txt_bought_ny             = (TextView)view.findViewById(R.id.txt_bought_ny);
             txt_client_name           = (TextView)view.findViewById(R.id.txt_client_name);
             txt_des_client            = (TextView)view.findViewById(R.id.txt_des_client);
             img_shadow               = (ImageView) view.findViewById(R.id.img_shadow);
             vw_line                       = (View) view.findViewById(R.id.vw_line);
              rel_call      = (RelativeLayout) view.findViewById(R.id.rel_call);
              lnr_pro         = (LinearLayout) view.findViewById(R.id.lnr_pro);
              songProgressBar      = (SeekBar) view.findViewById(R.id.songProgressBar);
              lnr_11          = (LinearLayout) view.findViewById(R.id.lnr_11);
               lnr_22         = (LinearLayout) view.findViewById(R.id.lnr_22);
               img_chat          = (ImageView) view.findViewById(R.id.img_chat);
               rel_no_show  = (RelativeLayout) view.findViewById(R.id.rel_no_show);
               txt_noentry_nxt    = (TextView) view.findViewById(R.id.txt_noentry_nxt);
             txt_noentry        = (TextView) view.findViewById(R.id.txt_noentry);
            txt_srch           = (TextView) view.findViewById(R.id.txt_srch);
               btn_buy_last = (RelativeLayout) view.findViewById(R.id.btn_buy_last);
               txt_by_now         = (TextView) view.findViewById(R.id.txt_by_now);
               frame_main      = (FrameLayout) view.findViewById(R.id.frame_main);
               img_file_desc     = (ImageView) view.findViewById(R.id.img_file_desc);
              progress_one     = (ProgressBar) view.findViewById(R.id.progress_one);
               progress_two    = (ProgressBar) view.findViewById(R.id.progress_two);
               img_one           = (ImageView) view.findViewById(R.id.img_one);
               img_two           = (ImageView) view.findViewById(R.id.img_two);
               progres_load_user = (ProgressBar) view.findViewById(R.id.progres_load_user);

            if (view.getTag() != null) {
                view.setTag(holderItem);
            }
        }

        public void bind(final int i, final OnItemClickListener listener) {
            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(i);
                }
            });

            methodTypeface();

            txt_play_pause.setTag("0");
        }

    }

    private void methodTypeface() {
        holderItem.txt_seller.setTypeface(utils.OpenSans_Light(ctx));
        holderItem.txt_name_side.setTypeface(utils.OpenSans_Light(ctx));
        holderItem.txt_client_name.setTypeface(utils.OpenSans_Light(ctx));
        holderItem.txt_des_client.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_budget.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_client_phn.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_loc.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_play.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_des.setTypeface(utils.OpenSans_Light(ctx));
        holderItem.txt_lead_price.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_read_more.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_ago.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_status.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_bought_ny.setTypeface(utils.Dina(ctx));
        holderItem.txt_noentry_nxt.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_noentry.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_srch.setTypeface(utils.OpenSans_Regular(ctx));
        holderItem.txt_by_now.setTypeface(utils.Dina(ctx));
    }
}


