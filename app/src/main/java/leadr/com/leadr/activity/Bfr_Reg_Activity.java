package leadr.com.leadr.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;

/**
 * Created by Abhilasha on 11/12/2017.
 */

public class Bfr_Reg_Activity extends AppCompatActivity {

    @BindView(R.id.rel_start)
    RelativeLayout rel_start;

    @BindView(R.id.rel_main)
    RelativeLayout rel_main;

    @BindView(R.id.txt_get_strt)
    TextView txt_get_strt;

    @BindView(R.id.txt_policy2)
    TextView txt_policy2;

    String phone;
    GlobalConstant utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_before_reg);

        // bind the view using butterknife
        ButterKnife.bind(this);
        utils = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(Bfr_Reg_Activity.this);

        txt_get_strt.setTypeface(utils.OpenSans_Regular(Bfr_Reg_Activity.this));
        txt_policy2.setTypeface(utils.OpenSans_Regular(Bfr_Reg_Activity.this));

        phone = getIntent().getStringExtra("phone");

        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en"))
        {
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
            rel_main.setBackground(getResources().getDrawable(R.drawable.startedd));

        }else{
            String languageToLoad  = "it";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
            rel_main.setBackground(getResources().getDrawable(R.drawable.startedd_he));
        }

        if(sharedPreferenceLeadr.get_GET_START().equalsIgnoreCase("00:00")){
            sharedPreferenceLeadr.set_GET_STARTT(CurrentTimeStart());
        }
        if(method_7DayCheck(sharedPreferenceLeadr.get_GET_START(),CurrentTimeEnd()).equalsIgnoreCase("1")){
            AppEventsLogger logger = AppEventsLogger.newLogger(Bfr_Reg_Activity.this);
            logger.logEvent("EVENT_NAME_LOGIN_NOT_REGISTERED");
        }
    }

    @OnClick(R.id.rel_start)
    public void onStartReg() {
        Intent i_phn_screen = new Intent(Bfr_Reg_Activity.this, RegistrationActivity.class);
        i_phn_screen.putExtra("phone",phone);
        startActivity(i_phn_screen);
        finish();
    }

    @Override
    public void onBackPressed() {

    }

    String CurrentTimeStart(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    String CurrentTimeEnd(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    String method_7DayCheck(final String start_date, final String end_date){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  start_date;
            String CurrentTime=  end_date;

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(end_date);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);


            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

             if(Integer.valueOf(dayDifference)>0&&Integer.valueOf(dayDifference)<1){
                 sharedPreferenceLeadr.set_GET_STARTT("00:00");
                 return "1";
            }else{
                duration_return = "0";
            }

        } catch (Exception exception) {
        }
        return  duration_return;
    }
}
