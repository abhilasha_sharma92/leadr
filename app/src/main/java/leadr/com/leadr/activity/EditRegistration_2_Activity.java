package leadr.com.leadr.activity;

import android.Manifest;
import android.Manifest.permission;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Animatable;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.BasePostprocessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import leadr.com.leadr.R;

import pref.SharedPreferenceLeadr;
import utils.Constants;
import utils.GlobalConstant;
import utils.NetworkingData;
import utils.UploadThumbAws;
import utils.UploadThumbAwsBuss;
import utils.UploadThumbBuss;
import utils.UploadThumbUser;
import utils.Util;


/**
 * Created by Admin on 2/20/2018.
 */

public class EditRegistration_2_Activity extends AppCompatActivity {

    @BindView(R.id.lnr_1)
    LinearLayout lnr_1;

    @BindView(R.id.lnr_2)
    LinearLayout lnr_2;

    @BindView(R.id.txt_busines)
    TextView txt_busines;

    @BindView(R.id.txt_bus_sit)
    TextView txt_bus_sit;

    @BindView(R.id.txt_businessname)
    TextView txt_businessname;

    @BindView(R.id.bus_infoo)
    TextView bus_infoo;

    @BindView(R.id.txt_two)
    TextView txt_two;

    @BindView(R.id.edt_buss_name)
    EditText edt_buss_name;

    @BindView(R.id.edt_buss_page)
    EditText edt_buss_page;

    @BindView(R.id.edt_buss_info)
    EditText edt_buss_info;

    @BindView(R.id.btn_nxt)
    Button btn_nxt;

    @BindView(R.id.btn_finish)
    Button btn_finish;

    @BindView(R.id.img_buss)
    SimpleDraweeView img_buss;


    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.img_back_top)
    ImageView img_back_top;

    @BindView(R.id.rel_edit)
    RelativeLayout rel_edit;

    @BindView(R.id.rel_reg)
    RelativeLayout rel_reg;

    @BindView(R.id.img_rotate_buss)
    ImageView img_user_rotate_buss;

    @BindView(R.id.img_buss_cancel)
    ImageView img_buss_cancel;

    @BindView(R.id.progres_load)
    ProgressBar progres_load;

    GlobalConstant utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    String buss_full = "";
    String buss_thumb = "";
    int rotation_buss = 0;

    String str_upload1 = null;
    String str_upload1_back = null;
    String userChoosenTask = null;
    File file_image_buss;

    int REQUEST_CAMERA = 3;
    int SELECT_FILE = 2;

    BottomSheetDialog dialog_storage;
    BottomSheetDialog dialog_deny;
    BottomSheetDialog dialog_buss;

    TransferObserver observerthumb_buss;
    TransferObserver observerfull_buss;
    private TransferUtility transferUtility;
    ProgressDialog progressDialog;
    private Util util;
    private File compressedImage;
    private File compressedImage_thumb;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_reg);

        ButterKnife.bind(this);

        utils = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(EditRegistration_2_Activity.this);
        util               = new Util();
        transferUtility    = util.getTransferUtility(this);


        txt_busines.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_businessname.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        edt_buss_name.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_bus_sit.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        edt_buss_page.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        bus_infoo.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        edt_buss_info.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));

        btn_nxt.setVisibility(View.GONE);
        lnr_1.setVisibility(View.GONE);
        lnr_2.setVisibility(View.VISIBLE);
        btn_finish.setVisibility(View.VISIBLE);
        img_back.setVisibility(View.VISIBLE);


        btn_finish.setText(getResources().getString(R.string.save));
        edt_buss_name.setSelection(edt_buss_name.getText().toString().length());

        try{
            if(sharedPreferenceLeadr.get_BUSS_THUMB().trim().length()>2){
                img_buss_cancel.setVisibility(View.VISIBLE);
                img_user_rotate_buss.setVisibility(View.VISIBLE);
                str_upload1_back = "";

                DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                        ImageRequestBuilder.newBuilderWithSource(Uri.parse(sharedPreferenceLeadr.get_BUSS_THUMB()))
                                .setRotationOptions(RotationOptions.disableRotation())
                                .setPostprocessor(new BasePostprocessor() {
                                    @Override
                                    public void process(Bitmap bitmap) {
                                    }
                                })
                                .build())
                        .setControllerListener(new BaseControllerListener<ImageInfo>() {
                            @Override
                            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                                progres_load.setVisibility(View.GONE);
                            }
                            @Override
                            public void onFailure(String id, Throwable throwable) {
                                super.onFailure(id, throwable);
                                progres_load.setVisibility(View.GONE);
                            }
                            @Override
                            public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                                progres_load.setVisibility(View.GONE);
                            }
                        })
                        .build();
                img_buss.setController(controller);

            }
        }catch (Exception e){ progres_load.setVisibility(View.GONE);}
        addAstresikBusiness();

        lnr_1.setVisibility(View.GONE);
        lnr_2.setVisibility(View.VISIBLE);
        img_back.setVisibility(View.GONE);
        txt_two.setBackground(getResources().getDrawable(R.drawable.two_ac));

        edt_buss_info.setText(sharedPreferenceLeadr.get_bus_info());
        edt_buss_page.setText(sharedPreferenceLeadr.get_bus_site());
        edt_buss_name.setText(sharedPreferenceLeadr.get_bus_name());

        edt_buss_name.setSelection(edt_buss_name.getText().toString().length());
        try{
            if(sharedPreferenceLeadr.get_BUSS_THUMB().trim().length()>2){
                img_buss_cancel.setVisibility(View.VISIBLE);
                img_user_rotate_buss.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){}

        if(sharedPreferenceLeadr.get_BUSS_THUMB().trim().length()<2) {
            if (!sharedPreferenceLeadr.get_LANGUAGE().equals("en")) {
                img_buss.setImageResource(R.drawable.addimage_he);
            } else {
                img_buss.setImageResource(R.drawable.addimage);
            }
        }
    }

    private void addAstresikBusiness() {
        String simple = getResources().getString(R.string.buss_name);
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();

        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        txt_businessname.setText(builder);
    }

    @OnClick(R.id.img_back)
    public void OnBack() {
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(sharedPreferenceLeadr.get_PERPRO().equalsIgnoreCase("1")){
            sharedPreferenceLeadr.set_PERPRO("0");
            if (utils.checkReadExternalPermission(EditRegistration_2_Activity.this)) {
                utils.hideKeyboard(EditRegistration_2_Activity.this);
                dialog_sel_image_user();
            }
        }


        rel_edit.setVisibility(View.VISIBLE);
        rel_reg.setVisibility(View.GONE);
        img_back.setVisibility(View.GONE);
    }


    @OnClick(R.id.img_back_top)
    public void OnBack2() {
        finish();
    }


    @OnClick(R.id.img_buss)
    public void selBussImage() {
        if (utils.checkReadExternalPermission(EditRegistration_2_Activity.this)&&utils.checkWriteExternalPermission(EditRegistration_2_Activity.this)) {
            utils.hideKeyboard(EditRegistration_2_Activity.this);
            dialog_sel_image_buss();
        } else {
            ActivityCompat.requestPermissions(
                    EditRegistration_2_Activity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE},
                    1
            );
        }

    }


    @OnClick(R.id.img_buss_cancel)
    public void onBussImgRemove() {
        if(str_upload1!=null) {
            img_buss.setImageDrawable(getResources().getDrawable(R.drawable.addimage));
            img_buss.setRotation(0);
            str_upload1 = null;
            str_upload1_back = null;
            img_buss_cancel.setVisibility(View.GONE);
            img_user_rotate_buss.setVisibility(View.GONE);
        }
        else if(str_upload1_back!=null) {
            img_buss.setImageDrawable(getResources().getDrawable(R.drawable.addimage));
            img_buss.setRotation(0);
            str_upload1 = null;
            str_upload1_back = null;
            img_buss_cancel.setVisibility(View.GONE);
            img_user_rotate_buss.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.img_rotate_buss)
    public void onBussImgRotate() {
        if(str_upload1!=null) {
            if(rotation_buss==0){
                rotation_buss = 90;
            }else if(rotation_buss==90){
                rotation_buss = 180;
            }else if(rotation_buss==180){
                rotation_buss = 270;
            }else if(rotation_buss==270){
                rotation_buss = 360;
            }else{
                rotation_buss = 0;
            }
            img_buss.setRotation(rotation_buss);
        }
       else  if(str_upload1_back!=null) {
            if(rotation_buss==0){
                rotation_buss = 90;
            }else if(rotation_buss==90){
                rotation_buss = 180;
            }else if(rotation_buss==180){
                rotation_buss = 270;
            }else if(rotation_buss==270){
                rotation_buss = 360;
            }else{
                rotation_buss = 0;
            }
            img_buss.setRotation(rotation_buss);
        }
    }

    public  void dialog_sel_image_buss(){
        final BottomSheetDialog dialog = new BottomSheetDialog (EditRegistration_2_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_sel_img, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);

        txt_title.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_gallery.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        txt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                userChoosenTask = getResources().getString(R.string.tak_pic);
                cameraIntent();

            }
        });
        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                userChoosenTask = getResources().getString(R.string.chuz_lib);
                galleryIntent();
            }
        });
        try{
            dialog.show();
        }catch (Exception e){

        }


    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                str_upload1 = "";
                str_upload1_back = "";
                onSelectFromGalleryResult(data);
                img_buss_cancel.setVisibility(View.VISIBLE);
                img_user_rotate_buss.setVisibility(View.VISIBLE);
            } else if (requestCode == REQUEST_CAMERA) {
                str_upload1 = "";
                str_upload1_back = "";
                onCaptureImageResult(data);
                img_buss_cancel.setVisibility(View.VISIBLE);
                img_user_rotate_buss.setVisibility(View.VISIBLE);
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        try {
            final Uri imageUri = data.getData();
            file_image_buss = new File(getRealPathFromURI(imageUri));

            img_buss.setImageURI(imageUri);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentURI, projection, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    private void onCaptureImageResult(Intent data) {
        Log.e("$ oncaputer", "oncap");
        File file = new File(Environment.getExternalStorageDirectory() + File.separator +
                "image.jpg");
        Bitmap thumbnail = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap resize_bitmap = getResizedBitmap(thumbnail, 1200);
        Matrix rotateRight = new Matrix();

        Bitmap resize_bit = null;
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(file.getPath());//Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +  timeStamp+"_picture.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Log.e("My camera Orientation", String.valueOf(orientation));
        switch (orientation) {

            case ExifInterface.ORIENTATION_UNDEFINED:
                Log.e("My UNDEF", "UNDEFI");
                rotateRight.postRotate(0);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
//                            resize_bitmap = BitmapTools.rotate(resize_bitmap, 90);
                Log.e("My SAMSUNG", "UNDEFI");
                rotateRight.postRotate(90);
                break;


            case ExifInterface.ORIENTATION_ROTATE_180:
//                            resize_bitmap = BitmapTools.rotate(resize_bitmap, 180);
                rotateRight.postRotate(180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
//                            resize_bitmap = BitmapTools.rotate(resize_bitmap, 270);
                rotateRight.postRotate(270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
//                            resize_bitmap = BitmapTools.rotate(resize_bitmap, 0);
                rotateRight.postRotate(0);
            default:
                break;
        }
        Bitmap image_view_bmap = null;
        try {                                       //resize_bitmap
            image_view_bmap = Bitmap.createBitmap(resize_bitmap, 0, 0, resize_bitmap.getWidth(), resize_bitmap.getHeight(), rotateRight, true);
        } catch (OutOfMemoryError ex)
        {
            ex.printStackTrace();
        }


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        image_view_bmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        }  catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e("3 Path", destination.getPath());
        Log.e("3 getAbsolutePath", destination.getAbsolutePath());
        Log.e("3 getName", destination.getName());
        file_image_buss = new File(getRealPathFromURI(Uri.parse(destination.getPath())));

        File imgFile = new File(file_image_buss.getPath());
        Uri img_uri = Uri.fromFile(imgFile);
        img_buss.setImageURI(img_uri);
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        resize_bitmap =getResizedBitmap(myBitmap, 1200);
//        img_buss.setImageBitmap(resize_bitmap);
    }


    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();
        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                //denied
                utils.hideKeyboard(EditRegistration_2_Activity.this);
                dialog_denyOne();
            }else{
                if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                    //allowed
                    utils.hideKeyboard(EditRegistration_2_Activity.this);
                    dialog_sel_image_user();

                } else{
                    utils.hideKeyboard(EditRegistration_2_Activity.this);
                    dialog_openStoragePer();
                    //set to never ask again
                    //do something here.
                }
            }
        }
    }


    public  void dialog_sel_image_user(){
        if(dialog_buss!=null){
            if(dialog_buss.isShowing()){
                dialog_buss.dismiss();
            }
        }
        dialog_buss = new BottomSheetDialog (EditRegistration_2_Activity.this);
        dialog_buss.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_sel_img, null);
        dialog_buss.setContentView(bottomSheetView);

        dialog_buss.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_camera = (TextView) dialog_buss.findViewById(R.id.txt_camera);
        TextView txt_gallery = (TextView) dialog_buss.findViewById(R.id.txt_gallery);
        TextView txt_ok = (TextView) dialog_buss.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog_buss.findViewById(R.id.txt_title);

        txt_title.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_gallery.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_gallery.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));


        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_buss.dismiss();
            }
        });

        txt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_buss.dismiss();
                userChoosenTask = getResources().getString(R.string.tak_pic);
                cameraIntent();

            }
        });
        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_buss.dismiss();
                userChoosenTask = getResources().getString(R.string.chuz_lib);
                galleryIntent();
            }
        });
        dialog_buss.show();

    }



    private void cameraIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        file_image_buss = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file_image_buss));
        intent.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_BACK);
        intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", false);
        startActivityForResult(intent, REQUEST_CAMERA);

    }


    private void galleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_FILE);
    }


    public  void dialog_denyOne(){
        if(dialog_deny!=null){
            if(dialog_deny.isShowing()){
                dialog_deny.dismiss();
            }
        }
        dialog_deny = new BottomSheetDialog (EditRegistration_2_Activity.this);
        dialog_deny.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog_deny.setContentView(bottomSheetView);

        dialog_deny.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok     = (TextView) dialog_deny.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog_deny.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog_deny.findViewById(R.id.txt_camera);

        txt_title.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_deny.dismiss();
            }
        });

        dialog_deny.show();
    }


    public  void dialog_openStoragePer(){
        if(dialog_storage!=null){
            if(dialog_storage.isShowing()){
                dialog_storage.dismiss();
            }
        }
        dialog_storage = new BottomSheetDialog (EditRegistration_2_Activity.this);
        dialog_storage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog_storage.setContentView(bottomSheetView);

        dialog_storage.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish = (Button) dialog_storage.findViewById(R.id.btn_finish);
        TextView txt_ok = (TextView) dialog_storage.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog_storage.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog_storage.findViewById(R.id.txt_camera);
        TextView txt_press = (TextView) dialog_storage.findViewById(R.id.txt_press);
        TextView txt_store = (TextView) dialog_storage.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        btn_finish.setTypeface(utils.OpenSans_Bold(EditRegistration_2_Activity.this));
        txt_ok.setTypeface(utils.OpenSans_Bold(EditRegistration_2_Activity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(EditRegistration_2_Activity.this));
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_storage.dismiss();
                sharedPreferenceLeadr.set_PERPRO("1");
                GlobalConstant.startInstalledAppDetailsActivity(EditRegistration_2_Activity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_storage.dismiss();
            }
        });

        dialog_storage.show();
    }


    @OnClick(R.id.btn_finish)
    public void OnSave() {
        utils.hideKeyboard(EditRegistration_2_Activity.this);

        if (edt_buss_name.getText().toString().trim().length() < 1) {
            utils.hideKeyboard(EditRegistration_2_Activity.this);
            dialog_msg_show(EditRegistration_2_Activity.this, getResources().getString(R.string.enter_buss_name),edt_buss_name);
        }else{
            method_Reg2();
        }
    }

    void method_Reg2() {
        if(str_upload1_back==null){
            sharedPreferenceLeadr.set_BUSS_THUMB("");
            sharedPreferenceLeadr.set_bus_img("");
        }
        String temp2 = sharedPreferenceLeadr.getUserId() +
                "_" + System.currentTimeMillis()+"2" + ".jpg";
        GlobalConstant.FILE_NAME2 = temp2.replaceAll(" ", "_");
            if(str_upload1!=null){
                if(progressDialog!=null){
                    if(progressDialog.isShowing()){
                        try{
                            progressDialog.dismiss();
                        }catch (Exception e){}
                    }
                }
                progressDialog = ProgressDialog.show(EditRegistration_2_Activity.this, "", getResources().getString(R.string.updat_wait));
                progressDialog.setCancelable(false);
                uploadThumbImgs_Buss("");
            }else{
                if(progressDialog!=null){
                    if(progressDialog.isShowing()){
                        try{
                            progressDialog.dismiss();
                        }catch (Exception e){}
                    }
                }
                progressDialog = ProgressDialog.show(EditRegistration_2_Activity.this, "", getResources().getString(R.string.updat_wait));
                progressDialog.setCancelable(false);
                api_Register();
            }
    }


    public void uploadThumbImgs_Buss(final String url1) {
        if(observerfull_buss!=null){
            observerfull_buss.cleanTransferListener();
        }

        Bitmap bmp = decodeFile(file_image_buss);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

        if(decoded.getWidth()>1200)
        {
            decoded.compress(Bitmap.CompressFormat.JPEG, 50, out);
        }
        else {
            decoded.compress(Bitmap.CompressFormat.JPEG, 80, out);
        }


        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(out.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }




        try {
            compressedImage = new Compressor(this).compressToFile(destination);
        } catch (IOException e) {
            e.printStackTrace();
            compressedImage = destination;
        }


        observerfull_buss = transferUtility.upload(Constants.BUCKET_NAME, System.currentTimeMillis()+"Leadr",
                compressedImage);

        String file_key = observerfull_buss.getKey();
        observerfull_buss.setTransferListener(new UploadListenerBussFull(file_key));
    }


    public void uploadThumbBussImg(final String url1) {
        if(observerthumb_buss!=null){
            observerthumb_buss.cleanTransferListener();
        }

        Bitmap bmp = decodeFile(file_image_buss);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

        if(decoded.getWidth()>500)
        {
            decoded.compress(Bitmap.CompressFormat.JPEG, 40, out);
        }
        else {
            decoded.compress(Bitmap.CompressFormat.JPEG, 50, out);
        }


        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(out.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try{
            compressedImage_thumb  = new Compressor(this)
                    .setMaxWidth(100)
                    .setMaxHeight(100)
                    .setQuality(30)
                    .setCompressFormat(Bitmap.CompressFormat.WEBP)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(destination);
        }catch (Exception e){compressedImage_thumb = destination;}

        observerthumb_buss = transferUtility.upload(Constants.BUCKET_NAME, System.currentTimeMillis()+"Leadr",
                compressedImage_thumb);


        String file_key = observerthumb_buss.getKey();
        observerthumb_buss.setTransferListener(new UploadListenerBussThumb(file_key));

    }


    private class UploadListenerBussFull implements TransferListener {
        String file_key_name = "";
        public UploadListenerBussFull( String file_key ) {
            this.file_key_name = file_key;
        }

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e("err1:", e.toString());
            if(progressDialog!=null) {
                try{
                    progressDialog.dismiss();
                }catch (Exception ew){}
            }
            observerfull_buss.cleanTransferListener();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.e("state1: ",newState.toString());
            if(newState.toString().equals("COMPLETED")) {

                Log.e("url3: ","https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name);
                buss_full = "https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name;
                observerfull_buss.cleanTransferListener();

                uploadThumbBussImg(buss_full);
            }
        }
    }


    private class UploadListenerBussThumb implements TransferListener {
        String file_key_name = "";
        public UploadListenerBussThumb( String file_key ) {
            this.file_key_name = file_key;
        }

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e("err1:", e.toString());
            if(progressDialog!=null) {
                if(progressDialog.isShowing()) {
                    try{
                        progressDialog.dismiss();
                    }catch (Exception ef){}
                }
            }
            observerthumb_buss.cleanTransferListener();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.e("state1: ",newState.toString());
            if(newState.toString().equals("COMPLETED")) {

                Log.e("url1: ","https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name);
                buss_thumb = "https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name;
                observerthumb_buss.cleanTransferListener();
                api_Register();
            }
        }
    }


    private Bitmap decodeFile(File f) {
        Bitmap b = null;

        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int IMAGE_MAX_SIZE = 1024;
        int scale = 1;
        if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
                    (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        try {
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        Log.d(TAG, "Width :" + b.getWidth() + " Height :" + b.getHeight());

        File destFile = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".png");
        try {
            FileOutputStream out = new FileOutputStream(destFile);
            b.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }


    private void api_Register() {
        AndroidNetworking.enableLogging();
        utils.dismissProgressdialog();

        String dev_token = "";
        try {
            dev_token = FirebaseInstanceId.getInstance().getToken();
        } catch (Exception e) {
            dev_token = sharedPreferenceLeadr.get_dev_token();
        }
//        phone = "+917696137554";

      /*  Log.e("url_signup: ", NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO);
        Log.e("job_title: ",  edt_jobtitle.getText().toString());
        Log.e("phone_number: ",  phone);
        Log.e("user_name: ",  edt_fullname.getText().toString());
        Log.e("business_name: ",  edt_buss_name.getText().toString());
        Log.e("business_fb_page: ",  edt_buss_page.getText().toString());
        Log.e("business_info: ",  edt_buss_info.getText().toString());
        Log.e("location_name: ",  loc);
        Log.e("lat: ",  lat);
        Log.e("lon: ",  long_);
        Log.e("category: ",  cate_sel_id);
        Log.e("user_image: ",  user_full);
        Log.e("user_image_thumb: ",  user_thumb);
        Log.e("business_image: ",  buss_full);
        Log.e("business_image_thumb: ",  buss_thumb);
        Log.e("device_token: ",  dev_token);
        Log.e("device_type: ",  "android");
        Log.e("id: ",  sharedPreferenceLeadr.get_UserId_Bfr());*/

        String id = sharedPreferenceLeadr.get_UserId_Bfr();
        String phone_number = sharedPreferenceLeadr.get_phone();
        if(buss_full.equals("")){
            if(str_upload1_back!=null) {
                if (sharedPreferenceLeadr.get_BUSS_THUMB().length() > 2) {
                    buss_full = sharedPreferenceLeadr.get_bus_img();
                    buss_thumb = sharedPreferenceLeadr.get_BUSS_THUMB();
                }
            }
        }


        String buss_info = GlobalConstant.ConvertToBase64(edt_buss_info.getText().toString()).trim();
        String buss_name = GlobalConstant.ConvertToBase64(edt_buss_name.getText().toString()).trim();
        String user_nm = sharedPreferenceLeadr.get_username();
        if(!user_nm.trim().equalsIgnoreCase("")){
            user_nm = GlobalConstant.ConvertToBase64(user_nm).trim();
        }
        String job_title = sharedPreferenceLeadr.get_jobTitle();
        if(!job_title.trim().equalsIgnoreCase("")){
            job_title = GlobalConstant.ConvertToBase64(job_title).trim();
        }

        String loc_name = "0";
        String country_name = "0";
        String add_ = "0";
        if(!sharedPreferenceLeadr.get_LOC_NAME().equalsIgnoreCase("")||!sharedPreferenceLeadr.get_LOC_NAME().equalsIgnoreCase("0")){
            loc_name = sharedPreferenceLeadr.get_LOC_NAME();
            loc_name = GlobalConstant.ConvertToBase64(loc_name).trim();
            country_name = sharedPreferenceLeadr.get_LOC_NAME_COUNTRY();
            country_name = GlobalConstant.ConvertToBase64(country_name).trim();
            add_ = sharedPreferenceLeadr.get_ADDRESS();
            add_ = GlobalConstant.ConvertToBase64(add_).trim();
        }

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO_VER2)
                .addBodyParameter("job_title", job_title)
                .addBodyParameter("id", id)
                .addBodyParameter("phone_number", phone_number)
                .addBodyParameter("user_name", user_nm)
                .addBodyParameter("business_name", buss_name)
                .addBodyParameter("business_fb_page", edt_buss_page.getText().toString())
                .addBodyParameter("business_info", buss_info)
                .addBodyParameter("location_name", loc_name)
                .addBodyParameter("lat", sharedPreferenceLeadr.get_LATITUDE())
                .addBodyParameter("lon", sharedPreferenceLeadr.get_LONGITUDE())
                .addBodyParameter("category", sharedPreferenceLeadr.get_CATEGORY())
                .addBodyParameter("user_image", sharedPreferenceLeadr.getProfilePicUrl())
                .addBodyParameter("business_image", buss_full)
                .addBodyParameter("device_token", dev_token)
                .addBodyParameter("device_type", "android")
                .addBodyParameter("country", country_name)
                .addBodyParameter("address", add_)
                .addBodyParameter("user_thum", sharedPreferenceLeadr.get_PROFILE_THUMB())
                .addBodyParameter("busi_thum", buss_thumb)
                .addBodyParameter("email", sharedPreferenceLeadr.get_EMAIL_REG())
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        if(progressDialog!=null) {
                            if(progressDialog.isShowing()) {
                                try{
                                    progressDialog.dismiss();
                                }catch (Exception ef){}
                            }
                        }
                        sharedPreferenceLeadr.setUserId(sharedPreferenceLeadr.get_UserId_Bfr());
                        if (response.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_bus_img(buss_full);
                            sharedPreferenceLeadr.set_BUSS_THUMB(buss_thumb);
                            sharedPreferenceLeadr.set_bus_name(edt_buss_name.getText().toString());
                            sharedPreferenceLeadr.set_bus_site(edt_buss_page.getText().toString());
                            sharedPreferenceLeadr.set_bus_info(edt_buss_info.getText().toString());
                            finish();
                        } else {
                            if(response.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(EditRegistration_2_Activity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }else{
                                utils.hideKeyboard(EditRegistration_2_Activity.this);
                                utils.dialog_msg_show(EditRegistration_2_Activity.this, response.optString("message"));
                            }
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                        if(progressDialog!=null) {
                            if(progressDialog.isShowing()) {
                                try{
                                    progressDialog.dismiss();
                                }catch (Exception ef){}
                            }
                        }
                    }
                });

    }

    public  void dialog_msg_show(Activity activity, String msg, final EditText edt){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }


    }
}
