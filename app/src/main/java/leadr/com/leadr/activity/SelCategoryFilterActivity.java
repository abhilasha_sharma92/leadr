package leadr.com.leadr.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import adapter.BusinessCat_Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import modal.CategoryPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

import static adapter.BusinessCat_Adapter.arr_cat_filter;
import static leadr.com.leadr.activity.FilterActivity.lst_cate;

/**
 * Created by Abhilasha on 5/12/2017.
 */

public class SelCategoryFilterActivity extends AppCompatActivity {

    @BindView(R.id.lnr_3)
    LinearLayout lnr_3;

    @BindView(R.id.recycl_cat)
    RecyclerView recycl_cat;

    @BindView(R.id.txt_bus_cc)
    TextView txt_bus_cc;

    @BindView(R.id.edt_search)
    EditText edt_search;

    @BindView(R.id.img_bck)
    ImageView img_bck;

    GlobalConstant utils;

    BusinessCat_Adapter adapter_cat;
    public static String cate_sel = "";
    public static String cate_sel_id = "";
    SharedPreferenceLeadr sharedPreferenceLeadr;

    ArrayList<CategoryPojo> arr_cat = new ArrayList<>();
    ArrayList<String> arr_sel_cat   = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selcat_activity);

        // bind the view using butterknife
        ButterKnife.bind(this);

        utils = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(SelCategoryFilterActivity.this);

        lnr_3.setVisibility(View.VISIBLE);

        if(utils.isNetworkAvailable(SelCategoryFilterActivity.this)){
            api_GetCategory();
        }else{
            utils.dialog_msg_show(SelCategoryFilterActivity.this,getResources().getString(R.string.no_internet));
        }

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                adapter_cat.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        edt_search.setTypeface(utils.OpenSans_Regular(SelCategoryFilterActivity.this));
        txt_bus_cc.setTypeface(utils.OpenSans_Regular(SelCategoryFilterActivity.this));
    }



    @OnClick(R.id.img_bck)
    public void onBack() {
       finish();
    }

    private void api_GetCategory(){
        AndroidNetworking.enableLogging();
        Log.e("url_getCat: ", NetworkingData.BASE_URL+ NetworkingData.GET_CATEGORY);
        utils.showProgressDialog(SelCategoryFilterActivity.this,getResources().getString(R.string.load_cat));
        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.GET_CATEGORY)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_getCat: ",result+"");

                        arr_cat.clear();
                        arr_sel_cat.clear();


                        if(result.optString("status").equals("1")){

                            String cat_id_sel = sharedPreferenceLeadr.get_CAT_CAT_FILTER_ID_TEMP();
                            if(cat_id_sel.contains(",")){
                                String[] animalsArray = cat_id_sel.split(",");
                                for(int i=0; i<animalsArray.length; i++){
                                    arr_sel_cat.add(animalsArray[i].trim());
                                }
                            }else{
                                arr_sel_cat.add(cat_id_sel.trim());
                            }

                            try {
                                JSONArray arr = result.getJSONArray("categories");
                                for(int i=0; i<arr.length(); i++) {
                                    JSONObject obj = arr.getJSONObject(i);
                                    CategoryPojo item = new CategoryPojo();
                                    item.setCategory(obj.getString("category_name"));
                                    item.setHebrew(obj.getString("hebrew"));
                                    item.setId(obj.getString("id"));
                                    item.setChecked(false);
                                    String same = null;
                                    for (int j = 0; j < arr_sel_cat.size(); j++) {
                                        if (obj.getString("id").equals(arr_sel_cat.get(j))) {
                                            same = "";
                                        }
                                    }
                                    if (same == null) {
                                        arr_cat.add(item);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if(result!=null){
                                    if(result.optString("message").contains("Suspended account")){
                                        AccountKit.logOut();
                                        sharedPreferenceLeadr.clearPreference();
                                        String languageToLoad  = "en";
                                        Locale locale = new Locale(languageToLoad);
                                        Locale.setDefault(locale);
                                        Configuration config = new Configuration();
                                        config.locale = locale;
                                        getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                        Intent i_nxt = new Intent(SelCategoryFilterActivity.this, GetStartedActivity.class);
                                        i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i_nxt);
                                        finish();
                                    }
                                }
                            }

                           /* for(int h=0; h<arr_sel_cat.size(); h++){
                            for(int g=0; g<arr_cat.size(); g++){
                              if(arr_cat.get(g).getId().equals(arr_sel_cat.get(h).trim())){
                                  CategoryPojo item = new CategoryPojo();
                                  item.setChecked(true);
                                  item.setCategory(arr_cat.get(g).getCategory());
                                  item.setId(arr_cat.get(g).getId());
                                  arr_cat.add(g,item);
                              }
                              }
                            }*/

                            LinearLayoutManager lnr_album = new LinearLayoutManager(SelCategoryFilterActivity.this, LinearLayoutManager.VERTICAL, false);
                            recycl_cat.setLayoutManager(lnr_album);
                            adapter_cat = new BusinessCat_Adapter(SelCategoryFilterActivity.this,arr_cat, new BusinessCat_Adapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(int i) {
                                    utils.hideKeyboard(SelCategoryFilterActivity.this);
                                    String same = null;
                                     for(int j=0; j<FilterActivity.arr_final_cat.size(); j++){
                                         if(FilterActivity.arr_final_cat.get(j).getId().equals(arr_cat_filter.get(i).getId())){
                                             same = "";
                                             break;
                                         }
                                     }
                                     if(same!=null){
                                         utils.dialog_msg_show(SelCategoryFilterActivity.this,getResources().getString(R.string.cat_exit));
                                     }else {

                                         if(FilterActivity.arr_final_cat!=null) {
                                             FilterActivity.arr_final_cat.add(arr_cat_filter.get(i));
                                         }
                                         adapter_cat.notifyDataSetChanged();
                                         if(adapter_cat!=null) {
                                             try{
                                                 FilterActivity.adapter_cat.notifyDataSetChanged();
                                             }catch (Exception e){}
                                         }
                                         GlobalConstant.setListViewHeightBasedOnItems(lst_cate, FilterActivity.arr_final_cat.size());

                                         List<String> arr_temp_id = new ArrayList<>();
                                         List<String> arr_temp_name = new ArrayList<>();
                                         List<String> arr_temp_namehe = new ArrayList<>();

                                         for (int k = 0; k < FilterActivity.arr_final_cat.size(); k++) {
                                             arr_temp_id.add(FilterActivity.arr_final_cat.get(k).getId().trim());
                                             arr_temp_name.add(FilterActivity.arr_final_cat.get(k).getCategory().trim());
                                             arr_temp_namehe.add(FilterActivity.arr_final_cat.get(k).getHebrew().trim());
                                         }

                                         sharedPreferenceLeadr.set_CAT_FILTER_NAME_TEMP(TextUtils.join(",", arr_temp_name));
                                         sharedPreferenceLeadr.set_CAT_FILTER_NAME_HE_TEMP(TextUtils.join(",", arr_temp_namehe));
                                         sharedPreferenceLeadr.set_CAT_FILTER_ID_TEMP(TextUtils.join(",", arr_temp_id));

                                         finish();
                                     }


                                }
                            });
                            recycl_cat.setAdapter(adapter_cat);
                        }
                        utils.dismissProgressdialog();
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }





}
