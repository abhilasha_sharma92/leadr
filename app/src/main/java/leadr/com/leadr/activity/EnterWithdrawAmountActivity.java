package leadr.com.leadr.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;

import org.json.JSONObject;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;


public class EnterWithdrawAmountActivity extends AppCompatActivity {

    @BindView(R.id.txt_how_mch)
    TextView txt_how_mch;

    @BindView(R.id.edt_amount)
    EditText edt_amount;

    @BindView(R.id.btn_nxt)
    Button btn_nxt;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.txt_withdraw)
    TextView txt_withdraw;

    GlobalConstant utils;
    String get_total;
    String admin_min;
    String min_amount = "0";
    SharedPreferenceLeadr sharedPreferenceLeadr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_withdraw_amount);

        ButterKnife.bind(this);
        utils = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(EnterWithdrawAmountActivity.this);

        method_SetTypeface();
        get_total = getIntent().getStringExtra("total");
        admin_min = getIntent().getStringExtra("admin_min");

        txt_withdraw.setText(getResources().getString(R.string.minn)+" $"+admin_min+" "+getResources().getString(R.string.maxx)+" $"+get_total);
        api_GetCount();
    }

    private void method_SetTypeface() {
        txt_how_mch.setTypeface(utils.OpenSans_Regular(EnterWithdrawAmountActivity.this));
        txt_withdraw.setTypeface(utils.OpenSans_Regular(EnterWithdrawAmountActivity.this));
        edt_amount.setTypeface(utils.OpenSans_Regular(EnterWithdrawAmountActivity.this));
        btn_nxt.setTypeface(utils.OpenSans_Regular(EnterWithdrawAmountActivity.this));
        txt_withdraw.setTypeface(utils.OpenSans_Regular(EnterWithdrawAmountActivity.this));
    }




    @OnClick(R.id.img_back)
    public void onBack() {
        finish();
    }



    private void api_GetCount(){
        AndroidNetworking.enableLogging();
        utils.showProgressDialog(EnterWithdrawAmountActivity.this, getResources().getString(R.string.load));

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_LEAD_INHOUR)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_count: ",result+"");
                        utils.dismissProgressdialog();
                        if(result.optString("status").equalsIgnoreCase("1")) {
                            min_amount = result.optString("min_amount");
                        }else if(result.optString("message").contains("Suspended account")){
                            AccountKit.logOut();
                            sharedPreferenceLeadr.clearPreference();
                            String languageToLoad  = "en";
                            Locale locale = new Locale(languageToLoad);
                            Locale.setDefault(locale);
                            Configuration config = new Configuration();
                            config.locale = locale;
                            getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                            Intent i_nxt = new Intent(EnterWithdrawAmountActivity.this, GetStartedActivity.class);
                            i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i_nxt);
                            finish();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }


    @OnClick(R.id.btn_nxt)
    public void onNxt() {
        if (edt_amount.getText().toString().trim().length() > 0) {
            if(Float.valueOf(edt_amount.getText().toString())>=Float.valueOf(admin_min)) {
                if(Float.valueOf(edt_amount.getText().toString())>Float.valueOf(get_total)){
                    utils.dialog_msg_show(EnterWithdrawAmountActivity.this,getResources().getString(R.string.am_more));
                }else {
                    Intent i_cnfrm = new Intent(EnterWithdrawAmountActivity.this, ConfirmWithdrawActivity.class);
                    i_cnfrm.putExtra("amount", edt_amount.getText().toString());
                    startActivity(i_cnfrm);
                    finish();
                }
            }else{
                utils.dialog_msg_show(EnterWithdrawAmountActivity.this,getResources().getString(R.string.less_am));
            }
        }else{
            utils.dialog_msg_show(EnterWithdrawAmountActivity.this,getResources().getString(R.string.amiunt));
        }

    }
}
