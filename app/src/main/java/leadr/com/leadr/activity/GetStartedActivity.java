package leadr.com.leadr.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

public class GetStartedActivity extends AppCompatActivity {

    @BindView(R.id.txt_policy)
    TextView txt_policy;

    @BindView(R.id.txt_service)
    TextView txt_service;


    @BindView(R.id.txt_sign)
    TextView txt_sign;

    @BindView(R.id.txt_privacy)
    TextView txt_privacy;

    @BindView(R.id.txt_and)
    TextView txt_and;

    @BindView(R.id.txt_eng_start)
    TextView txt_eng_start;

    @BindView(R.id.rel_approve)
    RelativeLayout rel_approve;

    @BindView(R.id.lnr_lang)
    RelativeLayout lnr_lang;

    @BindView(R.id.txt_start)
    TextView txt_start;

    @BindView(R.id.img_logo)
    ImageView img_logo;

    @BindView(R.id.progres_load)
    ProgressBar progres_load;


    public static int APP_REQUEST_CODE = 99;

    SharedPreferenceLeadr sharedPreferenceLeadr;
    GlobalConstant   utils;
    int change = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);

        // bind the view using butterknife
        ButterKnife.bind(this);

        sharedPreferenceLeadr = new SharedPreferenceLeadr(GetStartedActivity.this);
        utils = new GlobalConstant();


        txt_sign.setTypeface(utils.OpenSans_Regular(GetStartedActivity.this));
        txt_policy.setTypeface(utils.OpenSans_Regular(GetStartedActivity.this));
        txt_service.setTypeface(utils.OpenSans_Regular(GetStartedActivity.this));
        txt_start.setTypeface(utils.OpenSans_Regular(GetStartedActivity.this));
        txt_eng_start.setTypeface(utils.OpenSans_Regular(GetStartedActivity.this));


//        txt_and.setText(content_and);


        String token = FirebaseInstanceId.getInstance().getToken();

        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it"))
        {
            txt_eng_start.setText("ע ִב ר ִי ת");
//            lnr_lang.setBackground(getResources().getDrawable(R.drawable.hebrew));
            img_logo.setImageResource(R.drawable.beleadr_he);
        }else{
            txt_eng_start.setText("E N G L I S H");
//            lnr_lang.setBackground(getResources().getDrawable(R.drawable.english));
            img_logo.setImageResource(R.drawable.beleadr);
        }
    }



    /***Change language structure of APP at start****/
    @OnClick(R.id.lnr_lang)
    public void onChangLang() {
        dialog_changLang();
    }


    public  void dialog_changLang(){
        final BottomSheetDialog dialog = new BottomSheetDialog (GetStartedActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_chang_lang, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d     = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_title                 = (TextView) dialog.findViewById(R.id.txt_title);
        final TextView txt_eng             = (TextView) dialog.findViewById(R.id.txt_eng);
        final TextView txt_heb             = (TextView) dialog.findViewById(R.id.txt_heb);
        final TextView txt_ok              = (TextView) dialog.findViewById(R.id.txt_ok);
        RelativeLayout rel_eng             = (RelativeLayout) dialog.findViewById(R.id.rel_eng);
        RelativeLayout rel_heb             = (RelativeLayout) dialog.findViewById(R.id.rel_heb);
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        final ImageView img_tick_eng = (ImageView) dialog.findViewById(R.id.img_tick_eng);
        final ImageView img_tick_heb = (ImageView) dialog.findViewById(R.id.img_tick_heb);

        txt_title.setTypeface(utils.OpenSans_Regular(GetStartedActivity.this));
        txt_eng.  setTypeface(utils.OpenSans_Regular(GetStartedActivity.this));
        txt_heb.  setTypeface(utils.OpenSans_Regular(GetStartedActivity.this));
        txt_ok.   setTypeface(utils.OpenSans_Regular(GetStartedActivity.this));

        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(change==1) {
                    change = 0;
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            }
        });


        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en"))
        {
            img_tick_eng.setImageResource(R.drawable.tick_lang);
            img_tick_heb.setImageResource(R.drawable.gayab);
        }else{
            img_tick_eng.setImageResource(R.drawable.gayab);
            img_tick_heb.setImageResource(R.drawable.tick_lang);
        }

        rel_eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_tick_eng.setImageResource(R.drawable.tick_lang);
                img_tick_heb.setImageResource(R.drawable.gayab);
                txt_eng_start.setText("E N G L I S H");

//                lnr_lang.setBackground(getResources().getDrawable(R.drawable.english));

                change = 1;
                sharedPreferenceLeadr.set_LANGUAGE("en");
                String languageToLoad  = "en";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });

        rel_heb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_tick_eng.setImageResource(R.drawable.gayab);
                img_tick_heb.setImageResource(R.drawable.tick_lang);
                txt_eng_start.setText("עִברִית");

//                lnr_lang.setBackground(getResources().getDrawable(R.drawable.hebrew));

                change = 1;
                sharedPreferenceLeadr.set_LANGUAGE("it");
                String languageToLoad  = "it";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                if(change==1) {
                    change = 0;
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            }
        });
        dialog.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en"))
        {
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
//            rel_approve.setBackground(getResources().getDrawable(R.drawable.startbutton));

        }else{
            String languageToLoad  = "it";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
//            rel_approve.setBackground(getResources().getDrawable(R.drawable.startbutton_he));
        }
        SpannableString content = new SpannableString(getResources().getString(R.string.terms_serv));
        SpannableString content_priv = new SpannableString(getResources().getString(R.string.privacy));
        SpannableString content_and = new SpannableString(getResources().getString(R.string.and_with_space));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        content_priv.setSpan(new UnderlineSpan(), 0, content_priv.length(), 0);
        content_and.setSpan(new UnderlineSpan(), 0, content_and.length(), 0);
        txt_service.setText(content);
        txt_privacy.setText(content_priv);
    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }

    @OnClick(R.id.rel_approve)
    public void onButtonClick() {

        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
            String languageToLoad  = "he";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }
        final Intent intent = new Intent(GetStartedActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
//        api_ValidatePhone("+919646993150");//moto
//        api_ValidatePhone("+917696137554");//xiomi.................
//        api_ValidatePhone("+917986623724");//xiomi
//        api_ValidatePhone("+918437679082");//xiomi
//        api_ValidatePhone("+919592760171");//xiomi
//        api_ValidatePhone("+919988235623");//xiomi
//        api_ValidatePhone("+911234567890");//xiomi
//        api_ValidatePhone("+911234567898");//nexus
//        api_ValidatePhone("+911234567897");//nexus
//        api_ValidatePhone("+911234567896");//nexus
//        api_ValidatePhone("+919814274340");//nexus
//        api_ValidatePhone("+919814274341");//nexus
//        api_ValidatePhone("+972547876997");//nexus
//        api_ValidatePhone("+972547876997");//nexus......................................
//        api_ValidatePhone("+972527268362");//1 xiomi
//        api_ValidatePhone("+972523606892");//1 moto
//        api_ValidatePhone("+972543885322");//1 nexus
//        api_ValidatePhone("+972547876997");//1 nexus
//        api_ValidatePhone("+919872205068");//1 nexus
//        api_ValidatePhone("+910000000000");//1 nexus
//        api_ValidatePhone("+910000000009");//1 nexus
//        api_ValidatePhone("+910000000008");//1 redmi ...............
//        api_ValidatePhone("+910000000007");//1 nexus
//        api_ValidatePhone("+910000000006");//1 nexus
//        api_ValidatePhone("+910000000005");//1 nexus
//        api_ValidatePhone("+919463690078");//1 nexus
    }



    @OnClick(R.id.txt_service)
    public void onTermsClick() {
        Intent i_phn_screen = new Intent(GetStartedActivity.this, TermsPolicyActivity.class);
        i_phn_screen.putExtra("type","service");
        startActivity(i_phn_screen);
    }

    @OnClick(R.id.txt_privacy)
    public void onPrivacy() {
        Intent i_phn_screen = new Intent(GetStartedActivity.this, TermsPolicyActivity.class);
        i_phn_screen.putExtra("type","policy");
        startActivity(i_phn_screen);
    }


    @Override
    protected void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
            String languageToLoad  = "it";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }

        if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
//                showErrorActivity(loginResult.getError());
                Log.e("error: ",loginResult.getError()+"");
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
                utils.dialog_msg_show(GetStartedActivity.this,getResources().getString(R.string.login_cancl));
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                } else {
                    toastMessage = String.format(
                            "Success:%s...",
                            loginResult.getAuthorizationCode().substring(0,10));
                    Log.e("nmbr: ", loginResult.getAuthorizationCode().substring(0,10));
                }

                // If you have an authorization code, retrieve it from
                // loginResult.getAuthorizationCode()
                // and pass it to your server and exchange it for an access token.

                // Success! Start your next activity...

                method_Validate();
            }

            // Surface the result to your user in an appropriate way.
           /* Toast.makeText(
                    this,
                    toastMessage,
                    Toast.LENGTH_LONG)
                    .show();*/
        }
    }


    void method_Validate(){
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                final PhoneNumber number = account.getPhoneNumber();
                sharedPreferenceLeadr.set_signup("yes");
                sharedPreferenceLeadr.set_number(number.toString());
                api_ValidatePhone(number.toString());
            }

            @Override
            public void onError(final AccountKitError error) {
            }
        });
    }


    private void api_ValidatePhone(final String phone){
       if(progres_load!=null){
           progres_load.setVisibility(View.VISIBLE);
       }
        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
            String languageToLoad  = "it";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }

        AndroidNetworking.enableLogging();
        utils.showProgressDialog(this,"Loading ...");
        Log.e("url_val_phone: ", NetworkingData.BASE_URL+ NetworkingData.LOGIN);

        String dev_token = "";
        try{
            dev_token = FirebaseInstanceId.getInstance().getToken();
        }catch (Exception e){
            dev_token = sharedPreferenceLeadr.get_dev_token();
        }


        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.LOGIN)
                .addBodyParameter("phone_number",phone)
                .addBodyParameter("device_token",dev_token)
                .addBodyParameter("device_type","android")
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_login: ",result+"");
                        /* {"status":1,"message":"Logged-in successfully..","details":
                        [{"id":4,"phone_number":"+919646993150","user_name":"me 9646","user_image":"",
                        "user_thum":"","account_balance":0,"job_title":"","business_name":"ug","business_info":"",
                        "business_image":"","busi_thum":"","business_fb_page":"","category":"1","location_name":"",
                        "country":"","address":"0","lat":"","lon":"","stripe_id":"0","last_digits":"0",
                        "device_type":"android",
                        "device_token":"d5YRTII2hfo:APA91bEUZcgYjDo0pmHnWD0aCNSBB5hk6iFBHDjWwU1pV2XJP-CSa85j1QsDzHwliKVzF1qqf8teNxTXGlsD9pDw787qhlYYF2ltsIP0epBLefCyb2Fw2cCbdRaSIul75eg4HmsuF9Wz",
                        "chat_noti":1,"chat_sound":0,"new_ticket_noti":0,"new_ticket_sound":0,"new_sell_noti":0,"new_sell_sound":0,
                        "widget":0,"dimension_sys":0,"is_delete":0}],"chk":"1"}*/

                        utils.dismissProgressdialog();

                        String user_name = "";
                        sharedPreferenceLeadr.set_UserId_Bfr(result.optString("user_id"));
//                        Log.e("id: ",result.optString("user_id"));
                        if(progres_load!=null){
                            progres_load.setVisibility(View.GONE);
                        }
                        try {
                            JSONArray arr = result.getJSONArray("details");
                            for(int i=0; i<arr.length(); i++){
                                JSONObject obj = arr.getJSONObject(i);
                                user_name= obj.getString("user_name");
//                                Log.e("username: ",user_name);
                                if(!user_name.equals("0")) {
                                    String bus_name = obj.getString("business_name");
                                        try {
                                            try {
                                                bus_name = new String(Base64.decode(bus_name.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    String bus_info = obj.getString("business_info");
                                        try {
                                            try {
                                                bus_info = new String(Base64.decode(bus_info.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    String job_title = obj.getString("job_title");
                                        try {
                                            try {
                                                job_title = new String(Base64.decode(job_title.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    if(!user_name.equalsIgnoreCase("null")) {
                                            try {
                                                try {
                                                    user_name = new String(Base64.decode(user_name.getBytes(), Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (IllegalArgumentException e) {
                                                e.printStackTrace();
                                            }

                                    }

                                    sharedPreferenceLeadr.setUserId(obj.getString("id"));
                                    sharedPreferenceLeadr.set_UserId_Bfr(obj.getString("id"));
                                    sharedPreferenceLeadr.set_phone(obj.getString("phone_number"));
                                    sharedPreferenceLeadr.set_username(user_name);
                                    sharedPreferenceLeadr.setProfilePicUrl(obj.getString("user_image"));
                                    sharedPreferenceLeadr.set_jobTitle(job_title);
                                    sharedPreferenceLeadr.set_bus_name(bus_name);
                                    sharedPreferenceLeadr.set_bus_info(bus_info);
                                    sharedPreferenceLeadr.set_bus_img(obj.getString("business_image"));
                                    sharedPreferenceLeadr.set_bus_site(obj.getString("business_fb_page"));
                                    sharedPreferenceLeadr.set_PROFILE_THUMB(obj.getString("user_thum"));
                                    sharedPreferenceLeadr.set_BUSS_THUMB(obj.getString("busi_thum"));
                                    sharedPreferenceLeadr.set_CHAT_NOTIFY(obj.getString("chat_noti"));
                                    sharedPreferenceLeadr.set_CHAT_SOUND(obj.getString("chat_sound"));
                                    sharedPreferenceLeadr.set_TICKET_NOTIFY(obj.getString("new_ticket_noti"));
                                    sharedPreferenceLeadr.set_TICKET_SOUND(obj.getString("new_ticket_sound"));
                                    sharedPreferenceLeadr.set_SELL_NOTIFY(obj.getString("new_sell_noti"));
                                    sharedPreferenceLeadr.set_SELL_SOUND(obj.getString("new_sell_sound"));
                                    sharedPreferenceLeadr.set_NEWSELL_SOUND(obj.getString("new_lead_sound"));
                                    sharedPreferenceLeadr.set_NEWSELL_NOTIFY(obj.getString("new_lead_noti"));
                                    sharedPreferenceLeadr.set_WIDGET(obj.getString("widget"));

                                    if(obj.optString("stripe_id")!=null){
                                        if(!obj.optString("stripe_id").equalsIgnoreCase("null")){
                                            if(!obj.optString("stripe_id").equalsIgnoreCase("0")){
                                                sharedPreferenceLeadr.set_CARD("256");
                                            }
                                        }
                                    }
                                    sharedPreferenceLeadr.set_DIMENSION(obj.getString("dimension_sys"));
                                    if(obj.getString("email")!=null){
                                        if(obj.getString("email").trim().length()>0){
                                            if(!obj.getString("email").equals("0")){
                                                sharedPreferenceLeadr.set_EMAIL_REG(obj.getString("email"));
                                            }
                                        }
                                    }


                                    String cat_id = obj.getString("category");
                                    if(cat_id.contains(",")){
                                        String[] animalsArray = cat_id.split(",");
                                        cat_id = animalsArray[0].trim();
                                    }

                                    sharedPreferenceLeadr.set_CATEGORY(obj.getString("category"));
                                    sharedPreferenceLeadr.set_CAT_FILTER(obj.getString("category"));
                                    sharedPreferenceLeadr.set_CAT_FILTER_ID_TEMP(obj.getString("category"));
                                    sharedPreferenceLeadr.set_CATEGORY_SEL_ID(obj.getString("category"));

                                    String loc = obj.getString("location_name");
                                    if(loc.equals("0")){
                                        sharedPreferenceLeadr.set_LOC_TYPE("0");
                                        sharedPreferenceLeadr.set_LOC_TYPE_FILTER("0");
                                    }
                                    else if(loc.trim().equals("")){
                                        sharedPreferenceLeadr.set_LOC_TYPE("0");
                                        sharedPreferenceLeadr.set_LOC_TYPE_FILTER("0");
                                    }else{
                                        sharedPreferenceLeadr.set_LOC_TYPE("1");
                                        sharedPreferenceLeadr.set_LOC_TYPE_FILTER("1");

                                            try {
                                                try {
                                                    loc = new String(Base64.decode(loc.getBytes(),Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (IllegalArgumentException e) {
                                                e.printStackTrace();
                                            }


                                        String country_name = obj.getString("country");
                                            try {
                                                try {
                                                    country_name = new String(Base64.decode(country_name.getBytes(),Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (IllegalArgumentException e) {
                                                e.printStackTrace();

                                        }

                                        String add = obj.getString("address");
                                            try {
                                                try {
                                                    add = new String(Base64.decode(add.getBytes(),Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (IllegalArgumentException e) {
                                                e.printStackTrace();

                                        }

                                        sharedPreferenceLeadr.set_LOC_NAME(loc);
                                        sharedPreferenceLeadr.set_LOC_NAME_TEMP(loc);
                                        sharedPreferenceLeadr.set_LATITUDE(obj.getString("lat"));
                                        sharedPreferenceLeadr.set_LATITUDE_TEMP(obj.getString("lat"));
                                        sharedPreferenceLeadr.set_LONGITUDE(obj.getString("lon"));
                                        sharedPreferenceLeadr.set_LONGITUDE_TEMP(obj.getString("lon"));
                                        sharedPreferenceLeadr.set_LOC_NAME_COUNTRY(country_name);
                                        sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP(country_name);
                                        sharedPreferenceLeadr.set_ADDRESS(add);
                                        sharedPreferenceLeadr.set_ADDRESS_TEMP(add);
                                    }

                                }else{
                                    sharedPreferenceLeadr.set_UserId_Bfr(obj.optString("id"));
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                        Log.e("id2: ",sharedPreferenceLeadr.get_UserId_Bfr());
                        if(result.optString("status").equals("1")){
                            if(user_name.equals("0")){
                                Intent i_nxt = new Intent(GetStartedActivity.this, Bfr_Reg_Activity.class);
                                i_nxt.putExtra("phone",phone);
                                i_nxt.putExtra("type","reg");
                                startActivity(i_nxt);
//                                overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                            }else if(user_name.equals("null")){
                                Intent i_nxt = new Intent(GetStartedActivity.this, Bfr_Reg_Activity.class);
                                i_nxt.putExtra("phone",phone);
                                i_nxt.putExtra("type","reg");
                                startActivity(i_nxt);
//                                overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                            }else {
//                                sharedPreferenceLeadr.set_MIN_BUDGET("0");
                                sharedPreferenceLeadr.set_DATEINSTALLED(CurrentDate());
                                AppEventsLogger.setUserID(sharedPreferenceLeadr.getUserId());

                                Bundle user_props = new Bundle();
                                user_props.putString("Name", sharedPreferenceLeadr.get_username());
                                user_props.putString("Name business", sharedPreferenceLeadr.get_bus_name());
                                user_props.putString("Description business", sharedPreferenceLeadr.get_bus_info());
                                user_props.putString("Location business", sharedPreferenceLeadr.get_LOC_NAME());
                                user_props.putString("Category business", sharedPreferenceLeadr.get_CATEGORY());
                                AppEventsLogger.updateUserProperties(user_props, new GraphRequest.Callback() {
                                    @Override
                                    public void onCompleted(GraphResponse response) {

                                    }
                                });
//                                sharedPreferenceLeadr.set_MIN_BUDGET("0");
                                sharedPreferenceLeadr.set_FROM_PRICE("0");
//                                sharedPreferenceLeadr.set_TO_PRICE("0");

                                Intent i_nxt = new Intent(GetStartedActivity.this, MainActivity.class);
                                i_nxt.putExtra("phone", phone);
                                i_nxt.putExtra("type", "login");
                                startActivity(i_nxt);
//                                overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                                finish();
                            }
                        }else{
                            if(result.optString("message").contains("You have blocked by admin!")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                utils.dialog_msg_show(GetStartedActivity.this,getResources().getString(R.string.blocked));
                            }else {
                                Intent i_nxt = new Intent(GetStartedActivity.this, Bfr_Reg_Activity.class);
                                i_nxt.putExtra("phone", phone);
                                i_nxt.putExtra("type", "reg");
                                startActivity(i_nxt);
                            }
//                            overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }

    String CurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
}
