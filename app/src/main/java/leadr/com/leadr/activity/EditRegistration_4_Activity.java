package leadr.com.leadr.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import adapter.BusinessCat_Adapter;
import adapter.ProfileLocAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import leadr.com.leadr.activity.RegistrationActivity.LocRegister_Adapter;
import modal.CategoryPojo;
import modal.LatlngPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

/**
 * Created by Admin on 2/20/2018.
 */

public class EditRegistration_4_Activity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks{

    @BindView(R.id.lnr_1)
    LinearLayout lnr_1;

    @BindView(R.id.lnr_4)
    LinearLayout lnr_4;

    @BindView(R.id.txt_locc_an)
    TextView txt_locc_an;


    @BindView(R.id.txt_empty)
    TextView txt_empty;

    @BindView(R.id.txt_add_lc)
    TextView txt_add_lc;

    @BindView(R.id.recycl_loc)
    RecyclerView recycl_loc;


    @BindView(R.id.btn_nxt)
    Button btn_nxt;

    @BindView(R.id.btn_finish)
    Button btn_finish;

    @BindView(R.id.lnr_bottom_3)
    LinearLayout lnr_bottom_3;

    @BindView(R.id.lnr_bottom_3_2)
    LinearLayout lnr_bottom_3_2;

    @BindView(R.id.lnr_3)
    LinearLayout lnr_3;

    @BindView(R.id.rel_final)
    RelativeLayout rel_final;

    @BindView(R.id.lnr_5)
    LinearLayout lnr_5;

    @BindView(R.id.txt_loc_4)
    TextView txt_loc_4;

    @BindView(R.id.btn_finish_2)
    Button btn_finish_2;

    @BindView(R.id.card_full2)
    CardView card_full2;

    @BindView(R.id.card_full)
    CardView card_full;

    @BindView(R.id.btn_nxt_2)
    Button btn_nxt_2;

    @BindView(R.id.rel_add_loc1)
    RelativeLayout rel_add_loc1;

    @BindView(R.id.rel_4)
    RelativeLayout rel_4;

    @BindView(R.id.rel_main)
    RelativeLayout rel_main;

    @BindView(R.id.rel_bottom2)
    RelativeLayout rel_bottom2;

    @BindView(R.id.rel_bottom)
    RelativeLayout rel_bottom;

    @BindView(R.id.txt_four)
    TextView txt_four;

    @BindView(R.id.txt_five)
    TextView txt_five;

    @BindView(R.id.txt_three)
    TextView txt_three;

    @BindView(R.id.txt_two)
    TextView txt_two;

    @BindView(R.id.vw_shadow)
    View vw_shadow;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.img_back_top)
    ImageView img_back_top;

    @BindView(R.id.rel_edit)
    RelativeLayout rel_edit;

    @BindView(R.id.rel_reg)
    RelativeLayout rel_reg;

    GlobalConstant utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    String cate_sel = "";
    String cate_sel_id = "";

    LocRegister_Adapter adapter_loc;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient mGoogleApiClient;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private Double latitude = 0.0, longitude = 0.0;
    String get_cityName, getCountryName;

    ArrayList<LatlngPojo> arr_place = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_reg);

        ButterKnife.bind(this);

        utils = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(EditRegistration_4_Activity.this);

        mGoogleApiClient = new GoogleApiClient.Builder(EditRegistration_4_Activity.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();

        txt_locc_an.setTypeface(utils.OpenSans_Regular(EditRegistration_4_Activity.this));
        txt_empty.setTypeface(utils.OpenSans_Regular(EditRegistration_4_Activity.this));
        txt_add_lc.setTypeface(utils.OpenSans_Regular(EditRegistration_4_Activity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(EditRegistration_4_Activity.this));


        btn_nxt.setVisibility(View.GONE);
        lnr_1.setVisibility(View.GONE);
        btn_finish.setVisibility(View.VISIBLE);
        img_back.setVisibility(View.VISIBLE);

        txt_four.setBackground(getResources().getDrawable(R.drawable.four_ac));
        txt_five.setBackground(getResources().getDrawable(R.drawable.five_ac));
        txt_three.setBackground(getResources().getDrawable(R.drawable.three_ac));
        txt_two.setBackground(getResources().getDrawable(R.drawable.two_ac));

        rel_main.setBackground(getResources().getDrawable(R.color.colorGraybg));

        lnr_bottom_3.setVisibility(View.GONE);
        lnr_bottom_3_2.setVisibility(View.GONE);
        card_full.setVisibility(View.VISIBLE);
        card_full2.setVisibility(View.GONE);
        vw_shadow.setVisibility(View.GONE);
        lnr_3.setVisibility(View.GONE);
        recycl_loc.setVisibility(View.GONE);
        card_full.setVisibility(View.GONE);
        lnr_4.setVisibility(View.GONE);
        btn_finish.setVisibility(View.VISIBLE);
        btn_finish_2.setVisibility(View.VISIBLE);
        txt_loc_4.setVisibility(View.VISIBLE);
        lnr_5.setVisibility(View.VISIBLE);
        btn_nxt.setVisibility(View.GONE);
        btn_nxt_2.setVisibility(View.GONE);

        rel_bottom2.setVisibility(View.GONE);
        rel_bottom.setVisibility(View.VISIBLE);
        rel_add_loc1.setVisibility(View.VISIBLE);
        rel_final.setVisibility(View.VISIBLE);

        btn_finish.setText(getResources().getString(R.string.save));


        if(sharedPreferenceLeadr.get_LOC_NAME().equals("")){
            if(!sharedPreferenceLeadr.get_LOC_TYPE().equals("0")){
                method_Add_Loc();
            }
        }
        else if(sharedPreferenceLeadr.get_LOC_NAME().equals("0")){
            if(!sharedPreferenceLeadr.get_LOC_TYPE().equals("0")){
                method_Add_Loc();
            }
        }else{
            method_Add_Loc();
        }

    }

    @OnClick(R.id.img_back)
    public void OnBack() {
        finish();
    }

    void method_Add_Loc(){


        arr_place.clear();

        String loc_sel = sharedPreferenceLeadr.get_LOC_NAME();
        String lat_sel = sharedPreferenceLeadr.get_LATITUDE();
        String long_sel = sharedPreferenceLeadr.get_LONGITUDE();
        String country_sel = sharedPreferenceLeadr.get_LOC_NAME_COUNTRY();
        String add_sel = sharedPreferenceLeadr.get_ADDRESS();

        if(loc_sel.contains("@")){
            String[] animalsArray = loc_sel.split("@");
            String[] longArray = long_sel.split(",");
            String[] latArray = lat_sel.split(",");
            String[] countryArray = country_sel.split("@");
            String[] addArray = add_sel.split("@");
            for(int i=0; i<animalsArray.length; i++){
                LatlngPojo item = new LatlngPojo();
                item.setName(animalsArray[i].trim());
                item.setLong_(longArray[i].trim());
                item.setLat(latArray[i].trim());
                item.setCountryname(countryArray[i].trim());
                item.setAddress(addArray[i].trim());

                arr_place.add(item);
            }
        }else{
            LatlngPojo item = new LatlngPojo();
            item.setName(loc_sel.trim());
            item.setLong_(long_sel.trim());
            item.setLat(lat_sel.trim());
            item.setCountryname(country_sel.trim());
            item.setAddress(add_sel.trim());

            arr_place.add(item);
        }

        rel_add_loc1.setVisibility(View.GONE);

        rel_4.setVisibility(View.VISIBLE);
        recycl_loc.setVisibility(View.VISIBLE);
        lnr_4.setVisibility(View.VISIBLE);
        txt_loc_4.setVisibility(View.VISIBLE);
        rel_final.setVisibility(View.GONE);
        lnr_5.setVisibility(View.GONE);

        recycl_loc.setVisibility(View.VISIBLE);
        LinearLayoutManager lnr_album = new LinearLayoutManager(EditRegistration_4_Activity.this,
                LinearLayoutManager.VERTICAL, false);
        recycl_loc.setLayoutManager(lnr_album);
        adapter_loc = new LocRegister_Adapter(EditRegistration_4_Activity.this, arr_place);
        recycl_loc.setAdapter(adapter_loc);
    }

    @OnClick(R.id.btn_finish)
    public void OnSave() {
        utils.hideKeyboard(EditRegistration_4_Activity.this);

        api_Register();
    }



    @OnClick(R.id.rel_add_loc)
    public void onAddLoc() {
        findPlace(PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }


    @OnClick(R.id.rel_add_loc1)
    public void onAddLoc1() {
        findPlace(PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }


    public void findPlace(int placeAutocompleteRequestCode) {
        LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
                new LatLng(latitude, longitude), new LatLng(latitude, longitude));
        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
            String languageToLoad  = "he";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).build();
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                            .setFilter(typeFilter)
                            .build(EditRegistration_4_Activity.this);
            startActivityForResult(intent, placeAutocompleteRequestCode);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }



    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(EditRegistration_4_Activity.this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }


    @OnClick(R.id.img_back_top)
    public void OnBack2() {
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        rel_edit.setVisibility(View.VISIBLE);
        rel_reg.setVisibility(View.GONE);
        img_back.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                String languageToLoad  = "he";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getResources().updateConfiguration(config,getResources().getDisplayMetrics());
            }else{
                String languageToLoad  = "en";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getResources().updateConfiguration(config,getResources().getDisplayMetrics());
            }
            if (resultCode == RESULT_OK) {

                utils.hideKeyboard(EditRegistration_4_Activity.this);
                Place place = PlaceAutocomplete.getPlace(EditRegistration_4_Activity.this, data);


                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")) {
                    method_Loc_API(place.getId(), place.getLatLng().latitude, place.getLatLng().longitude,"en");
                }else{
                    method_Loc_API(place.getId(), place.getLatLng().latitude, place.getLatLng().longitude,"he");
                }


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(EditRegistration_4_Activity.this, data);
                // TODO: Handle the error.
                Log.i("", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }



    /***Get Address***/
    private void method_Loc_API( String id, final double latitude,final  double longitude,String language ) {
        AndroidNetworking.enableLogging();
        Log.e("url_locApi: ", NetworkingData.BASEURL_LOCATION+ id+"&key="+getResources().getString(R.string.gmail_key)
                +"&language="+language);

        AndroidNetworking.get(NetworkingData.BASEURL_LOCATION+ id+"&key="+getResources().getString(R.string.gmail_key)
                +"&language="+language)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_locApi: ",result+"");
                        //{"status":1,"message":"Details..","admin_settings":[{"id":1,"admin_id":1,"number_of_leads":4,"min_amount":"1
                        //  ","max_lead_price":"100","market_fee":"44"}]}

                        try{
                            JSONObject arr = result.getJSONObject("result");
                            String full_add = arr.getString("formatted_address");
                            Log.e("add: ",full_add);

                            LatlngPojo pojo = new LatlngPojo();
                            pojo.setName(full_add);
                            pojo.setLat(latitude+"");
                            pojo.setLong_(longitude+"");
                            pojo.setCountryname(full_add);
                            pojo.setAddress(full_add);
                            arr_place.add(pojo);

                            method_AddLoc_in_arry();
                        }catch (Exception e){
                            String g = e.toString();
                        }
                        utils.dismissProgressdialog();


                    }

                    private void method_AddLoc_in_arry() {
                        if (arr_place.size() > 0) {
                            rel_add_loc1.setVisibility(View.GONE);

                            rel_4.setVisibility(View.VISIBLE);
                            recycl_loc.setVisibility(View.VISIBLE);
                            lnr_4.setVisibility(View.VISIBLE);
                            txt_loc_4.setVisibility(View.VISIBLE);
                            rel_final.setVisibility(View.GONE);
                            lnr_5.setVisibility(View.GONE);
                        } else {
                            lnr_bottom_3.setVisibility(View.GONE);
                            lnr_bottom_3_2.setVisibility(View.GONE);
                            lnr_3.setVisibility(View.GONE);
                            recycl_loc.setVisibility(View.GONE);
                            card_full.setVisibility(View.GONE);
                            lnr_4.setVisibility(View.GONE);
                            btn_finish.setVisibility(View.VISIBLE);
                            btn_finish_2.setVisibility(View.VISIBLE);
                            txt_loc_4.setVisibility(View.VISIBLE);
                            lnr_5.setVisibility(View.VISIBLE);
                            btn_nxt.setVisibility(View.GONE);
                            btn_nxt_2.setVisibility(View.GONE);
                        }
                        recycl_loc.setVisibility(View.VISIBLE);
                        LinearLayoutManager lnr_album = new LinearLayoutManager(EditRegistration_4_Activity.this, LinearLayoutManager.VERTICAL, false);
                        recycl_loc.setLayoutManager(lnr_album);
                        adapter_loc = new LocRegister_Adapter(EditRegistration_4_Activity.this, arr_place);
                        recycl_loc.setAdapter(adapter_loc);
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();

                    }
                });
    }


    public void getAddress(double lat, double lng, boolean isEdit,String address) {
        Geocoder geocoder = new Geocoder(EditRegistration_4_Activity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            getCountryName = obj.getCountryName();

            try {
                get_cityName = obj.getSubAdminArea().toString();
            } catch (Exception e) {
                e.printStackTrace();
                get_cityName = obj.getLocality();
            }

            if(get_cityName==null){
                get_cityName = address;
            }
            LatlngPojo pojo = new LatlngPojo();
            pojo.setName(get_cityName);
            pojo.setLat(lat+"");
            pojo.setLong_(lng+"");
            pojo.setCountryname(getCountryName);
            pojo.setAddress(address);
            arr_place.add(pojo);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    public class LocRegister_Adapter extends RecyclerView.Adapter<LocRegister_Adapter.MyViewHolder> {

        Activity ctx;
        LocRegister_Adapter.MyViewHolder holderItem;

        public LocRegister_Adapter(Activity ctx, ArrayList<LatlngPojo> arr_place) {
            this.ctx = ctx;
        }


        @Override
        public LocRegister_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(ctx).inflate(R.layout.item_loc, parent, false);
            holderItem = new LocRegister_Adapter.MyViewHolder(itemView);

            return new LocRegister_Adapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(LocRegister_Adapter.MyViewHolder holder, int position) {
            holder.bind(position);
        }


        @Override
        public int getItemCount() {
            return arr_place.size();
        }


        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView txt_loc;
            ImageView img_remove;
            View vw_line;


            public MyViewHolder(View view) {
                super(view);

                txt_loc = (TextView) view.findViewById(R.id.txt_loc);
                img_remove = (ImageView) view.findViewById(R.id.img_remove);
                vw_line = (View) view.findViewById(R.id.vw_line);

                if (view.getTag() != null) {
                    view.setTag(holderItem);
                }


            }

            public void bind(final int i) {

                if(!arr_place.get(i).getCountryname().equals("")){
                    txt_loc.setText(arr_place.get(i).getName()+", "+arr_place.get(i).getCountryname());
                }else {
                    txt_loc.setText(arr_place.get(i).getName());
                }
                txt_loc.setText(arr_place.get(i).getAddress());
                txt_loc.setTypeface(utils.OpenSans_Regular(EditRegistration_4_Activity.this));

                if(i==arr_place.size()-1){
                    vw_line.setVisibility(View.GONE);
                }

                img_remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        arr_place.remove(arr_place.get(i));

                        notifyDataSetChanged();
                        if (arr_place.size() > 0) {
                            rel_add_loc1.setVisibility(View.GONE);

                            rel_4.setVisibility(View.VISIBLE);
                            recycl_loc.setVisibility(View.VISIBLE);
                            lnr_4.setVisibility(View.VISIBLE);
                            txt_loc_4.setVisibility(View.VISIBLE);
                            rel_final.setVisibility(View.GONE);
                            lnr_5.setVisibility(View.GONE);
                            rel_add_loc1.setVisibility(View.GONE);
                        } else {
                            lnr_bottom_3.setVisibility(View.GONE);
                            lnr_bottom_3_2.setVisibility(View.GONE);
                            lnr_3.setVisibility(View.GONE);
                            recycl_loc.setVisibility(View.GONE);
                            card_full.setVisibility(View.GONE);
                            lnr_4.setVisibility(View.GONE);
                            btn_finish.setVisibility(View.VISIBLE);
                            btn_finish_2.setVisibility(View.VISIBLE);
                            txt_loc_4.setVisibility(View.VISIBLE);
                            rel_final.setVisibility(View.VISIBLE);
                            rel_add_loc1.setVisibility(View.VISIBLE);
                            lnr_5.setVisibility(View.VISIBLE);
                            btn_nxt.setVisibility(View.GONE);
                            btn_nxt_2.setVisibility(View.GONE);
                            vw_line.setVisibility(View.GONE);
                        }
                    }
                });


            }


        }

    }

    private void api_Register() {
        AndroidNetworking.enableLogging();
        utils.dismissProgressdialog();
        utils.showProgressDialog(this, getResources().getString(R.string.load));

        String dev_token = "";
        try {
            dev_token = FirebaseInstanceId.getInstance().getToken();
        } catch (Exception e) {
            dev_token = sharedPreferenceLeadr.get_dev_token();
        }
//        phone = "+917696137554";

      /*  Log.e("url_signup: ", NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO);
        Log.e("job_title: ",  edt_jobtitle.getText().toString());
        Log.e("phone_number: ",  phone);
        Log.e("user_name: ",  edt_fullname.getText().toString());
        Log.e("business_name: ",  edt_buss_name.getText().toString());
        Log.e("business_fb_page: ",  edt_buss_page.getText().toString());
        Log.e("business_info: ",  edt_buss_info.getText().toString());
        Log.e("location_name: ",  loc);
        Log.e("lat: ",  lat);
        Log.e("lon: ",  long_);
        Log.e("category: ",  cate_sel_id);
        Log.e("user_image: ",  user_full);
        Log.e("user_image_thumb: ",  user_thumb);
        Log.e("business_image: ",  buss_full);
        Log.e("business_image_thumb: ",  buss_thumb);
        Log.e("device_token: ",  dev_token);
        Log.e("device_type: ",  "android");
        Log.e("id: ",  sharedPreferenceLeadr.get_UserId_Bfr());*/

        String lat = "0";
        String long_ = "0";
        String loc = "0";
        String country = "0";
        String address = "0";
        String catid = sharedPreferenceLeadr.get_CATEGORY();

        for (int i = 0; i < arr_place.size(); i++) {
            if (lat.equals("0")) {
                lat = arr_place.get(i).getLat();
                long_ = arr_place.get(i).getLong_();
                loc = arr_place.get(i).getName();
                country = arr_place.get(i).getCountryname();
                address = arr_place.get(i).getAddress();
            } else {
                lat = lat + "," + arr_place.get(i).getLat();
                long_ = long_ + "," + arr_place.get(i).getLong_();
                loc = loc + "@" + arr_place.get(i).getName();
                country = country + "@" + arr_place.get(i).getCountryname();
                address = address + "@" + arr_place.get(i).getAddress();
            }
        }

        if(loc.equals("0")){
            sharedPreferenceLeadr.set_LOC_TYPE("0");
        }else{
            sharedPreferenceLeadr.set_LOC_TYPE("1");
            sharedPreferenceLeadr.set_LOC_NAME(loc);
            sharedPreferenceLeadr.set_LOC_NAME_COUNTRY(country);
            sharedPreferenceLeadr.set_LATITUDE(lat);
            sharedPreferenceLeadr.set_LONGITUDE(long_);
            sharedPreferenceLeadr.set_ADDRESS(address);

            if(!address.trim().equalsIgnoreCase("")){
                address = GlobalConstant.ConvertToBase64(address).trim();
            }
            if(!country.trim().equalsIgnoreCase("")){
                country = GlobalConstant.ConvertToBase64(country).trim();
            }
            if(!loc.trim().equalsIgnoreCase("")){
                loc = GlobalConstant.ConvertToBase64(loc).trim();
            }
        }


        String job_title = sharedPreferenceLeadr.get_jobTitle();
        job_title = GlobalConstant.ConvertToBase64(job_title).trim();
        String id = sharedPreferenceLeadr.get_UserId_Bfr();
        String phone_number = sharedPreferenceLeadr.get_phone();
        String user_name = sharedPreferenceLeadr.get_username();
        user_name = GlobalConstant.ConvertToBase64(user_name).trim();
        String bus_nm = sharedPreferenceLeadr.get_bus_name();
        bus_nm = GlobalConstant.ConvertToBase64(bus_nm).trim();
        String bus_info = sharedPreferenceLeadr.get_bus_info();
        bus_info = GlobalConstant.ConvertToBase64(bus_info).trim();
        String device_token = dev_token;

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO_VER2)
                .addBodyParameter("job_title", job_title)
                .addBodyParameter("id", id)
                .addBodyParameter("phone_number", phone_number)
                .addBodyParameter("user_name", user_name)
                .addBodyParameter("business_name", bus_nm)
                .addBodyParameter("business_fb_page", sharedPreferenceLeadr.get_bus_site())
                .addBodyParameter("business_info",bus_info )
                .addBodyParameter("location_name", loc)
                .addBodyParameter("lat", lat)
                .addBodyParameter("lon", long_)
                .addBodyParameter("category", catid)
                .addBodyParameter("user_image", sharedPreferenceLeadr.getProfilePicUrl())
                .addBodyParameter("business_image", sharedPreferenceLeadr.get_bus_img())
                .addBodyParameter("device_token", dev_token)
                .addBodyParameter("device_type", "android")
                .addBodyParameter("country", country)
                .addBodyParameter("address", address)
                .addBodyParameter("user_thum", sharedPreferenceLeadr.get_PROFILE_THUMB())
                .addBodyParameter("busi_thum", sharedPreferenceLeadr.get_BUSS_THUMB())
                .addBodyParameter("email", sharedPreferenceLeadr.get_EMAIL_REG())

                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        sharedPreferenceLeadr.setUserId(sharedPreferenceLeadr.get_UserId_Bfr());
                        if (response.optString("status").equals("1")) {
                            finish();
                        } else {
                            if(response.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(EditRegistration_4_Activity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }else{
                                utils.hideKeyboard(EditRegistration_4_Activity.this);
                                utils.dialog_msg_show(EditRegistration_4_Activity.this, response.optString("message"));
                            }

                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });

    }

    public  void dialog_msg_show(Activity activity, String msg, final EditText edt){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }


    }

}
