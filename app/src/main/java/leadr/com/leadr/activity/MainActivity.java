package leadr.com.leadr.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.appevents.AppEventsLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fragment.BoughtScrollListFragment;
import fragment.BuyFragment;
import fragment.InboxFragment;
import fragment.MoreMenuFragment;

import leadr.com.leadr.R;
import modal.InboxPojo;
import pref.SharedPreferenceLeadr;
import utils.ChatApplication;
import utils.GlobalConstant;
import utils.NetworkingData;

import static fragment.BuyFragment.img_filter;
import static fragment.BuyFragment.vw_line;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.lnr_sell)
    LinearLayout lnr_sell;

    @BindView(R.id.lnr_buy)
    LinearLayout lnr_buy;

    @BindView(R.id.lnr_bought)
    LinearLayout lnr_bought;

    @BindView(R.id.lnr_inbox)
    LinearLayout lnr_inbox;

    @BindView(R.id.lnr_more)
    LinearLayout lnr_more;

    public static View vw_more;

    public static  View vw_inbox;

    public static View vw_sell;

    public static View vw_bought;

    public static View           vw_buy;
    public static RelativeLayout rel_bottom;
//    public static View vw_line;
    public static TextView textInboxCount;

    @BindView(R.id.txt_buy)
    TextView txt_buy;

    @BindView(R.id.txt_bought)
    TextView txt_bought;

    @BindView(R.id.txt_sell)
    TextView txt_sell;

    @BindView(R.id.txt_inbox)
    TextView txt_inbox;

    @BindView(R.id.txt_more)
    TextView txt_more;

    @BindView(R.id.txt_ryt)
    TextView txt_ryt;

    @BindView(R.id.txt_left)
    TextView txt_left;

    @BindView(R.id.img_tool_tip)
    ImageView img_tool_tip;

    @BindView(R.id.img_tool_tip_ryt)
    ImageView img_tool_tip_ryt;

    @BindView(R.id.rel_tool_tip)
    RelativeLayout rel_tool_tip;

    @BindView(R.id.img_cross)
    ImageView img_cross;

    @BindView(R.id.txt_close)
    TextView txt_close;


    public static android.support.v4.app.FragmentManager fragmentManager;
    SharedPreferenceLeadr                                sharedPreferenceLeadr;
    GlobalConstant                                       utils;

    public static Boolean aIsActive = false;

    private boolean isConnected = false;
    public static Fragment fragment_buy;
    public static Fragment fragment_bought;
    public static Fragment fragment_sell;
    public static Fragment fragment_inbox;
    public static Fragment fragment_more;

    Animation anim_left, anim_ryt;

    public static String state_maintain = "1";
    public static String Screen_Type = null;
    public static String Screen_Type2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // bind the view using butterknife
        ButterKnife.bind(this);

        fragmentManager       =  getSupportFragmentManager();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(MainActivity.this);
        utils                 = new GlobalConstant();



        vw_buy         = (View)findViewById(R.id.vw_buy);
        vw_bought      = (View)findViewById(R.id.vw_bought);
        vw_sell        = (View)findViewById(R.id.vw_sell);
        vw_inbox       = (View)findViewById(R.id.vw_inbox);
        vw_more        = (View)findViewById(R.id.vw_more);
        rel_bottom     = (RelativeLayout) findViewById(R.id.rel_bottom);
//        vw_line        = (View) findViewById(R.id.vw_line);
        textInboxCount = (TextView) findViewById(R.id.textInboxCount);

        LocalBroadcastManager.getInstance(this).registerReceiver(mYourBroadcastReceiver,
                new IntentFilter("thisIsForMyActivity"));

        String get_intet = getIntent().getStringExtra("noti");
        if(get_intet==null){
            state_maintain = "1";
            Screen_Type = "buy";
            Screen_Type2 = "buy";
            fragment_buy = new BuyFragment();
            fragmentManager.beginTransaction()
                    .add(R.id.contentContainer, fragment_buy)
                    .commit();

            vw_buy.setVisibility(View.VISIBLE);
            vw_bought.setVisibility(View.GONE);
            vw_sell.setVisibility(View.GONE);
            vw_inbox.setVisibility(View.GONE);
            vw_more.setVisibility(View.GONE);
        }else if(get_intet.equalsIgnoreCase("bought")){
            Screen_Type = "bought";
            Screen_Type2 = "bought";
            state_maintain = "0";
            get_intet = null;
            fragment_buy= null;

            fragment_bought = new BoughtScrollListFragment();
            fragmentManager.beginTransaction().add(R.id.contentContainer, fragment_bought).commit();

            if(fragment_inbox!=null){
                fragmentManager.beginTransaction().hide(fragment_inbox).commit();
            }
            if(fragment_sell!=null){
                fragmentManager.beginTransaction().hide(fragment_sell).commit();
            }
            if(fragment_more!=null){
                fragmentManager.beginTransaction().hide(fragment_more).commit();
            }
            if(fragment_buy!=null){
                fragmentManager.beginTransaction().hide(fragment_buy).commit();
            }
            try{
                BuyFragment buy = new BuyFragment();
                buy.remove_Callback();
            }catch (Exception e){}
            vw_buy.setVisibility(View.GONE);
            vw_bought.setVisibility(View.VISIBLE);
            vw_sell.setVisibility(View.GONE);
            vw_inbox.setVisibility(View.GONE);
            vw_more.setVisibility(View.GONE);

        }else if(get_intet.equalsIgnoreCase("all")){
            Screen_Type = "buy";
            Screen_Type2 = "buy";
            state_maintain = "1";
            get_intet = null;
            fragment_buy = new BuyFragment();
            fragmentManager.beginTransaction().add(R.id.contentContainer, fragment_buy).commit();


            vw_buy.setVisibility(View.VISIBLE);
            vw_bought.setVisibility(View.GONE);
            vw_sell.setVisibility(View.GONE);
            vw_inbox.setVisibility(View.GONE);
            vw_more.setVisibility(View.GONE);
        }else{
            Screen_Type = "inbox";
            Screen_Type2 = "inbox";
            state_maintain = "0";
            get_intet = null;
            fragment_buy= null;
            fragment_inbox = new InboxFragment();
            fragmentManager.beginTransaction().add(R.id.contentContainer, fragment_inbox).commit();

            vw_buy.setVisibility(View.GONE);
            vw_bought.setVisibility(View.GONE);
            vw_sell.setVisibility(View.GONE);
            vw_inbox.setVisibility(View.VISIBLE);
            vw_more.setVisibility(View.GONE);
        }
       /* BuyFragment frag = new BuyFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.contentContainer, frag)
                .commit();

        Fragment fragment_buy = new BuyFragment();
        fragmentManager.beginTransaction().replace(R.id.contentContainer, fragment_buy).commit();
        vw_buy.setVisibility(View.VISIBLE);
        vw_bought.setVisibility(View.GONE);
        vw_sell.setVisibility(View.GONE);
        vw_inbox.setVisibility(View.GONE);
        vw_more.setVisibility(View.GONE);*/

        txt_buy.setTypeface(utils.OpenSans_Regular(MainActivity.this));
        txt_bought.setTypeface(utils.OpenSans_Regular(MainActivity.this));
        txt_sell.setTypeface(utils.OpenSans_Regular(MainActivity.this));
        txt_inbox.setTypeface(utils.OpenSans_Regular(MainActivity.this));
        txt_more.setTypeface(utils.OpenSans_Regular(MainActivity.this));
        txt_close.setTypeface(utils.OpenSans_Regular(MainActivity.this));
        txt_left.setTypeface(utils.OpenSans_Regular(MainActivity.this));
        txt_ryt.setTypeface(utils.OpenSans_Regular(MainActivity.this));


        if(sharedPreferenceLeadr.get_TOOLTIP().equalsIgnoreCase("1")){
            sharedPreferenceLeadr.set_TOOLTIP("");
            rel_tool_tip.setVisibility(View.VISIBLE);
            lnr_sell.setEnabled(false);
            lnr_buy.setEnabled(false);
            lnr_more.setEnabled(false);
            lnr_inbox.setEnabled(false);
            lnr_bought.setEnabled(false);
            if(img_filter!=null) {
                img_filter.setEnabled(false);
            }
            method_AnimateRYT();
        }


    }

    private void method_AnimateRYT() {
        anim_ryt = AnimationUtils.loadAnimation(MainActivity.this,
                R.anim.slide_left);
        img_tool_tip_ryt.startAnimation(anim_ryt);
//        txt_ryt.startAnimation(anim_ryt);

        anim_ryt.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                anim_ryt.cancel();
                if(rel_tool_tip.getVisibility()==View.VISIBLE) {
                    method_animToLeft();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    private void method_animToLeft() {
        if(img_filter!=null) {
            img_filter.setEnabled(false);
        }
        img_tool_tip_ryt.setVisibility(View.GONE);
        txt_ryt.setVisibility(View.GONE);
        img_tool_tip.setVisibility(View.VISIBLE);
        txt_left.setVisibility(View.VISIBLE);
        anim_left = AnimationUtils.loadAnimation(MainActivity.this,
                R.anim.slide_right);

        img_tool_tip.startAnimation(anim_left);
//        txt_left.startAnimation(anim_left);
        anim_left.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                img_tool_tip_ryt.setVisibility(View.VISIBLE);
                img_tool_tip.setVisibility(View.GONE);
                txt_left.setVisibility(View.GONE);
                txt_ryt.setVisibility(View.VISIBLE);
//                rel_tool_tip.setVisibility(View.GONE);

                sharedPreferenceLeadr.set_TOOLTIP("");
                rel_tool_tip.setVisibility(View.VISIBLE);
                lnr_sell.setEnabled(false);
                lnr_buy.setEnabled(false);
                lnr_more.setEnabled(false);
                lnr_inbox.setEnabled(false);
                lnr_bought.setEnabled(false);
                if(img_filter!=null) {
                    img_filter.setEnabled(false);
                }
                method_AnimateRYT();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }



    @OnClick(R.id.rel_tool_tip)
    public void onCancelAnim() {
       /* rel_tool_tip.setVisibility(View.GONE);
        if(anim_left!=null){
            anim_left.cancel();
        }
        if(anim_ryt!=null){
            anim_ryt.cancel();
        }
        lnr_sell.setEnabled(true);
        lnr_buy.setEnabled(true);
        lnr_more.setEnabled(true);
        lnr_inbox.setEnabled(true);
        lnr_bought.setEnabled(true);
        if(img_filter!=null) {
            img_filter.setEnabled(true);
        }*/
    }


    @OnClick(R.id.img_cross)
    public void onCancelAnim2() {
        rel_tool_tip.setVisibility(View.GONE);
        if(anim_left!=null){
//            anim_left.cancel();
        }
        if(anim_ryt!=null){
//            anim_ryt.cancel();
        }
        lnr_sell.setEnabled(true);
        lnr_buy.setEnabled(true);
        lnr_more.setEnabled(true);
        lnr_inbox.setEnabled(true);
        lnr_bought.setEnabled(true);
        if(img_filter!=null) {
            img_filter.setEnabled(true);
        }
    }

    @OnClick(R.id.txt_close)
    public void onCancelAnim3() {
        rel_tool_tip.setVisibility(View.GONE);
        if(anim_left!=null){
//            anim_left.cancel();
        }
        if(anim_ryt!=null){
//            anim_ryt.cancel();
        }
        lnr_sell.setEnabled(true);
        lnr_buy.setEnabled(true);
        lnr_more.setEnabled(true);
        lnr_inbox.setEnabled(true);
        lnr_bought.setEnabled(true);
        if(img_filter!=null) {
            img_filter.setEnabled(true);
        }
    }



    @OnClick(R.id.lnr_sell)
    public void onSell() {
        Screen_Type = "sell";
        Screen_Type2 = "sell";
        state_maintain = "0";
        try{
            BuyFragment buy = new BuyFragment();
            buy.remove_Callback();
        }catch (Exception e){}

        Intent i = new Intent(MainActivity.this, SellActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.lnr_buy)
    public void onBuy() {
        Screen_Type = "buy";
        Screen_Type2 = "buy";
        state_maintain = "1";
        if(vw_buy.getVisibility()==View.GONE) {


            if(fragment_bought!=null){

                fragmentManager.beginTransaction().remove(fragment_bought).commit();
            }
            if(fragment_sell!=null){
                fragmentManager.beginTransaction().remove(fragment_sell).commit();
            }
            if(fragment_inbox!=null){
                fragmentManager.beginTransaction().remove(fragment_inbox).commit();
            }
            if(fragment_more!=null){
                fragmentManager.beginTransaction().remove(fragment_more).commit();
            }
            if(fragment_buy!=null){

                fragmentManager.beginTransaction().show(fragment_buy).commit();
                try{
                    BuyFragment frag = new BuyFragment();
                    frag.method_ResumeFragment();
                }catch (Exception e){}
            }else{
                fragment_buy = new BuyFragment();
                fragmentManager.beginTransaction().add(R.id.contentContainer, fragment_buy).commit();
            }
            vw_buy.setVisibility(View.VISIBLE);
            vw_bought.setVisibility(View.GONE);
            vw_sell.setVisibility(View.GONE);
            vw_inbox.setVisibility(View.GONE);
            vw_more.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.lnr_more)
    public void onMore() {
        Screen_Type = "more";
        Screen_Type2 = "more";
        state_maintain = "0";
        if(vw_more.getVisibility()==View.GONE) {
            fragment_more = new MoreMenuFragment();
            fragmentManager.beginTransaction().add(R.id.contentContainer, fragment_more).commit();

            if (fragment_inbox != null) {
                fragmentManager.beginTransaction().hide(fragment_inbox).commit();
            }
            if (fragment_sell != null) {
                fragmentManager.beginTransaction().hide(fragment_sell).commit();
            }
            if (fragment_bought != null) {
                fragmentManager.beginTransaction().hide(fragment_bought).commit();
            }
            if (fragment_buy != null) {
                fragmentManager.beginTransaction().hide(fragment_buy).commit();
            }
            try {
                BuyFragment buy = new BuyFragment();
                buy.remove_Callback();
            } catch (Exception e) {
            }
            vw_buy.setVisibility(View.GONE);
            vw_bought.setVisibility(View.GONE);
            vw_sell.setVisibility(View.GONE);
            vw_inbox.setVisibility(View.GONE);
            vw_more.setVisibility(View.VISIBLE);
        }

    }

    @OnClick(R.id.lnr_inbox)
    public void onInbox() {
        Screen_Type = "inbox";
        Screen_Type2 = "inbox";
        state_maintain = "0";
        if(vw_inbox.getVisibility()==View.GONE) {

            fragment_inbox = new InboxFragment();
            fragmentManager.beginTransaction().add(R.id.contentContainer, fragment_inbox).commit();

            if(fragment_buy!=null){
                fragmentManager.beginTransaction().hide(fragment_buy).commit();
            }
            if(fragment_more!=null){
                fragmentManager.beginTransaction().hide(fragment_more).commit();
            }
            if(fragment_sell!=null){
                fragmentManager.beginTransaction().hide(fragment_sell).commit();
            }
            if(fragment_bought!=null){
                fragmentManager.beginTransaction().hide(fragment_bought).commit();
            }
            if(fragment_buy!=null){
                fragmentManager.beginTransaction().hide(fragment_buy).commit();
            }

            try{
                BuyFragment buy = new BuyFragment();
                buy.remove_Callback();
            }catch (Exception e){}
            vw_buy.setVisibility(View.GONE);
            vw_bought.setVisibility(View.GONE);
            vw_sell.setVisibility(View.GONE);
            vw_inbox.setVisibility(View.VISIBLE);
            vw_more.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.lnr_bought)
    public void onBought() {
        Screen_Type = "bought";
        Screen_Type2 = "bought";
        state_maintain = "0";
        if(vw_bought.getVisibility()==View.GONE) {
            fragment_bought = new BoughtScrollListFragment();
            fragmentManager.beginTransaction().add(R.id.contentContainer, fragment_bought).commit();

            if(fragment_inbox!=null){
                fragmentManager.beginTransaction().hide(fragment_inbox).commit();
            }
            if(fragment_sell!=null){
                fragmentManager.beginTransaction().hide(fragment_sell).commit();
            }
            if(fragment_more!=null){
                fragmentManager.beginTransaction().hide(fragment_more).commit();
            }
            if(fragment_buy!=null){
                fragmentManager.beginTransaction().hide(fragment_buy).commit();
            }
            try{
                BuyFragment buy = new BuyFragment();
                buy.remove_Callback();
            }catch (Exception e){}
            vw_buy.setVisibility(View.GONE);
            vw_bought.setVisibility(View.VISIBLE);
            vw_sell.setVisibility(View.GONE);
            vw_inbox.setVisibility(View.GONE);
            vw_more.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if(BuyFragment.rel_dialog!=null){
            if(BuyFragment.rel_dialog.getVisibility()==View.VISIBLE){
                utils.hideKeyboard(MainActivity.this);
                BuyFragment.rel_dialog.setVisibility(View.GONE);
                rel_bottom.setVisibility(View.VISIBLE);
                vw_line.setVisibility(View.GONE);
            }else{
                methodBack();
            }
        }else {
            methodBack();
        }
    }

    private void methodBack() {
        Screen_Type = null;
        SimpleDateFormat format = new SimpleDateFormat("hh:mm");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(sharedPreferenceLeadr.get_APP_START());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            date2 = format.parse(CurrentTimeStart());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date1 != null && date2 != null) {
            long mills = date1.getTime() - date2.getTime();
            int mins = (int) (mills / (1000 * 60)) % 60;
            Bundle parameters = new Bundle();
            parameters.putString("TIME", mins + "");

            AppEventsLogger logger = AppEventsLogger.newLogger(MainActivity.this);
            logger.logEvent("EVENT_NAME_APP_CLOSED",
                    parameters);
        }

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);//***Change Here***
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Screen_Type = Screen_Type2;
        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en"))
        {
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());

        }else{
            String languageToLoad  = "it";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }
        api_GetInboxCount();
    }


    String CurrentTimeStart(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    private void api_GetInboxCount(){
        AndroidNetworking.enableLogging();
        Log.e("url_InboxCont: ", NetworkingData.BASE_URL+ NetworkingData.INBOX_TOTAL_COUNT_VER2);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.INBOX_TOTAL_COUNT_VER2)
                .addBodyParameter("userId",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chkadmin: ",result+"");
                      if(result.optString("status").equalsIgnoreCase("1")) {
                          if (result.optString("detail").equalsIgnoreCase("0")) {
                              textInboxCount.setVisibility(View.GONE);
                          } else {
                              textInboxCount.setVisibility(View.VISIBLE);
                              textInboxCount.setText(result.optString("detail"));
                          }
                      }else{
                          if(result.optString("message").contains("Suspended account")){
                              AccountKit.logOut();
                              sharedPreferenceLeadr.clearPreference();
                              String languageToLoad  = "en";
                              Locale locale = new Locale(languageToLoad);
                              Locale.setDefault(locale);
                              Configuration config = new Configuration();
                              config.locale = locale;
                              getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                              Intent i_nxt = new Intent(MainActivity.this, GetStartedActivity.class);
                              i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                              startActivity(i_nxt);
                              finish();
                          }else{
                              textInboxCount.setVisibility(View.GONE);
                          }
                      }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                    }
                });

    }


    @Override
    protected void onPause() {
        super.onPause();
        Screen_Type = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mYourBroadcastReceiver);

    }


    private final BroadcastReceiver mYourBroadcastReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive( Context context, Intent intent)
        {
            api_GetInboxCount();
            // now you can call all your fragments method here
        }
    };
}
