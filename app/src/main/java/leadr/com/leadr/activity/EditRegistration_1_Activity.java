package leadr.com.leadr.activity;

import android.Manifest;
import android.Manifest.permission;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Animatable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.BasePostprocessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.facebook.share.widget.ShareDialog.Mode;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import leadr.com.leadr.R;

import pref.SharedPreferenceLeadr;
import utils.Constants;
import utils.GlobalConstant;
import utils.NetworkingData;
import utils.UploadThumbAws;
import utils.UploadThumbUser;
import utils.Util;


/**
 * Created by Admin on 2/20/2018.
 */

public class EditRegistration_1_Activity extends AppCompatActivity {

    @BindView(R.id.txt_fullname)
    TextView txt_fullname;

    @BindView(R.id.txt_detail)
    TextView txt_detail;

    @BindView(R.id.txt_job)
    TextView txt_job;

    @BindView(R.id.edt_jobtitle)
    EditText edt_jobtitle;

    @BindView(R.id.edt_fullname)
    EditText edt_fullname;

    @BindView(R.id.btn_nxt)
    Button btn_nxt;

    @BindView(R.id.btn_finish)
    Button btn_finish;

    @BindView(R.id.img_user)
    SimpleDraweeView img_user;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.img_back_top)
    ImageView img_back_top;

    @BindView(R.id.rel_edit)
    RelativeLayout rel_edit;

    @BindView(R.id.rel_reg)
    RelativeLayout rel_reg;

    @BindView(R.id.img_user_rotate)
    ImageView img_user_rotate;

    @BindView(R.id.img_user_cancel)
    ImageView img_user_cancel;


    @BindView(R.id.progres_load1)
    ProgressBar progres_load;

    GlobalConstant        utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    String user_full = "";
    String user_thumb = "";

    String str_upload1      = null;
    String str_upload1_back = null;
    String userChoosenTask  = null;
    int rotation_user       = 0;
    File file_image;

    int REQUEST_CAMERA = 3;
    int SELECT_FILE = 2;

    BottomSheetDialog dialog_setting;
    BottomSheetDialog dialog_deny_once;
    BottomSheetDialog dialog_camera;

    TransferObserver observerthumb_user;
    TransferObserver observerfull_user;
    TransferUtility  transferUtility;
    ProgressDialog   progressDialog;
    private Util util;
    private File compressedImage;
    private File compressedImage_thumb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_reg);

        ButterKnife.bind(this);

        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(EditRegistration_1_Activity.this);

        txt_fullname.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        txt_detail.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        txt_job.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        edt_jobtitle.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        edt_fullname.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));

        btn_nxt.setVisibility(View.GONE);
        btn_finish.setVisibility(View.VISIBLE);
        img_back.setVisibility(View.VISIBLE);

        util               = new Util();
        transferUtility    = util.getTransferUtility(this);

        btn_finish.setText(getResources().getString(R.string.save));

        edt_jobtitle.setText(sharedPreferenceLeadr.get_jobTitle());
        edt_fullname.setText(sharedPreferenceLeadr.get_username());
        FirebaseInstanceId.getInstance().getToken();

        try{
           /* Picasso.with(EditRegistration_1_Activity.this).load(sharedPreferenceLeadr.getProfilePicUrl()).placeholder(R.drawable.user)
                    .into(img_user);*/
            if(sharedPreferenceLeadr.get_PROFILE_THUMB().trim().length()>2){
                progres_load.setVisibility(View.VISIBLE);
                img_user_cancel.setVisibility(View.VISIBLE);
                img_user_rotate.setVisibility(View.VISIBLE);
                str_upload1_back = "";

                DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                        ImageRequestBuilder.newBuilderWithSource(Uri.parse(sharedPreferenceLeadr.get_PROFILE_THUMB())).
                                setRotationOptions(RotationOptions.disableRotation())
                                .setPostprocessor(new BasePostprocessor() {
                                    @Override
                                    public void process(Bitmap bitmap) {
                                    }
                                })
                                .build())
                        .setControllerListener(new BaseControllerListener<ImageInfo>() {
                            @Override
                            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                                progres_load.setVisibility(View.GONE);
                            }
                            @Override
                            public void onFailure(String id, Throwable throwable) {
                                super.onFailure(id, throwable);
                                progres_load.setVisibility(View.GONE);
                            }
                            @Override
                            public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                                progres_load.setVisibility(View.GONE);
                            }
                        })
                        .build();
                img_user.setController(controller);
            }
        }catch (Exception e){}

        if(sharedPreferenceLeadr.getProfilePicUrl().trim().length()<2) {
            if (!sharedPreferenceLeadr.get_LANGUAGE().equals("en")) {
                img_user.setImageResource(R.drawable.addimage_he);
            } else {
                img_user.setImageResource(R.drawable.addimage);
            }
        }

        addAstresik();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(sharedPreferenceLeadr.get_PERPRO().equalsIgnoreCase("1")){
            sharedPreferenceLeadr.set_PERPRO("0");
            if (utils.checkReadExternalPermission(EditRegistration_1_Activity.this)) {
                utils.hideKeyboard(EditRegistration_1_Activity.this);
                dialog_sel_image_user();
            }
        }

        String hh = sharedPreferenceLeadr.get_jobTitle();


        edt_fullname.setSelection(edt_fullname.getText().toString().length());

        rel_edit.setVisibility(View.VISIBLE);
        rel_reg.setVisibility(View.GONE);
        img_back.setVisibility(View.GONE);
    }


    private void addAstresik() {
        String simple = getResources().getString(R.string.ful_name);
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();

        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        txt_fullname.setText(builder);
    }


    /*******select user image******/
    @OnClick(R.id.img_user)
    public void OnEditImage() {
            if (utils.checkReadExternalPermission(EditRegistration_1_Activity.this)&&utils.checkWriteExternalPermission(EditRegistration_1_Activity.this)) {
                utils.hideKeyboard(EditRegistration_1_Activity.this);
                dialog_sel_image_user();
            } else {
                ActivityCompat.requestPermissions(
                        EditRegistration_1_Activity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE},
                        1
                );
            }
    }


    @OnClick(R.id.img_back)
    public void OnBack() {
            finish();
    }


    @OnClick(R.id.img_back_top)
    public void OnBack2() {
            finish();
    }


    /***REMOVE USER image****/
    @OnClick(R.id.img_user_cancel)
    public void onUserImgRemove() {
        if(str_upload1!=null) {
            img_user.setImageDrawable(getResources().getDrawable(R.drawable.addimage));
            img_user.setRotation(0);
            str_upload1 = null;
            str_upload1_back = null;
            img_user_cancel.setVisibility(View.GONE);
            img_user_rotate.setVisibility(View.GONE);
        }else if(str_upload1_back!=null){
            img_user.setImageDrawable(getResources().getDrawable(R.drawable.addimage));
            img_user.setRotation(0);
            str_upload1 = null;
            str_upload1_back = null;
            img_user_cancel.setVisibility(View.GONE);
            img_user_rotate.setVisibility(View.GONE);
        }
    }


    /****ROTATE user image****/
    @OnClick(R.id.img_user_rotate)
    public void onUserImgRotate() {
        if(str_upload1!=null) {
            if(rotation_user==0){
                rotation_user = 90;
            }else if(rotation_user==90){
                rotation_user = 180;
            }else if(rotation_user==180){
                rotation_user = 270;
            }else if(rotation_user==270){
                rotation_user = 360;
            }else{
                rotation_user = 0;
            }
            img_user.setRotation(rotation_user);
        }else if(str_upload1_back!=null) {
            if(rotation_user==0){
                rotation_user = 90;
            }else if(rotation_user==90){
                rotation_user = 180;
            }else if(rotation_user==180){
                rotation_user = 270;
            }else if(rotation_user==270){
                rotation_user = 360;
            }else{
                rotation_user = 0;
            }
            img_user.setRotation(rotation_user);
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                str_upload1 = "";
                str_upload1_back = "";
                img_user_cancel.setVisibility(View.VISIBLE);
                img_user_rotate.setVisibility(View.VISIBLE);
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                str_upload1 = "";
                str_upload1_back = "";
                img_user_cancel.setVisibility(View.VISIBLE);
                img_user_rotate.setVisibility(View.VISIBLE);
                onCaptureImageResult(data);
            }
        }
    }


    private void onSelectFromGalleryResult(Intent data) {
        try {


            Uri filePath = data.getData();
//                final InputStream imageStream = getContentResolver().openInputStream(filePath);
//                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//
//                user_image = ConvertBitmapToString(selectedImage);
            File file= new File(getRealPathFromURI(filePath));
            Bitmap thumbnail = BitmapFactory.decodeFile(file.getAbsolutePath());
            Log.e("", "Orignal  bef getHeight gg= " + thumbnail.getHeight());
            Log.e("", "Orignal bef getWidth ggg= " + thumbnail.getWidth());


            Bitmap resize_bitmap = getResizedBitmap(thumbnail, 1200);
            Matrix rotateRight = new Matrix();

            Bitmap resize_bit = null;
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(file.getPath());//Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +  timeStamp+"_picture.jpg");
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Log.e("My camera Orientation", String.valueOf(orientation));
            switch (orientation) {

                case ExifInterface.ORIENTATION_UNDEFINED:
                    Log.e("My UNDEF", "UNDEFI");
                    rotateRight.postRotate(0);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    Log.e("My SAMSUNG", "UNDEFI");
                    rotateRight.postRotate(90);
                    break;


                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotateRight.postRotate(180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotateRight.postRotate(270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    rotateRight.postRotate(0);
                default:
                    break;
            }
            Bitmap image_view_bmap = null;
            try {
                image_view_bmap = Bitmap.createBitmap(resize_bitmap, 0, 0, resize_bitmap.getWidth(), resize_bitmap.getHeight(), rotateRight, true);
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
            }


            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            image_view_bmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);


            Log.e("", "Orignal af getHeight gg= " + image_view_bmap.getHeight());
            Log.e("", "Orignalaf  getWidth ggg= " + image_view_bmap.getWidth());

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            img_user.setImageURI(filePath);
            file_image = new File(getRealPathFromURI(Uri.parse(destination.getPath())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentURI, projection, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    private void onCaptureImageResult(Intent data) {
        Log.e("$ oncaputer", "oncap");
        File file = new File(Environment.getExternalStorageDirectory() + File.separator +
                "image.jpg");
        Bitmap thumbnail = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap resize_bitmap = getResizedBitmap(thumbnail, 1000);
        Matrix rotateRight = new Matrix();

        Bitmap resize_bit = null;
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(file.getPath());//Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +  timeStamp+"_picture.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Log.e("My camera Orientation", String.valueOf(orientation));
        switch (orientation) {

            case ExifInterface.ORIENTATION_UNDEFINED:
                Log.e("My UNDEF", "UNDEFI");
                rotateRight.postRotate(0);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
//                            resize_bitmap = BitmapTools.rotate(resize_bitmap, 90);
                Log.e("My SAMSUNG", "UNDEFI");
                rotateRight.postRotate(90);
                break;


            case ExifInterface.ORIENTATION_ROTATE_180:
//                            resize_bitmap = BitmapTools.rotate(resize_bitmap, 180);
                rotateRight.postRotate(180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
//                            resize_bitmap = BitmapTools.rotate(resize_bitmap, 270);
                rotateRight.postRotate(270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
//                            resize_bitmap = BitmapTools.rotate(resize_bitmap, 0);
                rotateRight.postRotate(0);
            default:
                break;
        }
        Bitmap image_view_bmap = null;
        try {                                       //resize_bitmap
            image_view_bmap = Bitmap.createBitmap(resize_bitmap, 0, 0, resize_bitmap.getWidth(), resize_bitmap.getHeight(),
                    rotateRight, true);
        } catch (OutOfMemoryError ex)
        {
            ex.printStackTrace();
        }


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        image_view_bmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        }  catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e("3 Path", destination.getPath());
        Log.e("3 getAbsolutePath", destination.getAbsolutePath());
        Log.e("3 getName", destination.getName());
        file_image = new File(getRealPathFromURI(Uri.parse(destination.getPath())));

        File imgFile = new File(file_image.getPath());
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        resize_bitmap =getResizedBitmap(myBitmap, 1000);
//        img_user.setImageBitmap(resize_bitmap);

        Uri img_uri = Uri.fromFile(imgFile);
        img_user.setImageURI(img_uri);
    }



    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();
        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                //denied
                utils.hideKeyboard(EditRegistration_1_Activity.this);
                dialog_denyOne();
            }else{
                if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                    //allowed
                    utils.hideKeyboard(EditRegistration_1_Activity.this);
                    dialog_sel_image_user();

                } else{
                    utils.hideKeyboard(EditRegistration_1_Activity.this);
                    dialog_openStoragePer();
                    //set to never ask again
                    //do something here.
                }
            }
        }
    }


    public  void dialog_sel_image_user(){
        if(dialog_camera!=null){
            if(dialog_camera.isShowing()){
                dialog_camera.dismiss();
            }
        }
        dialog_camera = new BottomSheetDialog (EditRegistration_1_Activity.this);
        dialog_camera.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_sel_img, null);
        dialog_camera.setContentView(bottomSheetView);

        dialog_camera.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_camera  = (TextView) dialog_camera.findViewById(R.id.txt_camera);
        TextView txt_gallery = (TextView) dialog_camera.findViewById(R.id.txt_gallery);
        TextView txt_ok      = (TextView) dialog_camera.findViewById(R.id.txt_ok);
        TextView txt_title   = (TextView) dialog_camera.findViewById(R.id.txt_title);

        txt_title.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        txt_gallery.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        txt_gallery.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));


        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_camera.dismiss();
            }
        });

        txt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_camera.dismiss();
                userChoosenTask = getResources().getString(R.string.tak_pic);
                cameraIntent();

            }
        });
        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_camera.dismiss();
                userChoosenTask = getResources().getString(R.string.chuz_lib);
                galleryIntent();
            }
        });
        dialog_camera.show();

    }



    private void cameraIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        file_image = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file_image));
        intent.putExtra("android.intent.extras.CAMERA_FACING", android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT);
        intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
        intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
        startActivityForResult(intent, REQUEST_CAMERA);

    }


    private void galleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_FILE);
    }


    public  void dialog_denyOne(){
        if(dialog_deny_once!=null){
            if(dialog_deny_once.isShowing()){
                dialog_deny_once.dismiss();
            }
        }
        dialog_deny_once = new BottomSheetDialog (EditRegistration_1_Activity.this);
        dialog_deny_once.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog_deny_once.setContentView(bottomSheetView);

        dialog_deny_once.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok     = (TextView) dialog_deny_once.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog_deny_once.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog_deny_once.findViewById(R.id.txt_camera);

        txt_title.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_deny_once.dismiss();
            }
        });

        dialog_deny_once.show();
    }


    public  void dialog_openStoragePer(){
        if(dialog_setting!=null){
            if(dialog_setting.isShowing()){
                dialog_setting.dismiss();
            }
        }
        dialog_setting = new BottomSheetDialog (EditRegistration_1_Activity.this);
        dialog_setting.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog_setting.setContentView(bottomSheetView);

        dialog_setting.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish = (Button) dialog_setting.findViewById(R.id.btn_finish);
        TextView txt_ok = (TextView) dialog_setting.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog_setting.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog_setting.findViewById(R.id.txt_camera);
        TextView txt_press = (TextView) dialog_setting.findViewById(R.id.txt_press);
        TextView txt_store = (TextView) dialog_setting.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        btn_finish.setTypeface(utils.OpenSans_Bold(EditRegistration_1_Activity.this));
        txt_ok.setTypeface(utils.OpenSans_Bold(EditRegistration_1_Activity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(EditRegistration_1_Activity.this));
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_setting.dismiss();
                sharedPreferenceLeadr.set_PERPRO("1");
                GlobalConstant.startInstalledAppDetailsActivity(EditRegistration_1_Activity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_setting.dismiss();
            }
        });

        dialog_setting.show();
    }

    @OnClick(R.id.btn_finish)
    public void OnSave() {
        if (edt_fullname.getText().toString().trim().length() < 1) {
            utils.hideKeyboard(EditRegistration_1_Activity.this);
           dialog_msg_show(EditRegistration_1_Activity.this, getResources().getString(R.string.enter_name),edt_fullname);
        }else{
            if(str_upload1_back==null){
                sharedPreferenceLeadr.set_PROFILE_THUMB("");
                sharedPreferenceLeadr.setProfilePicUrl("");
            }
            if(str_upload1==null){
                if(edt_fullname.getText().toString().trim().length()>0) {
                    method_Reg();
                }else{
                    utils.dialog_msg_show(EditRegistration_1_Activity.this, getResources().getString(R.string.enter_name));
                }
            }else{
                if(progressDialog!=null){
                    if(progressDialog.isShowing()){
                        try{
                            progressDialog.dismiss();
                        }catch (Exception e){}
                    }
                }
                progressDialog = ProgressDialog.show(EditRegistration_1_Activity.this, "", getResources().getString(R.string.updat_wait));
                progressDialog.setCancelable(false);
                String temp1 = sharedPreferenceLeadr.getUserId() +
                        "_" + System.currentTimeMillis()+"1" + ".jpg";
                GlobalConstant.FILE_NAME1 = temp1.replaceAll(" ", "_");
                uploadThumbImgs();
            }
        }
    }

    void method_Reg() {
        if(str_upload1_back==null){
            sharedPreferenceLeadr.setProfilePicUrl("");
            sharedPreferenceLeadr.set_PROFILE_THUMB("");
        }
        String temp1 = sharedPreferenceLeadr.getUserId() +
                "_" + System.currentTimeMillis()+"1" + ".jpg";
        GlobalConstant.FILE_NAME1 = temp1.replaceAll(" ", "_");
        if(str_upload1!=null) {
            if(progressDialog!=null){
                if(progressDialog.isShowing()){
                    try{
                        progressDialog.dismiss();
                    }catch (Exception e){}
                }
            }
            progressDialog = ProgressDialog.show(EditRegistration_1_Activity.this, "", getResources().getString(R.string.updat_wait));
            progressDialog.setCancelable(false);
            uploadThumbImgs();
        }else{
            if(progressDialog!=null){
                if(progressDialog.isShowing()){
                    try{
                        progressDialog.dismiss();
                    }catch (Exception e){}
                }
            }
            progressDialog = ProgressDialog.show(EditRegistration_1_Activity.this, "", getResources().getString(R.string.updat_wait));
            progressDialog.setCancelable(false);
            api_Register();
        }
    }

    public void uploadThumbImgs() {
        if(observerfull_user!=null){
            observerfull_user.cleanTransferListener();
        }

        Bitmap bmp = decodeFile(file_image);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

        if(decoded.getWidth()>1200)
        {
            decoded.compress(Bitmap.CompressFormat.JPEG, 50, out);
        }
        else {
            decoded.compress(Bitmap.CompressFormat.JPEG, 80, out);
        }


        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(out.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            compressedImage = new Compressor(this).compressToFile(destination);
        } catch (IOException e) {
            e.printStackTrace();
            compressedImage = destination;
        }


        observerfull_user = transferUtility.upload(Constants.BUCKET_NAME, System.currentTimeMillis()+"Leadr",
                compressedImage);


        String file_key = observerfull_user.getKey();
        observerfull_user.setTransferListener(new UploadListenerUSerFull(file_key));
    }



    private class UploadListenerUSerFull implements TransferListener {
        String file_key_name = "";
        public UploadListenerUSerFull( String file_key ) {
            this.file_key_name = file_key;
        }

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e("err1:", e.toString());
            if(progressDialog!=null) {
                try{
                    progressDialog.dismiss();
                }catch (Exception ef){}
            }
            observerfull_user.cleanTransferListener();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.e("state1: ",newState.toString());
            if(newState.toString().equals("COMPLETED")) {

                Log.e("url1: ","https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name);
                user_full  = "https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name;
                observerfull_user.cleanTransferListener();
                uploadThumbUserImg();
            }
        }
    }


    public void uploadThumbUserImg() {
        if(observerthumb_user!=null){
            observerthumb_user.cleanTransferListener();
        }

        Bitmap bmp = decodeFile(file_image);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

        if(decoded.getWidth()>500)
        {
            decoded.compress(Bitmap.CompressFormat.JPEG, 40, out);
        }
        else {
            decoded.compress(Bitmap.CompressFormat.JPEG, 50, out);
        }


        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(out.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try{
            compressedImage_thumb  = new Compressor(this)
                    .setMaxWidth(100)
                    .setMaxHeight(100)
                    .setQuality(30)
                    .setCompressFormat(Bitmap.CompressFormat.WEBP)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(destination);
        }catch (Exception w){
            compressedImage_thumb = destination;
        }

        observerthumb_user = transferUtility.upload(Constants.BUCKET_NAME, System.currentTimeMillis()+"Leadr",
                compressedImage_thumb);

        String file_key = observerthumb_user.getKey();
        observerthumb_user.setTransferListener(new UploadListenerThumb(file_key));

    }


    private class UploadListenerThumb implements TransferListener {
        String file_key_name = "";
        public UploadListenerThumb( String file_key ) {
            this.file_key_name = file_key;
        }

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e("err1:", e.toString());
            if(progressDialog!=null) {
                try{
                    progressDialog.dismiss();
                }catch (Exception f){}
            }
            observerthumb_user.cleanTransferListener();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.e("state1: ",newState.toString());
            if(newState.toString().equals("COMPLETED")) {

                Log.e("url1: ","https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name);
                user_thumb = "https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name;
                observerthumb_user.cleanTransferListener();
                api_Register();
            }
        }
    }


    private Bitmap decodeFile(File f) {
        Bitmap b = null;

        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int IMAGE_MAX_SIZE = 1024;
        int scale = 1;
        if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
                    (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        try {
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        Log.d(TAG, "Width :" + b.getWidth() + " Height :" + b.getHeight());

        File destFile = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".png");
        try {
            FileOutputStream out = new FileOutputStream(destFile);
            b.compress(Bitmap.CompressFormat.PNG, 80, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }


    private void api_Register() {
        AndroidNetworking.enableLogging();

        String dev_token = "";
        try {
            dev_token = FirebaseInstanceId.getInstance().getToken();
        } catch (Exception e) {
            dev_token = sharedPreferenceLeadr.get_dev_token();
        }
//        phone = "+917696137554";

      /*  Log.e("url_signup: ", NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO);
        Log.e("job_title: ",  edt_jobtitle.getText().toString());
        Log.e("phone_number: ",  phone);
        Log.e("user_name: ",  edt_fullname.getText().toString());
        Log.e("business_name: ",  edt_buss_name.getText().toString());
        Log.e("business_fb_page: ",  edt_buss_page.getText().toString());
        Log.e("business_info: ",  edt_buss_info.getText().toString());
        Log.e("location_name: ",  loc);
        Log.e("lat: ",  lat);
        Log.e("lon: ",  long_);
        Log.e("category: ",  cate_sel_id);
        Log.e("user_image: ",  user_full);
        Log.e("user_image_thumb: ",  user_thumb);
        Log.e("business_image: ",  buss_full);
        Log.e("business_image_thumb: ",  buss_thumb);
        Log.e("device_token: ",  dev_token);
        Log.e("device_type: ",  "android");
        Log.e("id: ",  sharedPreferenceLeadr.get_UserId_Bfr());*/

        String id = sharedPreferenceLeadr.get_UserId_Bfr();
        String phone_number = sharedPreferenceLeadr.get_phone();
        String device_token = dev_token;
        if(user_full.equals("")){
            if(str_upload1_back!=null) {
                if (sharedPreferenceLeadr.getProfilePicUrl().length() > 2) {
                    user_full = sharedPreferenceLeadr.getProfilePicUrl();
                    user_thumb = sharedPreferenceLeadr.get_PROFILE_THUMB();
                }
            }
        }

        String user_name = GlobalConstant.ConvertToBase64(edt_fullname.getText().toString()).trim();
        String job_title = GlobalConstant.ConvertToBase64(edt_jobtitle.getText().toString()).trim();
        String buss_name = sharedPreferenceLeadr.get_bus_name();
        if(!buss_name.trim().equalsIgnoreCase("")){
            buss_name = GlobalConstant.ConvertToBase64(buss_name).trim();
        }
        String buss_info = sharedPreferenceLeadr.get_bus_info();
        if(!buss_info.trim().equalsIgnoreCase("")){
            buss_info = GlobalConstant.ConvertToBase64(buss_info).trim();
        }

        String loc_name = "";
        String country_name = "";
        String add_ = "";
        if(!sharedPreferenceLeadr.get_LOC_NAME().equalsIgnoreCase("")||!sharedPreferenceLeadr.get_LOC_NAME().equalsIgnoreCase("0")){
            loc_name = sharedPreferenceLeadr.get_LOC_NAME();
            loc_name = GlobalConstant.ConvertToBase64(loc_name).trim();
            country_name = sharedPreferenceLeadr.get_LOC_NAME_COUNTRY();
            country_name = GlobalConstant.ConvertToBase64(country_name).trim();
            add_ = sharedPreferenceLeadr.get_ADDRESS();
            add_ = GlobalConstant.ConvertToBase64(add_).trim();
        }

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO_VER2)
                .addBodyParameter("job_title", job_title)
                .addBodyParameter("id", id)
                .addBodyParameter("phone_number", phone_number)
                .addBodyParameter("user_name", user_name)
                .addBodyParameter("business_name", buss_name)
                .addBodyParameter("business_fb_page", sharedPreferenceLeadr.get_bus_site())
                .addBodyParameter("business_info", buss_info)
                .addBodyParameter("location_name",loc_name )
                .addBodyParameter("lat", sharedPreferenceLeadr.get_LATITUDE())
                .addBodyParameter("lon", sharedPreferenceLeadr.get_LONGITUDE())
                .addBodyParameter("category", sharedPreferenceLeadr.get_CATEGORY())
                .addBodyParameter("user_image", user_full)
                .addBodyParameter("business_image", sharedPreferenceLeadr.get_bus_img())
                .addBodyParameter("device_token", dev_token)
                .addBodyParameter("device_type", "android")
                .addBodyParameter("country",country_name )
                .addBodyParameter("address",add_ )
                .addBodyParameter("user_thum", user_thumb)
                .addBodyParameter("busi_thum", sharedPreferenceLeadr.get_BUSS_THUMB())
                .addBodyParameter("email", sharedPreferenceLeadr.get_EMAIL_REG())

                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        str_upload1=null;
                        str_upload1_back=null;
                        utils.dismissProgressdialog();
                        if(progressDialog!=null){
                            if(progressDialog.isShowing()){
                                try{
                                    progressDialog.dismiss();
                                }catch (Exception e){}
                            }
                        }
                        sharedPreferenceLeadr.setUserId(sharedPreferenceLeadr.get_UserId_Bfr());
                        if (response.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_jobTitle(edt_jobtitle.getText().toString());
                            sharedPreferenceLeadr.setProfilePicUrl(user_full);
                            sharedPreferenceLeadr.set_PROFILE_THUMB(user_thumb);
                            sharedPreferenceLeadr.set_username(edt_fullname.getText().toString());

                            finish();
                        } else {
                            if(response.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(EditRegistration_1_Activity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }else{
                                utils.hideKeyboard(EditRegistration_1_Activity.this);
                                utils.dialog_msg_show(EditRegistration_1_Activity.this, response.optString("message"));
                            }

                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                        if(progressDialog!=null){
                            if(progressDialog.isShowing()){
                                try{
                                    progressDialog.dismiss();
                                }catch (Exception e){}
                            }
                        }
                    }
                });

    }

    public  void dialog_msg_show(Activity activity, String msg, final EditText edt){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }


    }
}
