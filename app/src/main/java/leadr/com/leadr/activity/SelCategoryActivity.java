package leadr.com.leadr.activity;

import android.Manifest;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import adapter.BusinessCat_Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import modal.CategoryPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

/**
 * Created by Abhilasha on 5/12/2017.
 */

public class SelCategoryActivity extends AppCompatActivity {

    @BindView(R.id.lnr_3)
    LinearLayout lnr_3;

    @BindView(R.id.recycl_cat)
    RecyclerView recycl_cat;

    @BindView(R.id.txt_bus_cc)
    TextView txt_bus_cc;

    @BindView(R.id.edt_search)
    EditText edt_search;

    @BindView(R.id.img_bck)
    ImageView img_bck;

    @BindView(R.id.img_save)
    ImageView img_save;

    GlobalConstant utils;

    BusinessCat_Adapter   adapter_cat;
    public static String  cate_sel = "";
    public static String  cate_sel_he = "";
    public static String  cate_sel_id = "";
    SharedPreferenceLeadr sharedPreferenceLeadr;

    ArrayList<CategoryPojo> arr_cat = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selcat_activity);

        // bind the view using butterknife
        ButterKnife.bind(this);

        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(SelCategoryActivity.this);

        lnr_3.setVisibility(View.VISIBLE);
        img_save.setVisibility(View.GONE);

        if(utils.isNetworkAvailable(SelCategoryActivity.this)){
            api_GetCategory();
        }else{
            utils.dialog_msg_show(SelCategoryActivity.this,getResources().getString(R.string.no_internet));
        }

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                adapter_cat.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        edt_search.setTypeface(utils.Hebrew_Regular(SelCategoryActivity.this));
        txt_bus_cc.setTypeface(utils.OpenSans_Regular(SelCategoryActivity.this));
    }



    @OnClick(R.id.img_bck)
    public void onBack() {
       finish();
    }

    private void api_GetCategory(){
        AndroidNetworking.enableLogging();
        utils.showProgressDialog(this,getResources().getString(R.string.load_cat));
        Log.e("url_getCat: ", NetworkingData.BASE_URL+ NetworkingData.GET_CATEGORY);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.GET_CATEGORY)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_getCat: ",result+"");
                        utils.dismissProgressdialog();
                        arr_cat.clear();

                        String cat_id_sel = sharedPreferenceLeadr.get_CATEGORY_SEL_ID();
                        Log.e("ids2: ",cat_id_sel);
                        String[] arr_id = null;
                        if(cat_id_sel.contains(",")) {
                            arr_id = cat_id_sel.trim().split(",");
                        }else{
                            arr_id = new String[1];
                            arr_id[0] = cat_id_sel;
                        }
                        Set<String> silly = new HashSet<String>(Arrays.asList(arr_id));

                        if(result.optString("status").equals("1")){
                            try {
                                JSONArray arr = result.getJSONArray("categories");
                                for(int i=0; i<arr.length(); i++){
                                    JSONObject obj = arr.getJSONObject(i);
                                    CategoryPojo item = new CategoryPojo();
                                    item.setCategory(obj.getString("category_name"));
                                    item.setId(obj.getString("id"));
                                    item.setHebrew(obj.getString("hebrew"));
                                    if (silly.contains(obj.getString("id"))) {
                                        item.setChecked(true);
                                    }else {
                                        item.setChecked(false);
                                    }
                                    arr_cat.add(item);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            LinearLayoutManager lnr_album = new LinearLayoutManager(SelCategoryActivity.this, LinearLayoutManager.VERTICAL, false);
                            recycl_cat.setLayoutManager(lnr_album);
                            adapter_cat = new BusinessCat_Adapter(SelCategoryActivity.this,arr_cat, new BusinessCat_Adapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(int i) {
                                    utils.hideKeyboard(SelCategoryActivity.this);
                                    if (arr_cat.get(i).getChecked()) {
                                        BusinessCat_Adapter.arr_cat_filter.get(i).setChecked(false);
                                        adapter_cat.notifyDataSetChanged();
                                    } else {
                                        BusinessCat_Adapter.arr_cat_filter.get(i).setChecked(true);
                                        adapter_cat.notifyDataSetChanged();
                                    }

                                    cate_sel = "";
                                    cate_sel_he = "";
                                    cate_sel_id = "";
                                    for (int j = 0; j < BusinessCat_Adapter.arr_cat_filter.size(); j++) {
                                        if (BusinessCat_Adapter.arr_cat_filter.get(j).getChecked()) {
                                            if (cate_sel.trim().length() > 1) {
                                                cate_sel = cate_sel + "," + BusinessCat_Adapter.arr_cat_filter.get(j).getCategory();
                                                cate_sel_he = cate_sel_he + "," + BusinessCat_Adapter.arr_cat_filter.get(j).getHebrew();
                                                cate_sel_id = cate_sel_id + "," + BusinessCat_Adapter.arr_cat_filter.get(j).getId();
                                            } else {
                                                cate_sel = BusinessCat_Adapter.arr_cat_filter.get(j).getCategory();
                                                cate_sel_he = BusinessCat_Adapter.arr_cat_filter.get(j).getHebrew();
                                                cate_sel_id = BusinessCat_Adapter.arr_cat_filter.get(j).getId();
                                            }
                                        }
                                    }

                                    sharedPreferenceLeadr.set_CATEGORY_SEL_ID(cate_sel_id);
                                    sharedPreferenceLeadr.set_CATEGORY_SEL_NAME(cate_sel);
                                    sharedPreferenceLeadr.set_CATEGORY_SEL_NAME_HE(cate_sel_he);
//                                    finish();
                                }
                            });
                            recycl_cat.setAdapter(adapter_cat);
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SelCategoryActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }


    @OnClick(R.id.img_save)
    public void SaveCHanges() {
        sharedPreferenceLeadr.set_CATEGORY_SEL_ID(cate_sel_id);
        sharedPreferenceLeadr.set_CATEGORY_SEL_NAME(cate_sel);
        sharedPreferenceLeadr.set_CATEGORY_SEL_NAME_HE(cate_sel_he);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
