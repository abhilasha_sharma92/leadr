package leadr.com.leadr.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import utils.GlobalConstant;
import utils.NetworkingData;


/**
 * Created by Abhilasha on 4/12/2017.
 */

public class TermsPolicyActivity extends AppCompatActivity {


    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.txt_head)
    TextView txt_head;

    @BindView(R.id.txt_head_1)
    TextView txt_head_1;

    @BindView(R.id.txt_head_2)
    TextView txt_head_2;

    @BindView(R.id.web_vw)
    WebView web_vw;

    @BindView(R.id.progress)
    ProgressBar progress;

    GlobalConstant utils;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().getStringExtra("type").equals("policy")){
            setContentView(R.layout.layout_policy);
            ButterKnife.bind(this);
            Log.e("priv: ",NetworkingData.BASE_URL+"privacy_policy");
            web_vw.loadUrl(NetworkingData.BASE_URL+"privacy_policy");
        }else{
            setContentView(R.layout.layout_service);
            ButterKnife.bind(this);
            Log.e("terms: ",NetworkingData.BASE_URL+"terms_of_use");
            web_vw.loadUrl(NetworkingData.BASE_URL+"terms_of_use");
        }


        utils = new GlobalConstant();

        txt_head.setTypeface(utils.OpenSans_Regular(TermsPolicyActivity.this));
        txt_head_1.setTypeface(utils.OpenSans_Regular(TermsPolicyActivity.this));
        txt_head_2.setTypeface(utils.OpenSans_Regular(TermsPolicyActivity.this));

        web_vw.clearCache(true);
        web_vw.clearHistory();
        web_vw.getSettings().setJavaScriptEnabled(true);
        web_vw.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        web_vw.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                progress.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.img_back)
    public void onback() {
       onBackPressed();
    }
}
