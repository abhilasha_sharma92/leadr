package leadr.com.leadr.activity;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.appevents.AppEventsLogger;
import com.squareup.picasso.Picasso;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import leadr.com.leadr.R;
import modal.BuyPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

public class MySellActivity extends AppCompatActivity {

    CardStackView   cardStack;
    ImageView       img_reload;
    RelativeLayout  rel_no_show;
    Button          btn_buy_last;
    TextView        txt_noentry_nxt;
    TextView        txt_noentry;
    TextView        txt_srch;
    ImageView       img_back;
    ProgressBar     progres_load;
    RelativeLayout  lnr_selectr;
    TextView        txt_buy_top;
    SwipeRefreshLayout swipeRefresh;

    SharedPreferenceLeadr sharedPreferenceLeadr;
    GlobalConstant        utils;
    BuyAdapter            adapter;

    ArrayList<BuyPojo> arr_buy = new ArrayList<>();
    String             phone_no = "";

    Handler mHandler = new Handler();
    Handler mHandler_timer = new Handler();
    Handler mHandler_seekbar;

    private MediaPlayer mMediaPlayer = null;
    private boolean     isPlaying    = false;

    private static CountDownTimer countDownTimer;

    int count_start_pause     = 30;
    int count_start_pause_add = 0;
    int resume_pos            = 0;
    String type_to_send ;
    String hours_added ;
    String days_added ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_sell);

        cardStack        = (CardStackView) findViewById(R.id.swipe_deck);
        img_reload       = (ImageView) findViewById(R.id.img_reload);
        img_back         = (ImageView) findViewById(R.id.img_back);
        rel_no_show      = (RelativeLayout) findViewById(R.id.rel_no_show);
        btn_buy_last     = (Button) findViewById(R.id.btn_buy_last);
        txt_srch         = (TextView) findViewById(R.id.txt_srch);
        txt_noentry      = (TextView) findViewById(R.id.txt_noentry);
        txt_noentry_nxt  = (TextView) findViewById(R.id.txt_noentry_nxt);
        txt_buy_top      = (TextView) findViewById(R.id.txt_buy_top);
        progres_load     = (ProgressBar) findViewById(R.id.progres_load);
        lnr_selectr      = (RelativeLayout) findViewById(R.id.lnr_selectr);
        swipeRefresh     = (SwipeRefreshLayout)findViewById(R.id.swipeRefresh);

        img_reload.setVisibility(View.GONE);

        sharedPreferenceLeadr = new SharedPreferenceLeadr(MySellActivity.this);
        utils                 = new GlobalConstant();
        cardStack.setSwipeThreshold(0.1f);

        type_to_send = sharedPreferenceLeadr.get_SELECTOR_SELL();

        btn_buy_last.setTypeface(utils.Dina(MySellActivity.this));
        txt_noentry_nxt.setTypeface(utils.Hebrew_Regular(MySellActivity.this));
        txt_noentry.setTypeface(utils.Hebrew_Regular(MySellActivity.this));
        txt_srch.setTypeface(utils.Hebrew_Regular(MySellActivity.this));
        txt_buy_top.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

        cardStack.setCardEventListener(new CardStackView.CardEventListener() {
            @Override
            public void onCardDragging(float percentX, float percentY) {
            }

            @Override
            public void onCardSwiped(SwipeDirection direction, int topIndex) {
                if (mMediaPlayer!=null){
                    if(mMediaPlayer.isPlaying()){
                        mMediaPlayer.stop();
                        mMediaPlayer.release();
                    }
                    mMediaPlayer = null;
                }
                if(countDownTimer!=null){
                    countDownTimer.cancel();
                    countDownTimer=null;
                }

                count_start_pause=30;
                count_start_pause_add=0;
                resume_pos=0;

                try {
                    arr_buy.remove(topIndex - 1);
                    adapter.notifyDataSetChanged();
//                        Log.e("adap:", adapter.getCount()+"");
                } catch (Exception e) {
                }

                if(topIndex==arr_buy.size()+1){
                    api_MySell();
                }
                if(img_reload!=null) {
                    if (arr_buy != null) {
                        if (arr_buy.size() > 0) {
                            if(topIndex>0){
                                img_reload.setVisibility(View.VISIBLE);
                            }else{
                                img_reload.setVisibility(View.GONE);
                            }
                        } else {
                            img_reload.setVisibility(View.GONE);
                        }
                    } else {
                        img_reload.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onCardReversed() {
            }

            @Override
            public void onCardMovedToOrigin() {
            }

            @Override
            public void onCardClicked(int index) {
            }
        });


        img_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(utils.isNetworkAvailable(MySellActivity.this)){
                    try{
                        mHandler_timer.removeCallbacks(runnable);
                    }catch (Exception e){}

                    api_MySell();
                }else{
                    utils.dialog_msg_show(MySellActivity.this,getResources().getString(R.string.no_internet));
                }
            }
        });


        btn_buy_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i_resell = new Intent(MySellActivity.this, SellActivity.class);
                startActivity(i_resell);
            }
        });


        lnr_selectr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogSelector();
            }
        });


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });


        if(utils.isNetworkAvailable(MySellActivity.this)){
            api_MySell();
        }else{
            utils.dialog_msg_show(MySellActivity.this,getResources().getString(R.string.no_internet));
        }


        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(false);
                api_MySell();
            }
        });
    }


    private void showDialogSelector() {
        final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_selectr_bought, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_nw_lead = (TextView) dialog.findViewById(R.id.txt_nw_lead);
        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);
        TextView txt_archive = (TextView) dialog.findViewById(R.id.txt_archive);

        txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
        txt_nw_lead.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
        txt_refund.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
        txt_archive.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

        txt_nw_lead.setText(getResources().getString(R.string.ld_fr_sale));
        txt_refund.setText(getResources().getString(R.string.rfnds));
        txt_archive.setText(getResources().getString(R.string.sold_cap));

        txt_nw_lead.setAllCaps(true);
        txt_archive.setAllCaps(true);

        if(type_to_send.equalsIgnoreCase("0")){
            txt_nw_lead.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_lang, 0);
            txt_refund.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt_archive.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        else if(type_to_send.equalsIgnoreCase("1")){
            txt_refund.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_lang, 0);
            txt_nw_lead.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt_archive.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        else if(type_to_send.equalsIgnoreCase("2")){
            txt_archive.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_lang, 0);
            txt_nw_lead.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt_refund.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        txt_nw_lead.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                type_to_send = "0";
                dialog.dismiss();
                txt_buy_top.setText(getResources().getString(R.string.ld_fr_sale));
                api_MySell();
            }
        });

        txt_refund.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                type_to_send = "1";
                dialog.dismiss();
                txt_buy_top.setText(getResources().getString(R.string.ld_fr_refnd));
                api_MySell();
            }
        });

        txt_archive.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                type_to_send = "2";
                dialog.dismiss();
                txt_buy_top.setText(getResources().getString(R.string.ld_fr_sell));
                api_MySell();
            }
        });
        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try{
            mHandler_timer.removeCallbacks(runnable);
        }catch (Exception e){}
    }

    @Override
    protected void onResume() {
        super.onResume();

        type_to_send = sharedPreferenceLeadr.get_SELECTOR_SELL();
        if(type_to_send.equalsIgnoreCase("0")){
            txt_buy_top.setText(getResources().getString(R.string.ld_fr_sale));
        }
        else if(type_to_send.equalsIgnoreCase("1")){
            txt_buy_top.setText(getResources().getString(R.string.ld_fr_refnd));
        }
        else if(type_to_send.equalsIgnoreCase("2")){
            txt_buy_top.setText(getResources().getString(R.string.ld_fr_sell));
        }

        if(SellActivity.save_changes!=null) {
            SellActivity.save_changes = null;
            if (utils.isNetworkAvailable(MySellActivity.this)) {
                api_MySell();
            } else {
                utils.dialog_msg_show(MySellActivity.this, getResources().getString(R.string.no_internet));
            }
        }else if(SellActivity.save_changes_sell !=null){
            SellActivity.save_changes_sell = null;
            if (utils.isNetworkAvailable(MySellActivity.this)) {
                api_MySell();
            } else {
                utils.dialog_msg_show(MySellActivity.this, getResources().getString(R.string.no_internet));
            }
        }else{
            if(arr_buy.size()==1){
                try{
                    mHandler_timer.postDelayed(runnable, 4000);
                }catch (Exception e){}
            }else{
                try{
                    mHandler_timer.removeCallbacks(runnable);
                }catch (Exception e){}
            }
        }
    }


    private void api_MySell(){
        if(progres_load!=null){
            progres_load.setVisibility(View.VISIBLE);
        }
        AndroidNetworking.enableLogging();
        Log.e("url_buy: ", NetworkingData.BASE_URL+ NetworkingData.MYSELLS);
        Log.e("userid",sharedPreferenceLeadr.getUserId());
        Log.e("type",type_to_send);

        sharedPreferenceLeadr.set_SELECTOR_SELL(type_to_send);

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.MYSELLS)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("type",type_to_send)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        arr_buy.clear();
                        Log.e("res_mySell: ",response+"");

                        if(progres_load!=null){
                            progres_load.setVisibility(View.GONE);
                        }

                        if (response.optString("status").equals("1")) {
                            rel_no_show.setVisibility(View.GONE);
                            cardStack.setVisibility(View.VISIBLE);
                            try {
                                JSONArray arr = response.getJSONArray("detail");
                                for(int i=0; i<arr.length(); i++){
                                    int views_By = 0;
                                    JSONObject obj = arr.getJSONObject(i);
                                    BuyPojo item = new BuyPojo();
                                    item.setAudio(obj.getString("audio"));
                                    item.setBudget(obj.getString("budget"));
                                    item.setBusiness_fb_page(obj.optString("business_fb_page"));
                                    item.setBusiness_image(obj.optString("business_image"));
                                    item.setBusiness_info(obj.optString("business_info"));
                                    item.setBusiness_name(obj.optString("business_name"));
                                    item.setCategory(obj.optString("category"));
                                    item.setCell_number(obj.optString("cell_number"));
                                    String client_name = obj.optString("client_name");
                                    if(client_name!=null){
                                        if(client_name.equalsIgnoreCase(" ")){
                                            client_name = getResources().getString(R.string.unknw);
                                        }else if(client_name.equalsIgnoreCase("")){
                                            client_name = getResources().getString(R.string.unknw);
                                        }
                                    }else{
                                        client_name = getResources().getString(R.string.unknw);
                                    }
                                    item.setClient_name(client_name);
                                    item.setDescription(obj.optString("description"));
                                    item.setId(obj.optString("id"));
                                    item.setJob_title(obj.optString("job_title"));
                                    item.setIs_delete(obj.optString("is_delete"));
                                    item.setLead_id(obj.optString("lead_id"));
                                    item.setLead_price(obj.optString("lead_price"));
                                    item.setPhone_number(obj.optString("phone_number"));
                                    item.setType(obj.optString("type"));
                                    item.setUser_id(obj.optString("user_id"));
                                    item.setUser_image(obj.optString("user_image"));
                                    item.setUser_name(obj.optString("user_name"));
                                    item.setLat(obj.optString("lat"));
                                    item.setLon(obj.optString("lon"));
                                    item.setLocation_name(obj.optString("location_name"));
                                    item.setCreated_time(obj.optString("created_time"));
                                    item.setTime(obj.optString("time"));
                                    item.setBuy_status(obj.optString("buy_status"));
                                    String count_views = obj.optString("view_by");
                                    item.setUser_thum(obj.optString("user_thum"));
                                    if(count_views.contains(",")){
                                        String[] arr_ount = count_views.split(",");
                                        views_By = arr_ount.length;
                                    }
                                    item.setView_by(String.valueOf(views_By));
                                    item.setCondition(false);
                                    item.setAddress(obj.optString("address"));
                                    item.setCountry(obj.optString("country"));
                                    item.setBuy_userbusinessinfo(obj.optString("buy_userbusinessinfo"));
                                    item.setBuy_userbusinessname(obj.optString("buy_userbusinessname"));
                                    item.setBuy_userimage(obj.optString("buy_userimage"));
                                    item.setBuy_username(obj.optString("buy_username"));
                                    item.setBuy_userid(obj.optString("buy_userid"));
                                    item.setLead_swipe_by(obj.optString("view_count"));
                                    item.setRefund_approved(obj.optString("refund_approved"));
                                    item.setRefund_time(obj.optString("refund_time"));
                                    item.setRefund_req_for(obj.optString("refund_req_for"));
                                    item.setRefund_req(obj.optString("refund_req"));
                                    item.setRefund_user_id(obj.optString("refund_user_id"));
                                    arr_buy.add(item);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            rel_no_show.setVisibility(View.VISIBLE);
                            cardStack.setVisibility(View.GONE);
                            if(type_to_send.equals("0")){
                                txt_noentry_nxt.setText(getResources().getString(R.string.mysell_one));
                                txt_noentry.setText(getResources().getString(R.string.mysell_two));
                                txt_srch.setText(getResources().getString(R.string.mysell_three));
                            }else if(type_to_send.equals("1")){
                                txt_noentry_nxt.setText(getResources().getString(R.string.mysell_ref_one));
                                txt_noentry.setText(getResources().getString(R.string.mysell_ref_two));
                                txt_srch.setText(getResources().getString(R.string.mysell_three));
                            }else{
                                txt_noentry_nxt.setText(getResources().getString(R.string.mysell_sale_one));
                                txt_noentry.setText(getResources().getString(R.string.mysell_sale_two));
                                txt_srch.setText(getResources().getString(R.string.mysell_three));
                            }
                        }

                        if(type_to_send.equals("1")){
                            hours_added = response.optString("hours");
                            days_added = response.optString("days");
                        }
                        adapter = new BuyAdapter(arr_buy, MySellActivity.this);
                        cardStack.setAdapter(adapter);
                        img_reload.setVisibility(View.GONE);

                        if(arr_buy.size()>1){
                            try{
                                mHandler_timer.postDelayed(runnable, 4000);
                            }catch (Exception e){}
                        }else{
                            try{
                                mHandler_timer.removeCallbacks(runnable);
                            }catch (Exception e){}
                        }
                        swipeRefresh.setRefreshing(false);
                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                        if(progres_load!=null){
                            progres_load.setVisibility(View.GONE);
                        }
                        img_reload.setVisibility(View.GONE);
//                        utils.dialog_msg_show(MySellActivity.this,"Internal Server Error");
                    }
                });

    }

    Runnable runnable = new Runnable(){
        @Override
        public void run() {
            if(adapter!=null){
                adapter.notifyDataSetChanged();
            }
            try{
                mHandler_timer.postDelayed(runnable, 40000);
            }catch (Exception e){}
        }
    };


    class BuyAdapter extends BaseAdapter {
        private List<BuyPojo> data;
        private Context context;
        String imge_user = "";

        public BuyAdapter(List<BuyPojo> data, Context context) {
            this.data = data;
            this.context = context;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if(view==null) {
                if (context != null) {
                    LayoutInflater inflater = LayoutInflater.from(context);
                    view = inflater.inflate(R.layout.item_sell, parent, false);
                }
            }
            if(view!=null){
                de.hdodenhof.circleimageview.CircleImageView img_user = (de.hdodenhof.circleimageview.CircleImageView)
                        view.findViewById(R.id.img_user);

                de.hdodenhof.circleimageview.CircleImageView img_eye  = (de.hdodenhof.circleimageview.CircleImageView)
                        view.findViewById(R.id.img_eye);
                TextView txt_seller              = (TextView)view.findViewById(R.id.txt_seller);
                TextView txt_ago                 = (TextView)view.findViewById(R.id.txt_ago);
                TextView txt_client_phn          = (TextView)view.findViewById(R.id.txt_client_phn);
                TextView txt_name_side           = (TextView)view.findViewById(R.id.txt_name_side);
                TextView txt_loc                 = (TextView)view.findViewById(R.id.txt_loc);
                TextView txt_budget              = (TextView)view.findViewById(R.id.txt_budget);
                final TextView txt_play_pause    = (TextView)view.findViewById(R.id.txt_pause_audio);
                final TextView txt_pause_audio   = (TextView)view.findViewById(R.id.txt_play_pause);
                TextView txt_play                = (TextView)view.findViewById(R.id.txt_play);
                TextView txt_des                 = (TextView)view.findViewById(R.id.txt_des);
                TextView txt_read_more           = (TextView)view.findViewById(R.id.txt_read_more);
                TextView txt_noentry             = (TextView)view.findViewById(R.id.txt_noentry);
                TextView txt_lead_price          = (TextView)view.findViewById(R.id.txt_lead_price);
                final Button btn_buy             = (Button)view.findViewById(R.id.btn_buy);
                TextView txt_client_name         = (TextView)view.findViewById(R.id.txt_client_name);
                TextView txt_des_client          = (TextView)view.findViewById(R.id.txt_des_client);
                TextView txt_secs                = (TextView)view.findViewById(R.id.txt_secs);
                final TextView txt_status                = (TextView)view.findViewById(R.id.txt_status);
                ImageView img_shadow             = (ImageView) view.findViewById(R.id.img_shadow);
                View vw_line                     = (View) view.findViewById(R.id.vw_line);
                final RelativeLayout rel_call    = (RelativeLayout) view.findViewById(R.id.rel_call);
                final RelativeLayout rel_top_pro = (RelativeLayout) view.findViewById(R.id.rel_top_pro);
                final SeekBar songProgressBar    = (SeekBar) view.findViewById(R.id.songProgressBar);
                final LinearLayout lnr_11        = (LinearLayout) view.findViewById(R.id.lnr_11);
                final  LinearLayout lnr_22       = (LinearLayout) view.findViewById(R.id.lnr_22);
                final  ImageView img_chat        = (ImageView) view.findViewById(R.id.img_chat);
                final  ImageView img_file_desc   = (ImageView) view.findViewById(R.id.img_file_desc);
                final ProgressBar progress_one      = (ProgressBar) view.findViewById(R.id.progress_one);
                final  ProgressBar progress_two      = (ProgressBar) view.findViewById(R.id.progress_two);
                final  ImageView img_one             = (ImageView) view.findViewById(R.id.img_one);
                final  ImageView img_two             = (ImageView) view.findViewById(R.id.img_two);
                final  ProgressBar progres_load_user = (ProgressBar) view.findViewById(R.id.progres_load_user);

                try {
                    txt_secs.setText(data.get(position).getTime()+" "+getResources().getString(R.string.secs));
                }catch (Exception e){}


                txt_seller.setTypeface(utils.OpenSans_Light(MySellActivity.this));
                txt_name_side.setTypeface(utils.OpenSans_Light(MySellActivity.this));
                txt_client_name.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                txt_des_client.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                txt_budget.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                txt_client_phn.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                txt_loc.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                txt_play.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                txt_des.setTypeface(utils.OpenSans_Light(MySellActivity.this));
                txt_lead_price.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                txt_read_more.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                txt_ago.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                btn_buy.setTypeface(utils.Dina(MySellActivity.this));


                songProgressBar.setOnTouchListener(new View.OnTouchListener(){
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });

                img_chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppEventsLogger logger = AppEventsLogger.newLogger(MySellActivity.this);
                        logger.logEvent("EVENT_NAME_buy_user_ask_about");
                        try{
                            mHandler_timer.removeCallbacks(runnable);
                        }catch (Exception e){}
                        Intent i_chang = new Intent(MySellActivity.this, ChatActivity.class);
                        i_chang.putExtra("image_other", imge_user);
                        i_chang.putExtra("name_other", data.get(position).getUser_name());
                        i_chang.putExtra("buss_other", data.get(position).getBusiness_name());
                        i_chang.putExtra("info_other", data.get(position).getBuy_userbusinessinfo());
                        i_chang.putExtra("id_other", data.get(position).getUser_id());
                        i_chang.putExtra("leadid_other", data.get(position).getLead_id());
                        i_chang.putExtra("lead_user_id", sharedPreferenceLeadr.getUserId());
                        i_chang.putExtra("other_user_id", sharedPreferenceLeadr.getUserId());
                        if(type_to_send.equals("2")){
                            i_chang.putExtra("typemy", "I Sold");
                            i_chang.putExtra("typeother", "I Bought");
                        }else{
                            i_chang.putExtra("typemy", "I Sell");
                            i_chang.putExtra("typeother", "I Buy");
                        }

                        startActivity(i_chang);
                    }
                });
                rel_call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        phone_no = data.get(position).getCell_number();
                        if (utils.checkReadCallLogPermission(MySellActivity.this)) {
                            utils.hideKeyboard(MySellActivity.this);
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:"+phone_no));
                            startActivity(callIntent);
                        } else {
                            ActivityCompat.requestPermissions(
                                    MySellActivity.this,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    1
                            );
                        }
                    }
                });

                if(data.get(position).getBuy_status().equals("0")){
                    img_eye.setVisibility(View.VISIBLE);
                    img_user.setVisibility(View.GONE);
                    txt_des_client.setVisibility(View.GONE);
                    img_chat.setVisibility(View.GONE);
                    txt_client_name.setText(data.get(position).getLead_swipe_by()+" "+ getResources().getString(R.string.buyr_saw));
                    btn_buy.setVisibility(View.VISIBLE);
                    txt_seller.setVisibility(View.GONE);
                    txt_seller.setText(getResources().getString(R.string.pubb));
                    txt_seller.setTextColor(getResources().getColor(R.color.colorBlack));
                    txt_seller.setTypeface(utils.OpenSans_Bold(MySellActivity.this));
                }else{
                    txt_seller.setTypeface(utils.OpenSans_Light(MySellActivity.this));
                    img_eye.setVisibility(View.GONE);
                    img_user.setVisibility(View.VISIBLE);
                    txt_seller.setVisibility(View.VISIBLE);
                    txt_seller.setText(getResources().getString(R.string.sell_buyer));
                    txt_des_client.setVisibility(View.VISIBLE);
                    img_chat.setVisibility(View.GONE);
                    txt_client_name.setTextColor(getResources().getColor(R.color.sellr_gray));
                    txt_client_name.setText(data.get(position).getBuy_username());
                    txt_des_client.setText(data.get(position).getBuy_userbusinessname());

                    if(data.get(position).getBuy_status().equals("2")) {
                        btn_buy.setVisibility(View.GONE);
                    }else{
                        btn_buy.setVisibility(View.VISIBLE);
                    }
                    if(!data.get(position).getUser_thum().equals("")){
                        if(!data.get(position).getUser_thum().equals("0")){
                            if(data.get(position).getUser_thum().trim().length()>2){
                                progres_load_user.setVisibility(View.VISIBLE);
                                Picasso.with(MySellActivity.this)
                                        .load(data.get(position).getUser_thum())
                                        .into(img_user, new com.squareup.picasso.Callback() {
                                            @Override
                                            public void onSuccess() {
                                                progres_load_user.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {
                                                progres_load_user.setVisibility(View.GONE);
                                            }
                                        });
                                imge_user = data.get(position).getUser_thum();
                            }else{
                                progres_load_user.setVisibility(View.GONE);
                                imge_user = "";
                            }
                        }else{
                            progres_load_user.setVisibility(View.GONE);
                            imge_user = "";
                        }
                    }else{
                        progres_load_user.setVisibility(View.GONE);
                        imge_user = "";
                    }

                }
                txt_name_side.setText(data.get(position).getClient_name());
                txt_des.setText(data.get(position).getDescription());
                txt_client_phn.setText(data.get(position).getCell_number());
//                txt_des_client.setText(data.get(position).getBusiness_name());
                if(data.get(position).getLead_price()!=null){
                    if(!data.get(position).getLead_price().equals("0")){
                        if(!data.get(position).getLead_price().equals("")) {
                            if (sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")) {
                                txt_lead_price.setText("$ "+Math.round(Float.valueOf(data.get(position).getLead_price())*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                            } else {
                                txt_lead_price.setText("$ " + data.get(position).getLead_price());
                            }
                        }else{
                            txt_lead_price.setText(getResources().getString(R.string.free));
                        }
                    }else{
                        txt_lead_price.setText(getResources().getString(R.string.free));
                    }
                }else{
                    txt_lead_price.setText(getResources().getString(R.string.free));
                }

                lnr_22.setVisibility(View.GONE);
                if(data.get(position).getAudio()!=null){
                    if(data.get(position).getAudio().trim().equals("")){
                        lnr_11.setVisibility(View.GONE);
                        img_file_desc.setVisibility(View.VISIBLE);
                    }else{
                        lnr_11.setVisibility(View.VISIBLE);
                        img_file_desc.setVisibility(View.INVISIBLE);
                    }
                }else{
                    lnr_11.setVisibility(View.GONE);
                    img_file_desc.setVisibility(View.INVISIBLE);
                }
                int screenSize = getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK;

                String gg = data.get(position).getDescription();
                if(gg.contains("\n")){
                    gg = data.get(position).getDescription().replaceAll("\n","........................................");
                }

                if(screenSize==Configuration.SCREENLAYOUT_SIZE_LARGE){
                    int length_desc = data.get(position).getDescription().length();

                    if(gg.length()>150){
                        txt_read_more.setVisibility(View.VISIBLE);
                        img_shadow.setVisibility(View.VISIBLE);
                        vw_line.setVisibility(View.GONE);
                    }else{
                        txt_read_more.setVisibility(View.INVISIBLE);
                        img_shadow.setVisibility(View.GONE);
                        vw_line.setVisibility(View.VISIBLE);
                    }
                }
                else if(screenSize==Configuration.SCREENLAYOUT_SIZE_NORMAL){
                    if(gg.length()>80){
                        txt_read_more.setVisibility(View.VISIBLE);
                        img_shadow.setVisibility(View.VISIBLE);
                        vw_line.setVisibility(View.GONE);
                    }else{
                        txt_read_more.setVisibility(View.INVISIBLE);
                        img_shadow.setVisibility(View.GONE);
                        vw_line.setVisibility(View.VISIBLE);
                    }
                }
                else if(screenSize==Configuration.SCREENLAYOUT_SIZE_SMALL){
                    if(gg.length()>40){
                        txt_read_more.setVisibility(View.VISIBLE);
                        img_shadow.setVisibility(View.VISIBLE);
                        vw_line.setVisibility(View.GONE);
                    }else{
                        txt_read_more.setVisibility(View.INVISIBLE);
                        img_shadow.setVisibility(View.GONE);
                        vw_line.setVisibility(View.VISIBLE);
                    }
                }

                if(data.get(position).getLat().equals("0")){
                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                } else
                if(data.get(position).getAddress()!=null){
                    if(!data.get(position).getAddress().equals("0")){
                        if(!data.get(position).getAddress().equals("")){
                            txt_loc.setText(data.get(position).getAddress());
                            txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                        }else{
                            if(data.get(position).getLocation_name()!=null){
                                if(!data.get(position).getLocation_name().equals("0")){
                                    if(!data.get(position).getLocation_name().equals("")){
                                        txt_loc.setText(data.get(position).getLocation_name());
                                        txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                    }else{
                                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                    }
                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }

                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }

                        }
                    }else{
                        if(data.get(position).getLocation_name()!=null){
                            if(!data.get(position).getLocation_name().equals("0")){
                                if(!data.get(position).getLocation_name().equals("")){
                                    txt_loc.setText(data.get(position).getLocation_name());
                                    txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }
                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }

                        }else{
                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                        }
                    }
                }else{
                    if(data.get(position).getLocation_name()!=null){
                        if(!data.get(position).getLocation_name().equals("0")){
                            if(!data.get(position).getLocation_name().equals("")){
                                txt_loc.setText(data.get(position).getLocation_name());
                            }else{
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }
                        }else{
                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                        }

                    }else{
                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                    }
                }
                if(data.get(position).getBudget().equals("0")){
                    txt_budget.setTextColor(getResources().getColor(R.color.gray_budget));
                    txt_budget.setText(getResources().getString(R.string.bud_unknw));
                }else {
                    if (sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")) {
                        txt_budget.setText("$ "+Math.round(Float.valueOf(data.get(position).getBudget())*
                                Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                    } else {
                        txt_budget.setText("$" + data.get(position).getBudget() + " " + getResources().getString(R.string.budgt_sel));
                    }
                }

                if(type_to_send.equalsIgnoreCase("1")) {
                    if (data.get(position).getRefund_approved().equalsIgnoreCase("0")) {
                        String duration = method_48HourCheck( data.get(position).getRefund_time(),data.get(position).getRefund_time());
                        if (duration.equalsIgnoreCase("0")) {
                            btn_buy.setVisibility(View.VISIBLE);
                        }
                    }

                    if (data.get(position).getRefund_req().equalsIgnoreCase("1") &&
                            data.get(position).getRefund_req_for().equalsIgnoreCase("1")
                            && data.get(position).getRefund_approved().equalsIgnoreCase("0")) {
                        String duratn = "0";
                        try {
                            duratn = method_48HourCheckUI(data.get(position).getRefund_date(), data.get(position).getRefund_time(),
                                    Integer.valueOf(hours_added), Integer.valueOf(days_added));
                        } catch (Exception e) {
                            duratn = method_48HourCheckUI(data.get(position).getRefund_date(), data.get(position).getRefund_time()
                                    , Integer.valueOf(hours_added), Integer.valueOf(days_added));
                        }
                        if (duratn.equalsIgnoreCase("1")) {
                            btn_buy.setTag("0");
                            btn_buy.setText(getResources().getString(R.string.info));
                        } else {
                              if(data.get(position).getRefund_req().equalsIgnoreCase("1")&&data.get(position).getRefund_req_for().equalsIgnoreCase("2")
                                    &&data.get(position).getRefund_approved().equalsIgnoreCase("2")){
                                  btn_buy.setText(getResources().getString(R.string.info));
                                  btn_buy.setTag("2");
                                  txt_status.setVisibility(View.VISIBLE);
                                  txt_status.setText(getResources().getString(R.string.ref_reqstd));
                                  btn_buy.setVisibility(View.VISIBLE);
                            }else{
                                  btn_buy.setText(getResources().getString(R.string.info));
                                  btn_buy.setTag("2");
                                  txt_status.setVisibility(View.VISIBLE);
                                  txt_status.setText(getResources().getString(R.string.ref_reqstd));
                                  btn_buy.setVisibility(View.VISIBLE);
                              }

                        }
                    }else{
                        if(data.get(position).getRefund_req().equalsIgnoreCase("1")&&data.get(position).getRefund_req_for().equalsIgnoreCase("2")
                                &&data.get(position).getRefund_approved().equalsIgnoreCase("0")){
                            btn_buy.setText(getResources().getString(R.string.info));
                            btn_buy.setTag("2");
                            txt_status.setVisibility(View.VISIBLE);
                            txt_status.setText(getResources().getString(R.string.tckt_opn));
                            btn_buy.setVisibility(View.VISIBLE);
                        }
                        else if(data.get(position).getRefund_req().equalsIgnoreCase("1")&&data.get(position).getRefund_req_for().equalsIgnoreCase("2")
                                &&data.get(position).getRefund_approved().equalsIgnoreCase("2")){
                            btn_buy.setText(getResources().getString(R.string.info));
                            btn_buy.setTag("2");
                            txt_status.setVisibility(View.VISIBLE);
                            txt_status.setText(getResources().getString(R.string.tckt_app));
                            btn_buy.setVisibility(View.VISIBLE);
                        } else if(data.get(position).getRefund_req().equalsIgnoreCase("1")&&data.get(position).getRefund_req_for().equalsIgnoreCase("1")
                                &&data.get(position).getRefund_approved().equalsIgnoreCase("1")){
                            txt_status.setText(getResources().getString(R.string.ref_app));
                            btn_buy.setText(getResources().getString(R.string.info));
                            btn_buy.setTag("2");
                            btn_buy.setVisibility(View.VISIBLE);
                            txt_status.setVisibility(View.VISIBLE);
                        }

                        else if(data.get(position).getRefund_req().equalsIgnoreCase("1")&&data.get(position).getRefund_req_for().equalsIgnoreCase("2")
                                &&data.get(position).getRefund_approved().equalsIgnoreCase("3")){
                            txt_status.setText(getResources().getString(R.string.tckt_dcln));
                            btn_buy.setText(getResources().getString(R.string.info));
                            btn_buy.setTag("2");
                            btn_buy.setVisibility(View.VISIBLE);
                            txt_status.setVisibility(View.VISIBLE);
                        }
                    }
                }
                txt_read_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            mHandler_timer.removeCallbacks(runnable);
                        }catch (Exception e){}
                        dialog_LongLead(MySellActivity.this,data.get(position).getDescription(),
                                getResources().getString(R.string.lead_desc));
                    }
                });

                btn_buy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(type_to_send.equalsIgnoreCase("0")){
                            dialog_AddOption();
                        }else {
                            if(btn_buy.getTag()!=null){
                                if(btn_buy.getTag().equals("2")){
                                    if(txt_status.getText().equals(getResources().getString(R.string.tckt_app))){
                                        dialogTcktApprove();
                                    } else if(txt_status.getText().toString().equalsIgnoreCase(getResources().getString(R.string.tckt_dcln))){
                                        dialogDeclineTickt();
                                    } else if(txt_status.getText().toString().equalsIgnoreCase(getResources().getString(R.string.ref_reqstd))){
                                        dialogAppREFUND();
                                    }else if(txt_status.getText().toString().equalsIgnoreCase(getResources().getString(R.string.ref_app))){
                                        dialogRefundApp();
                                    }else{
                                        dialogTcktOpen();
                                    }

                                }else{
                                    methodRefundStatus();
                                }
                            }else{
                                methodRefundStatus();
                            }
                        }
                    }
                    public  void dialog_AddOption(){
                        final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        View bottomSheetView  = MySellActivity.this.getLayoutInflater().inflate(R.layout.dialog_option_bought, null);
                        dialog.setContentView(bottomSheetView);

                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                BottomSheetDialog d = (BottomSheetDialog) dialog;
                                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                BottomSheetBehavior.from(bottomSheet)
                                        .setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        });

                        TextView txt_resell = (TextView) dialog.findViewById(R.id.txt_resell);
                        TextView txt_refund = (TextView) dialog.findViewById(R.id.txt_refund);
                        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
                        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);

                        txt_resell.setText(getResources().getString(R.string.edt_lead));
                        txt_refund.setText(getResources().getString(R.string.rem_lead));

                        txt_refund.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_ok.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_resell.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

                        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
                        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        txt_resell.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                dialog_Edit();

                            }

                            public  void dialog_Edit(){
                                final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_del_lead_popup, null);
                                dialog.setContentView(bottomSheetView);

                                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        BottomSheetDialog d = (BottomSheetDialog) dialog;
                                        FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                        BottomSheetBehavior.from(bottomSheet)
                                                .setState(BottomSheetBehavior.STATE_EXPANDED);
                                    }
                                });
                                TextView txt_msg            = (TextView) dialog.findViewById(R.id.txt_msg);
                                TextView txt_title          = (TextView) dialog.findViewById(R.id.txt_title);
                                TextView txt_ok             = (TextView) dialog.findViewById(R.id.txt_ok);
                                TextView txt_cancel         = (TextView) dialog.findViewById(R.id.txt_cancel);
                                LinearLayout lnr_mysell_del = (LinearLayout) dialog.findViewById(R.id.lnr_mysell_del);
                                assert txt_msg != null;

                                assert txt_title != null;
                                txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                                txt_ok.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                                txt_cancel.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                                txt_msg.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

                                txt_title.setText(getResources().getString(R.string.edt_lead)+"?");
                                txt_ok.setText(getResources().getString(R.string.yes_edt));
                                txt_msg.setText(getResources().getString(R.string.edt_body));

//                                lnr_mysell_del.setVisibility(View.VISIBLE);

                                RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
                                RelativeLayout rel_vw_cancel       = (RelativeLayout) dialog.findViewById(R.id.rel_vw_cancel);
                                LinearLayout lnr_cancel            = (LinearLayout) dialog.findViewById(R.id.lnr_cancel);

//                                lnr_cancel.setVisibility(View.VISIBLE);
                                rel_vw_cancel.setVisibility(View.GONE);

                                assert rel_vw_save_details != null;
                                rel_vw_cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                                rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        try{
                                            mHandler_timer.removeCallbacks(runnable);
                                        }catch (Exception e){}
                                        Intent i_resell = new Intent(MySellActivity.this, SellActivity.class);
                                        i_resell.putExtra("desc", data.get(position).getDescription());
                                        i_resell.putExtra("audio", data.get(position).getAudio());
                                        i_resell.putExtra("budget", data.get(position).getBudget());
                                        i_resell.putExtra("cat", data.get(position).getCategory());
                                        i_resell.putExtra("phone", data.get(position).getCell_number());
                                        i_resell.putExtra("client_name", data.get(position).getClient_name());
                                        i_resell.putExtra("lead_price", data.get(position).getLead_price());
                                        i_resell.putExtra("lead_id", data.get(position).getLead_id());
                                        i_resell.putExtra("time", data.get(position).getTime());
                                        i_resell.putExtra("resell", "0");
                                        if(!data.get(position).getLocation_name().equals("0")){
                                            if(!data.get(position).getLocation_name().equals("")){
                                                i_resell.putExtra("loc", data.get(position).getLocation_name());
                                                i_resell.putExtra("lat", data.get(position).getLat());
                                                i_resell.putExtra("long_", data.get(position).getLon());
                                                i_resell.putExtra("country", data.get(position).getCountry());
                                                i_resell.putExtra("address", data.get(position).getAddress());
                                            }else{
                                                i_resell.putExtra("loc", "0");
                                            }
                                        }else{
                                            i_resell.putExtra("loc", "0");
                                        }
                                        startActivity(i_resell);
                                    }

                                });

                                dialog.show();
                            }
                        });


                        txt_refund.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                dialog_Delete();
                            }

                            public  void dialog_Delete(){
                                final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_del_lead_popup, null);
                                dialog.setContentView(bottomSheetView);

                                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        BottomSheetDialog d = (BottomSheetDialog) dialog;
                                        FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                        BottomSheetBehavior.from(bottomSheet)
                                                .setState(BottomSheetBehavior.STATE_EXPANDED);
                                    }
                                });
                                TextView txt_msg            = (TextView) dialog.findViewById(R.id.txt_msg);
                                TextView txt_title          = (TextView) dialog.findViewById(R.id.txt_title);
                                TextView txt_ok             = (TextView) dialog.findViewById(R.id.txt_ok);
                                TextView txt_cancel         = (TextView) dialog.findViewById(R.id.txt_cancel);
                                LinearLayout lnr_mysell_del = (LinearLayout) dialog.findViewById(R.id.lnr_mysell_del);
                                assert txt_msg != null;

                                assert txt_title != null;
                                txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                                txt_ok.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                                txt_cancel.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                                txt_msg.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

                                txt_title.setText(getResources().getString(R.string.del_sale));
                                txt_ok.setText(getResources().getString(R.string.yes_del));
                                txt_msg.setText(getResources().getString(R.string.sure_del));

//                                lnr_mysell_del.setVisibility(View.VISIBLE);

                                RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
                                RelativeLayout rel_vw_cancel       = (RelativeLayout) dialog.findViewById(R.id.rel_vw_cancel);
                                LinearLayout lnr_cancel            = (LinearLayout) dialog.findViewById(R.id.lnr_cancel);

//                                lnr_cancel.setVisibility(View.VISIBLE);

                                assert rel_vw_save_details != null;
                                rel_vw_cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                                rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        try{
                                            mHandler_timer.removeCallbacks(runnable);
                                        }catch (Exception e){}
                                        methodDeleteLead();
                                    }
                                    void methodDeleteLead(){
                                        AndroidNetworking.enableLogging();
                                        utils.showProgressDialog(MySellActivity.this, getResources().getString(R.string.load));
                                        Log.e("url_del_lead: ", NetworkingData.BASE_URL + NetworkingData.DEL_LEAD);
                                        Log.e("lead_id: ", data.get(position).getLead_id());

                                        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.DEL_LEAD)
                                                .addBodyParameter("lead_id",data.get(position).getLead_id())
                                                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                                .setTag("msg")
                                                .setPriority(Priority.HIGH).doNotCacheResponse()
                                                .build()
                                                .getAsJSONObject(new JSONObjectRequestListener() {
                                                    @Override
                                                    public void onResponse(JSONObject result) {
                                                        Log.e("res_stripe: ", result + "");
                                                        if(result.optString("status").equals("1")){
                                                            Bundle parameters = new Bundle();
                                                            parameters.putString("DESCRIPTION", data.get(position).getDescription());

                                                            AppEventsLogger logger = AppEventsLogger.newLogger(MySellActivity.this);
                                                            logger.logEvent("EVENT_NAME_mysells_deleted_lead",
                                                                    parameters);

                                                            api_MySell();
                                                        }
                                                        utils.dismissProgressdialog();
                                                    }
                                                    @Override
                                                    public void onError(ANError error) {
                                                        utils.dismissProgressdialog();
                                                    }
                                                });
                                    }
                                });

                                dialog.show();
                            }
                        });

                        dialog.show();
                    }

                    private void dialogDeclineTickt() {
                        final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_sure, null);
                        dialog.setContentView(bottomSheetView);
                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                BottomSheetDialog d = (BottomSheetDialog) dialog;
                                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                BottomSheetBehavior.from(bottomSheet)
                                        .setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        });
                        TextView txt_resell  = (TextView) dialog.findViewById(R.id.txt_resell);
                        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
                        TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);

                        txt_resell.setGravity(Gravity.LEFT);
                        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                            txt_resell.setGravity(Gravity.RIGHT);
                        }
                        txt_title.setText(getResources().getString(R.string.tckt_dcln));
                        txt_resell.setText(getResources().getString(R.string.u_tckt_Dec));
                        txt_refund.setVisibility(View.GONE);

                        txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_resell.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                            txt_resell.setGravity(Gravity.RIGHT);
                        }

                        dialog.show();

                    }
                    private void dialogAppREFUND() {
                        final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_seller_rf_app, null);
                        dialog.setContentView(bottomSheetView);

                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                BottomSheetDialog d = (BottomSheetDialog) dialog;
                                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                BottomSheetBehavior.from(bottomSheet)
                                        .setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        });
                        TextView txt_issu_ref  = (TextView) dialog.findViewById(R.id.txt_issu_ref);
                        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
                        TextView txt_chat  = (TextView) dialog.findViewById(R.id.txt_chat);
                        TextView txt_ask_sellr  = (TextView) dialog.findViewById(R.id.txt_ask_sellr);
                        TextView txt_ask_refnd  = (TextView) dialog.findViewById(R.id.txt_ask_refnd);
                        TextView txt_rfn_buyr  = (TextView) dialog.findViewById(R.id.txt_rfn_buyr);
                        TextView txt_rf_byrt  = (TextView) dialog.findViewById(R.id.txt_rf_byrt);

                        txt_title.setAllCaps(true);
                        txt_issu_ref.setGravity(Gravity.LEFT);
                        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                            txt_issu_ref.setGravity(Gravity.RIGHT);
                            txt_ask_sellr.setGravity(Gravity.RIGHT);
                            txt_rfn_buyr.setGravity(Gravity.RIGHT);
                        }

                        txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_issu_ref.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_chat.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_ask_refnd.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_ask_sellr.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_rfn_buyr.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_rf_byrt.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

                        txt_ask_refnd.setText(data.get(position).getCell_number());
                        txt_ask_refnd.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick( View v ) {
                                dialog.dismiss();
                                try{
                                    mHandler_timer.removeCallbacks(runnable);
                                }catch (Exception e){}
                                phone_no = data.get(position).getCell_number();
                                if (utils.checkReadCallLogPermission(MySellActivity.this)) {
                                    utils.hideKeyboard(MySellActivity.this);
                                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                    callIntent.setData(Uri.parse("tel:"+phone_no));
                                    startActivity(callIntent);
                                } else {
                                    ActivityCompat.requestPermissions(
                                            MySellActivity.this,
                                            new String[]{Manifest.permission.CALL_PHONE},
                                            1
                                    );
                                }
                            }
                        });

                        txt_chat.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick( View v ) {
                               /* try{
                                    mHandler_timer.removeCallbacks(runnable);
                                }catch (Exception e){}
                                dialog.dismiss();
                                AppEventsLogger logger = AppEventsLogger.newLogger(MySellActivity.this);
                                logger.logEvent("EVENT_NAME_buy_user_ask_about");
                                Intent i_chang = new Intent(MySellActivity.this, ChatActivity.class);
                                i_chang.putExtra("image_other", imge_user);
                                i_chang.putExtra("name_other", data.get(position).getUser_name());
                                i_chang.putExtra("buss_other", data.get(position).getBusiness_name());
                                i_chang.putExtra("info_other", data.get(position).getBuy_userbusinessinfo());
                                i_chang.putExtra("id_other", data.get(position).getRefund_user_id());
                                i_chang.putExtra("leadid_other", data.get(position).getLead_id());
                                i_chang.putExtra("lead_user_id", data.get(position).getUser_id());
                                i_chang.putExtra("other_user_id", data.get(position).getRefund_user_id());
                                i_chang.putExtra("audio",data.get(position).getAudio());
                                i_chang.putExtra("audiotime", data.get(position).getTime());
                                i_chang.putExtra("desc", data.get(position).getDescription());
                                if(type_to_send.equals("2")){
                                    i_chang.putExtra("typemy", "I Sold");
                                    i_chang.putExtra("typeother", "I Bought");
                                }else{
                                    i_chang.putExtra("typemy", "I Sell");
                                    i_chang.putExtra("typeother", "I Buy");
                                }

                                startActivity(i_chang);*/
                            }
                        });

                        txt_rf_byrt.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick( View v ) {
                                dialog.dismiss();
                                methodSendRequestAPrrove();
                            }
                        });

                        dialog.show();

                    }
                    void methodRefundStatus(){
                        AndroidNetworking.enableLogging();
                        utils.showProgressDialog(MySellActivity.this, getResources().getString(R.string.load));
                        Log.e("url_del_lead: ", NetworkingData.BASE_URL + NetworkingData.CHECK_SELLER_REFUND_STATUS);
                        Log.e("lead_id: ", data.get(position).getLead_id());
                        Log.e("user_id: ", sharedPreferenceLeadr.getUserId());

                        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.CHECK_SELLER_REFUND_STATUS)
                                .addBodyParameter("lead_id",data.get(position).getLead_id())
                                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                .setTag("msg")
                                .setPriority(Priority.HIGH).doNotCacheResponse()
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject result) {
                                        Log.e("res_stripe: ", result + "");
                                        if(result.optString("status").equals("1")){
                                            methodSendRequestAPrrove();
                                        }else{
//                                            utils.dialog_msg_show(MySellActivity.this,getResources().getString(R.string.u_cannt));
                                            dialogAbortTickt2();
                                        }
                                        utils.dismissProgressdialog();
                                    }
                                    @Override
                                    public void onError(ANError error) {
                                        utils.dismissProgressdialog();
                                    }
                                });


                    }
                    private void dialogTcktApprove() {
                        final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_sure, null);
                        dialog.setContentView(bottomSheetView);
                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                BottomSheetDialog d = (BottomSheetDialog) dialog;
                                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                BottomSheetBehavior.from(bottomSheet)
                                        .setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        });
                        TextView txt_resell  = (TextView) dialog.findViewById(R.id.txt_resell);
                        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
                        TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);

//                        txt_resell.setGravity(Gravity.CENTER);
                        txt_title.setText(getResources().getString(R.string.tckt_app));
                        txt_resell.setText(getResources().getString(R.string.app_mysell_tckt));
                        txt_refund.setVisibility(View.GONE);

                        txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_resell.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

                        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                            txt_resell.setGravity(Gravity.RIGHT);
                        }

                        dialog.show();

                    }
                    private void dialogTcktOpen() {
                        final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_sure, null);
                        dialog.setContentView(bottomSheetView);
                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                BottomSheetDialog d = (BottomSheetDialog) dialog;
                                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                BottomSheetBehavior.from(bottomSheet)
                                        .setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        });
                        TextView txt_resell  = (TextView) dialog.findViewById(R.id.txt_resell);
                        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
                        TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);

//                        txt_resell.setGravity(Gravity.CENTER);
                        txt_title.setText(getResources().getString(R.string.tckt_opn));
                        txt_resell.setText(getResources().getString(R.string.open_mysell_tckt));
                        txt_refund.setVisibility(View.GONE);

                        txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_resell.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

                        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                            txt_resell.setGravity(Gravity.RIGHT);
                        }

                        dialog.show();

                    }
                    private void dialogAbortTickt2() {
                        final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_sure, null);
                        dialog.setContentView(bottomSheetView);
                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                BottomSheetDialog d = (BottomSheetDialog) dialog;
                                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                BottomSheetBehavior.from(bottomSheet)
                                        .setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        });
                        TextView txt_resell  = (TextView) dialog.findViewById(R.id.txt_resell);
                        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
                        TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);

                        txt_resell.setGravity(Gravity.CENTER);
                        txt_title.setText(getResources().getString(R.string.rfnd_abortd));
                        txt_resell.setText(getResources().getString(R.string.buyr_cannt)+" "+days_added+" "
                                +getResources().getString(R.string.dyss));
                        txt_refund.setVisibility(View.GONE);

                        txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_resell.setTypeface(utils.OpenSans_Regular(MySellActivity.this));


                        dialog.show();

                    }
                    void methodSendRequestAPrrove(){
                        AndroidNetworking.enableLogging();
//                        utils.showProgressDialog(MySellActivity.this, getResources().getString(R.string.load));
                        Log.e("url_del_lead: ", NetworkingData.BASE_URL + NetworkingData.APPROVE_BY_SELLER);
                        Log.e("lead_id: ", data.get(position).getLead_id());
                        Log.e("user_id: ", sharedPreferenceLeadr.getUserId());

                        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.APPROVE_BY_SELLER)
                                .addBodyParameter("lead_id",data.get(position).getLead_id())
                                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                .setTag("msg")
                                .setPriority(Priority.HIGH).doNotCacheResponse()
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject result) {
                                        utils.dismissProgressdialog();
                                        Log.e("res_stripe: ", result + "");
                                        if(result.optString("status").equals("1")){
                                            Toast.makeText(MySellActivity.this,"Refund approved",Toast.LENGTH_LONG).show();
                                            try{
                                                mHandler_timer.removeCallbacks(runnable);
                                            }catch (Exception e){}
                                            swipeRight();
                                        }else{
                                            utils.dialog_msg_show(MySellActivity.this,"Time's Up");
                                        }
                                        utils.dismissProgressdialog();
                                    }
                                    @Override
                                    public void onError(ANError error) {
                                        utils.dismissProgressdialog();
                                    }
                                });
                    }
                    private void dialogRefundApp() {
                        final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_sure, null);
                        dialog.setContentView(bottomSheetView);
                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                BottomSheetDialog d = (BottomSheetDialog) dialog;
                                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                BottomSheetBehavior.from(bottomSheet)
                                        .setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        });
                        TextView txt_resell = (TextView) dialog.findViewById(R.id.txt_resell);
                        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
                        TextView txt_refund = (TextView) dialog.findViewById(R.id.txt_refund);

                        txt_title.setText(getResources().getString(R.string.u_opnd));
                        txt_resell.setText(getResources().getString(R.string.our_rfnd_team));
                        txt_refund.setVisibility(View.GONE);

                        txt_title.setText(getResources().getString(R.string.ref_app));
                        txt_resell.setText(getResources().getString(R.string.u_app));

                        txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
                        txt_resell.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

                        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                            txt_resell.setGravity(Gravity.RIGHT);
                        }
                        dialog.show();

                    }
                });

                txt_ago.setText(method_date_toseT(position));



                rel_top_pro.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String gg = data.get(position).getBuy_userid();
                        Intent i_pro = new Intent(MySellActivity.this, ProfileOther_Activity.class);
                        i_pro.putExtra("id",data.get(position).getBuy_userid());
                        startActivity(i_pro);
                    }
                });

                txt_pause_audio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       /* if(txt_play_pause.getTag().equals("0")){
                            txt_play_pause.setTag("1");
                            onPlay(false);
                        }else{
                            txt_play_pause.setTag("0");
                            onPlay(true);
                        }*/
                       lnr_11.performClick();
                    }

                });

                txt_play_pause.setTag("0");

                lnr_11.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            try{
                                mHandler_timer.removeCallbacks(runnable);
                            }catch (Exception e){}
                        lnr_11.setVisibility(View.GONE);
                        lnr_11.setClickable(false);
                        lnr_22.setVisibility(View.VISIBLE);
                        if(txt_play_pause.getTag().equals("0")){
                            progress_one.setVisibility(View.VISIBLE);
                            progress_two.setVisibility(View.VISIBLE);
                            img_one.setVisibility(View.GONE);
                            img_two.setVisibility(View.GONE);
                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    progress_one.setVisibility(View.GONE);
                                    progress_two.setVisibility(View.GONE);
                                    img_one.setVisibility(View.VISIBLE);
                                    img_two.setVisibility(View.VISIBLE);
                                }

                            }, 1000);
                            txt_play_pause.setTag("1");
                            onPlay(false);
                        }else{
                            txt_play_pause.setTag("0");
                            onPlay(true);
                        }
                    }
                    //
                    private void onPlay(boolean isPlaying2){
                        if (!isPlaying2) {
                            //currently MediaPlayer is not playing audio
                            if(mMediaPlayer == null) {
                                txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
                                startPlaying(); //start from beginning
                                isPlaying = !isPlaying;
                            } else {
                                txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
                                resumePlaying(); //resume the currently paused MediaPlayer
                            }
                        } else {
                            //pause the MediaPlayer
                            txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pause_sell, 0, 0, 0);
                            pausePlaying();
                        }
                        /*lnr_11.setVisibility(View.GONE);
//                        lnr_11.setClickable(false);
                        lnr_22.setVisibility(View.VISIBLE);*/
                    }

                    private void startPlaying() {
                        mMediaPlayer = new MediaPlayer();
                        try {
                            mMediaPlayer.setDataSource(data.get(position).getAudio());
                            mMediaPlayer.prepare();

                            count_start_pause_add = 0;
                            count_start_pause = Integer.valueOf(data.get(position).getTime())+2;

                            songProgressBar.setMax(mMediaPlayer.getDuration());
                            songProgressBar.setProgress(0);
                            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer mp) {
                                    mMediaPlayer.start();
                                }
                            });
                        } catch (IOException e) {
                            Log.e("", "prepare() failed");
                        }
                        count_start_pause_add = 0;
                        method_countDown(Integer.valueOf(data.get(position).getTime())+1);
                        method_countDown_Seekbar();

                        mMediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                            @Override
                            public void onSeekComplete(MediaPlayer mp) {

                            }
                        });
                        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                stopPlaying();
                            }
                        });

                        //keep screen on while playing audio
                        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    }

                    void method_countDown_Seekbar(){
                        mHandler_seekbar = new Handler();
                        mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);

                    }

                     Runnable mRunnabl_seekbar = new Runnable() {
                        @Override
                        public void run() {
                            if(mMediaPlayer != null){
//                Log.e("seejbr: ",mCurrentPosition+"");
                                Log.e("seejbr2: ",mMediaPlayer.getCurrentPosition()+"");
                                songProgressBar.setProgress(mMediaPlayer.getCurrentPosition());
                            }
                            mHandler_seekbar.postDelayed(this, 15);
                        }
                    };


                    void method_countDown(int timer){
                        countDownTimer = null;
                        countDownTimer = new CountDownTimer(timer*1000, 1000) {
                            public void onTick(long millisUntilFinished) {
                                long millis = millisUntilFinished;
                                //Convert milliseconds into hour,minute and seconds
                                String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) -
                                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                        TimeUnit.MILLISECONDS.toSeconds(millis) -
                                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

                                txt_play_pause.setText(hms+"");

                                count_start_pause = count_start_pause-1;
                                count_start_pause_add = count_start_pause_add+1;

//                                songProgressBar.setProgress(count_start_pause_add);
                                if(count_start_pause==1){
//                                    songProgressBar.setProgress(count_start_pause_add);
                                    stopPlaying();
                                }
                            }
                            public void onFinish() {
                                countDownTimer = null;//set CountDownTimer to null
                            }
                        }.start();
                    }

                    private void updateSeekBar() {
                        mHandler.postDelayed(mRunnable, 1000);
                    }

                    private void pausePlaying() {
                        mHandler.removeCallbacks(mRunnable);
                        resume_pos = mMediaPlayer.getCurrentPosition();
                        countDownTimer.cancel();
                        mMediaPlayer.pause();

                        if(mHandler_seekbar!=null){
                            mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                        }
                    }

                    private void resumePlaying() {
//        mMediaPlayer.seekTo(resume_pos);
                        mHandler.removeCallbacks(mRunnable);
                        mMediaPlayer.start();
                        updateSeekBar();
                        method_countDown(count_start_pause-1);
                        mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);
//        updateSeekBar();
                    }

                    private void stopPlaying() {
                        lnr_11.setClickable(true);
                        mHandler.removeCallbacks(mRunnable);
                        mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                        mMediaPlayer.stop();
                        mMediaPlayer.reset();
                        mMediaPlayer.release();
                        mMediaPlayer = null;
                        resume_pos = 0;
                        lnr_11.setVisibility(View.VISIBLE);
                        lnr_22.setVisibility(View.GONE);

                        isPlaying = !isPlaying;
//                        songProgressBar.setProgress(songProgressBar.getMax());

                        if (countDownTimer != null) {
                            countDownTimer.cancel();
                        }
                        txt_play_pause.setTag("0");
                        //allow the screen to turn off again once audio is finished playing
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        mHandler_timer.postDelayed(runnable,40000);
                    }

                    //updating mSeekBar
                    private Runnable mRunnable = new Runnable() {
                        @Override
                        public void run() {
                            if(mMediaPlayer != null){

                                int mCurrentPosition = mMediaPlayer.getCurrentPosition();
                                long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);
                                long seconds = TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition)
                                        - TimeUnit.MINUTES.toSeconds(minutes);
                                updateSeekBar();
                            }
                        }
                    };
                });
            }



            return view;
        }

        private String method_date_toseT(int position) {
            String date_return = "";
            try {
                //Dates to compare
                String CurrentDate=  CurrentDate();
                String CurrentTime=  CurrentTime();
                String FinalDate=  getOnlyDate(data.get(position).getCreated_date()+ " "+ data.get(position).getCreated_time());
                String get_time =  data.get(position).getCreated_time();
                String FinalTime=  getDate(data.get(position).getCreated_date() + " "+get_time);

                Date date1;
                Date date1_T;
                Date date2;
                Date date2_T;

                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
                SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

                //Setting dates
                date1 = dates.parse(CurrentDate);
                date1_T = times.parse(FinalTime);
                date2 = dates.parse(FinalDate);
                date2_T = times.parse(CurrentTime);

                //Comparing dates
                long difference = Math.abs(date1.getTime() - date2.getTime());
                long differenceDates = difference / (24 * 60 * 60 * 1000);
                long mills =   date2_T.getTime()-date1_T.getTime();

                String api_year = FinalDate.split("-")[0];
                String curr_year = CurrentDate.split("-")[0];
                String sub_year = "0";

                try{
                    sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
                }catch (Exception e){}

                //Convert long to String
                String dayDifference = Long.toString(differenceDates);

                if(sub_year.equals("1")){
                    date_return = getResources().getString(R.string.few_mnth);
                }
                else if(sub_year.equals("2")){
                    date_return = getResources().getString(R.string.year_ago);
                }
                else if(Integer.valueOf(dayDifference)>0){
                    if(dayDifference.equals("1")){
                        date_return = dayDifference+getResources().getString(R.string.day);
                    }else{
                        date_return = dayDifference+" "+getResources().getString(R.string.days);
                    }
                }else{
                    int hours = (int) (mills / (1000 * 60 * 60));
                    int minutes = (int) (mills / (1000 * 60));
                    int seconds = (int) (mills / (1000));
                    if(hours>0){
                        date_return = hours+" "+getResources().getString(R.string.hr);
                    }else if(minutes>0){
                        date_return = minutes+" "+getResources().getString(R.string.min);
                    }else{
                        date_return = getResources().getString(R.string.few_sec_full);
                    }
                }

            } catch (Exception exception) {
                date_return = getResources().getString(R.string.fewdays);
            }
            return date_return;
        }
    }


    private String getOnlyDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);
        }
        catch (Exception e) {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }

    private String getDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);

        }
        catch (Exception e)
        {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }


    String CurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    String CurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions){
            if(ActivityCompat.shouldShowRequestPermissionRationale(MySellActivity.this, permission)){
                //denied
                utils.hideKeyboard(MySellActivity.this);
                dialog_denyOne();
            }else{
                if(ActivityCompat.checkSelfPermission(MySellActivity.this, permission) == PackageManager.PERMISSION_GRANTED){
                    //allowed
                    try{
                        mHandler_timer.removeCallbacks(runnable);
                    }catch (Exception e){}
                    utils.hideKeyboard(MySellActivity.this);
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+phone_no));
                    startActivity(callIntent);
                } else{
                    utils.hideKeyboard(MySellActivity.this);
                    dialog_openStoragePer();
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //do something here.
                }
            }
        }
    }



    public  void dialog_denyOne(){
        final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);

        txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

        txt_title.setText(getResources().getString(R.string.allow_phone));
        txt_camera.setText(MySellActivity.this.getResources().getString(R.string.text_call));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public  void dialog_openStoragePer(){
        final BottomSheetDialog dialog = new BottomSheetDialog (MySellActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish   = (Button) dialog.findViewById(R.id.btn_finish);
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_press  = (TextView) dialog.findViewById(R.id.txt_press);
        TextView txt_store  = (TextView) dialog.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Bold(MySellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Bold(MySellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(MySellActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(MySellActivity.this));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    mHandler_timer.removeCallbacks(runnable);
                }catch (Exception e){}
                GlobalConstant.startInstalledAppDetailsActivity(MySellActivity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    String method_48HourCheck(final String created_date, final String created_time){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_date+ " "+created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(created_date + " "+get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if(sub_year.equals("1")){
                duration_return = "1";
            }
            else if(sub_year.equals("2")){
                duration_return = "1";
            }
            else if(Integer.valueOf(dayDifference)>0){
                if(dayDifference.equals("1")){
                    duration_return = "0";

                } else if(dayDifference.equals("2")){
                    duration_return = "0";

                }else if(Integer.valueOf(dayDifference)>2){
                    duration_return = "1";

                }else{
                    int hours = (int) (mills / (1000 * 60 * 60));
                    int minutes = (int) (mills / (1000 * 60));
                    int seconds = (int) (mills / (1000));
                    if(hours>0){
                        duration_return = hours+" "+getResources().getString(R.string.hr);
                        if(hours<48){
                            duration_return = "0";
                        }else if(hours == 48){
                            if(minutes>0){
                                duration_return = "1";
                            }else{
                                duration_return = "0";
                            }
                        }else{
                            duration_return = "1";
                        }
                    }else if(minutes>0){
                        duration_return = "0";
                    }else{
                        duration_return = "0";
                    }
                }
            }else{
                duration_return= "0";
            }

        } catch (Exception exception) {
        }
        return  duration_return;
    }

    private String getDate1(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);

        }
        catch (Exception e)
        {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }

    public void swipeRight() {
        if(cardStack!=null){
            List<BuyPojo> spots = extractRemainingTouristSpots();
            if (spots.isEmpty()) {
                return;
            }

            View target = cardStack.getTopView();
            ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                    target, PropertyValuesHolder.ofFloat("rotation", 10f));
            rotation.setDuration(200);
            ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                    target, PropertyValuesHolder.ofFloat("translationX", 0f, 2000f));
            ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                    target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
            translateX.setStartDelay(100);
            translateY.setStartDelay(100);
            translateX.setDuration(500);
            translateY.setDuration(500);
            AnimatorSet set = new AnimatorSet();
            set.playTogether(rotation, translateX, translateY);

            cardStack.swipe(SwipeDirection.Right, set);

            try{
                mHandler_timer.postDelayed(runnable,40000);
            }catch (Exception e){}
        }
    }

    private ArrayList<BuyPojo> extractRemainingTouristSpots() {
        ArrayList<BuyPojo> spots = new ArrayList<>();
        for (int i = cardStack.getTopIndex(); i < adapter.getCount(); i++) {
            spots.add((BuyPojo) adapter.getItem(i));
        }

        return spots;
    }

    String method_48HourCheckUI(final String created_date, final String created_time,Integer hour,Integer days_added){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_date+ " "+created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(created_date + " "+get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            int days_added1 = days_added*24;
            days_added1 = days_added1+hour;
            Log.e("tot: ",days_added1+"");
            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            int hour2s = (int) (mills / (1000 * 60 * 60));
            int minutes2 = (int) (mills / (1000 * 60));
            int seconds2 = (int) (mills / (1000));

            if(days_added1==hour2s){
                if(minutes2>0){
                    hour2s =hour2s+1;
                }else if(seconds2>0){
                    hour2s =hour2s+1;
                }
            }
            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);
            if(days_added1<hour2s){
                duration_return = "1";
            }
            else if(sub_year.equals("1")){
                duration_return = "1";
            }
            else if(sub_year.equals("2")){
                duration_return = "1";
            }
            else if(Integer.valueOf(dayDifference)>0){
                if(dayDifference.equals("1")){
                    duration_return = "0";

                } else if(dayDifference.equals("2")){
                    duration_return = "0";

                }else if(Integer.valueOf(dayDifference)>2){
                    duration_return = "1";

                }else{
                    int hours = (int) (mills / (1000 * 60 * 60));
                    int minutes = (int) (mills / (1000 * 60));
                    int seconds = (int) (mills / (1000));
                    if(hours>0){
                        duration_return = hours+" "+getResources().getString(R.string.hr);
                        if(hours<hour){
                            duration_return = "0";
                        }else if(hours == hour){
                            if(minutes>0){
                                duration_return = "1";
                            }else{
                                duration_return = "0";
                            }
                        }else{
                            duration_return = "1";
                        }
                    }else if(minutes>0){
                        duration_return = "0";
                    }else{
                        duration_return = "0";
                    }
                }
            }else{
                duration_return= "0";
            }

        } catch (Exception exception) {
        }
        return  duration_return;
    }


    public  void dialog_LongLead( Activity activity, String msg, String title){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_long_txt_popup, null);
        dialog.setContentView(bottomSheetView);
        dialog.setCancelable(true);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);

                behavior.setPeekHeight(bottomSheet.getHeight());
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });


        TextView txt_msg   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog.findViewById(R.id.txt_ok);

        assert txt_msg != null;
        txt_msg.setText(msg);

        txt_msg.setMovementMethod(new ScrollingMovementMethod());

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_title.setText(title);
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        txt_msg.setGravity(Gravity.LEFT);
        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                mHandler.postDelayed(runnable,40000);
            }
        });
        try{
            dialog.show();
        }catch (Exception e){
        }
    }
}
