package leadr.com.leadr.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.appevents.AppEventsLogger;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.BankAccount;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import adapter.Inbox_Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import modal.InboxPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

public class PaymentMenuActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.txt_bought)
    TextView txt_bought;

    @BindView(R.id.txt_bought_lead_count)
    TextView txt_bought_lead_count;

    @BindView(R.id.txt_bought_refund_count)
    TextView txt_bought_refund_count;

    @BindView(R.id.txt_lead_price)
    TextView txt_lead_price;

    @BindView(R.id.txt_bought_refund_price)
    TextView txt_bought_refund_price;

    @BindView(R.id.txt_sold)
    TextView txt_sold;

    @BindView(R.id.txt_sold_lead_count)
    TextView txt_sold_lead_count;

    @BindView(R.id.txt_sold_lead_req_count)
    TextView txt_sold_lead_req_count;

    @BindView(R.id.txt_sold_price)
    TextView txt_sold_price;

    @BindView(R.id.txt_sold_refund_count)
    TextView txt_sold_refund_count;

    @BindView(R.id.txt_total_sold)
    TextView txt_total_sold;

    @BindView(R.id.txt_total_sold_price)
    TextView txt_total_sold_price;

    @BindView(R.id.txt_refund_price)
    TextView txt_refund_price;

    @BindView(R.id.txt_subtrct)
    TextView txt_subtrct;

    @BindView(R.id.txt_total)
    TextView txt_total;

    @BindView(R.id.txt_pay_info)
    TextView txt_pay_info;

    @BindView(R.id.txt_card)
    TextView txt_card;

    @BindView(R.id.txt_bought_refund_count_total)
    TextView txt_bought_refund_count_total;

    @BindView(R.id.txt_tot_bought)
    TextView txt_tot_bought;

    @BindView(R.id.txt_tot_price_bought)
    TextView txt_tot_price_bought;

    @BindView(R.id.txt_sold_pub_lead)
    TextView txt_sold_pub_lead;

    @BindView(R.id.img_edt_five)
    ImageView img_edt_five;

    @BindView(R.id.txt_wth_hstry)
    TextView txt_wth_hstry;

    @BindView(R.id.txt_trns)
    TextView txt_trns;

    @BindView(R.id.txt_funds)
    TextView txt_funds;


    @BindView(R.id.txt_hstry_price)
    TextView txt_hstry_price;


    @BindView(R.id.txt_fund_price)
    TextView txt_fund_price;


    @BindView(R.id.txt_trns_price)
    TextView txt_trns_price;

    @BindView(R.id.txt_total_price)
    TextView txt_total_price;

    @BindView(R.id.img_card)
    ImageView img_card;

    @BindView(R.id.btn_withdraw)
    Button btn_withdraw;

    @BindView(R.id.progres_load)
    ProgressBar progres_load;

    @BindView(R.id.txt_loc_4)
    TextView txt_loc_4;

    @BindView(R.id.lnr_sold_qstn)
    LinearLayout lnr_sold_qstn;

    BottomSheetDialog dialog_card;


    GlobalConstant        utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    String sell_status="0",bought_lead_count="0",bought_lead_price="0",bought_refund_count="0",bought_refund_approved_count="0",
            bought_refund_approved_price="0",publish_count="0",sold_lead_count="0",sold_lead_price
           ,sold_refund_approved_count="0",sold_refund_approved_price="0",sold_refund_count="0",total="0",sub_total="0",last_date,
            backend_am,market_fee,credit_exact,funds_to_withd,sold_fee, sold_total,withdrawal_amount,clearance_fee,min_selling_days;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_menu);

        ButterKnife.bind(this);
        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(PaymentMenuActivity.this);

        method_SetTypeface();

        if(utils.isNetworkAvailable(PaymentMenuActivity.this)){
            utils.showProgressDialog(PaymentMenuActivity.this,getResources().getString(R.string.load));
            api_payment();
        }else{
            utils.dialog_msg_show(PaymentMenuActivity.this,getResources().getString(R.string.no_internet));
        }


    }

    void method_SetTypeface() {
        txt_bought.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_bought_lead_count.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_lead_price.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_sold.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_sold_lead_count.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_sold_price.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_sold_refund_count.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_total_sold.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_total_sold_price.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_refund_price.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_total.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_total_price.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_bought_refund_count.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_bought_refund_price.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_pay_info.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_card.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_bought_refund_count_total.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_sold_pub_lead.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_sold_lead_req_count.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_subtrct.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_wth_hstry.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_trns.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_funds.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_hstry_price.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_fund_price.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_trns_price.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_tot_bought.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_tot_price_bought.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        btn_withdraw.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_loc_4.setTypeface(utils.Hebrew_Regular(PaymentMenuActivity.this));
    }


    @OnClick(R.id.img_edt_five)
    void OnEdit5() {
         Intent i_pay = new Intent(PaymentMenuActivity.this, PaymentActivity.class);
        startActivity(i_pay);
//        dialog_AddCard();
    }


    @OnClick(R.id.lnr_sold_qstn)
    void OnSoldDialog() {
        utils.dialog_msg_show_WithTitle_Payment(PaymentMenuActivity.this,getResources().getString(R.string.fee_sold)+" "+market_fee+
                "% "+"\n"+getResources().getString(R.string.feE_for)+" "+clearance_fee+"% "
                ,getResources().getString(R.string.sold_fee_caps));
    }


    @OnClick(R.id.img_card)
    void OnchangCard() {
       /* Intent i_pay = new Intent(PaymentMenuActivity.this, PaymentActivity.class);
        startActivity(i_pay);*/
        dialog_AddCard();
       /* Stripe stripe = new Stripe(this);
        stripe.setDefaultPublishableKey("pk_test_AFG6u0uaLgXBKwMkFmd2QtjP");
        BankAccount bankAccount = new BankAccount("000123456789","US","usd","110000000");
        stripe.createBankAccountToken(bankAccount, new TokenCallback() {
            @Override
            public void onError(Exception error) {
                Log.e("Stripe Error",error.getMessage());
            }

            @Override
            public void onSuccess(com.stripe.android.model.Token token) {
                Log.e("Bank Token", token.getId());
            }
        });*/
    }


    @OnClick(R.id.img_back)
    void onBack() {
        finish();
    }


    @OnClick(R.id.btn_withdraw)
    void onWithdraw() {
        if(Float.valueOf(credit_exact)>0){
            if(sharedPreferenceLeadr.get_CARD().equalsIgnoreCase("N/A")){
                dialog_msg_show();
            }else {
                if(!sell_status.equals("1")){
                    Intent i_withdraw = new Intent(PaymentMenuActivity.this,EnterWithdrawAmountActivity.class);
                    i_withdraw.putExtra("total",credit_exact);
                    i_withdraw.putExtra("admin_min",backend_am);
                    startActivity(i_withdraw);
                }else{
                    String dates = method_7DayCheck(last_date,CurrentDate());

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        c.setTime(sdf.parse(last_date));// all done
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    c.add(Calendar.DATE, Integer.valueOf(min_selling_days));
                    Date expDate = c.getTime();
                    SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
                    String inActiveDate = null;
                    inActiveDate = format1.format(expDate);
                    System.out.println(inActiveDate );
                    Log.e("date: ",expDate+"");
                    Log.e("date2: ",inActiveDate+"");

                    //Withdraw is possible 30 days after your last sell

//            Your can withdraw starting at 14/6/18
//            utils.dialog_msg_show(PaymentMenuActivity.this,getResources().getString(R.string.sell_less));
                    utils.dialog_msg_show(PaymentMenuActivity.this,getResources().getString(R.string.withd)+" "+min_selling_days
                            +" "+getResources().getString(R.string.afterday)
                            +"\n"+getResources().getString(R.string.y_can)+" "+inActiveDate);
                }
//                    api_VaidateCard();

            }
        }else{
            String back_int = backend_am;
            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                back_int = String.valueOf(Math.round(Float.valueOf(back_int)*
                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
            }
            utils.dialog_msg_show(PaymentMenuActivity.this,getResources().getString(R.string.min_fnd)+" "+getResources().getString(R.string.dollar)
                    +back_int+ "\n"+getResources().getString(R.string.plssell));
        }

    }


    void api_payment(){
        AndroidNetworking.enableLogging();
        Log.e("url_payment: ", NetworkingData.BASE_URL+ NetworkingData.VIEW_PAYMENT);
        Log.e("userId: ", sharedPreferenceLeadr.getUserId());
        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.VIEW_PAYMENT)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        Log.e("res_payment: ",response+"");
                        if(response.optString("status").equals("1")){

                            sell_status       = response.optString("selling_status").trim();
                            //1st block under bought
                            bought_lead_count = response.optString("bought_count").trim();
                            txt_bought_lead_count.setText(bought_lead_count+"  "+getResources().getString(R.string.lead));

                            bought_lead_price = response.optString("bought_price").trim();

                            String bought_int = bought_lead_price;
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                bought_int = String.valueOf(Math.round(Float.valueOf(bought_int)*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                            }
                            txt_lead_price.setText("(-"+getResources().getString(R.string.dollar)+bought_int+")");

                            //2nd block under bought
                            bought_refund_count = response.optString("bought_refund_count").trim();
                            txt_bought_refund_count_total.setText(bought_refund_count+"  "+getResources().getString(R.string.ref_req));

                            //3rd block under bought
                            bought_refund_approved_count = response.optString("bought_refund_approved_count").trim();
                            txt_bought_refund_count.setText(bought_refund_approved_count+"  "+getResources().getString(R.string.ref_appp));

                            bought_refund_approved_price = response.optString("bought_refund_approved_price").trim();
                            String bougt_ref = bought_refund_approved_price;
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                bougt_ref = String.valueOf(Math.round(Float.valueOf(bougt_ref)*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                            }
                            txt_bought_refund_price.setText("(+"+getResources().getString(R.string.dollar)+bougt_ref+")");

                            //1st block under sold
                            publish_count = response.optString("publish_count").trim();
                            txt_sold_pub_lead.setText(publish_count+"  "+getResources().getString(R.string.pub_ld));

                            //2nd clock under sold
                            sold_lead_count = response.optString("sold_count").trim();
                            txt_sold_lead_count.setText(sold_lead_count+"  "+getResources().getString(R.string.sold_ld));

                            sold_lead_price = response.optString("sold_price").trim();
                            String sold_lead = sold_lead_price;
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                sold_lead = String.valueOf(Math.round(Float.valueOf(sold_lead)*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                            }
                            txt_sold_price.setText("(+"+getResources().getString(R.string.dollar)+sold_lead+")");

                            //3rd block under sold
                            sold_refund_count = response.optString("sold_refund_count").trim();
                            txt_sold_lead_req_count.setText(sold_refund_count+"  "+getResources().getString(R.string.ref_req_lds));

                            //4th block under sold
                            sold_refund_approved_count = response.optString("sold_refund_approved_count").trim();
                            txt_sold_refund_count.setText(sold_refund_approved_count+"  "+getResources().getString(R.string.ref_req_app));

                            sold_refund_approved_price = response.optString("sold_refund_approved_price").trim();
                            String sold_ref_app = sold_refund_approved_price;
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                sold_ref_app = String.valueOf(Math.round(Float.valueOf(sold_ref_app)*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                            }
                            txt_refund_price.setText("(-"+getResources().getString(R.string.dollar)+sold_ref_app+")");


                            total                    = "0";
                            last_date                = response.optString("last_selling_date").trim();
                            backend_am               = response.optString("min_amount").trim();
                            market_fee               = response.optString("market_fee").trim();
                            withdrawal_amount        = response.optString("withdrawal_amount").trim();
                            clearance_fee            = response.optString("clearance_fee").trim();
                            min_selling_days         = response.optString("min_selling_days").trim();


                            sub_total = String.valueOf(Math.round(Float.valueOf(bought_refund_approved_price) +
                                    Float.valueOf(sold_lead_price)
                                    - Float.valueOf(sold_refund_approved_price)- Float.valueOf(bought_lead_price)));
                            String sub = sub_total;
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                sub = String.valueOf(Math.round(Float.valueOf(sub)*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                            }
                            txt_total_price.setText(""+getResources().getString(R.string.dollar)+sub);

                            /***Total for bought***/
                            float tot_bought = Float.valueOf(bought_refund_approved_price)-Float.valueOf(bought_lead_price);
                            if(tot_bought<0){
                                float neg  = tot_bought*-1;
                                float neg_ = neg;
                                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                    neg_ = Math.round(Float.valueOf(neg_)*
                                            Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS()));
                                }
                                txt_tot_price_bought.setText("(-"+getResources().getString(R.string.dollar)+Math.round(neg_)+")");
                            }else{
                                float neg_ = tot_bought;
                                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                    neg_ = Math.round(Float.valueOf(neg_)*
                                            Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS()));
                                }
                                txt_tot_price_bought.setText("("+getResources().getString(R.string.dollar)+Math.round(neg_)+")");
                            }

                            /***Total for sold***/
                            float tot_sold = Float.valueOf(sold_lead_price) - Float.valueOf(sold_refund_approved_price);
                            float neg_ = tot_sold;
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                neg_ = Math.round(Float.valueOf(neg_)*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS()));
                            }
                            txt_total_sold_price.setText("("+getResources().getString(R.string.dollar)+Math.round(neg_)+")");

                            sold_total = String.valueOf(Math.round(Float.valueOf(Float.valueOf(sold_lead_price)-
                                    Float.valueOf(sold_refund_approved_price))));
                            String sold_ = sold_total;
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                sold_ = String.valueOf(Math.round(Float.valueOf(sold_)*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                            }
                            //previous
                            //txt_hstry_price.setText("(-"+getResources().getString(R.string.dollar)+sold_+")");
                            //current static
                            txt_hstry_price.setText("(-"+getResources().getString(R.string.dollar)+"0"+")");

                            /***Total for Sold Fee under FUNDS****/
                            float percnt = Float.valueOf(market_fee)/100;
                            float per_ = percnt;
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                per_ = Math.round(Float.valueOf(per_)*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS()));
                            }
                            txt_trns_price.setText("(-"+getResources().getString(R.string.dollar)+per_+")");

                           /* if(total.contains("-")){
                                btn_withdraw.setText(getResources().getString(R.string.with_upto)+"200)");
                            }else{
                                btn_withdraw.setText(getResources().getString(R.string.with_upto)+total+")");
                            }*/


                           try{
                               if(Float.valueOf(sub_total)>Float.valueOf(total)){
                                   Float sum = Float.valueOf(sub_total) -Float.valueOf(total);
                                   funds_to_withd= String.valueOf(sum);
                                   float sum_toshow = sum;
                                   if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                       sum_toshow = Math.round(Float.valueOf(sum_toshow)*
                                               Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS()));
                                   }
                                   txt_fund_price.setText("("+getResources().getString(R.string.dollar)+String.valueOf(sum_toshow)+")");
                               }else{
                                   funds_to_withd= String.valueOf("0");
                                   txt_fund_price.setText("("+getResources().getString(R.string.dollar)+"0)");
                               }
                           }catch (Exception e){
                               funds_to_withd= String.valueOf("0");
                               txt_fund_price.setText("("+getResources().getString(R.string.dollar)+"0)");
                           }
                           //currently just set it as zero....
                            txt_fund_price.setText("("+getResources().getString(R.string.dollar)+"0)");

//                            funds_to_withd = "20";
                           if(funds_to_withd.equalsIgnoreCase("0")){
                               credit_exact = String.valueOf("0");
//                               btn_withdraw.setText(getResources().getString(R.string.with_upto)+" 0)");
                           }
                           else if(!funds_to_withd.equalsIgnoreCase("0")){
                               float percnt1 = Float.valueOf(market_fee)/100;
                               float amout_xxx1 = Float.valueOf(funds_to_withd)*percnt1;
                               float credt1 = Float.valueOf(funds_to_withd) - amout_xxx1;

                               credit_exact = String.valueOf(credt1);

//                               btn_withdraw.setText(getResources().getString(R.string.with_upto)+credt1+")");
                           }
                           else if(Float.valueOf(total)>Float.valueOf(backend_am)){
                               float percnt1 = Float.valueOf(market_fee)/100;
                               float amout_xxx1 = Float.valueOf(total)*percnt1;
                               float credt1 = Float.valueOf(total) - amout_xxx1;

                               credit_exact = String.valueOf(credt1);
//                               btn_withdraw.setText(getResources().getString(R.string.with_upto)+credt1+")");
                           }else{
                               float percnt1 = Float.valueOf(market_fee)/100;
                               float amout_xxx1 = Float.valueOf(total)*percnt1;
                               float credt1 = Float.valueOf(total) - amout_xxx1;
                               credit_exact = String.valueOf(credt1);
//                               btn_withdraw.setText(getResources().getString(R.string.with_upto)+credt1+")");
                           }
                            float sold_fee1 = Float.valueOf(market_fee)/100*Float.valueOf(neg_);
                            float nxt =
                                    Float.valueOf(Integer.valueOf(clearance_fee)/100)*Float.valueOf(sold_ref_app);
                            float tot_sold_fee_sum = sold_fee1+nxt;

                            sold_fee = String.valueOf(Math.round(sold_fee1+nxt));
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                txt_trns_price.setText("(-"+getResources().getString(R.string.dollar)+Float.valueOf(tot_sold_fee_sum)*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())+")");
                            }else{
                                txt_trns_price.setText("(-"+getResources().getString(R.string.dollar)+tot_sold_fee_sum+")");
                            }
                            credit_exact = String.valueOf(Math.round(Float.valueOf(sub_total)-
                                    Float.valueOf(sold_fee)-Float.valueOf(total)));
                            if(Float.valueOf(credit_exact)<0){
                                txt_fund_price.setText("("+getResources().getString(R.string.dollar) + 0 + ")");
                            }else {
                                String cred_toshow = credit_exact;
                                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                    cred_toshow = String.valueOf(Math.round(Float.valueOf(cred_toshow)*
                                            Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                                }
                                txt_fund_price.setText("("+getResources().getString(R.string.dollar) + String.valueOf(cred_toshow) + ")");
                            }

                        }else{
                            if(response.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(PaymentMenuActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }else{

                            }
                        }
//                        api_GetCount();
                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });

    }


    String CurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }



    String method_7DayCheck(final String start_date, final String end_date){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  start_date;
            String CurrentTime=  end_date;

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(end_date);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);


            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            return dayDifference;

        } catch (Exception exception) {
        }
        return  duration_return;
    }


    public  void dialog_msg_show( ){
        final BottomSheetDialog dialog = new BottomSheetDialog (PaymentMenuActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog.findViewById(R.id.txt_ok);

        txt_msg.setText(getResources().getString(R.string.plsupdt));
        txt_title.setText(getResources().getString(R.string.missng));

        txt_title.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_msg.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));

        dialog.show();

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                dialog_AddCard();
            }
        });


    }


    public  void dialog_AddCard() {
        final Dialog dialog = new Dialog(PaymentMenuActivity.this, android.R.style.Theme_Holo_NoActionBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_payment);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


        final Button btnok           = (Button) dialog.findViewById(R.id.btnok);
        final EditText edt_cvv       = (EditText) dialog.findViewById(R.id.edt_cvv);
        final EditText edt_month     = (EditText) dialog.findViewById(R.id.edt_month);
        final EditText edt_year      = (EditText) dialog.findViewById(R.id.edt_year);
        final EditText edt_card_no   = (EditText) dialog.findViewById(R.id.edt_card_no);
        final EditText edt_full_name = (EditText) dialog.findViewById(R.id.edt_full_name);
        final ImageView img_cancel   = (ImageView) dialog.findViewById(R.id.img_cancel);
        final TextView txt_top       = (TextView) dialog.findViewById(R.id.txt_top);
        final ImageView img_remove   = (ImageView) dialog.findViewById(R.id.img_remove);
        final RelativeLayout rel_bottm   = (RelativeLayout) dialog.findViewById(R.id.rel_bottm);
        final RelativeLayout rel_botm_real   = (RelativeLayout) dialog.findViewById(R.id.rel_botm_real);

        btnok.setTypeface(utils.Dina(PaymentMenuActivity.this));
        edt_cvv.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        edt_month.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        edt_year.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        edt_card_no.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        edt_full_name.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_top.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));


        KeyboardVisibilityEvent.setEventListener(
                PaymentMenuActivity.this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                        if(isOpen){
                            rel_bottm.setVisibility(View.VISIBLE);
                            rel_botm_real.setVisibility(View.GONE);
                        }else{
                            rel_bottm.setVisibility(View.GONE);
                            rel_botm_real.setVisibility(View.VISIBLE);
                        }
                    }
                });

        /***Disable copying number***/

        edt_card_no.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        edt_card_no.setLongClickable(false);
        edt_card_no.setTextIsSelectable(false);

        String text = "";
        if(sharedPreferenceLeadr.get_CARD().equalsIgnoreCase("N/A")){
            text = "<font color=#227cec>"+getResources().getString(R.string.frst_tym)+
                    "</font> <font color=#071a30>"+getResources().getString(R.string.you_must)+"</font>";
        }else{
            img_remove.setVisibility(View.VISIBLE);
            text =
                    "</font> <font color=#071a30>"+getResources().getString(R.string.ur_card)+"</font>";
        }
        txt_top.setText(Html.fromHtml(text));

        img_remove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                dialog.dismiss();
                utils.hideKeyboard(PaymentMenuActivity.this);
                dialog_RemoveSureCard();
            }


        });

        edt_cvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 3) {
                    utils.hideKeyboard(PaymentMenuActivity.this);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        edt_card_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 16) {
                    edt_month.requestFocus();
                    edt_month.setSelection(edt_month.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        edt_month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               try{
                   String working = s.toString();
                   Log.e("before: ",working+"");
               }catch (Exception e){}
                if (edt_month.getText().toString().matches("^2")|| edt_month.getText().toString().matches("^3")||
                        edt_month.getText().toString().matches("^4")||edt_month.getText().toString().matches("^5")||
                        edt_month.getText().toString().matches("^6")||edt_month.getText().toString().matches("^7")||
                        edt_month.getText().toString().matches("^8")||edt_month.getText().toString().matches("^9")) {
                    // Not allowed
                    edt_month.setText("");
                }else if(s.length() == 2){
                    if (Integer.valueOf(edt_month.getText().toString())>12){
                        edt_month.setText("1");
                        edt_month.setSelection(edt_month.getText().toString().length());
                    }else  if (s.length() == 2) {
                        edt_year.requestFocus();
                        edt_year.setSelection(edt_year.getText().length());
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edt_year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    edt_month.requestFocus();
                    edt_month.setSelection(edt_month.getText().length());
                }else if(s.length() == 4){
                    edt_cvv.requestFocus();
                    edt_cvv.setSelection(edt_cvv.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                method_check_values();
            }

            void method_check_values() {
                utils.hideKeyboard(PaymentMenuActivity.this);
                if (edt_full_name.getText().toString().trim().length() > 0) {
                    if (edt_card_no.getText().toString().trim().length() > 0) {
                        if (edt_month.getText().toString().trim().length() == 2) {
                            if (edt_year.getText().toString().trim().length() == 4) {
                                if (edt_cvv.getText().toString().trim().length() > 0) {
                                    Card card = new Card(edt_card_no.getText().toString(), Integer.valueOf(edt_month.getText().toString()),
                                            Integer.valueOf(edt_year.getText().toString()),
                                            edt_cvv.getText().toString());
                                    if (!card.validateCard()) {
                                        utils.hideKeyboard(PaymentMenuActivity.this);
                                        if (!card.validateNumber()) {
                                            utils.dialog_msg_show(PaymentMenuActivity.this, getResources().getString(R.string.in_cvv));
                                        } else if (!card.validateExpMonth()) {
                                            utils.dialog_msg_show(PaymentMenuActivity.this, getResources().getString(R.string.exp));
                                        } else if (!card.validateExpiryDate()) {
                                            utils.dialog_msg_show(PaymentMenuActivity.this, getResources().getString(R.string.expyr));
                                        } else if (!card.validateCVC()) {
                                            utils.dialog_msg_show(PaymentMenuActivity.this, getResources().getString(R.string.cvv_in));
                                        }
                                    } else {
                                        String substr = edt_card_no.getText().toString().substring(edt_card_no.getText().toString().
                                                length() - 4);
//                                        sharedPreferenceLeadr.set_CARD(substr);
                                        utils.hideKeyboard(PaymentMenuActivity.this);
                                        dialog.dismiss();
//                                        utils.showProgressDialog(ProfileActivity.this, getResources().getString(R.string.pls_wait));
                                        if(progres_load!=null){
                                            progres_load.setVisibility(View.VISIBLE);
                                        }
                                        if(dialog_card!=null){
                                            if(dialog_card.isShowing()){
                                                dialog_card.dismiss();
                                            }
                                        }
                                        dialog_CardFrstTYm(PaymentMenuActivity.this,getResources().getString(R.string.frst_tym_longr));
                                        method_SendToStripe(card,substr);
                                    }
                                } else {
                                    utils.dialog_msg_show(PaymentMenuActivity.this, getResources().getString(R.string.entr_cvv));
                                }
                            } else {
                                utils.dialog_msg_show(PaymentMenuActivity.this, getResources().getString(R.string.entr_yr));
                            }
                        } else {
                            utils.dialog_msg_show(PaymentMenuActivity.this, getResources().getString(R.string.entr_mm));
                        }
                    } else {
                        utils.dialog_msg_show(PaymentMenuActivity.this, getResources().getString(R.string.entr_nmbr));
                    }
                } else {
                    utils.dialog_msg_show(PaymentMenuActivity.this, getResources().getString(R.string.entr_namer));
                }
            }


            void method_SendToStripe(Card card,final String subcard) {
                Stripe stripe = new Stripe(PaymentMenuActivity.this, "pk_live_r9TQXzdQ0OeTo6NiRxHJQxft");
                stripe.createToken(
                        card,
                        new TokenCallback() {
                            public void onSuccess(Token token) {
                                if (utils.isNetworkAvailable(PaymentMenuActivity.this)) {
                                    api_SaveToken(token.getId(),subcard);
                                } else {
                                    if(dialog_card!=null){
                                        if(dialog_card.isShowing()){
                                            dialog_card.dismiss();
                                        }
                                    }
                                    utils.hideKeyboard(PaymentMenuActivity.this);
                                    utils.dialog_msg_show(PaymentMenuActivity.this, getResources().getString(R.string.no_internet));
                                }
                            }

                            public void onError(Exception error) {
                                utils.dismissProgressdialog();
                                utils.hideKeyboard(PaymentMenuActivity.this);
                                utils.dialog_msg_show(PaymentMenuActivity.this, error.toString());
                                if(dialog_card!=null){
                                    if(dialog_card.isShowing()){
                                        dialog_card.dismiss();
                                    }
                                }
                                if(progres_load!=null){
                                    progres_load.setVisibility(View.GONE);
                                }
                            }
                        }
                );
            }


            private void api_SaveToken(String token, final String subcard) {
                AndroidNetworking.enableLogging();
                Log.e("url_stripe: ", NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN);

                AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN)
                        .addBodyParameter("token", token)
                        .addBodyParameter("user_id", sharedPreferenceLeadr.getUserId())
                        .addBodyParameter("last_digits", subcard)
                        .setTag("msg")
                        .setPriority(Priority.HIGH).doNotCacheResponse()
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject result) {
                                Log.e("res_stripe: ", result + "");
                                //{"status":"1","customer":{"id":"cus_C4xJzoWD2YfwOO","object":"customer","account_balance":0,"created":1515128721,"currency":null,"default_source":"card_1BgnOOI0GVRrJIpDo8dLjinv","delinquent":false,"description":"Customer for LeadR app","discount":null,"email":null,"livemode":false,"metadata":{},"shipping":null,"sources":{"object":"list","data":[{"id":"card_1BgnOOI0GVRrJIpDo8dLjinv","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"Visa","country":"US","customer":"cus_C4xJzoWD2YfwOO","cvc_check":"pass","dynamic_last4":null,"exp_month":11,"exp_year":2022,"fingerprint":"kXn425uHw3F6DtGT","funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null}],"has_more":false,"total_count":1,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/subscriptions"}}}
                                if(progres_load!=null){
                                    progres_load.setVisibility(View.GONE);
                                }
                                if(dialog_card!=null){
                                    if(dialog_card.isShowing()){
                                        dialog_card.dismiss();
                                    }
                                }
                                if (result.optString("status").equals("1")) {
                                    AppEventsLogger logger = AppEventsLogger.newLogger(PaymentMenuActivity.this);
                                    logger.logEvent("EVENT_NAME_ADDED_PAYMENT_INFO");
                                    sharedPreferenceLeadr.set_CARD(subcard);
                                    txt_card.setText(getResources().getString(R.string.card_savd));
                                    utils.dialog_msg_show_WithTitle(PaymentMenuActivity.this,
                                            getResources().getString(R.string.card_sv),getResources().getString(R.string.succ));
                                } else {
                                    utils.dismissProgressdialog();

                                    dialog_errorCard();
                                }
                                utils.dismissProgressdialog();
                                if(dialog!=null) {
                                    if(dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                }
                            }

                            @Override
                            public void onError(ANError error) {
                                Log.i("", "---> On error  ");
                                utils.dismissProgressdialog();
                                if(dialog_card!=null){
                                    if(dialog_card.isShowing()){
                                        dialog_card.dismiss();
                                    }
                                }
                                if(progres_load!=null){
                                    progres_load.setVisibility(View.GONE);
                                }
                            }
                        });
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                Unregistrar unregistrar = KeyboardVisibilityEvent.registerEventListener(
                        PaymentMenuActivity.this,
                        new KeyboardVisibilityEventListener() {
                            @Override
                            public void onVisibilityChanged(boolean isOpen) {
                                // some code depending on keyboard visiblity status
                            }
                        });

// call this method when you don't need the event listener anymore
                unregistrar.unregister();
            }
        });

        dialog.show();
    }


    public  void dialog_errorCard(){
        final BottomSheetDialog dialog = new BottomSheetDialog (PaymentMenuActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_card_error, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        TextView txt_update = (TextView) dialog.findViewById(R.id.txt_update);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView btn_update = (Button) dialog.findViewById(R.id.btn_update);

        txt_title.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_update.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        btn_update.setTypeface(utils.OpenSans_Bold(PaymentMenuActivity.this));



        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialog_AddCard();
            }
        });

        dialog.show();


    }


    public  void dialog_RemoveSureCard(){
        final BottomSheetDialog dialog = new BottomSheetDialog (PaymentMenuActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_card_del, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_update);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        Button btn_update = (Button) dialog.findViewById(R.id.btn_update);

        txt_title.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        btn_update.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));
        txt_msg.setTypeface(utils.OpenSans_Regular(PaymentMenuActivity.this));


        btn_update.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                dialog.dismiss();
                api_DelCustomer();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
//                dialog_AddCard();
            }
        });
        dialog.show();


    }


    private void api_DelCustomer() {
        AndroidNetworking.enableLogging();
        progres_load.setVisibility(View.VISIBLE);
        Log.e("url_del: ", NetworkingData.BASE_URL + NetworkingData.DEL_CARDINFO);


        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.DEL_CARDINFO)
                .addBodyParameter("user_id", sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_stripe: ", result + "");
                        //{"status":"1","customer":{"id":"cus_C4xJzoWD2YfwOO","object":"customer","account_balance":0,"created":1515128721,"currency":null,"default_source":"card_1BgnOOI0GVRrJIpDo8dLjinv","delinquent":false,"description":"Customer for LeadR app","discount":null,"email":null,"livemode":false,"metadata":{},"shipping":null,"sources":{"object":"list","data":[{"id":"card_1BgnOOI0GVRrJIpDo8dLjinv","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"Visa","country":"US","customer":"cus_C4xJzoWD2YfwOO","cvc_check":"pass","dynamic_last4":null,"exp_month":11,"exp_year":2022,"fingerprint":"kXn425uHw3F6DtGT","funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null}],"has_more":false,"total_count":1,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/subscriptions"}}}

                        utils.dismissProgressdialog();
                        progres_load.setVisibility(View.GONE);
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_CARD("N/A");
                                    /*String text = "<font color=#227cec>"+getResources().getString(R.string.frst_tym)+
                                            "</font> <font color=#071a30>"+getResources().getString(R.string.you_must)+"</font>";
                                    txt_top.setText(Html.fromHtml(text));
                                    img_remove.setVisibility(View.GONE);*/
                            String text = "<font color=#EA4142>"+getResources().getString(R.string.miss_det)+"</font>";
                            txt_card.setText(Html.fromHtml(text));
                            utils.dialog_msg_show_WithTitle(PaymentMenuActivity.this,getResources().getString(R.string.succ_del),
                                    getResources().getString(R.string.succ));
                        } else {
                            utils.dismissProgressdialog();
                            Toast.makeText(PaymentMenuActivity.this,getResources().getString(R.string.succ_error_del),Toast.LENGTH_LONG).show();

                        }
                        utils.dismissProgressdialog();

                    }

                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                        progres_load.setVisibility(View.GONE);

                    }
                });
    }



    public  void dialog_CardFrstTYm(Activity activity, String msg){
        dialog_card = new BottomSheetDialog (activity);
        dialog_card.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog_card.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog_card.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog_card.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog_card.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog_card.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        txt_title.setText(getResources().getString(R.string.pls_wait));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog_card.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_card.dismiss();
            }
        });
        dialog_card.show();


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sharedPreferenceLeadr.get_CARD().equalsIgnoreCase("N/A")) {
            String text = "<font color=#EA4142>"+getResources().getString(R.string.miss_det)+"</font>";
            txt_card.setText(Html.fromHtml(text));
        }else{
            txt_card.setText(getResources().getString(R.string.card_savd));
        }
    }
}
