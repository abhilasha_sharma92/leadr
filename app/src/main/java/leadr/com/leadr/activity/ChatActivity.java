package leadr.com.leadr.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import com.facebook.accountkit.AccountKit;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.BasePostprocessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.squareup.picasso.Picasso;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import adapter.Inbox_Adapter;
import adapter.MessageListAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import leadr.com.leadr.R;
import modal.ChatPojo;
import modal.InboxPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

import static fragment.BuyFragment.img_filter;


public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.edittext_chatbox)
    EditText edittext_chatbox;

    @BindView(R.id.reyclerview_message_list)
    RecyclerView reyclerview_message_list;

    @BindView(R.id.img_user)
    SimpleDraweeView img_user;

    @BindView(R.id.progres_load_user)
    ProgressBar progres_load_user;

    @BindView(R.id.txt_client_name)
    TextView txt_client_name;

    @BindView(R.id.txt_des_client)
    TextView txt_des_client;

    @BindView(R.id.txt_about)
    TextView txt_about;

    MessageListAdapter adapter_bottom;
    GlobalConstant utils;

    SharedPreferenceLeadr sharedPreferenceLeadr;
    ArrayList<ChatPojo> arr_msgList = new ArrayList<>();

    String me_id = "";
    String other_id = "";
    String get_other_img = "";
    String get_other_name = "";
    String get_other_buss = "";
    String get_other_info = "";
    String get_other_id = "";
    String get_other_leadid = "";
    public static String get_type_my = "";
    String get_type_other = "";
    String get_lead_user_id = "";
    String get_other_user_id = "";
    String get_audio = "";
    String get_audioTime = "";
    String get_desc = "";
    String get_leadPrice = null;
    String get_Address = "0";
    String get_Budget = "0";
    String get_Category = "0";
    public static String inside_chat = null;

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(NetworkingData.CHAT_SERVER_URL);
        } catch (URISyntaxException e) {}
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ButterKnife.bind(this);

        mSocket.connect();

        sharedPreferenceLeadr = new SharedPreferenceLeadr(ChatActivity.this);
        utils                 = new GlobalConstant();

        if(getIntent().getStringExtra("noti")!=null){

            Connect();
            api_NotiData();

        }else {
            method_getIntent();
            Connect();

        }



        Button button_chatbox_send = (Button)findViewById(R.id.button_chatbox_send);
        button_chatbox_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSocket.connected()) {
//                    utils.hideKeyboard(ChatActivity.this);
                    if(edittext_chatbox.getText().toString().trim().length()>0) {
                        if(arr_msgList!=null){
                            if(arr_msgList.size()<1){
                                Bundle parameters = new Bundle();
                                if(get_leadPrice!=null){
                                    parameters.putString("VALUE", get_leadPrice);
                                }else{
                                    parameters.putString("VALUE", "0");
                                }

                                parameters.putString("Lead description", get_desc+get_Address+","+get_Budget+","+get_Category);

                                AppEventsLogger logger = AppEventsLogger.newLogger(ChatActivity.this);

                                logger.logEvent("EVENT_NAME_buy_user_ask_about",
                                        parameters);
                            }
                        }else{

                            Bundle parameters = new Bundle();
                            if(get_leadPrice!=null){
                                parameters.putString("VALUE", get_leadPrice);
                            }else{
                                parameters.putString("VALUE", "0");
                            }

                            parameters.putString("Lead description", get_desc+get_Address+","+get_Budget+","+get_Category);

                            AppEventsLogger logger = AppEventsLogger.newLogger(ChatActivity.this);

                            logger.logEvent("EVENT_NAME_buy_user_ask_about",
                                    parameters);
                        }
                        attemptSend();
                    }else {
                        edittext_chatbox.setError("Enter message");
                        edittext_chatbox.requestFocus();
                    }
                }
            }
        });

        img_user.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                Intent i_pro = new Intent(ChatActivity.this, ProfileOther_Activity.class);
                i_pro.putExtra("id",get_other_id);
                startActivity(i_pro);
            }
        });





        txt_client_name.setTypeface(utils.OpenSans_Regular(ChatActivity.this));
        txt_des_client.setTypeface(utils.OpenSans_Regular(ChatActivity.this));
        txt_about.setTypeface(utils.OpenSans_Regular(ChatActivity.this));
        edittext_chatbox.setTypeface(utils.OpenSans_Regular(ChatActivity.this));



        /***Keyboard Listener*****/
        KeyboardVisibilityEvent.setEventListener(
                ChatActivity.this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                        if(isOpen){
                            if(adapter_bottom!=null) {
                                reyclerview_message_list.getLayoutManager().scrollToPosition(adapter_bottom.getItemCount() - 1);
                            }
                        }
                    }
                });

    }



    /***Set Data from Noti*********/
    private void api_NotiData(){
        AndroidNetworking.enableLogging();
        Log.e("url_getnotiData: ", NetworkingData.BASE_URL+ NetworkingData.MSG_DETAIL_NOTI);
        String leadid = getIntent().getStringExtra("lead_id");
        String userId = getIntent().getStringExtra("user_id");
        String user_otherid = getIntent().getStringExtra("user_otherid");

        Log.e("userId: ", userId);
        Log.e("lead_id: ",leadid );
        Log.e("user_otherid: ", user_otherid);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.MSG_DETAIL_NOTI)
                .addBodyParameter("userId",userId)
                .addBodyParameter("lead_id",leadid)
                .addBodyParameter("other_userId",user_otherid)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        Log.e("res_inbox1: ",response+"");
                        //{"status":1,"message_detail":[{"msg_to":"74","username":"xiomi 90","image":""}]}
                        if (response.optString("status").equals("1")) {
                            try {
                                JSONArray arr = response.getJSONArray("message_detail");
                                for(int i=0; i<arr.length(); i++){
                                    JSONObject obj = arr.getJSONObject(i);
                                    InboxPojo item = new InboxPojo();


                                    String from_usrnm = obj.optString("from_user_name");
                                        try {
                                            try {
                                                from_usrnm = new String(Base64.decode(from_usrnm.getBytes(),Base64.DEFAULT),
                                                        "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }


                                    String to_user_name = obj.optString("to_user_name");
                                        try {
                                            try {
                                                to_user_name = new String(Base64.decode(to_user_name.getBytes(),Base64.DEFAULT),
                                                        "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }


                                    String from_busnm = obj.optString("from_business_name");
                                        try {
                                            try {
                                                from_busnm = new String(Base64.decode(from_busnm.getBytes(),Base64.DEFAULT),
                                                        "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }


                                    String to_business_name = obj.optString("to_business_name");
                                        try {
                                            try {
                                                to_business_name = new String(Base64.decode(to_business_name.getBytes(),Base64.DEFAULT),
                                                        "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }


                                    String from_busnminfo = obj.optString("from_business_info");
                                        try {
                                            try {
                                                from_busnminfo = new String(Base64.decode(from_busnminfo.getBytes(),Base64.DEFAULT),
                                                        "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }


                                    String to_business_info = obj.optString("to_business_info");
                                        try {
                                            try {
                                                to_business_info = new String(Base64.decode(to_business_info.getBytes(),Base64.DEFAULT),
                                                        "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }


                                    String desc = obj.optString("lead_description");
                                        try {
                                            try {
                                                desc = new String(Base64.decode(desc.getBytes(),Base64.DEFAULT),
                                                        "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }


                                    if (!sharedPreferenceLeadr.getUserId().equalsIgnoreCase(obj.optString("lead_user_id"))) {
                                        get_other_img     = obj.optString("from_user_thum");
                                        get_other_name    = from_usrnm;
                                        get_other_buss    = from_busnm;
                                        get_other_info    = from_busnminfo;
                                        get_other_id      = obj.optString("lead_user_id");
                                        get_other_leadid  = obj.optString("lead_id");
                                        get_type_my       = obj.optString("msg_from_type");
                                        get_type_other    = obj.optString("msg_to_type");
                                        get_lead_user_id  = obj.optString("lead_user_id");
                                        get_other_user_id = obj.optString("other_user_id");
                                        get_audio         = obj.optString("audio");
                                        get_audioTime     = obj.optString("time");
                                        get_desc          = desc;

                                        Log.e("get_type_my: ",get_type_my);
                                        Log.e("get_other_img: ",get_other_img);
                                        Log.e("get_other_name: ",get_other_name);
                                        Log.e("get_other_buss: ",get_other_buss);
                                        Log.e("get_other_info: ",get_other_info);
                                        Log.e("get_other_id: ",get_other_id);
                                        Log.e("get_other_leadid: ",get_other_leadid);
                                        Log.e("get_type_other: ",get_type_other);
                                        Log.e("get_lead_user_id: ",get_lead_user_id);
                                        Log.e("get_other_user_id: ",get_other_user_id);
                                        Log.e("get_audio: ",get_audio);
                                        Log.e("get_audioTime: ",get_audioTime);
                                        Log.e("get_desc: ",get_desc);
                                    }else{
                                        get_other_img     = obj.optString("to_user_thum");
                                        get_other_name    = to_user_name;
                                        get_other_buss    = to_business_name;
                                        get_other_info    = to_business_info;
                                        get_other_id      = obj.optString("other_user_id");
                                        get_other_leadid  = obj.optString("lead_id");
                                        get_type_my       = obj.optString("msg_to_type");
                                        get_type_other    = obj.optString("msg_to_type");
                                        get_lead_user_id  = obj.optString("lead_user_id");
                                        get_other_user_id = obj.optString("other_user_id");
                                        get_audio         = obj.optString("audio");
                                        get_audioTime     = obj.optString("time");
                                        get_desc          = desc;

                                        Log.e("get_type_my: ",get_type_my);
                                        Log.e("get_other_img: ",get_other_img);
                                        Log.e("get_other_name: ",get_other_name);
                                        Log.e("get_other_buss: ",get_other_buss);
                                        Log.e("get_other_info: ",get_other_info);
                                        Log.e("get_other_id: ",get_other_id);
                                        Log.e("get_other_leadid: ",get_other_leadid);
                                        Log.e("get_type_other: ",get_type_other);
                                        Log.e("get_lead_user_id: ",get_lead_user_id);
                                        Log.e("get_other_user_id: ",get_other_user_id);
                                        Log.e("get_audio: ",get_audio);
                                        Log.e("get_audioTime: ",get_audioTime);
                                        Log.e("get_desc: ",get_desc);
                                    }
                                }
                                 method_setValues();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if(response.optString("message").contains("Suspended account")){
                                    AccountKit.logOut();
                                    sharedPreferenceLeadr.clearPreference();
                                    String languageToLoad  = "en";
                                    Locale locale = new Locale(languageToLoad);
                                    Locale.setDefault(locale);
                                    Configuration config = new Configuration();
                                    config.locale = locale;
                                    getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                }
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("err: ",error.toString());
                        utils.dismissProgressdialog();
                    }
                });

    }


    private void Connect(){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("user_id", me_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.on(Socket.EVENT_CONNECT,onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("send_message", onSendMessage);
        mSocket.on("receive_message", onNewMessage);
        mSocket.connect();
    }


    private void method_getIntent() {
        get_other_img       = getIntent().getStringExtra("image_other");
        get_other_name      = getIntent().getStringExtra("name_other");
        get_other_buss      = getIntent().getStringExtra("buss_other");
        get_other_info      = getIntent().getStringExtra("info_other");
        get_other_id        = getIntent().getStringExtra("id_other");
        get_other_leadid    = getIntent().getStringExtra("leadid_other");
        get_type_my         = getIntent().getStringExtra("typemy");
        get_type_other      = getIntent().getStringExtra("typeother");
        get_lead_user_id    = getIntent().getStringExtra("lead_user_id");
        get_other_user_id   = getIntent().getStringExtra("other_user_id");
        get_audio           = getIntent().getStringExtra("audio");
        get_audioTime       = getIntent().getStringExtra("audiotime");
        get_desc            = getIntent().getStringExtra("desc");
        get_leadPrice            = getIntent().getStringExtra("price_lead");
        get_Address            = getIntent().getStringExtra("address");
        get_Budget            = getIntent().getStringExtra("budget");
        get_Category            = getIntent().getStringExtra("category");

        Log.e("get_type_my: ",get_type_my);
        Log.e("get_other_img: ",get_other_img);
        Log.e("get_other_name: ",get_other_name);
        Log.e("get_other_buss: ",get_other_buss);
//        Log.e("get_other_info: ",get_other_info);
        Log.e("get_other_id: ",get_other_id);
        Log.e("get_other_leadid: ",get_other_leadid);
        Log.e("get_type_other: ",get_type_other);
        Log.e("get_lead_user_id: ",get_lead_user_id);
        Log.e("get_other_user_id: ",get_other_user_id);
        Log.e("get_audio: ",get_audio);
        Log.e("get_audioTime: ",get_audioTime);
        Log.e("get_desc: ",get_desc);

        method_setValues();
       /* if(!sharedPreferenceLeadr.getUserId().equalsIgnoreCase(get_lead_user_id)) {
            btn_type.setText(arr_inbox.get(i).getMsg_from_type());
            txt_name.setText(arr_inbox.get(i).getFrom_user_name());
            try{
                Picasso.with(ctx).load(arr_inbox.get(i).getFrom_user_thum())
                        .into(img_user);
            }catch (Exception e){}
        }else{
            btn_type.setText(arr_inbox.get(i).getMsg_to_type());
            txt_name.setText(arr_inbox.get(i).getTo_user_name());
            try{
                Picasso.with(ctx).load(arr_inbox.get(i).getTo_user_thum())
                        .into(img_user);
            }catch (Exception e){}
        }*/


    }

    void method_setValues(){
        String type_Add = getResources().getString(R.string.lead__Ask_publs);
        if(get_type_my.equalsIgnoreCase("I Buy")){
            type_Add = getResources().getString(R.string.lead__Ask);
        }else if(get_type_my.equalsIgnoreCase("I Bought")){
            type_Add = getResources().getString(R.string.lead__Ask_bought);
        }else if(get_type_my.equalsIgnoreCase("I Sold")){
            type_Add = getResources().getString(R.string.lead__Ask_sold);
        }
        txt_client_name.setText(get_other_name+" ("+type_Add+")");
        if(get_other_buss!=null) {
            txt_des_client.setText(get_other_buss);
        }
        if(get_audioTime!=null) {
            if(!get_audioTime.trim().equalsIgnoreCase("")){
                if(get_desc.trim().equalsIgnoreCase("")){
                    txt_about.setText(getResources().getString(R.string.abt)+": " +getResources().getString(R.string.clkc));
                }else{
                    txt_about.setText(getResources().getString(R.string.abt)+": " +get_desc);
                }
            }else{
                txt_about.setText(getResources().getString(R.string.abt)+": " +get_desc);
            }
        }else{
            txt_about.setText(getResources().getString(R.string.abt)+": " +get_desc);
        }


        try{
            progres_load_user.setVisibility(View.VISIBLE);

            DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                    ImageRequestBuilder.newBuilderWithSource(Uri.parse(get_other_img)).
                            setRotationOptions(RotationOptions.disableRotation())
                            .setPostprocessor(new BasePostprocessor() {
                                @Override
                                public void process(Bitmap bitmap) {
                                }
                            })
                            .build())
                    .setControllerListener(new BaseControllerListener<ImageInfo>() {
                        @Override
                        public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                            progres_load_user.setVisibility(View.GONE);
                        }
                        @Override
                        public void onFailure(String id, Throwable throwable) {
                            super.onFailure(id, throwable);
                            progres_load_user.setVisibility(View.GONE);
                        }
                        @Override
                        public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                            progres_load_user.setVisibility(View.GONE);
                        }
                    })
                    .build();
            img_user.setController(controller);
        }catch (Exception e){progres_load_user.setVisibility(View.GONE);}


        me_id    = sharedPreferenceLeadr.getUserId();
        other_id = get_other_id;

        utils.showProgressDialog(ChatActivity.this,getResources().getString(R.string.load));
        api_GetChat();
        api_REadChat();
    }


    @OnClick(R.id.txt_about)
    public void aboutClick() {
        Intent i = new Intent(ChatActivity.this,AboutLeadActivity.class);
        i.putExtra("chat",get_type_my);
        i.putExtra("lead_id",get_other_leadid);
        startActivity(i);
    }


    @Override
    protected void onResume() {
        super.onResume();
        inside_chat = "";
    }

    /*****
     * DISCONNECTED****
     * */

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i("TAG", "diconnected");
                  /*  Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.disconnect), Toast.LENGTH_LONG).show();*/
                }
            });
        }
    };


    /*****
     * ERROR IN CONNECTION****
     * */
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("TAG", "Error connecting");
                  /*  Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.err_conn), Toast.LENGTH_LONG).show();*/
                }
            });
        }
    };


    /*****
     * CONNECTED****
     * */
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject=new JSONObject();
                    try {
                        jsonObject.put("user_id", me_id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("TAG", "connected");
                    mSocket.emit("touch_server", jsonObject);
                   /* Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.conn), Toast.LENGTH_LONG).show();*/
                }
            });
        }
    };


    /*****
     * SEND MESSAGE TO USER****
     * */
    private Emitter.Listener onSendMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e("SEndedddd.........", String.valueOf(data));
                    /*Toast.makeText(getApplicationContext(),
                            "sendingg", Toast.LENGTH_LONG).show();*/
                }
            });
        }
    };


    /*****
     * RECEIVING MESSAGE****
     * */
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];

                    Log.e("Reciving.........", String.valueOf(data));

                    String msg_get = "";
                    String myid_get = "";
                    String otherid_get = "";
                    String users_get = "";
                    try {
                         msg_get = data.getJSONObject("msg").getString("msg");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        myid_get = data.getJSONObject("reciever").getString("_id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        otherid_get = data.getJSONObject("sender").getString("_id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        users_get = data.getJSONObject("users").getString("lead_id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(myid_get.equals(sharedPreferenceLeadr.getUserId())){
                        if(otherid_get.equals(other_id)){
                            if(users_get.equalsIgnoreCase(get_other_leadid)) {
                                ChatPojo item = new ChatPojo();
                                item.setMsg(msg_get);
                                item.setCurrent_time(CurrentDate());
                                item.setMSGTYPE("sent");
                                adapter_bottom.addMsg(item);
                                reyclerview_message_list.getLayoutManager().scrollToPosition(adapter_bottom.getItemCount() - 1);

                                api_REadChat();
                            }
                        }
                    }
                }
            });
        }
    };



    private void attemptSend() {
        String message = edittext_chatbox.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            return;
        }
        edittext_chatbox.getText().clear();
        JSONObject obj=new JSONObject();
//get_other_id = other_id
        try {
            obj.put("user_id", me_id);
            obj.put("to_user_id", other_id);
            obj.put("lead_id", get_other_leadid);
            if(get_lead_user_id.equalsIgnoreCase(sharedPreferenceLeadr.getUserId())){
                obj.put("msg_to_type", get_type_my);
                obj.put("msg_from_type", get_type_other);
            }else{
                obj.put("msg_to_type", get_type_other);
                obj.put("msg_from_type", get_type_my);
            }

            obj.put("lead_user_id", me_id);
            obj.put("other_user_id", other_id);
            obj.put("lead_user_id", get_lead_user_id);
            obj.put("other_user_id", get_other_user_id);
            obj.put("msg", message);//mInputMessageView.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Sending", String.valueOf(obj));
        mSocket.emit("send_message", obj);

        ChatPojo item = new ChatPojo();
        item.setMsg(message);
        item.setCurrent_date(CurrentDate());
        item.setCurrent_time(CurrentTime());
        item.setMSGTYPE("receive");
        adapter_bottom.addMsg(item);
        reyclerview_message_list.getLayoutManager().scrollToPosition(adapter_bottom.getItemCount() - 1);
        api_REadChat();



    }


    String CurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    String CurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh-mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }





    private String getDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("hh-mm");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);
        }
        catch (Exception e) {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("receive_message", onNewMessage);

        Unregistrar unregistrar = KeyboardVisibilityEvent.registerEventListener(
                ChatActivity.this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                    }
                });

// call this method when you don't need the event listener anymore
        unregistrar.unregister();
    }


    @OnClick(R.id.img_back)
    public void onBackClick() {
        finish();
    }

    private void api_GetChat(){
        AndroidNetworking.enableLogging();
        Log.e("url_getchat: ", NetworkingData.CHAT_GET_LIST);
        Log.e("userid: ", sharedPreferenceLeadr.getUserId());
        Log.e("otherid: ", other_id);
        Log.e("lead_id: ", get_other_leadid);
        AndroidNetworking.post(NetworkingData.BASE_URL+NetworkingData.CHAT_GET_LIST)
                .addBodyParameter("userId",me_id)
                .addBodyParameter("to_userId",other_id)
                .addBodyParameter("lead_id",get_other_leadid)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        arr_msgList.clear();
                        utils.dismissProgressdialog();
                        Log.e("res_chat: ",response+"");
                        //2018-01-30T11:26:57.000Z
                        try {
                            JSONArray arr = response.getJSONArray("message_detail");
                            for(int i=0; i<arr.length(); i++){
                                JSONObject obj = arr.getJSONObject(i);
                                ChatPojo item = new ChatPojo();
                                item.setId(obj.getString("id"));

                                String full_dat_tym_buy = obj.optString("current_time_msg");
                                String[] spiltby_T_buy = full_dat_tym_buy.split("T");
                                String date_buy = spiltby_T_buy[0].trim();
                                String tym_wid_Z_buy = spiltby_T_buy[1].trim();
                                String spiltby_Dot1_buy = tym_wid_Z_buy.replace(".000Z","");
                                String replace_tym_buy = spiltby_Dot1_buy.replace(":","-");
                                item.setCurrent_date(date_buy);
                                item.setCurrent_time(replace_tym_buy);

                                /*String arr_date[] = date.split("-");
                                String arr_time[] = spiltby_Dot1_buy.split(":");
                                String year = arr_date[0];
                                String month = arr_date[1];
                                String day = arr_date[2];
                                String hour = arr_time[0];
                                String mm = arr_time[1];
                                String time_after_conv = getDate(hour+"-"+mm);
                                item.setCurrent_time(day+"/"+month+"/"+year+" "+time_after_conv);*/

                                item.setLead_id(obj.getString("lead_id"));
                                item.setMsg(obj.getString("msg"));
                                if(obj.getString("msg_from").equals(other_id)){
                                    item.setMSGTYPE("sent");
                                }else{
                                    item.setMSGTYPE("receive");
                                }
                                arr_msgList.add(item);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if(response!=null) {
                                if (response.optString("message").contains("Suspended account")) {
                                    AccountKit.logOut();
                                    sharedPreferenceLeadr.clearPreference();
                                    String languageToLoad = "en";
                                    Locale locale = new Locale(languageToLoad);
                                    Locale.setDefault(locale);
                                    Configuration config = new Configuration();
                                    config.locale = locale;
                                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());

                                    Intent i_nxt = new Intent(ChatActivity.this, GetStartedActivity.class);
                                    i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i_nxt);
                                    finish();
                                }
                            }
                        }

                        reyclerview_message_list.setHasFixedSize(true);
                        LinearLayoutManager lnr_bottom = new LinearLayoutManager(ChatActivity.this, LinearLayoutManager.VERTICAL, false);
                        reyclerview_message_list.setLayoutManager(lnr_bottom);
                        adapter_bottom = new MessageListAdapter(arr_msgList,get_other_img,ChatActivity.this);
                        reyclerview_message_list.setAdapter(adapter_bottom);
                        reyclerview_message_list.getLayoutManager().scrollToPosition(adapter_bottom.getItemCount() - 1);
                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });

    }

    private void api_REadChat(){
        AndroidNetworking.enableLogging();
        Log.e("url_getchat: ", "update_read_status");
        Log.e("userid: ", sharedPreferenceLeadr.getUserId());
        Log.e("otherid: ", other_id);
        Log.e("lead_id: ", get_other_leadid);
        AndroidNetworking.post(NetworkingData.BASE_URL+"update_read_status")
                .addBodyParameter("userId",me_id)
                .addBodyParameter("to_userId",other_id)
                .addBodyParameter("lead_id",get_other_leadid)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                    }
                    @Override
                    public void onError(ANError error) {
                    }
                });

    }


}
