package leadr.com.leadr.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.appevents.AppEventsLogger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

public class ConfirmWithdrawActivity extends AppCompatActivity {

    @BindView(R.id.txt_how_mch)
    TextView txt_how_mch;

    @BindView(R.id.txt_tckt)
    TextView txt_tckt;

    @BindView(R.id.txt_date)
    TextView txt_date;

    @BindView(R.id.txt_wth)
    TextView txt_wth;

    @BindView(R.id.txt_wth_amount)
    TextView txt_wth_amount;

    @BindView(R.id.txt_card)
    TextView txt_card;

    @BindView(R.id.txt_wthd)
    TextView txt_wthd;

    @BindView(R.id.btn_cancel)
    Button btn_cancel;

    @BindView(R.id.btn_cnfrm)
    Button btn_cnfrm;

    @BindView(R.id.img_back)
    ImageView img_back;

    GlobalConstant utils;
    String get_amount;
    String market_fee = "0";
    SharedPreferenceLeadr sharedPreferenceLeadr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_withdraw);

        ButterKnife.bind(this);
        utils = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(ConfirmWithdrawActivity.this);

        get_amount = getIntent().getStringExtra("amount");

        method_SetTypeface();
        api_chck_AdminSettings();
    }

    private void method_SetTypeface() {
        txt_how_mch.setTypeface(utils.OpenSans_Regular(ConfirmWithdrawActivity.this));
        txt_tckt.setTypeface(utils.OpenSans_Regular(ConfirmWithdrawActivity.this));
        txt_date.setTypeface(utils.OpenSans_Regular(ConfirmWithdrawActivity.this));
        txt_wth.setTypeface(utils.OpenSans_Regular(ConfirmWithdrawActivity.this));
        txt_wth_amount.setTypeface(utils.OpenSans_Regular(ConfirmWithdrawActivity.this));
        txt_card.setTypeface(utils.OpenSans_Regular(ConfirmWithdrawActivity.this));
        txt_wthd.setTypeface(utils.OpenSans_Regular(ConfirmWithdrawActivity.this));
        btn_cancel.setTypeface(utils.OpenSans_Regular(ConfirmWithdrawActivity.this));
        btn_cnfrm.setTypeface(utils.OpenSans_Regular(ConfirmWithdrawActivity.this));

        txt_wth_amount.setText("+$"+get_amount);

        txt_date.setText(method_CurrDate());

    }


    String method_CurrDate(){
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    @Override
    public void onBackPressed() {
        if (btn_cnfrm.getVisibility() == View.VISIBLE) {
           finish();
        } else {
            Intent i_nxt = new Intent(ConfirmWithdrawActivity.this, PaymentMenuActivity.class);
            startActivity(i_nxt);
            finish();
        }
    }

    @OnClick(R.id.img_back)
    public void onBack() {
       onBackPressed();
    }

    @OnClick(R.id.btn_cnfrm)
    public void onCnfrm() {
        utils.dialog_msg_show(ConfirmWithdrawActivity.this,"Under Development");
//        api_withdraw();
    }

    @OnClick(R.id.btn_cancel)
    public void onClose() {
       onBackPressed();
    }


    private void api_withdraw(){
        AndroidNetworking.enableLogging();
        Log.e("url_withdraw: ", NetworkingData.BASE_URL+ NetworkingData.WITHDRAW);
        Log.e("userId: ", sharedPreferenceLeadr.getUserId());
        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.WITHDRAW)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("amount",get_amount)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        Log.e("res_with: ",response+"");


                        if(response.optString("status").equals("1")){
                            Bundle parameters = new Bundle();
                            parameters.putString("VALUE OF THE withdrawal", get_amount+"");

                            AppEventsLogger logger = AppEventsLogger.newLogger(ConfirmWithdrawActivity.this);

                            logger.logEvent("EVENT_NAME_PAYMENTS_USER_WITHDRAW",
                                    parameters);

                            btn_cnfrm.setVisibility(View.GONE);
                            txt_wthd.setVisibility(View.VISIBLE);
                            btn_cancel.setText(getResources().getString(R.string.close_caps));
                            txt_how_mch.setText(getResources().getString(R.string.with_cmp));
                        }else{
                           utils.dialog_msg_show(ConfirmWithdrawActivity.this
                           ,response.optString("message"));
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });

    }


    /***
     * CHeck ADMIN Settings to set price range***/
    private void api_chck_AdminSettings(){
        AndroidNetworking.enableLogging();
        utils.showProgressDialog(ConfirmWithdrawActivity.this,getResources().getString(R.string.load));
        Log.e("url_chkSetting: ", NetworkingData.BASE_URL+ NetworkingData.GET_ADMIN_SETTING);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.GET_ADMIN_SETTING)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chkadmin: ",result+"");
                        //{"status":1,"message":"Details..","admin_settings":[{"id":1,"admin_id":1,"number_of_leads":4,"min_amount":"1
                        //  ","max_lead_price":"100","market_fee":"44"}]}

                        try{
                            JSONArray arr = result.getJSONArray("admin_settings");
                            for (int i = 0; i < arr.length(); i++) {
                                JSONObject obj = arr.getJSONObject(i);
                                market_fee = obj.optString("market_fee").trim();
                                Log.e("market_fee: ",market_fee);
                                float percnt = Float.valueOf(market_fee)/100;
                                float amout_xxx = Float.valueOf(get_amount)*percnt;
                                float credt = Float.valueOf(get_amount) - amout_xxx;
                               /* txt_card.setText(getResources().getString(R.string.ourfee)+" $"+amout_xxx+"\n"+
                                        getResources().getString(R.string.urcard)+" $"+credt);*/
                                txt_card.setVisibility(View.GONE);
                            }
                        }catch (Exception e){
                            if(result!=null) {
                                if (result.optString("message").contains("Suspended account")) {
                                    AccountKit.logOut();
                                    sharedPreferenceLeadr.clearPreference();
                                    String languageToLoad = "en";
                                    Locale locale = new Locale(languageToLoad);
                                    Locale.setDefault(locale);
                                    Configuration config = new Configuration();
                                    config.locale = locale;
                                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());

                                    Intent i_nxt = new Intent(ConfirmWithdrawActivity.this, GetStartedActivity.class);
                                    i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i_nxt);
                                    finish();
                                }
                            }
                        }

                        utils.dismissProgressdialog();
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }
}
