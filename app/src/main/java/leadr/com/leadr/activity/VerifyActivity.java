package leadr.com.leadr.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import utils.GlobalConstant;

public class VerifyActivity extends AppCompatActivity {

    @BindView(R.id.edt_otp1)
    EditText edt_otp1;

    @BindView(R.id.edt_otp2)
    EditText edt_otp2;

    @BindView(R.id.edt_otp3)
    EditText edt_otp3;

    @BindView(R.id.edt_otp4)
    EditText edt_otp4;

    @BindView(R.id.btn_next)
    Button btn_next;

    @BindView(R.id.txt_verify)
    TextView txt_verify;

    @BindView(R.id.img_back)
    ImageView img_back;

    String phone;
    String type;
    
    GlobalConstant utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

        // bind the view using butterknife
        ButterKnife.bind(this);

        utils = new GlobalConstant();

        phone = getIntent().getStringExtra("phone");
        type = getIntent().getStringExtra("type");

        txt_verify.setText("Please type the verification code sent to "+phone);

        edt_otp1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(edt_otp1.getText().toString().length()==1)     //size as per your requirement
                {
                    edt_otp2.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        edt_otp2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(edt_otp2.getText().toString().length()==1)     //size as per your requirement
                {
                    edt_otp3.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        edt_otp3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(edt_otp3.getText().toString().length()==1)     //size as per your requirement
                {
                    edt_otp4.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        edt_otp4.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(edt_otp4.getText().toString().length()==1)     //size as per your requirement
                {
                    if(type.equals("login")){
                        Intent i_nxt = new Intent(VerifyActivity.this, MainActivity.class);
                        i_nxt.putExtra("phone",phone);
                        startActivity(i_nxt);
                        overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                    }else{
                        Intent i_nxt = new Intent(VerifyActivity.this, Bfr_Reg_Activity.class);
                        i_nxt.putExtra("phone",phone);
                        startActivity(i_nxt);
                        overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                    }
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
    }


    @OnClick(R.id.btn_next)
    public void onNext() {
       if(edt_otp1.getText().toString().trim().length()==1 && edt_otp2.getText().toString().trim().length()==1 &&
               edt_otp3.getText().toString().trim().length()==1 && edt_otp4.getText().toString().trim().length()==1){

       }else{
           utils.dialog_msg_show(VerifyActivity.this,"Enter OTP first!!");
       }
    }

    @OnClick(R.id.img_back)
    public void onBack() {
      finish();
    }
}
