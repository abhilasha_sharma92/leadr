package leadr.com.leadr.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.BasePostprocessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.squareup.picasso.Picasso;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import leadr.com.leadr.activity.FilterActivity.CategoryListAdapter;
import modal.CategoryPojo;
import modal.ProfileOther_Pojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkStateReceiver;
import utils.NetworkStateReceiver.NetworkStateReceiverListener;
import utils.NetworkingData;

public class ProfileOther_Activity extends AppCompatActivity implements NetworkStateReceiverListener {

    @BindView(R.id.txt_user_name)
    TextView txt_user_name;

    @BindView(R.id.txt_info)
    TextView txt_info;

    @BindView(R.id.txt_sold_count)
    TextView txt_sold_count;

    @BindView(R.id.txt_sold)
    TextView txt_sold;

    @BindView(R.id.txt_buy_count)
    TextView txt_buy_count;

    @BindView(R.id.txt_buy)
    TextView txt_buy;

    @BindView(R.id.txt_refund_count)
    TextView txt_refund_count;

    @BindView(R.id.txt_refund)
    TextView txt_refund;

    @BindView(R.id.txt_bought_count)
    TextView txt_bought_count;

    @BindView(R.id.txt_bought)
    TextView txt_bought;

    @BindView(R.id.txt_i_sold)
    TextView txt_i_sold;

    @BindView(R.id.txt_isold_count)
    TextView txt_isold_count;

    @BindView(R.id.txt_buss_desc)
    TextView txt_buss_desc;

    @BindView(R.id.txt_loc)
    TextView txt_loc;

    @BindView(R.id.txt_site)
    TextView txt_site;

    @BindView(R.id.txt_desc_about)
    TextView txt_desc_about;

    @BindView(R.id.lnr_desc)
    LinearLayout lnr_desc;

    @BindView(R.id.txt_cate)
    TextView txt_cate;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.img_buss_img)
    SimpleDraweeView img_buss_img;

    @BindView(R.id.vw_desc)
    View vw_desc;

    @BindView(R.id.vw_name)
    View vw_name;

    @BindView(R.id.img_user)
    SimpleDraweeView img_user;

    @BindView(R.id.progres_load_user)
    ProgressBar progres_load_user;


    @BindView(R.id.progres_load_buss)
    ProgressBar progres_load_buss;

    GlobalConstant utils;
    String get_id;
    String user_img = "";
    String buss_img = "";
    String cate_en  = "";
    String cate_heb = "";
    ArrayList<ProfileOther_Pojo> arr_profile = new ArrayList<>();

    String[]              arr_cate ;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    NetworkStateReceiver networkStateReceiver;
    BottomSheetDialog dialog_internet;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_other);

        // bind the view using butterknife
        ButterKnife.bind(this);

        utils                 = new GlobalConstant();
        get_id                = getIntent().getStringExtra("id");
        sharedPreferenceLeadr = new SharedPreferenceLeadr(ProfileOther_Activity.this);

        methodTYpeface();

        if(utils.isNetworkAvailable(ProfileOther_Activity.this)){
           /* progres_load_user.setVisibility(View.VISIBLE);
            progres_load_buss.setVisibility(View.VISIBLE);*/
            api_OtherProfile();
        }else{
//            utils.dialog_msg_show(ProfileOther_Activity.this,getResources().getString(R.string.no_internet));
        }

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }



    private void methodTYpeface() {
        txt_user_name.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_isold_count.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_info.setTypeface(utils.OpenSans_Light(ProfileOther_Activity.this));
        txt_i_sold.setTypeface(utils.OpenSans_Light(ProfileOther_Activity.this));
        txt_sold_count.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_sold.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_buy_count.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_buy.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_refund_count.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_refund.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_bought_count.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_bought.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_buss_desc.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_loc.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_site.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        txt_desc_about.setTypeface(utils.OpenSans_Light(ProfileOther_Activity.this));
        txt_cate.setTypeface(utils.OpenSans_Light(ProfileOther_Activity.this));

    }


    @OnClick(R.id.txt_site)
    public void OnWebpage() {
        if(txt_site.getText().toString().trim().length()>0) {
            if (!txt_site.getText().toString().equals(getResources().getString(R.string.web_un))) {
                try {
                    String url = txt_site.getText().toString();
                    if (!url.startsWith("http://") || !url.startsWith("https://")) {
                        url = "http://" + url;
                    }
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } catch (Exception e) {
                    utils.dialog_msg_show(ProfileOther_Activity.this, getResources().getString(R.string.invalid));
                }
            }
        }
    }


    @OnClick(R.id.img_back)
    public void onBack() {
        finish();
    }


    @OnClick(R.id.img_user)
    public void onImageClick() {
        if(arr_profile.get(0).getUser_image().trim().length()>2) {
            dialog_fullImg(arr_profile.get(0).getUser_image());
        }
    }


    @OnClick(R.id.img_buss_img)
    public void onBussImageClick() {
        if(arr_profile.get(0).getBusiness_image().trim().length()>2) {
            dialog_fullImg(arr_profile.get(0).getBusiness_image());
        }
    }


    public  void dialog_fullImg(String img) {
        final Dialog dialog = new Dialog(ProfileOther_Activity.this, android.R.style.Theme_Holo_NoActionBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_full_image_fresco);

        final Button btn_close           = (Button) dialog.findViewById(R.id.btn_close);
        final SimpleDraweeView img_full  = (SimpleDraweeView) dialog.findViewById(R.id.img_full);
        final ProgressBar progres_load   = (ProgressBar) dialog.findViewById(R.id.progres_load);


        btn_close.setTypeface(utils.OpenSans_Regular(ProfileOther_Activity.this));
        progres_load.setVisibility(View.VISIBLE);

        Uri uri = Uri.parse(img);
        img_full.setImageURI(uri);

        DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                ImageRequestBuilder.newBuilderWithSource(Uri.parse(img))
                        .setPostprocessor(new BasePostprocessor() {
                            @Override
                            public void process(Bitmap bitmap) {
                            }
                        })
                        .build())
                .setControllerListener(new BaseControllerListener<ImageInfo>() {
                    @Override
                    public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                        progres_load_buss.setVisibility(View.GONE);
                    }
                    @Override
                    public void onFailure(String id, Throwable throwable) {
                        super.onFailure(id, throwable);
                        progres_load_buss.setVisibility(View.GONE);
                    }
                    @Override
                    public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                        progres_load_buss.setVisibility(View.GONE);
                    }
                })
                .build();
        img_full.setController(controller);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void api_OtherProfile(){
        AndroidNetworking.enableLogging();
//        utils.showProgressDialog(this,getResources().getString(R.string.load_profile));
        Log.e("url_profile: ", NetworkingData.BASE_URL+ NetworkingData.GET_PROFILE);
        Log.e("id: ", get_id);
        Log.e("user_id: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.GET_PROFILE)
                .addBodyParameter("id",get_id)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_profile: ",result+"");
                        //{"status":1,"message":"User Detail..",
                        // "data":{"no_of_leads":3,"is_delete":0,"device_token":"0","device_type":"0",
                        // "lon":"0","lat":"0","location_name":"0","category":"1","business_fb_page":"",
                        // "business_image":"","business_info":"","business_name":"leadr","job_title":"vp product ",
                        // "account_balance":0,"user_image":"","user_name":"test user 1","phone_number":"+972527268362","id":79}}

                        arr_profile.clear();

                        utils.dismissProgressdialog();
                        if(result.optString("status").equals("1")){
                            try {
                                JSONObject obj = result.getJSONObject("data");


                                    ProfileOther_Pojo item = new ProfileOther_Pojo();
                                    item.setId(obj.optString("id"));
                                    item.setPhone_number(obj.optString("phone_number"));

                                    String usr_nm = obj.optString("user_name");
                                    try {
                                        try {
                                            usr_nm = new String(Base64.decode(usr_nm.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }
                                    } catch (IllegalArgumentException e) {
                                        e.printStackTrace();
                                    }

                                    item.setUser_name(usr_nm);

                                    item.setUser_image(obj.optString("user_image"));

                                    String job_tit = obj.optString("job_title");
                                    try {
                                        try {
                                            job_tit = new String(Base64.decode(job_tit.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }
                                    } catch (IllegalArgumentException e) {
                                        e.printStackTrace();
                                    }

                                    item.setJob_title(job_tit);

                                String bus_name = obj.getString("business_name");
                                    try {
                                        try {
                                            bus_name = new String(Base64.decode(bus_name.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }
                                    } catch (IllegalArgumentException e) {
                                        e.printStackTrace();
                                    }

                                String bus_info = obj.optString("business_info");
                                    try {
                                        try {
                                            bus_info = new String(Base64.decode(bus_info.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }
                                    } catch (IllegalArgumentException e) {
                                        e.printStackTrace();
                                    }

                                    item.setBusiness_name(bus_name);
                                    item.setBusiness_info(bus_info);

                                    item.setBusiness_image(obj.optString("business_image"));
                                    item.setBusiness_fb_page(obj.optString("business_fb_page"));
                                    item.setCategory(obj.optString("category"));
                                    item.setIs_delete(obj.optString("is_delete"));

                                String loc = obj.optString("location_name");


                                try {
                                    try {
                                        loc = new String(Base64.decode(loc.getBytes(),Base64.DEFAULT), "UTF-8");
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                } catch (IllegalArgumentException e) {
                                    e.printStackTrace();

                                }
                                    item.setLocation_name(loc);
                                    item.setBought(obj.optString("no_of_bought_by_i"));
                                    item.setRefund(obj.optString("no_of_refunds"));
                                    item.setBuy_leads(obj.optString("buy_leads"));
                                    item.setSold_leads(obj.optString("sold_leads"));
                                    item.setUser_thum(obj.optString("user_thum"));
                                    item.setBusi_thum(obj.optString("busi_thum"));
                                    item.setCategory(obj.optString("category"));
                                    item.setCate_en(obj.optString("category_name"));
                                    item.setCate_heb(obj.optString("category_name_hebrew"));
                                    item.setNo_buyers(obj.optString("no_of_buyers"));
                                    item.setI_sold_to(obj.optString("no_of_bought_by_o"));
                                    arr_profile.add(item);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(ProfileOther_Activity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }

                        method_setData();
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                        progres_load_user.setVisibility(View.GONE);
                        progres_load_buss.setVisibility(View.GONE);
                    }
                });

    }

    private void method_setData() {
        try{
            txt_user_name.setText(arr_profile.get(0).getUser_name());
            String job_txt = "";
            try{
                job_txt = arr_profile.get(0).getJob_title();
            }catch (Exception e){}

            try{
                if(job_txt.trim().length()>0){
                    vw_name.setVisibility(View.VISIBLE);
                }
                txt_info.setText(job_txt);
            }catch (Exception e){}
            try{
                txt_buss_desc.setText(arr_profile.get(0).getBusiness_name());
            }catch (Exception e){}

            try{
                if(arr_profile.get(0).getBusiness_fb_page().trim().equals("")){
                    txt_site.setText(getResources().getString(R.string.web_un));
                    txt_site.setTextColor(getResources().getColor(R.color.gray_budget));
                    Drawable img = getResources().getDrawable( R.drawable.globe_pro_othr );
                    txt_site.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);            }
                else {
                    Drawable img = getResources().getDrawable( R.drawable.globe_pro_blue );
                    txt_site.setTextColor(getResources().getColor(R.color.colorPrimary));
                    txt_site.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
                    txt_site.setText(arr_profile.get(0).getBusiness_fb_page());
                }
            }catch (Exception e){}

            String desc_abt = "";
            try{
                desc_abt = arr_profile.get(0).getBusiness_info();
            }catch (Exception e){}
            if(desc_abt.trim().length()>0){
                vw_desc.setVisibility(View.VISIBLE);
                txt_desc_about.setVisibility(View.VISIBLE);
                lnr_desc.setVisibility(View.VISIBLE);
            }

            try{
                txt_desc_about.setText(arr_profile.get(0).getBusiness_info());
            }catch (Exception e){}

            try{
                if(arr_profile.get(0).getSold_leads()!=null){
                    if(arr_profile.get(0).getSold_leads().trim().length()>0){
                        txt_sold_count.setText(arr_profile.get(0).getSold_leads());
                    }
                }
            }catch (Exception e){}

            try{
                if(arr_profile.get(0).getRefund()!=null){
                    if(arr_profile.get(0).getRefund().trim().length()>0){
                        txt_refund_count.setText(arr_profile.get(0).getRefund());
                    }
                }
            }catch (Exception e){}

            try{
                if(arr_profile.get(0).getBought()!=null){
                    if(arr_profile.get(0).getBought().trim().length()>0){
                        txt_bought_count.setText(arr_profile.get(0).getBought());
                    }
                }
            }catch (Exception e){}

            try{
                if(arr_profile.get(0).getNo_buyers()!=null){
                    if(arr_profile.get(0).getNo_buyers().trim().length()>0){
                        txt_buy_count.setText(arr_profile.get(0).getNo_buyers());
                    }
                }
            }catch (Exception e){}

            try{
                if(arr_profile.get(0).getI_sold_to()!=null){
                    if(arr_profile.get(0).getI_sold_to().trim().length()>0){
                        txt_isold_count.setText(arr_profile.get(0).getI_sold_to());
                    }
                }
            }catch (Exception e){}

            try{
                if(!arr_profile.get(0).getLocation_name().equals("0")){
                    if(!arr_profile.get(0).getLocation_name().equals("")) {
                        String loc_nm = arr_profile.get(0).getLocation_name();


                        if(loc_nm.contains("@")) {
                            loc_nm = loc_nm.replace("@", "\n");
                        }
                        txt_loc.setText(loc_nm);
                    }
                }
            }catch (Exception e){}

            try{
                if(!arr_profile.get(0).getUser_thum().equals("")){
                    if(!arr_profile.get(0).getUser_thum().equals("0")){
                        try{
                            if(arr_profile.get(0).getUser_thum().length()>2){
                                progres_load_user.setVisibility(View.VISIBLE);

                                Uri uri = Uri.parse(arr_profile.get(0).getUser_thum());
//                            img_user.setImageURI(uri);

                                DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                                        ImageRequestBuilder.newBuilderWithSource(Uri.parse(arr_profile.get(0).getUser_thum())).
                                                setLocalThumbnailPreviewsEnabled(true)
                                                .setPostprocessor(new BasePostprocessor() {
                                                    @Override
                                                    public void process(Bitmap bitmap) {
                                                    }
                                                })
                                                .build())
                                        .setControllerListener(new BaseControllerListener<ImageInfo>() {
                                            @Override
                                            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                                                progres_load_user.setVisibility(View.GONE);
                                            }
                                            @Override
                                            public void onFailure(String id, Throwable throwable) {
                                                super.onFailure(id, throwable);
                                                progres_load_user.setVisibility(View.GONE);
                                            }
                                            @Override
                                            public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
//                                            progres_load_user.setVisibility(View.GONE);
                                            }
                                        })
                                        .build();
                                img_user.setController(controller);
                            }else{
                                progres_load_user.setVisibility(View.GONE);
                            }

                            user_img = arr_profile.get(0).getUser_thum();
                        }catch (Exception e){ progres_load_user.setVisibility(View.GONE);}
                    }else{
                        progres_load_user.setVisibility(View.GONE);
                    }
                }else{
                    progres_load_user.setVisibility(View.GONE);
                }
            }catch (Exception e){progres_load_user.setVisibility(View.GONE);}


            try{
                if(!arr_profile.get(0).getBusi_thum().equals("")){
                    if(!arr_profile.get(0).getBusi_thum().equals("0")){
                        if(arr_profile.get(0).getBusi_thum().length()>2){
                            progres_load_buss.setVisibility(View.VISIBLE);

                            Uri uri = Uri.parse(arr_profile.get(0).getBusi_thum());
                            img_buss_img.setImageURI(uri);

                            DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                                    ImageRequestBuilder.newBuilderWithSource(Uri.parse(arr_profile.get(0).getBusi_thum())).
                                    setLocalThumbnailPreviewsEnabled(true)
                                            .setPostprocessor(new BasePostprocessor() {
                                                @Override
                                                public void process(Bitmap bitmap) {
                                                }
                                            })
                                            .build())
                                    .setControllerListener(new BaseControllerListener<ImageInfo>() {
                                        @Override
                                        public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                                            progres_load_buss.setVisibility(View.GONE);
                                        }
                                        @Override
                                        public void onFailure(String id, Throwable throwable) {
                                            super.onFailure(id, throwable);
                                            progres_load_buss.setVisibility(View.GONE);
                                        }
                                        @Override
                                        public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                                            progres_load_buss.setVisibility(View.GONE);
                                        }
                                    })
                                    .build();
                            img_buss_img.setController(controller);
                        }else{
                            progres_load_buss.setVisibility(View.GONE);
                        }

                        buss_img = arr_profile.get(0).getBusi_thum();
                    }else{
                        progres_load_buss.setVisibility(View.GONE);

                    }
                }else{
                    progres_load_buss.setVisibility(View.GONE);

                }
            }catch (Exception e){progres_load_buss.setVisibility(View.GONE);}


            try{
                if(arr_profile.get(0).getCate_en().contains(",")){
                    String cate =  "";
                    if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")) {
                        cate = arr_profile.get(0).getCate_en().replace(",", "\n");
                    }else{
                        cate = arr_profile.get(0).getCate_heb().replace(",", "\n");
                    }
                    txt_cate.setVisibility(View.VISIBLE);
                    txt_cate.setText(cate);
                }else{
                    String cate =  "";
                    if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")) {
                        cate = arr_profile.get(0).getCate_en().replace(",", "\n");
                    }else{
                        cate = arr_profile.get(0).getCate_heb().replace(",", "\n");
                    }
                    txt_cate.setVisibility(View.VISIBLE);
                    txt_cate.setText(cate);
                }
            }catch (Exception e){}

        }catch (Exception e){}


    }

    @Override
    public void networkAvailable() {
        if(dialog_internet!=null){
            if(dialog_internet.isShowing()){
                dialog_internet.dismiss();
            }
        }
        if(arr_profile!=null){
            if(arr_profile.size()<1){
                api_OtherProfile();
            }
        }else{
            api_OtherProfile();
        }
    }

    @Override
    public void networkUnavailable() {
        dialog_msg_show(ProfileOther_Activity.this,getResources().getString(R.string.no_internet));
    }

    /*Dialog for Internet*/
    public  void dialog_msg_show( Activity activity, String msg){
        MainActivity.state_maintain = "1";
        dialog_internet = new BottomSheetDialog (activity);
        dialog_internet.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog_internet.setContentView(bottomSheetView);



        dialog_internet.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d     = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog_internet.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog_internet.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog_internet.findViewById(R.id.txt_ok);

        txt_msg.setText(msg);

        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog_internet.findViewById(R.id.rel_vw_save_details);

        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_internet.dismiss();
            }
        });
        try{
            if(dialog_internet!=null){
                if(!dialog_internet.isShowing()){
                    dialog_internet.show();

                }
            }
        }catch (Exception e){
        }
    }
}
