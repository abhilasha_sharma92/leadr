package leadr.com.leadr.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapter.CustomAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import modal.SpinnerModel;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

public class EnterPhoneActivity extends AppCompatActivity {

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.rel_phn)
    RelativeLayout rel_phn;

    @BindView(R.id.txt_phone)
    TextView txt_phone;

    @BindView(R.id.btn_next)
    Button btn_next;

    @BindView(R.id.edt_phn)
    EditText edt_phn;

    ArrayList<SpinnerModel> CustomListViewValuesArr = new ArrayList<SpinnerModel>();
    CustomAdapter adapter;

    GlobalConstant utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_phone);

        // bind the view using butterknife
        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);

        utils = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(EnterPhoneActivity.this);

        setListData();
        Resources res = getResources();
        adapter = new CustomAdapter(EnterPhoneActivity.this, R.layout.spinner_rows, CustomListViewValuesArr,res);
        spinner.setAdapter(adapter);

        // Listener called when spinner item selected
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                // Get selected row data to show on screen
                String Number    = ((TextView) v.findViewById(R.id.txt_row)).getText().toString();
                txt_phone.setText(Number);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        edt_phn.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() >= 10) {
                    btn_next.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    btn_next.setTag("1");
                }else{
                    btn_next.setBackgroundColor(getResources().getColor(R.color.colorBlueNonHighlight));
                    btn_next.setTag("0");
                }
            }
        });
    }


    @OnClick(R.id.rel_phn)
    public void onSpinnerClick() {
        spinner.setVisibility(View.VISIBLE);
       spinner.performClick();
    }

    @OnClick(R.id.btn_next)
    public void onNextClick() {
        if (btn_next.getTag().equals("1")) {
            if(utils.isNetworkAvailable(EnterPhoneActivity.this)){
                String phone = edt_phn.getText().toString();
                api_ValidatePhone(phone);
            }else {
                utils.dialog_msg_show(EnterPhoneActivity.this,getResources().getString(R.string.no_internet));
            }
        }else{
            utils.dialog_msg_show(EnterPhoneActivity.this,getResources().getString(R.string.enter_phone));
        }
    }


    /****** Function to set data in ArrayList *************/
    public void setListData()
    {

        // Now i have taken static values by loop.
        // For further inhancement we can take data by webservice / json / xml;

        for (int i = 0; i < 11; i++) {

            final SpinnerModel sched = new SpinnerModel();

            /******* Firstly take data in model object ******/
            sched.setNumber("+ "+i);

            /******** Take Model Object in ArrayList **********/
            CustomListViewValuesArr.add(sched);
        }

    }



    private void api_ValidatePhone(final String phone){
        AndroidNetworking.enableLogging();
        utils.showProgressDialog(this,"Loading ...");
        Log.e("url_val_phone: ", NetworkingData.BASE_URL+ NetworkingData.LOGIN);

        String dev_token = "";
        try{
            dev_token = FirebaseInstanceId.getInstance().getToken();
        }catch (Exception e){
            dev_token = sharedPreferenceLeadr.get_dev_token();
        }
        Log.e("dev_token: ",dev_token);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.LOGIN)
                .addBodyParameter("phone_number",phone)
                .addBodyParameter("device_token",dev_token)
                .addBodyParameter("device_type","android")
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_login: ",result+"");
                        utils.dismissProgressdialog();

                        try {
                            JSONArray arr = result.getJSONArray("details");
                            for(int i=0; i<arr.length(); i++){
                                JSONObject obj = arr.getJSONObject(i);
                                sharedPreferenceLeadr.setUserId(obj.getString("id"));
                                sharedPreferenceLeadr.set_phone(obj.getString("phone_number"));
                                sharedPreferenceLeadr.set_username(obj.getString("user_name"));
                                sharedPreferenceLeadr.setProfilePicUrl(obj.getString("user_image"));
                                sharedPreferenceLeadr.set_jobTitle(obj.getString("job_title"));
                                sharedPreferenceLeadr.set_bus_name(obj.getString("business_name"));
                                sharedPreferenceLeadr.set_bus_info(obj.getString("business_info"));
                                sharedPreferenceLeadr.set_bus_img(obj.getString("business_image"));
                                sharedPreferenceLeadr.set_bus_site(obj.getString("business_fb_page"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(result.optString("status").equals("1")){
                            Intent i_nxt = new Intent(EnterPhoneActivity.this, VerifyActivity.class);
                            i_nxt.putExtra("phone",phone);
                            i_nxt.putExtra("type","login");
                            startActivity(i_nxt);
//                            overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                        }else{
                            Intent i_nxt = new Intent(EnterPhoneActivity.this, VerifyActivity.class);
                            i_nxt.putExtra("phone",phone);
                            i_nxt.putExtra("type","reg");
                            startActivity(i_nxt);
//                            overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }

}

