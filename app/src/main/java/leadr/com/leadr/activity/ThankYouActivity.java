package leadr.com.leadr.activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;


public class ThankYouActivity extends AppCompatActivity {

    @BindView(R.id.txt_not_now)
    TextView txt_not_now;

    @BindView(R.id.txt_thank)
    TextView txt_thank;

    @BindView(R.id.txt_rate_us)
    TextView txt_rate_us;

    @BindView(R.id.txt_rateus)
    TextView txt_rateus;

    @BindView(R.id.txt_u_made)
    TextView txt_u_made;

    @BindView(R.id.txt_do_we)
    TextView txt_do_we;

    @BindView(R.id.btn_send)
    Button btn_send;

    @BindView(R.id.btn_no)
    Button btn_no;

    @BindView(R.id.btn_yes)
    Button btn_yes;

    @BindView(R.id.rel_1)
    RelativeLayout rel_1;

    @BindView(R.id.rel_2)
    RelativeLayout rel_2;


    @BindView(R.id.lnr_not_now)
    LinearLayout lnr_not_now;

    SharedPreferenceLeadr sharedPreferenceLeadr;
    GlobalConstant utils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_us);

        ButterKnife.bind(this);

        sharedPreferenceLeadr = new SharedPreferenceLeadr(ThankYouActivity.this);
        utils                 = new GlobalConstant();

        methodSetTypeface();
    }


    private void methodSetTypeface() {
        txt_not_now.setTypeface(utils.OpenSans_Regular(ThankYouActivity.this));
        txt_thank.setTypeface(utils.OpenSans_Regular(ThankYouActivity.this));
        txt_rate_us.setTypeface(utils.OpenSans_Regular(ThankYouActivity.this));
        btn_send.setTypeface(utils.OpenSans_Regular(ThankYouActivity.this));
        txt_rateus.setTypeface(utils.OpenSans_Regular(ThankYouActivity.this));
        txt_u_made.setTypeface(utils.OpenSans_Regular(ThankYouActivity.this));

        txt_rateus.setText(getResources().getString(R.string.thank));
        btn_send.setText(getResources().getString(R.string.close_caps));
        txt_u_made.setVisibility(View.GONE);
        txt_do_we.setVisibility(View.GONE);
        rel_1.setVisibility(View.GONE);
        txt_rate_us.setVisibility(View.GONE);
        txt_not_now.setVisibility(View.GONE);
        rel_2.setVisibility(View.VISIBLE);
    }



    @OnClick(R.id.lnr_not_now)
    public void onBack() {
        if(getIntent().getStringExtra("del")!=null){
            Intent i_nxt = new Intent(ThankYouActivity.this, GetStartedActivity.class);
            i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i_nxt);
            finish();
        }else{
            finish();
        }

    }


    @OnClick(R.id.btn_send)
    public void onSend() {
        if(getIntent().getStringExtra("del")!=null){
            Intent i_nxt = new Intent(ThankYouActivity.this, GetStartedActivity.class);
            i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i_nxt);
            finish();
        }else{
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if(getIntent().getStringExtra("del")!=null){
            Intent i_nxt = new Intent(ThankYouActivity.this, GetStartedActivity.class);
            i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i_nxt);
            finish();
        }else{
            super.onBackPressed();
        }

    }
}
