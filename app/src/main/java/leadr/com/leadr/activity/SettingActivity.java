package leadr.com.leadr.activity;

import android.Manifest;
import android.Manifest.permission;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.squareup.picasso.Picasso;
import com.zcw.togglebutton.ToggleButton;
import com.zcw.togglebutton.ToggleButton.OnToggleChanged;
import com.zhanglei.customizeview.SwitchView;

import org.json.JSONObject;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;
import vn.luongvo.widget.iosswitchview.SwitchView.OnCheckedChangeListener;


public class SettingActivity extends AppCompatActivity {

    @BindView(R.id.rel_logout)
    RelativeLayout rel_logout;

    @BindView(R.id.rel_del)
    RelativeLayout rel_del;

    @BindView(R.id.txt_sett)
    TextView txt_sett;

    @BindView(R.id.txt_chat)
    TextView txt_chat;

    @BindView(R.id.txt_notify)
    TextView txt_notify;

    @BindView(R.id.scroll)
    ScrollView scroll;

    @BindView(R.id.txt_sound)
    TextView txt_sound;

    @BindView(R.id.txt_tckt)
    TextView txt_tckt;

    @BindView(R.id.txt_sel)
    TextView txt_sel;

    @BindView(R.id.txt_widgt)
    TextView txt_widgt;

    @BindView(R.id.txt_unkwn)
    TextView txt_unkwn;

    @BindView(R.id.txt_dmns)
    TextView txt_dmns;

    @BindView(R.id.txt_priv)
    TextView txt_priv;

    @BindView(R.id.txt_legl)
    TextView txt_legl;

    @BindView(R.id.txt_priv_pol)
    TextView txt_priv_pol;

    @BindView(R.id.txt_trm)
    TextView txt_trm;

    @BindView(R.id.txt_support)
    TextView txt_support;

    @BindView(R.id.txt_email)
    TextView txt_email;

    @BindView(R.id.txt_acc)
    TextView txt_acc;

    @BindView(R.id.txt_logout)
    TextView txt_logout;

    @BindView(R.id.txt_del)
    TextView txt_del;

    @BindView(R.id.txt_ver)
    TextView txt_ver;

    @BindView(R.id.txt_ver_name)
    TextView txt_ver_name;

    @BindView(R.id.txt_online)
    TextView txt_online;

    @BindView(R.id.txt_sel_loc)
    TextView txt_sel_loc;

    @BindView(R.id.txt_hebrew)
    TextView txt_hebrew;

    @BindView(R.id.txt_nw_leads)
    TextView txt_nw_leads;

    @BindView(R.id.btn_online)
    Button btn_online;

    @BindView(R.id.btn_sel_loc)
    Button btn_sel_loc;

    @BindView(R.id.rel_email)
    RelativeLayout rel_email;

    @BindView(R.id.swtch_noti_chat)
    ToggleButton swtch_noti_chat;

    @BindView(R.id.swtch_sound_chat)
    ToggleButton swtch_sound_chat;

    @BindView(R.id.swtch_noti_tckt)
    ToggleButton swtch_noti_tckt;

    @BindView(R.id.swtch_sound_tckt)
    ToggleButton swtch_sound_tckt;

    @BindView(R.id.swtch_notify_sell)
    ToggleButton swtch_notify_sell;

    @BindView(R.id.swtch_sound_sell)
    ToggleButton swtch_sound_sell;

    @BindView(R.id.swtch_widget)
    ToggleButton swtch_widget;

    @BindView(R.id.swtch_hebrew)
    ToggleButton swtch_hebrew;

    @BindView(R.id.swtch_sound_newlead)
    ToggleButton swtch_sound_newlead;

    @BindView(R.id.swtch_notify_nw_lead)
    ToggleButton swtch_notify_nw_lead;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.rel_priv)
    RelativeLayout rel_priv;

    @BindView(R.id.rel_chng_lang)
    RelativeLayout rel_chng_lang;

    @BindView(R.id.txt_set_lang)
    TextView txt_set_lang;

    @BindView(R.id.edt_unkwn)
    EditText edt_unkwn;

    SharedPreferenceLeadr sharedPreferenceLeadr;
    GlobalConstant        utils;
    String                changed = "0";
    String                widget;
    public static String  WIDGET_CHANGE = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        ButterKnife.bind(this);

        sharedPreferenceLeadr = new SharedPreferenceLeadr(SettingActivity.this);
        utils                 = new GlobalConstant();

         methodSetTypeface();

         ///widget....
         widget = getIntent().getStringExtra("widget");
        if(widget!=null){
            focusOnView();
        }

        method_SetSHaredPref_Values();

        swtch_sound_sell.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(on){
                    if(sharedPreferenceLeadr.get_SELL_NOTIFY().equalsIgnoreCase("0")){
                        utils.dialog_msg_show(SettingActivity.this,getResources().getString(R.string.err_notify));
                        swtch_sound_sell.setToggleOff();
                    }else{
                        api_UpdateSELL_Sound("1");
                    }
                }else{
                    api_UpdateSELL_Sound("0");
                }
            }
        });

        swtch_notify_nw_lead.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(on){
                    api_UpdateNEWLead_Notify("1");
                }else{
                    api_UpdateNEWLead_Notify("0");
                }
            }
        });


        swtch_sound_newlead.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(on){
                    if(sharedPreferenceLeadr.get_NEWSELL_NOTIFY().equalsIgnoreCase("0")){
                        utils.dialog_msg_show(SettingActivity.this,getResources().getString(R.string.err_notify));
                        swtch_sound_newlead.setToggleOff();
                    }else{
                        api_UpdateNewLead_Sound("1");
                    }
                }else{
                    api_UpdateNewLead_Sound("0");
                }
            }
        });


        swtch_notify_sell.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(on){
                    api_UpdateSELL_Notify("1");
                }else{
                    api_UpdateSELL_Notify("0");
                }
            }
        });

        swtch_sound_tckt.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(on){
                    if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("0")){
                        utils.dialog_msg_show(SettingActivity.this,getResources().getString(R.string.err_notify));
                        swtch_sound_tckt.setToggleOff();
                    }else{
                        api_UpdateTICKET_SOUND("1");
                    }
                }else{
                    api_UpdateTICKET_SOUND("0");
                }
            }
        });

        swtch_noti_tckt.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(on){
                    api_UpdateTICKET_Notify("1");
                }else{
                    api_UpdateTICKET_Notify("0");
                }
            }
        });


        swtch_sound_chat.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(on){
                    if(sharedPreferenceLeadr.get_CHAT_NOTIFY().equalsIgnoreCase("0")){
                        utils.dialog_msg_show(SettingActivity.this,getResources().getString(R.string.err_notify));
                        swtch_sound_chat.setToggleOff();
                    }else{
                        api_UpdateChat_Sound("1");
                    }
                }else{
                    api_UpdateChat_Sound("0");
                }
            }
        });

        swtch_noti_chat.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(on){
                    api_UpdateChat_NOTI("1");
                }else{
                    api_UpdateChat_NOTI("0");
                }
            }
        });

        swtch_widget.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(on){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if(!utils.checkReadCallLogPermission(SettingActivity.this)){
                            ActivityCompat.requestPermissions(
                                    SettingActivity.this,
                                    new String[]{Manifest.permission.READ_CALL_LOG},
                                    3
                            );
                        }
                        else if(utils.checkReadContactPermission(SettingActivity.this)&&
                                utils.checkReadCallLogPermission(SettingActivity.this)&&
                                utils.checkPhoneStatePermission(SettingActivity.this)) {
                            WIDGET_CHANGE = null;
                            api_Update_WIDGET("1");
                        }else{
                            if (!utils.checkReadContactPermission(SettingActivity.this)) {
                                ActivityCompat.requestPermissions(
                                        SettingActivity.this,
                                        new String[]{Manifest.permission.READ_CONTACTS},
                                        2
                                );
                            }else{
                                ActivityCompat.requestPermissions(
                                        SettingActivity.this,
                                        new String[]{Manifest.permission.READ_PHONE_STATE},
                                        4
                                );
                            }
                        }
                    }

                }else{
                    WIDGET_CHANGE = null;
                    api_Update_WIDGET("0");
                }
            }
        });

        swtch_hebrew.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(on){
                    sharedPreferenceLeadr.set_LANGUAGE("it");
                    String languageToLoad  = "it";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                }else{
                    sharedPreferenceLeadr.set_LANGUAGE("en");
                    String languageToLoad  = "en";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getResources().updateConfiguration(config,getResources().getDisplayMetrics());
                }
                if(changed.equalsIgnoreCase("0")){
                    changed = "1";
//                    onBackPressed();
                }else{
                    changed = "0";
                }
                onBackPressed();
            }
        });

    }


    /***Previous default values get and set here***/
    private void method_SetSHaredPref_Values() {
        if(sharedPreferenceLeadr.get_CHAT_NOTIFY().equalsIgnoreCase("1")){
            swtch_noti_chat.setToggleOn();
        }else{
            swtch_noti_chat.setToggleOff();
        }

        if(sharedPreferenceLeadr.get_CHAT_SOUND().equalsIgnoreCase("1")){
            swtch_sound_chat.setToggleOn();
        }else{
            swtch_sound_chat.setToggleOff();
        }

        if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")){
            swtch_noti_tckt.setToggleOn();
        }else{
            swtch_noti_tckt.setToggleOff();
        }

        if(sharedPreferenceLeadr.get_TICKET_SOUND().equalsIgnoreCase("1")){
            swtch_sound_tckt.setToggleOn();
        }else{
            swtch_sound_tckt.setToggleOff();
        }

        if(sharedPreferenceLeadr.get_SELL_NOTIFY().equalsIgnoreCase("1")){
            swtch_notify_sell.setToggleOn();
        }else{
            swtch_notify_sell.setToggleOff();
        }

        if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
            swtch_sound_sell.setToggleOn();
        }else{
            swtch_sound_sell.setToggleOff();
        }

        if(sharedPreferenceLeadr.get_NEWSELL_NOTIFY().equalsIgnoreCase("1")){
            swtch_notify_nw_lead.setToggleOn();
        }else{
            swtch_notify_nw_lead.setToggleOff();
        }

        if(sharedPreferenceLeadr.get_NEWSELL_SOUND().equalsIgnoreCase("1")){
            swtch_sound_newlead.setToggleOn();
        }else{
            swtch_sound_newlead.setToggleOff();
        }

        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
            swtch_hebrew.setToggleOn();
            txt_set_lang.setText("עִברִית");
        }else{
            swtch_hebrew.setToggleOff();
            txt_set_lang.setText("English");
        }

        if(sharedPreferenceLeadr.get_WIDGET().equalsIgnoreCase("1")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(utils.checkReadContactPermission(SettingActivity.this) && utils.checkReadCallLogPermission(SettingActivity.this)
                        && ! utils.checkPhoneStatePermission(SettingActivity.this)){
                    swtch_widget.setToggleOff();
                    sharedPreferenceLeadr.set_WIDGET("0");
                }else{
                    swtch_widget.setToggleOn();
                }
            }else{
                swtch_widget.setToggleOn();
            }

        }else{
            swtch_widget.setToggleOff();
        }

        if(sharedPreferenceLeadr.get_DIMENSION().equalsIgnoreCase("0")){
            btn_online.setVisibility(View.GONE);
            txt_sel_loc.setVisibility(View.GONE);
            txt_online.setVisibility(View.VISIBLE);
            btn_sel_loc.setVisibility(View.VISIBLE);
        }else{
            btn_online.setVisibility(View.VISIBLE);
            txt_sel_loc.setVisibility(View.VISIBLE);
            txt_online.setVisibility(View.GONE);
            btn_sel_loc.setVisibility(View.GONE);
        }

        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")){
            LayoutParams params = new LayoutParams(
                    LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 0, 90, 0);
            txt_notify.setLayoutParams(params);
        }else{
            LayoutParams params = new LayoutParams(
                    LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 0, 120, 0);
            txt_notify.setLayoutParams(params);
        }
    }


    /***When intent is from widget to setting..scroll to this view...****/
    private final void focusOnView(){
        scroll.post(new Runnable() {
            @Override
            public void run() {
                scroll.scrollTo(0, txt_widgt.getTop());
            }
        });
    }


    /***Styling of all texts***/
    private void methodSetTypeface() {
        txt_sett.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_chat.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_notify.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_sound.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_tckt.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_sel.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_widgt.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_unkwn.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_dmns.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_priv.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_legl.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_priv_pol.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_trm.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_support.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_email.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_acc.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_logout.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_del.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_ver.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_ver_name.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_hebrew.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_set_lang.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_nw_leads.setTypeface(utils.OpenSans_Regular(SettingActivity.this));

    }


    /***Logout from application****/
    @OnClick(R.id.rel_logout)
    public void onLogout() {
        AccountKit.logOut();
        sharedPreferenceLeadr.clearPreference();
        String languageToLoad  = "en";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config,getResources().getDisplayMetrics());

        Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
        i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i_nxt);
        finish();
    }


    /***Navigate to privacy policy screen*/
    @OnClick(R.id.rel_priv)
    public void onPriv() {
        Intent i_phn_screen = new Intent(SettingActivity.this, TermsPolicyActivity.class);
        i_phn_screen.putExtra("type","policy");
        startActivity(i_phn_screen);
    }


    /***Open language change dialog****/
    @OnClick(R.id.rel_chng_lang)
    public void onChangLang() {
        dialog_changLang();
    }



    /****Change LANGUAGE DIALOG****/
    public  void dialog_changLang(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SettingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_chang_lang, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_title                 = (TextView) dialog.findViewById(R.id.txt_title);
        final TextView txt_eng             = (TextView) dialog.findViewById(R.id.txt_eng);
        final TextView txt_heb             = (TextView) dialog.findViewById(R.id.txt_heb);
        TextView txt_ok                    = (TextView) dialog.findViewById(R.id.txt_ok);
        RelativeLayout rel_eng             = (RelativeLayout) dialog.findViewById(R.id.rel_eng);
        RelativeLayout rel_heb             = (RelativeLayout) dialog.findViewById(R.id.rel_heb);
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        final ImageView img_tick_eng       = (ImageView) dialog.findViewById(R.id.img_tick_eng);
        final ImageView img_tick_heb       = (ImageView) dialog.findViewById(R.id.img_tick_heb);

        txt_title.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_eng.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_heb.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SettingActivity.this));

        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(changed.equalsIgnoreCase("1")) {
                    onBackPressed();
                }
            }
        });


        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")) {
//            img_tick_eng.setVisibility(View.VISIBLE);
//            img_tick_heb.setVisibility(View.INVISIBLE);
//            txt_eng.setPadding(20,0,0,0);

            img_tick_eng.setImageResource(R.drawable.tick_lang);
            img_tick_heb.setImageResource(R.drawable.gayab);
        }else{
//            img_tick_eng.setVisibility(View.INVISIBLE);
//            img_tick_heb.setVisibility(View.VISIBLE);
//            txt_eng.setPadding(50,0,0,0);
            img_tick_eng.setImageResource(R.drawable.gayab);
            img_tick_heb.setImageResource(R.drawable.tick_lang);
        }

        rel_eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                img_tick_eng.setVisibility(View.VISIBLE);
//                img_tick_heb.setVisibility(View.INVISIBLE);
//                txt_eng_start.setText("E N G L I S H");
                img_tick_eng.setImageResource(R.drawable.tick_lang);
                img_tick_heb.setImageResource(R.drawable.gayab);
                changed = "1";
                sharedPreferenceLeadr.set_LANGUAGE("en");
                String languageToLoad  = "en";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getResources().updateConfiguration(config,getResources().getDisplayMetrics());
                GlobalConstant.PAGINATION_IDS = "0";
                MainActivity.fragment_buy = null;

               onBackPressed();

            }
        });

        rel_heb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                img_tick_eng.setVisibility(View.INVISIBLE);
//                img_tick_heb.setVisibility(View.VISIBLE);
//                txt_eng_start.setText("עִברִית");
                img_tick_eng.setImageResource(R.drawable.gayab);
                img_tick_heb.setImageResource(R.drawable.tick_lang);

                changed = "1";
                sharedPreferenceLeadr.set_LANGUAGE("it");
                String languageToLoad  = "it";
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getResources().updateConfiguration(config,getResources().getDisplayMetrics());
                GlobalConstant.PAGINATION_IDS = "0";
                MainActivity.fragment_buy = null;

                onBackPressed();
            }
        });


        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                if(changed.equalsIgnoreCase("1")) {
                    onBackPressed();
                }
            }
        });
        dialog.show();
    }




    @OnClick(R.id.rel_terms)
    public void onTerms() {
        Intent i_phn_screen = new Intent(SettingActivity.this, TermsPolicyActivity.class);
        i_phn_screen.putExtra("type","service");
        startActivity(i_phn_screen);
    }


    @Override
    public void onBackPressed() {
       if(changed.equalsIgnoreCase("1")){
           Intent i = new Intent(SettingActivity.this,MainActivity.class);
          try{
              MainActivity.fragment_buy = null;
          }catch (Exception e){}
           i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
           startActivity(i);
           finish();
       }else{
           finish();
       }
    }

    @OnClick(R.id.img_back)
    public void onBack() {
        if(changed.equalsIgnoreCase("1")){
            Intent i = new Intent(SettingActivity.this,MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
        }else{
            finish();
        }
    }

    @OnClick(R.id.txt_online)
    public void onKmSelect() {
        btn_online.setVisibility(View.VISIBLE);
        txt_sel_loc.setVisibility(View.VISIBLE);
        txt_online.setVisibility(View.GONE);
        btn_sel_loc.setVisibility(View.GONE);
        api_Update_Dimension("1");
    }

    @OnClick(R.id.txt_sel_loc)
    public void onMiSelect() {
        btn_online.setVisibility(View.GONE);
        txt_sel_loc.setVisibility(View.GONE);
        txt_online.setVisibility(View.VISIBLE);
        btn_sel_loc.setVisibility(View.VISIBLE);
        api_Update_Dimension("0");
    }

    @OnClick(R.id.rel_del)
    public void onDel() {
        dialog_Del();
    }

    @OnClick(R.id.rel_email)
    public void onSendEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "support@beleadr.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Send feedback");
        startActivity(Intent.createChooser(emailIntent, null));
    }


    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        if(WIDGET_CHANGE!=null) {
            if (utils.checkReadContactPermission(SettingActivity.this) &&
                    utils.checkReadCallLogPermission(SettingActivity.this) &&
                    utils.checkPhoneStatePermission(SettingActivity.this)) {
                api_Update_WIDGET("1");
                WIDGET_CHANGE = null;
                swtch_widget.setToggleOn();
            } else {
                api_Update_WIDGET("0");
                WIDGET_CHANGE = null;
                swtch_widget.setToggleOff();
            }
        }
    }

    public  void dialog_Del(){
        final BottomSheetDialog dialog_per = new BottomSheetDialog(SettingActivity.this,R.style.Theme_TransparentD);
        dialog_per.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_del_acc, null);
        dialog_per.setContentView(bottomSheetView);

        dialog_per.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d     = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        TextView txt_dontgo    = (TextView)dialog_per.findViewById(R.id.txt_dontgo);
        TextView txt_u_sure    = (TextView)dialog_per.findViewById(R.id.txt_u_sure);
        TextView txt_del       = (TextView)dialog_per.findViewById(R.id.txt_del);
        Button btn_close       = (Button)dialog_per.findViewById(R.id.btn_close);
        RelativeLayout rel_del = (RelativeLayout)dialog_per.findViewById(R.id.rel_del);

        txt_dontgo.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_u_sure.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_del.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        btn_close.setTypeface(utils.OpenSans_Regular(SettingActivity.this));

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_per.dismiss();
            }
        });
        rel_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_per.dismiss();
                api_DelAcc();
            }
        });
        dialog_per.show();

    }

    private void api_DelAcc() {
        AndroidNetworking.enableLogging();
        utils.showProgressDialog(this, getResources().getString(R.string.load));
        Log.e("url_getCat: ", NetworkingData.BASE_URL + NetworkingData.DEL_ACC);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.DEL_ACC)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_delacc: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            String user_id = sharedPreferenceLeadr.getUserId();
                            AccountKit.logOut();
                            sharedPreferenceLeadr.clearPreference();
                            String languageToLoad  = "en";
                            Locale locale = new Locale(languageToLoad);
                            Locale.setDefault(locale);
                            Configuration config = new Configuration();
                            config.locale = locale;
                            getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                            Intent i_del = new Intent(SettingActivity.this,FeedbackActivity.class);
                            i_del.putExtra("user_id",user_id);
                            startActivity(i_del);
                            finish();
                        }else{
                            if(result.optString("message").equalsIgnoreCase("You have unsold leads!")){
                                dialog_ErrorDel(getResources().getString(R.string.u_must));
                            }
                            else if(result.optString("message").equalsIgnoreCase("Refund request is pending!")){
                                dialog_ErrorDel(getResources().getString(R.string.u_have));
                            }else{
                                dialog_ErrorDel(getResources().getString(R.string.u_with));
                            }

                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }

    private void api_UpdateChat_NOTI(final String type) {
        AndroidNetworking.enableLogging();
        Log.e("url_chat_noti: ", NetworkingData.BASE_URL + NetworkingData.CHAT_NOTI_);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());
        Log.e("type: ", type);

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.CHAT_NOTI_)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("chat_noti",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chat_noti: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_CHAT_NOTIFY(type);
                            if(type.equalsIgnoreCase("0")){
                                swtch_sound_chat.setToggleOff();
                                sharedPreferenceLeadr.set_CHAT_SOUND(type);
                                api_UpdateChat_Sound("0");
                            }
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }

    private void api_UpdateChat_Sound(final String type) {
        AndroidNetworking.enableLogging();
        Log.e("url_chat_sound: ", NetworkingData.BASE_URL + NetworkingData.CHAT_SOUND_);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.CHAT_SOUND_)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("chat_sound",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chat_sound: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_CHAT_SOUND(type);
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }

    private void api_UpdateTICKET_Notify(final String type) {
        AndroidNetworking.enableLogging();
        Log.e("url_tickt_notify: ", NetworkingData.BASE_URL + NetworkingData.TICKET_NOTIFY);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.TICKET_NOTIFY)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("new_ticket_noti",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chat_sound: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_TICKET_NOTIFY(type);
                            if(type.equalsIgnoreCase("0")){
                                swtch_sound_tckt.setToggleOff();
                                sharedPreferenceLeadr.set_TICKET_SOUND(type);
                                api_UpdateTICKET_SOUND("0");
                            }
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }

    private void api_UpdateTICKET_SOUND(final String type) {
        AndroidNetworking.enableLogging();
        Log.e("url_tickt_notify: ", NetworkingData.BASE_URL + NetworkingData.TICKET_SOUND);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.TICKET_SOUND)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("new_ticket_sound",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chat_sound: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_TICKET_SOUND(type);
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }

    private void api_UpdateSELL_Notify(final String type) {
        AndroidNetworking.enableLogging();
        Log.e("url_tickt_notify: ", NetworkingData.BASE_URL + NetworkingData.SELL_NOTIFY);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.SELL_NOTIFY)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("new_sell_noti",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chat_sound: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_SELL_NOTIFY(type);
                            if(type.equalsIgnoreCase("0")){
                                swtch_sound_sell.setToggleOff();
                                sharedPreferenceLeadr.set_SELL_SOUND(type);
                                api_UpdateSELL_Sound("0");
                            }
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }


    private void api_UpdateSELL_Sound(final String type) {
        AndroidNetworking.enableLogging();
        Log.e("url_tickt_notify: ", NetworkingData.BASE_URL + NetworkingData.SELL_SOUND);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.SELL_SOUND)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("new_sell_sound",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chat_sound: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_SELL_SOUND(type);
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }


    private void api_Update_WIDGET(final String type) {
        AndroidNetworking.enableLogging();
        Log.e("url_tickt_notify: ", NetworkingData.BASE_URL + NetworkingData.UPDATE_WIDGET);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.UPDATE_WIDGET)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("widget",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chat_sound: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_WIDGET(type);
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }


    private void api_UpdateNEWLead_Notify(final String type) {
        AndroidNetworking.enableLogging();
        Log.e("url_tickt_notify: ", NetworkingData.BASE_URL + NetworkingData.UPDATE_NEWLEAD_NOTIFY);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.UPDATE_NEWLEAD_NOTIFY)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("new_lead_noti",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chat_sound: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_NEWSELL_NOTIFY(type);
                            if(type.equalsIgnoreCase("0")){
                                swtch_sound_newlead.setToggleOff();
                                sharedPreferenceLeadr.set_NEWSELL_SOUND(type);
                                api_UpdateNewLead_Sound("0");
                            }
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }


    private void api_UpdateNewLead_Sound(final String type) {
        AndroidNetworking.enableLogging();
        Log.e("url_tickt_notify: ", NetworkingData.BASE_URL + NetworkingData.UPDATE_NEWLEAD_SOUND);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.UPDATE_NEWLEAD_SOUND)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("new_lead_sound",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chat_sound: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_NEWSELL_SOUND(type);
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }


    private void api_Update_Dimension(final String type) {
        AndroidNetworking.enableLogging();
        Log.e("url_dimen: ", NetworkingData.BASE_URL + NetworkingData.UPDATE_DIMENSION);
        Log.e("url_userid: ", sharedPreferenceLeadr.getUserId());
        Log.e("dimension_sys: ", type);

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.UPDATE_DIMENSION)
                .addBodyParameter("id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("(dimension_sys",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_dimension: ", result + "");
                        utils.dismissProgressdialog();
                        if (result.optString("status").equals("1")) {
                            //if 1 = km else miles
                            sharedPreferenceLeadr.set_DIMENSION(type);
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SettingActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                        sharedPreferenceLeadr.set_DIMENSION(type);
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }




    public  void dialog_ErrorDel(String header){
        final BottomSheetDialog dialog = new BottomSheetDialog (SettingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(header);

        txt_title.setVisibility(View.GONE);
        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_msg.setTypeface(utils.OpenSans_Regular(SettingActivity.this));

        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();


    }



    @Override
    public void onRequestPermissionsResult( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions) {
            if(requestCode == 2 ){
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    //denied
                    utils.hideKeyboard(SettingActivity.this);
                    api_Update_WIDGET("0");
                    dialog_denyOne_Call();
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        if(utils.checkReadContactPermission(SettingActivity.this)&&
                                utils.checkReadCallLogPermission(SettingActivity.this)&&
                                utils.checkPhoneStatePermission(SettingActivity.this)) {
                            api_Update_WIDGET("1");
                        }else if(!utils.checkReadCallLogPermission(SettingActivity.this)){
                            api_Update_WIDGET("0");
                            ActivityCompat.requestPermissions(
                                    SettingActivity.this,
                                    new String[]{Manifest.permission.READ_CALL_LOG},
                                    3
                            );
                        }else{
                            api_Update_WIDGET("0");
                            ActivityCompat.requestPermissions(
                                    SettingActivity.this,
                                    new String[]{Manifest.permission.READ_PHONE_STATE},
                                    4
                            );
                        }
                    } else {
                        api_Update_WIDGET("0");
                        utils.hideKeyboard(SettingActivity.this);
                        dialog_openStoragePer_call();
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        //do something here.
                    }
                }
            }else if(requestCode == 3 ){
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    //denied
                    swtch_widget.setToggleOff();
                    api_Update_WIDGET("0");
                    utils.hideKeyboard(SettingActivity.this);
                    dialog_denyOne_Phone();
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        if(utils.checkReadContactPermission(SettingActivity.this)&&
                                utils.checkReadCallLogPermission(SettingActivity.this)&&
                                utils.checkPhoneStatePermission(SettingActivity.this)) {
                            api_Update_WIDGET("1");
                        }else if(!utils.checkReadContactPermission(SettingActivity.this)){
                            api_Update_WIDGET("0");
                            ActivityCompat.requestPermissions(
                                    SettingActivity.this,
                                    new String[]{Manifest.permission.READ_CONTACTS},
                                    2
                            );
                        }else if(!utils.checkReadCallLogPermission(SettingActivity.this)){
                            api_Update_WIDGET("0");
                            ActivityCompat.requestPermissions(
                                    SettingActivity.this,
                                    new String[]{Manifest.permission.READ_CALL_LOG},
                                    2
                            );
                        }else{
                            api_Update_WIDGET("0");
                            ActivityCompat.requestPermissions(
                                    SettingActivity.this,
                                    new String[]{Manifest.permission.READ_PHONE_STATE},
                                    4
                            );
                        }
                    } else {
                        api_Update_WIDGET("0");
                        utils.hideKeyboard(SettingActivity.this);
                        if(!utils.checkReadCallPhonePermission(SettingActivity.this)){
                            dialog_openStoragePer_Phone();
                        }else{
                            dialog_openStoragePer_call();
                        }

                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        //do something here.
                    }
                }
            }else{
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    //denied
                    api_Update_WIDGET("0");
                    utils.hideKeyboard(SettingActivity.this);
                    dialog_denyOne_Call();
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        if(utils.checkReadContactPermission(SettingActivity.this)&&
                                utils.checkReadCallLogPermission(SettingActivity.this)&&
                                utils.checkPhoneStatePermission(SettingActivity.this)) {
                            api_Update_WIDGET("1");
                        }
                    } else {
                        api_Update_WIDGET("0");
                        utils.hideKeyboard(SettingActivity.this);
                        dialog_openStoragePer_call();
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        //do something here.
                    }
                }
            }
        }

    }


    public  void dialog_denyOne_Phone(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SettingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);

        txt_title.setText(getResources().getString(R.string.all_phn));
        txt_camera.setText(this.getResources().getString(R.string.text_with_allow_phone_widget));

        txt_title.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SettingActivity.this));

        WIDGET_CHANGE = null;
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swtch_widget.setToggleOff();
                dialog.dismiss();
                utils.hideKeyboard(SettingActivity.this);

            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                swtch_widget.setToggleOff();
            }
        });

    }


    public  void dialog_openStoragePer_Phone(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SettingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish = (Button) dialog.findViewById(R.id.btn_finish);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_press = (TextView) dialog.findViewById(R.id.txt_press);
        TextView txt_store = (TextView) dialog.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(SettingActivity.this));

        txt_title.setText(getResources().getString(R.string.all_phn));
        txt_store.setText(getResources().getString(R.string.turn3));

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                WIDGET_CHANGE = "";
                utils.hideKeyboard(SettingActivity.this);
                GlobalConstant.startInstalledAppDetailsActivity(SettingActivity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                swtch_widget.setToggleOff();
            }
        });
        try{
            dialog.show();
        }catch (Exception e){

        }


    }


    public  void dialog_denyOne_Call(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SettingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);

        txt_title.setText(getResources().getString(R.string.all_contct));
        txt_camera.setText(this.getResources().getString(R.string.text_with_bullet_call_widget));

        WIDGET_CHANGE = null;
        txt_title.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SettingActivity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                utils.hideKeyboard(SettingActivity.this);

            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                swtch_widget.setToggleOff();
            }
        });
        try{
            dialog.show();
        }catch (Exception e){

        }


    }

    public  void dialog_openStoragePer_call(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SettingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish   =  (Button) dialog.findViewById(R.id.btn_finish);
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_press  = (TextView) dialog.findViewById(R.id.txt_press);
        TextView txt_store  = (TextView) dialog.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(SettingActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(SettingActivity.this));

        txt_title.setText(getResources().getString(R.string.all_contct));
        txt_store.setText(getResources().getString(R.string.all_contct_three));

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                WIDGET_CHANGE = "";
                utils.hideKeyboard(SettingActivity.this);
                GlobalConstant.startInstalledAppDetailsActivity(SettingActivity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                swtch_widget.setToggleOff();
            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }


    }

}
