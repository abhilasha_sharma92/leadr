package leadr.com.leadr.activity;


import android.Manifest.permission;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.Constants;
import utils.GlobalConstant;
import utils.NetworkingData;
import utils.Util;


/**
 * Created by Admin on 2/20/2018.
 */

public class EditRegistration_6_Activity extends AppCompatActivity {

    @BindView(R.id.txt_fullname)
    TextView txt_fullname;

    @BindView(R.id.txt_detail)
    TextView txt_detail;

    @BindView(R.id.txt_job)
    TextView txt_job;

    @BindView(R.id.edt_jobtitle)
    EditText edt_jobtitle;

    @BindView(R.id.edt_fullname)
    EditText edt_fullname;

    @BindView(R.id.btn_nxt)
    Button btn_nxt;

    @BindView(R.id.btn_finish)
    Button btn_finish;

    @BindView(R.id.img_user)
    SimpleDraweeView img_user;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.img_back_top)
    ImageView img_back_top;

    @BindView(R.id.rel_edit)
    RelativeLayout rel_edit;

    @BindView(R.id.rel_reg)
    RelativeLayout rel_reg;

    @BindView(R.id.img_user_rotate)
    ImageView img_user_rotate;

    @BindView(R.id.img_user_cancel)
    ImageView img_user_cancel;

    @BindView(R.id.txt_ent_wrk)
    TextView txt_ent_wrk;

    @BindView(R.id.txt_email)
    TextView txt_email;

    @BindView(R.id.txt_em_again)
    TextView txt_em_again;

    @BindView(R.id.edt_wrk_email)
    EditText edt_wrk_email;

    @BindView(R.id.edt_wrk_again)
    EditText edt_wrk_again;

    @BindView(R.id.lnr_1)
    LinearLayout lnr_1;

    @BindView(R.id.lnr_5wrk)
    LinearLayout lnr_5wrk;

    @BindView(R.id.scroll_email)
    ScrollView scroll_email;

    GlobalConstant utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    String user_full = "";
    String user_thumb = "";

    String str_upload1 = null;
    String userChoosenTask = null;
    int rotation_user = 0;
    File file_image;

    int REQUEST_CAMERA = 3;
    int SELECT_FILE = 2;

    BottomSheetDialog dialog_setting;
    BottomSheetDialog dialog_deny_once;
    BottomSheetDialog dialog_camera;

    TransferObserver observerthumb_user;
    TransferObserver observerfull_user;
    private TransferUtility transferUtility;
    ProgressDialog progressDialog;
    private Util util;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_reg);

        ButterKnife.bind(this);

        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(EditRegistration_6_Activity.this);

        txt_fullname.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));
        txt_detail.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));
        txt_job.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));
        edt_jobtitle.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));
        edt_fullname.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));
        txt_ent_wrk.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));
        txt_email.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));
        txt_em_again.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));
        edt_wrk_email.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));
        edt_wrk_again.setTypeface(utils.OpenSans_Regular(EditRegistration_6_Activity.this));

        btn_nxt.setVisibility(View.GONE);
        btn_finish.setVisibility(View.VISIBLE);
        img_back.setVisibility(View.VISIBLE);

        util               = new Util();
        transferUtility    = util.getTransferUtility(this);

        btn_finish.setText(getResources().getString(R.string.save));

        edt_jobtitle.setText(sharedPreferenceLeadr.get_jobTitle());
        edt_fullname.setText(sharedPreferenceLeadr.get_username());
        FirebaseInstanceId.getInstance().getToken();

        addAstresikEmail();
        addAstresikEmailAgain();

        edt_wrk_again.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    scroll_email.fullScroll(ScrollView.FOCUS_DOWN);
                    edt_wrk_again.requestFocus();
                } else {
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        edt_wrk_email.setText(sharedPreferenceLeadr.get_EMAIL_REG());
        edt_wrk_email.setSelection(edt_wrk_email.getText().toString().length());
        edt_wrk_email.requestFocus();

        rel_edit.setVisibility(View.VISIBLE);
        rel_reg.setVisibility(View.GONE);
        img_back.setVisibility(View.GONE);

        lnr_1.setVisibility(View.GONE);
        lnr_5wrk.setVisibility(View.VISIBLE);
    }


    private void addAstresikEmail() {
        String simple = getResources().getString(R.string.wrk_email);
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();

        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        txt_email.setText(builder);
    }


    private void addAstresikEmailAgain() {
        String simple = getResources().getString(R.string.wrk_em_agn);
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();

        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        txt_em_again.setText(builder);
    }


    @OnClick(R.id.img_back)
    public void OnBack() {
            finish();
    }


    @OnClick(R.id.img_back_top)
    public void OnBack2() {
            finish();
    }



    @OnClick(R.id.btn_finish)
    public void OnSave() {
        if(edt_wrk_email.getText().toString().trim().length()>0){
            if(edt_wrk_again.getText().toString().trim().length()>0){
                if(edt_wrk_again.getText().toString().equalsIgnoreCase(edt_wrk_email.getText().toString())){
                    if (!utils.patternForEmailId.matcher(
                            edt_wrk_email.getText().toString()).matches()) {
                        utils.hideKeyboard(EditRegistration_6_Activity.this);
                       dialog_msg_show(EditRegistration_6_Activity.this,getResources().getString(R.string.enter_valid),edt_wrk_email);
                    }else{
                        api_Register();
                    }
                }else{
                    utils.hideKeyboard(EditRegistration_6_Activity.this);
                    dialog_msg_show(EditRegistration_6_Activity.this,getResources().getString(R.string.email_notmatch),edt_wrk_again);
                }
            }else{
                utils.hideKeyboard(EditRegistration_6_Activity.this);
                dialog_msg_show(EditRegistration_6_Activity.this,getResources().getString(R.string.entr_email_ag),edt_wrk_again);
            }
        }else{
            utils.hideKeyboard(EditRegistration_6_Activity.this);
            dialog_msg_show(EditRegistration_6_Activity.this,getResources().getString(R.string.entr_email),edt_wrk_email);
        }
    }



    private void api_Register() {
        AndroidNetworking.enableLogging();
        utils.dismissProgressdialog();
        utils.showProgressDialog(this, getResources().getString(R.string.load));

        String dev_token = "";
        try {
            dev_token = FirebaseInstanceId.getInstance().getToken();
        } catch (Exception e) {
            dev_token = sharedPreferenceLeadr.get_dev_token();
        }
//        phone = "+917696137554";

      /*  Log.e("url_signup: ", NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO);
        Log.e("job_title: ",  edt_jobtitle.getText().toString());
        Log.e("phone_number: ",  phone);
        Log.e("user_name: ",  edt_fullname.getText().toString());
        Log.e("business_name: ",  edt_buss_name.getText().toString());
        Log.e("business_fb_page: ",  edt_buss_page.getText().toString());
        Log.e("business_info: ",  edt_buss_info.getText().toString());
        Log.e("location_name: ",  loc);
        Log.e("lat: ",  lat);
        Log.e("lon: ",  long_);
        Log.e("category: ",  cate_sel_id);
        Log.e("user_image: ",  user_full);
        Log.e("user_image_thumb: ",  user_thumb);
        Log.e("business_image: ",  buss_full);
        Log.e("business_image_thumb: ",  buss_thumb);
        Log.e("device_token: ",  dev_token);
        Log.e("device_type: ",  "android");
        Log.e("id: ",  sharedPreferenceLeadr.get_UserId_Bfr());*/

        String loc_name = "0";
        String country_name = "0";
        String add_ = "0";
        if(!sharedPreferenceLeadr.get_LOC_NAME().equalsIgnoreCase("")||!sharedPreferenceLeadr.get_LOC_NAME().equalsIgnoreCase("0")){
            loc_name = sharedPreferenceLeadr.get_LOC_NAME();
            loc_name = GlobalConstant.ConvertToBase64(loc_name).trim();
            country_name = sharedPreferenceLeadr.get_LOC_NAME_COUNTRY();
            country_name = GlobalConstant.ConvertToBase64(country_name).trim();
            add_ = sharedPreferenceLeadr.get_ADDRESS();
            add_ = GlobalConstant.ConvertToBase64(add_).trim();
        }

        String job_title = sharedPreferenceLeadr.get_jobTitle();
        job_title = GlobalConstant.ConvertToBase64(job_title).trim();

        String usr_nm = sharedPreferenceLeadr.get_username();
        usr_nm = GlobalConstant.ConvertToBase64(usr_nm).trim();

        String bus_nm = sharedPreferenceLeadr.get_bus_name();
        bus_nm = GlobalConstant.ConvertToBase64(bus_nm).trim();

        String bus_info = sharedPreferenceLeadr.get_bus_info();
        bus_info = GlobalConstant.ConvertToBase64(bus_info).trim();

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO_VER2)
                .addBodyParameter("job_title", job_title)
                .addBodyParameter("id", sharedPreferenceLeadr.get_UserId_Bfr())
                .addBodyParameter("phone_number", sharedPreferenceLeadr.get_phone())
                .addBodyParameter("user_name",usr_nm )
                .addBodyParameter("business_name", bus_nm)
                .addBodyParameter("business_fb_page", sharedPreferenceLeadr.get_bus_site())
                .addBodyParameter("business_info", bus_info)
                .addBodyParameter("location_name", loc_name)
                .addBodyParameter("lat", sharedPreferenceLeadr.get_LATITUDE())
                .addBodyParameter("lon", sharedPreferenceLeadr.get_LONGITUDE())
                .addBodyParameter("category", sharedPreferenceLeadr.get_CATEGORY())
                .addBodyParameter("user_image", sharedPreferenceLeadr.getProfilePicUrl())
                .addBodyParameter("business_image", sharedPreferenceLeadr.get_bus_img())
                .addBodyParameter("device_token", dev_token)
                .addBodyParameter("device_type", "android")
                .addBodyParameter("country", country_name)
                .addBodyParameter("address", add_)
                .addBodyParameter("user_thum", sharedPreferenceLeadr.get_PROFILE_THUMB())
                .addBodyParameter("busi_thum", sharedPreferenceLeadr.get_BUSS_THUMB())
                .addBodyParameter("email", edt_wrk_again.getText().toString())

                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        Log.e("res: ",response.toString());
                        sharedPreferenceLeadr.setUserId(sharedPreferenceLeadr.get_UserId_Bfr());
                        if (response.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_EMAIL_REG(edt_wrk_email.getText().toString());

                            finish();
                        } else {
                            if(response.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(EditRegistration_6_Activity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }else{
                                utils.hideKeyboard(EditRegistration_6_Activity.this);
                                utils.dialog_msg_show(EditRegistration_6_Activity.this, response.optString("message"));
                            }

                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });

    }

    public  void dialog_msg_show(Activity activity, String msg, final EditText edt){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }


    }
}
