package leadr.com.leadr.activity;

import android.Manifest;
import android.Manifest.permission;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import adapter.BusinessCat_Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import leadr.com.leadr.R;
import modal.CategoryPojo;
import modal.LatlngPojo;
import pref.SharedPreferenceLeadr;
import utils.Constants;
import utils.GlobalConstant;
import utils.ImageSurfaceView;
import utils.NetworkingData;
import utils.PermissionUtil;
import utils.UploadThumbAws;
import utils.UploadThumbAwsBuss;
import utils.UploadThumbBuss;
import utils.UploadThumbUser;
import utils.Util;

import static utils.GlobalConstant.FILE_NAME1;
import static utils.GlobalConstant.startInstalledAppDetailsActivity;

public class RegistrationActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    @BindView(R.id.txt_fullname)
    TextView txt_fullname;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.img_back2)
    ImageView img_back2;

    @BindView(R.id.img_rotate_buss)
    ImageView img_user_rotate_buss;

    @BindView(R.id.img_user_rotate)
    ImageView img_user_rotate;

    @BindView(R.id.img_user_cancel)
    ImageView img_user_cancel;

    @BindView(R.id.img_buss_cancel)
    ImageView img_buss_cancel;

    @BindView(R.id.img_user)
    SimpleDraweeView img_user;

    @BindView(R.id.img_buss)
    SimpleDraweeView img_buss;

    @BindView(R.id.btn_nxt)
    Button btn_nxt;


    @BindView(R.id.btn_nxt_2)
    Button btn_nxt_2;

    @BindView(R.id.btn_finish)
    Button btn_finish;

    @BindView(R.id.btn_finish_2)
    Button btn_finish_2;

    @BindView(R.id.edt_fullname)
    EditText edt_fullname;

    @BindView(R.id.edt_buss_name)
    EditText edt_buss_name;

    @BindView(R.id.edt_buss_page)
    EditText edt_buss_page;


    @BindView(R.id.edt_buss_info)
    EditText edt_buss_info;

    @BindView(R.id.edt_jobtitle)
    EditText edt_jobtitle;

    @BindView(R.id.edt_search)
    EditText edt_search;

    @BindView(R.id.lnr_1)
    LinearLayout lnr_1;

    @BindView(R.id.lnr_2)
    LinearLayout lnr_2;

    @BindView(R.id.lnr_3)
    LinearLayout lnr_3;

    @BindView(R.id.lnr_bottom_3)
    LinearLayout lnr_bottom_3;

    @BindView(R.id.lnr_bottom_3_2)
    LinearLayout lnr_bottom_3_2;

    @BindView(R.id.lnr_4)
    LinearLayout lnr_4;

    @BindView(R.id.lnr_5wrk)
    LinearLayout lnr_5wrk;

    @BindView(R.id.txt_businessname)
    TextView txt_businessname;

    @BindView(R.id.txt_two)
    TextView txt_two;

    @BindView(R.id.recycl_cat)
    RecyclerView recycl_cat;

    @BindView(R.id.recycl_loc)
    RecyclerView recycl_loc;

    @BindView(R.id.txt_three)
    TextView txt_three;

    @BindView(R.id.txt_four)
    TextView txt_four;

    @BindView(R.id.txt_five)
    TextView txt_five;

    @BindView(R.id.txt_loc_4)
    TextView txt_loc_4;

    @BindView(R.id.txt_cat_sel)
    TextView txt_cat_sel;

    @BindView(R.id.txt_cat_sel_2)
    TextView txt_cat_sel_2;

    @BindView(R.id.rel_4)
    RelativeLayout rel_4;

    @BindView(R.id.rel_main)
    RelativeLayout rel_main;

    @BindView(R.id.rel_bottom2)
    RelativeLayout rel_bottom2;

    @BindView(R.id.rel_add_loc)
    RelativeLayout rel_add_loc;

    @BindView(R.id.rel_final)
    RelativeLayout rel_final;


    @BindView(R.id.rel_add_loc1)
    RelativeLayout rel_add_loc1;

    @BindView(R.id.rel_bottom)
    RelativeLayout rel_bottom;


    @BindView(R.id.lnr_5)
    LinearLayout lnr_5;

    @BindView(R.id.card_full)
    CardView card_full;

    @BindView(R.id.card_full_last)
    CardView card_full_last;

    @BindView(R.id.card_full2)
    CardView card_full2;

    @BindView(R.id.vw_shadow)
    View vw_shadow;

    @BindView(R.id.txt_detail)
    TextView txt_detail;

    @BindView(R.id.txt_locc_an)
    TextView txt_locc_an;

    @BindView(R.id.txt_empty)
    TextView txt_empty;

    @BindView(R.id.txt_add_lc)
    TextView txt_add_lc;

    @BindView(R.id.txt_job)
    TextView txt_job;

    @BindView(R.id.txt_busines)
    TextView txt_busines;

    @BindView(R.id.txt_bus_sit)
    TextView txt_bus_sit;

    @BindView(R.id.bus_infoo)
    TextView bus_infoo;

    @BindView(R.id.txt_bus_cc)
    TextView txt_bus_cc;

    @BindView(R.id.txt_ent_wrk)
    TextView txt_ent_wrk;

    @BindView(R.id.txt_email)
    TextView txt_email;

    @BindView(R.id.txt_em_again)
    TextView txt_em_again;

    @BindView(R.id.edt_wrk_email)
    EditText edt_wrk_email;

    @BindView(R.id.edt_wrk_again)
    EditText edt_wrk_again;

    @BindView(R.id.scroll_email)
    ScrollView scroll_email;

    int STEP = 1;
    int REQUEST_CAMERA = 3;
    int SELECT_FILE = 2;
    int sel = 0;
    int rotation_user = 0;
    int rotation_buss = 0;

    GlobalConstant      utils;
    BusinessCat_Adapter adapter_cat;
    LocRegister_Adapter adapter_loc;


    private static final int GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient mGoogleApiClient;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private Double latitude = 0.0, longitude = 0.0;
    protected Location mCurrentLocation;
    String userChoosenTask = "";
    String cate_sel = "";
    String cate_sel_id = "";
    String cate_sel_heb = "";
    String phone = "";
    String str_upload1 = null;
    String str_upload2 = null;
    String sel_type = "user";
    String get_cityName, getCountryName;
    String user_thumb = "";
    String buss_thumb = "";
    String buss_full = "";
    String user_full = "";
    File file_image;
    File file_image_buss;

    ArrayList<LatlngPojo> arr_place = new ArrayList<>();
    ArrayList<CategoryPojo> arr_cat = new ArrayList<>();
    ArrayList<CategoryPojo> arr_cat_checked = new ArrayList<>();

    SharedPreferenceLeadr sharedPreferenceLeadr;

    Camera mCamera;
    SurfaceView mPreview;
     BottomSheetDialog dialog;
     BottomSheetDialog dialog_buss;
     BottomSheetDialog dialog_denyonce;
     BottomSheetDialog dialog_settng;

    TransferObserver observerthumb_user;
    TransferObserver observerfull_user;
    TransferObserver observerthumb_buss;
    TransferObserver observerfull_buss;

    private TransferUtility transferUtility;
    ProgressDialog progressDialog;
    private Util   util;

    private ImageSurfaceView mImageSurfaceView;
    private Camera camera;
    private File compressedImage_user;
    private File compressedImage_buss;
    private File compressedImage_buss_thumb;
    private File compressedImage_user_thumb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        // bind the view using butterknife
        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);

        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(RegistrationActivity.this);
        mGoogleApiClient = new GoogleApiClient.Builder(RegistrationActivity.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();


        phone              = getIntent().getStringExtra("phone");
        util               = new Util();
        transferUtility    = util.getTransferUtility(this);

        addAstresik();
        addAstresikBusiness();
        addAstresikEmail();
        addAstresikEmailAgain();

        if (utils.isNetworkAvailable(RegistrationActivity.this)) {
            api_GetCategory();
        } else {
            utils.hideKeyboard(RegistrationActivity.this);
            utils.dialog_msg_show(RegistrationActivity.this, getResources().getString(R.string.no_internet));
        }

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (adapter_cat != null) {
                    adapter_cat.getFilter().filter(cs);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        edt_wrk_again.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    scroll_email.fullScroll(ScrollView.FOCUS_DOWN);
                    edt_wrk_again.requestFocus();
                } else {
                }
            }
        });


        methodTypeface();

        txt_cat_sel.setSelected(true);
        if(sharedPreferenceLeadr.get_REG_START().equalsIgnoreCase("00:00")){
            sharedPreferenceLeadr.set_REG_START(CurrentTimeStart());
        }

        if(!sharedPreferenceLeadr.get_LANGUAGE().equals("en")){
            img_user.setImageResource(R.drawable.addimage_he);
            img_buss.setImageResource(R.drawable.addimage_he);
        }else{
            img_user.setImageResource(R.drawable.addimage);
            img_buss.setImageResource(R.drawable.addimage);
        }
    }


    void methodTypeface(){
        txt_detail.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_ent_wrk.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        btn_nxt.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_fullname.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_em_again.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_email.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        edt_fullname.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        edt_wrk_email.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_job.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        edt_jobtitle.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        edt_wrk_again.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_busines.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_businessname.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        edt_buss_name.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_bus_sit.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        edt_buss_page.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        bus_infoo.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        edt_buss_info.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        edt_search.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_bus_cc.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_cat_sel.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_locc_an.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_empty.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_add_lc.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_loc_4.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en"))
        {
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());

        }else{
            String languageToLoad  = "it";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }

        if(STEP==4 ){
            utils.hideKeyboard(RegistrationActivity.this);
        }
        if(STEP == 1 || STEP == 2|| STEP ==5){
            if(sharedPreferenceLeadr.get_PER().equalsIgnoreCase("1")){
                sharedPreferenceLeadr.set_PER("0");
                if(STEP == 1){
                    sel_type = "user";
                    if (utils.checkReadExternalPermission(RegistrationActivity.this)) {
                        utils.hideKeyboard(RegistrationActivity.this);
                        dialog_sel_image_user();
                    }
                }else{
                    sel_type = "buss";
                    if (utils.checkReadExternalPermission(RegistrationActivity.this)) {
                        utils.hideKeyboard(RegistrationActivity.this);
                        dialog_sel_image_buss();
                    }
                }
            }
        }
    }

    @OnClick(R.id.img_back)
    public void onBack() {
        Onback();
    }

    @OnClick(R.id.img_back2)
    public void onBack2() {
        Onback();
    }


    @OnClick(R.id.img_user_cancel)
    public void onUserImgRemove() {
        if(str_upload1!=null) {
            img_user.setImageDrawable(getResources().getDrawable(R.drawable.addimage));
            img_user.setRotation(0);
            str_upload1 = null;
            sel_type = "user";
            sel = 0;
            img_user_cancel.setVisibility(View.GONE);
            img_user_rotate.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.img_buss_cancel)
    public void onBussImgRemove() {
        if(str_upload2!=null) {
            img_buss.setImageDrawable(getResources().getDrawable(R.drawable.addimage));
            img_buss.setRotation(0);
            str_upload2 = null;
            sel_type = "user";
            sel = 0;
            img_buss_cancel.setVisibility(View.GONE);
            img_user_rotate_buss.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.img_user_rotate)
    public void onUserImgRotate() {
        if(str_upload1!=null) {
            if(rotation_user==0){
                rotation_user = 90;
            }else if(rotation_user==90){
                rotation_user = 180;
            }else if(rotation_user==180){
                rotation_user = 270;
            }else if(rotation_user==270){
                rotation_user = 360;
            }else{
                rotation_user = 0;
            }
            img_user.setRotation(rotation_user);
        }
    }

    @OnClick(R.id.img_rotate_buss)
    public void onBussImgRotate() {
        if(str_upload2!=null) {
            if(rotation_buss==0){
                rotation_buss = 90;
            }else if(rotation_buss==90){
                rotation_buss = 180;
            }else if(rotation_buss==180){
                rotation_buss = 270;
            }else if(rotation_buss==270){
                rotation_buss = 360;
            }else{
                rotation_buss = 0;
            }
            img_buss.setRotation(rotation_buss);
        }
    }


/***Change UI at back press****/
    void Onback(){
        if (STEP == 1) {
//            onBackPressed();
        } else if (STEP == 2) {
            utils.hideKeyboard(RegistrationActivity.this);
            STEP = 1;
            lnr_2.setVisibility(View.GONE);
            card_full_last.setVisibility(View.GONE);
            lnr_bottom_3.setVisibility(View.GONE);
            lnr_bottom_3_2.setVisibility(View.GONE);
            img_back.setVisibility(View.GONE);
            lnr_1.setVisibility(View.VISIBLE);
            txt_two.setBackground(getResources().getDrawable(R.drawable.two_in));
        } else if (STEP == 3) {
            utils.hideKeyboard(RegistrationActivity.this);
            STEP = 2;
            lnr_3.setVisibility(View.GONE);
            lnr_bottom_3.setVisibility(View.GONE);
            lnr_bottom_3_2.setVisibility(View.GONE);
            card_full_last.setVisibility(View.GONE);
            lnr_2.setVisibility(View.VISIBLE);
            img_back.setVisibility(View.VISIBLE);
            txt_three.setBackground(getResources().getDrawable(R.drawable.three_in));
            card_full.setVisibility(View.VISIBLE);
            card_full2.setVisibility(View.GONE);
            vw_shadow.setVisibility(View.GONE);
            rel_bottom2.setVisibility(View.GONE);
            rel_bottom.setVisibility(View.VISIBLE);
        } else if (STEP == 4) {
            utils.hideKeyboard(RegistrationActivity.this);
            STEP = 3;
            txt_four.setBackground(getResources().getDrawable(R.drawable.four_in));
            txt_five.setBackground(getResources().getDrawable(R.drawable.five_in));

            rel_main.setBackground(getResources().getDrawable(R.color.colorGraybg));

            card_full.setVisibility(View.GONE);
            card_full_last.setVisibility(View.GONE);
            card_full2.setVisibility(View.VISIBLE);
            vw_shadow.setVisibility(View.VISIBLE);
            rel_bottom2.setVisibility(View.GONE);
            lnr_5wrk.setVisibility(View.GONE);
            rel_bottom.setVisibility(View.VISIBLE);

            lnr_bottom_3.setVisibility(View.VISIBLE);
            lnr_bottom_3_2.setVisibility(View.VISIBLE);
            lnr_3.setVisibility(View.VISIBLE);
            lnr_4.setVisibility(View.GONE);
            rel_final.setVisibility(View.GONE);
            lnr_5.setVisibility(View.GONE);
            btn_finish.setVisibility(View.GONE);
            txt_loc_4.setVisibility(View.GONE);
            btn_finish.setVisibility(View.GONE);
            btn_finish_2.setVisibility(View.GONE);
            card_full.setVisibility(View.VISIBLE);
            btn_nxt.setVisibility(View.VISIBLE);
            btn_nxt_2.setVisibility(View.VISIBLE);
        } else if (STEP == 5) {
            utils.hideKeyboard(RegistrationActivity.this);
            STEP = 4;
            txt_four.setBackground(getResources().getDrawable(R.drawable.four_ac));
            txt_five.setBackground(getResources().getDrawable(R.drawable.five_in));

            rel_main.setBackground(getResources().getDrawable(R.color.colorWhiteLight));

            lnr_bottom_3.setVisibility(View.GONE);
            lnr_bottom_3_2.setVisibility(View.GONE);
            card_full_last.setVisibility(View.GONE);
            card_full.setVisibility(View.VISIBLE);
            card_full2.setVisibility(View.GONE);
            vw_shadow.setVisibility(View.GONE);
            lnr_3.setVisibility(View.GONE);
            recycl_loc.setVisibility(View.GONE);
            card_full.setVisibility(View.GONE);
            lnr_4.setVisibility(View.GONE);
            btn_finish.setVisibility(View.GONE);
            btn_finish_2.setVisibility(View.GONE);
            txt_loc_4.setVisibility(View.VISIBLE);
            lnr_5.setVisibility(View.VISIBLE);
            btn_nxt.setVisibility(View.VISIBLE);
            btn_nxt_2.setVisibility(View.VISIBLE);

            rel_bottom2.setVisibility(View.GONE);
            rel_bottom.setVisibility(View.VISIBLE);
            rel_add_loc1.setVisibility(View.VISIBLE);
            rel_final.setVisibility(View.VISIBLE);
//                arr_place.clear();
            if (arr_place.size() > 0) {
                rel_add_loc1.setVisibility(View.GONE);

                rel_4.setVisibility(View.VISIBLE);
                recycl_loc.setVisibility(View.VISIBLE);
                lnr_4.setVisibility(View.VISIBLE);
                txt_loc_4.setVisibility(View.VISIBLE);
                rel_final.setVisibility(View.GONE);
                lnr_5.setVisibility(View.GONE);

                recycl_loc.setVisibility(View.VISIBLE);
                LinearLayoutManager lnr_album = new LinearLayoutManager(RegistrationActivity.this, LinearLayoutManager.VERTICAL, false);
                recycl_loc.setLayoutManager(lnr_album);
                adapter_loc = new LocRegister_Adapter(RegistrationActivity.this, arr_place);
                recycl_loc.setAdapter(adapter_loc);
            }
        }
    }



    void OnNext(){
        if (STEP == 1) {
            if (edt_fullname.getText().toString().trim().length() < 1) {
                utils.hideKeyboard(RegistrationActivity.this);
                dialog_msg_show(RegistrationActivity.this, getResources().getString(R.string.enter_name),edt_fullname);
            } else {
//                utils.hideKeyboard(RegistrationActivity.this);
                edt_buss_name.requestFocus();
                try{
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(edt_buss_name, InputMethodManager.SHOW_IMPLICIT);
                }catch (Exception e){}
                STEP = 2;
                lnr_1.setVisibility(View.GONE);
                card_full_last.setVisibility(View.GONE);
                lnr_2.setVisibility(View.VISIBLE);
                img_back.setVisibility(View.VISIBLE);
                txt_two.setBackground(getResources().getDrawable(R.drawable.two_ac));
            }
        } else if (STEP == 2) {
            utils.hideKeyboard(RegistrationActivity.this);

            if (edt_buss_name.getText().toString().trim().length() < 1) {
                utils.hideKeyboard(RegistrationActivity.this);
                utils.hideKeyboard(RegistrationActivity.this);
                dialog_msg_show(RegistrationActivity.this, getResources().getString(R.string.enter_buss_name),edt_buss_name);
            } else {
                STEP = 3;
                lnr_3.setVisibility(View.VISIBLE);
                lnr_bottom_3.setVisibility(View.VISIBLE);
                lnr_bottom_3_2.setVisibility(View.VISIBLE);
                lnr_2.setVisibility(View.GONE);
                card_full.setVisibility(View.GONE);
                card_full2.setVisibility(View.VISIBLE);
                vw_shadow.setVisibility(View.VISIBLE);
                card_full_last.setVisibility(View.GONE);
                rel_bottom2.setVisibility(View.GONE);
                rel_bottom.setVisibility(View.VISIBLE);
                txt_three.setBackground(getResources().getDrawable(R.drawable.three_ac));

                if (!cate_sel.equalsIgnoreCase("0")) {
                    if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                        txt_cat_sel.setText(cate_sel_heb);
                        txt_cat_sel_2.setText(cate_sel_heb);
                    }else{
                        txt_cat_sel.setText(cate_sel);
                        txt_cat_sel_2.setText(cate_sel);
                    }
                }


                LinearLayoutManager lnr_album = new LinearLayoutManager(RegistrationActivity.this, LinearLayoutManager.VERTICAL, false);
                recycl_cat.setLayoutManager(lnr_album);
                adapter_cat = new BusinessCat_Adapter(RegistrationActivity.this, arr_cat, new BusinessCat_Adapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int i) {
                        utils.hideKeyboard(RegistrationActivity.this);
                        if (arr_cat.get(i).getChecked()) {
                            BusinessCat_Adapter.arr_cat_filter.get(i).setChecked(false);
                            adapter_cat.notifyDataSetChanged();
                        } else {
                            BusinessCat_Adapter.arr_cat_filter.get(i).setChecked(true);
                            adapter_cat.notifyDataSetChanged();
                        }
                        arr_cat_checked.clear();
                        cate_sel = "";
                        cate_sel_id = "";
                        cate_sel_heb = "";
                        for (int j = 0; j < BusinessCat_Adapter.arr_cat_filter.size(); j++) {
                            if (BusinessCat_Adapter.arr_cat_filter.get(j).getChecked()) {
                                arr_cat_checked.add(BusinessCat_Adapter.arr_cat_filter.get(j));
                                if (cate_sel.trim().length() > 1) {
                                    cate_sel = cate_sel + "," + BusinessCat_Adapter.arr_cat_filter.get(j).getCategory();
                                    cate_sel_id = cate_sel_id + "," + BusinessCat_Adapter.arr_cat_filter.get(j).getId();
                                    cate_sel_heb = cate_sel_heb + "," + BusinessCat_Adapter.arr_cat_filter.get(j).getHebrew();
                                } else {
                                    cate_sel = BusinessCat_Adapter.arr_cat_filter.get(j).getCategory();
                                    cate_sel_id = BusinessCat_Adapter.arr_cat_filter.get(j).getId();
                                    cate_sel_heb = BusinessCat_Adapter.arr_cat_filter.get(j).getHebrew();
                                }

                            }
                            if (!cate_sel.equalsIgnoreCase("0")) {
                                if (sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")) {
                                    txt_cat_sel.setText(cate_sel);
                                    txt_cat_sel_2.setText(cate_sel);
                                } else {
                                    txt_cat_sel.setText(cate_sel_heb);
                                    txt_cat_sel_2.setText(cate_sel_heb);
                                }
                            }
                        }
                    }
                });
                recycl_cat.setAdapter(adapter_cat);
            }

        } else if (STEP == 3) {
            if(cate_sel.equals("")){
                utils.hideKeyboard(RegistrationActivity.this);
                utils.dialog_msg_show(RegistrationActivity.this,getResources().getString(R.string.enter_buss_cat));
            }else{
                utils.hideKeyboard(RegistrationActivity.this);
                STEP = 4;
                txt_four.setBackground(getResources().getDrawable(R.drawable.four_ac));
//                txt_five.setBackground(getResources().getDrawable(R.drawable.five_ac));

                rel_main.setBackground(getResources().getDrawable(R.color.colorWhiteLight));

                lnr_bottom_3.setVisibility(View.GONE);
                lnr_bottom_3_2.setVisibility(View.GONE);
                card_full.setVisibility(View.VISIBLE);
                card_full2.setVisibility(View.GONE);
                vw_shadow.setVisibility(View.GONE);
                lnr_3.setVisibility(View.GONE);
                recycl_loc.setVisibility(View.GONE);
                card_full.setVisibility(View.GONE);
                lnr_4.setVisibility(View.GONE);
                btn_finish.setVisibility(View.GONE);
                btn_finish_2.setVisibility(View.GONE);
                txt_loc_4.setVisibility(View.VISIBLE);
                lnr_5.setVisibility(View.VISIBLE);
                btn_nxt.setVisibility(View.VISIBLE);
                btn_nxt_2.setVisibility(View.VISIBLE);

                rel_bottom2.setVisibility(View.GONE);
                card_full_last.setVisibility(View.GONE);
                rel_bottom.setVisibility(View.VISIBLE);
                rel_add_loc1.setVisibility(View.VISIBLE);
                rel_final.setVisibility(View.VISIBLE);
//                arr_place.clear();
                if (arr_place.size() > 0) {
                    rel_add_loc1.setVisibility(View.GONE);

                    rel_4.setVisibility(View.VISIBLE);
                    recycl_loc.setVisibility(View.VISIBLE);
                    lnr_4.setVisibility(View.VISIBLE);
                    txt_loc_4.setVisibility(View.VISIBLE);
                    rel_final.setVisibility(View.GONE);
                    lnr_5.setVisibility(View.GONE);

                    recycl_loc.setVisibility(View.VISIBLE);
                    LinearLayoutManager lnr_album = new LinearLayoutManager(RegistrationActivity.this, LinearLayoutManager.VERTICAL, false);
                    recycl_loc.setLayoutManager(lnr_album);
                    adapter_loc = new LocRegister_Adapter(RegistrationActivity.this, arr_place);
                    recycl_loc.setAdapter(adapter_loc);
                }

            }
        } else if (STEP == 4) {
            edt_wrk_email.requestFocus();
            try{
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(edt_wrk_email, InputMethodManager.SHOW_IMPLICIT);
            }catch (Exception e){}
            STEP = 5;
            txt_four.setBackground(getResources().getDrawable(R.drawable.four_ac));
            txt_five.setBackground(getResources().getDrawable(R.drawable.five_ac));

            rel_main.setBackground(getResources().getDrawable(R.color.colorGraybg));

            lnr_bottom_3.setVisibility(View.GONE);
            lnr_1.setVisibility(View.GONE);
            lnr_2.setVisibility(View.GONE);
            lnr_4.setVisibility(View.GONE);
            lnr_5.setVisibility(View.GONE);
            img_back.setVisibility(View.VISIBLE);
            btn_nxt.setVisibility(View.GONE);
            lnr_5wrk.setVisibility(View.VISIBLE);
            lnr_bottom_3_2.setVisibility(View.GONE);
            card_full.setVisibility(View.VISIBLE);
            card_full2.setVisibility(View.GONE);
            vw_shadow.setVisibility(View.GONE);
            lnr_3.setVisibility(View.GONE);
            recycl_loc.setVisibility(View.GONE);
            lnr_4.setVisibility(View.GONE);
            txt_loc_4.setVisibility(View.GONE);
            lnr_5.setVisibility(View.GONE);
            btn_nxt.setVisibility(View.GONE);
            btn_nxt_2.setVisibility(View.GONE);

            rel_bottom2.setVisibility(View.GONE);
            rel_bottom.setVisibility(View.VISIBLE);
            btn_finish.setVisibility(View.VISIBLE);
            rel_add_loc1.setVisibility(View.GONE);
            rel_final.setVisibility(View.GONE);
            card_full_last.setVisibility(View.VISIBLE);
            card_full.setVisibility(View.GONE);

        } else if (STEP == 5) {

        }
    }



    @OnClick(R.id.btn_nxt)
    public void onNext() {
        OnNext();
    }

    @OnClick(R.id.btn_nxt_2)
    public void onNext2() {
        OnNext();
    }




    @OnClick(R.id.rel_add_loc)
    public void onAddLoc() {
        findPlace(PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }


    @OnClick(R.id.rel_add_loc1)
    public void onAddLoc1() {
        findPlace(PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }


    @OnClick(R.id.btn_finish)
    public void onReg() {
        if(edt_wrk_email.getText().toString().trim().length()>0){
            if(edt_wrk_again.getText().toString().trim().length()>0){
                if(edt_wrk_again.getText().toString().equalsIgnoreCase(edt_wrk_email.getText().toString())){
                    if (!utils.patternForEmailId.matcher(
                            edt_wrk_email.getText().toString()).matches()) {
                        utils.hideKeyboard(RegistrationActivity.this);
                        dialog_msg_show(RegistrationActivity.this,getResources().getString(R.string.enter_valid),edt_wrk_email);
                    }else{
                        method_Reg();
                    }
                }else{
                    utils.hideKeyboard(RegistrationActivity.this);
                    dialog_msg_show(RegistrationActivity.this,getResources().getString(R.string.email_notmatch),edt_wrk_again);
                }
            }else{
                utils.hideKeyboard(RegistrationActivity.this);
                dialog_msg_show(RegistrationActivity.this,getResources().getString(R.string.entr_email_ag),edt_wrk_again);
            }
        }else{
            utils.hideKeyboard(RegistrationActivity.this);
            dialog_msg_show(RegistrationActivity.this,getResources().getString(R.string.entr_email),edt_wrk_email);
        }
    }


    @OnClick(R.id.btn_finish_2)
    public void onReg2() {
        if(edt_wrk_email.getText().toString().trim().length()>0){
            if(edt_wrk_again.getText().toString().trim().length()>0){
                if(edt_wrk_again.getText().toString().equalsIgnoreCase(edt_wrk_email.getText().toString())){
                    if (!utils.patternForEmailId.matcher(
                            edt_wrk_email.getText().toString()).matches()) {
                        utils.hideKeyboard(RegistrationActivity.this);
                        dialog_msg_show(RegistrationActivity.this,getResources().getString(R.string.enter_valid),edt_wrk_email);
                    }else{
                        method_Reg();
                    }
                }else{
                    utils.hideKeyboard(RegistrationActivity.this);
                    dialog_msg_show(RegistrationActivity.this,getResources().getString(R.string.email_notmatch),edt_wrk_again);
                }
            }else{
                utils.hideKeyboard(RegistrationActivity.this);
                dialog_msg_show(RegistrationActivity.this,getResources().getString(R.string.entr_email_ag),edt_wrk_again);
            }
        }else{
            utils.hideKeyboard(RegistrationActivity.this);
            dialog_msg_show(RegistrationActivity.this,getResources().getString(R.string.entr_email),edt_wrk_email);
        }
    }


    void method_Reg() {
        String temp1 = sharedPreferenceLeadr.getUserId() +
                "_" + System.currentTimeMillis()+"1" + ".jpg";
        GlobalConstant.FILE_NAME1 = temp1.replaceAll(" ", "_");
        String temp2 = sharedPreferenceLeadr.getUserId() +
                "_" + System.currentTimeMillis()+"2" + ".jpg";
        GlobalConstant.FILE_NAME2 = temp2.replaceAll(" ", "_");
        if(str_upload1!=null) {
            if(progressDialog!=null){
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
            progressDialog = ProgressDialog.show(RegistrationActivity.this, "", getResources().getString(R.string.reg));
            progressDialog.setCancelable(false);
            uploadFullImgsUser();
        }else{
            if(str_upload2!=null){
                if(progressDialog!=null){
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                }
                progressDialog = ProgressDialog.show(RegistrationActivity.this, "", getResources().getString(R.string.reg));
                progressDialog.setCancelable(false);
                uploadThumbImgs_Buss("");
            }else{
                if(progressDialog!=null){
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                }
                progressDialog = ProgressDialog.show(RegistrationActivity.this, "", getResources().getString(R.string.reg));
                progressDialog.setCancelable(false);
                api_Register("","");
            }
        }

    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        try{
            int width = image.getWidth();
            int height = image.getHeight();
            float bitmapRatio = (float)width / (float) height;
            if (bitmapRatio > 0) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }
            return Bitmap.createScaledBitmap(image, width, height, true);
        }catch (Exception e){
            return null;
        }

    }


    public void uploadFullImgsUser() {

        if(observerfull_user!=null){
            observerfull_user.cleanTransferListener();
        }

        Bitmap bmp = decodeFile(file_image);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

        if(decoded.getWidth()>1200)
        {
            decoded.compress(Bitmap.CompressFormat.JPEG, 50, out);
        }
        else {
            decoded.compress(Bitmap.CompressFormat.JPEG, 80, out);
        }


        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(out.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            compressedImage_user = new Compressor(this).compressToFile(destination);
        } catch (IOException e) {
            e.printStackTrace();
            compressedImage_user = destination;
        }

        observerfull_user = transferUtility.upload(Constants.BUCKET_NAME, System.currentTimeMillis()+"Leadr",
                compressedImage_user);




        String file_key = observerfull_user.getKey();
        observerfull_user.setTransferListener(new UploadListenerUSerFull(file_key));
    }


    private class UploadListenerUSerFull implements TransferListener {
        String file_key_name = "";
        public UploadListenerUSerFull( String file_key ) {
            this.file_key_name = file_key;
        }

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e("err1:", e.toString());
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
            observerfull_user.cleanTransferListener();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.e("state1: ",newState.toString());
            if(newState.toString().equals("COMPLETED")) {

//                Log.e("key: ",(String) transferRecordMaps.get(pos).get("key"));

                Log.e("url1: ","https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name);
                user_full = "https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name;
                observerfull_user.cleanTransferListener();
                uploadThumbUserImg();
            }
        }
    }


    public void uploadThumbUserImg() {
        if(observerthumb_user!=null){
            observerthumb_user.cleanTransferListener();
        }

        Bitmap bmp = decodeFile(file_image);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

        if(decoded.getWidth()>500)
        {
            decoded.compress(Bitmap.CompressFormat.JPEG, 40, out);
        }
        else {
            decoded.compress(Bitmap.CompressFormat.JPEG, 50, out);
        }


        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(out.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            compressedImage_user_thumb = new Compressor(this).compressToFile(destination);
        } catch (IOException e) {
            e.printStackTrace();
            compressedImage_user_thumb = destination;
        }

        observerthumb_user = transferUtility.upload(Constants.BUCKET_NAME, System.currentTimeMillis()+"Leadr",
                compressedImage_user_thumb);


        String file_key = observerthumb_user.getKey();
        observerthumb_user.setTransferListener(new UploadListenerThumb(file_key));
    }





    private class UploadListenerThumb implements TransferListener {
        String file_key_name = "";
        public UploadListenerThumb( String file_key ) {
            this.file_key_name = file_key;
        }

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e("err1:", e.toString());
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
            observerthumb_user.cleanTransferListener();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.e("state1: ",newState.toString());
            if(newState.toString().equals("COMPLETED")) {


//                Log.e("key: ",(String) transferRecordMaps.get(pos).get("key"));

                Log.e("url1: ","https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name);
                user_thumb = "https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name;
                observerthumb_user.cleanTransferListener();
                if (str_upload2 != null) {
                    uploadThumbImgs_Buss(user_thumb);
                } else {
                    api_Register(user_thumb, "");
                }
            }
        }
    }


    private class UploadListenerBussThumb implements TransferListener {
        String file_key_name = "";
        public UploadListenerBussThumb( String file_key ) {
            this.file_key_name = file_key;
        }

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e("err1:", e.toString());
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
            observerthumb_buss.cleanTransferListener();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.e("state1: ",newState.toString());
            if(newState.toString().equals("COMPLETED")) {

                Log.e("url1: ","https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name);
                buss_thumb = "https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name;
                observerthumb_buss.cleanTransferListener();
                api_Register(user_full, buss_thumb);
            }
        }
    }


    private class UploadListenerBussFull implements TransferListener {
        String file_key_name = "";
        public UploadListenerBussFull( String file_key ) {
            this.file_key_name = file_key;
        }

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e("err1:", e.toString());
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
            observerfull_buss.cleanTransferListener();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.e("state1: ",newState.toString());
            if(newState.toString().equals("COMPLETED")) {

                if(progressDialog!=null) {
                    progressDialog.dismiss();
                }
//                Log.e("key: ",(String) transferRecordMaps.get(pos).get("key"));

                Log.e("url3: ","https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name);
                buss_full = "https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name;
                observerfull_buss.cleanTransferListener();

                uploadThumbBussImg(buss_full);
            }
        }
    }


    public void uploadThumbImgs_Buss(final String url1) {

        if(observerfull_buss!=null){
            observerfull_buss.cleanTransferListener();
        }

        Bitmap bmp = decodeFile(file_image_buss);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

        if(decoded.getWidth()>1200)
        {
            decoded.compress(Bitmap.CompressFormat.JPEG, 50, out);
        }
        else {
            decoded.compress(Bitmap.CompressFormat.JPEG, 80, out);
        }


        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(out.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            compressedImage_buss = new Compressor(this).compressToFile(destination);
        } catch (IOException e) {
            e.printStackTrace();
            compressedImage_buss = destination;
        }

        observerfull_buss = transferUtility.upload(Constants.BUCKET_NAME, System.currentTimeMillis()+"Leadr",
                compressedImage_buss);



        String file_key = observerfull_buss.getKey();
        observerfull_buss.setTransferListener(new UploadListenerBussFull(file_key));

    }


    public void uploadThumbBussImg(final String url1) {

        if(observerthumb_buss!=null){
            observerthumb_buss.cleanTransferListener();
        }

        Bitmap bmp = decodeFile(file_image_buss);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

        if(decoded.getWidth()>500)
        {
            decoded.compress(Bitmap.CompressFormat.JPEG, 40, out);
        }
        else {
            decoded.compress(Bitmap.CompressFormat.JPEG, 50, out);
        }


        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(out.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            compressedImage_buss_thumb = new Compressor(this).compressToFile(destination);
        } catch (IOException e) {
            e.printStackTrace();
            compressedImage_buss_thumb = destination;
        }

        observerthumb_buss = transferUtility.upload(Constants.BUCKET_NAME, System.currentTimeMillis()+"Leadr",
                compressedImage_buss_thumb);


        String file_key = observerthumb_buss.getKey();
        observerthumb_buss.setTransferListener(new UploadListenerBussThumb(file_key));

    }



    private Bitmap decodeFile(File f) {
        Bitmap b = null;

        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int IMAGE_MAX_SIZE = 1024;
        int scale = 1;
        if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
                    (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        try {
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        Log.d(TAG, "Width :" + b.getWidth() + " Height :" + b.getHeight());

        File destFile = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".png");
        try {
            FileOutputStream out = new FileOutputStream(destFile);
            b.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }


    @OnClick(R.id.img_user)
    public void selUserImage() {
        sel_type = "user";
        if (utils.checkReadExternalPermission(RegistrationActivity.this)&&utils.checkWriteExternalPermission(RegistrationActivity.this)) {
            utils.hideKeyboard(RegistrationActivity.this);
            dialog_sel_image_user();
        } else {
            ActivityCompat.requestPermissions(
                    RegistrationActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE},
                    1
            );
        }
    }


    @OnClick(R.id.img_buss)
    public void selBussImage() {
        sel_type = "buss";
        if (utils.checkReadExternalPermission(RegistrationActivity.this)&&utils.checkWriteExternalPermission(RegistrationActivity.this)) {
            utils.hideKeyboard(RegistrationActivity.this);
            dialog_sel_image_buss();
        } else {
            ActivityCompat.requestPermissions(
                    RegistrationActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE},
                    1
            );
        }

    }


    public  void dialog_sel_image_buss(){
        if(dialog_buss!=null){
            if(dialog_buss.isShowing()){
                dialog_buss.dismiss();
            }
        }
        dialog_buss = new BottomSheetDialog (RegistrationActivity.this);
        dialog_buss.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_sel_img, null);
        dialog_buss.setContentView(bottomSheetView);

        dialog_buss.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_camera = (TextView) dialog_buss.findViewById(R.id.txt_camera);
        TextView txt_gallery = (TextView) dialog_buss.findViewById(R.id.txt_gallery);
        TextView txt_ok = (TextView) dialog_buss.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog_buss.findViewById(R.id.txt_title);

        txt_title.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_gallery.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        txt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_buss.dismiss();
                sel = 1;
                userChoosenTask = getResources().getString(R.string.tak_pic);
                cameraIntent();

            }
        });
        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_buss.dismiss();
                userChoosenTask = getResources().getString(R.string.chuz_lib);
                sel = 1;
                galleryIntent();
            }
        });
        try{
            dialog_buss.show();
        }catch (Exception e){

        }


    }


    public  void dialog_sel_image_user(){
        if(dialog!=null){
            if(dialog.isShowing()){
                dialog.dismiss();
            }
        }
        dialog = new BottomSheetDialog (RegistrationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_sel_img, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_camera  = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_ok      = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);

        txt_title.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_gallery.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_gallery.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));


        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        txt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sel = 0;
                userChoosenTask = getResources().getString(R.string.tak_pic);
                cameraIntent();

            }
        });
        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                userChoosenTask =  getResources().getString(R.string.chuz_lib);
                sel = 0;
                galleryIntent();
            }
        });
        dialog.show();
    }


    public  void dialog_openStoragePer(){
        if(dialog_settng!=null){
            if(dialog_settng.isShowing()){
                dialog_settng.dismiss();
            }
        }
        dialog_settng = new BottomSheetDialog (RegistrationActivity.this);
        dialog_settng.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog_settng.setContentView(bottomSheetView);

        dialog_settng.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d     = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish   = (Button)   dialog_settng.findViewById(R.id.btn_finish);
        TextView txt_ok     = (TextView) dialog_settng.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog_settng.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog_settng.findViewById(R.id.txt_camera);
        TextView txt_press  = (TextView) dialog_settng.findViewById(R.id.txt_press);
        TextView txt_store  = (TextView) dialog_settng.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Bold(RegistrationActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Bold(RegistrationActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_settng.dismiss();
                sharedPreferenceLeadr.set_PER("1");
                GlobalConstant.startInstalledAppDetailsActivity(RegistrationActivity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog_settng.show();
    }


    public  void dialog_denyOne(){
        if(dialog_denyonce!=null){
            if(dialog_denyonce.isShowing()){
                dialog_denyonce.dismiss();
            }
        }
        dialog_denyonce = new BottomSheetDialog (RegistrationActivity.this);
        dialog_denyonce.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog_denyonce.setContentView(bottomSheetView);

        dialog_denyonce.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok     = (TextView) dialog_denyonce.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog_denyonce.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog_denyonce.findViewById(R.id.txt_camera);

        txt_title.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_denyonce.dismiss();
            }
        });

        try{
            dialog_denyonce.show();
        }catch (Exception e){

        }


    }


    private void cameraIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        if(sel==0){
            file_image = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file_image));
           try{
               intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
               intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
               intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
           }catch (Exception e){}
        }else{
            file_image_buss = new File(Environment.getExternalStorageDirectory() + File.separator + "image2.jpg");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file_image_buss));
            try {
                intent.putExtra("android.intent.extras.CAMERA_FACING", 0);
                intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 0);
                intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", false);
            }catch (Exception e){}
        }
        startActivityForResult(intent, REQUEST_CAMERA);

    }


    private void galleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_FILE);
    }


    public void findPlace(int placeAutocompleteRequestCode) {
        LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
                new LatLng(latitude, longitude), new LatLng(latitude, longitude));

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).build();
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                            .setFilter(typeFilter)
                            .build(RegistrationActivity.this);
            startActivityForResult(intent, placeAutocompleteRequestCode);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                utils.hideKeyboard(RegistrationActivity.this);
                Place place = PlaceAutocomplete.getPlace(RegistrationActivity.this, data);

//                getAddress(place.getLatLng().latitude,place.getLatLng().longitude,true,adress_to_send);

                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")) {
                    method_Loc_API(place.getId(), place.getLatLng().latitude, place.getLatLng().longitude,"en");
                }else{
                    method_Loc_API(place.getId(), place.getLatLng().latitude, place.getLatLng().longitude,"he");
                }

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(RegistrationActivity.this, data);
                // TODO: Handle the error.
                Log.i("", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE) {
                    if (sel == 0) {
                        str_upload1 = "";
                        onSelectFromGalleryResult(data);
                        img_user_cancel.setVisibility(View.VISIBLE);
                        img_user_rotate.setVisibility(View.VISIBLE);
                    } else {
                        str_upload2 = "";
                        onSelectFromGalleryResult_Buss(data);
                        img_buss_cancel.setVisibility(View.VISIBLE);
                        img_user_rotate_buss.setVisibility(View.VISIBLE);
                    }
                } else if (requestCode == REQUEST_CAMERA) {
                    if (sel == 0) {
                        str_upload1 = "";
                        onCaptureImageResult(data);
                        img_user_cancel.setVisibility(View.VISIBLE);
                        img_user_rotate.setVisibility(View.VISIBLE);
                    } else {
                        str_upload2 = "";
                        onCaptureImageResult_Buss(data);
                        img_buss_cancel.setVisibility(View.VISIBLE);
                        img_user_rotate_buss.setVisibility(View.VISIBLE);
                    }
                }

            }
        }
    }



    public void getAddress(double lat, double lng, boolean isEdit,String address) {
        Geocoder geocoder = new Geocoder(RegistrationActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            getCountryName = obj.getCountryName();

            try {
                get_cityName = obj.getSubAdminArea().toString();
            } catch (Exception e) {
                e.printStackTrace();
                get_cityName = obj.getLocality();
            }

            LatlngPojo pojo = new LatlngPojo();
            pojo.setName(get_cityName);
            pojo.setLat(lat+"");
            pojo.setLong_(lng+"");
            pojo.setCountryname(getCountryName);
            pojo.setAddress(address);
            arr_place.add(pojo);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void onSelectFromGalleryResult(Intent data) {
        try {


            Uri filePath = data.getData();
//                final InputStream imageStream = getContentResolver().openInputStream(filePath);
//                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//
//                user_image = ConvertBitmapToString(selectedImage);
            File file= new File(getRealPathFromURI(filePath));
            Bitmap thumbnail = BitmapFactory.decodeFile(file.getAbsolutePath());
            Log.e("", "Orignal  bef getHeight gg= " + thumbnail.getHeight());
            Log.e("", "Orignal bef getWidth ggg= " + thumbnail.getWidth());


            Bitmap resize_bitmap = getResizedBitmap(thumbnail, 1200);
            Matrix rotateRight = new Matrix();

            Bitmap resize_bit = null;
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(file.getPath());//Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +  timeStamp+"_picture.jpg");
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Log.e("My camera Orientation", String.valueOf(orientation));
            switch (orientation) {

                case ExifInterface.ORIENTATION_UNDEFINED:
                    Log.e("My UNDEF", "UNDEFI");
                    rotateRight.postRotate(0);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    Log.e("My SAMSUNG", "UNDEFI");
                    rotateRight.postRotate(90);
                    break;


                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotateRight.postRotate(180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotateRight.postRotate(270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    rotateRight.postRotate(0);
                default:
                    break;
            }
            Bitmap image_view_bmap = null;
            try {
                image_view_bmap = Bitmap.createBitmap(resize_bitmap, 0, 0, resize_bitmap.getWidth(), resize_bitmap.getHeight(), rotateRight, true);
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
            }


            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            image_view_bmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);


            Log.e("", "Orignal af getHeight gg= " + image_view_bmap.getHeight());
            Log.e("", "Orignalaf  getWidth ggg= " + image_view_bmap.getWidth());

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            img_user.setImageURI(filePath);
            file_image = new File(getRealPathFromURI(Uri.parse(destination.getPath())));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void onSelectFromGalleryResult_Buss(Intent data) {
        try {
            final Uri imageUri = data.getData();
            file_image_buss = new File(getRealPathFromURI(imageUri));
            img_buss.setImageURI(imageUri);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String getRealPathFromURI(Uri contentURI) {
        String result;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentURI, projection, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }



    private void onCaptureImageResult(Intent data) {
        File file = new File(Environment.getExternalStorageDirectory() + File.separator +
                "image.jpg");
        Bitmap thumbnail = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap resize_bitmap = getResizedBitmap(thumbnail, 1200);
        Matrix rotateRight = new Matrix();

        Bitmap resize_bit = null;
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(file.getPath());//Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +  timeStamp+"_picture.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Log.e("My camera Orientation", String.valueOf(orientation));
        switch (orientation) {

            case ExifInterface.ORIENTATION_UNDEFINED:
                rotateRight.postRotate(0);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotateRight.postRotate(90);
                break;


            case ExifInterface.ORIENTATION_ROTATE_180:
                rotateRight.postRotate(180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotateRight.postRotate(270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
                rotateRight.postRotate(0);
            default:
                break;
        }
        Bitmap image_view_bmap = null;
        if (resize_bitmap != null) {
            try {                                       //resize_bitmap
                image_view_bmap = Bitmap.createBitmap(resize_bitmap, 0, 0, resize_bitmap.getWidth(), resize_bitmap.getHeight(), rotateRight, true);
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
            }
        } else {
            try {                                       //resize_bitmap
                image_view_bmap = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), rotateRight, true);
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
            }
        }

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        image_view_bmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        file_image = new File(getRealPathFromURI(Uri.parse(destination.getPath())));

        File imgFile = new File(file_image.getPath());
        Uri img_uri = Uri.fromFile(imgFile);
        img_user.setImageURI(img_uri);
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        resize_bitmap = getResizedBitmap(myBitmap, 1200);
        /*if (resize_bitmap != null) {
            img_user.setImageBitmap(resize_bitmap);
        }else{
            img_user.setImageBitmap(thumbnail);
        }*/
    }


    private void onCaptureImageResult_Buss(Intent data) {
        File file = new File(Environment.getExternalStorageDirectory() + File.separator +
                "image2.jpg");
        Bitmap thumbnail = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap resize_bitmap = getResizedBitmap(thumbnail, 1200);
        Matrix rotateRight = new Matrix();

        Bitmap resize_bit = null;
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(file.getPath());//Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +  timeStamp+"_picture.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        switch (orientation) {
            case ExifInterface.ORIENTATION_UNDEFINED:
                rotateRight.postRotate(0);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotateRight.postRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotateRight.postRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotateRight.postRotate(270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
                rotateRight.postRotate(0);
            default:
                break;
        }
        Bitmap image_view_bmap = null;
        if (resize_bitmap != null) {
            try {                                       //resize_bitmap
                image_view_bmap = Bitmap.createBitmap(resize_bitmap, 0, 0, resize_bitmap.getWidth(), resize_bitmap.getHeight(), rotateRight, true);
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
            }
        } else {
            try {                                       //resize_bitmap
                image_view_bmap = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), rotateRight, true);
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
            }
        }

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        image_view_bmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        file_image_buss = new File(getRealPathFromURI(Uri.parse(destination.getPath())));

        File imgFile    = new File(file_image_buss.getPath());

        Uri img_uri = Uri.fromFile(imgFile);
        img_buss.setImageURI(img_uri);
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        resize_bitmap = getResizedBitmap(myBitmap, 1200);
        /*if (resize_bitmap != null) {
            img_buss.setImageBitmap(resize_bitmap);
        }else{
            img_buss.setImageBitmap(thumbnail);
        }*/
    }


    /****************Set Astresik***************************/
    private void addAstresik() {
        String simple = getResources().getString(R.string.ful_name);
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_fullname.setText(builder);
    }
    private void addAstresikBusiness() {
        String simple = getResources().getString(R.string.buss_name);
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_businessname.setText(builder);
    }
    private void addAstresikEmail() {
        String simple = getResources().getString(R.string.wrk_email);
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_email.setText(builder);
    }
    private void addAstresikEmailAgain() {
        String simple = getResources().getString(R.string.wrk_em_agn);
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_em_again.setText(builder);
    }
    /******************************************************/


    @Override
    public void onBackPressed() {
        Onback();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(RegistrationActivity.this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }


    /********Set Categories******************/
    private void api_GetCategory() {
        Log.e("id: ",sharedPreferenceLeadr.get_UserId_Bfr());
        AndroidNetworking.enableLogging();
        Log.e("url_getCat: ", NetworkingData.BASE_URL + NetworkingData.GET_CATEGORY);
        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.GET_CATEGORY)
                .addBodyParameter("user_id",sharedPreferenceLeadr.get_UserId_Bfr())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_getCat: ", result + "");
                        utils.dismissProgressdialog();
                        arr_cat.clear();
                        if (result.optString("status").equals("1")) {
                            try {
                                JSONArray arr = result.getJSONArray("categories");
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject obj = arr.getJSONObject(i);
                                    CategoryPojo item = new CategoryPojo();
                                    item.setCategory(obj.getString("category_name"));
                                    item.setId(obj.getString("id"));
                                    item.setHebrew(obj.getString("hebrew"));
                                    item.setChecked(false);
                                    arr_cat.add(item);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });
    }
    /***************************************/


    /*****REGISTER User*********************************************************/
    private void api_Register(final String img_1, final String img_2) {
        AndroidNetworking.enableLogging();

        String lat = "0";
        String long_ = "0";
        String loc = "0";
        String country = "0";
        String address = "0";

        for (int i = 0; i < arr_place.size(); i++) {
            if (lat.equals("0")) {
                lat = arr_place.get(i).getLat();
                long_ = arr_place.get(i).getLong_();
                loc = arr_place.get(i).getName();
                country = arr_place.get(i).getCountryname();
                address = arr_place.get(i).getAddress();
            } else {
                lat = lat + "," + arr_place.get(i).getLat();
                long_ = long_ + "," + arr_place.get(i).getLong_();
                loc = loc + "@" + arr_place.get(i).getName();
                country = country + "@" + arr_place.get(i).getCountryname();
                address = address + "@" + arr_place.get(i).getAddress();
            }
        }

        if(loc.equals("0")){
            sharedPreferenceLeadr.set_LOC_TYPE("0");
            sharedPreferenceLeadr.set_LOC_TYPE_FILTER("0");
        }else{
            sharedPreferenceLeadr.set_LOC_TYPE("1");
            sharedPreferenceLeadr.set_LOC_TYPE_FILTER("1");
            sharedPreferenceLeadr.set_LOC_NAME(loc);
            sharedPreferenceLeadr.set_LOC_NAME_TEMP(loc);
            sharedPreferenceLeadr.set_LOC_NAME_COUNTRY(country);
            sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP(country);
            sharedPreferenceLeadr.set_LATITUDE(lat);
            sharedPreferenceLeadr.set_LATITUDE_TEMP(lat);
            sharedPreferenceLeadr.set_LONGITUDE(long_);
            sharedPreferenceLeadr.set_LONGITUDE_TEMP(long_);
            sharedPreferenceLeadr.set_ADDRESS(address);
            sharedPreferenceLeadr.set_ADDRESS_TEMP(address);

            Log.e("location_name_befr: ",  loc);

            if(!address.trim().equalsIgnoreCase("")){
                address = GlobalConstant.ConvertToBase64(address).trim();
            }
            if(!country.trim().equalsIgnoreCase("")){
                country = GlobalConstant.ConvertToBase64(country).trim();
            }
            if(!loc.trim().equalsIgnoreCase("")){
                loc = GlobalConstant.ConvertToBase64(loc).trim();
            }
        }

        String dev_token = "";
        try {
            dev_token = FirebaseInstanceId.getInstance().getToken();
        } catch (Exception e) {
            dev_token = sharedPreferenceLeadr.get_dev_token();
        }

        Log.e("url_signup: ", NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO_VER2);
        Log.e("job_title: ",  edt_jobtitle.getText().toString());
        Log.e("phone_number: ",  phone);
        Log.e("user_name: ",  edt_fullname.getText().toString());
        Log.e("business_name: ",  edt_buss_name.getText().toString());
        Log.e("business_fb_page: ",  edt_buss_page.getText().toString());
        Log.e("business_info: ",  edt_buss_info.getText().toString());
        Log.e("location_name_after: ",  loc);
        Log.e("lat: ",  lat);
        Log.e("lon: ",  long_);
        Log.e("category: ",  cate_sel_id);
        Log.e("user_image: ",  user_full);
        Log.e("user_image_thumb: ",  user_thumb);
        Log.e("business_image: ",  buss_full);
        Log.e("business_image_thumb: ",  buss_thumb);
        Log.e("device_token: ",  dev_token);
        Log.e("device_type: ",  "android");
        Log.e("id: ",  sharedPreferenceLeadr.get_UserId_Bfr());
        Log.e("email: ",  edt_wrk_email.getText().toString());

        String job_title        = edt_jobtitle.getText().toString();
        String id               = sharedPreferenceLeadr.get_UserId_Bfr();
        String phone_number     = phone;
        String user_name        = edt_fullname.getText().toString();
        String business_name    = edt_buss_name.getText().toString();
        String business_fb_page = edt_buss_page.getText().toString();
        String business_info    = edt_buss_info.getText().toString();
        String location_name    = loc;

        String user_name_base = GlobalConstant.ConvertToBase64(user_name).trim();
        String business_name_base = GlobalConstant.ConvertToBase64(business_name).trim();
        String business_info_buy = GlobalConstant.ConvertToBase64(business_info).trim();
        job_title = GlobalConstant.ConvertToBase64(job_title).trim();

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO_VER2)
                .addBodyParameter("job_title"       , job_title)
                .addBodyParameter("id"              , id)
                .addBodyParameter("phone_number"    , phone_number)
                .addBodyParameter("user_name"       , user_name_base)
                .addBodyParameter("business_name"   , business_name_base)
                .addBodyParameter("business_fb_page", business_fb_page)
                .addBodyParameter("business_info"   , business_info_buy)
                .addBodyParameter("location_name"   , address)
                .addBodyParameter("lat"             , lat)
                .addBodyParameter("lon"             , long_)
                .addBodyParameter("category"        , cate_sel_id)
                .addBodyParameter("user_image"      , user_full)
                .addBodyParameter("business_image"  , buss_full)
                .addBodyParameter("device_token"    , dev_token)
                .addBodyParameter("device_type"     , "android")
                .addBodyParameter("country"         , country)
                .addBodyParameter("address"         , address)
                .addBodyParameter("user_thum"       , user_thumb)
                .addBodyParameter("busi_thum"       , buss_thumb)
                .addBodyParameter("email"           , edt_wrk_email.getText().toString())

                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("res_register: ", response + "");
//                        Log.e("userid: ", sharedPreferenceLeadr.get_UserId_Bfr() + "");
                        sharedPreferenceLeadr.setUserId(sharedPreferenceLeadr.get_UserId_Bfr());
                        //{"status":"1","message":"User signup successfully!","phone_number":"6000000000","user_name":"thanks","user_id":27}
                        if (response.optString("status").equals("1")) {
//                            sharedPreferenceLeadr.setUserId(response.optString("user_id"));
                            sharedPreferenceLeadr.set_bus_site(edt_buss_page.getText().toString());
                            sharedPreferenceLeadr.set_bus_img(buss_full);
                            sharedPreferenceLeadr.set_BUSS_THUMB(buss_thumb);
                            sharedPreferenceLeadr.set_bus_info(edt_buss_info.getText().toString());
                            sharedPreferenceLeadr.set_bus_name(edt_buss_name.getText().toString());
                            sharedPreferenceLeadr.set_jobTitle(edt_jobtitle.getText().toString());
                            sharedPreferenceLeadr.setProfilePicUrl(user_full);
                            sharedPreferenceLeadr.set_PROFILE_THUMB(user_thumb);
                            sharedPreferenceLeadr.set_username(edt_fullname.getText().toString());
                            sharedPreferenceLeadr.set_EMAIL_REG(edt_wrk_email.getText().toString());
                            sharedPreferenceLeadr.set_phone(phone);
                           /* if(cate_sel_id.contains(",")){
                                String[] animalsArray = cate_sel_id.split(",");
                                cate_sel_id = animalsArray[0].trim();
                            }*/
                            sharedPreferenceLeadr.set_CATEGORY(cate_sel_id);
                            sharedPreferenceLeadr.set_CATEGORYNAME_ENG_REG(cate_sel);
                            sharedPreferenceLeadr.set_CATEGORYNAME_HE_REG(cate_sel);
                            sharedPreferenceLeadr.set_CATEGORYNAME_HE_FIL(cate_sel);
                            sharedPreferenceLeadr.set_CATEGORYNAME_ENG_FIL(cate_sel);
                            sharedPreferenceLeadr.set_CAT_FILTER(cate_sel_id);
                            sharedPreferenceLeadr.set_CAT_FILTER_ID_TEMP(cate_sel_id);
                            sharedPreferenceLeadr.set_CATEGORY_SEL_ID(cate_sel_id);
//                            sharedPreferenceLeadr.set_MIN_BUDGET("0");
                            sharedPreferenceLeadr.set_DATEINSTALLED(CurrentDate());

                            SimpleDateFormat format = new SimpleDateFormat("hh:mm");
                            Date date1 = null;
                            Date date2 = null;
                            try {
                                 date1 = format.parse(sharedPreferenceLeadr.get_REG_START());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            try {
                                 date2 = format.parse(CurrentTimeEnd());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Bundle parameters = new Bundle();
                            long mills = date2.getTime() - date1.getTime();
                            int mins = (int) (mills/(1000*60)) % 60;
                            parameters.putString("EVENT_PARAM_LEVEL", mins+" mins");
                            if(mins==0){
                                int secs = (int) (mills/(1000*60*60)) % 60;
                                parameters.putString("EVENT_PARAM_LEVEL", secs+" secs");
                            }




                            AppEventsLogger logger = AppEventsLogger.newLogger(RegistrationActivity.this);
                            logger.logEvent("EVENT_NAME_ACTIVATED_APP");

                            logger.logEvent("EVENT_NAME_COMPLETED_REGISTRATION",
                                    parameters);

                            sharedPreferenceLeadr.set_REG_START("00:00");
                            sharedPreferenceLeadr.set_FROM_PRICE("0");
                            sharedPreferenceLeadr.set_TOOLTIP("1");

                            if(progressDialog!=null){
                                if(progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                            }

                            Intent i_nxt = new Intent(RegistrationActivity.this, MainActivity.class);
                            startActivity(i_nxt);
                            finish();
                        } else {
                            if(progressDialog!=null){
                                if(progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                            }

                            utils.hideKeyboard(RegistrationActivity.this);
                            utils.dialog_msg_show(RegistrationActivity.this, response.optString("message"));
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        if(progressDialog!=null){
                            if(progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                        }
                    }
                });
    }
    /**************************************************************************/



    /***Location Adapter************/
    public class LocRegister_Adapter extends RecyclerView.Adapter<LocRegister_Adapter.MyViewHolder> {
        Activity                         ctx;
        LocRegister_Adapter.MyViewHolder holderItem;

        public LocRegister_Adapter(Activity ctx, ArrayList<LatlngPojo> arr_place) {
            this.ctx = ctx;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(ctx).inflate(R.layout.item_loc, parent, false);
            holderItem = new LocRegister_Adapter.MyViewHolder(itemView);

            return new LocRegister_Adapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.bind(position);
        }


        @Override
        public int getItemCount() {
            return arr_place.size();
        }


        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView  txt_loc;
            ImageView img_remove;
            View      vw_line;

            public MyViewHolder(View view) {
                super(view);

                txt_loc    = (TextView) view.findViewById(R.id.txt_loc);
                img_remove = (ImageView) view.findViewById(R.id.img_remove);
                vw_line    = (View) view.findViewById(R.id.vw_line);

                if (view.getTag() != null) {
                    view.setTag(holderItem);
                }
            }
            public void bind(final int i) {
                if(!arr_place.get(i).getCountryname().equals("")){
                    txt_loc.setText(arr_place.get(i).getName()+", "+arr_place.get(i).getCountryname());
                }else {
                    txt_loc.setText(arr_place.get(i).getName());
                }
                txt_loc.setText(arr_place.get(i).getAddress());
                txt_loc.setTypeface(utils.OpenSans_Regular(RegistrationActivity.this));

                if(i==arr_place.size()-1){
                    vw_line.setVisibility(View.GONE);
                }

                img_remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        arr_place.remove(arr_place.get(i));

                        notifyDataSetChanged();
                        if (arr_place.size() > 0) {
                            rel_add_loc1.setVisibility(View.GONE);

                            rel_4.setVisibility(View.VISIBLE);
                            recycl_loc.setVisibility(View.VISIBLE);
                            lnr_4.setVisibility(View.VISIBLE);
                            txt_loc_4.setVisibility(View.VISIBLE);
                            rel_final.setVisibility(View.GONE);
                            lnr_5.setVisibility(View.GONE);
                            rel_add_loc1.setVisibility(View.GONE);
                        } else {
                            lnr_bottom_3.setVisibility(View.GONE);
                            lnr_bottom_3_2.setVisibility(View.GONE);
                            lnr_3.setVisibility(View.GONE);
                            recycl_loc.setVisibility(View.GONE);
                            card_full.setVisibility(View.GONE);
                            lnr_4.setVisibility(View.GONE);
                            btn_finish.setVisibility(View.GONE);
                            btn_finish_2.setVisibility(View.GONE);
                            txt_loc_4.setVisibility(View.VISIBLE);
                            rel_final.setVisibility(View.VISIBLE);
                            rel_add_loc1.setVisibility(View.VISIBLE);
                            vw_line.setVisibility(View.GONE);
                            lnr_5.setVisibility(View.VISIBLE);
                            btn_nxt.setVisibility(View.VISIBLE);
                            btn_nxt_2.setVisibility(View.VISIBLE);
                        }
                    }
                });

            }
        }
    }
    /******************************/


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                //denied
                utils.hideKeyboard(RegistrationActivity.this);
                dialog_denyOne();
            }else{
                if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                    //allowed
                    utils.hideKeyboard(RegistrationActivity.this);
                    if(utils.checkWriteExternalPermission(RegistrationActivity.this)&&utils.checkReadExternalPermission(RegistrationActivity.this)) {
                        if (sel_type.equals("user")) {
                            dialog_sel_image_user();
                        } else {
                            dialog_sel_image_buss();
                        }
                    }
                } else{
                    utils.hideKeyboard(RegistrationActivity.this);
                    dialog_openStoragePer();
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //do something here.
                }
            }
        }
    }



    /***Set current tim-Date // FOR ANALYTICS**************/
    String CurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    String CurrentTimeStart(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    String CurrentTimeEnd(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    /*****************************************************/


    /***Dialog for error in fields to request focus on Edittext*****/
    public  void dialog_msg_show(Activity activity, String msg, final EditText edt){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg                   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title                 = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok                    = (TextView) dialog.findViewById(R.id.txt_ok);
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);

        txt_msg.setText(msg);

        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.show();
    }


    /***Get Address***/
    private void method_Loc_API( String id, final double latitude,final  double longitude,String language ) {
        AndroidNetworking.enableLogging();
        Log.e("url_locApi: ", NetworkingData.BASEURL_LOCATION+ id+"&key="+getResources().getString(R.string.gmail_key)
                +"&language="+language);

        AndroidNetworking.get(NetworkingData.BASEURL_LOCATION+ id+"&key="+getResources().getString(R.string.gmail_key)
                +"&language="+language)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_locApi: ",result+"");
                        //{"status":1,"message":"Details..","admin_settings":[{"id":1,"admin_id":1,"number_of_leads":4,"min_amount":"1
                        //  ","max_lead_price":"100","market_fee":"44"}]}

                        try{
                            JSONObject arr = result.getJSONObject("result");
                            String full_add = arr.getString("formatted_address");
                            Log.e("add: ",full_add);

                            LatlngPojo pojo = new LatlngPojo();
                            pojo.setName(full_add);
                            pojo.setLat(latitude+"");
                            pojo.setLong_(longitude+"");
                            pojo.setCountryname(full_add);
                            pojo.setAddress(full_add);
                            arr_place.add(pojo);

                            method_AddLoc_in_arry();
                        }catch (Exception e){
                            String g = e.toString();
                        }
                        utils.dismissProgressdialog();


                    }

                    private void method_AddLoc_in_arry() {
                        if (arr_place.size() > 0) {
                            rel_add_loc1.setVisibility(View.GONE);

                            rel_4.setVisibility(View.VISIBLE);
                            recycl_loc.setVisibility(View.VISIBLE);
                            lnr_4.setVisibility(View.VISIBLE);
                            txt_loc_4.setVisibility(View.VISIBLE);
                            rel_final.setVisibility(View.GONE);
                            lnr_5.setVisibility(View.GONE);
                        } else {
                            lnr_bottom_3.setVisibility(View.GONE);
                            lnr_bottom_3_2.setVisibility(View.GONE);
                            lnr_3.setVisibility(View.GONE);
                            recycl_loc.setVisibility(View.GONE);
                            card_full.setVisibility(View.GONE);
                            lnr_4.setVisibility(View.GONE);
                            btn_finish.setVisibility(View.VISIBLE);
                            btn_finish_2.setVisibility(View.VISIBLE);
                            txt_loc_4.setVisibility(View.VISIBLE);
                            lnr_5.setVisibility(View.VISIBLE);
                            btn_nxt.setVisibility(View.GONE);
                            btn_nxt_2.setVisibility(View.GONE);
                        }
                        recycl_loc.setVisibility(View.VISIBLE);
                        LinearLayoutManager lnr_album = new LinearLayoutManager(RegistrationActivity.this, LinearLayoutManager.VERTICAL, false);
                        recycl_loc.setLayoutManager(lnr_album);
                        adapter_loc = new LocRegister_Adapter(RegistrationActivity.this, arr_place);
                        recycl_loc.setAdapter(adapter_loc);
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();

                    }
                });
    }
}
