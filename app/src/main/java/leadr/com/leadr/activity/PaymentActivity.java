package leadr.com.leadr.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.appevents.AppEventsLogger;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import org.json.JSONObject;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.KeyboardActivity;
import utils.NetworkingData;

public class PaymentActivity extends KeyboardActivity {

    @BindView(R.id.btnok)
    Button btnok;

    @BindView(R.id.btnok1)
    Button btnok1;

    @BindView(R.id.edt_cvv)
    EditText edt_cvv;

    @BindView(R.id.edt_month)
    EditText edt_month;

    @BindView(R.id.edt_year)
    EditText edt_year;

    @BindView(R.id.edt_card_no)
    EditText edt_card_no;

    @BindView(R.id.edt_full_name)
    EditText edt_full_name;

    @BindView(R.id.img_cancel)
    ImageView img_cancel;

    @BindView(R.id.img_remove)
    ImageView img_remove;

    @BindView(R.id.img_cancel1)
    ImageView img_cancel1;

    @BindView(R.id.rel_bottm)
    RelativeLayout rel_bottm;

    @BindView(R.id.rel_botm_real)
    RelativeLayout rel_botm_real;

    @BindView(R.id.txt_top)
    TextView txt_top;

    @BindView(R.id.progres_load)
    ProgressBar progres_load;

    @BindView(R.id.vw_line)
    View vw_line;

    GlobalConstant        utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;
    String                get_lead_id;
    BottomSheetDialog     dialog_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        // bind the view using butterknife
        ButterKnife.bind(this);

        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(PaymentActivity.this);
        get_lead_id           = getIntent().getStringExtra("lead_id");

        btnok.setTypeface(utils.Dina(PaymentActivity.this));
        btnok1.setTypeface(utils.Dina(PaymentActivity.this));
        edt_cvv.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        edt_month.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        edt_year.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        edt_card_no.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        edt_full_name.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        txt_top.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));

        String text = "";
        if(sharedPreferenceLeadr.get_CARD().equalsIgnoreCase("N/A")){
            img_remove.setVisibility(View.GONE);
            text = "<font color=#227cec>"+getResources().getString(R.string.frst_tym)+
                    "</font> <font color=#071a30>"+getResources().getString(R.string.you_must)+"</font>";
        }else{
            img_remove.setVisibility(View.VISIBLE);
            text =
                    "</font> <font color=#071a30>"+getResources().getString(R.string.ur_card)+"</font>";
        }
        txt_top.setText(Html.fromHtml(text));
        edt_month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    String working = s.toString();
                    Log.e("before: ",working+"");
                }catch (Exception e){}
                if (edt_month.getText().toString().matches("^2")|| edt_month.getText().toString().matches("^3")||
                        edt_month.getText().toString().matches("^4")||edt_month.getText().toString().matches("^5")||
                        edt_month.getText().toString().matches("^6")||edt_month.getText().toString().matches("^7")||
                        edt_month.getText().toString().matches("^8")||edt_month.getText().toString().matches("^9")) {
                    // Not allowed
                    edt_month.setText("");
                }else if(s.length() == 2){
                    if (Integer.valueOf(edt_month.getText().toString())>12){
                        edt_month.setText("1");
                        edt_month.setSelection(edt_month.getText().toString().length());
                    }else  if (s.length() == 2) {
                        edt_year.requestFocus();
                        edt_year.setSelection(edt_year.getText().length());
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edt_year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    edt_month.requestFocus();
                    edt_month.setSelection(edt_month.getText().length());
                }else if(s.length() == 4){
                    edt_cvv.requestFocus();
                    edt_cvv.setSelection(edt_cvv.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edt_cvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 3) {
                    utils.hideKeyboard(PaymentActivity.this);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edt_card_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 16) {
                    edt_month.requestFocus();
                    edt_month.setSelection(edt_month.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        /***Disable copying number***/

        edt_card_no.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        edt_card_no.setLongClickable(false);
        edt_card_no.setTextIsSelectable(false);


        /***Keyboard Listener*****/
        KeyboardVisibilityEvent.setEventListener(
                PaymentActivity.this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                        if(isOpen){
                            rel_bottm.setVisibility(View.VISIBLE);
                            rel_botm_real.setVisibility(View.GONE);
                            vw_line.setVisibility(View.GONE);
                        }else{
                            rel_bottm.setVisibility(View.VISIBLE);
                            rel_botm_real.setVisibility(View.GONE);
                            vw_line.setVisibility(View.GONE);
                        }
                    }
                });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Unregistrar unregistrar = KeyboardVisibilityEvent.registerEventListener(
                PaymentActivity.this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                    }
                });

// call this method when you don't need the event listener anymore
        unregistrar.unregister();
    }

    @OnClick(R.id.img_cancel)
    public void OnCancel() {
       finish();
    }

    @OnClick(R.id.img_cancel1)
    public void OnCancel1() {
       finish();
    }

    @OnClick(R.id.img_remove)
    public void OnRemoveCard() {
        utils.hideKeyboard(PaymentActivity.this);
        dialog_RemoveSureCard();
    }



    public  void dialog_RemoveSureCard(){
        final BottomSheetDialog dialog = new BottomSheetDialog (PaymentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_card_del, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d     = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_update = (TextView) dialog.findViewById(R.id.txt_update);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        Button btn_update   = (Button)   dialog.findViewById(R.id.btn_update);

        txt_title.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        btn_update.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        txt_update.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));

        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
            txt_update.setGravity(Gravity.RIGHT);
        }
        btn_update.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                dialog.dismiss();
                api_DelCustomer();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
//                dialog_AddCard();
            }
        });
        dialog.show();
    }



    private void api_DelCustomer() {
        AndroidNetworking.enableLogging();
        progres_load.setVisibility(View.VISIBLE);
        Log.e("url_del: ", NetworkingData.BASE_URL + NetworkingData.DEL_CARDINFO);


        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.DEL_CARDINFO)
                .addBodyParameter("user_id", sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_stripe: ", result + "");
                        //{"status":"1","customer":{"id":"cus_C4xJzoWD2YfwOO","object":"customer","account_balance":0,"created":1515128721,"currency":null,"default_source":"card_1BgnOOI0GVRrJIpDo8dLjinv","delinquent":false,"description":"Customer for LeadR app","discount":null,"email":null,"livemode":false,"metadata":{},"shipping":null,"sources":{"object":"list","data":[{"id":"card_1BgnOOI0GVRrJIpDo8dLjinv","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"Visa","country":"US","customer":"cus_C4xJzoWD2YfwOO","cvc_check":"pass","dynamic_last4":null,"exp_month":11,"exp_year":2022,"fingerprint":"kXn425uHw3F6DtGT","funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null}],"has_more":false,"total_count":1,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/subscriptions"}}}

                        utils.dismissProgressdialog();
                        progres_load.setVisibility(View.GONE);
                        if (result.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_CARD("N/A");
                           dialog_SUccess(PaymentActivity.this,getResources().getString(R.string.succ_del),
                                    getResources().getString(R.string.succ));
                        } else {
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(PaymentActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }else{
                                utils.dismissProgressdialog();
                                Toast.makeText(PaymentActivity.this,getResources().getString(R.string.succ_error_del),Toast.LENGTH_LONG).show();
                            }

                        }
                        String text = "";
                        if(sharedPreferenceLeadr.get_CARD().equalsIgnoreCase("N/A")){
                            img_remove.setVisibility(View.GONE);
                            text = "<font color=#227cec>"+getResources().getString(R.string.frst_tym)+
                                    "</font> <font color=#071a30>"+getResources().getString(R.string.you_must)+"</font>";
                        }else{
                            img_remove.setVisibility(View.VISIBLE);
                            text =
                                    "</font> <font color=#071a30>"+getResources().getString(R.string.ur_card)+"</font>";
                        }
                        txt_top.setText(Html.fromHtml(text));

                        utils.dismissProgressdialog();

                    }

                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                        progres_load.setVisibility(View.GONE);
                    }
                });
    }




    @OnClick(R.id.btnok)
    public void OnSubmit() {
        method_check_values();
    }



    @OnClick(R.id.btnok1)
    public void OnSubmit1() {
        method_check_values();
    }


    void method_check_values() {
        utils.hideKeyboard(PaymentActivity.this);
        if (edt_full_name.getText().toString().trim().length() > 0) {
            if (edt_card_no.getText().toString().trim().length() > 0) {
                if (edt_month.getText().toString().trim().length() == 2) {
                    if (edt_year.getText().toString().trim().length() == 4) {
                        if (edt_cvv.getText().toString().trim().length() > 0) {
                            Card card = new Card(edt_card_no.getText().toString(), Integer.valueOf(edt_month.getText().toString()),
                                    Integer.valueOf(edt_year.getText().toString()),
                                    edt_cvv.getText().toString());
                            if (!card.validateCard()) {
                                utils.hideKeyboard(PaymentActivity.this);
                                if (!card.validateNumber()) {
                                    utils.dialog_msg_show(PaymentActivity.this, getResources().getString(R.string.in_cvv));
                                } else if (!card.validateExpMonth()) {
                                    utils.dialog_msg_show(PaymentActivity.this, getResources().getString(R.string.exp));
                                } else if (!card.validateExpiryDate()) {
                                    utils.dialog_msg_show(PaymentActivity.this, getResources().getString(R.string.expyr));
                                } else if (!card.validateCVC()) {
                                    utils.dialog_msg_show(PaymentActivity.this, getResources().getString(R.string.cvv_in));
                                }
                            } else {
                                String substr = edt_card_no.getText().toString().substring(edt_card_no.getText().toString().
                                        length() - 4);
//                                        sharedPreferenceLeadr.set_CARD(substr);
                                utils.hideKeyboard(PaymentActivity.this);
//                                        utils.showProgressDialog(ProfileActivity.this, getResources().getString(R.string.pls_wait));
                                if(progres_load!=null){
                                    progres_load.setVisibility(View.VISIBLE);
                                }
                                if(dialog_card!=null){
                                    if(dialog_card.isShowing()){
                                        dialog_card.dismiss();
                                    }
                                }
                                dialog_CardFrstTYm(PaymentActivity.this,getResources().getString(R.string.frst_tym_longr));
                                method_SendToStripe(card,substr);
                            }
                        } else {
                            utils.dialog_msg_show(PaymentActivity.this, getResources().getString(R.string.entr_cvv));
                        }
                    } else {
                        utils.dialog_msg_show(PaymentActivity.this, getResources().getString(R.string.entr_yr));
                    }
                } else {
                    utils.dialog_msg_show(PaymentActivity.this, getResources().getString(R.string.entr_mm));
                }
            } else {
                utils.dialog_msg_show(PaymentActivity.this, getResources().getString(R.string.entr_nmbr));
            }
        } else {
            utils.dialog_msg_show(PaymentActivity.this, getResources().getString(R.string.entr_namer));
        }
    }


    public  void dialog_CardFrstTYm( Activity activity, String msg){
        dialog_card = new BottomSheetDialog (activity);
        dialog_card.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog_card.setContentView(bottomSheetView);

        dialog_card.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog_card.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog_card.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog_card.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        txt_title.setText(getResources().getString(R.string.pls_wait));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog_card.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_card.dismiss();
            }
        });
        dialog_card.show();


    }


    void method_SendToStripe(Card card,final String subcard) {
        Stripe stripe = new Stripe(PaymentActivity.this, "pk_live_r9TQXzdQ0OeTo6NiRxHJQxft");
        stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        if (utils.isNetworkAvailable(PaymentActivity.this)) {
                            api_SaveToken(token.getId(),subcard);
                        } else {
                            if(dialog_card!=null){
                                if(dialog_card.isShowing()){
                                    dialog_card.dismiss();
                                }
                            }
                            utils.hideKeyboard(PaymentActivity.this);
                            utils.dialog_msg_show(PaymentActivity.this, getResources().getString(R.string.no_internet));
                        }
                    }

                    public void onError(Exception error) {
                        sharedPreferenceLeadr.set_CARD("N/A");
                        utils.dismissProgressdialog();
                        utils.hideKeyboard(PaymentActivity.this);
                        utils.dialog_msg_show(PaymentActivity.this, error.toString());
                        if(dialog_card!=null){
                            if(dialog_card.isShowing()){
                                dialog_card.dismiss();
                            }
                        }
                        if(progres_load!=null){
                            progres_load.setVisibility(View.GONE);
                        }
                    }
                }
        );
    }


    private void api_SaveToken(String token, final String subcard) {
        AndroidNetworking.enableLogging();
        Log.e("url_stripe: ", NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN);

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN)
                .addBodyParameter("token", token)
                .addBodyParameter("user_id", sharedPreferenceLeadr.getUserId())
                .addBodyParameter("last_digits", subcard)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_stripe: ", result + "");
                        //{"status":"1","customer":{"id":"cus_C4xJzoWD2YfwOO","object":"customer","account_balance":0,"created":1515128721,"currency":null,"default_source":"card_1BgnOOI0GVRrJIpDo8dLjinv","delinquent":false,"description":"Customer for LeadR app","discount":null,"email":null,"livemode":false,"metadata":{},"shipping":null,"sources":{"object":"list","data":[{"id":"card_1BgnOOI0GVRrJIpDo8dLjinv","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"Visa","country":"US","customer":"cus_C4xJzoWD2YfwOO","cvc_check":"pass","dynamic_last4":null,"exp_month":11,"exp_year":2022,"fingerprint":"kXn425uHw3F6DtGT","funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null}],"has_more":false,"total_count":1,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/subscriptions"}}}
                        if(progres_load!=null){
                            progres_load.setVisibility(View.GONE);
                        }
                        if(dialog_card!=null){
                            if(dialog_card.isShowing()){
                                dialog_card.dismiss();
                            }
                        }
                        if (result.optString("status").equals("1")) {
                            AppEventsLogger logger = AppEventsLogger.newLogger(PaymentActivity.this);
                            logger.logEvent("EVENT_NAME_ADDED_PAYMENT_INFO");
                            sharedPreferenceLeadr.set_CARD(subcard);
                            dialog_SUccess(PaymentActivity.this,
                                    getResources().getString(R.string.card_sv),getResources().getString(R.string.succ));
                        } else {
                            utils.dismissProgressdialog();
                            sharedPreferenceLeadr.set_CARD("N/A");
                            dialog_errorCard();
                        }
                        utils.dismissProgressdialog();
                       /* if(dialog!=null) {
                            if(dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }*/
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                        if(dialog_card!=null){
                            if(dialog_card.isShowing()){
                                dialog_card.dismiss();
                            }
                        }
                        if(progres_load!=null){
                            progres_load.setVisibility(View.GONE);
                        }
                    }
                });
    }



    public  void dialog_SUccess(Activity activity, String msg, String title){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog.findViewById(R.id.txt_ok);

        txt_msg.setText(msg);
        txt_title.setText(title);

        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                finish();
            }
        });


    }

    public  void dialog_errorCard(){
        final BottomSheetDialog dialog = new BottomSheetDialog (PaymentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_card_error, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        TextView txt_update = (TextView) dialog.findViewById(R.id.txt_update);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView btn_update = (Button) dialog.findViewById(R.id.btn_update);

        txt_title.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        txt_update.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        btn_update.setTypeface(utils.OpenSans_Bold(PaymentActivity.this));



        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                dialog_AddCard();
            }
        });

        dialog.show();


    }



    private void api_Buy(){
        AndroidNetworking.enableLogging();
        utils.showProgressDialog(PaymentActivity.this,getResources().getString(R.string.pls_wait));
        Log.e("url_buy: ", NetworkingData.BASE_URL+ NetworkingData.BUY_LEAD);


        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.BUY_LEAD)
                .addBodyParameter("lead_id",get_lead_id)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_login: ",result+"");
                        utils.dismissProgressdialog();

                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }




    public  void dialog_card_err(){
        final BottomSheetDialog dialog = new BottomSheetDialog (PaymentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_card_error, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_update = (TextView) dialog.findViewById(R.id.txt_update);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView btn_update = (Button) dialog.findViewById(R.id.btn_update);


        txt_title.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        txt_update.setTypeface(utils.OpenSans_Regular(PaymentActivity.this));
        btn_update.setTypeface(utils.OpenSans_Bold(PaymentActivity.this));

        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }


}
