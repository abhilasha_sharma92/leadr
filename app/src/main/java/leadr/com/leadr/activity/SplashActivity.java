package leadr.com.leadr.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.MyApplication;


public class SplashActivity extends AppCompatActivity {

    int SPLASH_TIMER      = 3000;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    @BindView(R.id.rel_main)
    RelativeLayout rel_main;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        sharedPreferenceLeadr = new SharedPreferenceLeadr(SplashActivity.this);

        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
            rel_main.setBackground(getResources().getDrawable(R.drawable.splash_he));
        }else{
            rel_main.setBackground(getResources().getDrawable(R.drawable.splash));
        }


//        sharedPreferenceLeadr.set_MIN_BUDGET("0");
        sharedPreferenceLeadr.set_FROM_PRICE("0");
        sharedPreferenceLeadr.set_APP_START(CurrentTimeStart());
        sharedPreferenceLeadr.set_APP_START_SESSION(CurrentTimeStart());
        sharedPreferenceLeadr.set_SELECTOR_BOUGHT("0");
        sharedPreferenceLeadr.set_SELECTOR_SELL("0");
//        sharedPreferenceLeadr.set_TOOLTIP("1");

        GlobalConstant.PAGINATION_IDS = "0";
        GlobalConstant.PAGINATION_IDS_BOUGHT = "0";
        GlobalConstant.PAGINATION_IDS_SELL = "0";

        String token = FirebaseInstanceId.getInstance().getToken();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(sharedPreferenceLeadr.getUserId().equals("")) {
                    if(sharedPreferenceLeadr.get_signup().equals("yes")){
                        Intent i_nxt = new Intent(SplashActivity.this, Bfr_Reg_Activity.class);
                        i_nxt.putExtra("phone",sharedPreferenceLeadr.get_number());
                        i_nxt.putExtra("type","reg");
                        startActivity(i_nxt);
                    }else {
                        Intent i = new Intent(SplashActivity.this, GetStartedActivity.class);
                        startActivity(i);
                        finish();
                    }
                }else{
                    Intent i_nxt = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i_nxt);
                    finish();
                }
            }
        }, SPLASH_TIMER);
    }


    public  void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
        } catch (Exception e) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        printHashKey();
//        getLocation(30.7333,76.7794, 20000);
        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en"))
        {
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());

        }else{
            String languageToLoad  = "it";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
        }
//        editor.putString("lang","en");

    }


    String CurrentTimeStart(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    public static void getLocation(double x0, double y0, int radius) {
        Random random = new Random();

        // Convert radius from meters to degrees
        double radiusInDegrees = radius / 111000f;

        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        double new_x = x / Math.cos(Math.toRadians(y0));

        double foundLongitude = new_x + x0;
        double foundLatitude = y + y0;
        System.out.println("Longitude: " + foundLongitude + "  Latitude: " + foundLatitude );
    }
}
