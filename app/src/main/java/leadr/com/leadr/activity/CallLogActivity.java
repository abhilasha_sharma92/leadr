package leadr.com.leadr.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;


import java.util.ArrayList;
import java.util.Date;

import adapter.CallLog_Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import modal.CallDetails;
import utils.GlobalConstant;

public class CallLogActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    @BindView(R.id.recycl_call)
    RecyclerView recycl_call;

    @BindView(R.id.img_back)
    ImageView img_back;

    ArrayList<CallDetails> arr_call = new ArrayList<>();

    CallLog_Adapter adapter_call;

    GlobalConstant utils;
    private static final int URL_LOADER = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log);

        ButterKnife.bind(this);
        utils = new GlobalConstant();

        utils.showProgressDialog(CallLogActivity.this,getResources().getString(R.string.load));
        getLoaderManager().initLoader(URL_LOADER, null, CallLogActivity.this);
    }




    @OnClick(R.id.img_back)
    public void onBack() {
        finish();
    }




    /*RETRIEVE CALL LOGS*/
    public void getLastCallDetails() {
        StringBuffer sb = new StringBuffer();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Cursor managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
                null, null, null);
        if(arr_call.size()<5) {
            int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
            int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
            int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
            int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
            sb.append("Call Details :");
                while (managedCursor.moveToNext()) {
                    if(arr_call.size()<5){
                        String phNumber = managedCursor.getString(number);
                        String callType = managedCursor.getString(type);
                        String callDate = managedCursor.getString(date);
                        Date callDayTime = new Date(Long.valueOf(callDate));
                        String callDuration = managedCursor.getString(duration);
                        String dir = null;
                        int dircode = Integer.parseInt(callType);
                        switch (dircode) {

                            case CallLog.Calls.MISSED_TYPE:
                                dir = "MISSED";
                                break;
                        }
                        CallDetails item = new CallDetails();
                        item.setCall_date(callDayTime + "");
                        item.setCaller_number(phNumber);
                        item.setCall_duration(callDuration);
                        item.setCall_type(dir);
                        item.setCaller_name(getContactName(phNumber));

                        arr_call.add(item);

                        sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
                                + dir + " \nCall Date:--- " + callDayTime
                                + " \nCall duration in sec :--- " + callDuration);
                        sb.append("\n----------------------------------");
                    }else{
                        break;
                    }

                }
                managedCursor.close();
            }else{
                managedCursor.close();
            }

        Log.e("number: ",sb+"");


        utils.dismissProgressdialog();
        LinearLayoutManager lnr_album = new LinearLayoutManager(CallLogActivity.this, LinearLayoutManager.VERTICAL, false);
        recycl_call.setLayoutManager(lnr_album);
        adapter_call = new CallLog_Adapter(CallLogActivity.this,arr_call, new CallLog_Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(int i) {
                SellActivity.number_call = arr_call.get(i).getCaller_number();
                if(!arr_call.get(i).getCaller_name().equalsIgnoreCase(getResources().getString(R.string.unknw))) {
                    SellActivity.number_call_name = arr_call.get(i).getCaller_name();
                }
                finish();
            }
        });
        recycl_call.setAdapter(adapter_call);

    }


    public String getContactName(final String phoneNumber)
    {
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String contactName= getResources().getString(R.string.unknw);
        Cursor cursor=this.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactName=cursor.getString(0);
            }
            cursor.close();
        }

        return contactName;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case URL_LOADER:
                // Returns a new CursorLoader
                return new CursorLoader(
                        this,   // Parent activity context
                        CallLog.Calls.CONTENT_URI,        // Table to query
                        null,     // Projection to return
                        null,            // No selection clause
                        null,            // No selection arguments
                        null             // Default sort order
                );
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        getLastCallDetails();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
