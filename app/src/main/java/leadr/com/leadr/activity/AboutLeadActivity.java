package leadr.com.leadr.activity;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.appevents.AppEventsLogger;
import com.squareup.picasso.Picasso;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fragment.BoughtScrollListFragment;
import leadr.com.leadr.R;
import modal.BuyPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

import static leadr.com.leadr.activity.MainActivity.rel_bottom;
import static leadr.com.leadr.activity.MainActivity.vw_bought;
import static leadr.com.leadr.activity.MainActivity.vw_inbox;
import static leadr.com.leadr.activity.MainActivity.vw_more;
import static leadr.com.leadr.activity.MainActivity.vw_sell;


public class AboutLeadActivity extends AppCompatActivity {

    @BindView(R.id.txt_buy_top)
    TextView txt_buy_top;

    @BindView(R.id.txt_seller)
    TextView txt_seller;

    @BindView(R.id.txt_name_side)
    TextView txt_name_side;

    @BindView(R.id.txt_by_last)
    TextView txt_by_last;

    @BindView(R.id.btn_nxt_sold)
    Button btn_nxt_sold;

    @BindView(R.id.txt_ago)
    TextView txt_ago;

    @BindView(R.id.txt_client_name)
    TextView txt_client_name;

    @BindView(R.id.txt_des_client)
    TextView txt_des_client;

    @BindView(R.id.img_chat)
    ImageView img_chat;

    @BindView(R.id.txt_client_phn)
    TextView txt_client_phn;

    @BindView(R.id.txt_loc)
    TextView txt_loc;

    @BindView(R.id.txt_budget)
    TextView txt_budget;

    @BindView(R.id.txt_play)
    TextView txt_play;

    @BindView(R.id.txt_des)
    TextView txt_des;

    @BindView(R.id.txt_secs)
    TextView txt_secs;

    @BindView(R.id.txt_read_more)
    TextView txt_read_more;

    @BindView(R.id.img_shadow)
    ImageView img_shadow;

    @BindView(R.id.vw_line)
    View vw_line;

    @BindView(R.id.txt_lead_price)
    TextView txt_lead_price;

    @BindView(R.id.txt_by_now)
    TextView txt_by_now;

    @BindView(R.id.txt_pause_audio)
    TextView txt_play_pause;

    @BindView(R.id.txt_play_pause)
    TextView txt_pause_audio;

    @BindView(R.id.progres_load_user)
    ProgressBar progres_load_user;

    @BindView(R.id.progress_one)
    ProgressBar progress_one;

    @BindView(R.id.progress_two)
    ProgressBar progress_two;

    @BindView(R.id.img_one)
    ImageView img_one;


    @BindView(R.id.img_two)
    ImageView img_two;

    @BindView(R.id.lnr_22)
    LinearLayout lnr_22;

    @BindView(R.id.lnr_11)
    LinearLayout lnr_11;

    @BindView(R.id.img_file_desc)
    ImageView img_file_desc;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.img_remove)
    ImageView img_remove;

    @BindView(R.id.txt_top)
    TextView txt_top;

    @BindView(R.id.edt_full_name)
    EditText edt_full_name;

    @BindView(R.id.btnok)
    Button btnok;

    @BindView(R.id.btnok1)
    Button btnok1;

    @BindView(R.id.edt_cvv)
    EditText edt_cvv;

    @BindView(R.id.edt_month)
    EditText edt_month;

    @BindView(R.id.edt_card_no)
    EditText edt_card_no;

    @BindView(R.id.edt_year)
    EditText edt_year;

    @BindView(R.id.songProgressBar)
    SeekBar songProgressBar;

    @BindView(R.id.rel_top_pro)
    RelativeLayout rel_top_pro;

    @BindView(R.id.rel_dialog)
    RelativeLayout rel_dialog;

    @BindView(R.id.rel_bottm)
    RelativeLayout rel_bottm;

    @BindView(R.id.rel_botm_real)
    RelativeLayout rel_botm_real;

    @BindView(R.id.img_cancel)
    ImageView img_cancel;

    @BindView(R.id.img_cancel1)
    ImageView img_cancel1;


    RelativeLayout rel_call;
    ProgressBar progresS_load;
    RelativeLayout rel_start;
    RelativeLayout rel_sold;

    @BindView(R.id.img_user)
    de.hdodenhof.circleimageview.CircleImageView img_user ;

    @BindView(R.id.img_eye)
    de.hdodenhof.circleimageview.CircleImageView img_eye ;

    GlobalConstant        utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;
    String                lead_id,type;
    ArrayList<BuyPojo>    arr_lead = new ArrayList<>();

    private MediaPlayer mMediaPlayer = null;
    private boolean     isPlaying    = false;

    private static CountDownTimer countDownTimer;

    int count_start_pause     = 30;
    int count_start_pause_add = 0;
    int resume_pos            = 0;

    Handler mHandler_seekbar;
    Handler mHandler_timer = new Handler();
    String             phone_no = "";
    BottomSheetDialog dialog_card;
    BottomSheetDialog dialog_per;


    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        if(getIntent().getStringExtra("chat").equalsIgnoreCase("I Buy")){
            type = "0";
            setContentView(R.layout.activity_about_lead);
            ButterKnife.bind(this);
            txt_buy_top.setText(getResources().getString(R.string.lead__Ask));
        }
        else if(getIntent().getStringExtra("chat").equalsIgnoreCase("I Bought")){
            type = "1";
            setContentView(R.layout.activity_about_bought);
            ButterKnife.bind(this);
            rel_call = (RelativeLayout)findViewById(R.id.rel_call);
            txt_buy_top.setText(getResources().getString(R.string.lead__Ask_bought));
        }
        else if(getIntent().getStringExtra("chat").equalsIgnoreCase("I Sold")){
            type = "2";
            setContentView(R.layout.activity_about_sell);
            ButterKnife.bind(this);
            rel_call = (RelativeLayout)findViewById(R.id.rel_call);
            txt_buy_top.setText(getResources().getString(R.string.lead__Ask_sold));
        }
        else if(getIntent().getStringExtra("chat").equalsIgnoreCase("I Sell")){
            type = "3";
            setContentView(R.layout.activity_about_sell);
            ButterKnife.bind(this);
            rel_call = (RelativeLayout)findViewById(R.id.rel_call);
            txt_buy_top.setText(getResources().getString(R.string.lead__Ask_publs));
        }





        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(AboutLeadActivity.this);
        lead_id               = getIntent().getStringExtra("lead_id");
        methodTypeface();


        api_LeadDetail();

        txt_play_pause.setTag("0");

        lnr_11.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                lnr_11.setVisibility(View.GONE);
                lnr_11.setClickable(false);
                lnr_22.setVisibility(View.VISIBLE);
                if(txt_play_pause.getTag().equals("0")){
                    progress_one.setVisibility(View.VISIBLE);
                    progress_two.setVisibility(View.VISIBLE);
                    img_one.setVisibility(View.GONE);
                    img_two.setVisibility(View.GONE);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            progress_one.setVisibility(View.GONE);
                            progress_two.setVisibility(View.GONE);
                            img_one.setVisibility(View.VISIBLE);
                            img_two.setVisibility(View.VISIBLE);
                        }

                    }, 1000);
                    txt_play_pause.setTag("1");
                    onPlay(false);
                }else{
                    txt_play_pause.setTag("0");
                    onPlay(true);
                }
            }
            //
            private void onPlay(boolean isPlaying2){
                if (!isPlaying2) {
                    //currently MediaPlayer is not playing audio
                    if(mMediaPlayer == null) {
                        txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
                        startPlaying(); //start from beginning
                        isPlaying = !isPlaying;
                    } else {
                        txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
                        resumePlaying(); //resume the currently paused MediaPlayer
                    }
                } else {
                    //pause the MediaPlayer
                    txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pause_sell, 0, 0, 0);
                    pausePlaying();
                }
                        /*lnr_11.setVisibility(View.GONE);
//                        lnr_11.setClickable(false);
                        lnr_22.setVisibility(View.VISIBLE);*/
            }

            private void startPlaying() {
                mMediaPlayer = new MediaPlayer();
                try {
                    mMediaPlayer.setDataSource(arr_lead.get(0).getAudio());
                    mMediaPlayer.prepare();

                    count_start_pause_add = 0;
                    count_start_pause = Integer.valueOf(arr_lead.get(0).getTime())+2;

                    songProgressBar.setMax(mMediaPlayer.getDuration());
                    songProgressBar.setProgress(0);
                    mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mMediaPlayer.start();
                        }
                    });
                } catch (IOException e) {
                    Log.e("", "prepare() failed");
                }

                songProgressBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged( SeekBar seekBar, int progress, boolean fromUser ) {

                    }

                    @Override
                    public void onStartTrackingTouch( SeekBar seekBar ) {
                        if(mHandler_seekbar!=null){
                            mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                        }
                    }

                    @Override
                    public void onStopTrackingTouch( SeekBar seekBar ) {
                        if(mHandler_seekbar!=null){
                            mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                        }
                        // forward or backward to certain seconds
                        mMediaPlayer.seekTo(seekBar.getProgress());
                        method_countDown_Seekbar();
                        if(countDownTimer!=null){
                            countDownTimer.cancel();
                        }
                        Log.e("progress seek: ",seekBar.getProgress()+"");
                        Log.e("progress seek2: ",mMediaPlayer.getCurrentPosition()+"");
                        count_start_pause = Integer.valueOf(arr_lead.get(0).getTime())+2-seekBar.getProgress()/1000;
                        method_countDown(Integer.valueOf(arr_lead.get(0).getTime())+1-seekBar.getProgress()/1000);
                    }
                });

                count_start_pause_add = 0;
                method_countDown(Integer.valueOf(arr_lead.get(0).getTime())+1);
                method_countDown_Seekbar();

                mMediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                    @Override
                    public void onSeekComplete(MediaPlayer mp) {

                    }
                });
                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        stopPlaying();
                    }
                });

                //keep screen on while playing audio
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }

            void method_countDown_Seekbar(){
                mHandler_seekbar = new Handler();
                mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);

            }

            Runnable mRunnabl_seekbar = new Runnable() {
                @Override
                public void run() {
                    if(mMediaPlayer != null){
//                Log.e("seejbr: ",mCurrentPosition+"");
                        Log.e("seejbr2: ",mMediaPlayer.getCurrentPosition()+"");
                        songProgressBar.setProgress(mMediaPlayer.getCurrentPosition());
                    }
                    mHandler_seekbar.postDelayed(this, 15);
                }
            };


            void method_countDown(int timer){
                countDownTimer = null;
                countDownTimer = new CountDownTimer(timer*1000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        long millis = millisUntilFinished;
                        //Convert milliseconds into hour,minute and seconds
                        String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) -
                                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                TimeUnit.MILLISECONDS.toSeconds(millis) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

                        txt_play_pause.setText(hms+"");

                        count_start_pause = count_start_pause-1;
                        count_start_pause_add = count_start_pause_add+1;

//                                songProgressBar.setProgress(count_start_pause_add);
                        if(count_start_pause==1){
//                                    songProgressBar.setProgress(count_start_pause_add);
                            stopPlaying();
                        }
                    }
                    public void onFinish() {
                        countDownTimer = null;//set CountDownTimer to null
                    }
                }.start();
            }

            private void updateSeekBar() {
            }

            private void pausePlaying() {
                resume_pos = mMediaPlayer.getCurrentPosition();
                countDownTimer.cancel();
                mMediaPlayer.pause();

                if(mHandler_seekbar!=null){
                    mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                }
            }

            private void resumePlaying() {
//        mMediaPlayer.seekTo(resume_pos);
                mMediaPlayer.start();
                updateSeekBar();
                method_countDown(count_start_pause-1);
                mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);
//        updateSeekBar();
            }

            private void stopPlaying() {
                lnr_11.setClickable(true);
                mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                mMediaPlayer.stop();
                mMediaPlayer.reset();
                mMediaPlayer.release();
                mMediaPlayer = null;
                resume_pos = 0;
                lnr_11.setVisibility(View.VISIBLE);
                lnr_22.setVisibility(View.GONE);

                isPlaying = !isPlaying;
//                        songProgressBar.setProgress(songProgressBar.getMax());

                if (countDownTimer != null) {
                    countDownTimer.cancel();
                }
                txt_play_pause.setTag("0");
                //allow the screen to turn off again once audio is finished playing
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }

            //updating mSeekBar
            private Runnable mRunnable = new Runnable() {
                @Override
                public void run() {
                    if(mMediaPlayer != null){

                        int mCurrentPosition = mMediaPlayer.getCurrentPosition();
                        long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);
                        long seconds = TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition)
                                - TimeUnit.MINUTES.toSeconds(minutes);
                        updateSeekBar();
                    }
                }
            };
        });


        rel_top_pro.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i_pro = new Intent(AboutLeadActivity.this, ProfileOther_Activity.class);
                i_pro.putExtra("id",arr_lead.get(0).getBuy_userid());
                startActivity(i_pro);
            }
        });


        if(!type.equalsIgnoreCase("0")) {
            rel_call.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick( View v ) {
                    phone_no = arr_lead.get(0).getCell_number();
                    if (utils.checkReadCallLogPermission(AboutLeadActivity.this)) {
                        utils.hideKeyboard(AboutLeadActivity.this);
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + phone_no));
                        startActivity(callIntent);
                    } else {
                        ActivityCompat.requestPermissions(
                                AboutLeadActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                1
                        );
                    }
                }
            });
        }
    }



    @Override
    protected void onPause() {
        super.onPause();
        try{
            mHandler_timer.removeCallbacks(runnable);
        }catch (Exception e){}
    }



    @OnClick(R.id.img_back)
    public void onBAck() {
        onBackPressed();
    }


    private void methodTypeface() {
        txt_buy_top.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_seller.setTypeface(utils.OpenSans_Light(AboutLeadActivity.this));
        txt_des.setTypeface(utils.OpenSans_Light(AboutLeadActivity.this));
        txt_client_name.setTypeface(utils.OpenSans_Light(AboutLeadActivity.this));
        txt_ago.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_des_client.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_client_phn.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_loc.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_budget.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_play.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_read_more.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_lead_price.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_by_now.setTypeface(utils.Dina(AboutLeadActivity.this));
    }


    /***Get LEAD Detail**********API**********/
    private void api_LeadDetail() {
        AndroidNetworking.enableLogging();
        Log.e("url_lead_det: ", NetworkingData.BASE_URL + NetworkingData.LEAD_DETAIL);


        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.LEAD_DETAIL)
                .addBodyParameter("user_id", sharedPreferenceLeadr.getUserId())
                .addBodyParameter("lead_id", lead_id)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_leadid: ", result + "");

                        utils.dismissProgressdialog();
                        arr_lead.clear();
                        if (result.optString("status").equals("1")) {
                            try {
                                JSONArray arr = result.getJSONArray("detail");
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject obj = arr.getJSONObject(i);
                                    BuyPojo item = new BuyPojo();

                                    String desc = obj.getString("description");
                                        try {
                                            desc = new String(Base64.decode(desc.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    item.setDescription(desc);

                                    String leaduser = obj.getString("lead_username");
                                        try {
                                            leaduser = new String(Base64.decode(leaduser.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    item.setUser_name(leaduser);

                                    item.setAudio(obj.optString("audio"));
                                    item.setTime(obj.optString("time"));
                                    item.setBudget(obj.optString("budget"));
                                    item.setCategory(obj.optString("category"));
                                    item.setLead_price(obj.optString("lead_price"));
                                    item.setCell_number(obj.optString("cell_number"));
                                    item.setUser_id(obj.optString("user_id"));




                                    item.setType(obj.optString("type"));

                                    try{
                                        String full_dat_tym_buy = obj.optString("created_time");
                                        String[] spiltby_T_buy = full_dat_tym_buy.split("T");
                                        String date_buy = spiltby_T_buy[0].trim();
                                        String tym_wid_Z_buy = spiltby_T_buy[1].trim();
                                        String spiltby_Dot1_buy = tym_wid_Z_buy.replace(".000Z","");
                                        String replace_tym_buy = spiltby_Dot1_buy.replace(":","-");
                                        item.setCreated_date(date_buy);
                                        item.setCreated_time(replace_tym_buy);
                                    }catch (Exception e){}

                                    item.setBuy_status(obj.optString("buy_status"));
                                    item.setLead_swipe_by(obj.optString("swipe_count"));

                                    String loc_nm = obj.getString("location_name");
                                        try {
                                            loc_nm = new String(Base64.decode(loc_nm.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    item.setLocation_name(loc_nm);

                                    String country_nm = obj.getString("country");
                                        try {
                                            country_nm = new String(Base64.decode(country_nm.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    item.setCountry(country_nm);

                                    String add_nm = obj.getString("address");
                                        try {
                                            add_nm = new String(Base64.decode(add_nm.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    item.setAddress(add_nm);

                                    String client_name = obj.optString("client_name");
                                        try {
                                            client_name = new String(Base64.decode(client_name.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    if(client_name!=null){
                                        if(client_name.equalsIgnoreCase(" ")){
                                            client_name = getResources().getString(R.string.unknw);
                                        }else if(client_name.equalsIgnoreCase("")){
                                            client_name = getResources().getString(R.string.unknw);
                                        }
                                    }else{
                                        client_name = getResources().getString(R.string.unknw);
                                    }
                                    item.setClient_name(client_name);

                                    item.setLat(obj.optString("lat"));
                                    item.setLon(obj.optString("lon"));
                                    item.setView_by(obj.optString("view_by"));
                                    item.setLead_swipe_by(obj.optString("view_count"));
                                    item.setRefund_approved(obj.optString("refund_approved"));
                                    item.setRefund_req_for(obj.optString("refund_req_for"));
                                    item.setRefund_req(obj.optString("refund_req"));

                                    String lead_bussinfo =  obj.optString("lead_userbusinessinfo");
                                        try {
                                            lead_bussinfo = new String(Base64.decode(lead_bussinfo.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    item.setBusiness_info(lead_bussinfo);

                                    String lead_bussnm =  obj.optString("lead_userbusinessname");
                                        try {
                                            lead_bussnm = new String(Base64.decode(lead_bussnm.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    item.setBusiness_name(lead_bussnm);

                                    item.setUser_thum(obj.optString("lead_user_thum"));
                                    item.setUser_thum(obj.optString("lead_user_thum"));
                                    item.setLead_id(obj.optString("lead_id"));
                                    item.setClient_name(obj.optString("client_name"));
                                    item.setCell_number(obj.optString("cell_number"));

                                    try{
                                        String full_dat_tym_ref = obj.optString("refund_date");
                                        String[] spiltby_T_ref = full_dat_tym_ref.split("T");
                                        String date_ref = spiltby_T_ref[0].trim();
                                        String tym_wid_Z_ref = spiltby_T_ref[1].trim();
                                        String spiltby_Dot1_ref = tym_wid_Z_ref.replace(".000Z","");
                                        String replace_tym_ref = spiltby_Dot1_ref.replace(":","-");
                                        item.setCreated_date(date_ref);
                                        item.setCreated_time(replace_tym_ref);
                                    }catch (Exception e){}

                                    String usrnm = obj.optString("lead_username");
                                        try {
                                            usrnm = new String(Base64.decode(usrnm.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                    item.setUser_name(usrnm);
                                    arr_lead.add(item);

                                    if(type.equalsIgnoreCase("0")){
                                        method_BuyUI();

                                        RelativeLayout btn_buy               = (RelativeLayout)findViewById(R.id.btn_buy);
                                        progresS_load = (ProgressBar)  findViewById(R.id.progres_load);
                                        rel_start      = (RelativeLayout) findViewById(R.id.rel_start);
                                        rel_sold       = (RelativeLayout) findViewById(R.id.rel_sold);
                                        img_chat       = (ImageView) findViewById(R.id.img_chat);

                                        img_chat.setOnClickListener(new OnClickListener() {
                                            @Override
                                            public void onClick( View v ) {
                                                finish();
                                            }
                                        });
                                        btn_buy.setOnClickListener(new OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if(!arr_lead.get(0).getUser_name().equalsIgnoreCase(getResources().getString(R.string.usr_del))) {
                                                    if (progresS_load != null) {
                                                        progresS_load.setVisibility(View.VISIBLE);
                                                    }
                                                    if (arr_lead.get(0).getLead_price().equals("0")) {
                                                        api_CheckStatus(arr_lead.get(0).getLead_id(), arr_lead.get(0).getClient_name(),
                                                                arr_lead.get(0).getCell_number(), rel_start, rel_sold, arr_lead.get(0).getUser_id(), 0);
                                                    } else if (!arr_lead.get(0).getLead_price().equals("")) {
                                                        api_VaidateCard(arr_lead.get(0).getLead_id(), arr_lead.get(0).getClient_name(),
                                                                arr_lead.get(0).getCell_number(), rel_start, rel_sold, arr_lead.get(0).getUser_id(), 0);
                                                    } else {
                                                        api_VaidateCard(arr_lead.get(0).getLead_id(), arr_lead.get(0).getClient_name(),
                                                                arr_lead.get(0).getCell_number(), rel_start, rel_sold, arr_lead.get(0).getUser_id(), 0);
                                                    }
                                                }
                                            }

                                        });
                                    }

                                    if(type.equalsIgnoreCase("1")){
                                        method_BuyUI();
                                    }

                                    if(type.equalsIgnoreCase("3")){
                                        method_PublishedUI();
                                    }

                                    if(type.equalsIgnoreCase("2")){
                                        method_SOld_UI();
                                    }
                                }
                            }catch (Exception e){}

                            if(arr_lead.size()>0){
                                try{
                                    mHandler_timer.postDelayed(runnable, 4000);
                                }catch (Exception e){}
                            }
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });
    }



    Runnable runnable = new Runnable(){
        @Override
        public void run() {
            if(type.equalsIgnoreCase("2")){
                String ago = method_GetDuration_Sell(arr_lead.get(0).getCreated_date(),arr_lead.get(0).getCreated_time());
                txt_ago.setText(ago);
            }else if(type.equalsIgnoreCase("3")){
                String ago = method_GetDuration_Sell(arr_lead.get(0).getCreated_date(),arr_lead.get(0).getCreated_time());
                txt_ago.setText(ago);
            }else {
                txt_ago.setText(method_date_toseT(0));
            }
            try{
                mHandler_timer.postDelayed(runnable, 40000);
            }catch (Exception e){}
        }
    };


    private void method_BuyUI() {
        txt_seller.setTypeface(utils.OpenSans_Light(AboutLeadActivity.this));
        txt_client_name.setTypeface(utils.OpenSans_Light(AboutLeadActivity.this));
        txt_des_client.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_client_phn.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_budget.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_loc.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_play.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_des.setTypeface(utils.OpenSans_Light(AboutLeadActivity.this));
        txt_lead_price.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_read_more.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_ago.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_by_now.setTypeface(utils.Dina(AboutLeadActivity.this));
        txt_by_last.setTypeface(utils.Dina(AboutLeadActivity.this));
        btn_nxt_sold.setTypeface(utils.Dina(AboutLeadActivity.this));

        if(arr_lead!=null){
            try{
                try{
                    if(!arr_lead.get(0).getUser_thum().equals("")){
                        if(!arr_lead.get(0).getUser_thum().equals("0")){
                            if(arr_lead.get(0).getUser_thum().trim().length()>2){
                                progres_load_user.setVisibility(View.VISIBLE);
                                Picasso.with(AboutLeadActivity.this)
                                        .load(arr_lead.get(0).getUser_thum())
                                        .into(img_user, new com.squareup.picasso.Callback() {
                                            @Override
                                            public void onSuccess() {
                                                progres_load_user.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {
                                                progres_load_user.setVisibility(View.GONE);
                                            }
                                        });
                            }else{
                                progres_load_user.setVisibility(View.GONE);
                            }
                        }else{
                            progres_load_user.setVisibility(View.GONE);
                        }
                    }else{
                        progres_load_user.setVisibility(View.GONE);
                    }
                }catch (Exception e){}

                lnr_22.setVisibility(View.GONE);
                if(arr_lead.get(0).getAudio()!=null){
                    if(arr_lead.get(0).getAudio().trim().equals("")){
                        lnr_11.setVisibility(View.GONE);
                        img_file_desc.setVisibility(View.VISIBLE);
                    }else{
                        lnr_11.setVisibility(View.VISIBLE);
                        img_file_desc.setVisibility(View.INVISIBLE);
                    }
                }else{
                    lnr_11.setVisibility(View.GONE);
                    img_file_desc.setVisibility(View.INVISIBLE);
                }
                try{
                    txt_secs.setText(arr_lead.get(0).getTime()+" "+getResources().getString(R.string.secs));
                }catch (Exception e){}
                txt_client_name.setText(arr_lead.get(0).getUser_name());
                txt_des.setText(arr_lead.get(0).getDescription());
                txt_client_phn.setText(arr_lead.get(0).getCell_number().substring(0,5)+getResources().getString(R.string.xx));
                txt_des_client.setText(arr_lead.get(0).getBusiness_name());
                if(arr_lead.get(0).getLead_price()!=null){
                    if(!arr_lead.get(0).getLead_price().equals("0")){
                        if(!arr_lead.get(0).getLead_price().equals("")){
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                txt_lead_price.setText(getResources().getString(R.string.dollar)+" "
                                        +Math.round(Float.valueOf(arr_lead.get(0).getLead_price())*
                                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                            }else{
                                txt_lead_price.setText(getResources().getString(R.string.dollar)+" "
                                        +arr_lead.get(0).getLead_price());
                            }
                        }else{
                            txt_lead_price.setText(getResources().getString(R.string.free));
                        }
                    }else{
                        txt_lead_price.setText(getResources().getString(R.string.free));
                    }
                }else{
                    txt_lead_price.setText(getResources().getString(R.string.free));
                }

                method_SetReadMore(0);
                method_Budget(0);

                if(arr_lead.get(0).getLat().equals("0")){
                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                }
                else if(arr_lead.get(0).getAddress()!=null){
                    if(!arr_lead.get(0).getAddress().equals("0")){
                        if(!arr_lead.get(0).getAddress().equals("")){
                            txt_loc.setText(arr_lead.get(0).getAddress());
                            txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                        }else{
                            if(arr_lead.get(0).getLocation_name()!=null){
                                if(!arr_lead.get(0).getLocation_name().equals("0")){
                                    if(!arr_lead.get(0).getLocation_name().equals("")){
                                        txt_loc.setText(arr_lead.get(0).getLocation_name());
                                    }else{
                                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                    }
                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }

                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }
                        }
                    }else{
                        if(arr_lead.get(0).getLocation_name()!=null){
                            if(!arr_lead.get(0).getLocation_name().equals("0")){
                                if(!arr_lead.get(0).getLocation_name().equals("")){
                                    txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                    txt_loc.setText(arr_lead.get(0).getLocation_name());
                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }
                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }

                        }else{
                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                        }
                    }
                }else{
                    if(arr_lead.get(0).getLocation_name()!=null){
                        if(arr_lead.get(0).getLocation_name().equals("null")){
                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                        }
                        else if(!arr_lead.get(0).getLocation_name().equals("0")){
                            if(!arr_lead.get(0).getLocation_name().equals("")){
                                txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                txt_loc.setText(arr_lead.get(0).getLocation_name());
                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }
                        }else{
                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                        }

                    }else{
                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                    }
                }
                if(arr_lead.get(0).getBudget().equals("0")){
                    txt_budget.setTextColor(getResources().getColor(R.color.gray_budget));
                    txt_budget.setText(getResources().getString(R.string.bud_unknw));
                }else {
                    if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                        txt_budget.setText(getResources().getString(R.string.dollar)+" "
                                +Math.round(Float.valueOf(arr_lead.get(0).getBudget())*
                                Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                    }else{
                        txt_budget.setText(getResources().getString(R.string.dollar)+" "
                                + arr_lead.get(0).getBudget() + " "+getResources().getString(R.string.budgt_sel));
                    }
                }
                txt_ago.setText(method_date_toseT(0));
            }catch (Exception e){}
        }
    }

    private void method_PublishedUI(){
        txt_seller.setTypeface(utils.OpenSans_Light(AboutLeadActivity.this));
        txt_name_side.setTypeface(utils.OpenSans_Light(AboutLeadActivity.this));
        txt_client_name.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_des_client.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_budget.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_client_phn.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_loc.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_play.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_des.setTypeface(utils.OpenSans_Light(AboutLeadActivity.this));
        txt_lead_price.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_read_more.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_ago.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));

        try {
            txt_secs.setText(arr_lead.get(0).getTime()+" "+getResources().getString(R.string.secs));
        }catch (Exception e){}

        img_eye.setVisibility(View.VISIBLE);
        img_user.setVisibility(View.GONE);
        txt_des_client.setVisibility(View.GONE);
        img_chat.setVisibility(View.GONE);
        txt_client_name.setText(arr_lead.get(0).getLead_swipe_by()+" "+ getResources().getString(R.string.buyr_saw));
        txt_seller.setVisibility(View.GONE);
        txt_seller.setText(getResources().getString(R.string.pubb));
        txt_seller.setTextColor(getResources().getColor(R.color.colorBlack));
        txt_seller.setTypeface(utils.OpenSans_Bold(AboutLeadActivity.this));

        txt_name_side.setText(arr_lead.get(0).getClient_name());
        txt_des.setText(arr_lead.get(0).getDescription());
        txt_client_phn.setText(arr_lead.get(0).getCell_number());
        lnr_22.setVisibility(View.GONE);

        if(arr_lead.get(0).getLead_price()!=null){
            if(!arr_lead.get(0).getLead_price().equals("0")){
                if(!arr_lead.get(0).getLead_price().equals("")){
                    txt_lead_price.setText("$ "+arr_lead.get(0).getLead_price());
                }else{
                    txt_lead_price.setText(getResources().getString(R.string.free));
                }
            }else{
                txt_lead_price.setText(getResources().getString(R.string.free));
            }
        }else{
            txt_lead_price.setText(getResources().getString(R.string.free));
        }

        if(arr_lead.get(0).getAudio()!=null){
            if(arr_lead.get(0).getAudio().trim().equals("")){
                lnr_11.setVisibility(View.GONE);
                img_file_desc.setVisibility(View.VISIBLE);
            }else{
                lnr_11.setVisibility(View.VISIBLE);
                img_file_desc.setVisibility(View.INVISIBLE);
            }
        }else{
            lnr_11.setVisibility(View.GONE);
            img_file_desc.setVisibility(View.INVISIBLE);
        }
        method_SetReadMore(0);
        method_AddLocationSell(0);
        method_Budget(0);

        String ago = method_GetDuration_Sell(arr_lead.get(0).getCreated_date(),arr_lead.get(0).getCreated_time());
        txt_ago.setText(ago);
    }


    private void method_AddLocationSell(int position) {
        if(arr_lead.get(position).getLat().equals("0")){
            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
        }else
        if(arr_lead.get(position).getAddress()!=null){
            if(!arr_lead.get(position).getAddress().equals("0")){
                if(!arr_lead.get(position).getAddress().equals("")){
                    txt_loc.setText(arr_lead.get(position).getAddress());
                    txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                }else{
                    if(arr_lead.get(position).getLocation_name()!=null){
                        if(!arr_lead.get(position).getLocation_name().equals("0")){
                            if(!arr_lead.get(position).getLocation_name().equals("")){
                                txt_loc.setText(arr_lead.get(position).getLocation_name());
                                txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }
                        }else{
                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                        }

                    }else{
                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                    }

                }
            }else{
                if(arr_lead.get(position).getLocation_name()!=null){
                    if(!arr_lead.get(position).getLocation_name().equals("0")){
                        if(!arr_lead.get(position).getLocation_name().equals("")){
                            txt_loc.setText(arr_lead.get(position).getLocation_name());
                            txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                        }else{
                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                        }
                    }else{
                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                    }

                }else{
                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                }
            }
        }else{
            if(arr_lead.get(position).getLocation_name()!=null){
                if(!arr_lead.get(position).getLocation_name().equals("0")){
                    if(!arr_lead.get(position).getLocation_name().equals("")){
                        txt_loc.setText(arr_lead.get(position).getLocation_name());
                    }else{
                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                    }
                }else{
                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                }

            }else{
                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
            }
        }
    }

    private void method_Budget(int position) {
        if(arr_lead.get(position).getBudget().equals("0")){
            txt_budget.setTextColor(getResources().getColor(R.color.gray_budget));
            txt_budget.setText(getResources().getString(R.string.bud_unknw));
        }else {
            if (sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")) {
                txt_budget.setText(getResources().getString(R.string.dollar)+" " + Math.round(Float.valueOf(arr_lead.get(position).getBudget()) *
                        Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
            } else {
                txt_budget.setText(getResources().getString(R.string.dollar)+" " + arr_lead.get(position).getBudget() + " " + getResources().getString(R.string.budgt_sel));
            }
        }
    }

    private void method_SetReadMore(int position) {
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        String gg = arr_lead.get(position).getDescription();
        if(gg.contains("\n")){
            gg = arr_lead.get(position).getDescription().replaceAll("\n","........................................");
        }

        if(screenSize==Configuration.SCREENLAYOUT_SIZE_LARGE){
            int length_desc = arr_lead.get(position).getDescription().length();

            if(gg.length()>150){
                txt_read_more.setVisibility(View.VISIBLE);
                img_shadow.setVisibility(View.VISIBLE);
                vw_line.setVisibility(View.GONE);
            }else{
                txt_read_more.setVisibility(View.INVISIBLE);
                img_shadow.setVisibility(View.GONE);
                vw_line.setVisibility(View.VISIBLE);
            }
        }
        else if(screenSize==Configuration.SCREENLAYOUT_SIZE_NORMAL){
            if(gg.length()>80){
                txt_read_more.setVisibility(View.VISIBLE);
                img_shadow.setVisibility(View.VISIBLE);
                vw_line.setVisibility(View.GONE);
            }else{
                txt_read_more.setVisibility(View.INVISIBLE);
                img_shadow.setVisibility(View.GONE);
                vw_line.setVisibility(View.VISIBLE);
            }
        }
        else if(screenSize==Configuration.SCREENLAYOUT_SIZE_SMALL){
            if(gg.length()>40){
                txt_read_more.setVisibility(View.VISIBLE);
                img_shadow.setVisibility(View.VISIBLE);
                vw_line.setVisibility(View.GONE);
            }else{
                txt_read_more.setVisibility(View.INVISIBLE);
                img_shadow.setVisibility(View.GONE);
                vw_line.setVisibility(View.VISIBLE);
            }
        }
    }

    private String method_date_toseT(int position) {
        String date_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(arr_lead.get(position).getCreated_date()+ " "+ arr_lead.get(position).getCreated_time());
            String get_time =  arr_lead.get(position).getCreated_time();
            String FinalTime=  getDate(arr_lead.get(position).getCreated_date() + " "+get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if(sub_year.equals("1")){
                date_return = getResources().getString(R.string.few_mnth);
            }
            else if(sub_year.equals("2")){
                date_return =getResources().getString(R.string.year_ago);
            }
            else if(Integer.valueOf(dayDifference)>0){
                if(dayDifference.equals("1")){
                    date_return = dayDifference +getResources().getString(R.string.day);
                }else{
                    date_return = dayDifference + getResources().getString(R.string.days);
                }
            }else{
                int hours = (int) (mills / (1000 * 60 * 60));
                int minutes = (int) (mills / (1000 * 60));
                int seconds = (int) (mills / (1000));
                if(hours>0){
                    date_return = hours+" "+getResources().getString(R.string.hr);
                }else if(minutes>0){
                    date_return = minutes+" "+getResources().getString(R.string.min);
                }else{
                    date_return = getResources().getString(R.string.few_sec_full);
                }
            }

        } catch (Exception exception) {
            date_return = getResources().getString(R.string.fewdays);
        }
        return date_return;
    }

    String CurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    String CurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    private String getOnlyDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
            Log.e("time: ",TimeZone.getDefault()+"");
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);
        }
        catch (Exception e) {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }

    private String getDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);

        }
        catch (Exception e)
        {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }


    String method_GetDuration_Sell(final String created_date, final String created_time){
        String date_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_date+ " "+created_time);
            String get_time =  created_time;
            String FinalTime=  getDate(created_date + " "+get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if(sub_year.equals("1")){
                date_return = getResources().getString(R.string.few_mnth);
            }
            else if(sub_year.equals("2")){
                date_return = getResources().getString(R.string.year_ago);
            }
            else if(Integer.valueOf(dayDifference)>0){
                if(dayDifference.equals("1")){
                    date_return = dayDifference+getResources().getString(R.string.day);
                }else{
                    date_return = dayDifference+getResources().getString(R.string.days);
                }
            }else{
                int hours = (int) (mills / (1000 * 60 * 60));
                int minutes = (int) (mills / (1000 * 60));
                int seconds = (int) (mills / (1000));
                if(hours>0){
                    date_return = hours+" "+getResources().getString(R.string.hr);
                }else if(minutes>0){
                    date_return = minutes+" "+getResources().getString(R.string.min);
                }else{
                    date_return = getResources().getString(R.string.few_sec_full);
                }
            }

        } catch (Exception exception) {
            date_return = getResources().getString(R.string.fewdays);
        }
        return date_return;
    }


    private void method_SOld_UI(){
        txt_seller.setTypeface(utils.OpenSans_Light(AboutLeadActivity.this));
        img_eye.setVisibility(View.GONE);
        img_user.setVisibility(View.VISIBLE);
        txt_seller.setVisibility(View.VISIBLE);
        txt_seller.setText(getResources().getString(R.string.sell_buyer));
        txt_des_client.setVisibility(View.VISIBLE);
        img_chat.setVisibility(View.GONE);
        txt_client_name.setTextColor(getResources().getColor(R.color.sellr_gray));
        txt_client_name.setText(arr_lead.get(0).getBuy_username());
        txt_des_client.setText(arr_lead.get(0).getBuy_userbusinessname());

        try {
            txt_secs.setText(arr_lead.get(0).getTime()+" "+getResources().getString(R.string.secs));
        }catch (Exception e){}

        img_eye.setVisibility(View.VISIBLE);
        img_user.setVisibility(View.GONE);
        txt_des_client.setVisibility(View.GONE);
        img_chat.setVisibility(View.GONE);
        txt_client_name.setText(arr_lead.get(0).getLead_swipe_by()+" "+ getResources().getString(R.string.buyr_saw));
        txt_seller.setVisibility(View.GONE);
        txt_seller.setText(getResources().getString(R.string.pubb));
        txt_seller.setTextColor(getResources().getColor(R.color.colorBlack));
        txt_seller.setTypeface(utils.OpenSans_Bold(AboutLeadActivity.this));

        txt_name_side.setText(arr_lead.get(0).getClient_name());
        txt_des.setText(arr_lead.get(0).getDescription());
        txt_client_phn.setText(arr_lead.get(0).getCell_number());
        lnr_22.setVisibility(View.GONE);


        if(arr_lead.get(0).getLead_price()!=null){
            if(!arr_lead.get(0).getLead_price().equals("0")){
                if(!arr_lead.get(0).getLead_price().equals("")){
                    txt_lead_price.setText("$ "+arr_lead.get(0).getLead_price());
                }else{
                    txt_lead_price.setText(getResources().getString(R.string.free));
                }
            }else{
                txt_lead_price.setText(getResources().getString(R.string.free));
            }
        }else{
            txt_lead_price.setText(getResources().getString(R.string.free));
        }

        if(arr_lead.get(0).getAudio()!=null){
            if(arr_lead.get(0).getAudio().trim().equals("")){
                lnr_11.setVisibility(View.GONE);
                img_file_desc.setVisibility(View.VISIBLE);
            }else{
                lnr_11.setVisibility(View.VISIBLE);
                img_file_desc.setVisibility(View.INVISIBLE);
            }
        }else{
            lnr_11.setVisibility(View.GONE);
            img_file_desc.setVisibility(View.INVISIBLE);
        }
        method_SetReadMore(0);
        method_AddLocationSell(0);
        method_Budget(0);

        String ago = method_GetDuration_Sell(arr_lead.get(0).getCreated_date(),arr_lead.get(0).getCreated_time());
        txt_ago.setText(ago);
    }


    @Override
    public void onRequestPermissionsResult( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions){
            if(ActivityCompat.shouldShowRequestPermissionRationale(AboutLeadActivity.this, permission)){
                //denied
                utils.hideKeyboard(AboutLeadActivity.this);
                dialog_denyOne();
            }else{
                if(ActivityCompat.checkSelfPermission(AboutLeadActivity.this, permission) == PackageManager.PERMISSION_GRANTED){
                    //allowed
                    try{
                        mHandler_timer.removeCallbacks(runnable);
                    }catch (Exception e){}
                    utils.hideKeyboard(AboutLeadActivity.this);
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+phone_no));
                    startActivity(callIntent);
                } else{
                    utils.hideKeyboard(AboutLeadActivity.this);
                    dialog_openStoragePer();
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //do something here.
                }
            }
        }
    }


    public  void dialog_denyOne(){
        final BottomSheetDialog dialog = new BottomSheetDialog (AboutLeadActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);

        txt_title.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));

        txt_title.setText(getResources().getString(R.string.phone_per));
        txt_camera.setText(AboutLeadActivity.this.getResources().getString(R.string.stor_all_phone));

        txt_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public  void dialog_openStoragePer(){
        final BottomSheetDialog dialog = new BottomSheetDialog (AboutLeadActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish   = (Button) dialog.findViewById(R.id.btn_finish);
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_press  = (TextView) dialog.findViewById(R.id.txt_press);
        TextView txt_store  = (TextView) dialog.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Bold(AboutLeadActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Bold(AboutLeadActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }

        txt_title.setText(getResources().getString(R.string.phone_per));
        txt_store.setText(getResources().getString(R.string.phone_per_three));

        btn_finish.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    mHandler_timer.removeCallbacks(runnable);
                }catch (Exception e){}
                GlobalConstant.startInstalledAppDetailsActivity(AboutLeadActivity.this);
            }
        });
        txt_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void api_CheckStatus(final String lead_id, final String client_name, final String phone, final View vw_start
            , final View vw_sold, final String userId, final int position){
        AndroidNetworking.enableLogging();

        utils.dismissProgressdialog();
        if(progresS_load!=null){
            if(progresS_load.getVisibility()==View.GONE){
                progresS_load.setVisibility(View.VISIBLE);
            }
        }
        Log.e("url_chkStatus: ", NetworkingData.BASE_URL+ NetworkingData.CHECK_BUY_STATUS);
        Log.e("lead_id: ", lead_id);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_BUY_STATUS)
                .addBodyParameter("lead_id",lead_id)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chkStatus: ",result+"");
                        if(result.optString("status").equals("1")){
                            AppEventsLogger logger = AppEventsLogger.newLogger(AboutLeadActivity.this);
                            logger.logEvent("EVENT_NAME_PURCHASED");
                            api_Buy(lead_id, client_name,
                                    phone,vw_start,vw_sold,userId,position);
                        }else{
                            if(result.optString("message").equalsIgnoreCase("Lead deleted!")){
                                dialog_leadRemoved(AboutLeadActivity.this,getResources().getString(R.string.lead_remv));
                            }
                            else if(result.optString("message").equalsIgnoreCase("Already bought!")){
                                dialog_leadRemoved(AboutLeadActivity.this,getResources().getString(R.string.lead_alrdy));
                            }
                            else if(result.optString("message").contains("in edit mode!")){
                                dialog_leadRemoved(AboutLeadActivity.this,getResources().getString(R.string.lead_remvd));
                            }else{
                                vw_start.setVisibility(View.GONE);
                                vw_sold.setVisibility(View.VISIBLE);
                            }
                            if(progresS_load!=null) {
                                progresS_load.setVisibility(View.GONE);
                            }
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                        if(progresS_load!=null) {
                            progresS_load.setVisibility(View.GONE);
                        }
                    }
                });



    }


    public  void dialog_leadRemoved( Activity activity, String msg){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        dialog.setCancelable(true);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        img_remove.setVisibility(View.GONE);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        try{
            dialog.show();
        }catch (Exception e){

        }

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
            }
        });

    }


    private void api_VaidateCard(final String lead_id, final String client_name, final String phone, final View vw_start
            , final View vw_sold, final String UserId, final int position){
        AndroidNetworking.enableLogging();
        Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.CHECK_CARD);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_CARD)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chkCard: ",result+"");
                        utils.dismissProgressdialog();
                        if(result.optString("status").equals("0")){
                            if(progresS_load!=null) {
                                progresS_load.setVisibility(View.GONE);
                            }
                            sharedPreferenceLeadr.set_CARD("N/A");
                            dialog_AddCard(lead_id, client_name, phone,vw_start,vw_sold,UserId,position);
                        }else{
                            api_CheckStatus(lead_id, client_name, phone,vw_start,vw_sold,UserId, position);
                        }
//
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();
                        if(progresS_load!=null) {
                            progresS_load.setVisibility(View.GONE);
                        }
                    }
                });

    }


    public  void dialog_AddCard(final String lead_id, final String client_name, final String phone, final View vw_start
            , final View vw_sold, final String userId, final int position) {
        rel_dialog.setVisibility(View.VISIBLE);
        rel_bottom.setVisibility(View.GONE);
        vw_line.setVisibility(View.VISIBLE);

        String text = "";
        if(sharedPreferenceLeadr.get_CARD().equalsIgnoreCase("N/A")){
            img_remove.setVisibility(View.GONE);
            text = "<font color=#227cec>"+getResources().getString(R.string.frst_tym)+
                    "</font> <font color=#071a30>"+getResources().getString(R.string.you_must)+"</font>";
        }else{
            img_remove.setVisibility(View.VISIBLE);
            text =
                    "</font> <font color=#071a30>"+getResources().getString(R.string.ur_card)+"</font>";
        }
        txt_top.setText(Html.fromHtml(text));

        try {
            InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.showSoftInput(edt_full_name, 0);
        } catch (Exception e) {
        }
        edt_full_name.requestFocus();

        btnok.setTypeface(utils.Dina(AboutLeadActivity.this));
        btnok1.setTypeface(utils.Dina(AboutLeadActivity.this));
        edt_cvv.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        edt_month.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        edt_year.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        edt_card_no.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        edt_full_name.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_top.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));


        edt_card_no.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        edt_card_no.setLongClickable(false);
        edt_card_no.setTextIsSelectable(false);


        edt_cvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 3) {
                    utils.hideKeyboard(AboutLeadActivity.this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        edt_card_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 16) {
                    edt_month.requestFocus();
                    edt_month.setSelection(edt_month.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edt_month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    String working = s.toString();
                    Log.e("before: ",working+"");
                }catch (Exception e){}
                if (edt_month.getText().toString().matches("^2")|| edt_month.getText().toString().matches("^3")||
                        edt_month.getText().toString().matches("^4")||edt_month.getText().toString().matches("^5")||
                        edt_month.getText().toString().matches("^6")||edt_month.getText().toString().matches("^7")||
                        edt_month.getText().toString().matches("^8")||edt_month.getText().toString().matches("^9")) {
                    // Not allowed
                    edt_month.setText("");
                }else if(s.length() == 2){
                    if (Integer.valueOf(edt_month.getText().toString())>12){
                        edt_month.setText("1");
                        edt_month.setSelection(edt_month.getText().toString().length());
                    }else  if (s.length() == 2) {
                        edt_year.requestFocus();
                        edt_year.setSelection(edt_year.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edt_year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    edt_month.requestFocus();
                    edt_month.setSelection(edt_month.getText().length());
                }else if(s.length() == 4){
                    edt_cvv.requestFocus();
                    edt_cvv.setSelection(edt_cvv.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        /***Keyboard Listener*****/
        KeyboardVisibilityEvent.setEventListener(
                AboutLeadActivity.this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                        if(rel_dialog.getVisibility()==View.VISIBLE) {
                            if (isOpen) {
                                rel_bottm.setVisibility(View.VISIBLE);
                                rel_botm_real.setVisibility(View.GONE);
                            } else {
                                rel_bottm.setVisibility(View.GONE);
                                rel_botm_real.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });


        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Unregistrar unregistrar = KeyboardVisibilityEvent.registerEventListener(
                        AboutLeadActivity.this,
                        new KeyboardVisibilityEventListener() {
                            @Override
                            public void onVisibilityChanged(boolean isOpen) {
                            }
                        });

                unregistrar.unregister();
                rel_dialog.setVisibility(View.GONE);
                rel_bottom.setVisibility(View.VISIBLE);
                vw_line.setVisibility(View.GONE);
            }
        });


        img_cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_cancel.performClick();
            }
        });

        btnok1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                btnok.performClick();
            }
        });

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                method_check_values();
            }

            void method_check_values() {
                utils.hideKeyboard(AboutLeadActivity.this);
                if (edt_full_name.getText().toString().trim().length() > 0) {
                    if (edt_card_no.getText().toString().trim().length() > 0) {
                        if (edt_month.getText().toString().trim().length() == 2) {
                            if (edt_year.getText().toString().trim().length() == 4) {
                                if (edt_cvv.getText().toString().trim().length() > 0) {
                                    Card card = new Card(edt_card_no.getText().toString(), Integer.valueOf(edt_month.getText().toString()),
                                            Integer.valueOf(edt_year.getText().toString()),
                                            edt_cvv.getText().toString());
                                    if (!card.validateCard()) {
                                        utils.hideKeyboard(AboutLeadActivity.this);
                                        if (!card.validateNumber()) {
                                            dialog_msg_show(AboutLeadActivity.this, getResources().getString(R.string.in_cvv),edt_card_no);

                                        } else if (!card.validateExpMonth()) {
                                            dialog_msg_show(AboutLeadActivity.this, getResources().getString(R.string.exp),edt_month);

                                        } else if (!card.validateExpiryDate()) {
                                            dialog_msg_show(AboutLeadActivity.this, getResources().getString(R.string.expyr),edt_year);

                                        } else if (!card.validateCVC()) {
                                            dialog_msg_show(AboutLeadActivity.this, getResources().getString(R.string.cvv_in),edt_cvv);

                                        }
                                    } else {
                                        String substr = edt_card_no.getText().toString().substring(edt_card_no.getText().toString().
                                                length() - 4);
                                        utils.hideKeyboard(AboutLeadActivity.this);
                                        Unregistrar unregistrar = KeyboardVisibilityEvent.registerEventListener(
                                                AboutLeadActivity.this,
                                                new KeyboardVisibilityEventListener() {
                                                    @Override
                                                    public void onVisibilityChanged(boolean isOpen) {
                                                    }
                                                });

                                        unregistrar.unregister();
                                        rel_dialog.setVisibility(View.GONE);
                                        rel_bottom.setVisibility(View.VISIBLE);
                                        vw_line.setVisibility(View.GONE);

                                        if(progresS_load!=null) {
                                            progresS_load.setVisibility(View.VISIBLE);
                                        }
                                        if(dialog_card!=null){
                                            if(dialog_card.isShowing()){
                                                dialog_card.dismiss();
                                            }
                                        }
                                        dialog_CardFrstTYm(AboutLeadActivity.this,getResources().getString(R.string.frst_tym_longr));
                                        method_SendToStripe(card, lead_id,client_name,phone,vw_start,vw_sold,substr,userId,position);
                                    }
                                } else {
                                    dialog_msg_show(AboutLeadActivity.this, getResources().getString(R.string.entr_cvv),edt_cvv);

                                }
                            } else {
                                dialog_msg_show(AboutLeadActivity.this, getResources().getString(R.string.entr_yr),edt_year);

                            }
                        } else {
                            dialog_msg_show(AboutLeadActivity.this, getResources().getString(R.string.entr_mm),edt_month);

                        }
                    } else {
                        dialog_msg_show(AboutLeadActivity.this, getResources().getString(R.string.entr_nmbr),edt_card_no);

                    }
                } else {
                    dialog_msg_show(AboutLeadActivity.this, getResources().getString(R.string.entr_namer),edt_full_name);

                }
            }


            public  void dialog_msg_show(Activity activity, String msg, final EditText edt){
                final BottomSheetDialog dialog = new BottomSheetDialog (activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
                dialog.setContentView(bottomSheetView);
                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        BottomSheetDialog d = (BottomSheetDialog) dialog;
                        FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                        BottomSheetBehavior.from(bottomSheet)
                                .setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                });
                TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
                TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
                TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
                assert txt_msg != null;
                txt_msg.setText(msg);

                assert txt_title != null;
                txt_title.setTypeface(utils.OpenSans_Regular(activity));
                txt_ok.setTypeface(utils.OpenSans_Regular(activity));
                txt_msg.setTypeface(utils.OpenSans_Regular(activity));
                RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
                assert rel_vw_save_details != null;
                rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        try{
                            ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                        }catch (Exception e){}
                        edt.requestFocus();
                    }
                });
                try{
                    dialog.show();
                }catch (Exception e){

                }

                dialog.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss( DialogInterface dialog ) {
                        try{
                            ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                        }catch (Exception e){}
                        edt.requestFocus();
                    }
                });

            }

            void  method_SendToStripe(Card card, final String lead_id, final String client_name, final String phone, final
            View vw_start, final View vw_sold, final String subcard, final String userId, final int position) {
//                utils.showProgressDialog(getActivity(), getResources().getString(R.string.pls_wait));
                //pk_test_AFG6u0uaLgXBKwMkFmd2QtjP // pk_live_r9TQXzdQ0OeTo6NiRxHJQxft
                //pk_test_AFG6u0uaLgXBKwMkFmd2QtjP // pk_test_AFG6u0uaLgXBKwMkFmd2QtjP
                Stripe stripe = new Stripe(AboutLeadActivity.this, "pk_live_r9TQXzdQ0OeTo6NiRxHJQxft");
                stripe.createToken(
                        card,
                        new TokenCallback() {
                            public void onSuccess(Token token) {
                                utils.dismissProgressdialog();
                                if (utils.isNetworkAvailable(AboutLeadActivity.this)) {

//                                    utils.showProgressDialog(getActivity(), getResources().getString(R.string.pls_wait));
                                    api_SaveToken(token.getId(), lead_id,client_name,phone,vw_start,vw_sold,subcard,userId,position);
                                } else {
                                    if(progresS_load!=null) {
                                        progresS_load.setVisibility(View.GONE);
                                    }
                                    if(dialog_card!=null){
                                        if(dialog_card.isShowing()){
                                            dialog_card.dismiss();
                                        }
                                    }
                                    utils.hideKeyboard(AboutLeadActivity.this);
                                    utils.dialog_msg_show(AboutLeadActivity.this, getResources().getString(R.string.no_internet));
                                }
                            }

                            public void onError(Exception error) {
                                // Show localized error message
                                utils.dismissProgressdialog();
                                utils.hideKeyboard(AboutLeadActivity.this);
                                if(dialog_card!=null){
                                    if(dialog_card.isShowing()){
                                        dialog_card.dismiss();
                                    }
                                }
                                if(progresS_load!=null) {
                                    progresS_load.setVisibility(View.GONE);
                                }
                                utils.dialog_msg_show(AboutLeadActivity.this, "Error in Stripe, please try again!!");
                            }
                        }
                );
            }


            private void api_SaveToken(String token, final String lead_id, final String client_name, final String phone,
                                       final View vw_strat, final View vw_sold, final String subcard, final String userId
                    ,final int position) {
                AndroidNetworking.enableLogging();
                utils.dismissProgressdialog();
//                utils.showProgressDialog(getActivity(), getResources().getString(R.string.load));
                Log.e("url_stripe: ", NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN);
//                Log.e("token: ", token);

                AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN)
                        .addBodyParameter("token", token)
                        .addBodyParameter("user_id", sharedPreferenceLeadr.getUserId())
                        .addBodyParameter("last_digits", subcard)
                        .setTag("msg")
                        .setPriority(Priority.HIGH).doNotCacheResponse()
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject result) {
                                Log.e("res_stripe: ", result + "");
                                //{"status":"1","customer":{"id":"cus_C4xJzoWD2YfwOO","object":"customer","account_balance":0,"created":1515128721,"currency":null,"default_source":"card_1BgnOOI0GVRrJIpDo8dLjinv","delinquent":false,"description":"Customer for LeadR app","discount":null,"email":null,"livemode":false,"metadata":{},"shipping":null,"sources":{"object":"list","data":[{"id":"card_1BgnOOI0GVRrJIpDo8dLjinv","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"Visa","country":"US","customer":"cus_C4xJzoWD2YfwOO","cvc_check":"pass","dynamic_last4":null,"exp_month":11,"exp_year":2022,"fingerprint":"kXn425uHw3F6DtGT","funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null}],"has_more":false,"total_count":1,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/subscriptions"}}}

                                if(dialog_card!=null){
                                    if(dialog_card.isShowing()){
                                        dialog_card.dismiss();
                                    }
                                }
                                if (result.optString("status").equals("1")) {
                                    AppEventsLogger logger = AppEventsLogger.newLogger(AboutLeadActivity.this);
                                    logger.logEvent("EVENT_NAME_ADDED_PAYMENT_INFO");
                                    sharedPreferenceLeadr.set_CARD(subcard);
                                    utils.showProgressDialog(AboutLeadActivity.this, getResources().getString(R.string.load));
                                    api_CheckStatus(lead_id,client_name,phone,vw_start,vw_sold,userId, position);
                                } else {
                                    utils.dismissProgressdialog();
                                    if(progresS_load!=null) {
                                        progresS_load.setVisibility(View.GONE);
                                    }
                                    dialog_errorCard(lead_id,client_name,phone,vw_start,vw_sold,userId,position);
                                }
                            }

                            @Override
                            public void onError(ANError error) {
                                Log.i("", "---> On error  ");
                                utils.dismissProgressdialog();
                                if(dialog_card!=null){
                                    if(dialog_card.isShowing()){
                                        dialog_card.dismiss();
                                    }
                                }
                                if(progresS_load!=null) {
                                    progresS_load.setVisibility(View.GONE);
                                }
                            }
                        });

            }
        });

    }


    public  void dialog_errorCard(final String lead_id, final String client_name, final String phone, final View vw_start
            , final View vw_sold, final String userId,final int position){
        final BottomSheetDialog dialog = new BottomSheetDialog (AboutLeadActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_card_error, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        TextView txt_update = (TextView) dialog.findViewById(R.id.txt_update);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView btn_update = (Button) dialog.findViewById(R.id.btn_update);

        txt_title.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_update.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        btn_update.setTypeface(utils.OpenSans_Bold(AboutLeadActivity.this));

        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sharedPreferenceLeadr.set_CARD("N/A");
                dialog_AddCard(lead_id,client_name,phone,vw_start,vw_sold, userId, position);
            }
        });

        dialog.show();


    }

    public  void dialog_CardFrstTYm(Activity activity, String msg){
        dialog_card = new BottomSheetDialog (activity);
        dialog_card.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog_card.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog_card.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog_card.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog_card.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog_card.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        txt_title.setText(getResources().getString(R.string.pls_wait));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog_card.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_card.dismiss();
            }
        });
        dialog_card.show();


    }


    private void api_Buy(final String lead_id, final String client_name, final String phone, final View vw_start
            , final View vw_sold, final String userId,final int position){
        AndroidNetworking.enableLogging();
        Log.e("url_buy: ", NetworkingData.BASE_URL+ NetworkingData.BUY_LEAD);
        Log.e("lead_id: ", lead_id);
        Log.e("lead_user_id: ", userId);
        Log.e("user_id: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.BUY_LEAD)
                .addBodyParameter("lead_id",lead_id)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("lead_user_id",userId)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_buy: ",result+"");
                        if(progresS_load!=null) {
                            progresS_load.setVisibility(View.GONE);
                        }
                        if(result.optString("status").equals("1")){
                            sharedPreferenceLeadr.set_SELECTOR_BOUGHT("0");
                            Bundle parameters = new Bundle();
                            parameters.putString("EVENT_PARAM_DESCRIPTION", arr_lead.get(position).getAddress()+","+
                                    arr_lead.get(position).getBudget()+","+arr_lead.get(position).getDescription()+","+arr_lead.get(position).getCategory());

                            parameters.putString("EVENT_PARAM_LEVEL", method_date_toseT(position));
                            parameters.putString("Value", method_date_toseT(position));

                            AppEventsLogger logger = AppEventsLogger.newLogger(AboutLeadActivity.this);
                            Double price = 0.0;
                            try{
                                price = Double.valueOf(arr_lead.get(position).getLead_price());
                            }catch (Exception e){}

                            parameters.putString("Value", method_date_toseT(position));
                            String value = "";
                            if(arr_lead.get(position).getAudio()!=null){
                                if(!arr_lead.get(position).getAudio().trim().equalsIgnoreCase("")){
                                    value = "voice";
                                }
                            }
                            if(arr_lead.get(position).getDescription()!=null){
                                if(!arr_lead.get(position).getDescription().trim().equalsIgnoreCase("")) {
                                    if (value.equalsIgnoreCase("")) {
                                        value = "text";
                                    } else {
                                        value = value + "text";
                                    }
                                }
                            }
                            parameters.putString("Type_of_lead", value);

                            logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance("USD"));

                            logger.logEvent("EVENT_NAME_PURCHASED",price,
                                    parameters);

                            try{
                                dialog_SuccessBuy(lead_id,client_name,phone,vw_start,vw_sold);
                            }catch (Exception e){}

                        }else if(result.optString("msg").contains("You have problem with stripe")){
                            dialog_errorCard(lead_id,client_name,phone,vw_start,vw_sold,userId, position);
                        }else if(result.optString("msg").equalsIgnoreCase("You have bought three leads within one hour..")){
//                            utils.dialog_msg_show(getActivity(),result.optString("msg"));
                            String da_2 = result.optString("time");
//                            utils.dialog_msg_show(getActivity(),result.optString("msg"));
                            String full_time = getDate(CurrentDate_withoutTime()+" "+da_2);

                            try{
                                Date date1;
                                Date date1_T;
                                Date date2;
                                Date date2_T;

                                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

                                //Setting dates
                                date1 = dates.parse(CurrentDate());
                                date1_T = dates.parse(full_time);

                                long difference = Math.abs(date1.getTime() - date1_T.getTime());
                                int diff_int = 3600000 - Integer.valueOf((int) difference);
                                api_GetCount(Long.valueOf(diff_int));
                                Log.e("full time : ",full_time);
                                Log.e("difference : ",difference+"");
                            }catch (Exception e){}
                            //"time":"10-31-25"
                        }else{
                            vw_start.setVisibility(View.GONE);
                            vw_sold.setVisibility(View.VISIBLE);

                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        if(progresS_load!=null) {
                            progresS_load.setVisibility(View.GONE);
                        }
                        utils.dismissProgressdialog();
                    }

                    private String method_date_toseT(int position) {
                        String date_return = "";
                        try {
                            //Dates to compare
                            String CurrentDate=  CurrentDate();
                            String CurrentTime=  CurrentTime();
                            String FinalDate=  getOnlyDate(arr_lead.get(position).getCreated_date()+ " "+ arr_lead.get(position).getCreated_time());
                            String get_time =  arr_lead.get(position).getCreated_time();
                            String FinalTime=  getDate(arr_lead.get(position).getCreated_date() + " "+get_time);

                            Date date1;
                            Date date1_T;
                            Date date2;
                            Date date2_T;

                            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
                            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

                            //Setting dates
                            date1 = dates.parse(CurrentDate);
                            date1_T = times.parse(FinalTime);
                            date2 = dates.parse(FinalDate);
                            date2_T = times.parse(CurrentTime);

                            //Comparing dates
                            long difference = Math.abs(date1.getTime() - date2.getTime());
                            long differenceDates = difference / (24 * 60 * 60 * 1000);
                            long mills =   date2_T.getTime()-date1_T.getTime();

                            String api_year = FinalDate.split("-")[0];
                            String curr_year = CurrentDate.split("-")[0];
                            String sub_year = "0";

                            try{
                                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
                            }catch (Exception e){}

                            //Convert long to String
                            String dayDifference = Long.toString(differenceDates);

                            if(sub_year.equals("1")){
                                date_return = getResources().getString(R.string.few_mnth);
                            }
                            else if(sub_year.equals("2")){
                                date_return = getResources().getString(R.string.year_ago);
                            }
                            else if(Integer.valueOf(dayDifference)>0){
                                if(dayDifference.equals("1")){
                                    date_return = dayDifference+" "+getResources().getString(R.string.dyss);
                                }else{
                                    date_return = dayDifference+" "+getResources().getString(R.string.dyss);
                                }
                            }else{
                                int hours = (int) (mills / (1000 * 60 * 60));
                                int minutes = (int) (mills / (1000 * 60));
                                int seconds = (int) (mills / (1000));
                                if(hours>0){
                                    date_return = hours+" "+getResources().getString(R.string.hr);
                                }else if(minutes>0){
                                    date_return = minutes+" "+getResources().getString(R.string.min);
                                }else{
                                    date_return = getResources().getString(R.string.few_sec_full);
                                }
                            }

                        } catch (Exception exception) {
                            date_return = getResources().getString(R.string.fewdays);
                        }
                        return date_return;
                    }
                    private void api_GetCount(final long diff){
                        AndroidNetworking.enableLogging();
                        utils.showProgressDialog(AboutLeadActivity.this,getResources().getString(R.string.load));

                        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_LEAD_INHOUR)
                                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                .setTag("msg")
                                .setPriority(Priority.HIGH).doNotCacheResponse()
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject result) {
                                        Log.e("res_count: ",result+"");
                                        utils.dismissProgressdialog();
                                        String lead_count = "3";
                                        lead_count = result.optString("number_of_leads");
                                        dialog_msg_Timer(diff,lead_count);
                                    }
                                    @Override
                                    public void onError(ANError error) {
                                        Log.e("", "---> On error  ");
                                        utils.dismissProgressdialog();
                                        dialog_msg_Timer(Long.valueOf(3),"3");
                                    }
                                });

                    }


                    public  void dialog_msg_Timer(long diff, String lead_count){
                        final BottomSheetDialog dialog = new BottomSheetDialog (AboutLeadActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_custom_popup_timer, null);
                        dialog.setContentView(bottomSheetView);
                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                BottomSheetDialog d = (BottomSheetDialog) dialog;
                                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                BottomSheetBehavior.from(bottomSheet)
                                        .setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        });
                        TextView txt_msg            = (TextView) dialog.findViewById(R.id.txt_msg);
                        TextView txt_title          = (TextView) dialog.findViewById(R.id.txt_title);
                        TextView txt_ok             = (TextView) dialog.findViewById(R.id.txt_ok);
                        final  TextView txt_counter = (TextView) dialog.findViewById(R.id.txt_counter);
                        assert txt_msg != null;
                        txt_msg.setText(getResources().getString(R.string.u_can_buy)+lead_count+
                                getResources().getString(R.string.u_can_buy_nxt));

                        assert txt_title != null;
                        txt_title.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
                        txt_ok.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
                        txt_msg.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
                        txt_counter.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
                        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
                        assert rel_vw_save_details != null;
                        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        try{
                            dialog.show();
                        }catch (Exception e){

                        }



                        new CountDownTimer(diff, 1000) {

                            public void onTick(long millisUntilFinished) {
                                int seconds = (int) (millisUntilFinished / 1000) % 60;
                                int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
                                try{
                                    txt_counter.setText( getResources().getString(R.string.tym_nxt)+twoDigitString(minutes) + " : "
                                            + twoDigitString(seconds));
                                }catch (Exception e){}

                            }

                            public void onFinish() {
                                txt_counter.setText("You can buy it again!");
                            }
                        }.start();

                    }
                    private String twoDigitString(long number) {

                        if (number == 0) {
                            return "00";
                        }

                        if (number / 10 == 0) {
                            return "0" + number;
                        }

                        return String.valueOf(number);
                    }
                });

    }


    public  void dialog_SuccessBuy(final  String lead_id,final  String client_name,final  String phone, final View vw_start,
                                   final View vw_sold){
        dialog_per = new BottomSheetDialog (AboutLeadActivity.this,R.style.Theme_TransparentD);
        dialog_per.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_lead, null);
        dialog_per.setContentView(bottomSheetView);

        dialog_per.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        TextView txt_u_bought     = (TextView)dialog_per.findViewById(R.id.txt_u_bought);
        TextView txt_name         = (TextView)dialog_per.findViewById(R.id.txt_name);
        TextView txt_client_name  = (TextView)dialog_per.findViewById(R.id.txt_client_name);
        TextView txt_phone        = (TextView)dialog_per.findViewById(R.id.txt_phone);
        TextView txt_ur_lead      = (TextView)dialog_per.findViewById(R.id.txt_ur_lead);
        TextView txt_close        = (TextView)dialog_per.findViewById(R.id.txt_close);
        TextView txt_astrik       = (TextView)dialog_per.findViewById(R.id.txt_astrik);
        RelativeLayout rel_close  = (RelativeLayout)dialog_per.findViewById(R.id.rel_close);
        Button btn_call           = (Button)dialog_per.findViewById(R.id.btn_call);

        img_remove.setVisibility(View.GONE);

        txt_u_bought.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_name.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_client_name.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_phone.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_ur_lead.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        txt_close.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));
        btn_call.setTypeface(utils.OpenSans_Regular(AboutLeadActivity.this));

        ChatActivity.get_type_my = "I Bought";
        String text = "<font color=#071a30>"+AboutLeadActivity.this.getResources().getString(R.string.ur_lead)+
                "</font> <font color=#227cec>"+AboutLeadActivity.this.getResources().getString(R.string.my_lead)+"</font>";
        txt_ur_lead.setText(Html.fromHtml(text));

        if(client_name.equalsIgnoreCase("UNKNOWN")){
            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                txt_client_name.setText(getResources().getString(R.string.unknw));
            }else{
                txt_client_name.setText(client_name);
            }
        }else{
            txt_client_name.setText(client_name);
        }

        txt_phone.setText(phone);

        rel_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vw_sold.setVisibility(View.VISIBLE);
                vw_start.setVisibility(View.GONE);
                dialog_per.dismiss();
              /********Bought */


            }
        });


        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vw_sold.setVisibility(View.VISIBLE);
                vw_start.setVisibility(View.GONE);
                phone_no = phone;
                if (utils.checkReadCallLogPermission(AboutLeadActivity.this)) {
                    utils.hideKeyboard(AboutLeadActivity.this);
                    dialog_per.dismiss();

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+phone));
                    startActivity(callIntent);
                } else {
                    dialog_per.dismiss();
                    requestPermissions(
                            new String[]{Manifest.permission.CALL_PHONE},
                            1
                    );
                }


            }
        });

        dialog_per.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {
                vw_sold.setVisibility(View.VISIBLE);
                vw_start.setVisibility(View.GONE);
                dialog_per.dismiss();
            }
        });

        dialog_per.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                vw_sold.setVisibility(View.VISIBLE);
                vw_start.setVisibility(View.GONE);
                dialog_per.dismiss();

            }
        });


        dialog_per.show();


    }


    String CurrentDate_withoutTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
}
