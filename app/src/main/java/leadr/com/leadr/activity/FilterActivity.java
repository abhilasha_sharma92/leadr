package leadr.com.leadr.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.appyvet.materialrangebar.RangeBar;
import com.appyvet.materialrangebar.RangeBarTest;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.zcw.togglebutton.ToggleButton;
import com.zcw.togglebutton.ToggleButton.OnToggleChanged;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import modal.CategoryPojo;
import modal.LatlngPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;


/**
 * Created by Abhilasha on 26/12/2017.
 */

public class FilterActivity extends AppCompatActivity {

    public static ListView lst_cate;

    @BindView(R.id.rel_add_cat)
    RelativeLayout rel_add_cat;

    @BindView(R.id.img_save)
    ImageView img_save;

    @BindView(R.id.txt_filtr)
    TextView txt_filtr;

    @BindView(R.id.txt_des)
    TextView txt_des;

    @BindView(R.id.txt_add_cate)
    TextView txt_add_cate;

    @BindView(R.id.txt_loc_rang)
    TextView txt_loc_rang;

    @BindView(R.id.txt_price_rang)
    TextView txt_price_rang;

    @BindView(R.id.txt_job_loc)
    TextView txt_job_loc;

    @BindView(R.id.txt_online)
    TextView txt_online;

    @BindView(R.id.btn_online)
    TextView btn_online;

    @BindView(R.id.btn_sel_loc)
    TextView btn_sel_loc;

    @BindView(R.id.txt_sel_loc)
    TextView txt_sel_loc;

    @BindView(R.id.vw_line_act)
    View vw_line_act;

    @BindView(R.id.vw_bottom)
    View vw_bottom;

    @BindView(R.id.rel_add_loc)
    RelativeLayout rel_add_loc;

    @BindView(R.id.rel_loc_sel)
    RelativeLayout rel_loc_sel;

    @BindView(R.id.recycl_loc)
    RecyclerView recycl_loc;

    @BindView(R.id.rangeSeekbar)
    com.appyvet.materialrangebar.RangeBarTest rangeSeekbar;

    @BindView(R.id.seekbar_radius)
    RangeBar seekbar_radius;

    @BindView(R.id.rel_txt_range)
    RelativeLayout rel_txt_range;

    @BindView(R.id.seekbar)
    RangeBar seekbar;

    @BindView(R.id.txt_left)
    TextView txt_left;

    @BindView(R.id.txt_ryt)
    TextView txt_ryt;

    @BindView(R.id.txt_left_rad)
    TextView txt_left_rad;

    @BindView(R.id.txt_ryt_rad)
    TextView txt_ryt_rad;

    @BindView(R.id.txt_min_bud)
    TextView txt_min_bud;

    @BindView(R.id.txt_seek_single_left)
    TextView txt_seek_single_left;

    @BindView(R.id.txt_seek_single_ryt)
    TextView txt_seek_single_ryt;

    @BindView(R.id.txt_max_dtd)
    TextView txt_max_dtd;

    @BindView(R.id.txt_hr)
    TextView txt_hr;

    @BindView(R.id.btn_hr)
    Button btn_hr;

    @BindView(R.id.btn_day)
    Button btn_day;

    @BindView(R.id.txt_day)
    TextView txt_day;

    @BindView(R.id.edt_pblsh_dt)
    EditText edt_pblsh_dt;

    @BindView(R.id.rel_dt)
    RelativeLayout rel_dt;

    @BindView(R.id.edt_budget)
    EditText edt_budget;

    @BindView(R.id.edt_to_price)
    EditText edt_to_price;

    @BindView(R.id.rel_focus_price)
    RelativeLayout rel_focus_price;

    @BindView(R.id.rel_focus_budgt)
    RelativeLayout rel_focus_budgt;

    @BindView(R.id.txt_locc_an)
    TextView txt_locc_an;

    @BindView(R.id.img_quest)
    ImageView img_quest;

    @BindView(R.id.txt_price_swtch)
    TextView txt_price_swtch;

    @BindView(R.id.swtch_price_range)
    ToggleButton swtch_price_range;

    @BindView(R.id.rel_price)
    RelativeLayout rel_price;

    @BindView(R.id.txt_bud_switch)
    TextView txt_bud_switch;

    @BindView(R.id.swtch_bud_range)
    ToggleButton swtch_bud_range;

    @BindView(R.id.txt_max_date_switch)
    TextView txt_max_date_switch;

    @BindView(R.id.swtch_date_switch)
    ToggleButton swtch_date_switch;

    @BindView(R.id.rel_date)
    RelativeLayout rel_date;

    @BindView(R.id.txt_job_switch)
    TextView txt_job_switch;

    @BindView(R.id.txt_dimen_count)
    TextView txt_dimen_count;

    @BindView(R.id.swtch_job_loc)
    ToggleButton swtch_job_loc;

    @BindView(R.id.rel_txt_count)
    RelativeLayout rel_txt_count;

    @BindView(R.id.scroll)
    ScrollView scroll;


    GlobalConstant                    utils;
    SharedPreferenceLeadr             sharedPreferenceLeadr;
    public static CategoryListAdapter adapter_cat;
    LocRegister_Adapter               adapter_loc;

    ArrayList<String>                     arr_sel_cat = new ArrayList<>();
    public static ArrayList<CategoryPojo> arr_final_cat = new ArrayList<>();
    ArrayList<LatlngPojo>                 arr_place = new ArrayList<>();

    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private Double latitude = 0.0, longitude = 0.0;

    int from_price      = 0;
    int to_price        = 10;
    int to_price_single = 0;
    int from_rad        = 2;
    int to_rad          = 150;

    String get_cityName, getCountryName,admin_max_price = "200";
    public static String saved_filter = null;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        // bind the view using butterknife
        ButterKnife.bind(this);

        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(FilterActivity.this);

        lst_cate              = (ListView)findViewById(R.id.lst_cate);

        if(utils.isNetworkAvailable(FilterActivity.this)){
            //get category dynamically based on ids saved
            api_GetCategory();
        }else{
            utils.dialog_msg_show(FilterActivity.this, getResources().getString(R.string.no_internet));
        }


        //get admin max lead price...price is fetched in buy fragment due to issue while setting data dynamically
        //on rangeprogress bar - 3rd party library
        if(getIntent().getStringExtra("admin_max_price")!=null) {
            if (!getIntent().getStringExtra("admin_max_price").equalsIgnoreCase("")) {
                admin_max_price = getIntent().getStringExtra("admin_max_price");
            }
        }


        method_SetTypeface();

        vw_line_act.setVisibility(View.GONE);
        rel_add_loc.setVisibility(View.GONE);
        vw_bottom.setVisibility(View.GONE);

        //this method was used before switch structure
        method_SETLOC_ACC_to_Type();

        method_Set_SeekBar_Value();

        seekbar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex, String leftPinValue, String rightPinValue) {
                from_price = leftPinIndex;
                to_price_single = rightPinIndex;
                txt_seek_single_ryt.setText("$ " + rightPinIndex);


            }

        });

        //if location==1 set value on seekbar for radius
       method_SeekBar_Location();

       //set price range-where at start value (max value) is get from admin setting--initialising only
       method_Price_Range_Seekbar();

       //set day-hour UI
       method_HourDay_UI();

       //set saved values in text fields
       method_Set_SharedPref_in_Texts();

        //set price range-where at start value (max value) is get from admin setting--set previous saved value, if any
       method_RangeBar_ShredPref();

       //new additional-1.3...set switch structure for price range
       method_Switch_Price();

        //new additional-1.3...set switch structure for budget
       method_Switch_BUD();

        //new additional-1.3...set switch structure for hour/day
       method_Switch_Date();

        //new additional-1.3...set switch structure for job location
       method_Switch_Job();


    }

    private void method_Switch_Price() {
        if(sharedPreferenceLeadr.get_SWITCH_PRICE().equalsIgnoreCase("0")){
            swtch_price_range.setToggleOff();
            rangeSeekbar.setVisibility(View.GONE);
            rel_price.setVisibility(View.GONE);
        }else{
            swtch_price_range.setToggleOn();
            rangeSeekbar.setVisibility(View.VISIBLE);
            rel_price.setVisibility(View.VISIBLE);
        }

        swtch_price_range.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(!on){
                    rangeSeekbar.setVisibility(View.GONE);
                    rel_price.setVisibility(View.GONE);
                    sharedPreferenceLeadr.set_SWITCH_PRICE("0");
                }else{
                    rangeSeekbar.setVisibility(View.VISIBLE);
                    rel_price.setVisibility(View.VISIBLE);
                    sharedPreferenceLeadr.set_SWITCH_PRICE("1");
                }
            }
        });

    }


    private void method_Switch_BUD() {
        if(sharedPreferenceLeadr.get_SWITCH_BUD().equalsIgnoreCase("0")){
            swtch_bud_range.setToggleOff();
            rel_focus_budgt.setVisibility(View.GONE);
        }else{
            swtch_bud_range.setToggleOn();
            rel_focus_budgt.setVisibility(View.VISIBLE);
        }

        swtch_bud_range.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(!on){
                    utils.hideKeyboard(FilterActivity.this);
                    rel_focus_budgt.setVisibility(View.GONE);
                    sharedPreferenceLeadr.set_SWITCH_BUD("0");
                }else{
                    try{
                        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    }catch (Exception e){}
                    edt_budget.requestFocus();
                    try{
                        edt_budget.setSelection(edt_budget.getText().toString().length());
                    }catch (Exception e){}
                    rel_focus_budgt.setVisibility(View.VISIBLE);
                    sharedPreferenceLeadr.set_SWITCH_BUD("1");

                }
            }
        });

    }


    private void method_Switch_Date() {
        if(sharedPreferenceLeadr.get_SWITCH_DATE().equalsIgnoreCase("0")){
            swtch_date_switch.setToggleOff();
            rel_date.setVisibility(View.GONE);
        }else{
            swtch_date_switch.setToggleOn();
            rel_date.setVisibility(View.VISIBLE);
        }

        swtch_date_switch.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(!on){
                    utils.hideKeyboard(FilterActivity.this);
                    rel_date.setVisibility(View.GONE);
                    sharedPreferenceLeadr.set_SWITCH_DATE("0");
                }else{
                    try{
                        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    }catch (Exception e){}
                    edt_pblsh_dt.requestFocus();
                    try{
                        edt_pblsh_dt.setSelection(edt_pblsh_dt.getText().toString().length());
                    }catch (Exception e){}
                    rel_date.setVisibility(View.VISIBLE);
                    sharedPreferenceLeadr.set_SWITCH_DATE("1");
//                    scroll.fullScroll(ScrollView.FOCUS_DOWN);
                }
            }
        });

    }


    private void method_Switch_Job() {
        if(sharedPreferenceLeadr.get_SWITCH_JOB().equalsIgnoreCase("0")){
            swtch_job_loc.setToggleOff();
            btn_online.setVisibility(View.VISIBLE);
            txt_online.setVisibility(View.GONE);
            btn_sel_loc.setVisibility(View.GONE);
            txt_sel_loc.setVisibility(View.VISIBLE);
            rel_txt_range.setVisibility(View.GONE);
            seekbar_radius.setVisibility(View.GONE);
            rel_txt_count.setVisibility(View.GONE);

            txt_online.setVisibility(View.GONE);
            btn_sel_loc.setVisibility(View.GONE);
            txt_sel_loc.setVisibility(View.VISIBLE);
            rel_loc_sel.setVisibility(View.GONE);
            rel_txt_range.setVisibility(View.GONE);
            seekbar_radius.setVisibility(View.GONE);
            rel_txt_count.setVisibility(View.GONE);

            method_Add_Loc();
        }else{
            swtch_job_loc.setToggleOn();

            txt_online.setVisibility(View.VISIBLE);
            btn_sel_loc.setVisibility(View.VISIBLE);
            txt_sel_loc.setVisibility(View.GONE);
            rel_loc_sel.setVisibility(View.VISIBLE);
            rel_txt_range.setVisibility(View.VISIBLE);
            seekbar_radius.setVisibility(View.VISIBLE);
            rel_txt_count.setVisibility(View.VISIBLE);

            method_Add_Loc();
        }

        swtch_job_loc.setOnToggleChanged(new OnToggleChanged() {
            @Override
            public void onToggle( boolean on ) {
                if(!on){
                    txt_online.performClick();
                    sharedPreferenceLeadr.set_SWITCH_JOB("0");
                }else{
                    txt_sel_loc.performClick();
                    sharedPreferenceLeadr.set_SWITCH_JOB("1");
                }
            }
        });

    }


    private void method_RangeBar_ShredPref() {
        rangeSeekbar.setTickEnd(Float.valueOf(admin_max_price));
        if(sharedPreferenceLeadr.get_TO_PRICE().equals("")){
            to_price = Integer.valueOf(admin_max_price);
            txt_ryt.setText( "$"+admin_max_price);

            rangeSeekbar.setRangePinsByValue(Float.valueOf(sharedPreferenceLeadr.get_FROM_PRICE()),Float.valueOf(admin_max_price));
        }else {
            if(Float.valueOf(sharedPreferenceLeadr.get_TO_PRICE())>Float.valueOf(admin_max_price)){
//                rangeSeekbar.setSeekPinByValue(Float.valueOf(admin_max_price));
                txt_ryt.setText( "$"+admin_max_price);
                to_price = Integer.valueOf(admin_max_price);

                rangeSeekbar.setRangePinsByValue(Float.valueOf(sharedPreferenceLeadr.get_FROM_PRICE()),Float.valueOf(admin_max_price));
            }else{
//                rangeSeekbar.setSeekPinByValue(Float.valueOf(sharedPreferenceLeadr.get_TO_PRICE()));
                to_price = Integer.valueOf(sharedPreferenceLeadr.get_TO_PRICE());
                txt_ryt.setText( "$"+admin_max_price);

                rangeSeekbar.setRangePinsByValue(Float.valueOf(sharedPreferenceLeadr.get_FROM_PRICE()),
                        Float.valueOf(sharedPreferenceLeadr.get_TO_PRICE()));
            }

        }
    }


    private void method_Set_SharedPref_in_Texts() {
        if(!sharedPreferenceLeadr.get_MAX_TIME_COUNT().equalsIgnoreCase("?")){
            edt_pblsh_dt.setText(sharedPreferenceLeadr.get_MAX_TIME_COUNT());
        }

        if(!sharedPreferenceLeadr.get_MIN_BUDGET().equalsIgnoreCase("?")) {
            edt_budget.setText(sharedPreferenceLeadr.get_MIN_BUDGET());
        }


        edt_to_price.setText(sharedPreferenceLeadr.get_TO_PRICE());
        seekbar_radius.setTickStart(2f);
        seekbar_radius.setEnabled(seekbar_radius.isEnabled());
        seekbar_radius.setRangePinsByValue(2f,
                Float.valueOf(sharedPreferenceLeadr.get_TO_RADIUS()));

//        seekbar_radius.setSeekPinByValue(Float.valueOf(sharedPreferenceLeadr.get_TO_RADIUS()));


        if(sharedPreferenceLeadr.get_FROM_PRICE().equalsIgnoreCase("0")){
            txt_left.setText(getResources().getString(R.string.free));
        }

        if(sharedPreferenceLeadr.get_DIMENSION().equalsIgnoreCase("1")){
            txt_ryt_rad.setText( "150"+" "+getResources().getString(R.string.km));

        }else{
            txt_ryt_rad.setText( "150"+" "+getResources().getString(R.string.mi));
        }
    }


    private void method_HourDay_UI() {
        if(sharedPreferenceLeadr.get_HOUR_OR_DAY().equalsIgnoreCase("hour")){
            btn_hr.setVisibility(View.VISIBLE);
            txt_day.setVisibility(View.VISIBLE);
            btn_day.setVisibility(View.GONE);
            txt_hr.setVisibility(View.GONE);
        }else{
            btn_hr.setVisibility(View.GONE);
            txt_day.setVisibility(View.GONE);
            btn_day.setVisibility(View.VISIBLE);
            txt_hr.setVisibility(View.VISIBLE);
        }
    }


    private void method_Price_Range_Seekbar() {
        rangeSeekbar.setOnRangeBarChangeListener(new RangeBarTest.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBarTest rangeBar, int leftPinIndex,
                                              int rightPinIndex, String leftPinValue, String rightPinValue) {
                from_price = leftPinIndex;
                to_price   = Integer.valueOf(rightPinValue);
                if(leftPinIndex==0){
                    txt_left.setText(getResources().getString(R.string.free));
                }
            }

        });
    }


    private void method_SeekBar_Location() {
        seekbar_radius.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex, String leftPinValue, String rightPinValue) {
                from_rad = leftPinIndex;
                to_rad   = Integer.valueOf(rightPinValue);

                if(sharedPreferenceLeadr.get_DIMENSION().equalsIgnoreCase("1")){
                    txt_left_rad.setText( "2"+" "+getResources().getString(R.string.km));
                    txt_dimen_count.setText("+ "+to_rad+" "+getResources().getString(R.string.km)+" "+getResources().getString(R.string.rad));
                }else{
                    txt_left_rad.setText( "2"+" "+getResources().getString(R.string.mi));
                    txt_dimen_count.setText("+ "+to_rad+" "+getResources().getString(R.string.mi)+" "+getResources().getString(R.string.rad));
                }
            }

        });
    }


    private void method_Set_SeekBar_Value() {
        seekbar.setRangeBarEnabled(false);
        seekbar_radius.setRangeBarEnabled(false);
        seekbar.setTemporaryPins(false);
        seekbar.setBarRounded(true);
        rangeSeekbar.setBarRounded(true);
        seekbar_radius.setBarRounded(true);
        seekbar.setBarWeight(getValueInDP(7));
        rangeSeekbar.setBarWeight(getValueInDP(7));
        seekbar_radius.setBarWeight(getValueInDP(7));
        seekbar_radius.setConnectingLineWeight(getValueInDP(7));
        rangeSeekbar.setConnectingLineWeight(getValueInDP(7));
        seekbar.setConnectingLineWeight(getValueInDP(7));
        rangeSeekbar.setMinimumHeight(getValueInDP(155));
    }


    private void method_SETLOC_ACC_to_Type() {
        if(sharedPreferenceLeadr.get_LOC_NAME_TEMP().equals("")){
            if(sharedPreferenceLeadr.get_LOC_TYPE_FILTER().equals("0")){
                btn_online.setVisibility(View.VISIBLE);
                txt_online.setVisibility(View.GONE);
                btn_sel_loc.setVisibility(View.GONE);
                txt_sel_loc.setVisibility(View.VISIBLE);
                rel_txt_range.setVisibility(View.GONE);
                seekbar_radius.setVisibility(View.GONE);
                rel_txt_count.setVisibility(View.GONE);
                swtch_job_loc.setToggleOff();
            }else{
//                method_Add_Loc();
            }
        }
        else if(sharedPreferenceLeadr.get_LOC_NAME_TEMP().equals("0")){
            if(sharedPreferenceLeadr.get_LOC_TYPE_FILTER().equals("0")){
                btn_online.setVisibility(View.VISIBLE);
                txt_online.setVisibility(View.GONE);
                btn_sel_loc.setVisibility(View.GONE);
                txt_sel_loc.setVisibility(View.VISIBLE);
                rel_txt_range.setVisibility(View.GONE);
                seekbar_radius.setVisibility(View.GONE);
                rel_txt_count.setVisibility(View.GONE);
                swtch_job_loc.setToggleOn();
            }else{

//                method_Add_Loc();
            }
        }
        else if(sharedPreferenceLeadr.get_LOC_NAME_TEMP().equals("")){
            if(sharedPreferenceLeadr.get_LOC_TYPE_FILTER().equals("0")){
                btn_online.setVisibility(View.VISIBLE);
                txt_online.setVisibility(View.GONE);
                btn_sel_loc.setVisibility(View.GONE);
                txt_sel_loc.setVisibility(View.VISIBLE);
                rel_txt_range.setVisibility(View.GONE);
                seekbar_radius.setVisibility(View.GONE);
                rel_txt_count.setVisibility(View.GONE);
                swtch_job_loc.setToggleOff();
            }else{

//                method_Add_Loc();
            }
        }else{
//            method_Add_Loc();
        }

        txt_locc_an.setText(getResources().getString(R.string.add_loc));
    }


    private int getValueInDP(int value) {
        int valueInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                value,
                getResources().getDisplayMetrics());
        return valueInDp;
    }


    private void method_SetTypeface() {
        txt_filtr.setTypeface(utils.Hebrew_Regular(FilterActivity.this));
        txt_add_cate.setTypeface(utils.Hebrew_Regular(FilterActivity.this));
        txt_locc_an.setTypeface(utils.Hebrew_Regular(FilterActivity.this));
        txt_des.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_job_loc.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_online.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        btn_online.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        btn_sel_loc.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_sel_loc.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_left.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_ryt.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_min_bud.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_seek_single_ryt.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_seek_single_left.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_max_dtd.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_hr.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        btn_hr.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        btn_day.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        edt_pblsh_dt.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_day.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_left_rad.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_ryt_rad.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        edt_budget.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_loc_rang.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_price_rang.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_price_swtch.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_bud_switch.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_max_date_switch.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_job_switch.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_dimen_count.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
    }


    @OnClick(R.id.txt_sel_loc)
    public void onTxtLocClick() {
        //if location is selcted, if already added, then show previous locations from array
        LinearLayoutManager lnr_album = new LinearLayoutManager(FilterActivity.this, LinearLayoutManager.VERTICAL, false);
        recycl_loc.setLayoutManager(lnr_album);
        adapter_loc = new LocRegister_Adapter(FilterActivity.this,arr_place);
        recycl_loc.setAdapter(adapter_loc);

        btn_online.setVisibility(View.GONE);
        txt_sel_loc.setVisibility(View.GONE);
        rel_loc_sel.setVisibility(View.GONE);
        txt_online.setVisibility(View.VISIBLE);
        btn_sel_loc.setVisibility(View.VISIBLE);
        btn_sel_loc.setVisibility(View.VISIBLE);
        rel_loc_sel.setVisibility(View.VISIBLE);
        rel_txt_range.setVisibility(View.VISIBLE);
        seekbar_radius.setVisibility(View.VISIBLE);
        rel_txt_count.setVisibility(View.VISIBLE);
        if(arr_place.size()==0){
            findPlace(PLACE_AUTOCOMPLETE_REQUEST_CODE);
        }else{
            if(arr_place.size()<5){
                rel_add_loc.setVisibility(View.VISIBLE);
            }else{
                rel_add_loc.setVisibility(View.GONE);
            }
        }
    }


    @OnClick(R.id.txt_online)
    public void onOnline() {
        //when location is removed
        btn_online.setVisibility(View.VISIBLE);
        txt_online.setVisibility(View.GONE);
        btn_sel_loc.setVisibility(View.GONE);
        txt_sel_loc.setVisibility(View.VISIBLE);
        rel_loc_sel.setVisibility(View.GONE);
        seekbar_radius.setVisibility(View.GONE);
        rel_txt_range.setVisibility(View.GONE);
        rel_txt_range.setVisibility(View.GONE);
        rel_txt_count.setVisibility(View.GONE);
    }


    @OnClick(R.id.rel_add_loc)
    public void onAddMore() {
        findPlace(PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }


    @OnClick(R.id.rel_focus_price)
    public void onFocus1() {
        edt_to_price.requestFocus();
        try {
            InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.showSoftInput(edt_to_price, 0);
            edt_to_price.setSelection(edt_to_price.getText().toString().length());
        } catch (Exception e) {
        }
    }


    @OnClick(R.id.rel_dt)
    public void onFocus23() {
        edt_pblsh_dt.requestFocus();
        try {
            InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.showSoftInput(edt_pblsh_dt, 0);
            edt_pblsh_dt.setSelection(edt_pblsh_dt.getText().toString().length());
        } catch (Exception e) {
        }
    }


    @OnClick(R.id.rel_focus_budgt)
    public void onFocus2() {
        edt_budget.requestFocus();
        try {
            InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.showSoftInput(edt_budget, 0);
            edt_budget.setSelection(edt_budget.getText().toString().length());
        } catch (Exception e) {
        }
    }


    public void findPlace(int placeAutocompleteRequestCode) {
        LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
                new LatLng(latitude, longitude), new LatLng(latitude, longitude));

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).build();
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                            .setFilter(typeFilter)
                            .build(FilterActivity.this);
            startActivityForResult(intent, placeAutocompleteRequestCode);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }


    @OnClick(R.id.img_quest)
    public void onGuideClick() {
        //show guide content when clicked on question mark
        utils.hideKeyboard(FilterActivity.this);
        dialog_Show_Guide();
    }


    public  void dialog_Show_Guide( ){
        final BottomSheetDialog dialog = new BottomSheetDialog (FilterActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_filter_quest, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_lead_range = (TextView) dialog.findViewById(R.id.txt_lead_range);
        TextView txt_msg        = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title      = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_min_bud    = (TextView) dialog.findViewById(R.id.txt_min_bud);
        TextView txt_minbudg    = (TextView) dialog.findViewById(R.id.txt_minbudg);


        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
            txt_lead_range.setGravity(Gravity.RIGHT);
            txt_msg.setGravity(Gravity.RIGHT);
            txt_min_bud.setGravity(Gravity.RIGHT);
            txt_minbudg.setGravity(Gravity.RIGHT);
        }

        txt_title.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_min_bud.setTypeface(utils.OpenSans_Bold(FilterActivity.this));
        txt_lead_range.setTypeface(utils.OpenSans_Bold(FilterActivity.this));
        txt_msg.setTypeface(utils.OpenSans_Regular(FilterActivity.this));
        txt_minbudg.setTypeface(utils.OpenSans_Regular(FilterActivity.this));


        dialog.show();
    }


    /***ADD LOCATION**************
     * ******
     * */
    void method_Add_Loc(){
        String temp = sharedPreferenceLeadr.get_LOC_NAME_TEMP();
        if(sharedPreferenceLeadr.get_LOC_NAME_TEMP().equals("")){
            sharedPreferenceLeadr.set_LOC_NAME_TEMP(sharedPreferenceLeadr.get_LOC_NAME());
            sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP(sharedPreferenceLeadr.get_LOC_NAME_COUNTRY());
            sharedPreferenceLeadr.set_LATITUDE_TEMP(sharedPreferenceLeadr.get_LATITUDE());
            sharedPreferenceLeadr.set_LONGITUDE_TEMP(sharedPreferenceLeadr.get_LONGITUDE_TEMP());
            sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP(sharedPreferenceLeadr.get_LOC_NAME_COUNTRY());
            sharedPreferenceLeadr.set_ADDRESS_TEMP(sharedPreferenceLeadr.get_ADDRESS());
        }else if(sharedPreferenceLeadr.get_LOC_NAME_TEMP().equals("0")){
            sharedPreferenceLeadr.set_LOC_NAME_TEMP(sharedPreferenceLeadr.get_LOC_NAME());
            sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP(sharedPreferenceLeadr.get_LOC_NAME_COUNTRY());
            sharedPreferenceLeadr.set_LATITUDE_TEMP(sharedPreferenceLeadr.get_LATITUDE());
            sharedPreferenceLeadr.set_LONGITUDE_TEMP(sharedPreferenceLeadr.get_LONGITUDE_TEMP());
            sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP(sharedPreferenceLeadr.get_LOC_NAME_COUNTRY());
            sharedPreferenceLeadr.set_ADDRESS_TEMP(sharedPreferenceLeadr.get_ADDRESS());
        }
        String loc_sel = sharedPreferenceLeadr.get_LOC_NAME_TEMP();
        String lat_sel = sharedPreferenceLeadr.get_LATITUDE_TEMP();
        String long_sel = sharedPreferenceLeadr.get_LONGITUDE_TEMP();
        String country_sel = sharedPreferenceLeadr.get_LOC_NAME_COUNTRY_TEMP();
        String add_sel = sharedPreferenceLeadr.get_ADDRESS_TEMP();

        if(!loc_sel.equalsIgnoreCase("")) {
            if (loc_sel.contains("@")) {
                String[] animalsArray = loc_sel.split("@");
                String[] longArray = long_sel.split(",");
                String[] latArray = lat_sel.split(",");
                String[] countryArray = country_sel.split("@");
                String[] addressArray = add_sel.split("@");

                for (int k = 0; k < animalsArray.length; k++) {
                    LatlngPojo item = new LatlngPojo();
                    item.setName(animalsArray[ k ].trim());
                    item.setLong_(longArray[ k ].trim());
                    item.setLat(latArray[ k ].trim());
                    item.setCountryname(countryArray[ k ].trim());
                    item.setAddress(addressArray[ k ].trim());

                    arr_place.add(item);
                }
            } else {
                LatlngPojo item = new LatlngPojo();
                item.setName(loc_sel.trim());
                item.setLong_(long_sel.trim());
                item.setLat(lat_sel.trim());
                item.setCountryname(country_sel.trim());
                item.setAddress(add_sel.trim());

                arr_place.add(item);
            }


            LinearLayoutManager lnr_album = new LinearLayoutManager(FilterActivity.this, LinearLayoutManager.VERTICAL, false);
            recycl_loc.setLayoutManager(lnr_album);
            adapter_loc = new LocRegister_Adapter(FilterActivity.this, arr_place);
            recycl_loc.setAdapter(adapter_loc);
            if (arr_place.size() < 5) {
                rel_add_loc.setVisibility(View.VISIBLE);
            } else {
                rel_add_loc.setVisibility(View.GONE);
            }
        }else{
            swtch_job_loc.setToggleOff();
            btn_online.setVisibility(View.VISIBLE);
            txt_online.setVisibility(View.GONE);
            btn_sel_loc.setVisibility(View.GONE);
            txt_sel_loc.setVisibility(View.VISIBLE);
            rel_txt_range.setVisibility(View.GONE);
            seekbar_radius.setVisibility(View.GONE);
            rel_txt_count.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.rel_add_cat)
    public void onChangeCate() {
        //add multiple categories
        Intent i_filter = new Intent(FilterActivity.this, SelCategoryFilterActivity.class);
        startActivity(i_filter);
    }


    @OnClick(R.id.txt_hr)
    public void onHoursel() {
        //if hour is selected
        btn_day.setVisibility(View.GONE);
        txt_day.setVisibility(View.VISIBLE);
        btn_hr.setVisibility(View.VISIBLE);
        txt_hr.setVisibility(View.GONE);
    }


    @OnClick(R.id.txt_day)
    public void onDaysel() {
        //if day is selected
        btn_day.setVisibility(View.VISIBLE);
        txt_day.setVisibility(View.GONE);
        btn_hr.setVisibility(View.GONE);
        txt_hr.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.img_save)
    public void onSave() {
        //save all data on back press or save icon ---data saved at backend too
        if(edt_pblsh_dt.getText().toString().trim().equalsIgnoreCase("0")){
            utils.dialog_msg_show(FilterActivity.this,getResources().getString(R.string.hour_nt_valid));
        }else {
            method_SAVE();
        }
    }


    private void method_SAVE() {
        String lat_tosave="0" , lon_tosave= "0", to_price_tosave="0", from_price_tosave="0", min_budget_tosave="0", cat_id_tosave="0",
                day_tosave = "", hour_tosave = "", radius_tosave="0", radius_type_tosave="km";
        Bundle parameters = new Bundle();

        if(sharedPreferenceLeadr.get_CAT_FILTER_NAME_TEMP().equals("")){
            utils.hideKeyboard(FilterActivity.this);
            utils.dialog_msg_show(FilterActivity.this,getResources().getString(R.string.sel_cat));
        }/*else if(edt_pblsh_dt.getText().toString().trim().length()<1){
           utils.dialog_msg_show(FilterActivity.this,getResources().getString(R.string.max_dt_pub));
       }*/else{
            if(edt_pblsh_dt.getText().toString().trim().length()>0) {
                if (btn_hr.getVisibility() == View.VISIBLE) {
                    if(sharedPreferenceLeadr.get_SWITCH_DATE().equalsIgnoreCase("1")) {
                        hour_tosave = edt_pblsh_dt.getText().toString();
                    }
                    sharedPreferenceLeadr.set_HOUR_OR_DAY("hour");
                } else {
                    if(sharedPreferenceLeadr.get_SWITCH_DATE().equalsIgnoreCase("1")) {
                        day_tosave = edt_pblsh_dt.getText().toString();
                    }
                    sharedPreferenceLeadr.set_HOUR_OR_DAY("day");
                }
                sharedPreferenceLeadr.set_MAX_TIME_COUNT(edt_pblsh_dt.getText().toString());
            }else{
                sharedPreferenceLeadr.set_MAX_TIME_COUNT("?");
                sharedPreferenceLeadr.set_SWITCH_DATE("0");
                if (btn_hr.getVisibility() == View.VISIBLE) {
                    sharedPreferenceLeadr.set_HOUR_OR_DAY("hour");
                } else {
                    sharedPreferenceLeadr.set_HOUR_OR_DAY("day");
                }
            }

            if(edt_budget.getText().toString().trim().length()>0) {
                if (!edt_budget.getText().toString().equalsIgnoreCase("?")) {
                    to_price_single = Integer.valueOf(edt_budget.getText().toString());
                    min_budget_tosave = String.valueOf(to_price_single);
                    if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                        to_price_single = Integer.valueOf(Math.round(Float.valueOf(to_price_single)/
                                Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                        min_budget_tosave = String.valueOf(to_price_single);
                    }
                    sharedPreferenceLeadr.set_MIN_BUDGET(min_budget_tosave);
                    if(sharedPreferenceLeadr.get_SWITCH_BUD().equalsIgnoreCase("0")){
                        min_budget_tosave = "0";
                    }
                }else{
                    sharedPreferenceLeadr.set_MIN_BUDGET("?");
                    sharedPreferenceLeadr.set_SWITCH_BUD("0");
                }
            }else{
                sharedPreferenceLeadr.set_MIN_BUDGET("?");
                sharedPreferenceLeadr.set_SWITCH_BUD("0");
            }
          /* if(edt_to_price.getText().toString().trim().length()>0){
               to_price = Integer.valueOf(edt_to_price.getText().toString());
           }*/
            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                from_price = Integer.valueOf(Math.round(Float.valueOf(from_price)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                to_price = Integer.valueOf(Math.round(Float.valueOf(to_price)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
            }
            if(sharedPreferenceLeadr.get_SWITCH_PRICE().equalsIgnoreCase("1")) {
                from_price_tosave = String.valueOf(from_price);
                to_price_tosave = String.valueOf(to_price);
            }

            sharedPreferenceLeadr.set_FROM_PRICE(String.valueOf(from_price));
            sharedPreferenceLeadr.set_TO_PRICE(String.valueOf(to_price));
         /*  Log.e("print_cat_id: ",sharedPreferenceLeadr.get_CAT_CAT_FILTER_ID_TEMP());
           Log.e("print_cat_name: ",sharedPreferenceLeadr.get_CAT_FILTER_NAME_TEMP());*/
            cat_id_tosave = sharedPreferenceLeadr.get_CAT_CAT_FILTER_ID_TEMP();
            sharedPreferenceLeadr.set_CAT_FILTER(sharedPreferenceLeadr.get_CAT_CAT_FILTER_ID_TEMP());
            sharedPreferenceLeadr.set_CATEGORYNAME_HE_FIL(sharedPreferenceLeadr.get_CAT_CAT_FILTER_ID_TEMP());
            sharedPreferenceLeadr.set_CATEGORYNAME_ENG_FIL(sharedPreferenceLeadr.get_CAT_CAT_FILTER_ID_TEMP());


                if(arr_place!=null){
                    if(arr_place.size()>0){
                        String place = "";
                        for(int i=0; i<arr_place.size(); i++){
                            if(place.equalsIgnoreCase("")){
                                place = "tsert";
                                sharedPreferenceLeadr.set_LONGITUDE_TEMP(arr_place.get(0).getLong_());
                                sharedPreferenceLeadr.set_LATITUDE_TEMP(arr_place.get(0).getLat());
                                sharedPreferenceLeadr.set_LOC_NAME_TEMP(arr_place.get(0).getName());
                                sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP(arr_place.get(0).getCountryname());
                                sharedPreferenceLeadr.set_ADDRESS_TEMP(arr_place.get(0).getAddress());
                            }else{
                                sharedPreferenceLeadr.set_LONGITUDE_TEMP(sharedPreferenceLeadr.get_LONGITUDE_TEMP()+","+arr_place.get(i).getLong_());
                                sharedPreferenceLeadr.set_LATITUDE_TEMP(sharedPreferenceLeadr.get_LATITUDE_TEMP()+","+arr_place.get(i).getLat());
                                sharedPreferenceLeadr.set_LOC_NAME_TEMP(sharedPreferenceLeadr.get_LOC_NAME_TEMP()+"@"+arr_place.get(i).getName());
                                sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP(sharedPreferenceLeadr.get_LOC_NAME_COUNTRY_TEMP()+"@"+arr_place.get(i).getCountryname());
                                sharedPreferenceLeadr.set_ADDRESS_TEMP(sharedPreferenceLeadr.get_ADDRESS_TEMP()+"@"+arr_place.get(i).getAddress());
                            }

                            sharedPreferenceLeadr.set_LOC_TYPE_FILTER("1");
                        }
                        if(sharedPreferenceLeadr.get_SWITCH_JOB().equalsIgnoreCase("1")) {
                            lat_tosave = sharedPreferenceLeadr.get_LATITUDE_TEMP();
                            lon_tosave = sharedPreferenceLeadr.get_LONGITUDE_TEMP();
                        }
                    }else{
                        sharedPreferenceLeadr.set_LONGITUDE_TEMP("0.0");
                        sharedPreferenceLeadr.set_LATITUDE_TEMP("0.0");
                        sharedPreferenceLeadr.set_LOC_NAME_TEMP("0");
                        sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP("0");
                        sharedPreferenceLeadr.set_LOC_TYPE_FILTER("0");
                        sharedPreferenceLeadr.set_ADDRESS_TEMP("0");
                        sharedPreferenceLeadr.set_ADDRESS("0");
                    }
                }else{
                    sharedPreferenceLeadr.set_LONGITUDE_TEMP("0.0");
                    sharedPreferenceLeadr.set_LATITUDE_TEMP("0.0");
                    sharedPreferenceLeadr.set_LOC_NAME_TEMP("0");
                    sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP("0");
                    sharedPreferenceLeadr.set_LOC_TYPE_FILTER("0");
                    sharedPreferenceLeadr.set_ADDRESS_TEMP("0");
                    sharedPreferenceLeadr.set_ADDRESS("0");
                }

            saved_filter = "";
            sharedPreferenceLeadr.set_TO_RADIUS(String.valueOf(to_rad));
            if(sharedPreferenceLeadr.get_SWITCH_JOB().equalsIgnoreCase("1")) {
                radius_tosave = String.valueOf(to_rad);
            }

            parameters.putString("category", sharedPreferenceLeadr.get_CAT_CAT_FILTER_ID_TEMP());
            parameters.putString("location", sharedPreferenceLeadr.get_ADDRESS());
            parameters.putString("price"   , String.valueOf(to_price));

            AppEventsLogger logger = AppEventsLogger.newLogger(FilterActivity.this);

            logger.logEvent("EVENT_NAME_buy_user_CHANGE_FILTER",
                    parameters);
            // i_log.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            method_SaveDataAPI(lat_tosave,lon_tosave,to_price_tosave,from_price_tosave,min_budget_tosave,cat_id_tosave,day_tosave,hour_tosave,
                    radius_tosave,sharedPreferenceLeadr.get_DIMENSION());

        }
    }


    private void method_SaveDataAPI(String lat_tosave,String lon_tosave,String to_price_tosave,String from_price_tosave,
                                    String min_budget_tosave,String cat_id_tosave,String day_tosave,String hour_tosave,
                                    String radius_tosave,String radius_type_tosave) {
        AndroidNetworking.enableLogging();
        utils.showProgressDialog(FilterActivity.this,getResources().getString(R.string.pls_wait));
        Log.e("url_saveFilter: ", NetworkingData.BASE_URL+ NetworkingData.SAVE_FILTER);


        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.SAVE_FILTER)
                .addBodyParameter("lat",lat_tosave)
                .addBodyParameter("lon",lon_tosave)
                .addBodyParameter("to_price",to_price_tosave)
                .addBodyParameter("from_price",from_price_tosave)
                .addBodyParameter("min_budget",min_budget_tosave)
                .addBodyParameter("cat_id",cat_id_tosave)
                .addBodyParameter("day",day_tosave)
                .addBodyParameter("hour",hour_tosave)
                .addBodyParameter("radius",radius_tosave)
                .addBodyParameter("radius_type",radius_type_tosave)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_login: ",result+"");
                        utils.dismissProgressdialog();
                        finish();
//                        overridePendingTransition(R.anim.stay, R.anim.slide_out_up);
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                        finish();
//                        overridePendingTransition(R.anim.stay, R.anim.slide_out_up);
                    }
                });
    }


    @Override
    public void onBackPressed() {
        if(edt_pblsh_dt.getText().toString().trim().equalsIgnoreCase("0")){
            utils.dialog_msg_show(FilterActivity.this,getResources().getString(R.string.hour_nt_valid));
        }else {
            method_SAVE();
        }
    }


    private void api_GetCategory() {
        AndroidNetworking.enableLogging();
        Log.e("url_getCat: ", NetworkingData.BASE_URL + NetworkingData.GET_CATEGORY);

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.GET_CATEGORY)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_getCat: ", result + "");
                        utils.dismissProgressdialog();

                        arr_sel_cat.clear();
                        arr_final_cat.clear();

                        String cat_id_sel = sharedPreferenceLeadr.get_CAT_FILTER();

                        if(cat_id_sel.contains(",")){
                            String[] animalsArray = cat_id_sel.split(",");
                            for(int i=0; i<animalsArray.length; i++){
                                arr_sel_cat.add(animalsArray[i].trim());
                            }
                        }else{
                            arr_sel_cat.add(cat_id_sel.trim());
                        }
                        sharedPreferenceLeadr.set_CAT_FILTER_NAME("");
                        sharedPreferenceLeadr.set_CAT_FILTER_NAME_HE("");
                        sharedPreferenceLeadr.set_CAT_FILTER_NAME_HE_TEMP("");
                        sharedPreferenceLeadr.set_CAT_FILTER_NAME_TEMP("");
                        sharedPreferenceLeadr.set_CAT_FILTER_ID_TEMP(cat_id_sel);

                        if (result.optString("status").equals("1")) {
                            try {
                                JSONArray arr = result.getJSONArray("categories");
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject obj = arr.getJSONObject(i);
                                    for(int j=0; j<arr_sel_cat.size(); j++){
                                        if(obj.getString("id").equals(arr_sel_cat.get(j))){
                                            CategoryPojo item = new CategoryPojo();
                                            item.setCategory(obj.getString("category_name"));
                                            item.setHebrew(obj.getString("hebrew"));
                                            if(sharedPreferenceLeadr.get_CAT_FILTER_NAME().equals("")){
                                                sharedPreferenceLeadr.set_CAT_FILTER_NAME(obj.getString("category_name").trim());
                                                sharedPreferenceLeadr.set_CAT_FILTER_NAME_HE(obj.getString("hebrew").trim());
                                                sharedPreferenceLeadr.set_CAT_FILTER_NAME_TEMP(obj.getString("category_name").trim());
                                                sharedPreferenceLeadr.set_CAT_FILTER_NAME_HE_TEMP(obj.getString("hebrew").trim());
                                            }else{
                                                sharedPreferenceLeadr.set_CAT_FILTER_NAME(sharedPreferenceLeadr.get_CAT_FILTER_NAME().trim()+
                                                        ","+obj.getString("category_name").trim());
                                                sharedPreferenceLeadr.set_CAT_FILTER_NAME_HE(sharedPreferenceLeadr.get_CAT_FILTER_NAME_HE().trim()+
                                                        ","+obj.getString("hebrew").trim());
                                                sharedPreferenceLeadr.set_CAT_FILTER_NAME_TEMP(sharedPreferenceLeadr.get_CAT_FILTER_NAME_TEMP().trim()+
                                                        ","+obj.getString("category_name").trim());
                                                sharedPreferenceLeadr.set_CAT_FILTER_NAME_HE_TEMP(sharedPreferenceLeadr.get_CAT_FILTER_NAME_HE_TEMP().trim()+
                                                        ","+obj.getString("hebrew").trim());
                                            }

                                            item.setId(obj.getString("id"));
                                            item.setChecked(true);
                                            arr_final_cat.add(item);
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter_cat = new CategoryListAdapter(FilterActivity.this, arr_final_cat);
                        lst_cate.setAdapter(adapter_cat);
                        GlobalConstant.setListViewHeightBasedOnItems(lst_cate,arr_final_cat.size());
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }



    /******
     * ADAPTER******
     * */
    class CategoryListAdapter extends BaseAdapter {
        private Context ctx;
        private List<String> arr_temp_id = new ArrayList<>();
        private List<String> arr_temp_name = new ArrayList<>();
        private List<String> arr_temp_he = new ArrayList<>();
        SharedPreferenceLeadr sharedPreferenceLeadr;

        public CategoryListAdapter(Context ctx, ArrayList<CategoryPojo> arr_find_) {
            this.ctx       = ctx;
            sharedPreferenceLeadr = new SharedPreferenceLeadr(ctx);
        }

        @Override
        public int getCount() {
            return arr_final_cat.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view =  LayoutInflater.from(ctx).inflate(R.layout.item_loc_filter, viewGroup, false);

            final ImageView img_remove    = (ImageView)view.findViewById(R.id.img_remove);
            TextView txt_cat              = (TextView)view.findViewById(R.id.txt_loc);


            if(sharedPreferenceLeadr.get_LANGUAGE().equals("en")) {
                txt_cat.setText(arr_final_cat.get(i).getCategory());
            }else{
                txt_cat.setText(arr_final_cat.get(i).getHebrew());
            }

            txt_cat.setTypeface(utils.OpenSans_Regular(FilterActivity.this));


            img_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arr_temp_id.clear();
                    arr_temp_name.clear();
                    arr_temp_he.clear();

                    try{
                        FilterActivity.arr_final_cat.remove(FilterActivity.arr_final_cat.get(i));
                    }catch (Exception e){}
//                arr_find.remove(arr_find.get(i));
                    if(FilterActivity.adapter_cat!=null) {
                        FilterActivity.adapter_cat.notifyDataSetChanged();
                    }
                    GlobalConstant.setListViewHeightBasedOnItems(lst_cate,FilterActivity.arr_final_cat.size());
                    if(FilterActivity.arr_final_cat!=null){
                        if(FilterActivity.arr_final_cat.size()==0){
                            sharedPreferenceLeadr.set_CAT_FILTER_ID_TEMP("");
                            sharedPreferenceLeadr.set_CAT_FILTER_NAME_TEMP("");
                            sharedPreferenceLeadr.set_CAT_FILTER_NAME_HE_TEMP("");
                            Intent i_filter = new Intent(FilterActivity.this, SelCategoryFilterActivity.class);
                            startActivity(i_filter);
                        }else{
                            for(int k=0; k<FilterActivity.arr_final_cat.size(); k++){
                                arr_temp_id.add(FilterActivity.arr_final_cat.get(k).getId().trim());
                                arr_temp_name.add(FilterActivity.arr_final_cat.get(k).getCategory().trim());
                                arr_temp_he.add(FilterActivity.arr_final_cat.get(k).getHebrew().trim());
                            }

                            sharedPreferenceLeadr.set_CAT_FILTER_NAME_TEMP(TextUtils.join(",", arr_temp_name));
                            sharedPreferenceLeadr.set_CAT_FILTER_ID_TEMP(TextUtils.join(",", arr_temp_id));
                            sharedPreferenceLeadr.set_CAT_FILTER_NAME_HE_TEMP(TextUtils.join(",", arr_temp_he));
                        }
                    }

                }
            });
            return view;
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(FilterActivity.this, data);

                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")) {
                    method_Loc_API(place.getId(), place.getLatLng().latitude, place.getLatLng().longitude,"en");
                }else{
                    method_Loc_API(place.getId(), place.getLatLng().latitude, place.getLatLng().longitude,"he");
                }



            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(FilterActivity.this, data);
                // TODO: Handle the error.
                Log.i("", status.getStatusMessage());
                if(arr_place.size()>0){
//                    rel_add_loc.setVisibility(View.VISIBLE);
                    rel_add_loc.setVisibility(View.GONE);
                    vw_line_act.setVisibility(View.GONE);
                    vw_bottom.setVisibility(View.GONE);
                    vw_line_act.setVisibility(View.GONE);
                }else{
                    sharedPreferenceLeadr.set_SWITCH_JOB("0");
                    swtch_job_loc.setToggleOff();
                    btn_online.setVisibility(View.VISIBLE);
                    txt_sel_loc.setVisibility(View.VISIBLE);
                    txt_online.setVisibility(View.GONE);
                    btn_sel_loc.setVisibility(View.GONE);
                    rel_add_loc.setVisibility(View.GONE);
                    vw_bottom.setVisibility(View.GONE);
                    vw_line_act.setVisibility(View.GONE);
                    rel_loc_sel.setVisibility(View.GONE);
                    rel_txt_range.setVisibility(View.GONE);
                    seekbar_radius.setVisibility(View.GONE);
                    rel_txt_count.setVisibility(View.GONE);
                }


            } else if (resultCode == RESULT_CANCELED) {
                if(arr_place.size()>0){
                    rel_add_loc.setVisibility(View.GONE);
                    vw_line_act.setVisibility(View.GONE);
                    rel_add_loc.setVisibility(View.GONE);
                    vw_bottom.setVisibility(View.GONE);
                    vw_line_act.setVisibility(View.GONE);
                }else{
                    sharedPreferenceLeadr.set_SWITCH_JOB("0");
                    swtch_job_loc.setToggleOff();
                    btn_online.setVisibility(View.VISIBLE);
                    txt_sel_loc.setVisibility(View.VISIBLE);
                    txt_online.setVisibility(View.GONE);
                    btn_sel_loc.setVisibility(View.GONE);
                    rel_add_loc.setVisibility(View.GONE);
                    vw_bottom.setVisibility(View.GONE);
                    vw_line_act.setVisibility(View.GONE);
                    rel_loc_sel.setVisibility(View.GONE);
                    rel_txt_range.setVisibility(View.GONE);
                    seekbar_radius.setVisibility(View.GONE);
                    rel_txt_count.setVisibility(View.GONE);
                }

            }
        }
    }

    private void method_Loc_API( String id, final double latitude, final double longitude,String language ) {
        AndroidNetworking.enableLogging();
        Log.e("url_locApi: ", NetworkingData.BASEURL_LOCATION+ id+"&key="+getResources().getString(R.string.gmail_key)
                +"&language="+language);

        AndroidNetworking.get(NetworkingData.BASEURL_LOCATION+ id+"&key="+getResources().getString(R.string.gmail_key)
                +"&language="+language)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_locApi: ",result+"");
                        //{"status":1,"message":"Details..","admin_settings":[{"id":1,"admin_id":1,"number_of_leads":4,"min_amount":"1
                        //  ","max_lead_price":"100","market_fee":"44"}]}

                        try{
                            JSONObject arr = result.getJSONObject("result");
                            String full_add = arr.getString("formatted_address");
                            Log.e("add: ",full_add);
                            LatlngPojo pojo = new LatlngPojo();
                            pojo.setName(full_add);
                            pojo.setLat(latitude+"");
                            pojo.setLong_(longitude+"");
                            pojo.setCountryname(full_add);
                            pojo.setAddress(full_add);
                            arr_place.add(pojo);
                        }catch (Exception e){
                            String g = e.toString();
                        }
                        utils.dismissProgressdialog();

                       method_Set_Data();
                    }

                    private void method_Set_Data() {
                        if(arr_place.size()>0){
//                    rel_add_loc.setVisibility(View.VISIBLE);
                            vw_line_act.setVisibility(View.GONE);
                            rel_add_loc.setVisibility(View.GONE);
                            vw_bottom.setVisibility(View.GONE);
                            vw_line_act.setVisibility(View.GONE);
                            rel_loc_sel.setVisibility(View.VISIBLE);
                        }else{
                            btn_online.setVisibility(View.VISIBLE);
                            txt_sel_loc.setVisibility(View.VISIBLE);
                            txt_online.setVisibility(View.GONE);
                            btn_sel_loc.setVisibility(View.GONE);
                            rel_add_loc.setVisibility(View.GONE);
                            vw_bottom.setVisibility(View.GONE);
                            vw_line_act.setVisibility(View.GONE);
                            rel_loc_sel.setVisibility(View.GONE);
                            rel_txt_range.setVisibility(View.GONE);
                            seekbar_radius.setVisibility(View.GONE);
                            rel_txt_count.setVisibility(View.GONE);
                        }

                        recycl_loc.setVisibility(View.VISIBLE);
                        LinearLayoutManager lnr_album = new LinearLayoutManager(FilterActivity.this, LinearLayoutManager.VERTICAL, false);
                        recycl_loc.setLayoutManager(lnr_album);
                        adapter_loc = new LocRegister_Adapter(FilterActivity.this,arr_place);
                        recycl_loc.setAdapter(adapter_loc);

                        if(arr_place.size()<5){
                            rel_add_loc.setVisibility(View.VISIBLE);
                        }else{
                            rel_add_loc.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();

                    }
                });
    }


    public void getAddress(double lat, double lng, boolean isEdit,String address) {
        Geocoder geocoder = new Geocoder(FilterActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            getCountryName = obj.getCountryName();

            try {
                get_cityName = obj.getSubAdminArea().toString();
            } catch (Exception e) {
                e.printStackTrace();
                get_cityName = obj.getLocality();
            }


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    public class LocRegister_Adapter extends RecyclerView.Adapter<LocRegister_Adapter.MyViewHolder> {

        Activity ctx;
        LocRegister_Adapter.MyViewHolder holderItem;

        public LocRegister_Adapter(Activity ctx, ArrayList<LatlngPojo> arr_place) {
            this.ctx = ctx;
        }


        @Override
        public LocRegister_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(ctx).inflate(R.layout.item_loc_filter, parent, false);
            holderItem = new LocRegister_Adapter.MyViewHolder(itemView);

            return new LocRegister_Adapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(LocRegister_Adapter.MyViewHolder holder, int position) {
            holder.bind(position);
        }


        @Override
        public int getItemCount() {
            return arr_place.size();
        }


        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView  txt_loc;
            ImageView img_remove;
            View      vw_line;


            public MyViewHolder(View view) {
                super(view);

                txt_loc    = (TextView) view.findViewById(R.id.txt_loc);
                img_remove = (ImageView) view.findViewById(R.id.img_remove);
                vw_line    = (View) view.findViewById(R.id.vw_line);

                if (view.getTag() != null) {
                    view.setTag(holderItem);
                }
            }

            public void bind(final int i) {
                try{
                    if(!arr_place.get(i).getCountryname().equals("")){
                        txt_loc.setText(arr_place.get(i).getName()+", "+arr_place.get(i).getCountryname());
                    }else {
                        txt_loc.setText(arr_place.get(i).getName());
                    }

                }catch (Exception e){
                    txt_loc.setText(arr_place.get(i).getName());
                }
                txt_loc.setText(arr_place.get(i).getAddress());
                vw_line.setVisibility(View.GONE);

                img_remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        arr_place.remove(arr_place.get(i));
                        notifyDataSetChanged();

                        if(arr_place.size()>0){
                            rel_add_loc.setVisibility(View.GONE);
                            vw_line_act.setVisibility(View.GONE);
                            vw_bottom.setVisibility(View.GONE);
                            vw_line_act.setVisibility(View.GONE);
                            rel_loc_sel.setVisibility(View.VISIBLE);
                        }else{
//                            swtch_job_loc.setToggleOff();
                            btn_online.setVisibility(View.VISIBLE);
                            txt_sel_loc.setVisibility(View.VISIBLE);
                            txt_online.setVisibility(View.GONE);
                            btn_sel_loc.setVisibility(View.GONE);
                            rel_add_loc.setVisibility(View.GONE);
                            vw_bottom.setVisibility(View.GONE);
                            vw_line_act.setVisibility(View.GONE);
                            rel_loc_sel.setVisibility(View.GONE);
                            rel_txt_range.setVisibility(View.GONE);
                            seekbar_radius.setVisibility(View.GONE);
                            rel_txt_count.setVisibility(View.GONE);
                            findPlace(PLACE_AUTOCOMPLETE_REQUEST_CODE);
                        }
                        if(arr_place.size()<5){
                            rel_add_loc.setVisibility(View.VISIBLE);
                        }else{
                            rel_add_loc.setVisibility(View.GONE);
                        }
//                        method_false();
                    }
                });
            }
        }
    }


}
