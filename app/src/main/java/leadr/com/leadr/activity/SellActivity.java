package leadr.com.leadr.activity;

import android.Manifest;
import android.Manifest.permission;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import leadr.com.leadr.R;
import leadr.com.leadr.activity.RegistrationActivity.LocRegister_Adapter;
import modal.CallDetails;
import modal.CategoryPojo;
import modal.LatlngPojo;
import pref.SharedPreferenceLeadr;
import recording.DBHelper;
import recording.RecordingItem;
import recording.RecordingService;
import utils.Constants;
import utils.GlobalConstant;
import utils.NetworkingData;
import utils.Util;


/**
 * Created by Abhilasha on 5/12/2017.
 */

public class SellActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
             GoogleApiClient.ConnectionCallbacks{

    @BindView(R.id.txt_online)
    TextView txt_online;

    @BindView(R.id.txt_sel_loc)
    TextView txt_sel_loc;

    @BindView(R.id.btn_online)
    Button btn_online;

    @BindView(R.id.btn_sel_loc)
    Button btn_sel_loc;

    @BindView(R.id.btn_free)
    Button btn_free;

    @BindView(R.id.btn_2)
    Button btn_2;

    @BindView(R.id.btn_3)
    Button btn_3;

    @BindView(R.id.btn_4)
    Button btn_4;

    @BindView(R.id.btn_5)
    Button btn_5;

    @BindView(R.id.btn_6)
    Button btn_6;

    @BindView(R.id.btn_7)
    Button btn_7;

    @BindView(R.id.btn_8)
    Button btn_8;

    @BindView(R.id.btn_change_cat)
    Button btn_change_cat;

    @BindView(R.id.btn_publish)
    Button btn_publish;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.txt_cate)
    TextView txt_cate;

    @BindView(R.id.txt_doll)
    TextView txt_doll;

    @BindView(R.id.edt_budget)
    EditText edt_budget;

    @BindView(R.id.edt_buss_info)
    EditText edt_buss_info;

    @BindView(R.id.edt_name)
    EditText edt_name;

    @BindView(R.id.edt_phone)
    EditText edt_phone;

    @BindView(R.id.lnr_scroll)
    LinearLayout lnr_scroll;

    @BindView(R.id.scroll)
    ScrollView scroll;

    @BindView(R.id.edt_lead_price)
    EditText edt_lead_price;

    @BindView(R.id.rel_loc_sel)
    RelativeLayout rel_loc_sel;

    @BindView(R.id.rel_add_loc)
    RelativeLayout rel_add_loc;

    @BindView(R.id.rel_focus)
    RelativeLayout rel_focus;

    @BindView(R.id.rel_add_audio)
    RelativeLayout rel_add_audio;

    @BindView(R.id.rel_record_audio)
    LinearLayout rel_record_audio;

    @BindView(R.id.rel_play_audio)
    LinearLayout rel_play_audio;

    @BindView(R.id.rel_pause_audio)
    LinearLayout rel_pause_audio;

    @BindView(R.id.recycl_loc)
    RecyclerView recycl_loc;

    @BindView(R.id.txt_timer)
    TextView txt_timer;

    @BindView(R.id.txt_play)
    TextView txt_play;

    @BindView(R.id.txt_pause)
    TextView txt_pause;

    @BindView(R.id.txt_des)
    TextView txt_des;

    @BindView(R.id.txt_creat)
    TextView txt_creat;

    @BindView(R.id.txt_ms)
    TextView txt_ms;

    @BindView(R.id.txt_budg)
    TextView txt_budg;

    @BindView(R.id.txt_locc)
    TextView txt_locc;

    @BindView(R.id.txt_catt)
    TextView txt_catt;

    @BindView(R.id.txt_inf)
    TextView txt_inf;

    @BindView(R.id.txt_led)
    TextView txt_led;

    @BindView(R.id.txt_locc_an)
    TextView txt_locc_an;

    @BindView(R.id.btn_lastcall)
    Button btn_lastcall;

    @BindView(R.id.vw_line_act)
    View vw_line_act;

    @BindView(R.id.vw_bottom)
    View vw_bottom;

    @BindView(R.id.mChronometer)
    Chronometer mChronometer;

    @BindView(R.id.txt_record)
    TextView txt_record;

    @BindView(R.id.mChronometer_play)
    Chronometer mChronometer_play;

    String final_secs = "0";

    @BindView(R.id.mChronometer_pause)
    Chronometer mChronometer_pause;

    @BindView(R.id.lnr_loc)
    LinearLayout lnr_loc;

    @BindView(R.id.card_to_send)
    CardView card_to_send;

    @BindView(R.id.txt_pause_audio)
    TextView txt_pause_audio;

    @BindView(R.id.songProgressBar)
    SeekBar songProgressBar;

    @BindView(R.id.seek_pause)
    SeekBar seek_pause;

    @BindView(R.id.img_quest)
    ImageView img_quest;

    @BindView(R.id.img_pause_minus)
    ImageView img_pause_minus;

    @BindView(R.id.img_play_minus)
    ImageView img_play_minus;

    @BindView(R.id.img_record_minus)
    ImageView img_record_minus;

    @BindView(R.id.lnr_sell)
    LinearLayout lnr_sell;

    @BindView(R.id.lnr_buy_final)
    LinearLayout lnr_buy_final;

    @BindView(R.id.lnr_11)
    LinearLayout lnr_11;

    @BindView(R.id.img_one)
    ImageView img_one;

    @BindView(R.id.img_shadow)
    ImageView img_shadow;

    @BindView(R.id.vw_line)
    View vw_line;

    @BindView(R.id.txt_lead_price)
    TextView txt_lead_price;

    @BindView(R.id.btn_buy)
    Button btn_buy;

    @BindView(R.id.txt_seller)
    TextView txt_seller;

    @BindView(R.id.txt_ago)
    TextView txt_ago;

    @BindView(R.id.txt_client_name)
    TextView txt_client_name;

    @BindView(R.id.txt_des_client)
    TextView txt_des_client;

    @BindView(R.id.txt_client_phn)
    TextView txt_client_phn;

    @BindView(R.id.txt_loc)
    TextView txt_loc;

    @BindView(R.id.txt_budget)
    TextView txt_budget;

    @BindView(R.id.img_file_desc)
    ImageView img_file_desc;

    @BindView(R.id.txt_play_buy)
    TextView txt_play_buy;

    @BindView(R.id.txt_secs)
    TextView txt_secs;

    @BindView(R.id.txt_des_buy)
    TextView txt_des_buy;

    @BindView(R.id.txt_read_more)
    TextView txt_read_more;

    @BindView(R.id.frame_main)
    FrameLayout frame_main;

    @BindView(R.id.lnr_price3)
    RelativeLayout lnr_price3;

    @BindView(R.id.progress_share)
    ProgressBar progress_share;

    int online  = 0;
    int timer   = 0;
    int timer2  = 0;

    private static final int  GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient   mGoogleApiClient;
    public static final int   PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    private Double latitude = 0.0, longitude = 0.0;

    String lead_price = "free";
    String first_tym  = null;

    public static String number_call      = "";
    public static String number_call_name = "";
    String cat_id, cat_name,cat_hebrew, str_upload = null;

    ArrayList<LatlngPojo> arr_place = new ArrayList<>();

    LocRegister_Adapter   adapter_loc;
    GlobalConstant        utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    //Audio Recorder
    Random random ;
    String address_bitmap = "LOCATION NOT SPECIFIED";

    long        timeWhenPaused = 0;
    private int mRecordPromptCount = 0;
    private int mRecordPromptCount_pause = 0;

    private Handler mHandler = new Handler();

    private MediaPlayer mMediaPlayer = null;
    private boolean     isPlaying = false;

    //stores minutes and seconds of the length of the file.
    private static CountDownTimer countDownTimer;
    private static CountDownTimer countDownTimer_record;
    int                           resume_pos = 0;

    BottomSheetDialog dialog_share;
    BottomSheetDialog dialog_deny;
    BottomSheetDialog dialog_store;
    BottomSheetDialog dialog_micro;

    int count_start           = 30;
    int count_start_play      = 0;
    int count_start_pause     = 0;
    int count_start_pause_add = 0;

    String  get_desc,get_audio,get_budget,get_loc,get_addres,get_lat,get_long,get_country,get_cat,get_phone,get_client_name,
            get_lead_price,get_lead_id,get_time,get_resell=null;
    String  get_cityName, getCountryName,widget_nmbr;
    Handler mHandler_seekbar;
    public static String save_changes        = null;
    public static String save_changes_bought = null;
    public static String save_changes_sell   = null;

    private TransferUtility transferUtility;
    private Util            util;
    String                  file_key = "";
    String                  admin_max_price = "";

    CircleImageView img_user;

    TransferObserver  observer;
    BottomSheetDialog dialog_card;
    View layout_to_share;
    ProgressDialog progressDialog_share_lead;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell);

        ButterKnife.bind(this);

        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(SellActivity.this);
        random                = new Random();

          /*.....AWS..........*/
        util               = new Util();
        transferUtility    = util.getTransferUtility(this);


        if(sharedPreferenceLeadr.get_SELL_FIRST_TYM().equals("")){
            utils.hideKeyboard(SellActivity.this);
            dialog_Show_Guide(getResources().getString(R.string.guide),getResources().getString(R.string.guide_head));
        }

        mGoogleApiClient = new GoogleApiClient.Builder(SellActivity.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(SellActivity.this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();

        edt_lead_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if(cs.toString().length()>=1) {
                    Log.e("cs: ",cs.toString());
                    if (!cs.toString().startsWith("0")) {

                        lead_price = edt_lead_price.getText().toString();
                        btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                        btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                        btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                        btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                        btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                        btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                        btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                        btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));

                        btn_2.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                        btn_3.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                        btn_4.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                        btn_5.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                        btn_6.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                        btn_7.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                        btn_8.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                        btn_free.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                    }else {
                        if (edt_lead_price.getText().toString().trim().length() == 1) {
                            lead_price = edt_lead_price.getText().toString();
                            btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                            btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                            btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                            btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                            btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                            btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                            btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                            btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));

                            btn_2.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                            btn_3.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                            btn_4.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                            btn_5.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                            btn_6.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                            btn_7.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                            btn_8.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                            btn_free.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                        } else {
                            edt_lead_price.getText().clear();
                        }
                    }
                }else{
                    edt_lead_price.getText().clear();
                    edt_lead_price.setSelection(edt_lead_price.getText().length());
                    lead_price = "0";
                    btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
                    btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                    btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                    btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                    btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                    btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                    btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
                    btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));

                    btn_2.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                    btn_3.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                    btn_4.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                    btn_5.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                    btn_6.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                    btn_7.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                    btn_8.setTextColor(getResources().getColor(R.color.colorDarkGreen));
                    btn_free.setTextColor(getResources().getColor(R.color.colorWhite));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        edt_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence last1, int i, int i1, int i2) {

                if(edt_phone.getText().toString().length()>1) {
                    String last = last1.toString();
                    if (last.substring(last.length() - 1).equals("+")) {
                        edt_phone.setText(edt_phone.getText().toString().substring(0, edt_phone.getText().toString().length() - 1));
                        edt_phone.setSelection(edt_phone.getText().toString().length());

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        widget_nmbr = getIntent().getStringExtra("widget");
        if(widget_nmbr!=null){
            edt_phone.setText(widget_nmbr);
            edt_name.setText(getResources().getString(R.string.unknw));
        }

        method_TYpeface();

        txt_locc_an.setText(getResources().getString(R.string.add_loc));

        if (utils.isNetworkAvailable(SellActivity.this)) {
            api_GetCategory();
        } else {
            utils.hideKeyboard(SellActivity.this);
            utils.dialog_msg_show(SellActivity.this, getResources().getString(R.string.no_internet));
        }

        /*songProgressBar.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });*/

        songProgressBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged( SeekBar seekBar, int progress, boolean fromUser ) {

            }

            @Override
            public void onStartTrackingTouch( SeekBar seekBar ) {
                if(mHandler_seekbar!=null){
                    mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                }
            }

            @Override
            public void onStopTrackingTouch( SeekBar seekBar ) {
                if(mHandler_seekbar!=null){
                    mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                }

                // forward or backward to certain seconds
                try{
                    mMediaPlayer.seekTo(seekBar.getProgress());
                }catch (Exception e){
//                    Toast.makeText(SellActivity.this,"exc: ",Toast.LENGTH_LONG).show();
                    if(mMediaPlayer!=null){
                        mMediaPlayer.seekTo(0);
                    }
                }

                method_countDown_Seekbar();
                if(countDownTimer!=null){
                    countDownTimer.cancel();
                }
                Log.e("progress seek: ",seekBar.getProgress()+"");
                if(mMediaPlayer!=null) {
                    Log.e("progress seek2: ", mMediaPlayer.getCurrentPosition() + "");
                }
                count_start_pause = count_start_play-seekBar.getProgress()/1000;
                method_countDown(count_start_play-seekBar.getProgress()/1000);
            }
        });

        seek_pause.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        sliderListener sldListener = new sliderListener();
        seek_pause.setOnSeekBarChangeListener(sldListener);

        /*getIntent*/
        if(getIntent().getStringExtra("resell")!=null){
            if(getIntent().getStringExtra("resell").equals("1")){
                get_resell = "1";
                method_Get_Intent();
            }else if(getIntent().getStringExtra("resell").equals("0")){
                get_resell = "1";
                method_Get_Intent();
            }
        }


        sharedPreferenceLeadr.set_SELL_START(CurrentTimeStart());

/***IF LEAD is in EDIT MODE****/
        if(getIntent().getStringExtra("resell")!=null){
            if(getIntent().getStringExtra("resell").equalsIgnoreCase("0")){
                api_LeadEdit("1");
            }
        }
        if(utils.isNetworkAvailable(SellActivity.this)){
//            api_chck_AdminSettings();
        }else{
            utils.dialog_msg_show(SellActivity.this, getResources().getString(R.string.no_internet));
        }


        method_SetPreviousLOC();

        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")) {
            method_Price_IN_Heb();
        }

        edt_lead_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged( CharSequence s, int start, int count, int after ) {

            }

            @Override
            public void onTextChanged( CharSequence s, int start, int before, int count ) {
               if(s.length()==1){
                   scroll.fullScroll(ScrollView.FOCUS_DOWN);
                   edt_lead_price.requestFocus();
               }
            }

            @Override
            public void afterTextChanged( Editable s ) {

            }
        });

        edt_lead_price.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange( View v, boolean hasFocus ) {
                if(hasFocus){

                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void method_Price_IN_Heb() {
        btn_2.setText(" "+getResources().getString(R.string.dollar)+" "+Math.round(Float.valueOf(2)*
                Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));

        btn_3.setText(" "+getResources().getString(R.string.dollar)+" "+Math.round(Float.valueOf(5)*
                Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));

        btn_4.setText(" "+getResources().getString(R.string.dollar)+" "+Math.round(Float.valueOf(7.5f)*
                Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));

        btn_5.setText(" "+getResources().getString(R.string.dollar)+" "+Math.round(Float.valueOf(10)*
                Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));

        btn_6.setText(" "+getResources().getString(R.string.dollar)+" "+Math.round(Float.valueOf(15)*
                Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));

        btn_7.setText(" "+getResources().getString(R.string.dollar)+" "+Math.round(Float.valueOf(20)*
                Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));

        btn_8.setText(" "+getResources().getString(R.string.dollar)+" "+Math.round(Float.valueOf(25)*
                Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));

        txt_doll.setText(getResources().getString(R.string.dollar));
    }


    private void method_SetPreviousLOC() {
        if(!sharedPreferenceLeadr.get_LAT_SELL().equalsIgnoreCase("0")){
            LatlngPojo pojo = new LatlngPojo();
            pojo.setName(sharedPreferenceLeadr.get_LOC_NAME());
            pojo.setLat(sharedPreferenceLeadr.get_LAT_SELL().trim());
            pojo.setLong_(sharedPreferenceLeadr.get_LONG_SELL().trim());
            pojo.setCountryname(sharedPreferenceLeadr.get_COUNTRY_SELL());
            pojo.setAddress(sharedPreferenceLeadr.get_ADDRESS_SELL());
            arr_place.add(pojo);

            LinearLayoutManager lnr_album = new LinearLayoutManager(SellActivity.this, LinearLayoutManager.VERTICAL, false);
            recycl_loc.setLayoutManager(lnr_album);
            adapter_loc = new LocRegister_Adapter(SellActivity.this,arr_place);
            recycl_loc.setAdapter(adapter_loc);
            recycl_loc.setVisibility(View.VISIBLE);

            rel_add_loc.setVisibility(View.GONE);
            vw_line_act.setVisibility(View.GONE);
            online = 1;
            btn_online.setVisibility(View.GONE);
            txt_sel_loc.setVisibility(View.GONE);
            txt_online.setVisibility(View.VISIBLE);
            btn_sel_loc.setVisibility(View.VISIBLE);
            rel_loc_sel.setVisibility(View.VISIBLE);
        }
    }


     private void method_TYpeface() {
        txt_des.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_budg.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_locc.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_catt.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_inf.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_led.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_creat.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ms.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_locc_an.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_play.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_pause.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_timer.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        edt_phone.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        edt_buss_info.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        edt_lead_price.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        edt_budget.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        edt_name.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_2.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_3.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_4.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_5.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_6.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_7.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_8.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_change_cat.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_free.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_lastcall.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_online.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_publish.setTypeface(utils.Dina(SellActivity.this));
        btn_sel_loc.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_locc_an.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_cate.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_sel_loc.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_online.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_lead_price.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_seller.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ago.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_client_name.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_des_client.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_client_phn.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_loc.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_budget.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_play_buy.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_secs.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_des_buy.setTypeface(utils.OpenSans_Light(SellActivity.this));
        txt_read_more.setTypeface(utils.OpenSans_Light(SellActivity.this));
        btn_buy.setTypeface(utils.Dina(SellActivity.this));
    }


    String CurrentTimeStart(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    private class sliderListener implements OnSeekBarChangeListener {
        private int smoothnessFactor = 200;
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            progress = Math.round(progress / smoothnessFactor);
        }
        public void onStartTrackingTouch(SeekBar seekBar) {
        }
        public void onStopTrackingTouch(SeekBar seekBar) {
            seekBar.setProgress(Math.round((seekBar.getProgress() + (smoothnessFactor / 2)) / smoothnessFactor) * smoothnessFactor);
        }
    }


    void method_Get_Intent(){
        get_desc         = getIntent().getStringExtra("desc");
        get_audio        = getIntent().getStringExtra("audio");
        get_budget       = getIntent().getStringExtra("budget");
        get_cat          = getIntent().getStringExtra("cat");
        get_phone        = getIntent().getStringExtra("phone");
        get_client_name  = getIntent().getStringExtra("client_name");
        get_lead_price   = getIntent().getStringExtra("lead_price");
        get_lead_id      = getIntent().getStringExtra("lead_id");
        get_loc          = getIntent().getStringExtra("loc");
        get_time         = getIntent().getStringExtra("time");
        if(!get_loc.equals("0")){
            get_lat = getIntent().getStringExtra("lat");
            get_long = getIntent().getStringExtra("long_");
            get_country = getIntent().getStringExtra("country");
            get_addres       = getIntent().getStringExtra("address");

            online = 1;
            btn_online.setVisibility(View.GONE);
            txt_sel_loc.setVisibility(View.GONE);
            txt_online.setVisibility(View.VISIBLE);
            btn_sel_loc.setVisibility(View.VISIBLE);
            rel_loc_sel.setVisibility(View.VISIBLE);
            rel_add_loc.setVisibility(View.GONE);
            vw_line_act.setVisibility(View.GONE);
            vw_bottom.setVisibility(View.GONE);

            LatlngPojo pojo = new LatlngPojo();
            pojo.setName(get_loc);
            pojo.setLat(get_lat);
            pojo.setLong_(get_long);
            pojo.setCountryname(get_country);
            pojo.setAddress(get_addres);
            arr_place.add(pojo);

            recycl_loc.setVisibility(View.VISIBLE);
            LinearLayoutManager lnr_album = new LinearLayoutManager(SellActivity.this, LinearLayoutManager.VERTICAL, false);
            recycl_loc.setLayoutManager(lnr_album);
            adapter_loc = new LocRegister_Adapter(SellActivity.this,arr_place);
            recycl_loc.setAdapter(adapter_loc);
        }else{
            online = 0;
            btn_online.setVisibility(View.VISIBLE);
            txt_sel_loc.setVisibility(View.VISIBLE);
            txt_online.setVisibility(View.GONE);
            btn_sel_loc.setVisibility(View.GONE);
            rel_loc_sel.setVisibility(View.GONE);
        }
        edt_buss_info.setText(get_desc);
        if(!get_budget.equals("0")) {
            edt_budget.setText(get_budget);
        }
        edt_phone.setText(get_phone);
        edt_name.setText(get_client_name);
        edt_lead_price.setText(get_lead_price);

        if(!get_audio.equals("")){
            getIntentAudio();
        }

        if(get_lead_price.equalsIgnoreCase("0")){
            btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
            btn_free.setTextColor(getResources().getColor(R.color.colorWhite));
        }

        else if(get_lead_price.equalsIgnoreCase("2")){
            btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
            btn_2.setTextColor(getResources().getColor(R.color.colorWhite));
        }

        else if(get_lead_price.equalsIgnoreCase("5")){
            btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
            btn_5.setTextColor(getResources().getColor(R.color.colorWhite));
        }

        else if(get_lead_price.equalsIgnoreCase("7.5")){
            btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
            btn_7.setTextColor(getResources().getColor(R.color.colorWhite));
        }

        else if(get_lead_price.equalsIgnoreCase("10")){
            btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
            btn_5.setTextColor(getResources().getColor(R.color.colorWhite));
        }

        else if(get_lead_price.equalsIgnoreCase("15")){
            btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
            btn_6.setTextColor(getResources().getColor(R.color.colorWhite));
        }

        else if(get_lead_price.equalsIgnoreCase("20")){
            btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
            btn_7.setTextColor(getResources().getColor(R.color.colorWhite));
        }

        else if(get_lead_price.equalsIgnoreCase("25")){
            btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
            btn_8.setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }


    void getIntentAudio(){
        rel_record_audio.setVisibility(View.GONE);
        rel_add_audio.setVisibility(View.GONE);
        rel_play_audio.setVisibility(View.VISIBLE);

        count_start = 0;
        count_start_play      = Integer.valueOf(get_time);
        count_start_play      = Integer.valueOf(get_time);
        count_start_pause     = Integer.valueOf(get_time);
        count_start_pause_add = Integer.valueOf(get_time);
        sharedPreferenceLeadr.set_COUNT_START_PLAY(String.valueOf(count_start_play));

        int hr = Integer.valueOf(sharedPreferenceLeadr.get_COUNT_START_PLAY())/3600;
        int rem = Integer.valueOf(sharedPreferenceLeadr.get_COUNT_START_PLAY())%3600;
        int mn = rem/60;
        int sec = rem%60;
        String hrStr = (count_start_play<10 ? "0" : "")+hr;
        String mnStr = (mn<10 ? "0" : "")+mn;
        String secStr = (count_start_play<10 ? "0" : "")+sec;

        sharedPreferenceLeadr.set_COUNT_START("30");

        mChronometer_play.setText(mnStr+":"+secStr);
        mChronometer_play.setText("00:"+secStr+"");

        final_secs = String.valueOf(secStr);

//        mChronometer_play.setText(txt_record.getText().toString());
        txt_pause_audio.setText(mnStr+":"+secStr);
        txt_pause_audio.setText("00:"+secStr+"");


        sharedPreferenceLeadr.set_COUNT_START_PLAY(String.valueOf(count_start_play));
        sharedPreferenceLeadr.set_COUNT_START_PAUSE(String.valueOf(count_start_pause));
        sharedPreferenceLeadr.set_COUNT_START_PAUSE_ADD(String.valueOf(count_start_pause_add));
    }


    @Override
    protected void onResume() {
        super.onResume();
        save_changes = null;
        save_changes_bought = null;
        save_changes_sell = null;
        if(number_call!=null) {
            if (!number_call.equals("")) {
                edt_phone.setText(number_call);
                edt_name.setText(number_call_name);
                if (edt_phone.getText().toString().length() > 0) {
                    try {
                        InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        keyboard.showSoftInput(edt_phone, 0);
                    } catch (Exception e) {
                    }
                    edt_phone.requestFocus();

                    int centreY= (int) (edt_phone.getY() + 100);
                    scroll.smoothScrollBy(0, 600);
                }
            }
        }
        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")) {
            txt_cate.setText(sharedPreferenceLeadr.get_CATEGORY_SEL_NAME());
        }else{
            txt_cate.setText(sharedPreferenceLeadr.get_CATEGORY_SEL_NAME_HE());
        }
        if(first_tym==null) {
            first_tym = "";
            if (edt_buss_info.getText().toString().length() > 0) {
                try {
                    InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.showSoftInput(edt_buss_info, 0);
                } catch (Exception e) {
                }
                edt_buss_info.requestFocus();
            }
        }else{
            utils.hideKeyboard(SellActivity.this);
        }
    }



    public  void dialog_Show_Guide( String msg, String title){
        final BottomSheetDialog dialog = new BottomSheetDialog (SellActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_title.setText(title);
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_msg.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;

        sharedPreferenceLeadr.set_SELL_FIRST_TYM("1");

        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                try{
                    if(edt_buss_info.getText().toString().length()<1) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(edt_buss_info, InputMethodManager.SHOW_IMPLICIT);
                    }
                }catch (Exception e){}

            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                try{
                    if(edt_buss_info.getText().toString().length()<1) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(edt_buss_info, InputMethodManager.SHOW_IMPLICIT);
                    }
                }catch (Exception e){}
            }
        });
        dialog.show();
    }


    @OnClick(R.id.btn_lastcall)
    public void Lastcall() {

        if(utils.checkReadContactPermission(SellActivity.this) && utils.checkReadCallLogPermission(SellActivity.this)
                && ! utils.checkPhoneStatePermission(SellActivity.this)){
            Intent i = new Intent(SellActivity.this, CallLogActivity.class);
            startActivity(i);
        }else{
            if(!utils.checkReadCallLogPermission(SellActivity.this)){
                ActivityCompat.requestPermissions(
                        SellActivity.this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        6
                );
            }
            else if (!utils.checkReadContactPermission(SellActivity.this)) {
                ActivityCompat.requestPermissions(
                        SellActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        5
                );
            }/*else if(!utils.checkReadCallLogPermission(SellActivity.this)){
                ActivityCompat.requestPermissions(
                        SellActivity.this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        6
                );
            }*/else{
                ActivityCompat.requestPermissions(
                        SellActivity.this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        9
                );
            }

        }

    }


    @OnClick(R.id.txt_online)
    public void onTextClick() {
        online = 0;
        btn_online.setVisibility(View.VISIBLE);
        txt_sel_loc.setVisibility(View.VISIBLE);
        txt_online.setVisibility(View.GONE);
        btn_sel_loc.setVisibility(View.GONE);
        rel_loc_sel.setVisibility(View.GONE);
    }


    @OnClick(R.id.rel_focus)
    public void onRequestFocus() {
        edt_budget.requestFocus();
        try {
            InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.showSoftInput(edt_budget, 0);
            edt_budget.setSelection(edt_budget.getText().toString().length());
        } catch (Exception e) {
        }
    }


    @OnClick(R.id.lnr_price3)
    public void onRequestFocus_Price() {
        edt_lead_price.requestFocus();
        try {
            InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.showSoftInput(edt_lead_price, 0);
            edt_lead_price.setSelection(edt_lead_price.getText().toString().length());
        } catch (Exception e) {
        }
    }


    @OnClick(R.id.img_quest)
    public void onGuideClick() {
        utils.hideKeyboard(SellActivity.this);
        dialog_Show_Guide(getResources().getString(R.string.guide),getResources().getString(R.string.guide_head));
    }


    @OnClick(R.id.txt_sel_loc)
    public void onTxtLocClick() {
        online = 1;
        btn_online.setVisibility(View.GONE);
        txt_sel_loc.setVisibility(View.GONE);
        txt_online.setVisibility(View.VISIBLE);
        btn_sel_loc.setVisibility(View.VISIBLE);
        rel_loc_sel.setVisibility(View.VISIBLE);
        rel_add_loc.setVisibility(View.GONE);
        vw_line_act.setVisibility(View.GONE);
        vw_bottom.setVisibility(View.GONE);
        if(arr_place.size()==0){
            findPlace(PLACE_AUTOCOMPLETE_REQUEST_CODE);
        }
    }


    @OnClick(R.id.rel_loc_sel)
    public void onAddLoc() {
        findPlace(PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }


    @OnClick(R.id.img_back)
    public void onBack() {
        if(getIntent().getStringExtra("resell")!=null){
            if(getIntent().getStringExtra("resell").equalsIgnoreCase("0")){
                SellActivity.save_changes = "";
            }
        }
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        number_call = "";
        number_call_name = "";
        if(getIntent().getStringExtra("resell")!=null){
            if(getIntent().getStringExtra("resell").equalsIgnoreCase("0")){
                SellActivity.save_changes = "";
            }
        }
        super.onBackPressed();
    }

    @OnClick(R.id.btn_change_cat)
    public void onChangCat() {
        Intent i = new Intent(SellActivity.this,SelCategoryActivity.class);
        startActivity(i);
    }


    @OnClick(R.id.img_record_minus)
    public void onRemove1() {
        method_Remove_Record();
    }


    @OnClick(R.id.img_play_minus)
    public void onRemove2() {
        method_Remove_Record();
    }


    @OnClick(R.id.img_pause_minus)
    public void onRemove3() {
        method_Remove_Record();
    }


    void method_Remove_Record(){
        if(mMediaPlayer!=null){
            pausePlaying();
        }
        try{
            mMediaPlayer.release();
            mMediaPlayer = null;
            countDownTimer_record = null;
        }catch (Exception e){
            mMediaPlayer = null;
            countDownTimer_record = null;
        }
        resume_pos = 0;
        txt_pause.setTag("0");
        str_upload = null;
        get_resell = null;
        rel_record_audio.setVisibility(View.GONE);
        rel_play_audio.setVisibility(View.GONE);
        rel_pause_audio.setVisibility(View.GONE);
        rel_add_audio.setVisibility(View.VISIBLE);

        count_start = 30;
        count_start_play = 0;
        count_start_pause = 0;
        count_start_pause_add = 0;

        sharedPreferenceLeadr.set_COUNT_START("30");
        sharedPreferenceLeadr.set_COUNT_START_PLAY("");
        sharedPreferenceLeadr.set_COUNT_START_PAUSE("");
        sharedPreferenceLeadr.set_COUNT_START_PAUSE_ADD("");

    }


    @OnClick(R.id.rel_record_audio)
    public void onStopRecording() {
        method_StopRecording();
    }


    void method_StopRecording(){
        rel_record_audio.setVisibility(View.GONE);
        rel_play_audio.setVisibility(View.VISIBLE);

        count_start = 0;

        onRecord(false);
//        t_start.cancel();
        mRecordPromptCount = mRecordPromptCount-1;
        mRecordPromptCount_pause = mRecordPromptCount-1;

        try{
            sharedPreferenceLeadr.set_COUNT_START_PLAY(String.valueOf(count_start_play));

            int hr = Integer.valueOf(sharedPreferenceLeadr.get_COUNT_START_PLAY())/3600;
            int rem = Integer.valueOf(sharedPreferenceLeadr.get_COUNT_START_PLAY())%3600;
            int mn = rem/60;
            int sec = rem%60;
            String hrStr = (count_start_play<10 ? "0" : "")+hr;
            String mnStr = (mn<10 ? "0" : "")+mn;
            String secStr = (count_start_play<10 ? "0" : "")+sec;
//        txt_play.setText("PLAY "+mnStr+":"+secStr);
            if(countDownTimer_record!=null) {
                countDownTimer_record.cancel();
            }

            sharedPreferenceLeadr.set_COUNT_START("30");
            mChronometer_play.setText(mnStr+":"+secStr);
            mChronometer_play.setText("00:"+secStr+"");

            final_secs = String.valueOf(secStr);

//        mChronometer_play.setText(txt_record.getText().toString());
            txt_pause_audio.setText(mnStr+":"+secStr);
            txt_pause_audio.setText("00:"+secStr+"");

            sharedPreferenceLeadr.set_COUNT_START_PLAY(String.valueOf(count_start_play));
            sharedPreferenceLeadr.set_COUNT_START_PAUSE(String.valueOf(count_start_pause));
            sharedPreferenceLeadr.set_COUNT_START_PAUSE_ADD(String.valueOf(count_start_pause_add));
        }catch (Exception e){
        }
    }


    @OnClick(R.id.rel_play_audio)
    public void onPlayRecording() {
        rel_pause_audio.setVisibility(View.VISIBLE);
        rel_play_audio.setVisibility(View.GONE);
        timer2 = timer;

        playAudio(false);
    }


    @OnClick(R.id.txt_pause)
    public void onPauseRecording() {
        if(txt_pause.getTag().equals("0")){
            playAudio(true);
            txt_pause.setTag("1");
//            txt_pause.setText("PLAY");
            txt_pause.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pause_sell, 0, 0, 0);
        }else{
            playAudio(false);
            txt_pause.setTag("0");
//            txt_pause.setText("PAUSE");
            txt_pause.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
        }

    }



    @OnClick(R.id.rel_add_audio)
    public void onAddAudio() {
        if(utils.checkMicrophonePermission(SellActivity.this) && utils.checkReadExternalPermission(SellActivity.this) &&
                utils.checkWriteExternalPermission(SellActivity.this)  ){
            method_StartRecord();
        }else{
            if (!utils.checkWriteExternalPermission(SellActivity.this)&&!utils.checkReadExternalPermission(SellActivity.this)) {
                ActivityCompat.requestPermissions(
                        SellActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },
                        3
                );
            }else{
                ActivityCompat.requestPermissions(
                        SellActivity.this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        4
                );
            }
        }
    }


    void method_StartRecord(){
        onRecord(true);

        rel_add_audio.setVisibility(View.GONE);
        rel_record_audio.setVisibility(View.VISIBLE);
        method_countDown_Record();
    }


    void method_countDown_Record(){
        count_start = Integer.valueOf(sharedPreferenceLeadr.get_COUNT_START());
        countDownTimer_record = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                count_start = count_start-1;
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
//                txt_pause.setText("Pause: "+hms);//set text
//                txt_play.setText("Pause: "+hms);//set text
                mChronometer.setText(hms);
                txt_record.setText(hms);

                count_start_play = count_start_play+1;
                count_start_pause = count_start_pause+1;
                count_start_pause_add = count_start_pause_add+1;
                Log.e("count_start: ",count_start+"");
                if(count_start==1){
                    count_start = 0;
                    count_start_play = count_start_play+1;
                    count_start_pause = count_start_pause+1;
                    count_start_pause_add = count_start_pause_add+1;
                    method_StopRecording();
                }
            }

            public void onFinish() {

                countDownTimer_record = null;//set CountDownTimer to null
            }
        }.start();
    }


    private void onRecord(boolean start){
        Intent intent = new Intent(SellActivity.this, RecordingService.class);

        if (start) {
            // start recording
            //mPauseButton.setVisibility(View.VISIBLE);
            File folder = new File(Environment.getExternalStorageDirectory() + "/SoundRecorder");
            if (!folder.exists()) {
                //folder /SoundRecorder doesn't exist, create the folder
                folder.mkdir();
            }

            //start Chronometer
            mChronometer.setBase(SystemClock.elapsedRealtime());
            mChronometer.start();
            mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                @Override
                public void onChronometerTick(Chronometer chronometer) {
                    mRecordPromptCount++;
                }
            });

            //start RecordingService
            //keep screen on while recording
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            startService(intent);

            mRecordPromptCount++;

        } else {
            str_upload = "";
            //stop recording
            //mPauseButton.setVisibility(View.GONE);
            mChronometer.stop();
//            mChronometer.setBase(SystemClock.elapsedRealtime());
            timeWhenPaused = 0;
            if(countDownTimer_record!=null) {
                countDownTimer_record.cancel();
            }

            stopService(intent);

//            mChronometer_play.setText(txt_record.getText().toString());
            //allow the screen to turn off again once recording is finished
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }


    public void playAudio(Boolean state) {
        onPlay(state);
    }

    ////////////////////
    // Play start/stop
    private void onPlay(boolean isPlaying2){
        if (!isPlaying2) {
            txt_pause.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
            //currently MediaPlayer is not playing audio
            if(mMediaPlayer == null) {
                startPlaying(); //start from beginning
                isPlaying = !isPlaying;
            } else {
                resumePlaying(); //resume the currently paused MediaPlayer
            }

        } else {
            //pause the MediaPlayer
            pausePlaying();
        }
    }


    private void startPlaying() {
        mMediaPlayer = new MediaPlayer();

        try {
            if(get_resell!=null){
                    mMediaPlayer.setDataSource(get_audio);

            }else{
                    mMediaPlayer.setDataSource("/storage/emulated/0/SoundRecorder/" + sharedPreferenceLeadr.get_FILE_NAME());

            }
                mMediaPlayer.prepare();


            count_start_play = Integer.valueOf(sharedPreferenceLeadr.get_COUNT_START_PLAY());
            count_start_pause_add = Integer.valueOf(sharedPreferenceLeadr.get_COUNT_START_PAUSE_ADD());
            count_start_pause = Integer.valueOf(sharedPreferenceLeadr.get_COUNT_START_PAUSE());


            songProgressBar.setMax(mMediaPlayer.getDuration()+1);
            songProgressBar.setProgress(0);
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mMediaPlayer.start();
                }
            });
        } catch (IOException e) {
            Log.e("", "prepare() failed");
        }
        count_start_pause_add = 0;
        method_countDown(count_start_play);
        method_countDown_Seekbar();

        mMediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {

            }
        });
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stopPlaying();
            }
        });



        //keep screen on while playing audio
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    private void updateSeekBar() {
        mHandler.postDelayed(mRunnable, 1000);
    }


    void method_countDown(int timer){
        countDownTimer = null;
        timer = timer+1;
        count_start_pause = count_start_pause+1;
        Log.e("count_pause: ",count_start_pause+"");
        Log.e("count_pause: ",count_start_pause+"");

        countDownTimer = new CountDownTimer(timer*1000, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
//                txt_pause.setText("Pause: "+hms);//set text
//                txt_play.setText("Pause: "+hms);//set text
                mChronometer_play.setText(hms);
                mChronometer_pause.setText(hms);

                if(count_start_play!=1){
                    if(hms.equalsIgnoreCase("00:01")){
//                        songProgressBar.setProgress(count_start_play+1);
                    }
                }
                mChronometer_pause.setVisibility(View.GONE);
                txt_pause_audio.setVisibility(View.VISIBLE);

                count_start_pause = count_start_pause-1;
                count_start_pause_add = count_start_pause_add+1;

                Log.e("count_pause: ",count_start_pause+"");
                Log.e("count_progress: ",count_start_pause_add+"");

                mChronometer_pause.setText(count_start_pause+"");
                txt_pause_audio.setText(hms+"");
                if(count_start_pause_add==count_start_play){
                    count_start_pause_add = count_start_pause_add+1;
                }


//                songProgressBar.setProgress(count_start_pause_add);
                Log.e("prog: ",count_start_pause+"");
                if(count_start_pause==0){
//                    songProgressBar.setProgress(count_start_play+1);
                    stopPlaying();
                }
                mRecordPromptCount_pause = mRecordPromptCount_pause--;

            }

            public void onFinish() {

                countDownTimer = null;//set CountDownTimer to null
            }
        }.start();
    }


    void method_countDown_Seekbar(){
        mHandler_seekbar = new Handler();
        mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);

    }


    private Runnable mRunnabl_seekbar = new Runnable() {
        @Override
        public void run() {
            if(mMediaPlayer != null){
                songProgressBar.setProgress(mMediaPlayer.getCurrentPosition());
                int smoothnessFactor = 10;
                songProgressBar.setProgress(Math.round((mMediaPlayer.getCurrentPosition() + (smoothnessFactor / 2)) / smoothnessFactor) * smoothnessFactor);
            }
            mHandler_seekbar.postDelayed(this, 15);
        }
    };



    private void pausePlaying() {
        mHandler.removeCallbacks(mRunnable);
        resume_pos = mMediaPlayer.getCurrentPosition();
        if(countDownTimer!=null) {
            countDownTimer.cancel();
        }
        if(mMediaPlayer!=null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
        }


        if(mHandler_seekbar!=null){
            mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
        }

//        songProgressBar.setProgress(mMediaPlayer.getCurrentPosition());

    }


    private void resumePlaying() {
//        mMediaPlayer.seekTo(resume_pos);
        mHandler.removeCallbacks(mRunnable);
        if(mMediaPlayer!=null) {
            mMediaPlayer.start();
        }
        updateSeekBar();
        method_countDown(count_start_pause-1);
//        updateSeekBar();
        mHandler_seekbar.postDelayed(mRunnabl_seekbar, 300);

    }


    private void stopPlaying() {
        mHandler.removeCallbacks(mRunnable);
       if(mMediaPlayer!=null){
           mMediaPlayer.stop();
           mMediaPlayer.reset();
           mMediaPlayer.release();
       }
        mMediaPlayer = null;
        resume_pos = 0;

        isPlaying = !isPlaying;
//        songProgressBar.setProgress(songProgressBar.getMax());
        rel_pause_audio.setVisibility(View.GONE);
        rel_play_audio.setVisibility(View.VISIBLE);

        if(countDownTimer!=null) {
            countDownTimer.cancel();
        }
        int hr = Integer.valueOf(sharedPreferenceLeadr.get_COUNT_START_PLAY())/3600;
        int rem = Integer.valueOf(sharedPreferenceLeadr.get_COUNT_START_PLAY())%3600;
        int mn = rem/60;
        int sec = rem%60;
        String hrStr = (hr<10 ? "0" : "")+hr;
        String mnStr = (mn<10 ? "0" : "")+mn;
        String secStr = (sec<10 ? "0" : "")+sec;
//        txt_play.setText("PLAY "+mnStr+":"+secStr);


        mChronometer_play.setText(mnStr+":"+secStr);
        mRecordPromptCount_pause = mRecordPromptCount;
//        songProgressBar.setProgress(0);
        //allow the screen to turn off again once audio is finished playing
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    //updating mSeekBar
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if(mMediaPlayer != null){
                int mCurrentPosition = mMediaPlayer.getCurrentPosition();
                long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);

                updateSeekBar();
            }
        }
    };


    @OnClick(R.id.btn_publish)
    public void onPublish() {
        if(lead_price.equals("Free")){
            lead_price = "0";
        }
        if(utils.isNetworkAvailable(SellActivity.this)){
            api_chck_AdminSettings();
        }else{
            utils.dialog_msg_show(SellActivity.this, getResources().getString(R.string.no_internet));
        }

    }



    void method_SellLEad(){
        if(edt_buss_info.getText().toString().trim().length()>0){
            method_InsideDesc();
        }else if(str_upload!=null){
            method_InsideDesc();
        }else{
            dialog_msg_showForDes(SellActivity.this,getResources().getString(R.string.type_or),edt_buss_info);

        }
    }


    void method_InsideDesc(){
        utils.hideKeyboard(SellActivity.this);
        if(online==1){
            if(arr_place.size()==0){
                utils.hideKeyboard(SellActivity.this);
                utils.dialog_msg_show(SellActivity.this,getResources().getString(R.string.sel_locs));
            }else{
                if(txt_cate.getText().toString().trim().length()>0){
                    if(edt_phone.getText().toString().trim().length()<9){
                        edt_phone.requestFocus();
                        dialog_msg_showForPhone(SellActivity.this,getResources().getString(R.string.valid_ph),edt_phone);
                        int nY_Pos = txt_led.getTop();
                        scroll.scrollTo(0,nY_Pos);
                    }else{
                        String lat   = "0";
                        String long_ = "0";
                        String loc   = "0";
                        String country   = "0";
                        String address   = "0";
//                                  API
                        if(online==1) {
                            for (int i = 0; i < arr_place.size(); i++) {
                                if (lat.equals("0")) {
                                    lat = arr_place.get(i).getLat();
                                    long_ = arr_place.get(i).getLong_();
                                    loc = arr_place.get(i).getName();
                                    country = arr_place.get(i).getCountryname();
                                    address = arr_place.get(i).getAddress();
                                } else {
                                    lat = lat + "," + arr_place.get(i).getLat();
                                    long_ = long_ + "," + arr_place.get(i).getLong_();
                                    loc = loc + "," + arr_place.get(i).getName();
                                    country = arr_place.get(i).getCountryname();
                                    address = arr_place.get(i).getAddress();
                                }
                            }
                        }
                        if(!lat.equalsIgnoreCase("0")){
                            sharedPreferenceLeadr.set_LAT_SELL(lat+"");
                            sharedPreferenceLeadr.set_LONG_SELL(long_+"");
                            sharedPreferenceLeadr.set_LOC_NAME_SELL(loc);
                            sharedPreferenceLeadr.set_COUNTRY_SELL(country);
                            sharedPreferenceLeadr.set_LOC_ADDRESS_SELL(address);
                        }else{
                            sharedPreferenceLeadr.set_LAT_SELL("0");
                            sharedPreferenceLeadr.set_LONG_SELL("0");
                            sharedPreferenceLeadr.set_LOC_NAME_SELL("0");
                            sharedPreferenceLeadr.set_COUNTRY_SELL("0");
                            sharedPreferenceLeadr.set_LOC_ADDRESS_SELL("0");
                        }
                        if(utils.isNetworkAvailable(SellActivity.this)) {
                            if(SelCategoryActivity.cate_sel_id!=null){
                                if(!SelCategoryActivity.cate_sel_id.equals("")){
                                    cat_id = SelCategoryActivity.cate_sel_id;
                                }
                            }
                            utils.hideKeyboard(SellActivity.this);
                            if(str_upload!=null){
                                utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
                                File file_to_send = new File("/storage/emulated/0/SoundRecorder/"+sharedPreferenceLeadr.get_FILE_NAME());
                                beginUpload(file_to_send.getPath());
                            }else {
                                final_secs = "0";
                                if(getIntent().getStringExtra("resell")!=null){
                                    if(getIntent().getStringExtra("resell").equals("1")){
                                        utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
                                        api_ReSell(lat, long_, loc,"",country,address);
                                    }else if(getIntent().getStringExtra("resell").equals("0")){
                                        utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
                                        api_ReSell_Same(lat, long_, loc,"",country,address);
                                    }else{
                                        utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
                                        api_Sell(lat, long_, loc,"",country,address);
                                    }
                                }else{
                                    utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
                                    api_Sell(lat, long_, loc,"",country,address);
                                }
                            }
                        }else{
                            utils.hideKeyboard(SellActivity.this);
                            utils.dialog_msg_show(SellActivity.this,getResources().getString(R.string.no_internet));
                        }
                    }
                }else{
                    utils.hideKeyboard(SellActivity.this);
                    utils.dialog_msg_show(SellActivity.this,getResources().getString(R.string.sel_cats));
                }
            }
        }else{
            if(txt_cate.getText().toString().trim().length()>0){
                if(edt_phone.getText().toString().trim().length()<9){
                    edt_phone.requestFocus();
                    dialog_msg_showForPhone(SellActivity.this,getResources().getString(R.string.valid_ph),edt_phone);
                    int nY_Pos = txt_led.getBottom();
                    scroll.scrollTo(0,nY_Pos);
                }else{
                    String lat   = "0";
                    String long_ = "0";
                    String loc   = "0";
                    String country   = "0";
                    String address   = "0";
//                                  API
                    if(online==1) {
                        for (int i = 0; i < arr_place.size(); i++) {
                            if (lat.equals("0")) {
                                lat = arr_place.get(i).getLat();
                                long_ = arr_place.get(i).getLong_();
                                loc = arr_place.get(i).getName();
                                country = arr_place.get(i).getCountryname();
                                address = arr_place.get(i).getAddress();
                            } else {
                                lat = lat + "," + arr_place.get(i).getLat();
                                long_ = long_ + "," + arr_place.get(i).getLong_();
                                loc = loc + "," + arr_place.get(i).getName();
                                country = arr_place.get(i).getCountryname();
                                address = arr_place.get(i).getAddress();
                            }
                        }
                    }
                    if(!lat.equalsIgnoreCase("0")){
                        sharedPreferenceLeadr.set_LAT_SELL(lat+"");
                        sharedPreferenceLeadr.set_LONG_SELL(long_+"");
                        sharedPreferenceLeadr.set_LOC_NAME_SELL(loc);
                        sharedPreferenceLeadr.set_COUNTRY_SELL(country);
                        sharedPreferenceLeadr.set_LOC_ADDRESS_SELL(address);
                    }else{
                        sharedPreferenceLeadr.set_LAT_SELL("0");
                        sharedPreferenceLeadr.set_LONG_SELL("0");
                        sharedPreferenceLeadr.set_LOC_NAME_SELL("0");
                        sharedPreferenceLeadr.set_COUNTRY_SELL("0");
                        sharedPreferenceLeadr.set_LOC_ADDRESS_SELL("0");
                    }
//                              API
                    if(utils.isNetworkAvailable(SellActivity.this)) {
                        if(SelCategoryActivity.cate_sel_id!=null){
                            if(!SelCategoryActivity.cate_sel_id.equals("")){
                                cat_id = SelCategoryActivity.cate_sel_id;
                            }
                        }
                        utils.hideKeyboard(SellActivity.this);
                        if(str_upload!=null){
                            File file_to_send = new File("/storage/emulated/0/SoundRecorder/"+sharedPreferenceLeadr.get_FILE_NAME());
                            utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
                            beginUpload(file_to_send.getPath());
                        }else {
                            final_secs = "0";
                            if(getIntent().getStringExtra("resell")!=null){
                                if(getIntent().getStringExtra("resell").equals("1")){
                                    utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
                                    api_ReSell(lat, long_, loc,"",country,address);
                                }else if(getIntent().getStringExtra("resell").equals("0")){
                                    utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
                                    api_ReSell_Same(lat, long_, loc,"",country,address);
                                }else{
                                    utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
                                    api_Sell(lat, long_, loc,"",country,address);
                                }
                            }else{
                                utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
                                api_Sell(lat, long_, loc,"",country,address);
                            }
                        }
                    }else{
                        utils.hideKeyboard(SellActivity.this);
                        utils.dialog_msg_show(SellActivity.this,getResources().getString(R.string.no_internet));
                    }
                }
            }else{
                utils.hideKeyboard(SellActivity.this);
                utils.dialog_msg_show(SellActivity.this,getResources().getString(R.string.sel_cats));
            }
        }
    }


    public  void dialog_msg_show(Activity activity, String msg,final EditText edt){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg    = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);

        txt_msg.setText(msg);

        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.show();
    }


    public  void dialog_msg_showWithTitle(Activity activity, String msg, String title,final EditText edt){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog.findViewById(R.id.txt_ok);

        txt_msg.setText(msg);
        txt_title.setText(title);

        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
               /* try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();*/

                scroll.fullScroll(ScrollView.FOCUS_DOWN);
                edt_lead_price.requestFocus();
            }
        });

        dialog.show();

    }


    public  void dialog_msg_showForPhone(Activity activity, String msg,final EditText edt){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog.findViewById(R.id.txt_ok);

        txt_msg.setText(msg);

        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
//                scroll.scrollTo(0, 0);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                if(imm!=null) {
                    if (imm.isAcceptingText()) {
                        int centreY= (int) (txt_lead_price.getY() + txt_lead_price.getHeight() *4);
                        scroll.smoothScrollBy(0, centreY);
                    } else {
                        int centreY= (int) (txt_lead_price.getY() + txt_lead_price.getHeight() / 2);
                        scroll.smoothScrollBy(0, centreY);
                    }
                }
            }
        });

        dialog.show();
    }


    public  void dialog_msg_showForDes(Activity activity, String msg,final EditText edt){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog.findViewById(R.id.txt_ok);

        txt_msg.setText(msg);

        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt_buss_info.requestFocus();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt_buss_info.requestFocus();
                scroll.scrollTo(0, 0);
            }
        });

        dialog.show();
    }


    private void beginUpload(String filePath) {
//        utils.showProgressDialog(SellActivity.this,getResources().getString(R.string.load_upload));
        if (filePath == null) {
            Toast.makeText(this, "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(filePath);

        if(observer!=null){
            observer.cleanTransferListener();
        }
        observer = transferUtility.upload(Constants.BUCKET_NAME, file.getName(),
                file);
        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */
        file_key = observer.getKey();
        observer.setTransferListener(new UploadListener(file_key));
    }


    private class UploadListener implements TransferListener {
        String file_key_name = "";

        public UploadListener( String file_key ) {
            this.file_key_name = file_key;
        }

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e("err: ",e.toString());
            if(observer!=null){
                observer.cleanTransferListener();
            }
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            String aws_url = "https://s3-ap-southeast-1.amazonaws.com/beleadr/"+file_key_name;

            if(newState.toString().equals("COMPLETED")) {
                if(observer!=null){
                    observer.cleanTransferListener();
                }
                utils.dismissProgressdialog();
                String lat   = "";
                String long_ = "";
                String loc   = "";
                String country   = "";
                String address   = "";
//                                  API
                for(int i=0; i<arr_place.size(); i++){
                    if(lat.equals("")){
                        lat = arr_place.get(i).getLat();
                        long_ = arr_place.get(i).getLong_();
                        loc = arr_place.get(i).getName();
                        country = arr_place.get(i).getCountryname();
                        address = arr_place.get(i).getAddress();
                    }else{
                        lat = lat +","+arr_place.get(i).getLat();
                        long_ = long_ +","+arr_place.get(i).getLong_();
                        loc = loc +","+arr_place.get(i).getName();
                        country = arr_place.get(i).getCountryname();
                        address = arr_place.get(i).getAddress();
                    }
                }
                if(getIntent().getStringExtra("resell")!=null){
                    if(getIntent().getStringExtra("resell").equals("1")){
                        utils.showProgressDialog(SellActivity.this,getResources().getString(R.string.ceat_wait));
                        api_ReSell(lat, long_, loc,aws_url,country,address);
                    }else{
                        utils.showProgressDialog(SellActivity.this,getResources().getString(R.string.ceat_wait));
                        api_Sell(lat, long_, loc,aws_url,country,address);
                    }
                }else{
                    utils.showProgressDialog(SellActivity.this,getResources().getString(R.string.ceat_wait));
                    api_Sell(lat, long_, loc,aws_url,country,address);
                }
            }
        }
    }


    @OnClick(R.id.btn_free)
    public void onFree() {
        edt_lead_price.setText("0");
        edt_lead_price.setSelection(edt_lead_price.getText().length());
        lead_price = "0";
        btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
        btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));

        btn_2.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_3.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_4.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_5.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_6.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_7.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_8.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_free.setTextColor(getResources().getColor(R.color.colorWhite));

    }

    @OnClick(R.id.btn_2)
    public void on2Amount() {
        lead_price = "2";
        edt_lead_price.setText("2");
        txt_doll.setVisibility(View.VISIBLE);
        edt_lead_price.setSelection(edt_lead_price.getText().length());
        btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
        btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));

        btn_2.setTextColor(getResources().getColor(R.color.colorWhite));
        btn_3.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_4.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_5.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_6.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_7.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_8.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_free.setTextColor(getResources().getColor(R.color.colorDarkGreen));
    }

    @OnClick(R.id.btn_3)
    public void on5Amount() {
        lead_price = "5";
        edt_lead_price.setText("5");
        txt_doll.setVisibility(View.VISIBLE);
        edt_lead_price.setSelection(edt_lead_price.getText().length());
        btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
        btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));

        btn_2.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_3.setTextColor(getResources().getColor(R.color.colorWhite));
        btn_4.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_5.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_6.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_7.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_8.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_free.setTextColor(getResources().getColor(R.color.colorDarkGreen));
    }

    @OnClick(R.id.btn_4)
    public void on7dot5Amount() {
        lead_price = "7.5";
        edt_lead_price.setText("7.5");
        txt_doll.setVisibility(View.VISIBLE);
        edt_lead_price.setSelection(edt_lead_price.getText().length());
        btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
        btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));

        btn_2.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_3.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_4.setTextColor(getResources().getColor(R.color.colorWhite));
        btn_5.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_6.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_7.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_8.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_free.setTextColor(getResources().getColor(R.color.colorDarkGreen));
    }

    @OnClick(R.id.btn_5)
    public void on10Amount() {
        lead_price = "10";
        edt_lead_price.setText("10");
        txt_doll.setVisibility(View.VISIBLE);
        edt_lead_price.setSelection(edt_lead_price.getText().length());
        btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
        btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));

        btn_2.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_3.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_4.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_5.setTextColor(getResources().getColor(R.color.colorWhite));
        btn_6.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_7.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_8.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_free.setTextColor(getResources().getColor(R.color.colorDarkGreen));
    }


    @OnClick(R.id.btn_6)
    public void on15Amount() {
        lead_price = "15";
        edt_lead_price.setText("15");
        txt_doll.setVisibility(View.VISIBLE);
        edt_lead_price.setSelection(edt_lead_price.getText().length());
        btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
        btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));

        btn_2.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_3.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_4.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_5.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_6.setTextColor(getResources().getColor(R.color.colorWhite));
        btn_7.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_8.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_free.setTextColor(getResources().getColor(R.color.colorDarkGreen));
    }


    @OnClick(R.id.btn_7)
    public void on20Amount() {
        lead_price = "20";
        edt_lead_price.setText("20");
        txt_doll.setVisibility(View.VISIBLE);
        edt_lead_price.setSelection(edt_lead_price.getText().length());
        btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));
        btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));

        btn_2.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_3.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_4.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_5.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_6.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_7.setTextColor(getResources().getColor(R.color.colorWhite));
        btn_8.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_free.setTextColor(getResources().getColor(R.color.colorDarkGreen));
    }

    @OnClick(R.id.btn_8)
    public void on25Amount() {
        lead_price = "25";
        edt_lead_price.setText("25");
        txt_doll.setVisibility(View.VISIBLE);
        edt_lead_price.setSelection(edt_lead_price.getText().length());
        btn_free.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_2.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_3.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_4.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_5.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_6.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_7.setBackground(getResources().getDrawable(R.drawable.frame_round_full_white));
        btn_8.setBackground(getResources().getDrawable(R.drawable.frame_round_full_lightblue));

        btn_2.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_3.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_4.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_5.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_6.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_7.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        btn_8.setTextColor(getResources().getColor(R.color.colorWhite));
        btn_free.setTextColor(getResources().getColor(R.color.colorDarkGreen));
    }


    public void findPlace(int placeAutocompleteRequestCode) {
        LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
                new LatLng(latitude, longitude), new LatLng(latitude, longitude));

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).build();
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                            .setFilter(typeFilter)
                            .build(SellActivity.this);
            startActivityForResult(intent, placeAutocompleteRequestCode);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                utils.hideKeyboard(SellActivity.this);
                Place place = PlaceAutocomplete.getPlace(SellActivity.this, data);

                String[] arr_save_all = place.toString().split(",");

                String city_name = place.getName()+"";

                LatLng ltlng = place.getLatLng();
                Double latitude = ltlng.latitude;
                Double longitude = ltlng.longitude;
                String adress_to_send = place.getAddress().toString();
                if(adress_to_send.contains(city_name)){
                    adress_to_send = adress_to_send.replace(city_name,"");
                    if(adress_to_send.contains(",,")){
                        adress_to_send = adress_to_send.replace(",,",",");
                    }
                    if(adress_to_send.contains(", ,")){
                        adress_to_send = adress_to_send.replace(", ,",",");
                    }
                    if(adress_to_send.startsWith(",")){
                        adress_to_send =  adress_to_send.substring(1);
                    }
                    if (adress_to_send.endsWith(",")) {
                        adress_to_send = adress_to_send.substring(0, adress_to_send.length() - 1);
                    }
                }
                if(!adress_to_send.equals("")) {
                    adress_to_send = city_name + "," + adress_to_send;
                }else{
                    adress_to_send = city_name ;
                }
                address_bitmap = adress_to_send;

//                getAddress(place.getLatLng().latitude,place.getLatLng().longitude,true,adress_to_send);

                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")) {
                    method_Loc_API(place.getId(), place.getLatLng().latitude, place.getLatLng().longitude,"en");
                }else{
                    method_Loc_API(place.getId(), place.getLatLng().latitude, place.getLatLng().longitude,"he");
                }


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(SellActivity.this, data);
                // TODO: Handle the error.
                Log.i("", status.getStatusMessage());
                if(arr_place.size()>0){
                    rel_add_loc.setVisibility(View.GONE);
                    vw_line_act.setVisibility(View.GONE);
                }else{
                    online = 0;
                    btn_online.setVisibility(View.VISIBLE);
                    txt_sel_loc.setVisibility(View.VISIBLE);
                    txt_online.setVisibility(View.GONE);
                    btn_sel_loc.setVisibility(View.GONE);
                    rel_loc_sel.setVisibility(View.GONE);
                    address_bitmap = "LOCATION NOT SPECIFIED";
                }

            } else if (resultCode == RESULT_CANCELED) {
                if(arr_place.size()>0){
                    rel_add_loc.setVisibility(View.GONE);
                    vw_line_act.setVisibility(View.GONE);
                }else{
                    online = 0;
                    btn_online.setVisibility(View.VISIBLE);
                    txt_sel_loc.setVisibility(View.VISIBLE);
                    txt_online.setVisibility(View.GONE);
                    btn_sel_loc.setVisibility(View.GONE);
                    rel_loc_sel.setVisibility(View.GONE);
                    address_bitmap = "LOCATION NOT SPECIFIED";
                }
            }
        }
    }




    /***Get Address***/
    private void method_Loc_API( String id, final double latitude,final  double longitude,String language ) {
        AndroidNetworking.enableLogging();
        Log.e("url_locApi: ", NetworkingData.BASEURL_LOCATION+ id+"&key="+getResources().getString(R.string.gmail_key)
                +"&language="+language);

        AndroidNetworking.get(NetworkingData.BASEURL_LOCATION+ id+"&key="+getResources().getString(R.string.gmail_key)
                +"&language="+language)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_locApi: ",result+"");
                        //{"status":1,"message":"Details..","admin_settings":[{"id":1,"admin_id":1,"number_of_leads":4,"min_amount":"1
                        //  ","max_lead_price":"100","market_fee":"44"}]}

                        try{
                            JSONObject arr = result.getJSONObject("result");
                            String full_add = arr.getString("formatted_address");
                            String name = arr.getString("name");
                            Log.e("add1: ",full_add);
                            Log.e("name: ",name);
                           /* if(!full_add.contains(name)){
                                full_add = name+","+full_add;
                            }
                            Log.e("add2: ",full_add);*/
                            LatlngPojo pojo = new LatlngPojo();
                            pojo.setName(full_add);
                            pojo.setLat(latitude+"");
                            pojo.setLong_(longitude+"");
                            pojo.setCountryname(full_add);
                            pojo.setAddress(full_add);
                            arr_place.add(pojo);

                            method_AddLoc_in_arry();
                        }catch (Exception e){
                            String g = e.toString();
                        }
                        utils.dismissProgressdialog();


                    }

                    private void method_AddLoc_in_arry() {
                        if(arr_place.size()>0){
                            rel_add_loc.setVisibility(View.GONE);
                            vw_line_act.setVisibility(View.GONE);

                        }else{
                            online = 0;
                            btn_online.setVisibility(View.VISIBLE);
                            txt_sel_loc.setVisibility(View.VISIBLE);
                            txt_online.setVisibility(View.GONE);
                            btn_sel_loc.setVisibility(View.GONE);
                            rel_loc_sel.setVisibility(View.GONE);
                            address_bitmap = "LOCATION NOT SPECIFIED";

                        }



                        recycl_loc.setVisibility(View.VISIBLE);
                        LinearLayoutManager lnr_album = new LinearLayoutManager(SellActivity.this, LinearLayoutManager.VERTICAL, false);
                        recycl_loc.setLayoutManager(lnr_album);
                        adapter_loc = new LocRegister_Adapter(SellActivity.this,arr_place);
                        recycl_loc.setAdapter(adapter_loc);
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();

                    }
                });
    }


    public void getAddress(double lat, double lng, boolean isEdit,String address) {
        Geocoder geocoder = new Geocoder(SellActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            getCountryName = obj.getCountryName();

            try {
                get_cityName = obj.getSubAdminArea().toString();
            } catch (Exception e) {
                e.printStackTrace();
                get_cityName = obj.getLocality();
            }

            LatlngPojo pojo = new LatlngPojo();
            pojo.setName(get_cityName);
            pojo.setLat(lat+"");
            pojo.setLong_(lng+"");
            pojo.setCountryname(getCountryName);
            pojo.setAddress(address);
            arr_place.add(pojo);

            /*sharedPreferenceLeadr.set_LAT_SELL(lat+"");
            sharedPreferenceLeadr.set_LONG_SELL(lng+"");
            sharedPreferenceLeadr.set_LOC_NAME_SELL(get_cityName);
            sharedPreferenceLeadr.set_COUNTRY_SELL(getCountryName);
            sharedPreferenceLeadr.set_LOC_ADDRESS_SELL(address);*/
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }



    private void api_Sell(String lat, String long_, String loc, final String audio_url, String country,String address){
        AndroidNetworking.enableLogging();

        if(lead_price.equals("free")){
            lead_price = "0";
        }
        String budget = edt_budget.getText().toString();
        if(budget.trim().length()==0){
            budget = "0";
        }else{
            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                budget = String.valueOf(Math.round(Float.valueOf(budget)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
            }
        }
        String desc = "";
        if(edt_buss_info.getText().toString().length()>0)
        {
            desc = edt_buss_info.getText().toString();
        }
       /* Log.e("url_sell: ", NetworkingData.BASE_URL+ NetworkingData.SELL);
        Log.e("description",edt_buss_info.getText().toString());
        Log.e("lead_price",lead_price);
        Log.e("budget",edt_budget.getText().toString());
        Log.e("lat",lat);
        Log.e("lon",long_);
        Log.e("location_name",loc);
        Log.e("category",cat_id);
        Log.e("user_id",sharedPreferenceLeadr.getUserId());
        Log.e("cell_number",edt_phone.getText().toString());
        Log.e("client_name",edt_name.getText().toString());
        Log.e("type",String.valueOf(online));
        Log.e("audio",audio_url);
        Log.e("address",address);
        Log.e("country",country);*/

        if(!desc.trim().equalsIgnoreCase("")){
            desc = GlobalConstant.ConvertToBase64(desc).trim();
        }
        if(!loc.trim().equalsIgnoreCase("")){
            loc = GlobalConstant.ConvertToBase64(loc).trim();
        }
        if(!country.trim().equalsIgnoreCase("")){
            country = GlobalConstant.ConvertToBase64(country).trim();
        }
        if(!address.trim().equalsIgnoreCase("")){
            address = GlobalConstant.ConvertToBase64(address).trim();
        }

        String client_nm = edt_name.getText().toString();
        if(!client_nm.trim().equalsIgnoreCase("")){
            client_nm = GlobalConstant.ConvertToBase64(client_nm).trim();
        }

        final LayoutInflater inflater =
                (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout_to_share = inflater.inflate(R.layout.test,null);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.SELL)
                .addBodyParameter("description",desc)
                .addBodyParameter("lead_price",lead_price)
                .addBodyParameter("budget",budget)
                .addBodyParameter("lat",lat)
                .addBodyParameter("lon",long_)
                .addBodyParameter("location_name",loc)
                .addBodyParameter("country",country)
                .addBodyParameter("category",sharedPreferenceLeadr.get_CATEGORY_SEL_ID())
                .addBodyParameter("audio",audio_url)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("cell_number",edt_phone.getText().toString())
                .addBodyParameter("client_name",client_nm)
                .addBodyParameter("type",String.valueOf(online))
                .addBodyParameter("time",String.valueOf(final_secs))
                .addBodyParameter("address",address)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("res_sell: ",response+"");
                        if (response.optString("status").equals("1")) {
                            if(number_call!=null){
                                number_call = null;
                            }

                            SimpleDateFormat format = new SimpleDateFormat("hh:mm");
                            Date date1 = null;
                            Date date2 = null;
                            try {
                                date1 = format.parse(sharedPreferenceLeadr.get_REG_START());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            try {
                                date2 = format.parse(CurrentTimeStart());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            long mills = date1.getTime() - date2.getTime();
                            int mins = (int) (mills/(1000*60)) % 60;

                            Bundle parameters = new Bundle();
                            parameters.putString("DESCRIPTION", mins+" min");
                            if(getIntent().getStringExtra("widget")!=null){
                                parameters.putString("Type_of_source","widget");
                            }else{
                                parameters.putString("Type_of_source", "inside app");
                            }
                            String value = "";
                            if(audio_url!=null){
                                if(!audio_url.trim().equalsIgnoreCase("")){
                                    value = "voice";
                                }
                            }
                            if(edt_buss_info.getText().toString()!=null){
                                if(!edt_buss_info.getText().toString().trim().equalsIgnoreCase("")) {
                                    if (value.equalsIgnoreCase("")) {
                                        value = "text";
                                    } else {
                                        value = value + "text";
                                    }
                                }
                            }
                            parameters.putString("Type_of_lead", value);
                            AppEventsLogger logger = AppEventsLogger.newLogger(SellActivity.this);
                            logger.logEvent("EVENT_NAME_SELL_published A LEAD",
                                    parameters);

                            utils.hideKeyboard(SellActivity.this);
                            utils.dismissProgressdialog();
//                            utils.showProgressDialog(SellActivity.this,getResources().getString(R.string.create_share));



//                            dialog_published1(layout);
                            dialog_published1(layout_to_share);
                        }else{
                            if(response.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SellActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }else{
                                utils.hideKeyboard(SellActivity.this);
                                utils.dialog_msg_show(SellActivity.this,response.optString("message"));
                            }

                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });

    }



    private void api_ReSell(String lat, String long_, String loc, String audio_url, String country,String address){
        AndroidNetworking.enableLogging();
//        utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
        String desc = "";
        if(edt_buss_info.getText().toString().length()>0)
        {
            desc = edt_buss_info.getText().toString();
        }
        if(lead_price.equals("free")){
            lead_price = "0";
        }
        String budget = edt_budget.getText().toString();
        if(budget.trim().length()==0){
            budget = "0";
        }else{
            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                budget = String.valueOf(Math.round(Float.valueOf(budget)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
            }
        }
       /* Log.e("url_sell: ", NetworkingData.BASE_URL+ NetworkingData.SELL);
        Log.e("description",edt_buss_info.getText().toString());
        Log.e("lead_price",lead_price);
        Log.e("budget",edt_budget.getText().toString());
        Log.e("lat",lat);
        Log.e("lon",long_);
        Log.e("location_name",loc);
        Log.e("category",cat_id);
        Log.e("user_id",sharedPreferenceLeadr.getUserId());
        Log.e("cell_number",edt_phone.getText().toString());
        Log.e("client_name",edt_name.getText().toString());
        Log.e("type",String.valueOf(online));
        Log.e("audio",audio_url);
        Log.e("get_lead_id",get_lead_id);
        Log.e("address",address);
*/
        if(!desc.trim().equalsIgnoreCase("")){
            desc = GlobalConstant.ConvertToBase64(desc).trim();
        }

        String client_nm = edt_name.getText().toString();
        if(!client_nm.trim().equalsIgnoreCase("")){
            client_nm = GlobalConstant.ConvertToBase64(client_nm).trim();
        }

        final LayoutInflater inflater =
                (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout_to_share = inflater.inflate(R.layout.test,null);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.SELL)
                .addBodyParameter("description",desc)
                .addBodyParameter("lead_price",lead_price)
                .addBodyParameter("budget",budget)
                .addBodyParameter("lat",lat)
                .addBodyParameter("lon",long_)
                .addBodyParameter("location_name",loc)
                .addBodyParameter("country",country)
                .addBodyParameter("category",sharedPreferenceLeadr.get_CATEGORY_SEL_ID())
                .addBodyParameter("audio",audio_url)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("cell_number",edt_phone.getText().toString())
                .addBodyParameter("client_name",client_nm)
                .addBodyParameter("type",String.valueOf(online))
                .addBodyParameter("time",String.valueOf(final_secs))
                .addBodyParameter("address",address)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        save_changes_bought = "";
                        Log.e("res_sell: ",response+"");
                        if (response.optString("status").equals("1")) {
                            if(number_call!=null){
                                number_call = null;
                            }
                            utils.hideKeyboard(SellActivity.this);

//                            utils.showProgressDialog(SellActivity.this,getResources().getString(R.string.create_share));
                            dialog_published1(layout_to_share);
                        }else{
                            if(response.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(SellActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }else{
                                utils.hideKeyboard(SellActivity.this);
                                utils.dialog_msg_show(SellActivity.this,response.optString("message"));
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });

    }


    private void api_ReSell_Same(String lat, String long_, String loc, String audio_url, String country, String address){
        AndroidNetworking.enableLogging();
//        utils.showProgressDialog(this,getResources().getString(R.string.ceat_wait));
        if(lead_price.equals("free")){
            lead_price = "0";
        }
        String budget = edt_budget.getText().toString();
        if(budget.trim().length()==0){
            budget = "0";
        }else{
            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                budget = String.valueOf(Math.round(Float.valueOf(budget)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
            }
        }
        String desc = "";
        if(edt_buss_info.getText().toString().length()>0)
        {
            desc = edt_buss_info.getText().toString();
        }
        /*Log.e("url_sell: ", NetworkingData.BASE_URL+ NetworkingData.UPDATE_LEAD);
        Log.e("description",edt_buss_info.getText().toString());
        Log.e("lead_price",lead_price);
        Log.e("budget",edt_budget.getText().toString());
        Log.e("lat",lat);
        Log.e("lon",long_);
        Log.e("location_name",loc);
        Log.e("category",cat_id);
        Log.e("user_id",sharedPreferenceLeadr.getUserId());
        Log.e("cell_number",edt_phone.getText().toString());
        Log.e("client_name",edt_name.getText().toString());
        Log.e("type",String.valueOf(online));
        Log.e("audio",audio_url);
        Log.e("get_lead_id",get_lead_id);
        Log.e("address",address);*/

        if(!desc.trim().equalsIgnoreCase("")){
            desc = GlobalConstant.ConvertToBase64(desc).trim();
        }

        String client_nm = edt_name.getText().toString();
        if(!client_nm.trim().equalsIgnoreCase("")){
            client_nm = GlobalConstant.ConvertToBase64(client_nm).trim();
        }

        final LayoutInflater inflater =
                (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout_to_share = inflater.inflate(R.layout.test,null);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.UPDATE_LEAD)
                .addBodyParameter("description",desc)
                .addBodyParameter("lead_price",lead_price)
                .addBodyParameter("budget",budget)
                .addBodyParameter("lat",lat)
                .addBodyParameter("lon",long_)
                .addBodyParameter("location_name",loc)
                .addBodyParameter("country",country)
                .addBodyParameter("category",sharedPreferenceLeadr.get_CATEGORY_SEL_ID())
                .addBodyParameter("audio",audio_url)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("cell_number",edt_phone.getText().toString())
                .addBodyParameter("client_name",client_nm)
                .addBodyParameter("type",String.valueOf(online))
                .addBodyParameter("time",String.valueOf(final_secs))
                .addBodyParameter("lead_id",get_lead_id)
                .addBodyParameter("address",address)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        save_changes = "";
                        Log.e("res_sell: ",response+"");
                        if (response.optString("status").equals("1")) {
                            if(number_call!=null){
                                number_call = null;
                            }
                            utils.hideKeyboard(SellActivity.this);

                            Bundle parameters = new Bundle();
                            parameters.putString("DESCRIPTION", edt_buss_info.getText().toString());

                            AppEventsLogger logger = AppEventsLogger.newLogger(SellActivity.this);
                            logger.logEvent("EVENT_NAME_mysells_edit_lead",
                                    parameters);



                            dialog_published1(layout_to_share);
                        }else{
                            utils.hideKeyboard(SellActivity.this);
                            utils.dialog_msg_show(SellActivity.this,response.optString("message"));
                        }

                        if(getIntent().getStringExtra("resell")!=null){
                            if(getIntent().getStringExtra("resell").equalsIgnoreCase("0")){
                                api_LeadEdit("0");
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });

    }




    public class LocRegister_Adapter extends RecyclerView.Adapter<LocRegister_Adapter.MyViewHolder> {

        Activity                         ctx;
        LocRegister_Adapter.MyViewHolder holderItem;

        public LocRegister_Adapter(Activity ctx, ArrayList<LatlngPojo> arr_place) {
            this.ctx = ctx;
        }


        @Override
        public LocRegister_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(ctx).inflate(R.layout.item_loc, parent, false);
            holderItem = new LocRegister_Adapter.MyViewHolder(itemView);

            return new LocRegister_Adapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(LocRegister_Adapter.MyViewHolder holder, int position) {
            holder.bind(position);
        }


        @Override
        public int getItemCount() {
            return arr_place.size();
        }


        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView txt_loc;
            ImageView img_remove;
            View vw_line;


            public MyViewHolder(View view) {
                super(view);

                txt_loc = (TextView) view.findViewById(R.id.txt_loc);
                img_remove = (ImageView) view.findViewById(R.id.img_remove);
                vw_line = (View) view.findViewById(R.id.vw_line);

                if (view.getTag() != null) {
                    view.setTag(holderItem);
                }


            }

            public void bind(final int i) {

                if(!arr_place.get(i).getCountryname().equals("")){
                    txt_loc.setText(arr_place.get(i).getName()+", "+arr_place.get(i).getCountryname());
                }else {
                    txt_loc.setText(arr_place.get(i).getName());
                }
                txt_loc.setText(arr_place.get(i).getAddress());
                vw_line.setVisibility(View.GONE);

                img_remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        arr_place.remove(arr_place.get(i));

                        notifyDataSetChanged();
                        if(arr_place.size()>0){
                            rel_add_loc.setVisibility(View.GONE);
                            vw_line_act.setVisibility(View.GONE);
                        }else{
                            /*sharedPreferenceLeadr.set_LAT_SELL("");
                            sharedPreferenceLeadr.set_LONG_SELL("");
                            sharedPreferenceLeadr.set_LOC_NAME_SELL("");
                            sharedPreferenceLeadr.set_COUNTRY_SELL("");
                            sharedPreferenceLeadr.set_LOC_ADDRESS_SELL("");*/

                            online = 0;
                            btn_online.setVisibility(View.VISIBLE);
                            txt_sel_loc.setVisibility(View.VISIBLE);
                            txt_online.setVisibility(View.GONE);
                            btn_sel_loc.setVisibility(View.GONE);
                            rel_loc_sel.setVisibility(View.GONE);
                            address_bitmap = "LOCATION NOT SPECIFIED";
                        }
                    }
                });
            }
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions) {
            if (requestCode == 3 || requestCode == 4) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    //denied
                    utils.hideKeyboard(SellActivity.this);
                    if(!utils.checkWriteExternalPermission(SellActivity.this)){
                        dialog_FirstDenyStorage();
                    }else if(!utils.checkMicrophonePermission(SellActivity.this)){
                        dialog_FirstDenyMicro();
                    }
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        if (utils.checkMicrophonePermission(SellActivity.this) &&
                                utils.checkReadExternalPermission(SellActivity.this) &&
                                utils.checkWriteExternalPermission(SellActivity.this)) {
                            utils.hideKeyboard(SellActivity.this);
                            onRecord(true);

                            rel_add_audio.setVisibility(View.GONE);
                            rel_record_audio.setVisibility(View.VISIBLE);
                            method_countDown_Record();
                        }else if(!utils.checkMicrophonePermission(SellActivity.this)){
                            ActivityCompat.requestPermissions(
                                    SellActivity.this,
                                    new String[]{Manifest.permission.RECORD_AUDIO},
                                    4
                            );
                        }else{
                            ActivityCompat.requestPermissions(
                                    SellActivity.this,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    3
                            );
                        }
                    } else {
                        utils.hideKeyboard(SellActivity.this);
                        if(!utils.checkWriteExternalPermission(SellActivity.this)){
                            dialog_FullDeny_Storage();
                        }else if(!utils.checkMicrophonePermission(SellActivity.this)){
                            dialog_FullDeny_Micro();
                        }
//                        dialog_openStoragePer();
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        //do something here.
                    }
                }
            }
            else if(requestCode == 8 ){
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                    //denied
                    utils.hideKeyboard(SellActivity.this);
                    dialog_denyOne_Storage();
                }else{
                    if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                        //allowed
                        dialog_share.dismiss();
                        utils.showProgressDialog(SellActivity.this,getResources().getString(R.string.create_share));
//                        progress_share.setVisibility(View.VISIBLE);
//                        dialog_CardFrstTYm(SellActivity.this,"test");

                        progressDialog_share_lead = ProgressDialog.show(SellActivity.this, "", getResources().getString(R.string.create_share));
                        progressDialog_share_lead.setCancelable(false);
                        progressDialog_share_lead.show();
//                        methodShare_AddContentinOtherLayout(layout_to_share);
                        getBitmapByView(layout_to_share);
                    } else{
                        utils.hideKeyboard(SellActivity.this);
                        dialog_openStoragePer_Storage();
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        //do something here.
                    }
                }
            }
            else if(requestCode == 6 ){
                /***Call Log****/
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    //denied
                    utils.hideKeyboard(SellActivity.this);
                    dialog_denyOne_Phone();
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        if(utils.checkReadContactPermission(SellActivity.this)&&
                                utils.checkReadCallLogPermission(SellActivity.this)&&
                                utils.checkPhoneStatePermission(SellActivity.this)) {
                            Intent i = new Intent(SellActivity.this, CallLogActivity.class);
                            startActivity(i);
//                            overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                        }else if(!utils.checkReadContactPermission(SellActivity.this)){
                            ActivityCompat.requestPermissions(
                                    SellActivity.this,
                                    new String[]{Manifest.permission.READ_CONTACTS},
                                    5
                            );
                        }else {
                            ActivityCompat.requestPermissions(
                                    SellActivity.this,
                                    new String[]{Manifest.permission.READ_PHONE_STATE},
                                    9
                            );
                        }
                    } else {
                        utils.hideKeyboard(SellActivity.this);
                        dialog_openStoragePer_Phone();
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        //do something here.
                    }
                }
            }
            else if(requestCode == 5||requestCode == 9 ){
/***Phone state and Read Contacts*/
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    //denied
                    utils.hideKeyboard(SellActivity.this);
                    if(requestCode==5){
                        dialog_denyOne_Contact();
                    }else {
                        dialog_denyOne_Contact();
                    }
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        if(utils.checkReadContactPermission(SellActivity.this)&&
                                utils.checkReadCallLogPermission(SellActivity.this)&&
                                utils.checkPhoneStatePermission(SellActivity.this)) {
                            Intent i = new Intent(SellActivity.this, CallLogActivity.class);
                            startActivity(i);
//                            overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                        }else if(!utils.checkReadCallLogPermission(SellActivity.this)){
                            ActivityCompat.requestPermissions(
                                    SellActivity.this,
                                    new String[]{Manifest.permission.READ_CALL_LOG},
                                    6
                            );
                        }else {
                            ActivityCompat.requestPermissions(
                                    SellActivity.this,
                                    new String[]{Manifest.permission.READ_PHONE_STATE},
                                    9
                            );
                        }
                    } else {
                        utils.hideKeyboard(SellActivity.this);
                        dialog_openStoragePer_Contact();
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        //do something here.
                    }
                }
            }else{
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    //denied
                    utils.hideKeyboard(SellActivity.this);
                    dialog_denyOne_Contact();
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        if(utils.checkReadContactPermission(SellActivity.this) &&
                                utils.checkReadCallLogPermission(SellActivity.this)  ){
                            Intent i = new Intent(SellActivity.this, CallLogActivity.class);
                            startActivity(i);
//                            overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
                        }else{
                            ActivityCompat.requestPermissions(
                                    SellActivity.this,
                                    new String[]{Manifest.permission.READ_CALL_LOG},
                                    6
                            );
                        }
                    } else {
                        utils.hideKeyboard(SellActivity.this);
                        dialog_openStoragePer_call();
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        //do something here.
                    }
                }
            }
        }

    }


    public  void dialog_openStoragePer_Storage(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SellActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish   = (Button)   dialog.findViewById(R.id.btn_finish);
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_press  = (TextView) dialog.findViewById(R.id.txt_press);
        TextView txt_store  = (TextView) dialog.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Bold(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Bold(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                GlobalConstant.startInstalledAppDetailsActivity(SellActivity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }



    public  void dialog_denyOne_Storage(){
        if(dialog_deny!=null){
            if(dialog_deny.isShowing()){
                try{
                    dialog_deny.dismiss();
                }catch (Exception e){}
            }
        }
        dialog_deny = new BottomSheetDialog (SellActivity.this);
        dialog_deny.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog_deny.setContentView(bottomSheetView);

        dialog_deny.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok = (TextView) dialog_deny.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog_deny.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog_deny.findViewById(R.id.txt_camera);

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_deny.dismiss();
               /* ActivityCompat.requestPermissions(
                        RegistrationActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);*/
            }
        });

        try{
            dialog_deny.show();
        }catch (Exception e){

        }
    }




    public  void dialog_FirstDenyStorage(){
        if(dialog_deny!=null){
            if(dialog_deny.isShowing()){
                try{
                    dialog_deny.dismiss();
                }catch (Exception e){}
            }
        }
        dialog_deny = new BottomSheetDialog (SellActivity.this);
        dialog_deny.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog_deny.setContentView(bottomSheetView);

        dialog_deny.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok = (TextView) dialog_deny.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog_deny.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog_deny.findViewById(R.id.txt_camera);

        txt_title.setText(getResources().getString(R.string.store_per));
        txt_camera.setText(this.getResources().getString(R.string.str_all_storage));

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_deny.dismiss();
                utils.hideKeyboard(SellActivity.this);

            }
        });

        try{
            dialog_deny.show();
        }catch (Exception e){

        }


    }


    public  void dialog_FirstDenyMicro(){
        if(dialog_deny!=null){
            if(dialog_deny.isShowing()){
                try{
                    dialog_deny.dismiss();
                }catch (Exception e){}
            }
        }
        dialog_deny = new BottomSheetDialog (SellActivity.this);
        dialog_deny.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog_deny.setContentView(bottomSheetView);

        dialog_deny.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok     = (TextView) dialog_deny.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog_deny.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog_deny.findViewById(R.id.txt_camera);

        txt_title.setText(getResources().getString(R.string.micro_per));
        txt_camera.setText(this.getResources().getString(R.string.all_micro));

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_deny.dismiss();
                utils.hideKeyboard(SellActivity.this);
            }
        });

        dialog_deny.show();
    }


    public  void dialog_denyOne_Contact(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SellActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);

        txt_title.setText(getResources().getString(R.string.all_contct));
        txt_camera.setText(this.getResources().getString(R.string.cntct_allow));

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                utils.hideKeyboard(SellActivity.this);

            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }


    }


    public  void dialog_denyOne_Phone(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SellActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);

        txt_title.setText(getResources().getString(R.string.phone_per));
        txt_camera.setText(this.getResources().getString(R.string.stor_all_phone));

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                utils.hideKeyboard(SellActivity.this);

            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }


    }


    public  void dialog_FullDeny_Storage(){
        if(dialog_store!=null){
            if(dialog_store.isShowing()){
                dialog_store.dismiss();
            }
        }
        dialog_store = new BottomSheetDialog (SellActivity.this);
        dialog_store.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog_store.setContentView(bottomSheetView);

        dialog_store.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish    = (Button)   dialog_store.findViewById(R.id.btn_finish);
        TextView txt_ok      = (TextView) dialog_store.findViewById(R.id.txt_ok);
        TextView txt_title   = (TextView) dialog_store.findViewById(R.id.txt_title);
        TextView txt_camera  = (TextView) dialog_store.findViewById(R.id.txt_camera);
        TextView txt_press   = (TextView) dialog_store.findViewById(R.id.txt_press);
        TextView txt_store   = (TextView) dialog_store.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        txt_title.setText(getResources().getString(R.string.store_per));
        txt_store.setText(getResources().getString(R.string.turn_store));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_store.dismiss();
                GlobalConstant.startInstalledAppDetailsActivity(SellActivity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_store.dismiss();
                utils.hideKeyboard(SellActivity.this);
            }
        });


        dialog_store.show();

    }


    public  void dialog_FullDeny_Micro(){
        if(dialog_micro!=null){
            if(dialog_micro.isShowing()){
                dialog_micro.dismiss();
            }
        }
        dialog_micro = new BottomSheetDialog (SellActivity.this);
        dialog_micro.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog_micro.setContentView(bottomSheetView);

        dialog_micro.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish    = (Button)   dialog_micro.findViewById(R.id.btn_finish);
        TextView txt_ok      = (TextView) dialog_micro.findViewById(R.id.txt_ok);
        TextView txt_title   = (TextView) dialog_micro.findViewById(R.id.txt_title);
        TextView txt_camera  = (TextView) dialog_micro.findViewById(R.id.txt_camera);
        TextView txt_press   = (TextView) dialog_micro.findViewById(R.id.txt_press);
        TextView txt_store   = (TextView) dialog_micro.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        txt_title.setText(getResources().getString(R.string.micro_per));
        txt_store.setText(getResources().getString(R.string.turn_micro));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_micro.dismiss();
                GlobalConstant.startInstalledAppDetailsActivity(SellActivity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_micro.dismiss();
                utils.hideKeyboard(SellActivity.this);
            }
        });


        dialog_micro.show();

    }


    public  void dialog_openStoragePer_call(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SellActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish    = (Button) dialog.findViewById(R.id.btn_finish);
        TextView txt_ok      = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera  = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_press   = (TextView) dialog.findViewById(R.id.txt_press);
        TextView txt_store   = (TextView) dialog.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        txt_title.setText(getResources().getString(R.string.all_contct));
        txt_store.setText(getResources().getString(R.string.all_contct_three));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                utils.hideKeyboard(SellActivity.this);
                GlobalConstant.startInstalledAppDetailsActivity(SellActivity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }


    public  void dialog_openStoragePer_Phone(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SellActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        Button btn_finish    = (Button) dialog.findViewById(R.id.btn_finish);
        TextView txt_ok      = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera  = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_press   = (TextView) dialog.findViewById(R.id.txt_press);
        TextView txt_store   = (TextView) dialog.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        txt_title.setText(getResources().getString(R.string.all_phn));
        txt_store.setText(getResources().getString(R.string.all_phn_three));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                utils.hideKeyboard(SellActivity.this);
                GlobalConstant.startInstalledAppDetailsActivity(SellActivity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public  void dialog_openStoragePer_Contact(){
        final BottomSheetDialog dialog = new BottomSheetDialog (SellActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        Button btn_finish    = (Button) dialog.findViewById(R.id.btn_finish);
        TextView txt_ok      = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera  = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_press   = (TextView) dialog.findViewById(R.id.txt_press);
        TextView txt_store   = (TextView) dialog.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_camera.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_press.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_store.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        txt_title.setText(getResources().getString(R.string.all_contct));
        txt_store.setText(getResources().getString(R.string.all_contct_three));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                utils.hideKeyboard(SellActivity.this);
                GlobalConstant.startInstalledAppDetailsActivity(SellActivity.this);
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void api_GetCategory() {
        AndroidNetworking.enableLogging();
        Log.e("url_getCat: ", NetworkingData.BASE_URL + NetworkingData.GET_CATEGORY);

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.GET_CATEGORY)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_getCat: ", result + "");
                        utils.dismissProgressdialog();
                        String cat_id_sel = sharedPreferenceLeadr.get_CATEGORY_SEL_ID();
                        Log.e("ids: ",cat_id_sel);
                        if(getIntent().getStringExtra("resell")!=null){
                            if(getIntent().getStringExtra("resell").equals("1")){


                                cat_id_sel = getIntent().getStringExtra("cat");
                            }
                        }


                        String[] arr_id = null;
                        if(cat_id_sel.contains(",")) {
                            arr_id = cat_id_sel.trim().split(",");
                        }else{
                            arr_id = new String[1];
                            arr_id[0] = cat_id_sel;
                        }
                        Set<String> silly = new HashSet<String>(Arrays.asList(arr_id));

                        cat_id = "";
                        cat_name = "";
                        cat_hebrew = "";

                        if (result.optString("status").equals("1")) {
                            try {
                                JSONArray arr = result.getJSONArray("categories");
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject obj = arr.getJSONObject(i);
                                    CategoryPojo item = new CategoryPojo();
                                    item.setCategory(obj.getString("category_name"));
                                    item.setId(obj.getString("id"));
                                    item.setHebrew(obj.getString("hebrew"));

                                    if (silly.contains(obj.getString("id"))) {
                                        if (cat_name.trim().length() > 1) {
                                            cat_id =  cat_id+","+obj.getString("id");
                                            cat_name =  cat_name+","+obj.getString("category_name");
                                            cat_hebrew =  cat_hebrew+","+obj.getString("hebrew");
                                        }else{
                                            cat_id =  obj.getString("id");
                                            cat_name =  obj.getString("category_name");
                                            cat_hebrew =  obj.getString("hebrew");
                                        }
                                        item.setChecked(true);
                                    }else {
                                        item.setChecked(false);
                                    }
                                }
                                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")) {
                                    txt_cate.setText(cat_name);
                                    sharedPreferenceLeadr.set_CATEGORY_SEL_NAME(cat_name);
                                }else{
                                    txt_cate.setText(cat_hebrew);
                                    sharedPreferenceLeadr.set_CATEGORY_SEL_NAME_HE(cat_hebrew);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }


    public  void dialog_published(){
        dialog_share = new BottomSheetDialog (SellActivity.this);
        dialog_share.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_lead_publish, null);
        dialog_share.setContentView(bottomSheetView);

        dialog_share.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_optnl       = (TextView) dialog_share.findViewById(R.id.txt_optnl);
        TextView txt_title       = (TextView) dialog_share.findViewById(R.id.txt_title);
        TextView txt_share       = (TextView) dialog_share.findViewById(R.id.txt_share);
        TextView txt_ok          = (TextView) dialog_share.findViewById(R.id.txt_ok);
        RelativeLayout rel_close = (RelativeLayout) dialog_share.findViewById(R.id.rel_close);
        ImageView img_share      = (ImageView) dialog_share.findViewById(R.id.img_share);

//        lnr_buy_final.setVisibility(View.VISIBLE);
//        lnr_sell.setVisibility(View.GONE);
        txt_title.setAllCaps(false);


        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_share.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_optnl.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        rel_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_share.dismiss();
                save_changes_sell = "";
                finish();
            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (utils.checkReadExternalPermission(SellActivity.this)&&utils.checkWriteExternalPermission(SellActivity.this)) {
//                    utils.showProgressDialog(SellActivity.this,getResources().getString(R.string.create_share));

                    dialog_share.dismiss();

                    progressDialog_share_lead = ProgressDialog.show(SellActivity.this, "", getResources().getString(R.string.create_share));
                    progressDialog_share_lead.setCancelable(false);
                    progressDialog_share_lead.show();
//                    methodShare_AddContentinOtherLayout(layout_to_share);
                    getBitmapByView(layout_to_share);
                } else {
                    utils.dismissProgressdialog();
                    ActivityCompat.requestPermissions(
                            SellActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE},
                            8
                    );
                }
            }
        });

        dialog_share.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                utils.dismissProgressdialog();
                save_changes_sell = "";
                Bundle parameters = new Bundle();
                parameters.putString("Published_to_friends", "false");
                AppEventsLogger logger = AppEventsLogger.newLogger(SellActivity.this);
                logger.logEvent("EVENT_NAME_SELL_published A LEAD",
                        parameters);
                finish();
            }
        });

        dialog_share.show();

    }
    public  void dialog_published1(final View layout){
        dialog_share = new BottomSheetDialog (SellActivity.this);
        dialog_share.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_lead_publish, null);
        dialog_share.setContentView(bottomSheetView);

        dialog_share.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_optnl       = (TextView) dialog_share.findViewById(R.id.txt_optnl);
        TextView txt_title       = (TextView) dialog_share.findViewById(R.id.txt_title);
        TextView txt_share       = (TextView) dialog_share.findViewById(R.id.txt_share);
        TextView txt_ok          = (TextView) dialog_share.findViewById(R.id.txt_ok);
        RelativeLayout rel_close = (RelativeLayout) dialog_share.findViewById(R.id.rel_close);
        ImageView img_share      = (ImageView) dialog_share.findViewById(R.id.img_share);

//

//        lnr_buy_final.setVisibility(View.VISIBLE);
//        lnr_sell.setVisibility(View.GONE);
        txt_title.setAllCaps(false);


        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_share.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_optnl.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        rel_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_share.dismiss();
                save_changes_sell = "";
                finish();
            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (utils.checkReadExternalPermission(SellActivity.this)&&utils.checkWriteExternalPermission(SellActivity.this)) {
//                    utils.showProgressDialog(SellActivity.this,getResources().getString(R.string.create_share));

                    dialog_share.dismiss();
                    progressDialog_share_lead = ProgressDialog.show(SellActivity.this, "", getResources().getString(R.string.create_share));
                    progressDialog_share_lead.setCancelable(false);
                    progressDialog_share_lead.show();

//                    methodShare_AddContentinOtherLayout(layout);
                    getBitmapByView(layout_to_share);
                } else {
                    utils.dismissProgressdialog();
                    ActivityCompat.requestPermissions(
                            SellActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE},
                            8
                    );
                }
            }
        });

        dialog_share.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                utils.dismissProgressdialog();
                save_changes_sell = "";
                Bundle parameters = new Bundle();
                parameters.putString("Published_to_friends", "false");
                AppEventsLogger logger = AppEventsLogger.newLogger(SellActivity.this);
                logger.logEvent("EVENT_NAME_SELL_published A LEAD",
                        parameters);
                finish();
            }
        });

        dialog_share.show();

    }


    void methodShare_AddContentinOtherLayout(View layout){
//        progress_share.setVisibility(View.VISIBLE);

        Bitmap bitmap = getBitmapFromURL(sharedPreferenceLeadr.get_PROFILE_THUMB());
        CircleImageView img_user          =  (CircleImageView)layout.findViewById(R.id.img_user);
        try{
           /* Picasso.with(SellActivity.this).load(sharedPreferenceLeadr.get_PROFILE_THUMB()).fit()
                    .into(img_user);*/
            img_user.setImageBitmap(bitmap);
        }catch (Exception e){}

        TextView txt_client_name = (TextView)layout.findViewById(R.id.txt_client_name);
        TextView txt_des_client  = (TextView)layout.findViewById(R.id.txt_des_client);
        TextView txt_client_phn  = (TextView)layout.findViewById(R.id.txt_client_phn);
        TextView txt_ago         = (TextView)layout.findViewById(R.id.txt_ago);
        TextView txt_loc         = (TextView)layout.findViewById(R.id.txt_loc);
        TextView txt_budget      = (TextView)layout.findViewById(R.id.txt_budget);
        TextView txt_secs        = (TextView)layout.findViewById(R.id.txt_secs);
        TextView txt_des_buy     = (TextView)layout.findViewById(R.id.txt_des_buy);
        TextView txt_lead_price  = (TextView)layout.findViewById(R.id.txt_lead_price);
        TextView txt_read_more   = (TextView)layout.findViewById(R.id.txt_read_more);
        TextView txt_seller      = (TextView)layout.findViewById(R.id.txt_seller);
        ImageView img_file_desc  = (ImageView)layout.findViewById(R.id.img_file_desc);
        ImageView img_one        = (ImageView)layout.findViewById(R.id.img_one);
        ImageView img_shadow     = (ImageView)layout.findViewById(R.id.img_shadow);
        LinearLayout lnr_11      = (LinearLayout)layout.findViewById(R.id.lnr_11);
        View vw_line             = (View) layout.findViewById(R.id.vw_line);
        Button btn_buy           = (Button) layout.findViewById(R.id.btn_buy);

        txt_client_name.setText(sharedPreferenceLeadr.get_username());
        txt_des_client.setText(sharedPreferenceLeadr.get_bus_name());

        txt_seller.setTypeface(utils.OpenSans_Light(SellActivity.this));
        txt_client_name.setTypeface(utils.OpenSans_Light(SellActivity.this));
        txt_des_client.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_budget.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_client_phn.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_loc.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_des_client.setTypeface(utils.OpenSans_Light(SellActivity.this));
        txt_lead_price.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_read_more.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        txt_ago.setTypeface(utils.OpenSans_Regular(SellActivity.this));
        btn_buy.setTypeface(utils.Dina(SellActivity.this));

        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
            txt_lead_price.setGravity(Gravity.LEFT);
        }
        txt_client_phn.setText(edt_phone.getText().toString().substring(0,5)+getResources().getString(R.string.xx));
        txt_ago.setText(getResources().getString(R.string.few_sec_full));
        if(arr_place.size()>0){
            txt_loc.setText(arr_place.get(0).getAddress());
        }else{
            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
        }

        String budget = edt_budget.getText().toString();
        if(budget.trim().length()==0){
            budget = getResources().getString(R.string.bud_unknw);
            txt_budget.setTextColor(getResources().getColor(R.color.gray_budget));
            txt_budget.setText(budget);
        }else{
            txt_budget.setText(getResources().getString(R.string.dollar)+budget);
        }

        txt_secs.setText(String.valueOf(final_secs));

        if(edt_buss_info.getText().toString().trim().length()>0) {
            txt_des_buy.setText(edt_buss_info.getText().toString());
            if(str_upload==null){
                img_file_desc.setVisibility(View.VISIBLE);
            }
        }

        if(lead_price.equals("free")){
            txt_lead_price.setText(getResources().getString(R.string.free));
        }else if(lead_price.equalsIgnoreCase("0")){
            txt_lead_price.setText(getResources().getString(R.string.free));
        }else{
            txt_lead_price.setText(getResources().getString(R.string.dollar)+" "+lead_price);
        }

        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        if(String.valueOf(final_secs).equals("0")){
            lnr_11.setVisibility(View.GONE);
            img_one.setVisibility(View.GONE);
            img_file_desc.setVisibility(View.VISIBLE);
        }else{
            lnr_11.setVisibility(View.VISIBLE);
            img_one.setVisibility(View.VISIBLE);
            img_file_desc.setVisibility(View.INVISIBLE);
        }
        String gg = edt_buss_info.getText().toString();
        if(gg.contains("\n")){
            gg = edt_buss_info.getText().toString().replaceAll("\n","........................................");
        }

        txt_read_more.setTypeface(utils.OpenSans_Regular(SellActivity.this));

        if(screenSize==Configuration.SCREENLAYOUT_SIZE_LARGE){
            int length_desc = edt_buss_info.getText().toString().length();

            if(gg.length()>150){
                txt_read_more.setVisibility(View.VISIBLE);
                img_shadow.setVisibility(View.VISIBLE);
                vw_line.setVisibility(View.GONE);
            }else{
                txt_read_more.setVisibility(View.INVISIBLE);
                img_shadow.setVisibility(View.GONE);
                vw_line.setVisibility(View.VISIBLE);
            }
        }
        else if(screenSize==Configuration.SCREENLAYOUT_SIZE_NORMAL){
            if(gg.length()>80){
                txt_read_more.setVisibility(View.VISIBLE);
                img_shadow.setVisibility(View.VISIBLE);
                vw_line.setVisibility(View.GONE);
            }else{
                txt_read_more.setVisibility(View.INVISIBLE);
                img_shadow.setVisibility(View.GONE);
                vw_line.setVisibility(View.VISIBLE);
            }
        }
        else if(screenSize==Configuration.SCREENLAYOUT_SIZE_SMALL){
            if(gg.length()>40){
                txt_read_more.setVisibility(View.VISIBLE);
                img_shadow.setVisibility(View.VISIBLE);
                vw_line.setVisibility(View.GONE);
            }else{
                txt_read_more.setVisibility(View.INVISIBLE);
                img_shadow.setVisibility(View.GONE);
                vw_line.setVisibility(View.VISIBLE);
            }
        }
    }


    public Bitmap getBitmapFromURL(String strURL) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG,20/100,byteArrayOutputStream);
            return myBitmap;
        } catch (Exception e) {
            return null;
        }
    }


    public  void getBitmapByView(View layout) {
        int h = 0;
        Bitmap bitmap = null;

        progressDialog_share_lead = ProgressDialog.show(SellActivity.this, "", getResources().getString(R.string.create_share));
        progressDialog_share_lead.setCancelable(false);
        progressDialog_share_lead.show();


        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        // create bitmap with target size
        bitmap = Bitmap.createBitmap(metrics.widthPixels,metrics.heightPixels,
                Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
//        progress_share.setVisibility(View.VISIBLE);
        methodShare_AddContentinOtherLayout(layout_to_share);
        layout.setDrawingCacheEnabled(true);
        layout.measure(
                MeasureSpec.makeMeasureSpec(canvas.getWidth(),MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(canvas.getHeight(),MeasureSpec.EXACTLY));
        layout.layout(0,0,layout.getMeasuredWidth(),layout.getMeasuredHeight());
        canvas.drawBitmap(layout.getDrawingCache(),0,0,new Paint());

//        scrollView.draw(canvas);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream("/sdcard/screen_test.png");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            if (null != out) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
            }
        } catch (IOException e) {
            // TODO: handle exception
        }
        share_bitMap_to_Apps(bitmap);
    }


    public void share_bitMap_to_Apps(Bitmap bitmap)
    {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

//        progress_share.setVisibility(View.GONE);
//        dialog_card.dismiss();

        utils.dismissProgressdialog();
        if(progressDialog_share_lead!=null){
            if(progressDialog_share_lead.isShowing()){
                progressDialog_share_lead.dismiss();
            }
        }

        Bundle parameters = new Bundle();
        parameters.putString("Published_to_friends", "true");
        AppEventsLogger logger = AppEventsLogger.newLogger(SellActivity.this);
        logger.logEvent("EVENT_NAME_SELL_published A LEAD",
                parameters);


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "Download LeadR.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/png");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/Download LeadR.jpg"));
        startActivity(Intent.createChooser(intent, "Share"));

        finish();


    }



    /***
     * Set lead is in edit mode***/
    private void api_LeadEdit(String type){
        AndroidNetworking.enableLogging();
        Log.e("url_leadEdit: ", NetworkingData.BASE_URL+ NetworkingData.LEAD_EDIT_MODE);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.LEAD_EDIT_MODE)
                .addBodyParameter("lead_id",getIntent().getStringExtra("lead_id"))
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("type",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_lead_edit: ",result+"");

                        utils.dismissProgressdialog();

                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }


    /***
     * CHeck ADMIN Settings to set price range***/
    private void api_chck_AdminSettings(){
        progress_share.setVisibility(View.VISIBLE);
        AndroidNetworking.enableLogging();
        Log.e("url_chkSetting: ", NetworkingData.BASE_URL+ NetworkingData.GET_ADMIN_SETTING);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.GET_ADMIN_SETTING)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chkadmin: ",result+"");
                        //{"status":1,"message":"Details..","admin_settings":[{"id":1,"admin_id":1,"number_of_leads":4,"min_amount":"1
                        //  ","max_lead_price":"100","market_fee":"44"}]}

                        try{
                            JSONArray arr = result.getJSONArray("admin_settings");
                            for (int i = 0; i < arr.length(); i++) {
                                JSONObject obj  = arr.getJSONObject(i);
                                admin_max_price = obj.optString("max_lead_price");

                                String hh = sharedPreferenceLeadr.get_PRICE_ILS();
                                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                    admin_max_price = String.valueOf(Math.round(Float.valueOf(admin_max_price)*
                                            Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                                }
                            }
                        }catch (Exception e){}
                        utils.dismissProgressdialog();
                        progress_share.setVisibility(View.GONE);
                        if(!admin_max_price.equals("")){
                            try{
                                if(Integer.valueOf(admin_max_price)<Integer.valueOf(lead_price)) {
                                    dialog_msg_showWithTitle(SellActivity.this, getResources().getString(R.string.mor_lead_price)
                                                    +" "+ getResources().getString(R.string.dollar)+" " + admin_max_price
                                            , getResources().getString(R.string.err), edt_lead_price);
                                }else{
                                    if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                        lead_price = String.valueOf(Math.round(Float.valueOf(lead_price)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                                    }
                                    method_SellLEad();
                                }
                            }catch (Exception e){
                                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                    lead_price = String.valueOf(Math.round(Float.valueOf(lead_price)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                                }
                                method_SellLEad();
                            }
                        }else{
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                lead_price = String.valueOf(Math.round(Float.valueOf(lead_price)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                            }
                            method_SellLEad();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();
                        progress_share.setVisibility(View.GONE);
                        if(!admin_max_price.equals("")){
                            try{
                                if(Integer.valueOf(admin_max_price)<Integer.valueOf(lead_price)) {
                                    dialog_msg_showWithTitle(SellActivity.this, getResources().getString(R.string.mor_lead_price)
                                                    +" "+ getResources().getString(R.string.dollar)+" " + admin_max_price
                                            , getResources().getString(R.string.err), edt_lead_price);
                                }else{
                                    if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                        lead_price = String.valueOf(Math.round(Float.valueOf(lead_price)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                                    }
                                    method_SellLEad();
                                }
                            }catch (Exception e){
                                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                    lead_price = String.valueOf(Math.round(Float.valueOf(lead_price)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                                }
                                method_SellLEad();
                            }
                        }else{
                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                lead_price = String.valueOf(Math.round(Float.valueOf(lead_price)/Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                            }
                            method_SellLEad();
                        }
                    }
                });

    }

    public  void dialog_CardFrstTYm(Activity activity, String msg){
        dialog_card = new BottomSheetDialog (activity);
        dialog_card.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog_card.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog_card.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog_card.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog_card.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog_card.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        txt_title.setText(getResources().getString(R.string.pls_wait));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog_card.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_card.dismiss();
            }
        });
        dialog_card.show();


    }

}
