package leadr.com.leadr.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import adapter.BusinessCat_Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import modal.CategoryPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;
import utils.UploadThumbAwsBuss;
import utils.UploadThumbBuss;

/**
 * Created by Admin on 2/20/2018.
 */

public class EditRegistration_3_Activity extends AppCompatActivity {

    @BindView(R.id.lnr_1)
    LinearLayout lnr_1;

    @BindView(R.id.lnr_3)
    LinearLayout lnr_3;

    @BindView(R.id.txt_bus_cc)
    TextView txt_bus_cc;

    @BindView(R.id.txt_three)
    TextView txt_three;

    @BindView(R.id.edt_search)
    EditText edt_search;

    @BindView(R.id.recycl_cat)
    RecyclerView recycl_cat;

    @BindView(R.id.btn_nxt)
    Button btn_nxt;

    @BindView(R.id.btn_finish)
    Button btn_finish;

    @BindView(R.id.lnr_bottom_3)
    LinearLayout lnr_bottom_3;

    @BindView(R.id.lnr_bottom_3_2)
    LinearLayout lnr_bottom_3_2;

    @BindView(R.id.lnr_2)
    LinearLayout lnr_2;

    @BindView(R.id.card_full)
    CardView card_full;

    @BindView(R.id.card_full2)
    CardView card_full2;

    @BindView(R.id.vw_shadow)
    View vw_shadow;

    @BindView(R.id.rel_bottom2)
    RelativeLayout rel_bottom2;

    @BindView(R.id.rel_bottom)
    RelativeLayout rel_bottom;

    @BindView(R.id.txt_two)
    TextView txt_two;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.img_back_top)
    ImageView img_back_top;

    @BindView(R.id.rel_edit)
    RelativeLayout rel_edit;

    @BindView(R.id.rel_reg)
    RelativeLayout rel_reg;

    GlobalConstant utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;
    BusinessCat_Adapter adapter_cat;

    ArrayList<CategoryPojo> arr_cat = new ArrayList<>();
    ArrayList<CategoryPojo> arr_cat_checked = new ArrayList<>();

    String cate_sel = "";
    String cate_sel_he = "";
    String cate_sel_id = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_reg);

        ButterKnife.bind(this);

        utils = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(EditRegistration_3_Activity.this);

        txt_bus_cc.setTypeface(utils.OpenSans_Regular(EditRegistration_3_Activity.this));
        edt_search.setTypeface(utils.OpenSans_Regular(EditRegistration_3_Activity.this));
        btn_finish.setTypeface(utils.OpenSans_Regular(EditRegistration_3_Activity.this));

        lnr_3.setVisibility(View.VISIBLE);
        lnr_bottom_3.setVisibility(View.GONE);
        lnr_bottom_3_2.setVisibility(View.GONE);
        lnr_2.setVisibility(View.GONE);
        card_full.setVisibility(View.GONE);
        card_full2.setVisibility(View.VISIBLE);
        vw_shadow.setVisibility(View.VISIBLE);
        rel_bottom2.setVisibility(View.GONE);
        rel_bottom.setVisibility(View.VISIBLE);
        img_back.setVisibility(View.VISIBLE);
        txt_three.setBackground(getResources().getDrawable(R.drawable.three_ac));
        txt_two.setBackground(getResources().getDrawable(R.drawable.two_ac));

        btn_nxt.setVisibility(View.GONE);
        lnr_1.setVisibility(View.GONE);
        lnr_3.setVisibility(View.VISIBLE);
        btn_finish.setVisibility(View.VISIBLE);

        btn_finish.setText(getResources().getString(R.string.save));

        edt_search.setTextColor(getResources().getColor(R.color.colorBlack));
        if (utils.isNetworkAvailable(EditRegistration_3_Activity.this)) {
            api_GetCategory();
        } else {
            utils.hideKeyboard(EditRegistration_3_Activity.this);
            utils.dialog_msg_show(EditRegistration_3_Activity.this, getResources().getString(R.string.no_internet));
        }

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (adapter_cat != null) {
                    adapter_cat.getFilter().filter(cs);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    @OnClick(R.id.img_back_top)
    public void OnBack2() {
        finish();
    }



    @Override
    protected void onResume() {
        super.onResume();
        rel_edit.setVisibility(View.VISIBLE);
        rel_reg.setVisibility(View.GONE);
        img_back.setVisibility(View.GONE);
    }

    @OnClick(R.id.img_back)
    public void OnBack() {
        finish();
    }


    @OnClick(R.id.btn_finish)
    public void OnSave() {
        utils.hideKeyboard(EditRegistration_3_Activity.this);

        if(cate_sel.equals("")){
            utils.hideKeyboard(EditRegistration_3_Activity.this);
            utils.dialog_msg_show(EditRegistration_3_Activity.this,getResources().getString(R.string.enter_buss_cat));
        }else if(cate_sel_he.equals("")){
            utils.hideKeyboard(EditRegistration_3_Activity.this);
            utils.dialog_msg_show(EditRegistration_3_Activity.this,getResources().getString(R.string.enter_buss_cat));
        }else{
            api_Register();
        }
    }





    private void api_Register() {
        AndroidNetworking.enableLogging();
        utils.dismissProgressdialog();
        utils.showProgressDialog(this, getResources().getString(R.string.load));

        String dev_token = "";
        try {
            dev_token = FirebaseInstanceId.getInstance().getToken();
        } catch (Exception e) {
            dev_token = sharedPreferenceLeadr.get_dev_token();
        }
//        phone = "+917696137554";

      /*  Log.e("url_signup: ", NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO);
        Log.e("job_title: ",  edt_jobtitle.getText().toString());
        Log.e("phone_number: ",  phone);
        Log.e("user_name: ",  edt_fullname.getText().toString());
        Log.e("business_name: ",  edt_buss_name.getText().toString());
        Log.e("business_fb_page: ",  edt_buss_page.getText().toString());
        Log.e("business_info: ",  edt_buss_info.getText().toString());
        Log.e("location_name: ",  loc);
        Log.e("lat: ",  lat);
        Log.e("lon: ",  long_);
        Log.e("category: ",  cate_sel_id);
        Log.e("user_image: ",  user_full);
        Log.e("user_image_thumb: ",  user_thumb);
        Log.e("business_image: ",  buss_full);
        Log.e("business_image_thumb: ",  buss_thumb);
        Log.e("device_token: ",  dev_token);
        Log.e("device_type: ",  "android");
        Log.e("id: ",  sharedPreferenceLeadr.get_UserId_Bfr());*/

        String job_title = sharedPreferenceLeadr.get_jobTitle();
        job_title = GlobalConstant.ConvertToBase64(job_title).trim();
        String id = sharedPreferenceLeadr.get_UserId_Bfr();
        String phone_number = sharedPreferenceLeadr.get_phone();
        String user_name = sharedPreferenceLeadr.get_username();
        user_name = GlobalConstant.ConvertToBase64(user_name).trim();
        String bus_nm = sharedPreferenceLeadr.get_bus_name();
        bus_nm = GlobalConstant.ConvertToBase64(bus_nm).trim();
        String bus_info = sharedPreferenceLeadr.get_bus_info();
        bus_info = GlobalConstant.ConvertToBase64(bus_info).trim();
        String device_token = dev_token;

        String loc_name = "0";
        String country_name = "0";
        String add_ = "0";
        if(!sharedPreferenceLeadr.get_LOC_NAME().equalsIgnoreCase("")||!sharedPreferenceLeadr.get_LOC_NAME().equalsIgnoreCase("0")){
            loc_name = sharedPreferenceLeadr.get_LOC_NAME();
            loc_name = GlobalConstant.ConvertToBase64(loc_name).trim();
            country_name = sharedPreferenceLeadr.get_LOC_NAME_COUNTRY();
            country_name = GlobalConstant.ConvertToBase64(country_name).trim();
            add_ = sharedPreferenceLeadr.get_ADDRESS();
            add_ = GlobalConstant.ConvertToBase64(add_).trim();
        }


        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.UPDATE_PRO_VER2)
                .addBodyParameter("job_title", job_title)
                .addBodyParameter("id", id)
                .addBodyParameter("phone_number", phone_number)
                .addBodyParameter("user_name", user_name)
                .addBodyParameter("business_name", bus_nm)
                .addBodyParameter("business_fb_page", sharedPreferenceLeadr.get_bus_site())
                .addBodyParameter("business_info",bus_info )
                .addBodyParameter("location_name", loc_name)
                .addBodyParameter("lat", sharedPreferenceLeadr.get_LATITUDE())
                .addBodyParameter("lon", sharedPreferenceLeadr.get_LONGITUDE())
                .addBodyParameter("category", cate_sel_id)
                .addBodyParameter("user_image", sharedPreferenceLeadr.getProfilePicUrl())
                .addBodyParameter("business_image", sharedPreferenceLeadr.get_bus_img())
                .addBodyParameter("device_token", dev_token)
                .addBodyParameter("device_type", "android")
                .addBodyParameter("country", country_name)
                .addBodyParameter("address", add_)
                .addBodyParameter("user_thum", sharedPreferenceLeadr.get_PROFILE_THUMB())
                .addBodyParameter("busi_thum", sharedPreferenceLeadr.get_BUSS_THUMB())
                .addBodyParameter("email", sharedPreferenceLeadr.get_EMAIL_REG())

                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        sharedPreferenceLeadr.setUserId(sharedPreferenceLeadr.get_UserId_Bfr());
                        if (response.optString("status").equals("1")) {
                            sharedPreferenceLeadr.set_CATEGORY(cate_sel_id);
                            sharedPreferenceLeadr.set_CAT_FILTER(cate_sel_id);
                            sharedPreferenceLeadr.set_CAT_FILTER_ID_TEMP(cate_sel_id);
                            finish();
                        } else {
                            if(response.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(EditRegistration_3_Activity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }else{
                                utils.hideKeyboard(EditRegistration_3_Activity.this);
                                utils.dialog_msg_show(EditRegistration_3_Activity.this, response.optString("message"));
                            }

                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });

    }

    public  void dialog_msg_show(Activity activity, String msg, final EditText edt){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                try{
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (Exception e){}
                edt.requestFocus();
            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }


    }


    private void api_GetCategory() {
        utils.showProgressDialog(EditRegistration_3_Activity.this,getResources().getString(R.string.load));
        AndroidNetworking.enableLogging();
        Log.e("url_getCat: ", NetworkingData.BASE_URL + NetworkingData.GET_CATEGORY);

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.GET_CATEGORY)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_getCat: ", result + "");
                        utils.dismissProgressdialog();
                        arr_cat.clear();


                        String cat_id_sel = sharedPreferenceLeadr.get_CATEGORY();
                        String[] arr_id = null;
                        if(cat_id_sel.contains(",")) {
                             arr_id = cat_id_sel.trim().split(",");
                        }else{
                            arr_id = new String[1];
                            arr_id[0] = cat_id_sel;
                        }
                        Set<String> silly = new HashSet<String>(Arrays.asList(arr_id));

                        if (result.optString("status").equals("1")) {
                            try {
                                JSONArray arr = result.getJSONArray("categories");
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject obj = arr.getJSONObject(i);
                                    CategoryPojo item = new CategoryPojo();
                                    item.setCategory(obj.getString("category_name"));
                                    item.setId(obj.getString("id"));
                                    item.setHebrew(obj.getString("hebrew"));
                                    if (silly.contains(obj.getString("id"))) {
                                        item.setChecked(true);
                                    }else {
                                        item.setChecked(false);
                                    }
                                    arr_cat.add(item);
                                }


                                LinearLayoutManager lnr_album = new LinearLayoutManager(EditRegistration_3_Activity.this, LinearLayoutManager.VERTICAL, false);
                                recycl_cat.setLayoutManager(lnr_album);
                                adapter_cat = new BusinessCat_Adapter(EditRegistration_3_Activity.this, arr_cat, new BusinessCat_Adapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(int i) {
                                        utils.hideKeyboard(EditRegistration_3_Activity.this);
                                        if (arr_cat.get(i).getChecked()) {
                                            BusinessCat_Adapter.arr_cat_filter.get(i).setChecked(false);
                                            adapter_cat.notifyDataSetChanged();
                                        } else {
                                            BusinessCat_Adapter.arr_cat_filter.get(i).setChecked(true);
                                            adapter_cat.notifyDataSetChanged();
                                        }
                                        arr_cat_checked.clear();
                                        cate_sel = "";
                                        cate_sel_id = "";
                                        cate_sel_he = "";
                                        for (int j = 0; j < BusinessCat_Adapter.arr_cat_filter.size(); j++) {
                                            if (BusinessCat_Adapter.arr_cat_filter.get(j).getChecked()) {
                                                arr_cat_checked.add(BusinessCat_Adapter.arr_cat_filter.get(j));
                                                if (cate_sel.trim().length() > 1) {
                                                    cate_sel = cate_sel + "," + BusinessCat_Adapter.arr_cat_filter.get(j).getCategory();
                                                    cate_sel_id = cate_sel_id + "," + BusinessCat_Adapter.arr_cat_filter.get(j).getId();
                                                    cate_sel_he = cate_sel_he + "," + BusinessCat_Adapter.arr_cat_filter.get(j).getHebrew();
                                                } else {
                                                    cate_sel = BusinessCat_Adapter.arr_cat_filter.get(j).getCategory();
                                                    cate_sel_id = BusinessCat_Adapter.arr_cat_filter.get(j).getId();
                                                    cate_sel_he = BusinessCat_Adapter.arr_cat_filter.get(j).getHebrew();
                                                }

                                            }

                                        }
                                    }
                                });
                                recycl_cat.setAdapter(adapter_cat);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(EditRegistration_3_Activity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }
}
