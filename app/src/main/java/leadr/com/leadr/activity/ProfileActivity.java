package leadr.com.leadr.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.BasePostprocessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.squareup.picasso.Picasso;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import adapter.BusinessCat_Adapter;
import adapter.ProfileCatAdapter;
import adapter.ProfileLocAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import modal.CategoryPojo;
import modal.LatlngPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;


public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.img_user)
    SimpleDraweeView img_user;

    @BindView(R.id.img_buss)
    SimpleDraweeView img_buss;

    @BindView(R.id.img_buss_dummy)
    ImageView img_buss_dummy;

    @BindView(R.id.img_edt_img)
    ImageView img_edt_img;

    @BindView(R.id.img_edt_two)
    ImageView img_edt_two;

    @BindView(R.id.edt_my_name)
    EditText edt_my_name;

    @BindView(R.id.txt_info)
    TextView txt_info;

    @BindView(R.id.txt_bus_info)
    TextView txt_bus_info;

    @BindView(R.id.edt_des_client)
    EditText edt_des_client;

    @BindView(R.id.edt_buss_name)
    EditText edt_buss_name;

    @BindView(R.id.edt_buss_info)
    EditText edt_buss_info;

    @BindView(R.id.edt_buss_page)
    EditText edt_buss_page;

    @BindView(R.id.img_edt_three)
    ImageView img_edt_three;

    @BindView(R.id.txt_bus_cat)
    TextView txt_bus_cat;

    @BindView(R.id.lst_cate)
    ListView lst_cate;

    @BindView(R.id.lst_loc)
    ListView lst_loc;

    @BindView(R.id.txt_bus_loc)
    TextView txt_bus_loc;

    @BindView(R.id.img_edt_four)
    ImageView img_edt_four;

    @BindView(R.id.img_edt_five)
    ImageView img_edt_five;

    @BindView(R.id.txt_pay_info)
    TextView txt_pay_info;

    @BindView(R.id.txt_card)
    TextView txt_card;

    @BindView(R.id.txt_pro)
    TextView txt_pro;

    @BindView(R.id.txt_no_loc)
    TextView txt_no_loc;

    @BindView(R.id.txt_cat_sel)
    TextView txt_cat_sel;

    @BindView(R.id.txt_phn)
    TextView txt_phn;

    @BindView(R.id.txt_phnnmbr)
    TextView txt_phnnmbr;

    @BindView(R.id.txt_miss_img)
    TextView txt_miss_img;

    @BindView(R.id.txt_wrk)
    TextView txt_wrk;

    @BindView(R.id.txt_email)
    TextView txt_email;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.img_edt_wrk)
    ImageView img_edt_wrk;

    @BindView(R.id.progres_load)
    ProgressBar progres_load;

    @BindView(R.id.progres_load_user)
    ProgressBar progres_load_user;

    @BindView(R.id.progres_load_buss)
    ProgressBar progres_load_buss;

    @BindView(R.id.frame_buss)
    FrameLayout frame_buss;

    GlobalConstant        utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    ArrayList<String>       arr_cat_sel = new ArrayList<>();
    ArrayList<CategoryPojo> arr_cat_all = new ArrayList<>();
    ArrayList<LatlngPojo>   arr_place = new ArrayList<>();

    int edit_               = 0;
    String cate_sel_id      = "";
    String cate_sel_name    = "";
    String cate_sel_name_he = "";
    String user_full        = "";
    String user_thumb       = "";

    ProfileCatAdapter adapter_cate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // bind the view using butterknife
        ButterKnife.bind(this);

        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(ProfileActivity.this);

        methodTypeface();

        lst_cate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                if (arr_cat_all.get(i).getChecked()) {
                    if (adapter_cate != null) {
                        BusinessCat_Adapter.arr_cat_filter.get(i).setChecked(false);
                        adapter_cate.notifyDataSetChanged();
                    } else {
                        BusinessCat_Adapter.arr_cat_filter.get(i).setChecked(true);
                        adapter_cate.notifyDataSetChanged();
                    }
                }
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        progres_load.setVisibility(View.GONE);
        if(SelCategoryProfileActivity.cate_sel!=null){
            txt_cat_sel.setText(SelCategoryProfileActivity.cate_sel);
            cate_sel_id      = SelCategoryProfileActivity.cate_sel_id;
            cate_sel_name    = SelCategoryProfileActivity.cate_sel;
            cate_sel_name_he = SelCategoryProfileActivity.cate_sel_he;
            SelCategoryProfileActivity.cate_sel     = null;
            SelCategoryProfileActivity.cate_sel_id  = null;
            SelCategoryProfileActivity.cate_sel     = null;
            SelCategoryProfileActivity.cate_sel_he  = null;
        }

        user_full  = sharedPreferenceLeadr.getProfilePicUrl();
        user_thumb = sharedPreferenceLeadr.get_PROFILE_THUMB();

        if (utils.isNetworkAvailable(ProfileActivity.this)) {
            api_GetCategory();
        } else {
            utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.no_internet));
        }

        if(sharedPreferenceLeadr.get_LOC_NAME().equals("")){
            if(sharedPreferenceLeadr.get_LOC_TYPE().equals("0")){
                txt_no_loc.setVisibility(View.VISIBLE);
                lst_loc.setVisibility(View.GONE);
            }else{
                method_Add_Loc();
            }
        }
        else if(sharedPreferenceLeadr.get_LOC_NAME().equals("0")){
            if(sharedPreferenceLeadr.get_LOC_TYPE().equals("0")){
                txt_no_loc.setVisibility(View.VISIBLE);
                lst_loc.setVisibility(View.GONE);
            }else{
                method_Add_Loc();
            }
        }else{
            if(sharedPreferenceLeadr.get_LOC_TYPE().equals("0")){
                txt_no_loc.setVisibility(View.VISIBLE);
                lst_loc.setVisibility(View.GONE);
            }else{
                method_Add_Loc();
            }
        }

        methodSetSharedPrefValues();
        methodCalculatePercnt();

    }



    private void methodSetSharedPrefValues() {
        txt_phn.setText(sharedPreferenceLeadr.get_phone());
        edt_my_name.setText(sharedPreferenceLeadr.get_username());
        edt_buss_name.setText(sharedPreferenceLeadr.get_bus_name());

        /*Set email*/
        if(sharedPreferenceLeadr.get_EMAIL_REG().equals("")){
            String text = "<font color=#EA4142>"+getResources().getString(R.string.mis_email)+"</font>";
            txt_email.setText(Html.fromHtml(text));
        }else{
            txt_email.setText(sharedPreferenceLeadr.get_EMAIL_REG());
        }

        /*Set job title*/
        if(sharedPreferenceLeadr.get_jobTitle().equalsIgnoreCase("")){
            if(sharedPreferenceLeadr.getProfilePicUrl().equalsIgnoreCase("")){
                String text = "<font color=#EA4142>"+getResources().getString(R.string.miss_job)+"</font>";
                edt_des_client.setText(Html.fromHtml(text.trim()));
            }else {
                String text = "<font color=#EA4142>"+getResources().getString(R.string.miss_jobtitle)+"</font>";
                edt_des_client.setText(Html.fromHtml(text.trim()));
            }
        }else{
            edt_des_client.setText(sharedPreferenceLeadr.get_jobTitle().trim());
        }

        /*Set buss info*/
        if(sharedPreferenceLeadr.get_bus_info().equalsIgnoreCase("")){
            String text = "<font color=#EA4142>"+getResources().getString(R.string.miss_buss)+"</font>";
            edt_buss_info.setText(Html.fromHtml(text.trim()));
        }else {
            edt_buss_info.setText(sharedPreferenceLeadr.get_bus_info().trim());
        }

        /*Set buss site*/
        if(sharedPreferenceLeadr.get_bus_site().equalsIgnoreCase("")){
            String text = "<font color=#EA4142>"+getResources().getString(R.string.miss_web)+"</font>";
            edt_buss_page.setText(Html.fromHtml(text));
        }else {
            edt_buss_page.setText(sharedPreferenceLeadr.get_bus_site());
        }

        /*Set card details*/
        if(sharedPreferenceLeadr.get_CARD().equalsIgnoreCase("N/A")) {
            String text = "<font color=#EA4142>"+getResources().getString(R.string.miss_card_info)+"</font>";
            txt_card.setText(Html.fromHtml(text));
        }else{
            txt_card.setText(getResources().getString(R.string.card_savd));
        }

        /*Set buss image*/
        if(sharedPreferenceLeadr.get_BUSS_THUMB().trim().length()<2){
            txt_miss_img.setVisibility(View.VISIBLE);
            img_buss_dummy.setVisibility(View.VISIBLE);
            img_buss.setVisibility(View.GONE);
            progres_load_buss.setVisibility(View.GONE);
            frame_buss.setVisibility(View.GONE);
        }else{
            txt_miss_img.setVisibility(View.GONE);
            img_buss_dummy.setVisibility(View.GONE);
            img_buss.setVisibility(View.VISIBLE);
            progres_load_buss.setVisibility(View.VISIBLE);
            frame_buss.setVisibility(View.VISIBLE);

            Uri uri = Uri.parse(sharedPreferenceLeadr.get_BUSS_THUMB());
//            img_buss.setImageURI(uri);

            DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                    ImageRequestBuilder.newBuilderWithSource(Uri.parse(sharedPreferenceLeadr.get_BUSS_THUMB()))
                            .setLocalThumbnailPreviewsEnabled(true)
                            .setPostprocessor(new BasePostprocessor() {
                                @Override
                                public void process(Bitmap bitmap) {
                                }
                            })
                            .build())
                    .setControllerListener(new BaseControllerListener<ImageInfo>() {
                        @Override
                        public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                            progres_load_buss.setVisibility(View.GONE);
                        }
                        @Override
                        public void onFailure(String id, Throwable throwable) {
                            super.onFailure(id, throwable);
                            progres_load_buss.setVisibility(View.GONE);
                        }
                        @Override
                        public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                            progres_load_buss.setVisibility(View.GONE);
                        }
                    })
                    .build();
            img_buss.setController(controller);
        }


        /*Set user image*/
        if(sharedPreferenceLeadr.get_PROFILE_THUMB().trim().length()<2){
            progres_load_user.setVisibility(View.GONE);
            Picasso.with(ProfileActivity.this).load(R.drawable.user).placeholder(R.drawable.user)
                    .into(img_user);
        }else{
            progres_load_user.setVisibility(View.VISIBLE);
            Uri uri = Uri.parse(sharedPreferenceLeadr.get_PROFILE_THUMB());
//            img_user.setImageURI(uri);

            DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                    ImageRequestBuilder.newBuilderWithSource(Uri.parse(sharedPreferenceLeadr.get_PROFILE_THUMB()))
                            .setLocalThumbnailPreviewsEnabled(true)
                            .setPostprocessor(new BasePostprocessor() {
                                @Override
                                public void process(Bitmap bitmap) {
                                }
                            })
                            .build())
                    .setControllerListener(new BaseControllerListener<ImageInfo>() {
                        @Override
                        public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                            progres_load.setVisibility(View.GONE);
                        }
                        @Override
                        public void onFailure(String id, Throwable throwable) {
                            super.onFailure(id, throwable);
                            progres_load.setVisibility(View.GONE);
                        }
                        @Override
                        public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                            progres_load.setVisibility(View.GONE);
                        }
                    })
                    .build();
            img_user.setController(controller);
        }
    }


    /***percentage added in strings file***/
    private void methodCalculatePercnt() {
        int tot_perc = 0;
        if(!sharedPreferenceLeadr.get_username().equalsIgnoreCase("")){
            tot_perc = Integer.valueOf(getResources().getString(R.string.prcnt_fullnm));
        }  if(!sharedPreferenceLeadr.get_jobTitle().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_job_tit));
        }  if(!sharedPreferenceLeadr.get_PROFILE_THUMB().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_userimg));
        } if(!sharedPreferenceLeadr.get_bus_name().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_busnm));
        }if(!sharedPreferenceLeadr.get_bus_site().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_bussite));
        }if(!sharedPreferenceLeadr.get_bus_img().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_busimg));
        }if(!sharedPreferenceLeadr.get_bus_info().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_businfo));
        }if(!sharedPreferenceLeadr.get_CATEGORY().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_buscat));
        }if(!sharedPreferenceLeadr.get_LOC_NAME().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_busloc));
        }if(!sharedPreferenceLeadr.get_EMAIL_REG().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_email));
        }
        txt_pro.setText(getResources().getString(R.string.my_pro)+" ("+tot_perc+"%)");
    }


    /***ADD LOCATION**************
     * ******
     * */
    void method_Add_Loc(){
        txt_no_loc.setVisibility(View.GONE);
        lst_loc.setVisibility(View.VISIBLE);

        arr_place.clear();


        String loc_sel     = sharedPreferenceLeadr.get_LOC_NAME();
        String lat_sel     = sharedPreferenceLeadr.get_LATITUDE();
        String long_sel    = sharedPreferenceLeadr.get_LONGITUDE();
        String country_sel = sharedPreferenceLeadr.get_LOC_NAME_COUNTRY();

        if(loc_sel.contains("@")){
            String[] animalsArray = loc_sel.split("@");
            String[] longArray = long_sel.split(",");
            String[] latArray = lat_sel.split(",");
            String[] countryArray = country_sel.split("@");
            for(int i=0; i<animalsArray.length; i++){
                LatlngPojo item = new LatlngPojo();
                item.setName(animalsArray[i].trim());
                item.setLong_(longArray[i].trim());
                item.setLat(latArray[i].trim());
                item.setCountryname(countryArray[i].trim());

                arr_place.add(item);
            }
        }else{
            LatlngPojo item = new LatlngPojo();
            item.setName(loc_sel.trim());
            item.setLong_(long_sel.trim());
            item.setLat(lat_sel.trim());
            item.setCountryname(country_sel.trim());

            arr_place.add(item);
        }

        ProfileLocAdapter adapter_loc = new ProfileLocAdapter(arr_place,ProfileActivity.this);
        lst_loc.setAdapter(adapter_loc);
        GlobalConstant.setListViewHeightBasedOnItems(lst_loc,arr_place.size());
    }


    /***TYpeface of screen****/
    private void methodTypeface() {
        edt_my_name.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        edt_des_client.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_info.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_bus_info.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        edt_buss_name.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        edt_buss_info.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        edt_buss_page.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_bus_cat.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_bus_loc.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_pay_info.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_card.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_pro.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_no_loc.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_cat_sel.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_phn.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_phnnmbr.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_miss_img.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_wrk.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_email.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));

        edt_my_name.setEnabled(false);
        edt_des_client.setEnabled(false);
        edt_buss_name.setEnabled(false);
        edt_buss_info.setEnabled(false);
        edt_buss_page.setEnabled(false);
    }


    /***Display full image when clicked on user image***/
    @OnClick(R.id.img_user)
    public void OnFullProfileImage() {
        if(sharedPreferenceLeadr.getProfilePicUrl().trim().length()>2) {
            dialog_fullImg(sharedPreferenceLeadr.getProfilePicUrl(),sharedPreferenceLeadr.get_PROFILE_THUMB());
        }
    }


    /***Display full image when clicked on user image***/
    @OnClick(R.id.img_buss)
    public void OnBussFullImage() {
        if(sharedPreferenceLeadr.get_bus_img().trim().length()>2) {
            dialog_fullImg(sharedPreferenceLeadr.get_bus_img(),sharedPreferenceLeadr.get_BUSS_THUMB());
        }
    }


/***Display image full for  : user profile and buss image*/
    public  void dialog_fullImg(final String img, final String thumb) {
        final Dialog dialog = new Dialog(ProfileActivity.this, android.R.style.Theme_Holo_NoActionBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_full_image_fresco);

        final Button btn_close            = (Button) dialog.findViewById(R.id.btn_close);
        final SimpleDraweeView draweeView = (SimpleDraweeView) dialog.findViewById(R.id.img_full);
        final ProgressBar progres_load    = (ProgressBar) dialog.findViewById(R.id.progres_load);


        btn_close.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        progres_load.setVisibility(View.VISIBLE);
        DraweeController controller;

        Uri uri = Uri.parse(thumb);
//        draweeView.setImageURI(uri);

        controller = Fresco.newDraweeControllerBuilder().setImageRequest(
                ImageRequestBuilder.newBuilderWithSource(Uri.parse(img)).setRotationOptions(RotationOptions.disableRotation())
                        .setLocalThumbnailPreviewsEnabled(true)
                        .setPostprocessor(new BasePostprocessor() {
                            @Override
                            public void process(Bitmap bitmap) {
                            }
                        })
                        .build())
                .setControllerListener(new BaseControllerListener<ImageInfo>() {
                    @Override
                    public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                        progres_load.setVisibility(View.GONE);
                    }
                    @Override
                    public void onFailure(String id, Throwable throwable) {
                        super.onFailure(id, throwable);
                        progres_load.setVisibility(View.GONE);
                    }
                    @Override
                    public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                        progres_load.setVisibility(View.GONE);
                    }
                })
                .build();
        draweeView.setController(controller);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @OnClick(R.id.img_back)
    public void Onback() {
       try{
           SelCategoryProfileActivity.cate_sel = null;
       }catch (Exception e){}
       finish();
    }



    /**On user own detail edit option(first block)*/
    @OnClick(R.id.img_edt_img)
    public void OnEdit1() {
       Intent i = new Intent(ProfileActivity.this,EditRegistration_1_Activity.class);
       startActivity(i);
    }

    /**On buss detail edit option(2nd block)*/
    @OnClick(R.id.img_edt_two)
    public void OnEdit2() {
        Intent i = new Intent(ProfileActivity.this,EditRegistration_2_Activity.class);
        startActivity(i);
    }


    /**On category edit option(4rd block)*/
    @OnClick(R.id.img_edt_three)
    public void OnEdit3() {
        Intent i = new Intent(ProfileActivity.this,EditRegistration_3_Activity.class);
        startActivity(i);
    }


    /**On location edit option(5th block)*/
    @OnClick(R.id.img_edt_four)
    public void OnEdit4() {
        Intent i = new Intent(ProfileActivity.this,EditRegistration_4_Activity.class);
        startActivity(i);
    }


    /**On email edit option(3rd block)*/
    @OnClick(R.id.img_edt_wrk)
    public void OnEdit6() {
        Intent i = new Intent(ProfileActivity.this,EditRegistration_6_Activity.class);
        startActivity(i);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try{
            SelCategoryProfileActivity.cate_sel = null;
        }catch (Exception e){}
    }


    private void api_GetCategory() {
        AndroidNetworking.enableLogging();
        Log.e("url_getCat: ", NetworkingData.BASE_URL + NetworkingData.GET_CATEGORY);

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.GET_CATEGORY)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_getCat: ", result + "");
                        utils.dismissProgressdialog();
                        cate_sel_name = "";
                        cate_sel_id = "";
                        cate_sel_name_he = "";

                        arr_cat_sel.clear();
                        arr_cat_all.clear();

                        String cat_id_sel = sharedPreferenceLeadr.get_CATEGORY();
                        if(cat_id_sel.contains(",")) {
                            String[] arr_id = cat_id_sel.split(",");
                            for(int j=0; j<arr_id.length; j++){
                                arr_cat_sel.add(arr_id[j].trim());
                            }
                        }else{
                            arr_cat_sel.add(cat_id_sel.trim());
                        }

                        if (result.optString("status").equals("1")) {
                            try {
                                JSONArray arr = result.getJSONArray("categories");
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject obj = arr.getJSONObject(i);
                                    for(int k=0; k<arr_cat_sel.size(); k++){
                                        if(obj.getString("id").equals(arr_cat_sel.get(k))){
                                            CategoryPojo item = new CategoryPojo();
                                            item.setCategory(obj.getString("category_name"));
                                            item.setId(obj.getString("id"));
                                            item.setHebrew(obj.getString("hebrew"));
                                            item.setChecked(false);
                                            arr_cat_all.add(item);
                                            if(cate_sel_id.equalsIgnoreCase("")){
                                                cate_sel_id = obj.getString("id");
                                                cate_sel_name = obj.getString("category_name");
                                                cate_sel_name_he = obj.getString("hebrew");
                                            }else{
                                                cate_sel_id = cate_sel_id+","+obj.getString("id");
                                                cate_sel_name = cate_sel_name+","+obj.getString("category_name");
                                                cate_sel_name_he = cate_sel_name_he+","+obj.getString("hebrew");
                                            }
                                        }
                                    }
                                }

                                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("en")){
                                    txt_cat_sel.setText(cate_sel_name);
                                }else{
                                    txt_cat_sel.setText(cate_sel_name_he);

                                }

                                adapter_cate = new ProfileCatAdapter(arr_cat_all,ProfileActivity.this);
                                lst_cate.setAdapter(adapter_cate);
                                if(cate_sel_name.contains(",")) {
                                    lst_cate.setVisibility(View.VISIBLE);
                                    txt_cat_sel.setVisibility(View.GONE);
                                }
                                GlobalConstant.setListViewHeightBasedOnItems(lst_cate,arr_cat_all.size());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            if(result.optString("message").contains("Suspended account")){
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                String languageToLoad  = "en";
                                Locale locale = new Locale(languageToLoad);
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                Intent i_nxt = new Intent(ProfileActivity.this, GetStartedActivity.class);
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });
    }


    /***Hidden view, not in use currently(payment edit)****/
    @OnClick(R.id.img_edt_five)
    public void OnEdit5() {
            edit_ = 5;
            edt_my_name.setEnabled(false);
            edt_des_client.setEnabled(false);

            edt_buss_name.setEnabled(false);
            edt_buss_info.setEnabled(false);
            edt_buss_page.setEnabled(false);

            dialog_AddCard();
    }
    /*not in use*/
    public  void dialog_AddCard() {
        final Dialog dialog = new Dialog(ProfileActivity.this, android.R.style.Theme_Holo_NoActionBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_payment);

        final Button btnok           = (Button) dialog.findViewById(R.id.btnok);
        final EditText edt_cvv       = (EditText) dialog.findViewById(R.id.edt_cvv);
        final EditText edt_month     = (EditText) dialog.findViewById(R.id.edt_month);
        final EditText edt_year      = (EditText) dialog.findViewById(R.id.edt_year);
        final EditText edt_card_no   = (EditText) dialog.findViewById(R.id.edt_card_no);
        final EditText edt_full_name = (EditText) dialog.findViewById(R.id.edt_full_name);
        final ImageView img_cancel   = (ImageView) dialog.findViewById(R.id.img_cancel);
        final TextView txt_top       = (TextView) dialog.findViewById(R.id.txt_top);

        btnok.setTypeface(utils.Dina(ProfileActivity.this));
        edt_cvv.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        edt_month.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        edt_year.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        edt_card_no.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        edt_full_name.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_top.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));

        String text = "";
        if(sharedPreferenceLeadr.get_CARD().equalsIgnoreCase("N/A")){
            text = "<font color=#227cec>"+getResources().getString(R.string.frst_tym)+
                    "</font> <font color=#071a30>"+getResources().getString(R.string.you_must)+"</font>";
        }else{
            text =
                    "</font> <font color=#071a30>"+getResources().getString(R.string.ur_card)+"</font>";
        }
        txt_top.setText(Html.fromHtml(text));


        edt_cvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 3) {
                    utils.hideKeyboard(ProfileActivity.this);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        edt_card_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 16) {
                    edt_month.requestFocus();
                    edt_month.setSelection(edt_month.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        edt_month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 2) {
                    edt_year.requestFocus();
                    edt_year.setSelection(edt_year.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edt_year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    edt_month.requestFocus();
                    edt_month.setSelection(edt_month.getText().length());
                }else if(s.length() == 4){
                    edt_cvv.requestFocus();
                    edt_cvv.setSelection(edt_cvv.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                method_check_values();
            }

            void method_check_values() {
                utils.hideKeyboard(ProfileActivity.this);
                if (edt_full_name.getText().toString().trim().length() > 0) {
                    if (edt_card_no.getText().toString().trim().length() > 0) {
                        if (edt_month.getText().toString().trim().length() == 2) {
                            if (edt_year.getText().toString().trim().length() == 4) {
                                if (edt_cvv.getText().toString().trim().length() > 0) {
                                    Card card = new Card(edt_card_no.getText().toString(), Integer.valueOf(edt_month.getText().toString()),
                                            Integer.valueOf(edt_year.getText().toString()),
                                            edt_cvv.getText().toString());
                                    if (!card.validateCard()) {
                                        utils.hideKeyboard(ProfileActivity.this);
                                        if (!card.validateNumber()) {
                                            utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.in_cvv));
                                        } else if (!card.validateExpMonth()) {
                                            utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.exp));
                                        } else if (!card.validateExpiryDate()) {
                                            utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.expyr));
                                        } else if (!card.validateCVC()) {
                                            utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.cvv_in));
                                        }
                                    } else {
                                        String substr = edt_card_no.getText().toString().substring(edt_card_no.getText().toString().
                                                length() - 4);
//                                        sharedPreferenceLeadr.set_CARD(substr);
                                        utils.hideKeyboard(ProfileActivity.this);
                                        dialog.dismiss();
//                                        utils.showProgressDialog(ProfileActivity.this, getResources().getString(R.string.pls_wait));
                                        if(progres_load!=null){
                                            progres_load.setVisibility(View.VISIBLE);
                                        }
                                        method_SendToStripe(card,substr);
                                    }
                                } else {
                                    utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.entr_cvv));
                                }
                            } else {
                                utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.entr_yr));
                            }
                        } else {
                            utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.entr_mm));
                        }
                    } else {
                        utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.entr_nmbr));
                    }
                } else {
                    utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.entr_namer));
                }
            }


            void method_SendToStripe(Card card,final String subcard) {
                Stripe stripe = new Stripe(ProfileActivity.this, "pk_live_r9TQXzdQ0OeTo6NiRxHJQxft");
                stripe.createToken(
                        card,
                        new TokenCallback() {
                            public void onSuccess(Token token) {
                                if (utils.isNetworkAvailable(ProfileActivity.this)) {
                                    api_SaveToken(token.getId(),subcard);
                                } else {
                                    utils.hideKeyboard(ProfileActivity.this);
                                    utils.dialog_msg_show(ProfileActivity.this, getResources().getString(R.string.no_internet));
                                }
                            }

                            public void onError(Exception error) {
                                utils.dismissProgressdialog();
                                utils.hideKeyboard(ProfileActivity.this);
                                utils.dialog_msg_show(ProfileActivity.this, error.toString());
                                if(progres_load!=null){
                                    progres_load.setVisibility(View.GONE);
                                }
                            }
                        }
                );
            }


            private void api_SaveToken(String token, final String subcard) {
                AndroidNetworking.enableLogging();
                Log.e("url_stripe: ", NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN);

                AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN)
                        .addBodyParameter("token", token)
                        .addBodyParameter("user_id", sharedPreferenceLeadr.getUserId())
                        .addBodyParameter("last_digits", subcard)
                        .setTag("msg")
                        .setPriority(Priority.HIGH).doNotCacheResponse()
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject result) {
                                Log.e("res_stripe: ", result + "");
                                //{"status":"1","customer":{"id":"cus_C4xJzoWD2YfwOO","object":"customer","account_balance":0,"created":1515128721,"currency":null,"default_source":"card_1BgnOOI0GVRrJIpDo8dLjinv","delinquent":false,"description":"Customer for LeadR app","discount":null,"email":null,"livemode":false,"metadata":{},"shipping":null,"sources":{"object":"list","data":[{"id":"card_1BgnOOI0GVRrJIpDo8dLjinv","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"Visa","country":"US","customer":"cus_C4xJzoWD2YfwOO","cvc_check":"pass","dynamic_last4":null,"exp_month":11,"exp_year":2022,"fingerprint":"kXn425uHw3F6DtGT","funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null}],"has_more":false,"total_count":1,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/subscriptions"}}}
                                if(progres_load!=null){
                                    progres_load.setVisibility(View.GONE);
                                }

                                if (result.optString("status").equals("1")) {
                                    AppEventsLogger logger = AppEventsLogger.newLogger(ProfileActivity.this);
                                    logger.logEvent("EVENT_NAME_ADDED_PAYMENT_INFO");
                                    sharedPreferenceLeadr.set_CARD(subcard);
                                    txt_card.setText(getResources().getString(R.string.card_savd));
                                } else {
                                    utils.dismissProgressdialog();

                                    dialog_errorCard();
                                }
                                utils.dismissProgressdialog();
                                if(dialog!=null) {
                                    if(dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                }
                            }

                            @Override
                            public void onError(ANError error) {
                                Log.i("", "---> On error  ");
                                utils.dismissProgressdialog();
                                if(progres_load!=null){
                                    progres_load.setVisibility(View.GONE);
                                }
                            }
                        });
            }
        });

        dialog.show();
    }
    /*not in use*/
    public  void dialog_errorCard(){
        final BottomSheetDialog dialog = new BottomSheetDialog (ProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_card_error, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        TextView txt_update = (TextView) dialog.findViewById(R.id.txt_update);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView btn_update = (Button) dialog.findViewById(R.id.btn_update);

        txt_title.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_ok.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        txt_update.setTypeface(utils.OpenSans_Regular(ProfileActivity.this));
        btn_update.setTypeface(utils.OpenSans_Bold(ProfileActivity.this));

        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialog_AddCard();
            }
        });

        dialog.show();


    }
}
