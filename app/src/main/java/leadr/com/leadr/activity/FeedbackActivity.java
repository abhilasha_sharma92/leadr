package leadr.com.leadr.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.appevents.AppEventsLogger;

import org.json.JSONObject;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

import static utils.GlobalConstant.PAGINATION_IDS_BOUGHT;


public class FeedbackActivity extends AppCompatActivity {

    @BindView(R.id.lnr_not_now)
    LinearLayout lnr_not_now;

    @BindView(R.id.txt_not_now)
    TextView txt_not_now;

    @BindView(R.id.txt_how_mch)
    TextView txt_how_mch;

    @BindView(R.id.edt_feedback)
    EditText edt_feedback;

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.btn_send)
    Button btn_send;

    SharedPreferenceLeadr sharedPreferenceLeadr;
    GlobalConstant utils;
    String userid ;
    String leadid ;
    String user_id ;
    String leadprice ;
    public static String save_changes_bought ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        ButterKnife.bind(this);

        sharedPreferenceLeadr = new SharedPreferenceLeadr(FeedbackActivity.this);
        utils = new GlobalConstant();

        methodSetTypeface();

         if (getIntent().getStringExtra("feedback") != null) {
            txt_how_mch.setText(getResources().getString(R.string.want_new_ftr));
        } else if (getIntent().getStringExtra("like") != null) {
            txt_how_mch.setText(getResources().getString(R.string.i_dnt));
        } else if (getIntent().getStringExtra("bug") != null) {
            txt_how_mch.setText(getResources().getString(R.string.i_bug));
        }  else if (getIntent().getStringExtra("somethng") != null) {
            txt_how_mch.setText(getResources().getString(R.string.smtng));
        } else if (getIntent().getStringExtra("refund") != null) {
             img_back.setVisibility(View.VISIBLE);
             txt_not_now.setVisibility(View.GONE);
             leadid = getIntent().getStringExtra("leadid");
             user_id = getIntent().getStringExtra("user_id");
             leadprice = getIntent().getStringExtra("leadprice");
             txt_how_mch.setText(getResources().getString(R.string.rfnd_tckt));
             edt_feedback.setHint(getResources().getString(R.string.ref_feed));
        }
        if(getIntent().getStringExtra("user_id")!=null){
            userid = getIntent().getStringExtra("user_id");
        }else{
            userid = sharedPreferenceLeadr.getUserId();
        }
    }

    private void methodSetTypeface() {
        txt_not_now.setTypeface(utils.OpenSans_Regular(FeedbackActivity.this));
        txt_how_mch.setTypeface(utils.OpenSans_Regular(FeedbackActivity.this));
        edt_feedback.setTypeface(utils.OpenSans_Regular(FeedbackActivity.this));
        btn_send.setTypeface(utils.Dina(FeedbackActivity.this));
    }

    @OnClick(R.id.lnr_not_now)
    public void onBack() {
        if (getIntent().getStringExtra("rate") != null) {
            finish();
        } else {
            onBackPressed();
        }
    }

    @OnClick(R.id.btn_send)
    public void onSendFeedback() {
        if(edt_feedback.getText().toString().trim().length()>0) {
            if(getIntent().getStringExtra("refund")!=null){
                api_refund();
            }else {
                methodFeedback();
            }
        }else{
            utils.dialog_msg_show_WithTitle(FeedbackActivity.this,getResources().getString(R.string.enter_msg),getResources().getString(R.string.feed_miss));
        }
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getStringExtra("rate") != null) {
            finish();
        }  else if (getIntent().getStringExtra("feedback") != null) {
            finish();
        } else if (getIntent().getStringExtra("like") != null) {
            finish();
        } else if (getIntent().getStringExtra("bug") != null) {
            finish();
        } else if (getIntent().getStringExtra("somethng") != null) {
            finish();
        } else if (getIntent().getStringExtra("refund") != null) {
            finish();
        }  else {
            AccountKit.logOut();
            sharedPreferenceLeadr.clearPreference();
            Intent i_nxt = new Intent(FeedbackActivity.this, GetStartedActivity.class);
            i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i_nxt);
            finish();
        }

    }




    private void methodFeedback() {
        AndroidNetworking.enableLogging();
        String type = "1";
        if (getIntent().getStringExtra("rate") != null) {
            type = "5";
        } else if (getIntent().getStringExtra("feedback") != null) {
            type = "1";
        } else if (getIntent().getStringExtra("like") != null) {
            type = "2";
        } else if (getIntent().getStringExtra("bug") != null) {
            type = "3";
        }else if (getIntent().getStringExtra("refund") != null) {
            type = "6";
        }else if (getIntent().getStringExtra("somethng") != null) {
            type = "7";
        } else {
            type = "4";
        }
        utils.showProgressDialog(this, getResources().getString(R.string.load));
        Log.e("url_getfeedback: ", NetworkingData.BASE_URL + NetworkingData.FEEDBACK);
        Log.e("userid: ", sharedPreferenceLeadr.getUserId());
        Log.e("content: ", edt_feedback.getText().toString());
        Log.e("type: ", type);

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.FEEDBACK)
                .addBodyParameter("user_id",userid)
                .addBodyParameter("content",edt_feedback.getText().toString())
                .addBodyParameter("type",type)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_delacc: ", result + "");
                        utils.dismissProgressdialog();

                        if(result.optString("status").equalsIgnoreCase("1")) {
                            if (getIntent().getStringExtra("rate") != null) {
                                Intent i_nxt = new Intent(FeedbackActivity.this, ThankYouActivity.class);
                                startActivity(i_nxt);
                                finish();
                            } else if (getIntent().getStringExtra("feedback") != null) {
                                Intent i_nxt = new Intent(FeedbackActivity.this, ThankYouActivity.class);
                                startActivity(i_nxt);
                                finish();
                            } else if (getIntent().getStringExtra("like") != null) {
                                Intent i_nxt = new Intent(FeedbackActivity.this, ThankYouActivity.class);
                                startActivity(i_nxt);
                                finish();
                            } else if (getIntent().getStringExtra("bug") != null) {
                                Intent i_nxt = new Intent(FeedbackActivity.this, ThankYouActivity.class);
                                startActivity(i_nxt);
                                finish();
                            } else if (getIntent().getStringExtra("somethng") != null) {
                                Intent i_nxt = new Intent(FeedbackActivity.this, ThankYouActivity.class);
                                startActivity(i_nxt);
                                finish();
                            } else if (getIntent().getStringExtra("refund") != null) {
                                api_refund();
                            } else {
                                AccountKit.logOut();
                                sharedPreferenceLeadr.clearPreference();
                                Intent i_nxt = new Intent(FeedbackActivity.this, ThankYouActivity.class);
                                i_nxt.putExtra("del", "del");
                                i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i_nxt);
                                finish();
                            }
                        }else  if(result.optString("message").contains("Suspended account")){
                            AccountKit.logOut();
                            sharedPreferenceLeadr.clearPreference();
                            String languageToLoad  = "en";
                            Locale locale = new Locale(languageToLoad);
                            Locale.setDefault(locale);
                            Configuration config = new Configuration();
                            config.locale = locale;
                            getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                            Intent i_nxt = new Intent(FeedbackActivity.this, GetStartedActivity.class);
                            i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i_nxt);
                            finish();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }


    private void api_refund(){
        AndroidNetworking.enableLogging();
        utils.showProgressDialog(FeedbackActivity.this,getResources().getString(R.string.load));
        Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.REFUND);
        Log.e("user_id: ", sharedPreferenceLeadr.getUserId());
        Log.e("lead_id: ", leadid);
        Log.e("lead_user_id: ", user_id);
        Log.e("req_for: ", "2");

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.REFUND)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("lead_id",leadid)
                .addBodyParameter("lead_user_id",user_id)
                .addBodyParameter("lead_price",leadprice)
                .addBodyParameter("req_for","2")
                .addBodyParameter("feedback",edt_feedback.getText().toString())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chkCard: ",result+"");
                        utils.dismissProgressdialog();


                        if(result.optString("status").equalsIgnoreCase("1")){
                            save_changes_bought = "";
                            PAGINATION_IDS_BOUGHT = "1";
                            finish();
                        }else{
                            utils.dialog_msg_show(FeedbackActivity.this,result.optString("message"));
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();
                    }
                });

    }

    @Override
    protected void onResume() {
        super.onResume();
        save_changes_bought = null;
    }
}
