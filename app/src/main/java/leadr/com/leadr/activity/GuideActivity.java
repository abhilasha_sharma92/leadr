package leadr.com.leadr.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import utils.GlobalConstant;
import utils.NetworkingData;


/**
 * Created by Abhilasha on 4/12/2017.
 */

public class GuideActivity extends AppCompatActivity {


    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.txt_head)
    TextView txt_head;

    @BindView(R.id.txt_head_1)
    TextView txt_head_1;

    @BindView(R.id.txt_head_2)
    TextView txt_head_2;

    GlobalConstant utils;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.guide);

        // bind the view using butterknife
        ButterKnife.bind(this);
        utils = new GlobalConstant();

        txt_head.setTypeface(utils.OpenSans_Regular(GuideActivity.this));
        txt_head_1.setTypeface(utils.OpenSans_Regular(GuideActivity.this));
        txt_head_2.setTypeface(utils.OpenSans_Regular(GuideActivity.this));

    }

    @OnClick(R.id.img_back)
    public void onback() {
       onBackPressed();
    }
}
