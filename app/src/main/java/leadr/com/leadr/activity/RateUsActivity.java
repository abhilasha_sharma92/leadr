package leadr.com.leadr.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leadr.com.leadr.R;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;

public class RateUsActivity extends AppCompatActivity {

    @BindView(R.id.txt_not_now)
    TextView txt_not_now;

    @BindView(R.id.txt_thank)
    TextView txt_thank;

    @BindView(R.id.txt_rate_us)
    TextView txt_rate_us;

    @BindView(R.id.txt_rateus)
    TextView txt_rateus;

    @BindView(R.id.txt_u_made)
    TextView txt_u_made;

    @BindView(R.id.btn_send)
    Button btn_send;

    @BindView(R.id.btn_no)
    Button btn_no;

    @BindView(R.id.btn_yes)
    Button btn_yes;

    @BindView(R.id.rel_1)
    RelativeLayout rel_1;

    @BindView(R.id.rel_2)
    RelativeLayout rel_2;


    @BindView(R.id.lnr_not_now)
    LinearLayout lnr_not_now;

    SharedPreferenceLeadr sharedPreferenceLeadr;
    GlobalConstant utils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_us);

        ButterKnife.bind(this);

        sharedPreferenceLeadr = new SharedPreferenceLeadr(RateUsActivity.this);
        utils = new GlobalConstant();

//        sharedPreferenceLeadr.set_DAYPASSED("1");

        methodSetTypeface();
        rel_1.setVisibility(View.VISIBLE);
        rel_2.setVisibility(View.GONE);

    }

    private void methodSetTypeface() {
        txt_not_now.setTypeface(utils.OpenSans_Regular(RateUsActivity.this));
        txt_thank.setTypeface(utils.OpenSans_Regular(RateUsActivity.this));
        txt_rate_us.setTypeface(utils.OpenSans_Regular(RateUsActivity.this));
        btn_send.setTypeface(utils.OpenSans_Regular(RateUsActivity.this));
        txt_rateus.setTypeface(utils.OpenSans_Regular(RateUsActivity.this));
        txt_u_made.setTypeface(utils.OpenSans_Regular(RateUsActivity.this));
    }



    @OnClick(R.id.lnr_not_now)
    public void onBack() {
        AppEventsLogger logger = AppEventsLogger.newLogger(RateUsActivity.this);
        logger.logEvent("EVENT_NAME_RATED");
       Intent i_feed = new Intent(RateUsActivity.this,FeedbackActivity.class);
       i_feed.putExtra("rate","1");
       startActivity(i_feed);
       finish();
    }



    @OnClick(R.id.btn_send)
    public void onPlaystore() {
        AppEventsLogger logger = AppEventsLogger.newLogger(RateUsActivity.this);
        logger.logEvent("EVENT_NAME_RATED");
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            finish();
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            finish();
        }
    }



    @OnClick(R.id.btn_yes)
    public void onYes() {
        rel_1.setVisibility(View.GONE);
        rel_2.setVisibility(View.VISIBLE);
    }



    @OnClick(R.id.btn_no)
    public void onNo() {
        AppEventsLogger logger = AppEventsLogger.newLogger(RateUsActivity.this);
        logger.logEvent("EVENT_NAME_RATED");
        Intent i_feed = new Intent(RateUsActivity.this,FeedbackActivity.class);
        i_feed.putExtra("rate","1");
        startActivity(i_feed);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
