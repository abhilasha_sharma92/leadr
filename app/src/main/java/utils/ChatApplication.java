package utils;

import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory;
import com.facebook.imagepipeline.core.ImagePipelineConfig;


import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.socket.client.IO;
import io.socket.client.Socket;
import leadr.com.leadr.activity.RegistrationActivity;
import okhttp3.OkHttpClient;
import pref.SharedPreferenceLeadr;


/**
 * Created by Apps Maven on 18/04/2017.
 */

public class ChatApplication extends Application {
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(NetworkingData.CHAT_SERVER_URL);
//            IO.Options
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.setIsDebugEnabled(true);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        DiskCacheConfig diskCacheConfig = DiskCacheConfig.newBuilder(this).build();
        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        ImagePipelineConfig config = OkHttpImagePipelineConfigFactory
                .newBuilder(this, okHttpClient) .setMainDiskCacheConfig(diskCacheConfig)
                .setDiskCacheEnabled(true).setResizeAndRotateEnabledForNetwork(false).
                        setDownsampleEnabled(true)
    .build();
       /* ImagePipelineConfig imagePipelineConfig = OkHttpImagePipelineConfigFactory.newBuilder(this)
                .setMainDiskCacheConfig(diskCacheConfig).setDiskCacheEnabled(true).setResizeAndRotateEnabledForNetwork(false).
                setDownsampleEnabled(true).setNetworkFetcher()
                .build();*/
        Fresco.initialize(this,config);
    }

    @Override
    public void onTrimMemory(final int level) {
        super.onTrimMemory(level);
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) { // Works for Activity
            // Get called every-time when application went to background.
            SimpleDateFormat format = new SimpleDateFormat("hh:mm");
            Date date1 = null;
            Date date2 = null;
            SharedPreferenceLeadr sharedPreferenceLeadr = new SharedPreferenceLeadr(getApplicationContext());
            try {
                date1 = format.parse(sharedPreferenceLeadr.get_APP_START_SESSION());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                date2 = format.parse(CurrentTimeEnd());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long mills = date1.getTime() - date2.getTime();
            int mins = (int) (mills/(1000*60)) % 60;


            Bundle parameters = new Bundle();
            parameters.putString("TIME", mins+"minutes");

            AppEventsLogger logger = AppEventsLogger.newLogger(getApplicationContext());
            logger.logEvent("EVENT_NAME_APP_CLOSED");

        } else if (level == ComponentCallbacks2.TRIM_MEMORY_COMPLETE) { // Works for FragmentActivty
        }
    }


    String CurrentTimeEnd(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
}
