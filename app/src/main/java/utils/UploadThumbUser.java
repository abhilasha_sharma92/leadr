package utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import leadr.com.leadr.activity.RegistrationActivity;

import static utils.GlobalConstant.FILE_NAME1;


public class UploadThumbUser extends AsyncTask<Void, Integer, Void> {
    private OnThumbTaskCompleted listener;
    Context context;
    File ff;
    String s3_server_path = "";
    String AWS_URL = "";
    int percentage;
    String image_width="";
    String image_height = "";

    public UploadThumbUser(File ff, Context context, OnThumbTaskCompleted listener) {
        Log.e("" + getClass(), "UploadAws constructor called");
        this.context = context;
        this.listener = listener;
        this.ff = ff;
    }

    public interface OnThumbTaskCompleted {
        void OnThumbTaskCompleted(String img_path, String aws_url);

        void onError(String img_path, String aws_url);
    }

    protected void onPreExecute() {
        Log.e("S3PutObjectTask", "onPreExecute");
    }


    @Override
    protected Void doInBackground(Void... params) {
        try {
            File pictures = null;
            AmazonS3Client s3Client = null;
            TransferUtility tx = null;


            File imgFile = new File(ff.getPath());
            try {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                Bitmap photoBitMap = null;
                Log.e("Bef Width", String.valueOf(myBitmap.getWidth()));
                if(myBitmap.getWidth()>500)
                {
                    photoBitMap = RegistrationActivity.getResizedBitmap(myBitmap,300);
                }
                else {
                    photoBitMap=myBitmap;
                }
                image_height= String.valueOf(photoBitMap.getHeight());
                image_width = String.valueOf(photoBitMap.getWidth());

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                if(photoBitMap.getWidth()>500)
                {
                    photoBitMap.compress(Bitmap.CompressFormat.JPEG, 30, out);
                }
                else {
                    photoBitMap.compress(Bitmap.CompressFormat.JPEG, 40, out);
                }

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");


                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(out.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("JAS Path", destination.getPath());
                Log.e("JAS getAbsolutePath", destination.getAbsolutePath());
                Log.e("JAS getName", destination.getName());
//                }
                s3_server_path = destination.getPath();//shrdpref.getString(Constants.PIC_PATH,"");
                Log.e("--- s3_sever_path-----=", "" + s3_server_path);
                ClientConfiguration clientConfiguration = new ClientConfiguration();
                clientConfiguration.setSocketTimeout(300000); // 5min
                clientConfiguration.setConnectionTimeout(30000); // 0.5 min
                clientConfiguration.setMaxErrorRetry(3);
////                clientConfiguration.setAc
                clientConfiguration.setMaxConnections(8); // what would be an optimal value here.

                    CognitoCachingCredentialsProvider credentialsProvider =
                            new CognitoCachingCredentialsProvider(
                                    context.getApplicationContext(),
                                    Constants.COGNITO_POOL_ID,
                                    Regions.AP_SOUTHEAST_1);

                pictures = new File(s3_server_path);
                s3Client = new AmazonS3Client(credentialsProvider,clientConfiguration);//new BasicAWSCredentials(Utility.test_AWS_Access_Key, Utility.test_Secret_key), clientConfiguration);

                tx = new TransferUtility(s3Client, context);
                s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));


            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                //'https://<bucketname>.s3.amazonaws.com' + '/'+ bucketname + '/' + filename

                s3Client.setEndpoint("s3.amazonaws.com");//s3.amazonaws.com");

//                flength = pictures.length();
                final String PYT_bucket = Constants.BUCKET_NAME;

                if (s3Client.doesBucketExist(PYT_bucket)) {
                    Log.e("Warn", "service called bucket exist");
                } else {
                    s3Client.createBucket(PYT_bucket);
                    Log.e("FOS signature", "Bucket created");
                }

                Log.e("UplAs sel_url called: ", "" + FILE_NAME1);
                String selfie_url = "";
                selfie_url = image_width+"x"+image_height  + System.currentTimeMillis() + ".jpg";
                String S3SERVER = "https://s3-ap-southeast-1.amazonaws.com/beleadr/";
                AWS_URL = S3SERVER + selfie_url;

                final TransferObserver observer = tx.upload(PYT_bucket, selfie_url, pictures);

                observer.setTransferListener(new TransferListener() {
                    int Xtotal = 0;

                    @Override
                    public void onStateChanged(int arg0, TransferState state) {
                        Log.e("onStateChanged", "on state changed " + state);
                        if(state.toString().equals("COMPLETED")){
//                            Toast.makeText(context,"inside",Toast.LENGTH_LONG).show();
                            listener.OnThumbTaskCompleted(s3_server_path, AWS_URL);
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        Log.e("onProgressChanged", "total bytes " + observer.getBytesTotal());
                        Log.e("onProgressChanged", "total bytes transfered " + observer.getBytesTransferred());
                        try{
                            percentage = (int) (bytesCurrent / bytesTotal * 100);
                        }catch (Exception e){}

                        Log.e("onProgressChanged", "total percentage " + percentage);

                        Xtotal += (int) observer.getBytesTransferred();

                        try {
                            publishProgress(percentage);//(int) ((Xtotal * 100) / flength));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(int arg0, Exception arg1) {
                        Log.e("onError=" + arg0, "on Error  " + arg1.toString());
                        percentage = 101;
                        listener.onError(s3_server_path, AWS_URL);
                        Toast.makeText(context, "" + arg1.toString(), Toast.LENGTH_SHORT).show();
                    }
                });


                    do {

                    } while (percentage < 100);

                Log.e("percentage", "percentage  " + percentage);


                if (percentage == 100) {
                    s3Client.setObjectAcl(PYT_bucket, FILE_NAME1, CannedAccessControlList.PublicRead);


//                    listener.OnThumbTaskCompleted(s3_server_path, AWS_URL);
                }
            } catch (Exception e) {
                e.printStackTrace();
                listener.onError(s3_server_path, AWS_URL);
            }


//            }

        } catch (Exception exception) {
            exception.printStackTrace();
            listener.onError(s3_server_path, AWS_URL);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void o) {
        super.onPostExecute(o);
        Log.v("==.onPostExecute()= ", "onPostExecute");

    }

    @Override
    protected void onProgressUpdate(Integer... progres) {
        super.onProgressUpdate(progres);
        Log.d("oProgsUpdate pgres[0]", "" + progres[0]);

    }




}
