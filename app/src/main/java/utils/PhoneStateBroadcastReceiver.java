package utils;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import leadr.com.leadr.R;
import leadr.com.leadr.activity.CallLogActivity;
import leadr.com.leadr.activity.SellActivity;
import leadr.com.leadr.activity.SettingActivity;
import pref.SharedPreferenceLeadr;

import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE;
import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by admin on 08-Feb-18.
 */

public class PhoneStateBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "";
    Context mContext;
    String incoming_number;
    private int prev_state;
    private Intent int_;

    private WindowManager wm;
    private static LinearLayout ly1;
    private WindowManager.LayoutParams params1;
    static CustomPhoneStateListener phoneStateListener;
    Boolean iscallreceived=false;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "------> On Receive ");
        int_ = intent;

        if(phoneStateListener==null) {
            TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE); //TelephonyManager object
            phoneStateListener = new CustomPhoneStateListener();
            telephony.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE); //Register our listener with TelephonyManager

            Bundle bundle = intent.getExtras();
            String phoneNr = bundle.getString("incoming_number");
            Log.i(TAG, "---> phoneNr : " + phoneNr);
            mContext = context;
        }
    }

    /* Custom PhoneStateListener */
    public class CustomPhoneStateListener extends PhoneStateListener {

        private static final String TAG = "CustomPhoneStateListener";

        int num_ = 0;

        @Override
        public void onCallStateChanged(int state, String incomingNumber){

            if( incomingNumber != null && incomingNumber.length() > 0 )
                incoming_number = incomingNumber;

            switch(state){
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.i("", "---> CALL_STATE_RINGING");
                    prev_state=state;
                    break;

                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.i("", "---> CALL_STATE_OFFHOOK");
//                    iscallreceived=true;
                    prev_state=state;
                    break;

                case TelephonyManager.CALL_STATE_IDLE:

                    Log.i("", "---> CALL_STATE_IDLE : "+incoming_number);

                    if((prev_state == TelephonyManager.CALL_STATE_OFFHOOK))
                    {
                        prev_state=state;

                        SharedPreferenceLeadr sharedPreferenceLeadr = new SharedPreferenceLeadr(mContext);
                        if(sharedPreferenceLeadr.get_WIDGET().equalsIgnoreCase("1")){
                            if(!iscallreceived){
                                iscallreceived   = true;
                                GlobalConstant utils = new GlobalConstant();
                                if(utils.checkReadContactPermission(mContext) && utils.checkReadCallLogPermission(mContext)  ) {
                                    Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(incoming_number));

                                    String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

                                    Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);

                                    String contactName = mContext.getResources().getString(R.string.unknw);

                                    if (cursor != null) {
                                        if (cursor.moveToFirst()) {
                                            contactName = cursor.getString(0);
                                        }
                                        cursor.close();
                                    }
                                    Log.e("", "---> name : " + contactName);

                                    if (contactName.equalsIgnoreCase(mContext.getResources().getString(R.string.unknw))) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext.getApplicationContext());
                                        LayoutInflater inflater = LayoutInflater.from(mContext);
                                        View dialogView = inflater.inflate(R.layout.activity_widget, null);


                                        builder.setView(dialogView);
                                        final AlertDialog alert = builder.create();
                                        alert.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                            alert.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY );
                                        }else {
                                            alert.getWindow().setType(LayoutParams.TYPE_PHONE);
                                        }
                                        alert.setCancelable(true);
                                        try{
                                            alert.show();
                                        }catch (Exception e){
                                        }
                                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                        Window window = alert.getWindow();
//                        window.addFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
//                                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                                        window.setGravity(Gravity.CENTER_VERTICAL);
                                        lp.copyFrom(window.getAttributes());
                                        //This makes the dialog take up the full width
                                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                        window.setAttributes(lp);

                                        Button btn_no = (Button) alert.findViewById(R.id.btn_no);
                                        Button btn_yes = (Button) alert.findViewById(R.id.btn_yes);
                                        ImageView img_setting = (ImageView) alert.findViewById(R.id.img_setting);

                                        btn_no.setOnClickListener(new OnClickListener() {
                                            @Override
                                            public void onClick( View v ) {
                                                iscallreceived=false;
                                                alert.dismiss();
                                                alert.cancel();
                                            }
                                        });

                                        btn_yes.setOnClickListener(new OnClickListener() {
                                            @Override
                                            public void onClick( View v ) {
                                                iscallreceived=false;
                                                alert.dismiss();
                                                alert.cancel();
                                                Intent intent = new Intent(mContext, SellActivity.class);
                                                intent.putExtra("widget",incoming_number);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                mContext.startActivity(intent);
                                            }
                                        });

                                        img_setting.setOnClickListener(new OnClickListener() {
                                            @Override
                                            public void onClick( View v ) {
                                                iscallreceived=false;
                                                alert.dismiss();
                                                alert.cancel();
                                                Intent intent = new Intent(mContext, SettingActivity.class);
                                                intent.putExtra("widget",incoming_number);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                mContext.startActivity(intent);
                                            }
                                        });

                                        alert.setOnDismissListener(new OnDismissListener() {
                                            @Override
                                            public void onDismiss( DialogInterface dialog ) {
                                                iscallreceived=false;
                                            }
                                        });
                                    }

                                }
                            }
                        }

                        Log.i("", "----> Call Answered ");
                        break;
                    }
                    if((prev_state == TelephonyManager.CALL_STATE_RINGING))
                    {
                        prev_state=state;
                        Log.i("", "----> Rejected or Missed call ");
                        break;
                    }
                    break;
            }
        }
    }

    private boolean isAppInForeground(Context context)
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            ActivityManager am = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
            ActivityManager.RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);
            String foregroundTaskPackageName = foregroundTaskInfo.topActivity.getPackageName();

            return foregroundTaskPackageName.toLowerCase().equals(context.getPackageName().toLowerCase());
        }
        else
        {
            ActivityManager.RunningAppProcessInfo appProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(appProcessInfo);
            if (appProcessInfo.importance == IMPORTANCE_FOREGROUND || appProcessInfo.importance == IMPORTANCE_VISIBLE)
            {
                return true;
            }

            KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            // App is foreground, but screen is locked, so show notification
            return km.inKeyguardRestrictedInputMode();
        }
    }


}
