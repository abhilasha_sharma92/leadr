package utils;


public class NetworkingData {

//    /*BASE URL*/
//        public static String BASE_URL = "http://18.216.137.132/";

    //Client server
//    public static String BASE_URL = "http://13.250.167.1/";
    //new instance test server
    //to be used by client
    public static String BASE_URL = "http://52.76.111.65/";

    /*Live by partner*/
//    public static String BASE_URL = "http://13.250.167.1/";


    /*Live by partner*/
    public static String GET_RATE = "http://data.fixer.io/api/latest?access_key=1fc3136a2d6e8fddafdbe72954b954bf&format=1";
//
    /*REGISTRATION*/
    public static String REG = "register";

    /*Get Category*/
    public static String GET_CATEGORY = "get_category";

    /*Login*/
    public static String LOGIN = "login";

    /*SELL*/
    public static String SELL = "create_lead";

    /*BUY*/
//    public static String BUY = "search_lead";

    /*BUY*/
    public static String BUY = "search_lead_pagination";

    /*BUY*/
    public static String BUY_LOC = "search_lead_pagination_loc";
    /*BUY*/

    public static String BUY_LOC1 = "search_lead_pagination_loc_test1";

    /*SWIPE*/
    public static String SWIPE = "lead_swipe";

    /*Bought*/
    public static String BOUGHT = "bought";

    /*Bought*/
    public static String BOUGHT_PAGINATION = "bought_new";

    /*BUY*/
    public static String BUY_LEAD = "buy_lead";


    /*BUY*/
    public static String UPDATE_PRO = "update_profile";


    /*updated PRofile  api*/
    public static String UPDATE_PRO_VER2 = "update_profile_new";


    /*Del ACC*/
    public static String DEL_ACC = "deactivate_account";


    /*get user profile*/
    public static String GET_PROFILE = "get_profile";


    /*save token*/
    public static String SAVE_TOKEN = "save_customer";


    /*chk card*/
    public static String CHECK_CARD = "check_stripe_id";


    /*updtae lead*/
    public static String UPDATE_LEAD = "update_lead";


    /*add analytic to view*/
    public static String ANALYTIC_LEAD = "view_users";


    /*my leads*/
    public static String MYSELLS = "my_leads";


    /*my leads*/
    public static String MYSELLS_PAGIN = "my_leads_new";


    /*del my leads*/
    public static String DEL_LEAD = "delete_lead";


    /*refund bought*/
    public static String REFUND = "refund";


    /*feedback send*/
    public static String FEEDBACK = "feedback";


    /*check buy status befor buying*/
    public static String CHECK_BUY_STATUS = "check_buy_status";


    /*check buy status befor buying*/
    public static String GET_LIST_OFCHAT = "user_message_list";


    /*check buy status befor buying*/
    public static String VIEW_PAYMENT = "view_payments";


    public static final String CHAT_SERVER_URL = "http://52.76.111.65";


    public static final String CHAT_GET_LIST = "user_message";


    public static final String FRAG_INBOX = "user_message_list";


    public static final String CHAT_NOTI_ = "update_chat_noti";


    public static final String CHAT_SOUND_ = "update_chat_sound";


    public static final String TICKET_NOTIFY = "update_new_ticket_noti";


    public static final String TICKET_SOUND = "update_new_ticket_sound";


    public static final String SELL_NOTIFY = "update_new_sell_noti";



    public static final String SELL_SOUND = "update_new_sell_sound";



    public static final String UPDATE_NEWLEAD_NOTIFY = "update_new_lead_noti";



    public static final String UPDATE_NEWLEAD_SOUND = "update_new_lead_sound";




    public static final String UPDATE_WIDGET = "update_widget";



    public static final String UPDATE_DIMENSION = "update_dimension_sys";



    public static final String ARCHIVE = "archive";



    public static final String HISTORY = "history";



    public static final String WITHDRAW = "withdrawal";



    public static final String GET = "get_feedback_types";



    public static final String CHECK_REFUND_STATUS = "check_refund_status";



    public static final String CHECK_LEAD_INHOUR = "numberof_leads";



    public static final String GET_ADMIN_SETTING = "admin_settings";



    public static final String LEAD_EDIT_MODE = "lead_edit_mode";



    public static final String CHECK_FIRST_TYM_REFUND = "buyer_check_time";



    public static final String CHECK_SECOND_TYM_REFUND = "ticket_check_time";



    public static final String CHECK_SELLER_REFUND_STATUS = "seller_check_time";



    public static final String APPROVE_BY_SELLER = "refund_approved_seller";



    public static final String DEL_CARDINFO = "delete_customer";



    public static final String INBOX_MULTIPLE_COUNT = "user_message_count";



    public static final String INBOX_TOTAL_COUNT = "inbox_count";



    public static final String INBOX_TOTAL_COUNT_VER2 = "inbox_count_test";



    public static final String SAVE_FILTER = "save_filters";



    public static final String LEAD_DETAIL = "lead_detail";



    public static final String MSG_DETAIL_NOTI = "user_message_list_noti";



    public static final String BASEURL_LOCATION = "https://maps.googleapis.com/maps/api/place/details/json?placeid=";

}
