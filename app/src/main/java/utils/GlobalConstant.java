package utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormatSymbols;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import leadr.com.leadr.R;

/**
 * Created by Abhilasha on 29/12/2016.
 */

public class GlobalConstant {

    static ProgressDialog progressDialog ;
    public static String PROFILE_PIC = "";
    public static String USERNAME = "";
    private static String strSeparator = ",";
    public static String FILE_NAME1 = "";
    public static String FILE_NAME2 = "";
    public static String FILE_NAME3 = "";
    public static String FILE_NAME4 = "";
    public static String FILE_NAME5 = "";
    public static String CAT_ID = "0";
    public static String PAGINATION_IDS = "0";
    public static String PAGINATION_IDS_BOUGHT = "0";
    public static String PAGINATION_IDS_SELL = "0";
    public static String CURRENCY = "4";


    public String regularExpresionOfEmailId = "^(?!.{51})([A-Za-z0-9])+([A-Za-z0-9._-])+@([A-Za-z0-9._-])+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public Pattern patternForEmailId = Pattern
            .compile(regularExpresionOfEmailId);


    public   void showProgressDialog(Context con, String message){
        try{
            if(con!=null) {
                progressDialog = ProgressDialog.show(con, "", message);
                progressDialog.setCancelable(false);
            }
        }catch (Exception e){}
    }

    public   void showProgressDialog_Cancelable(Context con, String message){
        try{
            if(con!=null) {
                if(!progressDialog.isShowing()) {
                    progressDialog = ProgressDialog.show(con, "", message);
                    progressDialog.setCancelable(true);
                }
            }
        }catch (Exception e){}
    }

    public  void  dismissProgressdialog() {
        try {
            if(progressDialog!=null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }catch (Exception e){
            Log.e("exc: ",e.toString());
        }
    }


    public  void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public  boolean isNetworkAvailable(Context ctx) {
        try{
            ConnectivityManager connectivityManager = (ConnectivityManager)ctx. getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }catch (Exception e){
            Log.e("exc: ",e.toString());
            return true;
        }

    }

    public  void dialog_msg_show(Activity activity, String msg){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        dialog.setCanceledOnTouchOutside(true);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(OpenSans_Regular(activity));
        txt_ok.setTypeface(OpenSans_Regular(activity));
        txt_msg.setTypeface(OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        try{
            dialog.show();
        }catch (Exception e){

        }


    }

    public  void dialog_msg_show_WithTitle(Activity activity, String msg, String title){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);
        txt_title.setText(title);

        assert txt_title != null;
        txt_title.setTypeface(OpenSans_Regular(activity));
        txt_ok.setTypeface(OpenSans_Regular(activity));
        txt_msg.setTypeface(OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        try{
            dialog.show();
        }catch (Exception e){

        }


    }

    public  void dialog_msg_show_WithTitle_Payment(Activity activity, String msg, String title){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);
        txt_title.setText(title);

        assert txt_title != null;
        txt_title.setTypeface(OpenSans_Regular(activity));
        txt_ok.setTypeface(OpenSans_Regular(activity));
        txt_msg.setTypeface(OpenSans_Bold(activity));

        txt_msg.setGravity(Gravity.LEFT);
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        try{
            dialog.show();
        }catch (Exception e){

        }


    }



    public  void dialog_msg_show_WithTitle_Desc(Activity activity, String msg, String title){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog.findViewById(R.id.txt_ok);

        assert txt_msg != null;
        txt_msg.setText(msg);

        txt_msg.setMovementMethod(new ScrollingMovementMethod());

        assert txt_title != null;
        txt_title.setTypeface(OpenSans_Regular(activity));
        txt_title.setText(title);
        txt_ok.setTypeface(OpenSans_Regular(activity));
        txt_msg.setTypeface(OpenSans_Regular(activity));

        txt_msg.setGravity(Gravity.LEFT);
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        try{
            dialog.show();
        }catch (Exception e){
        }
    }



    public  void dialog_msg_show_WithTitle_Desc_NoCancel(Activity activity, String msg, String title){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_long_txt_popup, null);
        dialog.setContentView(bottomSheetView);
        dialog.setCancelable(false);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                /*final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState > BottomSheetBehavior.STATE_DRAGGING)
                            bottomSheet.post(new Runnable() {
                                @Override public void run() {
                                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                                }
                            });
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });*/
                behavior.setPeekHeight(bottomSheet.getHeight());
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });


        TextView txt_msg   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog.findViewById(R.id.txt_ok);

        assert txt_msg != null;
        txt_msg.setText(msg);

        txt_msg.setMovementMethod(new ScrollingMovementMethod());

        assert txt_title != null;
        txt_title.setTypeface(OpenSans_Regular(activity));
        txt_title.setText(title);
        txt_ok.setTypeface(OpenSans_Regular(activity));
        txt_msg.setTypeface(OpenSans_Regular(activity));

        txt_msg.setGravity(Gravity.LEFT);
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        try{
            dialog.show();
        }catch (Exception e){
        }
    }






    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
//        context.finish();
    }

    public boolean checkReadExternalPermission(Context ctx) {
        String permission = "android.permission.READ_EXTERNAL_STORAGE";
        int res = ctx.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public boolean checkMicrophonePermission(Context ctx) {
        String permission = "android.permission.RECORD_AUDIO";
        int res = ctx.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public boolean checkWriteExternalPermission(Context ctx) {
        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = ctx.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    public boolean checkReadContactPermission(Context ctx) {
        String permission = "android.permission.READ_CONTACTS";
        int res = ctx.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    public boolean checkReadCallLogPermission(Context ctx) {
        String permission = "android.permission.READ_CALL_LOG";
        int res = ctx.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    public boolean checkReadCallPhonePermission(Context ctx) {
        String permission = "android.permission.CALL_PHONE";
        int res = ctx.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    public boolean checkPhonePermission(Context ctx) {
        String permission = "android.permission.CALL_PHONE";
        int res = ctx.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    public boolean checkPhoneStatePermission(Context ctx) {
        String permission = "android.permission.READ_PHONE_STATE";
        int res = ctx.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public Typeface OpenSans_Regular(Activity act){
        Typeface custom_font = Typeface.createFromAsset(act.getAssets(),  "font/open_san_reg.ttf");
        return custom_font;
    }

    public Typeface OpenSans_Bold(Activity act){
        Typeface custom_font = Typeface.createFromAsset(act.getAssets(),  "font/open_sans_bold.ttf");
        return custom_font;
    }

    public Typeface OpenSans_Light(Activity act){
        Typeface custom_font = Typeface.createFromAsset(act.getAssets(),  "font/open_sans_light.ttf");
        return custom_font;
    }

    public Typeface Hebrew_Regular(Activity act){
        Typeface custom_font = Typeface.createFromAsset(act.getAssets(),  "font/hebrew.ttf");
        return custom_font;
    }

    public Typeface Dina(Activity act){
        Typeface custom_font = Typeface.createFromAsset(act.getAssets(),  "font/dina.ttf");
        return custom_font;
    }

    public static void setListViewHeightBasedOnItems(ListView target_Listview, int limit) // LIMIT 0 FOR SHOWING ALLL CONTENTS
    {
        if (limit == 0) {
            ListAdapter listAdapter = target_Listview.getAdapter();
            if (listAdapter != null) {
                int numberOfItems = listAdapter.getCount();
                int totalItemsHeight = 0;
                for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                    View item = listAdapter.getView(itemPos, null, target_Listview);
                    item.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                    totalItemsHeight += item.getMeasuredHeight();
                }

                int totalDividersHeight = target_Listview.getDividerHeight() * (numberOfItems - 1);

                ViewGroup.LayoutParams params = target_Listview.getLayoutParams();
                params.height = totalItemsHeight + totalDividersHeight;
                target_Listview.setLayoutParams(params);
                target_Listview.requestLayout();
            } else {

            }
        } else {
            ListAdapter listAdapter = target_Listview.getAdapter();
            if (listAdapter != null) {
                int numberOfItems = listAdapter.getCount();
                int totalItemsHeight = 0;
                for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                    if (itemPos < limit) {
                        View item = listAdapter.getView(itemPos, null, target_Listview);
                        item.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                        totalItemsHeight += item.getMeasuredHeight();
                    }
                }
                int totalDividersHeight = target_Listview.getDividerHeight() * (numberOfItems - 1);
                ViewGroup.LayoutParams params = target_Listview.getLayoutParams();
                params.height = totalItemsHeight + totalDividersHeight;
                target_Listview.setLayoutParams(params);
                target_Listview.requestLayout();
            }
        }
    }

    public static String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }


    public static String getDateSuffix( int day) {
        switch (day) {
            case 1: case 21: case 31:
                return ("st");

            case 2: case 22:
                return ("nd");

            case 3: case 23:
                return ("rd");

            default:
                return ("th");
        }
    }

    public static  String ConvertToBase64(String text){
        byte[] data = new byte[ 0 ];
        data = text.getBytes(StandardCharsets.UTF_8);
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return  base64;
    }

    /*@NonNull
    public static Boolean checkForEncode( String string) {
        String pattern = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
        Pattern r = Pattern.compile(pattern);
        string = string.replaceAll(" ","");
        Matcher m = r.matcher(string);
        if (m.find()) {
            return true;
        } else {
            return  false;
        }
    }*/

    public int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double)progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }



    public static String encrypt( String value) {
        String value2 = value;
        try {
            String test = Base64.encodeToString(value.getBytes(), Base64.DEFAULT);
            return test;
        }
        catch (IllegalArgumentException e) {
            // TODO: handle exception
            return  value;
        }
        catch (Exception e) {
            String valueee = value;
            return  value2;
        }
    }

}
