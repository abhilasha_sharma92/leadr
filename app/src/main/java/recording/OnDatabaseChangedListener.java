package recording;

/**
 * Created by Umesh on 14/12/2017.
 */

public interface OnDatabaseChangedListener {

    void onNewDatabaseEntryAdded();
    void onDatabaseEntryRenamed();
}
