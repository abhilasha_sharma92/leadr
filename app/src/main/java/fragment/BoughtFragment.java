package fragment;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.appevents.AppEventsLogger;
import com.squareup.picasso.Picasso;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import leadr.com.leadr.R;
import leadr.com.leadr.activity.ChatActivity;
import leadr.com.leadr.activity.FeedbackActivity;
import leadr.com.leadr.activity.MainActivity;
import leadr.com.leadr.activity.ProfileOther_Activity;
import leadr.com.leadr.activity.SellActivity;
import modal.BuyPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;

import static leadr.com.leadr.activity.MainActivity.fragment_bought;
import static leadr.com.leadr.activity.MainActivity.fragment_buy;
import static leadr.com.leadr.activity.MainActivity.fragment_inbox;
import static leadr.com.leadr.activity.MainActivity.fragment_more;
import static leadr.com.leadr.activity.MainActivity.fragment_sell;
import static leadr.com.leadr.activity.MainActivity.vw_bought;
import static leadr.com.leadr.activity.MainActivity.vw_buy;
import static leadr.com.leadr.activity.MainActivity.vw_inbox;
import static leadr.com.leadr.activity.MainActivity.vw_more;
import static leadr.com.leadr.activity.MainActivity.vw_sell;


/**
 * Created by Abhilasha on 5/12/2017.
 */

public class BoughtFragment extends Fragment {

    Unbinder      unbinder;

    CardStackView cardStack;
    TextView      txt_buy_top;
    TextView      txt_history;

    @BindView(R.id.img_filter)
    ImageView      img_filter;

    @BindView(R.id.progres_load)
    ProgressBar    progres_load;

    @BindView(R.id.lnr_selectr)
    RelativeLayout   lnr_selectr;

    @BindView(R.id.img_sel)
    ImageView      img_sel;

    GlobalConstant utils;

    ArrayList<BuyPojo> arr_buy = new ArrayList<>();

    BuyAdapter            adapter;
    RefundAdapter         adapter_refund;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    String phone_no = "",type_to_send ;

    private Handler mHandler = new Handler();

    private MediaPlayer mMediaPlayer = null;
    private boolean     isPlaying = false;

    private static CountDownTimer countDownTimer;

    int count_start_pause     = 30;
    int count_start_pause_add = 0;
    int resume_pos            = 0;
    int time_pending          = 0;
    int time_pending2         = 0;
    Handler mHandler_seekbar;
    BottomSheetDialog dialog_card;
    String hours_added = "0";
    String days_added = "0";

    Handler handler_timer = new Handler();


    public BoughtFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_buy, container, false);

        // bind view using butter knife
        unbinder              = ButterKnife.bind(this, view);
        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(getActivity());

        type_to_send = sharedPreferenceLeadr.get_SELECTOR_BOUGHT();

        cardStack    = (CardStackView)view. findViewById(R.id.swipe_deck);
        txt_buy_top  = (TextView) view. findViewById(R.id.txt_buy_top);
        txt_history  = (TextView) view. findViewById(R.id.txt_history);

        txt_history.setVisibility(View.GONE);
        img_sel.setVisibility(View.VISIBLE);
        img_filter.setImageResource(R.drawable.reload);
        img_filter.setVisibility(View.GONE);

        txt_buy_top.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_history.setTypeface(utils.OpenSans_Regular(getActivity()));
        cardStack.setSwipeThreshold(0.1f);

        txt_buy_top.setText(getResources().getString(R.string.type_one));

        cardStack.setCardEventListener(new CardStackView.CardEventListener() {
            @Override
            public void onCardDragging(float percentX, float percentY) {
            }
            @Override
            public void onCardSwiped(SwipeDirection direction, int topIndex) {
                    if (mMediaPlayer!=null){
                        if(mMediaPlayer.isPlaying()){
                            mMediaPlayer.stop();
                            mMediaPlayer.release();
                        }
                        mMediaPlayer = null;
                    }
                    if(countDownTimer!=null){
                        countDownTimer.cancel();
                        countDownTimer=null;
                    }
                     count_start_pause=30;
                     count_start_pause_add=0;
                     resume_pos=0;

                try {
                    arr_buy.remove(topIndex - 1);
                    if(type_to_send.equalsIgnoreCase("1")){
                        adapter_refund.notifyDataSetChanged();
                    }else {
                        adapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                }

                     if(topIndex==arr_buy.size()+1){
                         api_Bought();
                     }
                if(img_filter!=null) {
                         if(arr_buy.size()>0) {
                             img_filter.setVisibility(View.VISIBLE);
                         }
                      }

                try{
                    handler_timer.removeCallbacks(runnable_timer);
                }catch (Exception e){}
                try{
                    handler_timer.postDelayed(runnable_timer, 40000);
                }catch (Exception e){}
            }

            @Override
            public void onCardReversed() {
            }

            @Override
            public void onCardMovedToOrigin() {
            }

            @Override
            public void onCardClicked(int index) {
            }
        });





        if(getActivity()!=null) {
            if (utils.isNetworkAvailable(getActivity())) {
                api_Bought();
            } else {
                utils.dialog_msg_show(getActivity(), getResources().getString(R.string.no_internet));
            }
        }

        return view;
    }


    Runnable runnable_timer = new Runnable(){
        @Override
        public void run() {
            if(type_to_send.equalsIgnoreCase("1")){
                if(adapter_refund!=null){
                    adapter_refund.notifyDataSetChanged();
                }
            }else {
                if(adapter!=null){
                    adapter.notifyDataSetChanged();
                }
            }

            try{
                handler_timer.postDelayed(runnable_timer, 40000);
            }catch (Exception e){}
        }
    };



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        try{
            handler_timer.removeCallbacks(runnable_timer);
        }catch (Exception e){}
    }


    @Override
    public void onDetach() {
        super.onDetach();
        try{
            handler_timer.removeCallbacks(runnable_timer);
        }catch (Exception e){}
    }


    @OnClick(R.id.img_filter)
    public void onBack() {
        if(getActivity()!=null) {
            if (utils.isNetworkAvailable(getActivity())) {
                try{
                    handler_timer.removeCallbacks(runnable_timer);
                }catch (Exception e){}

                api_Bought();
            } else {
                utils.dialog_msg_show(getActivity(), getResources().getString(R.string.no_internet));
            }
        }
    }



    @OnClick(R.id.lnr_selectr)
    public void onSelSelector() {
       showDialogSelector();
    }


    private void showDialogSelector() {
        final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_selectr_bought, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_nw_lead = (TextView) dialog.findViewById(R.id.txt_nw_lead);
        TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);
        TextView txt_archive = (TextView) dialog.findViewById(R.id.txt_archive);

        txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_nw_lead.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_refund.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_archive.setTypeface(utils.OpenSans_Regular(getActivity()));

        if(type_to_send.equalsIgnoreCase("0")){
            txt_nw_lead.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_lang, 0);
            txt_refund.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt_archive.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        else if(type_to_send.equalsIgnoreCase("1")){
            txt_refund.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_lang, 0);
            txt_nw_lead.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt_archive.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        else if(type_to_send.equalsIgnoreCase("2")){
            txt_archive.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_lang, 0);
            txt_nw_lead.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt_refund.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        txt_nw_lead.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                type_to_send = "0";
                dialog.dismiss();
                txt_buy_top.setText(getResources().getString(R.string.type_one));
                api_Bought();
            }
        });

        txt_refund.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                type_to_send = "1";
                dialog.dismiss();
                txt_buy_top.setText(getResources().getString(R.string.type_two));
                api_Bought();
            }
        });

        txt_archive.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                type_to_send = "2";
                dialog.dismiss();
                txt_buy_top.setText(getResources().getString(R.string.type_three));
                api_Bought();
            }
        });
        dialog.show();
    }


    @Override
    public void onResume() {
        super.onResume();

        type_to_send = sharedPreferenceLeadr.get_SELECTOR_BOUGHT();

        if (txt_buy_top != null){
            if (type_to_send.equalsIgnoreCase("0")) {
                txt_buy_top.setText(getResources().getString(R.string.type_one));
            } else if (type_to_send.equalsIgnoreCase("1")) {
                txt_buy_top.setText(getResources().getString(R.string.type_two));
            } else {
                txt_buy_top.setText(getResources().getString(R.string.type_three));
            }
    }


        if(getActivity()!=null){
            if(SellActivity.save_changes_bought!=null) {
                SellActivity.save_changes_bought = null;
                api_Bought();
            }else{
                try{
                    handler_timer.postDelayed(runnable_timer, 40000);
                }catch (Exception e){}
            }
        }else{
            try{
                handler_timer.postDelayed(runnable_timer, 40000);
            }catch (Exception e){}
        }
        if(img_filter!=null) {
            img_filter.setVisibility(View.GONE);
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        try{
            handler_timer.removeCallbacks(runnable_timer);
        }catch (Exception e){}
    }


    @Override
    public void onStart() {
        super.onStart();
    }


    private void api_Bought(){
        if(progres_load!=null){
            progres_load.setVisibility(View.VISIBLE);
        }

        sharedPreferenceLeadr.set_SELECTOR_BOUGHT(type_to_send);

        AndroidNetworking.enableLogging();
        Log.e("url_bought: ", NetworkingData.BASE_URL + NetworkingData.BOUGHT);
        Log.e("userid: ", sharedPreferenceLeadr.getUserId());
        Log.e("type: ", type_to_send);
        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.BOUGHT)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("type",type_to_send)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.dismissProgressdialog();
                        arr_buy.clear();
                        if(progres_load!=null){
                            progres_load.setVisibility(View.GONE);
                        }

                        Log.e("res_bought: ",response+"");
                        if (response.optString("status").equals("1")) {
                            try {
                                JSONArray arr = response.getJSONArray("detail");
                                for(int i=0; i<arr.length(); i++){
                                    JSONObject obj = arr.getJSONObject(i);
                                    BuyPojo item = new BuyPojo();
                                    item.setAudio(obj.getString("audio"));
                                    item.setBudget(obj.getString("budget"));
                                    item.setBusiness_fb_page(obj.optString("business_fb_page"));
                                    item.setBusiness_image(obj.optString("business_image"));
                                    item.setBusiness_info(obj.optString("business_info"));
                                    item.setBusiness_name(obj.optString("business_name"));
                                    item.setCategory(obj.optString("category"));
                                    item.setCell_number(obj.optString("cell_number"));
                                    String client_name = obj.optString("client_name");
                                    if(client_name!=null){
                                        if(client_name.equalsIgnoreCase(" ")){
                                            client_name = getResources().getString(R.string.unknw);
                                        }else if(client_name.equalsIgnoreCase("")){
                                            client_name = getResources().getString(R.string.unknw);
                                        }
                                    }else{
                                        client_name = getResources().getString(R.string.unknw);
                                    }
                                    item.setClient_name(client_name);
                                    item.setDescription(obj.optString("description"));
                                    item.setId(obj.optString("id"));
                                    item.setJob_title(obj.optString("job_title"));
                                    //if it 0 then it means lead is in active
                                    item.setIs_delete(obj.optString("is_delete"));
                                    item.setLead_id(obj.optString("lead_id"));
                                    item.setLead_price(obj.optString("lead_price"));
                                    item.setPhone_number(obj.optString("phone_number"));
                                    item.setType(obj.optString("type"));
                                    item.setUser_id(obj.optString("user_id"));
                                    item.setUser_image(obj.optString("user_image"));
                                    String usr_nm = obj.optString("user_name");
                                    if(usr_nm.equals("user deleted account")){
                                        item.setUser_name(getResources().getString(R.string.usr_del));
                                        item.setBusiness_name(getResources().getString(R.string.usr_del));
                                    }else {
                                        item.setUser_name(obj.optString("user_name"));
                                    }
                                    item.setLat(obj.optString("lat"));
                                    item.setLon(obj.optString("lon"));
                                    item.setLocation_name(obj.optString("location_name"));
                                    item.setCreated_time(obj.optString("created_time"));
                                    //"buy_time":"07-26-53","buy_date":"2018-03-28"
                                    item.setBuy_time(obj.optString("buy_time"));
                                    item.setTime(obj.optString("time"));
                                    item.setCountry(obj.optString("country"));
                                    item.setCondition(false);
                                    item.setAddress(obj.optString("address"));
                                    item.setUser_thum(obj.optString("user_thum"));
                                    item.setRefund_req(obj.optString("refund_req"));
                                    item.setRefund_approved(obj.optString("refund_approved"));
                                    item.setReject_refund_req(obj.optString("reject_refund_req"));
                                    item.setRefund_req_for(obj.optString("refund_req_for"));
                                    item.setRefund_time(obj.optString("refund_time"));
                                    arr_buy.add(item);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            BuyPojo item = new BuyPojo();
                            item.setAudio("");
                            item.setBudget("");
                            item.setBusiness_fb_page("");
                            item.setBusiness_image("");
                            item.setBusiness_info("");
                            item.setBusiness_name("");
                            item.setCategory("");
                            item.setCell_number("");
                            item.setClient_name("");
                            item.setDescription("");
                            item.setId("");
                            item.setJob_title("");
                            //if it 0 then it means lead is in active
                            item.setIs_delete("");
                            item.setLead_id("");
                            item.setLead_price("");
                            item.setPhone_number("");
                            item.setType("");
                            item.setUser_id("");
                            item.setUser_image("");
                            item.setUser_name("");
                            item.setLat("");
                            item.setLon("");
                            item.setLocation_name("");
                            item.setCreated_time("");
                            item.setTime("");
                            item.setCountry("");
                            item.setCondition(false);
                            item.setAddress("");
                            item.setUser_thum("");
                            item.setRefund_req("");
                            item.setRefund_approved("");
                            item.setReject_refund_req("");
                            item.setBuy_time("");
                            item.setRefund_req_for("");
                            item.setRefund_time("");
                            arr_buy.add(item);

                        }
                        /*if(img_filter!=null) {
                            if (arr_buy != null) {
                                if (arr_buy.size() > 1) {
                                    img_filter.setVisibility(View.VISIBLE);
                                } else {
                                    img_filter.setVisibility(View.GONE);
                                }
                            } else {
                                img_filter.setVisibility(View.GONE);
                            }
                        }*/
                        if(img_filter!=null) {
                            img_filter.setVisibility(View.GONE);
                        }
                        hours_added = response.optString("hours");
                        days_added = response.optString("days");
                        if(getContext()!=null) {
                            if (cardStack != null) {
                                if(type_to_send.equalsIgnoreCase("0")) {
                                    adapter = new BuyAdapter(arr_buy, getContext());
                                    cardStack.setAdapter(adapter);
                                }else if(type_to_send.equalsIgnoreCase("1")){
                                    adapter_refund = new RefundAdapter(arr_buy, getContext());
                                    cardStack.setAdapter(adapter_refund);
                                }else if(type_to_send.equalsIgnoreCase("2")){
                                    adapter = new BuyAdapter(arr_buy, getContext());
                                    cardStack.setAdapter(adapter);
                                }
                            }
                        }
                        try{
                            handler_timer.postDelayed(runnable_timer,40000);
                        }catch (Exception e){}


                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                        if(progres_load!=null){
                            progres_load.setVisibility(View.GONE);
                        }
                    }
                });

    }






    class BuyAdapter extends BaseAdapter {
        private List<BuyPojo> data;
        private Context context;
        String imge_user = "";

        public BuyAdapter(List<BuyPojo> data, Context context) {
            this.data = data;
            this.context = context;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if(view==null) {
                if (getActivity() != null) {
                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    view = inflater.inflate(R.layout.item_bought, parent, false);
                }
            }

            if(view!=null){
                de.hdodenhof.circleimageview.CircleImageView img_user = (de.hdodenhof.circleimageview.CircleImageView)
                        view.findViewById(R.id.img_user);
                TextView txt_seller                = (TextView)view.findViewById(R.id.txt_seller);
                TextView txt_ago                   = (TextView)view.findViewById(R.id.txt_ago);
                TextView txt_client_phn            = (TextView)view.findViewById(R.id.txt_client_phn);
                TextView txt_name_side             = (TextView)view.findViewById(R.id.txt_name_side);
                TextView txt_loc                   = (TextView)view.findViewById(R.id.txt_loc);
                TextView txt_budget                = (TextView)view.findViewById(R.id.txt_budget);
                final TextView txt_play_pause      = (TextView)view.findViewById(R.id.txt_pause_audio);
                final TextView txt_pause_audio     = (TextView)view.findViewById(R.id.txt_play_pause);
                final TextView txt_secs            = (TextView)view.findViewById(R.id.txt_secs);
                TextView txt_play                  = (TextView)view.findViewById(R.id.txt_play);
                TextView txt_des                   = (TextView)view.findViewById(R.id.txt_des);
                TextView txt_status                = (TextView)view.findViewById(R.id.txt_status);
                TextView txt_read_more             = (TextView)view.findViewById(R.id.txt_read_more);
                TextView txt_lead_price            = (TextView)view.findViewById(R.id.txt_lead_price);
                final RelativeLayout btn_buy       = (RelativeLayout)view.findViewById(R.id.btn_buy);
                TextView txt_bought_ny             = (TextView)view.findViewById(R.id.txt_bought_ny);
                TextView txt_client_name           = (TextView)view.findViewById(R.id.txt_client_name);
                TextView txt_des_client            = (TextView)view.findViewById(R.id.txt_des_client);
                ImageView img_shadow               = (ImageView) view.findViewById(R.id.img_shadow);
                View vw_line                       = (View) view.findViewById(R.id.vw_line);
                final RelativeLayout rel_call      = (RelativeLayout) view.findViewById(R.id.rel_call);
                final LinearLayout lnr_pro         = (LinearLayout) view.findViewById(R.id.lnr_pro);
                final SeekBar songProgressBar      = (SeekBar) view.findViewById(R.id.songProgressBar);
                final LinearLayout lnr_11          = (LinearLayout) view.findViewById(R.id.lnr_11);
                final  LinearLayout lnr_22         = (LinearLayout) view.findViewById(R.id.lnr_22);
                final  ImageView img_chat          = (ImageView) view.findViewById(R.id.img_chat);
                final  RelativeLayout rel_no_show  = (RelativeLayout) view.findViewById(R.id.rel_no_show);
                final  TextView txt_noentry_nxt    = (TextView) view.findViewById(R.id.txt_noentry_nxt);
                final  TextView txt_noentry        = (TextView) view.findViewById(R.id.txt_noentry);
                final  TextView txt_srch           = (TextView) view.findViewById(R.id.txt_srch);
                final  RelativeLayout btn_buy_last = (RelativeLayout) view.findViewById(R.id.btn_buy_last);
                final  TextView txt_by_now         = (TextView) view.findViewById(R.id.txt_by_now);
                final  FrameLayout frame_main      = (FrameLayout) view.findViewById(R.id.frame_main);
                final  ImageView img_file_desc     = (ImageView) view.findViewById(R.id.img_file_desc);
                final ProgressBar progress_one     = (ProgressBar) view.findViewById(R.id.progress_one);
                final  ProgressBar progress_two    = (ProgressBar) view.findViewById(R.id.progress_two);
                final  ImageView img_one           = (ImageView) view.findViewById(R.id.img_one);
                final  ImageView img_two           = (ImageView) view.findViewById(R.id.img_two);
                final  ProgressBar progres_load_user = (ProgressBar) view.findViewById(R.id.progres_load_user);

                txt_seller.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_name_side.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_client_name.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_des_client.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_budget.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_client_phn.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_loc.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_play.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_des.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_lead_price.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_read_more.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_ago.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_status.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_bought_ny.setTypeface(utils.Dina(getActivity()));
                txt_noentry_nxt.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_noentry.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_srch.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_by_now.setTypeface(utils.Dina(getActivity()));

                txt_play_pause.setTag("0");


                if(type_to_send.equalsIgnoreCase("2")){
                    txt_noentry_nxt.setText(getResources().getString(R.string.emty_arch_one));
                    txt_noentry.setText(getResources().getString(R.string.emty_arch_two));
                    btn_buy_last.setVisibility(View.GONE);
                    txt_srch.setVisibility(View.GONE);
                    btn_buy.setVisibility(View.GONE);
                }

                btn_buy_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(getActivity()!=null) {
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}


                            if(fragment_bought!=null){
                                MainActivity.fragmentManager.beginTransaction().remove(fragment_bought).commit();
                            }
                            if(fragment_sell!=null){
                                MainActivity.fragmentManager.beginTransaction().remove(fragment_sell).commit();
                            }
                            if(fragment_inbox!=null){
                                MainActivity.fragmentManager.beginTransaction().remove(fragment_inbox).commit();
                            }
                            if(fragment_more!=null){
                                MainActivity.fragmentManager.beginTransaction().remove(fragment_more).commit();
                            }
                            if(fragment_buy!=null){
                                MainActivity.fragmentManager.beginTransaction().show(fragment_buy).commit();
                            }else{
                                fragment_buy = new BuyFragment();
                                MainActivity. fragmentManager.beginTransaction().add(R.id.contentContainer, fragment_buy).commit();
                            }
                            vw_buy.setVisibility(View.VISIBLE);
                            vw_bought.setVisibility(View.GONE);
                            vw_sell.setVisibility(View.GONE);
                            vw_inbox.setVisibility(View.GONE);
                            vw_more.setVisibility(View.GONE);
                        }
                    }
                });

                if(!data.get(position).getLead_id().equalsIgnoreCase("")){
                    frame_main.setVisibility(View.VISIBLE);
                    rel_no_show.setVisibility(View.GONE);

                    songProgressBar.setOnTouchListener(new View.OnTouchListener(){
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return true;
                        }
                    });


                    if(!data.get(position).getUser_thum().equals("")){
                        if(!data.get(position).getUser_thum().equals("0")){
                            if(data.get(position).getUser_thum().trim().length()>2){
                                progres_load_user.setVisibility(View.VISIBLE);
                                Picasso.with(getActivity())
                                        .load(data.get(position).getUser_thum())
                                        .into(img_user, new com.squareup.picasso.Callback() {
                                            @Override
                                            public void onSuccess() {
                                                progres_load_user.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {
                                                progres_load_user.setVisibility(View.GONE);
                                            }
                                        });
                                imge_user = data.get(position).getUser_thum();
                            }else{
                                progres_load_user.setVisibility(View.GONE);
                                imge_user = "";
                            }
                        }else{
                            progres_load_user.setVisibility(View.GONE);
                            imge_user = "";
                        }
                    }else{
                        progres_load_user.setVisibility(View.GONE);
                        imge_user = "";
                    }

                    lnr_22.setVisibility(View.GONE);
                    if(data.get(position).getAudio()!=null){
                        if(data.get(position).getAudio().trim().equals("")){
                            lnr_11.setVisibility(View.GONE);
                            img_file_desc.setVisibility(View.VISIBLE);
                        }else{
                            lnr_11.setVisibility(View.VISIBLE);
                            img_file_desc.setVisibility(View.INVISIBLE);
                        }
                    }else{
                        lnr_11.setVisibility(View.GONE);
                        img_file_desc.setVisibility(View.INVISIBLE);
                    }

                    rel_call.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            phone_no = data.get(position).getCell_number();
                            if (utils.checkReadCallLogPermission(getActivity())) {
                                utils.hideKeyboard(getActivity());
                                try{
                                    handler_timer.removeCallbacks(runnable_timer);
                                }catch (Exception e){}

                                AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                                logger.logEvent("EVENT_NAME_BOUGHT_CALLED_CLIENT");

                                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                callIntent.setData(Uri.parse("tel:"+phone_no));
                                startActivity(callIntent);
                            } else {
                                requestPermissions(
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        1
                                );
                            }
                        }
                    });

                    try {
                        txt_secs.setText(data.get(position).getTime()+" "+getResources().getString(R.string.secs));
                    }catch (Exception e){}

                    txt_client_name.setText(data.get(position).getUser_name());
                    txt_name_side.setText(data.get(position).getClient_name());
                    txt_des.setText(data.get(position).getDescription());
                    txt_client_phn.setText(data.get(position).getCell_number());
                    txt_des_client.setText(data.get(position).getBusiness_name());
                    if(data.get(position).getLead_price()!=null){
                        if(!data.get(position).getLead_price().equals("0")){
                            if(!data.get(position).getLead_price().equals("")){
                                txt_lead_price.setText("$ "+data.get(position).getLead_price());
                            }else{
                                txt_lead_price.setText(getResources().getString(R.string.free));
                            }
                        }else{
                            txt_lead_price.setText(getResources().getString(R.string.free));
                        }
                    }else{
                        txt_lead_price.setText(getResources().getString(R.string.free));
                    }

                    int screenSize = getResources().getConfiguration().screenLayout &
                            Configuration.SCREENLAYOUT_SIZE_MASK;

                    String gg = data.get(position).getDescription();
                    if(gg.contains("\n")){
                        gg = data.get(position).getDescription().replaceAll("\n","........................................");
                    }

                    if(screenSize==Configuration.SCREENLAYOUT_SIZE_LARGE){
                        if(gg.length()>150){
                            txt_read_more.setVisibility(View.VISIBLE);
                            img_shadow.setVisibility(View.VISIBLE);
                            vw_line.setVisibility(View.GONE);
                        }else{
                            txt_read_more.setVisibility(View.INVISIBLE);
                            img_shadow.setVisibility(View.GONE);
                            vw_line.setVisibility(View.VISIBLE);
                        }
                    }
                    else if(screenSize==Configuration.SCREENLAYOUT_SIZE_NORMAL){
                        if(gg.length()>80){
                            txt_read_more.setVisibility(View.VISIBLE);
                            img_shadow.setVisibility(View.VISIBLE);
                            vw_line.setVisibility(View.GONE);
                        }else{
                            txt_read_more.setVisibility(View.INVISIBLE);
                            img_shadow.setVisibility(View.GONE);
                            vw_line.setVisibility(View.VISIBLE);
                        }
                    }
                    else if(screenSize==Configuration.SCREENLAYOUT_SIZE_SMALL){
                        if(gg.length()>40){
                            txt_read_more.setVisibility(View.VISIBLE);
                            img_shadow.setVisibility(View.VISIBLE);
                            vw_line.setVisibility(View.GONE);
                        }else{
                            txt_read_more.setVisibility(View.INVISIBLE);
                            img_shadow.setVisibility(View.GONE);
                            vw_line.setVisibility(View.VISIBLE);
                        }
                    }

                    if(data.get(position).getLat().equals("0")){
                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                    }else
                    if(data.get(position).getAddress()!=null){
                        if(!data.get(position).getAddress().equals("0")){
                            if(!data.get(position).getAddress().equals("")){
                                txt_loc.setText(data.get(position).getAddress());
                                txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                            }else{
                                if(data.get(position).getLocation_name()!=null){
                                    if(!data.get(position).getLocation_name().equals("0")){
                                        if(!data.get(position).getLocation_name().equals("")){
                                            txt_loc.setText(data.get(position).getLocation_name());
                                            txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                        }else{
                                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                        }
                                    }else{
                                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                    }

                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }

                            }
                        }else{
                            if(data.get(position).getLocation_name()!=null){
                                if(!data.get(position).getLocation_name().equals("0")){
                                    if(!data.get(position).getLocation_name().equals("")){
                                        txt_loc.setText(data.get(position).getLocation_name());
                                        txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                    }else{
                                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                    }
                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }

                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }
                        }
                    }else{
                        if(data.get(position).getLocation_name()!=null){
                            if(!data.get(position).getLocation_name().equals("0")){
                                if(!data.get(position).getLocation_name().equals("")){
                                    txt_loc.setText(data.get(position).getLocation_name());
                                    txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                }else{
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }
                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }

                        }else{
                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                        }
                    }

                    if(data.get(position).getBudget().equals("0")){
                        txt_budget.setTextColor(getResources().getColor(R.color.gray_budget));
                        txt_budget.setText(getResources().getString(R.string.bud_unknw));
                    }else {
                        txt_budget.setText("$" + data.get(position).getBudget() + " "+getResources().getString(R.string.budgt_sel));
                    }

                    txt_ago.setText(method_GetDuration(data.get(position).getCreated_time()));

                    txt_read_more.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}

                            dialog_LongLead(getActivity(),data.get(position).getDescription(),
                                    getResources().getString(R.string.lead_desc));
                        }
                    });

                    if(data.get(position).getRefund_req().equals("1")){
                        btn_buy.setTag("1");
                        txt_bought_ny.setText(getResources().getString(R.string.info));
                        txt_status.setVisibility(View.VISIBLE);
                        if(data.get(position).getRefund_approved().equals("0")){
                            if(data.get(position).getReject_refund_req().equals("0")) {
//                                txt_status.setText(getResources().getString(R.string.ref_pend));
                                txt_status.setText(getResources().getString(R.string.req_sent));
                            }else{
                                txt_status.setText(getResources().getString(R.string.ref_reject));
                            }
                        }else{
                            txt_status.setText(getResources().getString(R.string.ref_app));
                        }
                    }else if(data.get(position).getReject_refund_req().equals("1")){
                        btn_buy.setTag("1");
                        txt_bought_ny.setText(getResources().getString(R.string.info));
                        txt_status.setVisibility(View.VISIBLE);
                        txt_status.setText(getResources().getString(R.string.ref_reject));
                    }else if(data.get(position).getRefund_approved().equals("1")){
                        btn_buy.setTag("1");
                        txt_bought_ny.setText(getResources().getString(R.string.info));
                        txt_status.setVisibility(View.VISIBLE);
                        txt_status.setText(getResources().getString(R.string.ref_app));
                    }else{
                        txt_status.setVisibility(View.GONE);
                        btn_buy.setTag("0");
                    }
                    btn_buy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (btn_buy.getTag().equals("1")) {
                                dialog_Request_Pending();
                            } else {
                                apiRefund1Check();
//                                dialog_AddOption();
                            }
                        }
                        private void apiRefund1Check(){
                            AndroidNetworking.enableLogging();
//                            utils.showProgressDialog(getActivity(),getActivity().getResources().getString(R.string.load));
                            Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.CHECK_FIRST_TYM_REFUND);
                            Log.e("user_id: ", sharedPreferenceLeadr.getUserId());
                            Log.e("lead_id: ", data.get(position).getLead_id());

                            AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_FIRST_TYM_REFUND)
                                    .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                    .addBodyParameter("lead_id",data.get(position).getLead_id())
                                    .setTag("msg")
                                    .setPriority(Priority.HIGH).doNotCacheResponse()
                                    .build()
                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject result) {
                                            Log.e("res_refundTym1: ",result+"");
                                           String open_similar = "0";
                                            if(result.optString("status").equals("0")){
                                                if(result.optString("message").contains("Can not ask refund!")){
                                                    dialog_AddOption("1",result.optString("buy_time"));
                                                }else{
                                                    dialog_AddOption("0","");
                                                }
                                            }else{
                                                dialog_AddOption("0","");
                                            }
//                                            dialog_AddOption();
                                        }
                                        @Override
                                        public void onError(ANError error) {
                                            Log.e("", "---> On error  ");
                                            utils.dismissProgressdialog();
                                            dialog_AddOption("0","");
                                        }
                                    });

                        }
                        void dialog_Request_Pending(){
                            final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            View bottomSheetView  = getActivity().getLayoutInflater().inflate(R.layout.dialog_bought_u_askd, null);
                            dialog.setContentView(bottomSheetView);

                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog) {
                                    BottomSheetDialog d = (BottomSheetDialog) dialog;
                                    FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                    BottomSheetBehavior.from(bottomSheet)
                                            .setState(BottomSheetBehavior.STATE_EXPANDED);
                                }
                            });
                            TextView txt_close   = (TextView) dialog.findViewById(R.id.txt_close);
                            TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
                            TextView txt_resell  = (TextView) dialog.findViewById(R.id.txt_resell);
                            TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);
                            View vw_one  = (View) dialog.findViewById(R.id.vw_one);

                            txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_close.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_resell.setTypeface(utils.OpenSans_Bold(getActivity()));
                            txt_refund.setTypeface(utils.OpenSans_Regular(getActivity()));

                            txt_title.setText(getResources().getString(R.string.u_askd));
                            txt_resell.setText(getResources().getString(R.string.we_will));
                            txt_close.setText(getResources().getString(R.string.cancel));

                            vw_one.setVisibility(View.GONE);
                            txt_refund.setVisibility(View.GONE);
                            txt_close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });

                            dialog.show();
                        }


                        public  void dialog_AddOption( final String txt_change, String time){
                            final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            View bottomSheetView  = getActivity().getLayoutInflater().inflate(R.layout.dialog_option_bought, null);
                            dialog.setContentView(bottomSheetView);

                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog) {
                                    BottomSheetDialog d = (BottomSheetDialog) dialog;
                                    FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                    BottomSheetBehavior.from(bottomSheet)
                                            .setState(BottomSheetBehavior.STATE_EXPANDED);
                                }
                            });

                            TextView txt_resell = (TextView) dialog.findViewById(R.id.txt_resell);
                            final TextView txt_refund = (TextView) dialog.findViewById(R.id.txt_refund);
                            final TextView txt_archive = (TextView) dialog.findViewById(R.id.txt_archive);
                            TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
                            TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);

                            txt_archive.setVisibility(View.VISIBLE);

                            if(data.get(position).getLead_price()==null){
                                txt_refund.setVisibility(View.GONE);
                            }else if(data.get(position).getLead_price().equalsIgnoreCase("0")){
                                txt_refund.setVisibility(View.GONE);
                            }else if(data.get(position).getLead_price().equalsIgnoreCase("Free")){
                                txt_refund.setVisibility(View.GONE);
                            }


                            txt_refund.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_ok.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_resell.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_archive.setTypeface(utils.OpenSans_Regular(getActivity()));

                            RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
                            rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });

                            txt_archive.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //api add to archive
                                    //to do
                                    try{
                                        handler_timer.removeCallbacks(runnable_timer);
                                    }catch (Exception e){}

                                    apiArchive();
                                    dialog.dismiss();
                                }
                                private void apiArchive(){
                                    AndroidNetworking.enableLogging();
                                    utils.showProgressDialog(getActivity(),getActivity().getResources().getString(R.string.load));
                                    Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.ARCHIVE);
                                    Log.e("user_id: ", sharedPreferenceLeadr.getUserId());
                                    Log.e("lead_id: ", data.get(position).getLead_id());
                                    Log.e("lead_user_id: ", data.get(position).getUser_id());

                                    AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.ARCHIVE)
                                            .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                            .addBodyParameter("lead_id",data.get(position).getLead_id())
                                            .addBodyParameter("lead_user_id",data.get(position).getUser_id())
                                            .setTag("msg")
                                            .setPriority(Priority.HIGH).doNotCacheResponse()
                                            .build()
                                            .getAsJSONObject(new JSONObjectRequestListener() {
                                                @Override
                                                public void onResponse(JSONObject result) {
                                                    Log.e("res_chkCard: ",result+"");
                                                    utils.dismissProgressdialog();
                                                    dialog.dismiss();
                                                    swipeRight();
                                                }
                                                @Override
                                                public void onError(ANError error) {
                                                    Log.e("", "---> On error  ");
                                                    utils.dismissProgressdialog();
                                                }
                                            });

                                }
                            });


                            String duration = method_48HourCheck(data.get(position).getBuy_time());
                            /*if(duration.equals("0")){
                                txt_refund.setTag("0");
                                txt_refund.setTextColor(getResources().getColor(R.color.gray_budget));
                                int duration_set = 48-time_pending;
                                txt_refund.setText(getResources().getString(R.string.refund)+" "+context.getResources().getString(R.string.in)+duration_set
                                        +context.getResources().getString(R.string.hrs));
                                txt_refund.setTextColor(getResources().getColor(R.color.gray_budget));
                            }else{
                                String duration_days = method_7DayCheck(data.get(position).getBuy_date(),data.get(position).getBuy_time());
                                if(Integer.valueOf(duration_days)>10){
                                    txt_refund.setTag("0");
                                    txt_refund.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_refund.setText(context.getResources().getString(R.string.u_can));
                                }else{
                                    int duration_set = 10-Integer.valueOf(duration_days);
                                    txt_refund.setText(getResources().getString(R.string.refund)*//*+" (in "+duration_set+" days)"*//*);
                                    txt_refund.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_refund.setTag("1");
                                }

                            }*/
                            if(txt_change.equals("1")){
                                String left_time = method_CalculateTimeLeft(time);
                                txt_refund.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_refund.setAllCaps(true);
                                txt_refund.setText(getResources().getString(R.string.nine_left)+"("+left_time+" "+getResources().getString(R.string.passd)+")");
                            }else {
                                txt_refund.setText(getResources().getString(R.string.refund));
                            }

                            txt_refund.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if(txt_change.equals("0")) {
                                        dialog.dismiss();
//                                    Dialog_AskRefund();
                                        apiRefund1Check();
                                    }
                                }

                                private void apiRefund1Check(){
                                    AndroidNetworking.enableLogging();
                                    utils.showProgressDialog(getActivity(),getActivity().getResources().getString(R.string.load));
                                    Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.CHECK_FIRST_TYM_REFUND);
                                    Log.e("user_id: ", sharedPreferenceLeadr.getUserId());
                                    Log.e("lead_id: ", data.get(position).getLead_id());

                                    AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_FIRST_TYM_REFUND)
                                            .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                            .addBodyParameter("lead_id",data.get(position).getLead_id())
                                            .setTag("msg")
                                            .setPriority(Priority.HIGH).doNotCacheResponse()
                                            .build()
                                            .getAsJSONObject(new JSONObjectRequestListener() {
                                                @Override
                                                public void onResponse(JSONObject result) {
                                                    Log.e("res_refundTym1: ",result+"");
                                                    //{"":"0","message":" Not available!","buy_time":"10-40-00",
                                                    // "buy_date":"2018-05-15"}

                                                    //{"status":"1","message":" Available!","buy_time":"08-40-00",
                                                    // "buy_date":"2018-05-15"}

                                                    //{"status":"1","message":" Available!","buy_time":"12-02-08",
                                                    // "buy_date":"2018-05-18","days":7,"hours":48}

                                                    //if asked for refund but time has not passed - case 102(Can not ask refund!)

                                                    //"status":"1","message":"Available!" - now you can ask for refund(case 104)

                                                    //"status":"0","message":"Can not ask refund!" - hours has passed but user did not
                                                   // asked for refund, so now disable refund functionality (case 105)
                                                    utils.dismissProgressdialog();
                                                    dialog.dismiss();
                                                    Dialog_AskFrstRefund(result.optString("status"),result.optString("buy_time"),
                                                            result.optString("message")
                                                            ,result.optString("days"),result.optString("hours"));
                                                }
                                                @Override
                                                public void onError(ANError error) {
                                                    Log.e("", "---> On error  ");
                                                    utils.dismissProgressdialog();
                                                }
                                            });

                                }

                                private void Dialog_AskFrstRefund(String status, String time, String msg, String date_added,
                                                                  String time_added){
                                    final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    View bottomSheetView  = getActivity().getLayoutInflater().inflate(R.layout.dialog_askfrst_refund, null);
                                    dialog.setContentView(bottomSheetView);

                                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                        @Override
                                        public void onShow(DialogInterface dialog) {
                                            BottomSheetDialog d = (BottomSheetDialog) dialog;
                                            FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                            BottomSheetBehavior.from(bottomSheet)
                                                    .setState(BottomSheetBehavior.STATE_EXPANDED);
                                        }
                                    });
                                    TextView txt_nmbr             = (TextView) dialog.findViewById(R.id.txt_nmbr);
                                    final TextView txt_askrefund  = (TextView) dialog.findViewById(R.id.txt_askrefund);
                                    final TextView txt_issu_ref   = (TextView) dialog.findViewById(R.id.txt_issu_ref);
                                    final TextView txt_ask_sellr  = (TextView) dialog.findViewById(R.id.txt_ask_sellr);

                                    if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                                        txt_issu_ref.setGravity(Gravity.RIGHT);
                                        txt_ask_sellr.setGravity(Gravity.RIGHT);
                                    }

                                    txt_nmbr.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    txt_askrefund.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    txt_issu_ref.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    txt_ask_sellr.setTypeface(utils.OpenSans_Regular(getActivity()));


                                    txt_nmbr.setText(data.get(position).getCell_number());

                                    txt_nmbr.setOnClickListener(new OnClickListener() {
                                        @Override
                                        public void onClick( View v ) {
                                            if (utils.checkReadCallLogPermission(getActivity())) {
                                                utils.hideKeyboard(getActivity());
                                                dialog.dismiss();

                                                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                                callIntent.setData(Uri.parse("tel:"+phone_no));
                                                startActivity(callIntent);
                                            } else {
                                                dialog.dismiss();
                                                requestPermissions(
                                                        new String[]{Manifest.permission.CALL_PHONE},
                                                        1
                                                );
                                            }
                                        }
                                    });

                                    if(status.equals("0")){
                                        try{
                                            String duration = method_48HourCheckDynamic( time, Integer.valueOf(time_added));
                                        }catch (Exception e){
                                            String duration = method_48HourCheck( time);
                                        }

                                        if(msg.contains("Can not ask refund!")){
                                            //time has passed and user did not asked for refund
                                            String left_time = method_CalculateTimeLeft(time);
                                            txt_askrefund.setTextColor(getResources().getColor(R.color.gray_budget));
                                            txt_askrefund.setTag("0");
                                            txt_askrefund.setText(getResources().getString(R.string.nine_left)+"("+left_time+" "+getResources().getString(R.string.passd)+")");
                                        }
                                        else if(time_pending==0){
                                            //cannot ask refund
                                            txt_askrefund.setTag("0");
                                            String left_time = method_CalculateTimeLeftHOURS(time,time_added);

                                            txt_askrefund.setTextColor(getResources().getColor(R.color.gray_budget));
                                            txt_askrefund.setText(getResources().getString(R.string.refund)+" "+
                                                    context.getResources().getString(R.string.in)+left_time+")");
                                        }else if(msg.equalsIgnoreCase("Not available!")){
                                            int final_counter = Integer.valueOf(time_added) - time_pending;
                                            txt_askrefund.setTextColor(getResources().getColor(R.color.gray_budget));
                                            txt_askrefund.setTag("0");
                                            txt_askrefund.setText(getResources().getString(R.string.refund)+" "+
                                                    context.getResources().getString(R.string.in)+final_counter
                                                    +context.getResources().getString(R.string.hrs));
                                        }else{
                                            txt_askrefund.setTextColor(getResources().getColor(R.color.gray_budget));
                                            txt_askrefund.setTag("0");
                                            txt_askrefund.setText(getResources().getString(R.string.nine_left));
                                        }
                                    }else{

                                        //can ask for refund now
                                        String duration = method_7Day1Check(time);
                                        int final_counter = Integer.valueOf(date_added) - time_pending;
                                        String left_time = method_CalculateTimeLeft(time);
                                        txt_askrefund.setTag("1");
                                        txt_askrefund.setText(getResources().getString(R.string.refund)+" ("+
                                                context.getResources().getString(R.string.within)+" "+final_counter
                                                        +getResources().getString(R.string.d)+")"
                                                /*+context.getResources().getString(R.string.dy_left)*/);
                                    }

                                    txt_askrefund.setOnClickListener(new OnClickListener() {
                                        @Override
                                        public void onClick( View v ) {
                                            if(txt_askrefund.getTag().equals("1")){
                                                api_refund("1");
                                            }
                                        }

                                        private void api_refund(String type){
                                            AndroidNetworking.enableLogging();
                                            utils.showProgressDialog(getActivity(),context.getResources().getString(R.string.load));
                                            Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.REFUND);
                                            Log.e("user_id: ", sharedPreferenceLeadr.getUserId());
                                            Log.e("lead_id: ", data.get(position).getLead_id());
                                            Log.e("lead_user_id: ", data.get(position).getUser_id());
                                            Log.e("req_for: ", type);

                                            AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.REFUND)
                                                    .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                                    .addBodyParameter("lead_id",data.get(position).getLead_id())
                                                    .addBodyParameter("lead_user_id",data.get(position).getUser_id())
                                                    .addBodyParameter("lead_price",data.get(position).getLead_price())
                                                    .addBodyParameter("req_for",type)
                                                    .setTag("msg")
                                                    .setPriority(Priority.HIGH).doNotCacheResponse()
                                                    .build()
                                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                                        @Override
                                                        public void onResponse(JSONObject result) {
                                                            Log.e("res_chkCard: ",result+"");
                                                            utils.dismissProgressdialog();
                                                            dialog.dismiss();
                                                            if(result.optString("status").equalsIgnoreCase("1")) {
                                                                AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                                                                logger.logEvent("EVENT_NAME_BOUGHT_ASKED_REFUND");

                                                               /* try {
                                                                    handler_timer.postDelayed(runnable_timer, 40000);
                                                                } catch (Exception e) {
                                                                }*/
//                                                                Dialog_RefundApproved();
                                                                type_to_send = "1";
                                                                txt_buy_top.setText(getResources().getString(R.string.type_two));
                                                                api_Bought();
                                                            }else{
                                                                utils.dialog_msg_show(getActivity(),result.optString("message"));
                                                            }

                                                        }
                                                        @Override
                                                        public void onError(ANError error) {
                                                            Log.e("", "---> On error  ");
                                                            utils.dismissProgressdialog();
                                                        }
                                                    });

                                        }
                                    });
                                    dialog.show();
                                }

                                private void api_chck_status(){
                                    AndroidNetworking.enableLogging();
                                    utils.showProgressDialog(getActivity(),context.getResources().getString(R.string.load));
                                    Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.CHECK_REFUND_STATUS);
                                    Log.e("user_id: ", sharedPreferenceLeadr.getUserId());
                                    Log.e("lead_id: ", data.get(position).getLead_id());

                                    AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_REFUND_STATUS)
                                            .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                            .addBodyParameter("lead_id",data.get(position).getLead_id())
                                            .setTag("msg")
                                            .setPriority(Priority.HIGH).doNotCacheResponse()
                                            .build()
                                            .getAsJSONObject(new JSONObjectRequestListener() {
                                                @Override
                                                public void onResponse(JSONObject result) {
                                                    Log.e("res_chkstatus: ",result+"");
                                                    utils.dismissProgressdialog();
                                                   if(result.optString("status").equalsIgnoreCase("0")){
                                                       if(result.optString("message").equalsIgnoreCase("You should have Stripe account!")){
                                                           dialog_AddCard(data.get(position).getLead_id(),sharedPreferenceLeadr.getUserId(),position);
                                                       }
                                                       else if(result.optString("message").equalsIgnoreCase("Lead price should not be 0!")){
                                                           utils.dialog_msg_show(getActivity(),getResources().getString(R.string.lead_free));

                                                           try{
                                                               handler_timer.postDelayed(runnable_timer, 40000);
                                                           }catch (Exception e){}
                                                       }
                                                       else if(result.optString("message").equalsIgnoreCase("Lead deleted!")){
                                                           swipeRight();
                                                           utils.dialog_msg_show(getActivity(),getResources().getString(R.string.lead_del));
                                                       }
                                                   }else{
                                                       api_refund();
                                                   }
                                                }
                                                @Override
                                                public void onError(ANError error) {
                                                    Log.e("", "---> On error  ");
                                                    utils.dismissProgressdialog();
                                                }
                                            });

                                }

                                public  void dialog_AddCard(final String lead_id, final String userId, final int position) {
                                    final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Holo_NoActionBar_Fullscreen);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.activity_payment);

                                    final Button btnok           = (Button) dialog.findViewById(R.id.btnok);
                                    final EditText edt_cvv       = (EditText) dialog.findViewById(R.id.edt_cvv);
                                    final EditText edt_month     = (EditText) dialog.findViewById(R.id.edt_month);
                                    final EditText edt_year      = (EditText) dialog.findViewById(R.id.edt_year);
                                    final EditText edt_card_no   = (EditText) dialog.findViewById(R.id.edt_card_no);
                                    final EditText edt_full_name = (EditText) dialog.findViewById(R.id.edt_full_name);
                                    final ImageView img_cancel   = (ImageView) dialog.findViewById(R.id.img_cancel);
                                    final TextView txt_top       = (TextView) dialog.findViewById(R.id.txt_top);

                                    btnok.setTypeface(utils.Dina(getActivity()));
                                    edt_cvv.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    edt_month.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    edt_year.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    edt_card_no.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    edt_full_name.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    txt_top.setTypeface(utils.OpenSans_Regular(getActivity()));

                                    String text = "";
                                    if(sharedPreferenceLeadr.get_CARD().equalsIgnoreCase("N/A")){
                                        text = "<font color=#227cec>"+getResources().getString(R.string.frst_tym)+
                                                "</font> <font color=#071a30>"+getResources().getString(R.string.you_must)+"</font>";
                                    }else{
                                        text =
                                                "</font> <font color=#071a30>"+getResources().getString(R.string.ur_card)+"</font>";
                                    }
                                    txt_top.setText(Html.fromHtml(text));

                                    edt_cvv.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                        }

                                        @Override
                                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                                            if (s.length() == 3) {
                                                utils.hideKeyboard(getActivity());
                                            }
                                        }

                                        @Override
                                        public void afterTextChanged(Editable s) {

                                        }
                                    });


                                    edt_card_no.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                        }

                                        @Override
                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                            if (s.length() == 16) {
                                                edt_month.requestFocus();
                                                edt_month.setSelection(edt_month.getText().length());
                                            }
                                        }

                                        @Override
                                        public void afterTextChanged(Editable s) {

                                        }
                                    });

                                    edt_month.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                        }
                                        @Override
                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                            if (s.length() == 2) {
                                                edt_year.requestFocus();
                                                edt_year.setSelection(edt_year.getText().length());
                                            }
                                        }

                                        @Override
                                        public void afterTextChanged(Editable s) {
                                        }
                                    });

                                    edt_year.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                        }

                                        @Override
                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                            if (s.length() == 0) {
                                                edt_month.requestFocus();
                                                edt_month.setSelection(edt_month.getText().length());
                                            }else if(s.length() == 4){
                                                edt_cvv.requestFocus();
                                                edt_cvv.setSelection(edt_cvv.getText().length());
                                            }
                                        }
                                        @Override
                                        public void afterTextChanged(Editable s) {
                                        }
                                    });


                                    img_cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });


                                    btnok.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            method_check_values();
                                        }

                                        void method_check_values() {
                                            utils.hideKeyboard(getActivity());
                                            if (edt_full_name.getText().toString().trim().length() > 0) {
                                                if (edt_card_no.getText().toString().trim().length() > 0) {
                                                    if (edt_month.getText().toString().trim().length() == 2) {
                                                        if (edt_year.getText().toString().trim().length() == 4) {
                                                            if (edt_cvv.getText().toString().trim().length() > 0) {
                                                                Card card = new Card(edt_card_no.getText().toString(), Integer.valueOf(edt_month.getText().toString()),
                                                                        Integer.valueOf(edt_year.getText().toString()),
                                                                        edt_cvv.getText().toString());
                                                                if (!card.validateCard()) {
                                                                    utils.hideKeyboard(getActivity());
                                                                    if (!card.validateNumber()) {
                                                                        dialog_msg_show(getActivity(), getResources().getString(R.string.in_cvv),edt_card_no);

                                                                    } else if (!card.validateExpMonth()) {
                                                                        dialog_msg_show(getActivity(), getResources().getString(R.string.exp),edt_month);

                                                                    } else if (!card.validateExpiryDate()) {
                                                                        dialog_msg_show(getActivity(), getResources().getString(R.string.expyr),edt_year);

                                                                    } else if (!card.validateCVC()) {
                                                                        dialog_msg_show(getActivity(), getResources().getString(R.string.cvv_in),edt_cvv);

                                                                    }
                                                                } else {
                                                                    String substr = edt_card_no.getText().toString().substring(edt_card_no.getText().toString().
                                                                            length() - 4);
                                                                    utils.hideKeyboard(getActivity());
                                                                    dialog.dismiss();
//                                        utils.showProgressDialog(getActivity(), getResources().getString(R.string.pls_wait));
                                                                    if(dialog_card!=null){
                                                                        if(dialog_card.isShowing()){
                                                                            dialog_card.dismiss();
                                                                        }
                                                                    }
                                                                    dialog_CardFrstTYm(getActivity(),getResources().getString(R.string.frst_tym_longr));
                                                                    method_SendToStripe(card, lead_id,substr,userId,position);
                                                                }
                                                            } else {
                                                                dialog_msg_show(getActivity(), getResources().getString(R.string.entr_cvv),edt_cvv);

                                                            }
                                                        } else {
                                                            dialog_msg_show(getActivity(), getResources().getString(R.string.entr_yr),edt_year);

                                                        }
                                                    } else {
                                                        dialog_msg_show(getActivity(), getResources().getString(R.string.entr_mm),edt_month);

                                                    }
                                                } else {
                                                    dialog_msg_show(getActivity(), getResources().getString(R.string.entr_nmbr),edt_card_no);

                                                }
                                            } else {
                                                dialog_msg_show(getActivity(), getResources().getString(R.string.entr_namer),edt_full_name);

                                            }
                                        }


                                        public  void dialog_msg_show( Activity activity, String msg, final EditText edt){
                                            final BottomSheetDialog dialog = new BottomSheetDialog (activity);
                                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
                                            dialog.setContentView(bottomSheetView);
                                            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                                @Override
                                                public void onShow(DialogInterface dialog) {
                                                    BottomSheetDialog d = (BottomSheetDialog) dialog;
                                                    FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                                    BottomSheetBehavior.from(bottomSheet)
                                                            .setState(BottomSheetBehavior.STATE_EXPANDED);
                                                }
                                            });
                                            TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
                                            TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
                                            TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
                                            assert txt_msg != null;
                                            txt_msg.setText(msg);

                                            assert txt_title != null;
                                            txt_title.setTypeface(utils.OpenSans_Regular(activity));
                                            txt_ok.setTypeface(utils.OpenSans_Regular(activity));
                                            txt_msg.setTypeface(utils.OpenSans_Regular(activity));
                                            RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
                                            assert rel_vw_save_details != null;
                                            rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                    try{
                                                        ((InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                                                    }catch (Exception e){}
                                                    edt.requestFocus();
                                                }
                                            });
                                            try{
                                                dialog.show();
                                            }catch (Exception e){

                                            }

                                            dialog.setOnDismissListener(new OnDismissListener() {
                                                @Override
                                                public void onDismiss( DialogInterface dialog ) {
                                                    try{
                                                        ((InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                                                    }catch (Exception e){}
                                                    edt.requestFocus();
                                                }
                                            });

                                        }
                                        void method_SendToStripe(Card card, final String lead_id, final String subcard, final String userId, final int position) {
//                utils.dismissProgressdialog();//pk_test_AFG6u0uaLgXBKwMkFmd2QtjP
//                utils.dismissProgressdialog();//pk_live_r9TQXzdQ0OeTo6NiRxHJQxft
                                            Stripe stripe = new Stripe(getActivity(), "pk_live_r9TQXzdQ0OeTo6NiRxHJQxft");
                                            stripe.createToken(
                                                    card,
                                                    new TokenCallback() {
                                                        public void onSuccess(Token token) {
//                                utils.dismissProgressdialog();
                                                            if (utils.isNetworkAvailable(getActivity())) {
//                                    utils.showProgressDialog(getActivity(), getResources().getString(R.string.pls_wait));
                                                                api_SaveToken(token.getId(), lead_id,subcard,userId,position);
                                                            } else {
                                                                if(progres_load!=null) {
                                                                    progres_load.setVisibility(View.GONE);
                                                                }
                                                                utils.hideKeyboard(getActivity());
                                                                utils.dialog_msg_show(getActivity(), getResources().getString(R.string.no_internet));
                                                            }
                                                        }

                                                        public void onError(Exception error) {
                                                            // Show localized error message
                                                            utils.dismissProgressdialog();
                                                            utils.hideKeyboard(getActivity());
                                                            if(progres_load!=null) {
                                                                progres_load.setVisibility(View.GONE);
                                                            }
                                                            if(dialog_card!=null){
                                                                if(dialog_card.isShowing()){
                                                                    dialog_card.dismiss();
                                                                }
                                                            }
                                                            utils.dialog_msg_show(getActivity(), getResources().getString(R.string.err_card));
                                                        }
                                                    }
                                            );
                                        }


                                        private void api_SaveToken(String token, final String lead_id, final String subcard, final String userId,final int position) {
                                            AndroidNetworking.enableLogging();
//                utils.dismissProgressdialog();
//                utils.showProgressDialog(getActivity(), getResources().getString(R.string.load));
                                            Log.e("url_stripe: ", NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN);
//                Log.e("token: ", token);

                                            AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN)
                                                    .addBodyParameter("token", token)
                                                    .addBodyParameter("user_id", sharedPreferenceLeadr.getUserId())
                                                    .addBodyParameter("last_digits", subcard)
                                                    .setTag("msg")
                                                    .setPriority(Priority.HIGH).doNotCacheResponse()
                                                    .build()
                                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                                        @Override
                                                        public void onResponse(JSONObject result) {
                                                            Log.e("res_stripe: ", result + "");
                                                            //{"status":"1","customer":{"id":"cus_C4xJzoWD2YfwOO","object":"customer","account_balance":0,"created":1515128721,"currency":null,"default_source":"card_1BgnOOI0GVRrJIpDo8dLjinv","delinquent":false,"description":"Customer for LeadR app","discount":null,"email":null,"livemode":false,"metadata":{},"shipping":null,"sources":{"object":"list","data":[{"id":"card_1BgnOOI0GVRrJIpDo8dLjinv","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"Visa","country":"US","customer":"cus_C4xJzoWD2YfwOO","cvc_check":"pass","dynamic_last4":null,"exp_month":11,"exp_year":2022,"fingerprint":"kXn425uHw3F6DtGT","funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null}],"has_more":false,"total_count":1,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/subscriptions"}}}
                                                            if(dialog_card!=null){
                                                                if(dialog_card.isShowing()){
                                                                    dialog_card.dismiss();
                                                                }
                                                            }
                                                            if (result.optString("status").equals("1")) {
                                                                AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                                                                logger.logEvent("EVENT_NAME_ADDED_PAYMENT_INFO");
                                                                sharedPreferenceLeadr.set_CARD(subcard);
                                                                api_chck_status();
                                                            } else {
                                                                utils.dismissProgressdialog();
                                                                if(progres_load!=null) {
                                                                    progres_load.setVisibility(View.GONE);
                                                                }
                                                                dialog_errorCard(lead_id,userId,position);
                                                            }
//                                utils.dismissProgressdialog();
                                                        }

                                                        @Override
                                                        public void onError(ANError error) {
                                                            Log.i("", "---> On error  ");
                                                            utils.dismissProgressdialog();
                                                            if(dialog_card!=null){
                                                                if(dialog_card.isShowing()){
                                                                    dialog_card.dismiss();
                                                                }
                                                            }
                                                            if(progres_load!=null) {
                                                                progres_load.setVisibility(View.GONE);
                                                            }
                                                        }
                                                    });

                                        }
                                    });

                                    try {
                                        dialog.show();
                                    } catch (Exception e) {

                                    }


                                }


                                public  void dialog_errorCard(final String lead_id, final String userId,final int position){
                                    final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    View bottomSheetView  = getActivity().getLayoutInflater().inflate(R.layout.dialog_card_error, null);
                                    dialog.setContentView(bottomSheetView);
                                    BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                        @Override
                                        public void onShow(DialogInterface dialog) {
                                            BottomSheetDialog d = (BottomSheetDialog) dialog;
                                            FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                            BottomSheetBehavior.from(bottomSheet)
                                                    .setState(BottomSheetBehavior.STATE_EXPANDED);
                                        }
                                    });

                                    TextView txt_update = (TextView) dialog.findViewById(R.id.txt_update);
                                    TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
                                    TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
                                    TextView btn_update = (Button) dialog.findViewById(R.id.btn_update);

                                    txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    txt_ok.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    txt_update.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    btn_update.setTypeface(utils.OpenSans_Bold(getActivity()));

                                    RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
                                    rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();

                                            try{
                                                handler_timer.postDelayed(runnable_timer, 40000);
                                            }catch (Exception e){}
                                        }
                                    });

                                    btn_update.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                            dialog_AddCard(lead_id, userId, position);
                                        }
                                    });

                                    dialog.setOnDismissListener(new OnDismissListener() {
                                        @Override
                                        public void onDismiss( DialogInterface dialog ) {
                                            try{
                                                handler_timer.postDelayed(runnable_timer, 40000);
                                            }catch (Exception e){}
                                        }
                                    });
                                    dialog.show();


                                }


                                private void api_refund(){
                                    AndroidNetworking.enableLogging();
                                    utils.showProgressDialog(getActivity(),context.getResources().getString(R.string.load));
                                    Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.REFUND);
                                    Log.e("user_id: ", sharedPreferenceLeadr.getUserId());
                                    Log.e("lead_id: ", data.get(position).getLead_id());
                                    Log.e("lead_user_id: ", data.get(position).getUser_id());

                                    AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.REFUND)
                                            .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                            .addBodyParameter("lead_id",data.get(position).getLead_id())
                                            .addBodyParameter("lead_user_id",data.get(position).getUser_id())
                                            .addBodyParameter("lead_price",data.get(position).getLead_price())
                                            .setTag("msg")
                                            .setPriority(Priority.HIGH).doNotCacheResponse()
                                            .build()
                                            .getAsJSONObject(new JSONObjectRequestListener() {
                                                @Override
                                                public void onResponse(JSONObject result) {
                                                    Log.e("res_chkCard: ",result+"");
                                                    utils.dismissProgressdialog();
                                                    dialog.dismiss();
                                                    AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                                                    logger.logEvent("EVENT_NAME_BOUGHT_ASKED_REFUND");

                                                    try{
                                                        handler_timer.postDelayed(runnable_timer, 40000);
                                                    }catch (Exception e){}
                                                    Dialog_RefundApproved();
                                                }
                                                @Override
                                                public void onError(ANError error) {
                                                    Log.e("", "---> On error  ");
                                                    utils.dismissProgressdialog();
                                                }
                                            });

                                }
                                void Dialog_RefundApproved(){
                                    final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    View bottomSheetView  = getActivity().getLayoutInflater().inflate(R.layout.dialog_bought_u_askd, null);
                                    dialog.setContentView(bottomSheetView);

                                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                        @Override
                                        public void onShow(DialogInterface dialog) {
                                            BottomSheetDialog d = (BottomSheetDialog) dialog;
                                            FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                            BottomSheetBehavior.from(bottomSheet)
                                                    .setState(BottomSheetBehavior.STATE_EXPANDED);
                                        }
                                    });
                                    TextView txt_close   = (TextView) dialog.findViewById(R.id.txt_close);
                                    TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
                                    TextView txt_resell  = (TextView) dialog.findViewById(R.id.txt_resell);
                                    TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);
                                    View vw_one          = (View) dialog.findViewById(R.id.vw_one);

                                    txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    txt_close.setTypeface(utils.OpenSans_Regular(getActivity()));
                                    txt_resell.setTypeface(utils.OpenSans_Bold(getActivity()));
                                    txt_refund.setTypeface(utils.OpenSans_Regular(getActivity()));

                                    txt_title.setText(getResources().getString(R.string.u_askd));
                                    txt_resell.setText(getResources().getString(R.string.we_will));
                                    txt_close.setText(getResources().getString(R.string.cancel));

                                    vw_one.setVisibility(View.GONE);
                                    txt_refund.setVisibility(View.GONE);
                                    txt_close.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                            swipeRight();

//                                            method_setToInfo();
                                        }
                                    });

                                    dialog.setOnDismissListener(new OnDismissListener() {
                                        @Override
                                        public void onDismiss( DialogInterface dialog ) {
                                            swipeRight();
                                            if(adapter!=null){
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    });

                                    dialog.show();
                                }

                                void method_setToInfo(){
                                    BuyPojo item = new BuyPojo();
                                    item.setAudio(data.get(position).getAudio());
                                    item.setBudget(data.get(position).getBudget());
                                    item.setBusiness_fb_page(data.get(position).getBusiness_fb_page());
                                    item.setBusiness_image(data.get(position).getBusiness_image());
                                    item.setBusiness_info(data.get(position).getBusiness_info());
                                    item.setBusiness_name(data.get(position).getBusiness_name());
                                    item.setCategory(data.get(position).getCategory());
                                    item.setCell_number(data.get(position).getCell_number());
                                    String client_name = data.get(position).getClient_name();
                                    item.setClient_name(client_name);
                                    item.setDescription(data.get(position).getDescription());
                                    item.setId(data.get(position).getId());
                                    item.setJob_title(data.get(position).getJob_title());
                                    //if it 0 then it means lead is in active
                                    item.setIs_delete(data.get(position).getIs_delete());
                                    item.setLead_id(data.get(position).getLead_id());
                                    item.setLead_price(data.get(position).getLead_price());
                                    item.setPhone_number(data.get(position).getPhone_number());
                                    item.setType(data.get(position).getType());
                                    item.setUser_id(data.get(position).getUser_id());
                                    item.setUser_image(data.get(position).getUser_image());
                                    item.setUser_name(data.get(position).getUser_name());
                                    item.setLat(data.get(position).getLat());
                                    item.setLon(data.get(position).getLon());
                                    item.setLocation_name(data.get(position).getLocation_name());
                                    item.setCreated_time(data.get(position).getCreated_time());
                                    //"buy_time":"07-26-53","buy_date":"2018-03-28"
                                    item.setBuy_time(data.get(position).getBuy_time());
                                    item.setTime(data.get(position).getTime());
                                    item.setCountry(data.get(position).getCountry());
                                    item.setCondition(false);
                                    item.setAddress(data.get(position).getAddress());
                                    item.setUser_thum(data.get(position).getUser_thum());
                                    item.setRefund_req("1");
                                    item.setRefund_approved("0");
                                    item.setReject_refund_req(data.get(position).getReject_refund_req());
                                    arr_buy.set(position,item);
                                }
                            });

                            txt_resell.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();

                                    try{
                                        handler_timer.removeCallbacks(runnable_timer);
                                    }catch (Exception e){}

                                    Intent i_resell = new Intent(getActivity(), SellActivity.class);
                                    i_resell.putExtra("desc", data.get(position).getDescription());
                                    i_resell.putExtra("audio", data.get(position).getAudio());
                                    i_resell.putExtra("budget", data.get(position).getBudget());
                                    i_resell.putExtra("cat", data.get(position).getCategory());
                                    i_resell.putExtra("phone", data.get(position).getCell_number());
                                    i_resell.putExtra("client_name", data.get(position).getClient_name());
                                    i_resell.putExtra("lead_price", data.get(position).getLead_price());
                                    i_resell.putExtra("lead_id", data.get(position).getLead_id());
                                    i_resell.putExtra("time", data.get(position).getTime());
                                    i_resell.putExtra("resell", "1");
                                    if(!data.get(position).getLocation_name().equals("0")){
                                        if(!data.get(position).getLocation_name().equals("")){
                                            i_resell.putExtra("loc", data.get(position).getLocation_name());
                                            i_resell.putExtra("lat", data.get(position).getLat());
                                            i_resell.putExtra("long_", data.get(position).getLon());
                                            i_resell.putExtra("country", data.get(position).getCountry());
                                            i_resell.putExtra("address", data.get(position).getAddress());
                                        }else{
                                            i_resell.putExtra("loc", "0");
                                        }
                                    }else{
                                        i_resell.putExtra("loc", "0");
                                    }
                                    startActivity(i_resell);
                                }
                            });

                            dialog.show();


                        }

                    });


                    img_chat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}
                            Intent i_chang = new Intent(getActivity(), ChatActivity.class);
                            i_chang.putExtra("image_other", imge_user);
                            i_chang.putExtra("name_other", data.get(position).getUser_name());
                            i_chang.putExtra("buss_other", data.get(position).getBusiness_name());
                            i_chang.putExtra("info_other", data.get(position).getBuy_userbusinessinfo());
                            i_chang.putExtra("id_other", data.get(position).getUser_id());
                            i_chang.putExtra("leadid_other", data.get(position).getLead_id());
                            i_chang.putExtra("lead_user_id", data.get(position).getUser_id());
                            i_chang.putExtra("other_user_id", sharedPreferenceLeadr.getUserId());
                            i_chang.putExtra("audio",data.get(position).getAudio());
                            i_chang.putExtra("audiotime", data.get(position).getTime());
                            i_chang.putExtra("desc", data.get(position).getDescription());
                            i_chang.putExtra("typemy", "I Bought");
                            i_chang.putExtra("typeother", "I Sold");
                            startActivity(i_chang);

                          /*  Intent i_chang = new Intent(getActivity(), ChatActivity.class);
                            i_chang.putExtra("image_other",imge_user);
                            i_chang.putExtra("name_other",data.get(position).getUser_name());
                            i_chang.putExtra("buss_other",data.get(position).getBusiness_name());
                            i_chang.putExtra("info_other",data.get(position).getBuy_userbusinessinfo());
                            i_chang.putExtra("id_other",data.get(position).getUser_id());
                            i_chang.putExtra("leadid_other",data.get(position).getLead_id());
                            i_chang.putExtra("type","buy");
                            startActivity(i_chang);*/
                        }
                    });

                    lnr_pro.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}

                            Intent i_pro = new Intent(getActivity(), ProfileOther_Activity.class);
                            i_pro.putExtra("id",data.get(position).getUser_id());
                            startActivity(i_pro);
                        }
                    });


                    txt_pause_audio.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            lnr_11.performClick();
                        }
                    });

                    lnr_11.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}

                            lnr_11.setVisibility(View.GONE);
                            lnr_11.setClickable(false);
                            lnr_22.setVisibility(View.VISIBLE);
                            if(txt_play_pause.getTag().equals("0")){
                                progress_one.setVisibility(View.VISIBLE);
                                progress_two.setVisibility(View.VISIBLE);
                                img_one.setVisibility(View.GONE);
                                img_two.setVisibility(View.GONE);
                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        progress_one.setVisibility(View.GONE);
                                        progress_two.setVisibility(View.GONE);
                                        img_one.setVisibility(View.VISIBLE);
                                        img_two.setVisibility(View.VISIBLE);
                                    }

                                }, 1000);
                                txt_play_pause.setTag("1");
                                onPlay(false);
                            }else{
                                txt_play_pause.setTag("0");
                                onPlay(true);
                            }
                        }
                        //
                        private void onPlay(boolean isPlaying2){
                            if (!isPlaying2) {
                                //currently MediaPlayer is not playing audio
                                if(mMediaPlayer == null) {
                                    txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
                                    startPlaying(); //start from beginning
                                    isPlaying = !isPlaying;
                                } else {
                                    txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
                                    resumePlaying(); //resume the currently paused MediaPlayer
                                }
                            } else {
                                //pause the MediaPlayer
                                txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pause_sell, 0, 0, 0);
                                pausePlaying();
                            }
                        /*lnr_11.setVisibility(View.GONE);
//                        lnr_11.setClickable(false);
                        lnr_22.setVisibility(View.VISIBLE);*/
                        }

                        private void startPlaying() {
                            mMediaPlayer = new MediaPlayer();
                            try {
                                mMediaPlayer.setDataSource(data.get(position).getAudio());
                                mMediaPlayer.prepare();

                                count_start_pause_add = 0;
                                count_start_pause = Integer.valueOf(data.get(position).getTime())+2;

                                songProgressBar.setMax(mMediaPlayer.getDuration());
                                songProgressBar.setProgress(0);
                                mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        mMediaPlayer.start();
                                    }
                                });
                            } catch (IOException e) {
                                Log.e("", "prepare() failed");
                            }
                            count_start_pause_add = 0;
                            method_countDown(Integer.valueOf(data.get(position).getTime())+1);
                            method_countDown_Seekbar();

                            mMediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                                @Override
                                public void onSeekComplete(MediaPlayer mp) {

                                }
                            });
                            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    stopPlaying();
                                }
                            });

                            //keep screen on while playing audio
                            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        }

                        void method_countDown_Seekbar(){
                            mHandler_seekbar = new Handler();
                            mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);

                        }

                         Runnable mRunnabl_seekbar = new Runnable() {
                            @Override
                            public void run() {
                                if(mMediaPlayer != null){
//                Log.e("seejbr: ",mCurrentPosition+"");
                                    Log.e("seejbr2: ",mMediaPlayer.getCurrentPosition()+"");
                                    songProgressBar.setProgress(mMediaPlayer.getCurrentPosition());
                                }
                                mHandler_seekbar.postDelayed(this, 15);
                            }
                        };


                        void method_countDown(int timer){
                            countDownTimer = null;
                            countDownTimer = new CountDownTimer(timer*1000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    long millis = millisUntilFinished;
                                    //Convert milliseconds into hour,minute and seconds
                                    String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) -
                                                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                            TimeUnit.MILLISECONDS.toSeconds(millis) -
                                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

                                    txt_play_pause.setText(hms+"");

                                    count_start_pause = count_start_pause-1;
                                    count_start_pause_add = count_start_pause_add+1;

//                                songProgressBar.setProgress(count_start_pause_add);
                                    if(count_start_pause==1){
//                                    songProgressBar.setProgress(count_start_pause_add);
                                        stopPlaying();
                                    }
                                }
                                public void onFinish() {
                                    countDownTimer = null;//set CountDownTimer to null
                                }
                            }.start();
                        }

                        private void updateSeekBar() {
                            mHandler.postDelayed(mRunnable, 1000);
                        }

                        private void pausePlaying() {
                            mHandler.removeCallbacks(mRunnable);
                            resume_pos = mMediaPlayer.getCurrentPosition();
                            countDownTimer.cancel();
                            mMediaPlayer.pause();

                            if(mHandler_seekbar!=null){
                                mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                            }
                        }

                        private void resumePlaying() {
//        mMediaPlayer.seekTo(resume_pos);
                            mHandler.removeCallbacks(mRunnable);
                            mMediaPlayer.start();
                            updateSeekBar();
                            method_countDown(count_start_pause-1);
                            mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);
//        updateSeekBar();
                        }

                        private void stopPlaying() {
                            if(getActivity()!=null) {
                                lnr_11.setClickable(true);
                                mHandler.removeCallbacks(mRunnable);
                                mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                                mMediaPlayer.stop();
                                mMediaPlayer.reset();
                                mMediaPlayer.release();
                                mMediaPlayer = null;
                                resume_pos = 0;
                                lnr_11.setVisibility(View.VISIBLE);
                                lnr_22.setVisibility(View.GONE);

                                isPlaying = !isPlaying;
//                        songProgressBar.setProgress(songProgressBar.getMax());

                                if (countDownTimer != null) {
                                    countDownTimer.cancel();
                                }
                                txt_play_pause.setTag("0");
                                //allow the screen to turn off again once audio is finished playing
                                if (getActivity() != null) {
                                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                }

                                try {
                                    handler_timer.postDelayed(runnable_timer, 40000);
                                } catch (Exception e) {
                                }
                            }
                        }

                        //updating mSeekBar
                        private Runnable mRunnable = new Runnable() {
                            @Override
                            public void run() {
                                if(mMediaPlayer != null){

                                    int mCurrentPosition = mMediaPlayer.getCurrentPosition();
                                    long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);
                                    long seconds = TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition)
                                            - TimeUnit.MINUTES.toSeconds(minutes);
                                    updateSeekBar();
                                }
                            }
                        };
                    });
                }else{
                    frame_main.setVisibility(View.GONE);
                    rel_no_show.setVisibility(View.VISIBLE);
                }
            }

            return view;
        }

    }




    class RefundAdapter extends BaseAdapter {
        private List<BuyPojo> data;
        private Context context;
        String imge_user = "";

        public RefundAdapter(List<BuyPojo> data, Context context) {
            this.data = data;
            this.context = context;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if(view==null) {
                if (getActivity() != null) {
                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    view = inflater.inflate(R.layout.item_bought, parent, false);
                }
            }

            if(view!=null){
                de.hdodenhof.circleimageview.CircleImageView img_user = (de.hdodenhof.circleimageview.CircleImageView)
                        view.findViewById(R.id.img_user);
                TextView txt_seller                = (TextView)view.findViewById(R.id.txt_seller);
                TextView txt_ago                   = (TextView)view.findViewById(R.id.txt_ago);
                TextView txt_client_phn            = (TextView)view.findViewById(R.id.txt_client_phn);
                TextView txt_name_side             = (TextView)view.findViewById(R.id.txt_name_side);
                TextView txt_loc                   = (TextView)view.findViewById(R.id.txt_loc);
                TextView txt_budget                = (TextView)view.findViewById(R.id.txt_budget);
                final TextView txt_play_pause      = (TextView)view.findViewById(R.id.txt_pause_audio);
                final TextView txt_pause_audio     = (TextView)view.findViewById(R.id.txt_play_pause);
                final TextView txt_secs            = (TextView)view.findViewById(R.id.txt_secs);
                TextView txt_play                  = (TextView)view.findViewById(R.id.txt_play);
                TextView txt_des                   = (TextView)view.findViewById(R.id.txt_des);
                final TextView txt_status          = (TextView)view.findViewById(R.id.txt_status);
                TextView txt_read_more             = (TextView)view.findViewById(R.id.txt_read_more);
                TextView txt_lead_price            = (TextView)view.findViewById(R.id.txt_lead_price);
                final RelativeLayout btn_buy       = (RelativeLayout)view.findViewById(R.id.btn_buy);
                TextView txt_bought_ny             = (TextView)view.findViewById(R.id.txt_bought_ny);
                TextView txt_client_name           = (TextView)view.findViewById(R.id.txt_client_name);
                TextView txt_des_client            = (TextView)view.findViewById(R.id.txt_des_client);
                ImageView img_shadow               = (ImageView) view.findViewById(R.id.img_shadow);
                View vw_line                       = (View) view.findViewById(R.id.vw_line);
                final RelativeLayout rel_call      = (RelativeLayout) view.findViewById(R.id.rel_call);
                final LinearLayout lnr_pro         = (LinearLayout) view.findViewById(R.id.lnr_pro);
                final SeekBar songProgressBar      = (SeekBar) view.findViewById(R.id.songProgressBar);
                final LinearLayout lnr_11          = (LinearLayout) view.findViewById(R.id.lnr_11);
                final  LinearLayout lnr_22         = (LinearLayout) view.findViewById(R.id.lnr_22);
                final  ImageView img_chat          = (ImageView) view.findViewById(R.id.img_chat);
                final  RelativeLayout rel_no_show  = (RelativeLayout) view.findViewById(R.id.rel_no_show);
                final  TextView txt_noentry_nxt    = (TextView) view.findViewById(R.id.txt_noentry_nxt);
                final  TextView txt_noentry        = (TextView) view.findViewById(R.id.txt_noentry);
                final  TextView txt_srch           = (TextView) view.findViewById(R.id.txt_srch);
                final  RelativeLayout btn_buy_last = (RelativeLayout) view.findViewById(R.id.btn_buy_last);
                final  TextView txt_by_now         = (TextView) view.findViewById(R.id.txt_by_now);
                final  FrameLayout frame_main      = (FrameLayout) view.findViewById(R.id.frame_main);
                final  ImageView img_file_desc     = (ImageView) view.findViewById(R.id.img_file_desc);
                final ProgressBar progress_one     = (ProgressBar) view.findViewById(R.id.progress_one);
                final  ProgressBar progress_two    = (ProgressBar) view.findViewById(R.id.progress_two);
                final  ImageView img_one           = (ImageView) view.findViewById(R.id.img_one);
                final  ImageView img_two           = (ImageView) view.findViewById(R.id.img_two);
                final  ProgressBar progres_load_user = (ProgressBar) view.findViewById(R.id.progres_load_user);

                txt_seller.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_name_side.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_client_name.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_des_client.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_budget.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_client_phn.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_loc.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_play.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_des.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_lead_price.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_read_more.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_ago.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_status.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_bought_ny.setTypeface(utils.Dina(getActivity()));
                txt_noentry_nxt.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_noentry.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_srch.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_by_now.setTypeface(utils.Dina(getActivity()));

                txt_play_pause.setTag("0");

                txt_noentry_nxt.setText(getResources().getString(R.string.emty_rfnd_one));
                txt_noentry.setText(getResources().getString(R.string.emty_rfnd_two));

                btn_buy_last.setVisibility(View.GONE);
                txt_srch.setVisibility(View.GONE);

                btn_buy_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(getActivity()!=null) {
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}


                            if(fragment_bought!=null){
                                MainActivity.fragmentManager.beginTransaction().remove(fragment_bought).commit();
                            }
                            if(fragment_sell!=null){
                                MainActivity.fragmentManager.beginTransaction().remove(fragment_sell).commit();
                            }
                            if(fragment_inbox!=null){
                                MainActivity.fragmentManager.beginTransaction().remove(fragment_inbox).commit();
                            }
                            if(fragment_more!=null){
                                MainActivity.fragmentManager.beginTransaction().remove(fragment_more).commit();
                            }
                            if(fragment_buy!=null){
                                MainActivity.fragmentManager.beginTransaction().show(fragment_buy).commit();
                            }else{
                                fragment_buy = new BuyFragment();
                                MainActivity. fragmentManager.beginTransaction().add(R.id.contentContainer, fragment_buy).commit();
                            }
                            vw_buy.setVisibility(View.VISIBLE);
                            vw_bought.setVisibility(View.GONE);
                            vw_sell.setVisibility(View.GONE);
                            vw_inbox.setVisibility(View.GONE);
                            vw_more.setVisibility(View.GONE);
                        }
                    }
                });

                if(!data.get(position).getLead_id().equalsIgnoreCase("")){
                    frame_main.setVisibility(View.VISIBLE);
                    rel_no_show.setVisibility(View.GONE);

                    songProgressBar.setOnTouchListener(new View.OnTouchListener(){
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return true;
                        }
                    });


                    if(!data.get(position).getUser_thum().equals("")){
                        if(!data.get(position).getUser_thum().equals("0")){
                            if(data.get(position).getUser_thum().trim().length()>2){
                                progres_load_user.setVisibility(View.VISIBLE);
                                Picasso.with(getActivity())
                                        .load(data.get(position).getUser_thum())
                                        .into(img_user, new com.squareup.picasso.Callback() {
                                            @Override
                                            public void onSuccess() {
                                                progres_load_user.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {
                                                progres_load_user.setVisibility(View.GONE);
                                            }
                                        });
                                imge_user = data.get(position).getUser_thum();
                            }else{
                                progres_load_user.setVisibility(View.GONE);
                                imge_user = "";
                            }
                        }else{
                            progres_load_user.setVisibility(View.GONE);
                            imge_user = "";
                        }
                    }else{
                        progres_load_user.setVisibility(View.GONE);
                        imge_user = "";
                    }

                    lnr_22.setVisibility(View.GONE);
                    if(data.get(position).getAudio()!=null){
                        if(data.get(position).getAudio().trim().equals("")){
                            lnr_11.setVisibility(View.GONE);
                            img_file_desc.setVisibility(View.VISIBLE);
                        }else{
                            lnr_11.setVisibility(View.VISIBLE);
                            img_file_desc.setVisibility(View.INVISIBLE);
                        }
                    }else{
                        lnr_11.setVisibility(View.GONE);
                        img_file_desc.setVisibility(View.INVISIBLE);
                    }

                    rel_call.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            phone_no = data.get(position).getCell_number();
                            if (utils.checkReadCallLogPermission(getActivity())) {
                                utils.hideKeyboard(getActivity());
                                try{
                                    handler_timer.removeCallbacks(runnable_timer);
                                }catch (Exception e){}

                                AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                                logger.logEvent("EVENT_NAME_BOUGHT_CALLED_CLIENT");

                                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                callIntent.setData(Uri.parse("tel:"+phone_no));
                                startActivity(callIntent);
                            } else {
                                requestPermissions(
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        1
                                );
                            }
                        }
                    });

                    try {
                        txt_secs.setText(data.get(position).getTime()+" "+getResources().getString(R.string.secs));
                    }catch (Exception e){}

                    txt_client_name.setText(data.get(position).getUser_name());
                    txt_name_side.setText(data.get(position).getClient_name());
                    txt_des.setText(data.get(position).getDescription());
                    txt_client_phn.setText(data.get(position).getCell_number());
                    txt_des_client.setText(data.get(position).getBusiness_name());

                    if(data.get(position).getLead_price()!=null){
                        if(!data.get(position).getLead_price().equals("0")){
                            if(!data.get(position).getLead_price().equals("")){
                                txt_lead_price.setText("$ "+data.get(position).getLead_price());
                            }else{
                                txt_lead_price.setText(getResources().getString(R.string.free));
                            }
                        }else{
                            txt_lead_price.setText(getResources().getString(R.string.free));
                        }
                    }else{
                        txt_lead_price.setText(getResources().getString(R.string.free));
                    }

                    int screenSize = getResources().getConfiguration().screenLayout &
                            Configuration.SCREENLAYOUT_SIZE_MASK;

                    String gg = data.get(position).getDescription();
                    if(gg.contains("\n")){
                        gg = data.get(position).getDescription().replaceAll("\n","........................................");
                    }

                    if(screenSize==Configuration.SCREENLAYOUT_SIZE_LARGE){
                        if(gg.length()>150){
                            txt_read_more.setVisibility(View.VISIBLE);
                            img_shadow.setVisibility(View.VISIBLE);
                            vw_line.setVisibility(View.GONE);
                        }else{
                            txt_read_more.setVisibility(View.INVISIBLE);
                            img_shadow.setVisibility(View.GONE);
                            vw_line.setVisibility(View.VISIBLE);
                        }
                    }
                    else if(screenSize==Configuration.SCREENLAYOUT_SIZE_NORMAL){
                        if(gg.length()>80){
                            txt_read_more.setVisibility(View.VISIBLE);
                            img_shadow.setVisibility(View.VISIBLE);
                            vw_line.setVisibility(View.GONE);
                        }else{
                            txt_read_more.setVisibility(View.INVISIBLE);
                            img_shadow.setVisibility(View.GONE);
                            vw_line.setVisibility(View.VISIBLE);
                        }
                    }
                    else if(screenSize==Configuration.SCREENLAYOUT_SIZE_SMALL){
                        if(gg.length()>40){
                            txt_read_more.setVisibility(View.VISIBLE);
                            img_shadow.setVisibility(View.VISIBLE);
                            vw_line.setVisibility(View.GONE);
                        }else{
                            txt_read_more.setVisibility(View.INVISIBLE);
                            img_shadow.setVisibility(View.GONE);
                            vw_line.setVisibility(View.VISIBLE);
                        }
                    }

                    if(data.get(position).getLat().equals("0")){
                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                    }else
                    if(data.get(position).getAddress()!=null){
                        if(!data.get(position).getAddress().equals("0")){
                            if(!data.get(position).getAddress().equals("")){
                                txt_loc.setText(data.get(position).getAddress());
                                txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                            }else{
                                if(data.get(position).getLocation_name()!=null){
                                    if(!data.get(position).getLocation_name().equals("0")){
                                        if(!data.get(position).getLocation_name().equals("")){
                                            txt_loc.setText(data.get(position).getLocation_name());
                                            txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                        }else{
                                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                        }
                                    }else{
                                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                    }

                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }

                            }
                        }else{
                            if(data.get(position).getLocation_name()!=null){
                                if(!data.get(position).getLocation_name().equals("0")){
                                    if(!data.get(position).getLocation_name().equals("")){
                                        txt_loc.setText(data.get(position).getLocation_name());
                                        txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                    }else{
                                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                    }
                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }

                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }
                        }
                    }else{
                        if(data.get(position).getLocation_name()!=null){
                            if(!data.get(position).getLocation_name().equals("0")){
                                if(!data.get(position).getLocation_name().equals("")){
                                    txt_loc.setText(data.get(position).getLocation_name());
                                    txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                }else{
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }
                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }

                        }else{
                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                        }
                    }

                    if(data.get(position).getBudget().equals("0")){
                        txt_budget.setTextColor(getResources().getColor(R.color.gray_budget));
                        txt_budget.setText(getResources().getString(R.string.bud_unknw));
                    }else {
                        txt_budget.setText("$" + data.get(position).getBudget() + " "+getResources().getString(R.string.budgt_sel));
                    }

                    txt_ago.setText(method_GetDuration(data.get(position).getCreated_time()));

                    txt_read_more.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}

                            dialog_LongLead(getActivity(),data.get(position).getDescription(),
                                    getResources().getString(R.string.lead_desc));
                        }
                    });

                    /*if(data.get(position).getRefund_req().equals("1")){
                        btn_buy.setTag("1");
                        txt_bought_ny.setText(getResources().getString(R.string.info));
                        txt_status.setVisibility(View.VISIBLE);
                        if(data.get(position).getRefund_approved().equals("0")){
                            if(data.get(position).getReject_refund_req().equals("0")) {
                                txt_status.setText(getResources().getString(R.string.ref_pend));
                            }else{
                                txt_status.setText(getResources().getString(R.string.ref_reject));
                            }
                        }else{
                            txt_status.setText(getResources().getString(R.string.ref_app));
                        }
                    }else if(data.get(position).getReject_refund_req().equals("1")){
                        btn_buy.setTag("1");
                        txt_bought_ny.setText(getResources().getString(R.string.info));
                        txt_status.setVisibility(View.VISIBLE);
                        txt_status.setText(getResources().getString(R.string.ref_reject));
                    }else if(data.get(position).getRefund_approved().equals("1")){
                        btn_buy.setTag("1");
                        txt_bought_ny.setText(getResources().getString(R.string.info));
                        txt_status.setVisibility(View.VISIBLE);
                        txt_status.setText(getResources().getString(R.string.ref_app));
                    }else{
                        txt_status.setVisibility(View.GONE);
                        btn_buy.setTag("0");
                    }*/
                    txt_bought_ny.setText(getResources().getString(R.string.optn_s));
                    if(data.get(position).getRefund_req().equalsIgnoreCase("1")&&data.get(position).getRefund_req_for().equalsIgnoreCase("1")
                            &&data.get(position).getRefund_approved().equalsIgnoreCase("0")){
                        String duratn = "0";
                       try{
                            duratn = method_48HourCheckUI(data.get(position).getRefund_time(),Integer.valueOf(hours_added),Integer.valueOf(days_added));
                       }catch (Exception e){
                           duratn = method_48HourCheckUI(data.get(position).getRefund_time(),Integer.valueOf(hours_added),Integer.valueOf(days_added));
                       }
                        if(duratn.equalsIgnoreCase("1")){
                            txt_status.setText(getResources().getString(R.string.rfnd_abortd));
                            txt_bought_ny.setText(getResources().getString(R.string.info));
                        }else{
//                            txt_status.setText(getResources().getString(R.string.ref_pend));
                            txt_status.setText(getResources().getString(R.string.req_sent));
                        }
                    }

                    else if(data.get(position).getRefund_req().equalsIgnoreCase("1")&&data.get(position).getRefund_req_for().equalsIgnoreCase("1")
                            &&data.get(position).getRefund_approved().equalsIgnoreCase("1")){
                        txt_status.setText(getResources().getString(R.string.ref_app));
                    }

                    else if(data.get(position).getRefund_req().equalsIgnoreCase("1")&&data.get(position).getRefund_req_for().equalsIgnoreCase("2")
                            &&data.get(position).getRefund_approved().equalsIgnoreCase("0")){
                        txt_bought_ny.setText(getResources().getString(R.string.info));
                        txt_status.setText(getResources().getString(R.string.tckt_pend));
                    }

                    else if(data.get(position).getRefund_req().equalsIgnoreCase("1")&&data.get(position).getRefund_req_for().equalsIgnoreCase("2")
                            &&data.get(position).getRefund_approved().equalsIgnoreCase("2")){
                        txt_status.setText(getResources().getString(R.string.tckt_app));
                    }

                    else if(data.get(position).getRefund_req().equalsIgnoreCase("1")&&data.get(position).getRefund_req_for().equalsIgnoreCase("2")
                            &&data.get(position).getRefund_approved().equalsIgnoreCase("3")){
                        txt_status.setText(getResources().getString(R.string.tckt_dcln));
                    }


                    btn_buy.setTag("0");
                    txt_status.setVisibility(View.VISIBLE);
                    txt_status.setTag(data.get(position).getLead_id());

                    btn_buy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(!txt_status.getText().toString().equalsIgnoreCase(getResources().getString(R.string.ref_app))){
                                if(txt_status.getText().toString().equalsIgnoreCase(getResources().getString(R.string.tckt_pend))){
                                    dialogPendingTickt();
                                }
                                else if(txt_status.getText().toString().equalsIgnoreCase(getResources().getString(R.string.tckt_app))){
                                    dialogAppoveTickt();
                                }
                                else if(txt_status.getText().toString().equalsIgnoreCase(getResources().getString(R.string.tckt_dcln))){
                                    dialogDeclineTickt();
                                }
                                else if(txt_status.getText().toString().equalsIgnoreCase(getResources().getString(R.string.rfnd_abortd))){
                                    dialogAbortTickt();
                                }else {
                                    apiRefund2Check(data.get(position).getLead_id());
                                }
                            }else{
                                dialogAppoveTickt();
                            }
                        }

                        private void dialogPendingTickt() {
                            final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_sure, null);
                            dialog.setContentView(bottomSheetView);
                            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog) {
                                    BottomSheetDialog d = (BottomSheetDialog) dialog;
                                    FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                    BottomSheetBehavior.from(bottomSheet)
                                            .setState(BottomSheetBehavior.STATE_EXPANDED);
                                }
                            });
                            TextView txt_resell = (TextView) dialog.findViewById(R.id.txt_resell);
                            TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
                            TextView txt_refund = (TextView) dialog.findViewById(R.id.txt_refund);

                            txt_title.setText(getResources().getString(R.string.u_opnd));
                            txt_resell.setText(getResources().getString(R.string.our_rfnd_team));
                            txt_refund.setVisibility(View.GONE);

                            txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_resell.setTypeface(utils.OpenSans_Regular(getActivity()));

                            if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                                txt_resell.setGravity(Gravity.RIGHT);
                            }
                            dialog.show();

                        }

                        private void dialogAppoveTickt() {
                            final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_sure, null);
                            dialog.setContentView(bottomSheetView);
                            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog) {
                                    BottomSheetDialog d = (BottomSheetDialog) dialog;
                                    FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                    BottomSheetBehavior.from(bottomSheet)
                                            .setState(BottomSheetBehavior.STATE_EXPANDED);
                                }
                            });
                            TextView txt_resell = (TextView) dialog.findViewById(R.id.txt_resell);
                            TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
                            TextView txt_refund = (TextView) dialog.findViewById(R.id.txt_refund);

                            txt_title.setText(getResources().getString(R.string.tckt_app));
                            txt_resell.setText(getResources().getString(R.string.u_appr_tckt));
                            txt_refund.setVisibility(View.GONE);

                            txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_resell.setTypeface(utils.OpenSans_Regular(getActivity()));
                            if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                                txt_resell.setGravity(Gravity.RIGHT);
                            }

                            dialog.show();

                        }

                        private void dialogAbortTickt() {
                            final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_sure, null);
                            dialog.setContentView(bottomSheetView);
                            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog) {
                                    BottomSheetDialog d = (BottomSheetDialog) dialog;
                                    FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                    BottomSheetBehavior.from(bottomSheet)
                                            .setState(BottomSheetBehavior.STATE_EXPANDED);
                                }
                            });
                            TextView txt_resell  = (TextView) dialog.findViewById(R.id.txt_resell);
                            TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
                            TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);

                            txt_resell.setGravity(Gravity.CENTER);
                            txt_title.setText(getResources().getString(R.string.rfnd_abortd));
                            txt_resell.setText(getResources().getString(R.string.u_abrtd)+" "+days_added+" "
                                    +getResources().getString(R.string.dyss));
                            txt_refund.setVisibility(View.GONE);

                            txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_resell.setTypeface(utils.OpenSans_Regular(getActivity()));
                            if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                                txt_resell.setGravity(Gravity.RIGHT);
                            }

                            dialog.show();

                        }

                        private void dialogDeclineTickt() {
                            final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_sure, null);
                            dialog.setContentView(bottomSheetView);
                            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog) {
                                    BottomSheetDialog d = (BottomSheetDialog) dialog;
                                    FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                    BottomSheetBehavior.from(bottomSheet)
                                            .setState(BottomSheetBehavior.STATE_EXPANDED);
                                }
                            });
                            TextView txt_resell  = (TextView) dialog.findViewById(R.id.txt_resell);
                            TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
                            TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);

                            txt_resell.setGravity(Gravity.LEFT);
                            if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                                txt_resell.setGravity(Gravity.RIGHT);
                            }
                            txt_title.setText(getResources().getString(R.string.tckt_dcln));
                            txt_resell.setText(getResources().getString(R.string.u_tckt_Dec));
                            txt_refund.setVisibility(View.GONE);

                            txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_resell.setTypeface(utils.OpenSans_Regular(getActivity()));
                            if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                                txt_resell.setGravity(Gravity.RIGHT);
                            }

                            dialog.show();

                        }


                        private void apiRefund2Check(String lead_id){
                            AndroidNetworking.enableLogging();
                            utils.showProgressDialog(getActivity(),getActivity().getResources().getString(R.string.load));
                            Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.CHECK_SECOND_TYM_REFUND);
                            Log.e("user_id: ", sharedPreferenceLeadr.getUserId());
                            Log.e("lead_id: ", lead_id);

                            AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_SECOND_TYM_REFUND)
                                    .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                    .addBodyParameter("lead_id",lead_id)
                                    .setTag("msg")
                                    .setPriority(Priority.HIGH).doNotCacheResponse()
                                    .build()
                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject result) {
                                            Log.e("res_refundTym1: ",result+"");
                                            //{"":"0","message":" Not available!","buy_time":"10-40-00",
                                            // "buy_date":"2018-05-15"}

                                            //{"status":"1","message":" Available!","buy_time":"08-40-00",
                                            // "buy_date":"2018-05-15"}

                                            // {"status":"0","message":" Not result found!"}
//{"status":"0","message":" Not available!","buy_time":"13-13-00","buy_date":"2018-05-23"}

                                            //{"status":"1","message":" Available!","buy_time":"12-02-08","buy_date":"2018-05-18",
                                            // "days":7,"hours":48}


                                            String time = result.optString("buy_time");


                                            utils.dismissProgressdialog();
                                            if(result.optString("message").equalsIgnoreCase("Over")){
                                                dialogAbortTickt2();
                                            }
                                            if(result.optString("message").equalsIgnoreCase("Not result found!")){
                                                dialogAbortTickt2();
                                            }
                                            else if(result.optString("message").equalsIgnoreCase("Not result found!")){
                                                if(time!=null){
                                                    if(time.trim().length()<1){
                                                        Dialog_AskFrstRefund(result.optString("status"), result.optString("buy_time"),
                                                                 "Over", result.optString("days"),
                                                                result.optString("hours"));
                                                    }else{
                                                        Dialog_AskFrstRefund(result.optString("status"), result.optString("buy_time"),
                                                                 result.optString("message"), result.optString("days"),
                                                                result.optString("hours"));
                                                    }

                                                }else{
                                                    Dialog_AskFrstRefund(result.optString("status"), result.optString("buy_time"),
                                                             "Over", result.optString("days"),
                                                            result.optString("hours"));
                                                }


                                            }else {
                                                Dialog_AskFrstRefund(result.optString("status"), result.optString("buy_time"),
                                                        result.optString("message"), result.optString("days"),
                                                        result.optString("hours"));
                                            }
                                        }
                                        @Override
                                        public void onError(ANError error) {
                                            Log.e("", "---> On error  ");
                                            utils.dismissProgressdialog();
                                        }
                                    });

                        }

                        private void Dialog_AskFrstRefund( String status, String time,  String msg,
                                                           final String date_Added, final String time_added){
                            final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            View bottomSheetView  = getActivity().getLayoutInflater().inflate(R.layout.dialog_refund_tckt, null);
                            dialog.setContentView(bottomSheetView);

                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog) {
                                    BottomSheetDialog d     = (BottomSheetDialog) dialog;
                                    FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.
                                            design_bottom_sheet);
                                    BottomSheetBehavior.from(bottomSheet)
                                            .setState(BottomSheetBehavior.STATE_EXPANDED);
                                }
                            });
                            TextView txt_chat                = (TextView) dialog.findViewById(R.id.txt_chat);
                            final TextView txt_opn_tckt      = (TextView) dialog.findViewById(R.id.txt_opn_tckt);
                            final TextView txt_title         = (TextView) dialog.findViewById(R.id.txt_title);
                            final TextView txt_issu_ref      = (TextView) dialog.findViewById(R.id.txt_issu_ref);
                            final TextView txt_ask_sellr     = (TextView) dialog.findViewById(R.id.txt_ask_sellr);


                            txt_chat.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_opn_tckt.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_issu_ref.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_ask_sellr.setTypeface(utils.OpenSans_Regular(getActivity()));

                            txt_title.setText(getResources().getString(R.string.u_askd));
                            txt_title.setAllCaps(true);
                            txt_issu_ref.setText(/*getResources().getString(R.string.we_added_1)+"\n\n"
                                    +*/getResources().getString(R.string.we_added_2)+" "+hours_added+" "
                                    +getResources().getString(R.string.we_added_3)+"\n\n"+getResources().getString(R.string.we_added_4));

                            txt_ask_sellr.setText(getResources().getString(R.string.seler_dont_1)+" "+hours_added+" "+
                            getResources().getString(R.string.seler_dont_2));

                            if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                                txt_issu_ref.setGravity(Gravity.RIGHT);
                                txt_ask_sellr.setGravity(Gravity.RIGHT);
                            }

                            txt_chat.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick( View v ) {
                                    dialog.dismiss();
                                    try{
                                        handler_timer.removeCallbacks(runnable_timer);
                                    }catch (Exception e){}
                                    Intent i_chang = new Intent(getActivity(), ChatActivity.class);
                                    i_chang.putExtra("image_other", imge_user);
                                    i_chang.putExtra("name_other", data.get(position).getUser_name());
                                    i_chang.putExtra("buss_other", data.get(position).getBusiness_name());
                                    i_chang.putExtra("info_other", data.get(position).getBuy_userbusinessinfo());
                                    i_chang.putExtra("id_other", data.get(position).getUser_id());
                                    i_chang.putExtra("leadid_other", data.get(position).getLead_id());
                                    i_chang.putExtra("lead_user_id", data.get(position).getUser_id());
                                    i_chang.putExtra("other_user_id", sharedPreferenceLeadr.getUserId());
                                    i_chang.putExtra("audio",data.get(position).getAudio());
                                    i_chang.putExtra("audiotime", data.get(position).getTime());
                                    i_chang.putExtra("desc", data.get(position).getDescription());
                                    i_chang.putExtra("typemy", "I Bought");
                                    i_chang.putExtra("typeother", "I Sell");
                                    startActivity(i_chang);
                                }
                            });


                            if(status.equals("0")){
                                try {
                                    String duration  = method_48HourCheckDynamic(time,Integer.valueOf(time_added));
                                    String duration2 = method_7Day2Check(time);
                                }catch (Exception e){}


                                if(msg.contains("Over")){
                                   /* txt_opn_tckt.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_opn_tckt.setTag("0");
                                    txt_opn_tckt.setText(getResources().getString(R.string.nine_left)+" (0"
                                            +context.getResources().getString(R.string.dy_left));*/
                                    dialog.dismiss();
                                    dialogAbortTickt2();
                                }else if(msg.contains("Not available with refund!")){
                                    String left_time = method_CalculateTimeLeftHOURS(time,time_added);
                                    txt_opn_tckt.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_opn_tckt.setTag("0");
                                    txt_opn_tckt.setText(getResources().getString(R.string.opwnew_tckt)+left_time
                                            +")");
                                }else if(msg.contains("Not available without refund!")){
                                    String left_time = method_CalculateTimeLeftHOURS(time,time_added);
                                    txt_opn_tckt.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_opn_tckt.setTag("0");
                                    txt_opn_tckt.setText(getResources().getString(R.string.opwnew_tckt)+left_time
                                            +")");
                                }else if(msg.contains("Not result found!")){
                                    int final_counter = Integer.valueOf(date_Added) - time_pending2;
                                    if(final_counter>0){
                                        txt_opn_tckt.setTextColor(getResources().getColor(R.color.gray_budget));
                                        txt_opn_tckt.setTag("0");
                                        txt_opn_tckt.setText(getResources().getString(R.string.opwnew_tckt)+" 0"
                                                +context.getResources().getString(R.string.dy_left));
                                    }else{
                                        dialog.dismiss();
                                        dialogAbortTickt2();
                                      /*  txt_opn_tckt.setTextColor(getResources().getColor(R.color.gray_budget));
                                        txt_opn_tckt.setTag("0");
                                        txt_opn_tckt.setText(getResources().getString(R.string.no_day));*/
                                    }
                                }
                                else if(time_pending==0){
                                    txt_opn_tckt.setTag("0");
                                    txt_opn_tckt.setTextColor(getResources().getColor(R.color.gray_budget));
                                    String time_left = method_CalculateTimeLeftHOURS(time,time_added);
                                    txt_opn_tckt.setText(getResources().getString(R.string.opwn_tckt)+" "+
                                            time_left+")"
                                            /*+context.getResources().getString(R.string.hr)*/);
                                }else if(msg.equalsIgnoreCase("Not available!")){
                                    int final_counter = Integer.valueOf(time_added) - time_pending;
                                    txt_opn_tckt.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_opn_tckt.setTag("0");
                                    txt_opn_tckt.setText(getResources().getString(R.string.opwn_tckt)+" "+
                                            context.getResources().getString(R.string.in)+final_counter
                                            +context.getResources().getString(R.string.hrs));
                                }
                            }else{
                                String duration = method_CalculateTimeLeftDAYS(time,days_added);
                                int final_counter = Integer.valueOf(date_Added) - time_pending;
                                txt_opn_tckt.setTag("1");
                                txt_opn_tckt.setText(getResources().getString(R.string.opwnew_tckt)+" "+duration
                                        +context.getResources().getString(R.string.dy_left));
                            }

                            txt_opn_tckt.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick( View v ) {
                                    if(txt_opn_tckt.getTag().equals("1")){

                                        api_refund("2");
                                    }
                                }

                                private void api_refund(String type){
                                    AndroidNetworking.enableLogging();
                                    utils.showProgressDialog(getActivity(),context.getResources().getString(R.string.load));
                                    Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.REFUND);
                                    Log.e("user_id: ", sharedPreferenceLeadr.getUserId());
                                    Log.e("lead_id: ", data.get(position).getLead_id());
                                    Log.e("lead_user_id: ", data.get(position).getUser_id());
                                    Log.e("req_for: ", type);

                                    AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.REFUND)
                                            .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                            .addBodyParameter("lead_id",txt_status.getTag().toString())
                                            .addBodyParameter("lead_user_id",data.get(position).getUser_id())
                                            .addBodyParameter("lead_price",data.get(position).getLead_price())
                                            .addBodyParameter("req_for",type)
                                            .setTag("msg")
                                            .setPriority(Priority.HIGH).doNotCacheResponse()
                                            .build()
                                            .getAsJSONObject(new JSONObjectRequestListener() {
                                                @Override
                                                public void onResponse(JSONObject result) {
                                                    Log.e("res_chkCard: ",result+"");
                                                    utils.dismissProgressdialog();
                                                    dialog.dismiss();
                                                    AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                                                    logger.logEvent("EVENT_NAME_BOUGHT_ASKED_REFUND");
                                                    if(result.optString("status").equalsIgnoreCase("1")){
                                                        try{
                                                            handler_timer.removeCallbacks(runnable_timer);
                                                        }catch (Exception e){}
                                                        swipeRight();
                                                         Intent i_nxt = new Intent(getActivity(), FeedbackActivity.class);
                                                         i_nxt.putExtra("refund","refund");
                                                         i_nxt.putExtra("user_id",data.get(position).getUser_id());
                                                         i_nxt.putExtra("leadid",data.get(position).getLead_id());
                                                         startActivity(i_nxt);
                                                    }else{
                                                        utils.dialog_msg_show(getActivity(),result.optString("message"));
                                                    }

                                                }
                                                @Override
                                                public void onError(ANError error) {
                                                    Log.e("", "---> On error  ");
                                                    utils.dismissProgressdialog();
                                                }
                                            });

                                }

                            });


                            dialog.show();
                        }
                        private void dialogAbortTickt2() {
                            final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            View bottomSheetView  = getLayoutInflater().inflate(R.layout.dialog_bought_sure, null);
                            dialog.setContentView(bottomSheetView);
                            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog) {
                                    BottomSheetDialog d = (BottomSheetDialog) dialog;
                                    FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                    BottomSheetBehavior.from(bottomSheet)
                                            .setState(BottomSheetBehavior.STATE_EXPANDED);
                                }
                            });
                            TextView txt_resell  = (TextView) dialog.findViewById(R.id.txt_resell);
                            TextView txt_title   = (TextView) dialog.findViewById(R.id.txt_title);
                            TextView txt_refund  = (TextView) dialog.findViewById(R.id.txt_refund);

                            txt_resell.setGravity(Gravity.CENTER);
                            txt_title.setText(getResources().getString(R.string.rfnd_abortd));
                            txt_resell.setText(getResources().getString(R.string.u_abrtd)+" "+days_added+" "
                                    +getResources().getString(R.string.dyss));
                            txt_refund.setVisibility(View.GONE);

                            txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                            txt_resell.setTypeface(utils.OpenSans_Regular(getActivity()));
                            if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
                                txt_resell.setGravity(Gravity.RIGHT);
                            }

                            dialog.show();

                        }
                    });


                    img_chat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}
                            Intent i_chang = new Intent(getActivity(), ChatActivity.class);
                            i_chang.putExtra("image_other", imge_user);
                            i_chang.putExtra("name_other", data.get(position).getUser_name());
                            i_chang.putExtra("buss_other", data.get(position).getBusiness_name());
                            i_chang.putExtra("info_other", data.get(position).getBuy_userbusinessinfo());
                            i_chang.putExtra("id_other", data.get(position).getUser_id());
                            i_chang.putExtra("leadid_other", data.get(position).getLead_id());
                            i_chang.putExtra("lead_user_id", data.get(position).getUser_id());
                            i_chang.putExtra("other_user_id", sharedPreferenceLeadr.getUserId());
                            i_chang.putExtra("audio",data.get(position).getAudio());
                            i_chang.putExtra("audiotime", data.get(position).getTime());
                            i_chang.putExtra("desc", data.get(position).getDescription());
                            i_chang.putExtra("typemy", "I Bought");
                            i_chang.putExtra("typeother", "I Sell");
                            startActivity(i_chang);

                          /*  Intent i_chang = new Intent(getActivity(), ChatActivity.class);
                            i_chang.putExtra("image_other",imge_user);
                            i_chang.putExtra("name_other",data.get(position).getUser_name());
                            i_chang.putExtra("buss_other",data.get(position).getBusiness_name());
                            i_chang.putExtra("info_other",data.get(position).getBuy_userbusinessinfo());
                            i_chang.putExtra("id_other",data.get(position).getUser_id());
                            i_chang.putExtra("leadid_other",data.get(position).getLead_id());
                            i_chang.putExtra("type","buy");
                            startActivity(i_chang);*/
                        }
                    });

                    lnr_pro.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}

                            Intent i_pro = new Intent(getActivity(), ProfileOther_Activity.class);
                            i_pro.putExtra("id",data.get(position).getUser_id());
                            startActivity(i_pro);
                        }
                    });


                    txt_pause_audio.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            lnr_11.performClick();
                        }
                    });

                    lnr_11.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            lnr_11.setVisibility(View.GONE);
                            lnr_11.setClickable(false);
                            lnr_22.setVisibility(View.VISIBLE);
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}


                            songProgressBar.setProgress(0);
                            if(txt_play_pause.getTag().equals("0")){
                                progress_one.setVisibility(View.VISIBLE);
                                progress_two.setVisibility(View.VISIBLE);
                                img_one.setVisibility(View.GONE);
                                img_two.setVisibility(View.GONE);
                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        progress_one.setVisibility(View.GONE);
                                        progress_two.setVisibility(View.INVISIBLE);
                                        img_one.setVisibility(View.VISIBLE);
                                        img_two.setVisibility(View.VISIBLE);
                                    }

                                }, 1000);
                                txt_play_pause.setTag("1");
                                onPlay(false);
                            }else{
                                txt_play_pause.setTag("0");
                                onPlay(true);
                            }
                        }
                        //
                        private void onPlay(boolean isPlaying2){
                            if (!isPlaying2) {
                                //currently MediaPlayer is not playing audio
                                if(mMediaPlayer == null) {
                                    txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
                                    startPlaying(); //start from beginning
                                    isPlaying = !isPlaying;
                                } else {
                                    txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
                                    resumePlaying(); //resume the currently paused MediaPlayer
                                }
                            } else {
                                //pause the MediaPlayer
                                txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pause_sell, 0, 0, 0);
                                pausePlaying();
                            }
                        /*lnr_11.setVisibility(View.GONE);
//                        lnr_11.setClickable(false);
                        lnr_22.setVisibility(View.VISIBLE);*/
                        }

                        private void startPlaying() {
                            mMediaPlayer = new MediaPlayer();
                            try {
                                mMediaPlayer.setDataSource(data.get(position).getAudio());
                                mMediaPlayer.prepare();

                                count_start_pause_add = 0;
                                count_start_pause = Integer.valueOf(data.get(position).getTime())+2;

                                songProgressBar.setMax(mMediaPlayer.getDuration());
                                songProgressBar.setProgress(0);
                                mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        mMediaPlayer.start();
                                    }
                                });
                            } catch (IOException e) {
                                Log.e("", "prepare() failed");
                            }
                            count_start_pause_add = 0;
                            method_countDown(Integer.valueOf(data.get(position).getTime())+1);
                            method_countDown_Seekbar();

                            mMediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                                @Override
                                public void onSeekComplete(MediaPlayer mp) {

                                }
                            });
                            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    stopPlaying();
                                }
                            });

                            //keep screen on while playing audio
                            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        }

                        void method_countDown_Seekbar(){
                            mHandler_seekbar = new Handler();
                            mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);

                        }

                         Runnable mRunnabl_seekbar = new Runnable() {
                            @Override
                            public void run() {
                                if(mMediaPlayer != null){
//                Log.e("seejbr: ",mCurrentPosition+"");
                                    Log.e("seejbr2: ",mMediaPlayer.getCurrentPosition()+"");
                                    songProgressBar.setProgress(mMediaPlayer.getCurrentPosition());
                                }
                                mHandler_seekbar.postDelayed(this, 15);
                            }
                        };


                        void method_countDown(int timer){
                            countDownTimer = null;
                            countDownTimer = new CountDownTimer(timer*1000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    long millis = millisUntilFinished;
                                    //Convert milliseconds into hour,minute and seconds
                                    String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) -
                                                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                            TimeUnit.MILLISECONDS.toSeconds(millis) -
                                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

                                    txt_play_pause.setText(hms+"");

                                    count_start_pause = count_start_pause-1;
                                    count_start_pause_add = count_start_pause_add+1;

//                                songProgressBar.setProgress(count_start_pause_add);
                                    if(count_start_pause==1){
//                                    songProgressBar.setProgress(count_start_pause_add);
                                        stopPlaying();
                                    }
                                }
                                public void onFinish() {
                                    countDownTimer = null;//set CountDownTimer to null
                                }
                            }.start();
                        }

                        private void updateSeekBar() {
                            mHandler.postDelayed(mRunnable, 1000);
                        }

                        private void pausePlaying() {
                            mHandler.removeCallbacks(mRunnable);
                            resume_pos = mMediaPlayer.getCurrentPosition();
                            countDownTimer.cancel();
                            mMediaPlayer.pause();

                            if(mHandler_seekbar!=null){
                                mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                            }
                        }

                        private void resumePlaying() {
//        mMediaPlayer.seekTo(resume_pos);
                            mHandler.removeCallbacks(mRunnable);
                            mMediaPlayer.start();
                            updateSeekBar();
                            method_countDown(count_start_pause-1);
                            mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);
//        updateSeekBar();
                        }

                        private void stopPlaying() {
                            lnr_11.setClickable(true);
                            mHandler.removeCallbacks(mRunnable);
                            mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                            mMediaPlayer.stop();
                            mMediaPlayer.reset();
                            mMediaPlayer.release();
                            mMediaPlayer = null;
                            resume_pos = 0;
                            lnr_11.setVisibility(View.VISIBLE);
                            lnr_22.setVisibility(View.GONE);

                            isPlaying = !isPlaying;
//                        songProgressBar.setProgress(songProgressBar.getMax());

                            if (countDownTimer != null) {
                                countDownTimer.cancel();
                            }
                            txt_play_pause.setTag("0");
                            //allow the screen to turn off again once audio is finished playing
                            if (getActivity() != null) {
                                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                            }

                            try{
                                handler_timer.postDelayed(runnable_timer, 40000);
                            }catch (Exception e){}
                        }

                        //updating mSeekBar
                        private Runnable mRunnable = new Runnable() {
                            @Override
                            public void run() {
                                if(mMediaPlayer != null){

                                    int mCurrentPosition = mMediaPlayer.getCurrentPosition();
                                    long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);
                                    long seconds = TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition)
                                            - TimeUnit.MINUTES.toSeconds(minutes);
                                    updateSeekBar();
                                }
                            }
                        };
                    });
                }else{
                    frame_main.setVisibility(View.GONE);
                    rel_no_show.setVisibility(View.VISIBLE);
                }
            }

            return view;
        }

    }


    String method_GetDuration( final String created_time){
        String date_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if(sub_year.equals("1")){
                date_return = getActivity().getResources().getString(R.string.few_mnth);
            }
            else if(sub_year.equals("2")){
                date_return = getActivity().getResources().getString(R.string.year_ago);
            }
            else if(Integer.valueOf(dayDifference)>0){
                if(dayDifference.equals("1")){
                    date_return = dayDifference+getActivity().getResources().getString(R.string.day);
                }else{
                    date_return = dayDifference+getActivity().getResources().getString(R.string.days);
                }
            }else{
                int hours = (int) (mills / (1000 * 60 * 60));
                int minutes = (int) (mills / (1000 * 60));
                int seconds = (int) (mills / (1000));
                if(hours>0){
                    date_return = hours+" "+getResources().getString(R.string.hr);
                }else if(minutes>0){
                    date_return = minutes+" "+getResources().getString(R.string.min);
                }else{
                    date_return = getActivity().getResources().getString(R.string.few_sec_full);
                }
            }

        } catch (Exception exception) {
            date_return = getActivity().getResources().getString(R.string.fewdays);
        }
        return date_return;
    }

    private String getDate1(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);

        }
        catch (Exception e)
        {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }




    private String getOnlyDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);

        }
        catch (Exception e)
        {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }


    String method_48HourCheck( final String created_time){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if(sub_year.equals("1")){
                duration_return = "1";
            }
            else if(sub_year.equals("2")){
                duration_return = "1";
            }
            else if(Integer.valueOf(dayDifference)>0){
                if(dayDifference.equals("1")){
                    time_pending = 24;
                    duration_return = "0";

                }else{
                    int hours = (int) (mills / (1000 * 60 * 60));
                    int minutes = (int) (mills / (1000 * 60));
                    int seconds = (int) (mills / (1000));
                    if(hours>0){
                        duration_return = hours+" "+getResources().getString(R.string.hr);
                        if(hours<48){
                            duration_return = "0";
                            time_pending = hours;
                        }else if(hours == 48){
                            if(minutes>0){
                                duration_return = "1";
                            }else{
                                duration_return = "0";
                                time_pending = hours;
                            }
                        }else{
                            duration_return = "1";
                        }
                    }else if(minutes>0){
                        duration_return = "0";
                        time_pending = 0;
                    }else{
                        duration_return = "0";
                        time_pending = 0;
                    }
                }
            }else{
                int hours = (int) (mills / (1000 * 60 * 60));
                int minutes = (int) (mills / (1000 * 60));
                int seconds = (int) (mills / (1000));
                if(hours>0){
                    duration_return = hours+" "+getResources().getString(R.string.hr);
                    if(hours<48){
                        time_pending = hours;
                        duration_return = "0";
                    }else if(hours == 48){
                        time_pending = 48;
                        if(minutes>0){
                            duration_return = "1";
                        }else{
                            duration_return = "0";
                        }
                    }else{
                        time_pending = hours;
                        duration_return = "1";
                    }
                }else if(minutes>0){
                    time_pending = 0;
                    duration_return = "0";
                }else{
                    time_pending = 0;
                    duration_return = "0";
                }
            }


        } catch (Exception exception) {
        }



        return  duration_return;
    }


    String method_CalculateTimeLeft( final String created_time){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if(sub_year.equals("1")){
                duration_return = "1 YEAR";
            }
            else if(sub_year.equals("2")){
                duration_return = "2 YEAR";
            }
            else if(Integer.valueOf(dayDifference)>0){
                duration_return = dayDifference+getResources().getString(R.string.d);
            }else{
                int hours = (int) (mills / (1000 * 60 * 60));
                int minutes = (int) (mills / (1000 * 60));
                int seconds = (int) (mills / (1000));
                if(hours>0){
                    duration_return = hours+ " "+getResources().getString(R.string.hr);
                }else if(minutes>0){
                    duration_return = minutes+" "+getResources().getString(R.string.min);
                }else{
                    duration_return = seconds+" "+getResources().getString(R.string.min);
                }
            }


        } catch (Exception exception) {
        }



        return  duration_return;
    }


    String method_CalculateTimeLeftHOURS( final String created_time,String hour_backend){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if(sub_year.equals("1")){
                String sub = String.valueOf(Integer.valueOf(hour_backend)-8760);
                duration_return = sub+" YEAR";
            }
            else if(sub_year.equals("2")){
                String sub = String.valueOf(Integer.valueOf(hour_backend)-8760);
                duration_return = sub+" YEAR";
            }
            else if(Integer.valueOf(dayDifference)>0){
                String sub = String.valueOf(Integer.valueOf(hour_backend)-Integer.valueOf(dayDifference)*24);
                duration_return = sub+" D";
//                duration_return = dayDifference+" D";
            }else{
                int hours = (int) (mills / (1000 * 60 * 60));
                int minutes = (int) (mills / (1000 * 60));
                int seconds = (int) (mills / (1000));
                if(hours>0){
                    String sub = String.valueOf(Integer.valueOf(hour_backend)-hours);
                    duration_return = sub+ " "+getResources().getString(R.string.hr);
                }else if(minutes>0) {
                    String sub = String.valueOf(Integer.valueOf(hour_backend) * 60 - minutes);
                    if (Integer.valueOf(hour_backend)*60 > 60){
                        duration_return = hour_backend+" "+getResources().getString(R.string.hr);
                        }else {
                        duration_return = sub + " "+getResources().getString(R.string.min);
                    }
                }else{
                    String sub = String.valueOf(Integer.valueOf(hour_backend)-seconds);
                    duration_return = hour_backend+" "+getResources().getString(R.string.hr);
                }
            }


        } catch (Exception exception) {
        }



        return  duration_return;
    }


    String method_CalculateTimeLeftDAYS( final String created_time, final String day){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if(Integer.valueOf(dayDifference)>0){
                duration_return = String.valueOf(Integer.valueOf(day)-Integer.valueOf(dayDifference))+getResources().getString(R.string.d);
            }
           else{
                int hours = (int) (mills / (1000 * 60 * 60));
                int minutes = (int) (mills / (1000 * 60));
                int seconds = (int) (mills / (1000));
                if(hours>0){
                    duration_return = hours+ getResources().getString(R.string.hr);
                }else if(minutes>0) {
                        duration_return = minutes +getResources().getString(R.string.min);
                }else{
                    duration_return = seconds+getResources().getString(R.string.secs);
                }
            }


        } catch (Exception exception) {
        }



        return  duration_return;
    }


    String method_48HourCheckDynamic( final String created_time, final Integer hour){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);


            int hours = (int) (mills / (1000 * 60 * 60));
            int minutes = (int) (mills / (1000 * 60));
            int seconds = (int) (mills / (1000));
            if(hours>0){
                duration_return = hours+" "+getResources().getString(R.string.hr);
                if(hours<Integer.valueOf(hour)){
                    duration_return = "0";
                    time_pending = hours;
                }else if(hours == Integer.valueOf(hour)){
                    if(minutes>0){
                        duration_return = "1";
                    }else{
                        duration_return = "0";
                        time_pending = hours;
                    }
                }else{
                    duration_return = "1";
                }
            }else if(minutes>0){
                duration_return = "0";
                time_pending = 0;
            }else{
                duration_return = "0";
                time_pending = 0;
            }
        } catch (Exception exception) {
        }



        return  duration_return;
    }


    String method_7Day1Check( final String created_time){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();//yyyy-MM-dd HH-mm-ss
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);


             if(Integer.valueOf(dayDifference)>0){
                time_pending = Integer.valueOf(dayDifference);
                duration_return = "1";
            }else{
                time_pending = 0;
                duration_return = "1";
            }

        } catch (Exception exception) {
        }
        return  duration_return;
    }


    String method_7Day2Check( final String created_time){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();//yyyy-MM-dd HH-mm-ss
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if(sub_year.equals("1")){
                duration_return = "1";
            }
            else if(sub_year.equals("2")){
                duration_return = "1";
            }
            else if(Integer.valueOf(dayDifference)>0){
                time_pending2 = Integer.valueOf(dayDifference);
                duration_return = "1";
            }else{
                time_pending2 = 0;
                duration_return = "1";
            }

        } catch (Exception exception) {
        }
        return  duration_return;
    }


    String method_7DayCheck( final String created_time, final String hour, final String day){

        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate =  CurrentDate();//yyyy-MM-dd HH-mm-ss
            String CurrentTime =  CurrentTime();
            String FinalDate   =  getOnlyDate(created_time);
            String get_time    =  created_time;
            String FinalTime   =  getDate1(get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1   = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2   = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            //Comparing dates
            long difference      =   Math.abs(date1.getTime() - date2.getTime());
            long differenceDates =   difference / (24 * 60 * 60 * 1000);
            long mills           =   date2_T.getTime()-date1_T.getTime();

            String api_year  = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year  = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if(sub_year.equals("1")){
                duration_return = "1";
            }
            else if(sub_year.equals("2")){
                duration_return = "1";
            }
            else if(Integer.valueOf(dayDifference)>0){
                time_pending2 = Integer.valueOf(dayDifference);
                duration_return = "1";
            }else{
                time_pending2 = 0;
                duration_return = "1";
            }

        } catch (Exception exception) {
        }
        return  duration_return;
    }


    String method_48HourCheckUI( final String created_time,Integer hour,Integer days_added){
        String date_tosend = "";
        String duration_return = "";
        try {
            //Dates to compare
            String CurrentDate=  CurrentDate();
            String CurrentTime=  CurrentTime();
            String FinalDate=  getOnlyDate(created_time);
            String get_time =  created_time;
            String FinalTime=  getDate1(get_time);

            Date date1;
            Date date1_T;
            Date date2;
            Date date2_T;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date1_T = times.parse(FinalTime);
            date2 = dates.parse(FinalDate);
            date2_T = times.parse(CurrentTime);

            int days_added1 = days_added*24;
            days_added1 = days_added1+hour;
            Log.e("tot: ",days_added1+"");
            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            long mills =   date2_T.getTime()-date1_T.getTime();

            int hour2s = (int) (mills / (1000 * 60 * 60));
            int minutes2 = (int) (mills / (1000 * 60));
            int seconds2 = (int) (mills / (1000));

            if(days_added1==hour2s){
                if(minutes2>0){
                    hour2s =hour2s+1;
                }else if(seconds2>0){
                    hour2s =hour2s+1;
                }
            }
            String api_year = FinalDate.split("-")[0];
            String curr_year = CurrentDate.split("-")[0];
            String sub_year = "0";

            try{
                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
            }catch (Exception e){}

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);
            if(days_added1<hour2s){
                duration_return = "1";
            }
            else if(sub_year.equals("1")){
                duration_return = "1";
            }
            else if(sub_year.equals("2")){
                duration_return = "1";
            }
            else if(Integer.valueOf(dayDifference)>0){
                if(dayDifference.equals("1")){
                    duration_return = "0";

                } else if(dayDifference.equals("2")){
                    duration_return = "0";

                }else if(Integer.valueOf(dayDifference)>2){
                    duration_return = "1";

                }else{
                    int hours = (int) (mills / (1000 * 60 * 60));
                    int minutes = (int) (mills / (1000 * 60));
                    int seconds = (int) (mills / (1000));
                    if(hours>0){
                        duration_return = hours+" "+getResources().getString(R.string.hr);
                        if(hours<hour){
                            duration_return = "0";
                        }else if(hours == hour){
                            if(minutes>0){
                                duration_return = "1";
                            }else{
                                duration_return = "0";
                            }
                        }else{
                            duration_return = "1";
                        }
                    }else if(minutes>0){
                        duration_return = "0";
                    }else{
                        duration_return = "0";
                    }
                }
            }else{
                duration_return= "0";
            }

        } catch (Exception exception) {
        }
        return  duration_return;
    }

    String CurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        //2018-05-30 17-49-07
        return formattedDate;
    }

    String CurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        //2018-05-30 17-49-07
        return formattedDate;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions){
            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)){
                //denied
                utils.hideKeyboard(getActivity());
                dialog_denyOne();
            }else{
                if(ActivityCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED){
                    //allowed
                    try{
                        handler_timer.removeCallbacks(runnable_timer);
                    }catch (Exception e){}

                    utils.hideKeyboard(getActivity());
                    AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                    logger.logEvent("EVENT_NAME_BOUGHT_CALLED_CLIENT");

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+phone_no));
                    startActivity(callIntent);
                } else{
                    utils.hideKeyboard(getActivity());
                    dialog_openStoragePer();
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //do something here.
                }
            }
        }
    }



    public  void dialog_denyOne(){
        final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);

        txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_ok.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_camera.setTypeface(utils.OpenSans_Regular(getActivity()));

        txt_title.setText(getResources().getString(R.string.allow_phone));
        txt_camera.setText(getActivity().getResources().getString(R.string.text_call));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public  void dialog_openStoragePer(){
        final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish   = (Button) dialog.findViewById(R.id.btn_finish);
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_press  = (TextView) dialog.findViewById(R.id.txt_press);
        TextView txt_store  = (TextView) dialog.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
        btn_finish.setTypeface(utils.OpenSans_Bold(getActivity()));
        txt_ok.setTypeface(utils.OpenSans_Bold(getActivity()));
        txt_camera.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_press.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_store.setTypeface(utils.OpenSans_Regular(getActivity()));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{
                    handler_timer.removeCallbacks(runnable_timer);
                }catch (Exception e){}

                GlobalConstant.startInstalledAppDetailsActivity(getActivity());
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void swipeRight() {
        if(cardStack!=null){
            List<BuyPojo> spots = extractRemainingTouristSpots();
            if (spots.isEmpty()) {
                return;
            }

            View target = cardStack.getTopView();
            ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                    target, PropertyValuesHolder.ofFloat("rotation", 10f));
            rotation.setDuration(200);
            ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                    target, PropertyValuesHolder.ofFloat("translationX", 0f, 2000f));
            ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                    target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
            translateX.setStartDelay(100);
            translateY.setStartDelay(100);
            translateX.setDuration(500);
            translateY.setDuration(500);
            AnimatorSet set = new AnimatorSet();
            set.playTogether(rotation, translateX, translateY);

            cardStack.swipe(SwipeDirection.Right, set);

            try{
                handler_timer.postDelayed(runnable_timer, 40000);
            }catch (Exception e){}
        }
    }


    private ArrayList<BuyPojo> extractRemainingTouristSpots() {
        ArrayList<BuyPojo> spots = new ArrayList<>();
        if(type_to_send.equalsIgnoreCase("1")){
            for (int i = cardStack.getTopIndex(); i < adapter_refund.getCount(); i++) {
                spots.add((BuyPojo) adapter_refund.getItem(i));
            }
        }else {
            for (int i = cardStack.getTopIndex(); i < adapter.getCount(); i++) {
                spots.add((BuyPojo) adapter.getItem(i));
            }
        }

        return spots;
    }


    public  void dialog_CardFrstTYm(Activity activity, String msg){
        dialog_card = new BottomSheetDialog (activity);
        dialog_card.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog_card.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog_card.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog_card.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog_card.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog_card.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        txt_title.setText(getResources().getString(R.string.pls_wait));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog_card.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_card.dismiss();
            }
        });
        dialog_card.show();


    }


    public  void dialog_LongLead(Activity activity, String msg, String title){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_long_txt_popup, null);
        dialog.setContentView(bottomSheetView);
        dialog.setCancelable(true);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);

                behavior.setPeekHeight(bottomSheet.getHeight());
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });


        TextView txt_msg   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog.findViewById(R.id.txt_ok);

        assert txt_msg != null;
        txt_msg.setText(msg);

        txt_msg.setMovementMethod(new ScrollingMovementMethod());

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_title.setText(title);
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        txt_msg.setGravity(Gravity.LEFT);
       dialog.setOnDismissListener(new OnDismissListener() {
           @Override
           public void onDismiss( DialogInterface dialog ) {
               handler_timer.postDelayed(runnable_timer,40000);
           }
       });
        try{
            dialog.show();
        }catch (Exception e){
        }
    }
}
