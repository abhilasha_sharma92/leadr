package fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import leadr.com.leadr.R;
import leadr.com.leadr.activity.FeedbackActivity;
import leadr.com.leadr.activity.GuideActivity;
import leadr.com.leadr.activity.MySell_ListActivity;
import leadr.com.leadr.activity.PaymentMenuActivity;
import leadr.com.leadr.activity.ProfileActivity;
import leadr.com.leadr.activity.SettingActivity;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;

/**
 * Created by Abhilasha on 5/12/2017.
 */

public class MoreMenuFragment extends Fragment {

    Unbinder unbinder;

    @BindView(R.id.lnr_payment)
    LinearLayout lnr_payment;

    @BindView(R.id.lnr_setting)
    LinearLayout lnr_setting;

    @BindView(R.id.lnr_mysell)
    LinearLayout lnr_mysell;

    @BindView(R.id.rel_pro)
    RelativeLayout rel_pro;

    @BindView(R.id.txt_mysell)
    TextView txt_mysell;

    @BindView(R.id.txt_pro)
    TextView txt_pro;

    @BindView(R.id.txt_paymnt)
    TextView txt_paymnt;

    @BindView(R.id.txt_ask_adv)
    TextView txt_ask_adv;

    @BindView(R.id.txt_sett)
    TextView txt_sett;

    @BindView(R.id.btn_feature)
    Button btn_feature;

    @BindView(R.id.btn_dontlyk)
    Button btn_dontlyk;

    @BindView(R.id.btn_somethng_else)
    Button btn_somethng_else;

    @BindView(R.id.btn_bug)
    Button btn_bug;

    @BindView(R.id.txt_percnt)
    TextView txt_percnt;

    @BindView(R.id.lnr_guide)
    LinearLayout lnr_guide;

    @BindView(R.id.circularProgressBar)
    CircularProgressBar circularProgressBar;

    @BindView(R.id.vw_rate)
    View vw_rate;

    @BindView(R.id.lnr_rate)
    LinearLayout lnr_rate;

    @BindView(R.id.txt_rateus)
    TextView txt_rateus;


    GlobalConstant        utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;



    public MoreMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_more_menu
                , container, false);

        // bind view using butter knife
        unbinder              = ButterKnife.bind(this, view);
        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(getActivity());

        txt_mysell.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_pro.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_paymnt.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_sett.setTypeface(utils.OpenSans_Regular(getActivity()));
        btn_feature.setTypeface(utils.OpenSans_Regular(getActivity()));
        btn_dontlyk.setTypeface(utils.OpenSans_Regular(getActivity()));
        btn_somethng_else.setTypeface(utils.OpenSans_Regular(getActivity()));
        btn_bug.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_ask_adv.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_percnt.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_rateus.setTypeface(utils.OpenSans_Regular(getActivity()));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        methodCalculatePercnt();
    }

    private void methodCalculatePercnt() {
        int animationDuration = 2500; // 2500ms = 2,5s
        int tot_perc = 0;
        if(!sharedPreferenceLeadr.get_username().equalsIgnoreCase("")){
            tot_perc = Integer.valueOf(getResources().getString(R.string.prcnt_fullnm));
        }  if(!sharedPreferenceLeadr.get_jobTitle().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_job_tit));
        }  if(!sharedPreferenceLeadr.get_PROFILE_THUMB().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_userimg));
        } if(!sharedPreferenceLeadr.get_bus_name().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_busnm));
        }if(!sharedPreferenceLeadr.get_bus_site().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_bussite));
        }if(!sharedPreferenceLeadr.get_bus_img().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_busimg));
        }if(!sharedPreferenceLeadr.get_bus_info().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_businfo));
        }if(!sharedPreferenceLeadr.get_CATEGORY().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_buscat));
        }if(!sharedPreferenceLeadr.get_LOC_NAME().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_busloc));
        }if(!sharedPreferenceLeadr.get_EMAIL_REG().equalsIgnoreCase("")){
            tot_perc = tot_perc+Integer.valueOf(getResources().getString(R.string.prcnt_email));
        }
        circularProgressBar.setProgressWithAnimation(tot_perc, animationDuration);
        txt_percnt.setText(tot_perc+"%");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // unbind the view to free some memory
        unbinder.unbind();
    }



    @OnClick(R.id.lnr_payment)
    public void onPaymentClick() {
        Intent i_nxt = new Intent(getActivity(), PaymentMenuActivity.class);
        startActivity(i_nxt);
//        getActivity().overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
    }

    @OnClick(R.id.rel_pro)
    public void onProfileClick() {
        Intent i_nxt = new Intent(getActivity(), ProfileActivity.class);
        startActivity(i_nxt);
//        getActivity().overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
    }

    @OnClick(R.id.lnr_mysell)
    public void onMysellClick() {
        Intent i_nxt = new Intent(getActivity(), MySell_ListActivity.class);
        startActivity(i_nxt);
    }

    @OnClick(R.id.btn_feature)
    public void onFeedbckFeature() {
        Intent i_nxt = new Intent(getActivity(), FeedbackActivity.class);
        i_nxt.putExtra("feedback","feature");
        startActivity(i_nxt);
    }


    @OnClick(R.id.btn_dontlyk)
    public void onFeedbckDonte() {
        Intent i_nxt = new Intent(getActivity(), FeedbackActivity.class);
        i_nxt.putExtra("like","like");
        startActivity(i_nxt);
    }



    @OnClick(R.id.btn_somethng_else)
    public void onFeedbckDsomethng() {
        Intent i_nxt = new Intent(getActivity(), FeedbackActivity.class);
        i_nxt.putExtra("somethng","somethng");
        startActivity(i_nxt);
//        Toast.makeText(getActivity(),"In progress",Toast.LENGTH_LONG).show();
    }


    @OnClick(R.id.btn_bug)
    public void onFeedbckBug() {
        Intent i_nxt = new Intent(getActivity(), FeedbackActivity.class);
        i_nxt.putExtra("bug","bug");
        startActivity(i_nxt);
    }


    @OnClick(R.id.lnr_guide)
    public void onGuide() {
        Intent i_phn_screen = new Intent(getContext(), GuideActivity.class);
        i_phn_screen.putExtra("type","guide");
        startActivity(i_phn_screen);
    }

    @OnClick(R.id.lnr_setting)
    public void onLogout() {
       /* AccountKit.logOut();
        sharedPreferenceLeadr.clearPreference();
        Intent i_nxt = new Intent(getActivity(), GetStartedActivity.class);
        startActivity(i_nxt);
        getActivity().overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
        getActivity().finish();*/

        Intent i_nxt = new Intent(getActivity(), SettingActivity.class);
        startActivity(i_nxt);
//        getActivity().overridePendingTransition(R.anim.enter_in, R.anim.enter_out);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

}
