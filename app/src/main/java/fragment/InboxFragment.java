package fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import adapter.CallLog_Adapter;
import adapter.Inbox_Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import leadr.com.leadr.R;
import leadr.com.leadr.activity.CallLogActivity;
import leadr.com.leadr.activity.ChatActivity;
import leadr.com.leadr.activity.GetStartedActivity;
import leadr.com.leadr.activity.MainActivity;
import leadr.com.leadr.activity.MySellActivity;
import leadr.com.leadr.activity.PaymentMenuActivity;
import leadr.com.leadr.activity.ProfileActivity;
import leadr.com.leadr.activity.SellActivity;
import leadr.com.leadr.activity.SettingActivity;
import modal.BuyPojo;
import modal.InboxPojo;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkStateReceiver;
import utils.NetworkStateReceiver.NetworkStateReceiverListener;
import utils.NetworkingData;

/**
 * Created by Abhilasha on 2/7/2018.
 */

public class InboxFragment extends Fragment implements NetworkStateReceiverListener {

    Unbinder unbinder;

    @BindView(R.id.recycl_inbox)
    RecyclerView recycl_inbox;

    @BindView(R.id.rel_no_show)
    RelativeLayout rel_no_show;

    @BindView(R.id.txt_noentry_nxt)
    TextView txt_noentry_nxt;

    @BindView(R.id.txt_noentry)
    TextView txt_noentry;

    GlobalConstant        utils;
    SharedPreferenceLeadr sharedPreferenceLeadr;
    Inbox_Adapter         adapter;

    ArrayList<InboxPojo> arr_inbox = new ArrayList<>();

    NetworkStateReceiver networkStateReceiver;
    BottomSheetDialog    dialog_internet;

    public InboxFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_inbox
                , container, false);

        // bind view using butter knife
        unbinder = ButterKnife.bind(this, view);
        utils = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(getActivity());

        txt_noentry_nxt.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_noentry.setTypeface(utils.OpenSans_Regular(getActivity()));

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mYourBroadcastReceiver,
                new IntentFilter("thisIsForMyFragment"));

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        getActivity().registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if(utils.isNetworkAvailable(getActivity())){
            if(ChatActivity.inside_chat==null) {

            }
            utils.showProgressDialog_Cancelable(getActivity(), getResources().getString(R.string.load));
            api_inbox();

        }else{
            utils.dialog_msg_show(getActivity(),getResources().getString(R.string.no_internet));
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mYourBroadcastReceiver);
        networkStateReceiver.removeListener(this);
        getActivity().unregisterReceiver(networkStateReceiver);
    }


    private void api_inbox(){

        AndroidNetworking.enableLogging();
        Log.e("url_inbox: ", NetworkingData.BASE_URL+ NetworkingData.FRAG_INBOX);
        Log.e("userId: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.FRAG_INBOX)
                .addBodyParameter("userId",sharedPreferenceLeadr.getUserId())
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Activity activity = getActivity();
                        if(isAdded() && activity!=null){

//                        utils.dismissProgressdialog();
                            arr_inbox.clear();
                            Log.e("res_inbox: ",response+"");
                            //{"status":1,"message_detail":[{"msg_to":"74","username":"xiomi 90","image":""}]}
                            if (response.optString("status").equals("1")) {
                                try{
                                    rel_no_show.setVisibility(View.GONE);
                                    recycl_inbox.setVisibility(View.VISIBLE);
                                }catch (Exception e){
                                    Log.e("exc: ",e.toString());
                                }

                                try {
                                    JSONArray arr = response.getJSONArray("message_detail");
                                    for(int i=0; i<arr.length(); i++){
                                        JSONObject obj = arr.getJSONObject(i);
                                        InboxPojo item = new InboxPojo();
                                        item.setLead_id(obj.optString("lead_id"));
                                        item.setMsg_to(obj.optString("msg_to"));
                                        item.setMsg_from(obj.optString("msg_from"));

                                        String from_usrnm = obj.optString("from_user_name");
                                        try {
                                            try {
                                                from_usrnm = new String(Base64.decode(from_usrnm.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        item.setFrom_user_name(from_usrnm);

                                        item.setFrom_user_thum(obj.optString("from_user_thum"));

                                        String desc = obj.optString("lead_description");
                                        try {
                                            try {
                                                desc = new String(Base64.decode(desc.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        item.setLead_description(desc);
                                        item.setMsg_from_type(obj.optString("msg_from_type"));
                                        item.setMsg_to_type(obj.optString("msg_to_type"));

                                        String to_usrnm = obj.optString("to_user_name");
                                        try {
                                            try {
                                                to_usrnm = new String(Base64.decode(to_usrnm.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        item.setTo_user_name(to_usrnm);

                                        item.setTo_user_thum(obj.optString("to_user_thum"));
                                        item.setLead_user_id(obj.optString("lead_user_id"));
                                        item.setOther_user_id(obj.optString("other_user_id"));

                                        String bus_name_to = obj.getString("to_business_name");
                                        try {
                                            try {
                                                bus_name_to = new String(Base64.decode(bus_name_to.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        String bus_info_to = obj.optString("to_business_info");
                                        try {
                                            try {
                                                bus_info_to = new String(Base64.decode(bus_info_to.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }


                                        String bus_name_from = obj.optString("from_business_name");
                                        try {
                                            try {
                                                bus_name_from = new String(Base64.decode(bus_name_from.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        String bus_info_from = obj.optString("from_business_info");
                                        try {
                                            try {
                                                bus_info_from = new String(Base64.decode(bus_info_from.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }


                                        item.setFrom_business_info(bus_info_from);
                                        item.setTo_business_name(bus_name_to);
                                        item.setTo_business_info(bus_info_to);
                                        item.setFrom_business_name(bus_name_from);
                                        item.setMsg(obj.optString("msg"));
                                        item.setAudio(obj.optString("audio"));
                                        item.setAudiotime(obj.optString("time"));


                                        try{
                                            String full_dat_tym_buy = obj.optString("current_time_msg");
                                            String[] spiltby_T_buy = full_dat_tym_buy.split("T");
                                            String date_buy = spiltby_T_buy[0].trim();
                                            String tym_wid_Z_buy = spiltby_T_buy[1].trim();
                                            String spiltby_Dot1_buy = tym_wid_Z_buy.replace(".000Z","");
                                            String replace_tym_buy = spiltby_Dot1_buy.replace(":","-");
                                            item.setDate(date_buy);
                                            item.setTime(replace_tym_buy);
                                        }catch (Exception e){}


                                   /* String time = obj.getString("current_time_msg");
                                    String arr_full[] = time.split("T");
                                    String date = arr_full[0];
                                    String time_split = arr_full[1];
                                    String arr_date[] = date.split("-");
                                    String arr_time[] = time_split.split(":");
                                    String year = arr_date[0];
                                    String month = arr_date[1];
                                    String day = arr_date[2];
                                    String hour = arr_time[0];
                                    String mm = arr_time[1];
                                    String time_after_conv = getDate(hour+"-"+mm);

                                    item.setTime(time_after_conv);
                                    item.setDate(year+"-"+month+"-"+day);*/


                                        String alraedy_added = null;
                                  /* for(int k=0; k<arr_inbox.size(); k++){
                                       if(arr_inbox.get(k).getLead_id().equalsIgnoreCase(obj.optString("lead_id"))){
                                           for(int m=0; m<arr_inbox.size(); m++){
                                               if(arr_inbox.get(m).getMsg_to().equalsIgnoreCase(obj.optString("msg_to"))||
                                                       arr_inbox.get(m).getMsg_from().equalsIgnoreCase(obj.optString("msg_from"))){
                                                   alraedy_added = "";
                                               }
                                           }
                                       }
                                   }*/


                                        if(obj.optString("sold_status").equalsIgnoreCase("1")){
                                            if(obj.optString("sold_user_id").equalsIgnoreCase(sharedPreferenceLeadr.getUserId())){
                                                item.setMsg_from_type("I Bought");
                                                item.setMsg_to_type("I Sold");
                                            }else{
                                                item.setMsg_to_type("I Sold");
                                                item.setMsg_from_type("I Bought");
                                            }
                                        }

                                        arr_inbox.add(item);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    if(response.optString("message").contains("Suspended account")){
                                        AccountKit.logOut();
                                        sharedPreferenceLeadr.clearPreference();
                                        String languageToLoad  = "en";
                                        Locale locale = new Locale(languageToLoad);
                                        Locale.setDefault(locale);
                                        Configuration config = new Configuration();
                                        config.locale = locale;
                                        getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                        Intent i_nxt = new Intent(getActivity(), GetStartedActivity.class);
                                        i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i_nxt);
                                        getActivity().finish();
                                    }
                                }
                            }else{
                                try{
                                    rel_no_show.setVisibility(View.VISIBLE);
                                    recycl_inbox.setVisibility(View.GONE);
                                }catch (Exception e){}

                            }
                            if(recycl_inbox!=null) {
                                LinearLayoutManager lnr_album = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                recycl_inbox.setLayoutManager(lnr_album);

                                adapter = new Inbox_Adapter(getActivity(), arr_inbox, new Inbox_Adapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(int i) {
                                        if(!arr_inbox.get(i).getFrom_user_name().equalsIgnoreCase("user deleted account")) {
                                            if (!sharedPreferenceLeadr.getUserId().equalsIgnoreCase(arr_inbox.get(i).getLead_user_id())) {
                                                Intent i_chang = new Intent(getActivity(), ChatActivity.class);
                                                String othr_bus = arr_inbox.get(i).getFrom_business_name();
                                                i_chang.putExtra("image_other", arr_inbox.get(i).getFrom_user_thum());
                                                i_chang.putExtra("name_other", arr_inbox.get(i).getFrom_user_name());
                                                i_chang.putExtra("buss_other", othr_bus);
                                                i_chang.putExtra("info_other", arr_inbox.get(i).getFrom_business_info());
                                                i_chang.putExtra("id_other", arr_inbox.get(i).getLead_user_id());
                                                i_chang.putExtra("leadid_other", arr_inbox.get(i).getLead_id());
                                                i_chang.putExtra("lead_user_id", arr_inbox.get(i).getLead_user_id());
                                                i_chang.putExtra("other_user_id", arr_inbox.get(i).getOther_user_id());
                                                i_chang.putExtra("typemy", arr_inbox.get(i).getMsg_from_type());
                                                i_chang.putExtra("typeother", arr_inbox.get(i).getMsg_to_type());
                                                i_chang.putExtra("audio", arr_inbox.get(i).getAudio());
                                                i_chang.putExtra("audiotime", arr_inbox.get(i).getAudiotime());
                                                i_chang.putExtra("desc", arr_inbox.get(i).getLead_description());
                                                startActivity(i_chang);
                                            } else {
                                                Intent i_chang = new Intent(getActivity(), ChatActivity.class);
                                                i_chang.putExtra("image_other", arr_inbox.get(i).getTo_user_thum());
                                                i_chang.putExtra("name_other", arr_inbox.get(i).getTo_user_name());
                                                i_chang.putExtra("buss_other", arr_inbox.get(i).getTo_business_name());
                                                i_chang.putExtra("info_other", arr_inbox.get(i).getTo_business_info());
                                                i_chang.putExtra("id_other", arr_inbox.get(i).getOther_user_id());
                                                i_chang.putExtra("leadid_other", arr_inbox.get(i).getLead_id());
                                                i_chang.putExtra("lead_user_id", arr_inbox.get(i).getLead_user_id());
                                                i_chang.putExtra("other_user_id", arr_inbox.get(i).getOther_user_id());
                                                i_chang.putExtra("typemy", arr_inbox.get(i).getMsg_to_type());
                                                i_chang.putExtra("typeother", arr_inbox.get(i).getMsg_from_type());
                                                i_chang.putExtra("audio", arr_inbox.get(i).getAudio());
                                                i_chang.putExtra("audiotime", arr_inbox.get(i).getAudiotime());
                                                i_chang.putExtra("desc", arr_inbox.get(i).getLead_description());
                                                startActivity(i_chang);
                                            }
                                        }
                                    }
                                });
                                if(recycl_inbox!=null) {
                                    recycl_inbox.setAdapter(adapter);
                                }
                                api_Counter();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });

    }

    private void api_Counter(){
        AndroidNetworking.enableLogging();
        Log.e("url_inbox_counter: ", NetworkingData.BASE_URL+ NetworkingData.INBOX_MULTIPLE_COUNT);
        Log.e("userId: ", sharedPreferenceLeadr.getUserId());
        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.INBOX_MULTIPLE_COUNT)
                .addBodyParameter("userId",sharedPreferenceLeadr.getUserId())
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Activity activity = getActivity();
                        if(isAdded() && activity!=null){

                            utils.dismissProgressdialog();
                            Log.e("res_inbox: ",response+"");
                            //{"status":1,"message_detail":[{"msg_to":"74","username":"xiomi 90","image":""}]}
                            if (response.optString("status").equals("1")) {
                                try {
                                    JSONArray arr = response.getJSONArray("message_detail");
                                    for(int i=0; i<arr.length(); i++){
                                        JSONObject obj = arr.getJSONObject(i);
                                        InboxPojo item = new InboxPojo();
                                        item.setLead_id_count(obj.optString("lead_id"));
                                        item.setChat_cnt(obj.optString("chat_cnt"));

                                        String same = null;
                                        int pos_ = 0;
                                        for(int k=0;k<arr_inbox.size();k++){
                                            if(obj.optString("lead_id").equalsIgnoreCase(arr_inbox.get(k).getLead_id())){
                                                same = "";
                                                pos_ = k;

                                                item.setLead_id(arr_inbox.get(k).getLead_id());
                                                item.setMsg_to(arr_inbox.get(k).getMsg_to());
                                                item.setMsg_from(arr_inbox.get(k).getMsg_from());
                                                item.setFrom_user_name(arr_inbox.get(k).getFrom_user_name());
                                                item.setFrom_user_thum(arr_inbox.get(k).getFrom_user_thum());
                                                item.setLead_description(arr_inbox.get(k).getLead_description());
                                                item.setMsg_from_type(arr_inbox.get(k).getMsg_from_type());
                                                item.setMsg_to_type(arr_inbox.get(k).getMsg_to_type());
                                                item.setTo_user_name(arr_inbox.get(k).getTo_user_name());
                                                item.setTo_user_thum(arr_inbox.get(k).getTo_user_thum());
                                                item.setLead_user_id(arr_inbox.get(k).getLead_user_id());
                                                item.setOther_user_id(arr_inbox.get(k).getOther_user_id());
                                                item.setFrom_business_info(arr_inbox.get(k).getFrom_business_info());
                                                item.setTo_business_name(arr_inbox.get(k).getTo_business_name());
                                                item.setTo_business_info(arr_inbox.get(k).getTo_business_info());
                                                item.setFrom_business_name(arr_inbox.get(k).getFrom_business_name());
                                                item.setMsg(arr_inbox.get(k).getMsg());
                                                item.setAudio(arr_inbox.get(k).getAudio());
                                                item.setAudiotime(arr_inbox.get(k).getTime());
                                                item.setChat_cnt(obj.optString("chat_cnt_to"));

                                                item.setTime(arr_inbox.get(k).getTime());
                                                item.setDate(arr_inbox.get(k).getDate());

                                                arr_inbox.set(k,item);
                                                break;
                                            }
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            if(adapter!=null){
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        utils.dismissProgressdialog();
                    }
                });
    }




    private final BroadcastReceiver mYourBroadcastReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive( Context context, Intent intent)
        {
            api_inbox();
            // now you can call all your fragments method here
        }
    };

    @Override
    public void networkAvailable() {
        if(dialog_internet!=null){
            if(dialog_internet.isShowing()){
                dialog_internet.dismiss();
            }
        }
        if(arr_inbox!=null){
            if(arr_inbox.size()<1){
                api_inbox();
            }
        }else{
            api_inbox();
        }
    }

    @Override
    public void networkUnavailable() {
        dialog_msg_show(getActivity(),getResources().getString(R.string.no_internet));
    }


    /*Dialog for Internet*/
    public  void dialog_msg_show( Activity activity, String msg){
        MainActivity.state_maintain = "1";
        dialog_internet = new BottomSheetDialog (activity);
        dialog_internet.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog_internet.setContentView(bottomSheetView);



        dialog_internet.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d     = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog_internet.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog_internet.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog_internet.findViewById(R.id.txt_ok);

        txt_msg.setText(msg);

        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog_internet.findViewById(R.id.rel_vw_save_details);

        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_internet.dismiss();
            }
        });
        try{
            if(dialog_internet!=null){
                if(!dialog_internet.isShowing()){
                    dialog_internet.show();

                }
            }
        }catch (Exception e){
        }
    }
}
