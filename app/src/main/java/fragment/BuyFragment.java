package fragment;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.accountkit.AccountKit;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.BasePostprocessor;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.mindorks.butterknifelite.ButterKnifeLite;
import com.squareup.picasso.Picasso;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import leadr.com.leadr.R;
import leadr.com.leadr.activity.ChatActivity;
import leadr.com.leadr.activity.FilterActivity;
import leadr.com.leadr.activity.GetStartedActivity;
import leadr.com.leadr.activity.MainActivity;
import leadr.com.leadr.activity.PaymentActivity;
import leadr.com.leadr.activity.ProfileActivity;
import leadr.com.leadr.activity.ProfileOther_Activity;
import leadr.com.leadr.activity.RateUsActivity;
import leadr.com.leadr.activity.RegistrationActivity;
import leadr.com.leadr.activity.SellActivity;
import modal.BuyPojo;
import modal.LatlngPojo;
import pref.SharedPreferenceLeadr;
import utils.CircleTransform;
import utils.GlobalConstant;
import utils.NetworkStateReceiver;
import utils.NetworkStateReceiver.NetworkStateReceiverListener;
import utils.NetworkingData;

import static leadr.com.leadr.activity.MainActivity.rel_bottom;
import static leadr.com.leadr.activity.MainActivity.vw_bought;
import static leadr.com.leadr.activity.MainActivity.vw_inbox;
//import static leadr.com.leadr.activity.MainActivity.vw_line;
import static leadr.com.leadr.activity.MainActivity.vw_more;
import static leadr.com.leadr.activity.MainActivity.vw_sell;
import static utils.GlobalConstant.PAGINATION_IDS;


/**
 * Created by Abhilasha on 5/12/2017.
 */

public class BuyFragment extends Fragment implements View.OnClickListener,NetworkStateReceiverListener {


    CardStackView                cardStack;
    TextView                     txt_buy_top;
    public static ImageView      img_filter;
    RelativeLayout               rel_no_show;
    RelativeLayout               rel_main_vw;
    public static RelativeLayout rel_dialog;
    public static View           vw_line;

    ImageView      img_undo;
    ProgressBar    progresS_load;

    GlobalConstant utils;

    ArrayList<BuyPojo>    arr_buy = new ArrayList<>();
    ArrayList<BuyPojo>    arr_buy_lastSwipe = new ArrayList<>();
    ArrayList<LatlngPojo> arr_place = new ArrayList<>();
    ArrayList<String>     arr_ids = new ArrayList<>();

    BuyAdapter            adapter;
    SharedPreferenceLeadr sharedPreferenceLeadr;

    private Handler mHandler = new Handler();
    Handler         mHandler_seekbar;

    private MediaPlayer mMediaPlayer = null;
    private boolean     isPlaying = false;

    private static CountDownTimer countDownTimer;

    int count_start_pause = 30;
    int count_start_pause_add = 0;
    int resume_pos = 0;
    String            phone_no = "000000";
    String            admin_max_price = "";
    BottomSheetDialog dialog_per;
    public  Handler   handler_refresh = new Handler();
    Handler           handler_timer = new Handler();
    CountDownTimer ct ;
    int count_arr = 0;
    BottomSheetDialog dialog_card;


    /***Payment block***/
    Button         btnok;
    Button         btnok1;
    EditText       edt_cvv;
    EditText       edt_month;
    EditText       edt_year;
    EditText       edt_card_no;
    EditText       edt_full_name;
    ImageView      img_cancel;
    ImageView      img_cancel1;
    ImageView      img_remove;
    RelativeLayout rel_bottm;
    RelativeLayout rel_botm_real;
    TextView       txt_top;
    String         undo = null;
    Context        ctx;

    NetworkStateReceiver networkStateReceiver;
    BottomSheetDialog    dialog_internet;

    public BuyFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_buy, container, false);

        ButterKnife.bind(getActivity());
        ButterKnifeLite.bind(getActivity());
        // bind view using butter knife
        utils                 = new GlobalConstant();
        sharedPreferenceLeadr = new SharedPreferenceLeadr(getActivity());
        ctx                   = getActivity();

        cardStack     = (CardStackView)view. findViewById(R.id.swipe_deck);
        txt_buy_top   = (TextView) view. findViewById(R.id.txt_buy_top);
        rel_no_show   = (RelativeLayout) view. findViewById(R.id.rel_no_show);
        rel_main_vw   = (RelativeLayout) view. findViewById(R.id.rel_main_vw);
        rel_dialog    = (RelativeLayout) view. findViewById(R.id.rel_dialog);
        img_filter    = (ImageView) view. findViewById(R.id.img_filter);
        img_undo      = (ImageView) view. findViewById(R.id.img_undo);
        progresS_load = (ProgressBar) view. findViewById(R.id.progres_load);

        btnok          = (Button) view. findViewById(R.id.btnok);
        btnok1         = (Button) view. findViewById(R.id.btnok1);
        edt_cvv        = (EditText) view. findViewById(R.id.edt_cvv);
        edt_month      = (EditText) view. findViewById(R.id.edt_month);
        edt_year       = (EditText) view. findViewById(R.id.edt_year);
        edt_card_no    = (EditText) view. findViewById(R.id.edt_card_no);
        edt_full_name  = (EditText) view. findViewById(R.id.edt_full_name);
        img_cancel     = (ImageView) view. findViewById(R.id.img_cancel);
        img_cancel1    = (ImageView) view. findViewById(R.id.img_cancel1);
        img_remove     = (ImageView) view. findViewById(R.id.img_remove);
        txt_top        = (TextView) view. findViewById(R.id.txt_top);
        rel_bottm      = (RelativeLayout) view. findViewById(R.id.rel_bottm);
        rel_botm_real  = (RelativeLayout) view. findViewById(R.id.rel_botm_real);
        vw_line        = (View) view. findViewById(R.id.vw_line);


        txt_buy_top.setTypeface(utils.OpenSans_Regular(getActivity()));
        btnok.setTypeface(utils.Dina(getActivity()));
        btnok1.setTypeface(utils.Dina(getActivity()));
        edt_cvv.setTypeface(utils.OpenSans_Regular(getActivity()));
        edt_month.setTypeface(utils.OpenSans_Regular(getActivity()));
        edt_year.setTypeface(utils.OpenSans_Regular(getActivity()));
        edt_card_no.setTypeface(utils.OpenSans_Regular(getActivity()));
        edt_full_name.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_top.setTypeface(utils.OpenSans_Regular(getActivity()));

        if(cardStack!=null) {
            cardStack.setSwipeThreshold(0.1f);
        }
        String text = "<font color=#227cec>First time only</font> <font color=#071a30>, You must add your credit card information for selling and buying leads</font>";
        txt_top.setText(Html.fromHtml(text));


        img_filter.setOnClickListener(this);

        if(getActivity()!=null) {
            OnResumeMethodApiCalled();
        }

        cardStack.setCardEventListener(new CardStackView.CardEventListener() {
            @Override
            public void onCardDragging(float percentX, float percentY) {
            }

            @Override
            public void onCardSwiped(SwipeDirection direction, int topIndex) {
                MainActivity.state_maintain = "1";
                if(undo==null){
                    img_undo.setVisibility(View.VISIBLE);
                }
                undo = null;

                if (mMediaPlayer!=null){
                    if(mMediaPlayer.isPlaying()){
                        mMediaPlayer.stop();
                        mMediaPlayer.release();
                    }
                    mMediaPlayer = null;
                }
                if(countDownTimer!=null){
                    countDownTimer.cancel();
                    countDownTimer=null;
                }
                count_start_pause=30;
                count_start_pause_add=0;
                resume_pos=0;
                if(!direction.toString().equals("Right")) {
                    if (topIndex != arr_buy.size()) {
                        if(arr_buy!=null){
                        if(arr_buy.size()!=1) {
                            if(arr_buy.size()>0){
                                api_unlyk(arr_buy.get(0).getLead_id(), "1");
                            }
                        }
                        }
                    }
                }
                if(direction.toString().equals("Right")){
                    if(topIndex!=arr_buy.size()) {

                    }
                }
                if(arr_buy!=null) {
                    if (arr_buy.size() != 1) {
                        if (arr_buy.size() > 0) {
                            api_Analytic(arr_buy.get(0).getLead_id());
                        }
                    }
                }
                try {
                    arr_buy_lastSwipe.clear();
                    if(arr_buy.size()!=1) {
                        if (arr_buy.size() > 0) {
                            arr_buy_lastSwipe.add(arr_buy.get(0));
                        }
                    }
                    if(arr_buy.size()>0) {
                        arr_buy.remove(0);
                    }
                    if(adapter!=null) {
                        adapter.notifyDataSetChanged();
                    }
//                        Log.e("adap:", adapter.getCount()+"");
                } catch (Exception e) {
                }

                if(arr_buy.size()==3){
//                    Toast.makeText(getActivity(),"pos topindex: ",Toast.LENGTH_LONG).show();
                    Log.e("topIndex: ",topIndex+"");
//                    Log.e("arrbuy size: ",arr_buy.size()+"");
                    OnPagination();
                }
                if(arr_buy.size()==0){
                    img_undo.setVisibility(View.GONE);
                    remove_Callback();
                    PAGINATION_IDS = "0";
                    OnResumeMethodApiCalled();
                }
                else if(arr_buy.size()==1){
                    try{
                        handler_refresh.postDelayed(runnable, 0);
                    }catch (Exception e){}
                }else{
                    remove_Callback();
                }
                try{
                    handler_timer.removeCallbacks(runnable_timer);
                }catch (Exception e){}
                try{
                    handler_timer.postDelayed(runnable_timer, 40000);
                }catch (Exception e){}

            }

            @Override
            public void onCardReversed() {
            }

            @Override
            public void onCardMovedToOrigin() {
            }

            @Override
            public void onCardClicked(int index) {
            }
        });





        /****UNDO functionality******/
        img_undo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                if(arr_buy_lastSwipe!=null){
                    if(arr_buy_lastSwipe.size()>0){
                        if(adapter!=null){
                            if(arr_buy_lastSwipe.size()>0) {
                                api_unlyk(arr_buy_lastSwipe.get(0).getLead_id(), "0");
                            }
                        }
                    }
                }
            }
        });


        try{
            networkStateReceiver = new NetworkStateReceiver();
            networkStateReceiver.addListener(this);
            getActivity().registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        }catch (Exception e){}
        return view;
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            remove_Callback();
            handler_timer.removeCallbacks(runnable_timer);
            networkStateReceiver.removeListener(this);
        }catch (Exception e){}
        getActivity().unregisterReceiver(networkStateReceiver);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        try{
            remove_Callback();
        }catch (Exception e){}
        try{
            handler_timer.removeCallbacks(runnable_timer);
        }catch (Exception e){}
    }

    @Override
    public void onStop() {
        super.onStop();
        try{
            handler_timer.removeCallbacks(runnable_timer);
        }catch (Exception e){}
        try{
            remove_Callback();
        }catch (Exception e){}
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            handler_timer.removeCallbacks(runnable_timer);
        }catch (Exception e){}
        try{
            remove_Callback();
        }catch (Exception e){}
    }

    void OnResumeMethodApiCalled(){
        if(img_undo!=null){
            img_undo.setVisibility(View.GONE);
        }
        try{
            remove_Callback();
        }catch (Exception e){}
        if(utils.isNetworkAvailable(getActivity())) {
            if(dialog_internet!=null){
                if(dialog_internet.isShowing()){
                    dialog_internet.dismiss();
                }
            }
            if(sharedPreferenceLeadr.get_LOC_NAME_TEMP().equals("")){
                if(sharedPreferenceLeadr.get_LOC_TYPE_FILTER().equals("0")){
                    LatlngPojo item = new LatlngPojo();
                    item.setName("0");
                    item.setLong_("0");
                    item.setLat("0");

                    arr_place.add(item);
                }else{
                    method_Add_Loc();
                }
            }else{
                method_Add_Loc();
            }
            if(progresS_load!=null) {
                progresS_load.setVisibility(View.VISIBLE);
            }
            api_Buy();
        }else{
            /*try{
                handler_refresh.postDelayed(runnable, 4000);
            }catch (Exception e){}
            if( dialog_internet!=null){
                if( !dialog_internet.isShowing()){
                    dialog_msg_show(getActivity(),getResources().getString(R.string.no_internet));
                }
            }*/
        }
    }

    void OnResumeRefreshMethodApiCalled(){
        try{
            remove_Callback();
        }catch (Exception e){}
        if(utils.isNetworkAvailable(getActivity())) {
            if(dialog_internet!=null){
                if(dialog_internet.isShowing()){
                    dialog_internet.dismiss();
                }
            }
            if(sharedPreferenceLeadr.get_LOC_NAME_TEMP().equals("")){
                if(sharedPreferenceLeadr.get_LOC_TYPE_FILTER().equals("0")){
                    LatlngPojo item = new LatlngPojo();
                    item.setName("0");
                    item.setLong_("0");
                    item.setLat("0");

                    arr_place.add(item);
                }else{
                    method_Add_Loc();
                }
            }else{
                method_Add_Loc();
            }
//            progresS_load.setVisibility(View.VISIBLE);
            api_Buy();
        }else{
            /*try{
                handler_refresh.postDelayed(runnable, 4000);
            }catch (Exception e){}
            if( dialog_internet!=null){
                if( !dialog_internet.isShowing()){
                    dialog_msg_show(getActivity(),getResources().getString(R.string.no_internet));
                }
            }else{
                dialog_msg_show(getActivity(),getResources().getString(R.string.no_internet));
            }*/
        }
    }


    public  void remove_Callback(){
        try{
            handler_refresh.removeCallbacks(null);
            Log.e("inside callback ","true");
        }catch (Exception e){}
    }

    void OnPagination(){
        try{
            remove_Callback();
        }catch (Exception e){}
        if(utils.isNetworkAvailable(getActivity())) {
            if(dialog_internet!=null){
                if(dialog_internet.isShowing()){
                    dialog_internet.dismiss();
                }
            }
            if(sharedPreferenceLeadr.get_LOC_NAME_TEMP().equals("")){
                if(sharedPreferenceLeadr.get_LOC_TYPE_FILTER().equals("0")){
                    LatlngPojo item = new LatlngPojo();
                    item.setName("0");
                    item.setLong_("0");
                    item.setLat("0");

                    arr_place.add(item);
                }else{
                    method_Add_Loc();
                }
            }else{
                method_Add_Loc();
            }
//            progresS_load.setVisibility(View.VISIBLE);
            api_Buy_Pagination();
        }else{/*
            try{
                handler_refresh.postDelayed(runnable, 4000);
            }catch (Exception e){}
            if( dialog_internet!=null){
                if( !dialog_internet.isShowing()){
                    dialog_msg_show(getActivity(),getResources().getString(R.string.no_internet));
                }
            }else{
                dialog_msg_show(getActivity(),getResources().getString(R.string.no_internet));
            }*/
        }
    }


    Runnable runnable = new Runnable(){
        @Override
        public void run() {
            try{
                Log.e("cond: ","1");
                remove_Callback();
            }catch (Exception e){}
            PAGINATION_IDS = "0";
            if(ctx!=null) {
                Log.e("cond: ","2");
                if(MainActivity.state_maintain.equalsIgnoreCase("1")) {
                    Log.e("cond: ","3");
                    OnResumeRefreshMethodApiCalled();
                }else{
                    Log.e("cond: ","4");
                    try{
                        handler_refresh.postDelayed(runnable, 1000);
                    }catch  (Exception e){}
                }
            }else{
                Log.e("cond: ","15");
                Log.e(" api not called: ","true");
            }
        }
    };


    Runnable runnable_timer = new Runnable(){
        @Override
        public void run() {
            if(adapter!=null){
                adapter.notifyDataSetChanged();
            }
            try{
                handler_timer.postDelayed(runnable_timer, 40000);
            }catch (Exception e){}
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.state_maintain = "1";
        method_ResumeFragment();
    }

    public void method_ResumeFragment(){
        if(ctx!=null){
            if(FilterActivity.saved_filter!=null) {
                FilterActivity.saved_filter = null;
                PAGINATION_IDS = "0";
                OnResumeMethodApiCalled();
            }else{
                if(ctx!=null){
                    if(arr_buy!=null){
                        if(arr_buy.size()==1){
                            try{
                                handler_refresh.postDelayed(runnable, 100);
                            }catch  (Exception e){}
                        }else{
                            try{
                                remove_Callback();
                            }catch (Exception e){}
                        }
                        try{
                            handler_timer.removeCallbacks(runnable_timer);
                        }catch (Exception e){}
                        if(arr_buy.size()>1){
                            try{
                                handler_timer.postDelayed(runnable_timer, 4000);
                            }catch (Exception e){}
                        }
                    }
                }
            }
        }else{
                if(arr_buy!=null){
                    if(arr_buy.size()==1){
                        try{
                            handler_refresh.postDelayed(runnable, 4000);
                        }catch (Exception e){}
                    }else{
                        try{
                            remove_Callback();
                        }catch (Exception e){}
                        try{
                            handler_timer.removeCallbacks(runnable_timer);
                        }catch (Exception e){}
                        if(arr_buy.size()>1){
                            try{
                                handler_timer.postDelayed(runnable_timer, 4000);
                            }catch (Exception e){}
                        }
                    }
                }
        }
    }

    public void swipeRight() {
        if(cardStack!=null){
            undo = "";
            List<BuyPojo> spots = extractRemainingTouristSpots();
            if (spots.isEmpty()) {
                return;
            }

            View target = cardStack.getTopView();
            ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                    target, PropertyValuesHolder.ofFloat("rotation", 10f));
            rotation.setDuration(200);
            ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                    target, PropertyValuesHolder.ofFloat("translationX", 0f, 2000f));
            ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                    target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
            translateX.setStartDelay(100);
            translateY.setStartDelay(100);
            translateX.setDuration(500);
            translateY.setDuration(500);
            AnimatorSet set = new AnimatorSet();
            set.playTogether(rotation, translateX, translateY);

            cardStack.swipe(SwipeDirection.Right, set);
            handler_timer.postDelayed(runnable_timer,40000);
            img_undo.setVisibility(View.GONE);
        }
    }


    private ArrayList<BuyPojo> extractRemainingTouristSpots() {
        ArrayList<BuyPojo> spots = new ArrayList<>();
        for (int i = cardStack.getTopIndex(); i < adapter.getCount(); i++) {
            spots.add((BuyPojo) adapter.getItem(i));
        }
        return spots;
    }


    @Override
    public void onStart() {
        super.onStart();
    }


    /***GET LOCATION**************
     * ******
     * */

    void method_Add_Loc(){
        if(sharedPreferenceLeadr.get_LOC_NAME_TEMP().equals("")){
            sharedPreferenceLeadr.set_LOC_NAME_TEMP(sharedPreferenceLeadr.get_LOC_NAME());
            sharedPreferenceLeadr.set_LATITUDE_TEMP(sharedPreferenceLeadr.get_LATITUDE());
            sharedPreferenceLeadr.set_LONGITUDE_TEMP(sharedPreferenceLeadr.get_LONGITUDE());
            sharedPreferenceLeadr.set_LOC_NAME_COUNTRY_TEMP(sharedPreferenceLeadr.get_LOC_NAME_COUNTRY());
        }
        String loc_sel = sharedPreferenceLeadr.get_LOC_NAME_TEMP();
        String lat_sel = sharedPreferenceLeadr.get_LATITUDE_TEMP();
        String long_sel = sharedPreferenceLeadr.get_LONGITUDE_TEMP();
        String country_sel = sharedPreferenceLeadr.get_LOC_NAME_COUNTRY_TEMP();

        arr_place.clear();

        if(lat_sel.contains(",")){
            String[] animalsArray = loc_sel.split(",");
            String[] longArray = long_sel.split(",");
            String[] latArray = lat_sel.split(",");
            String[] countryArray = country_sel.split(",");
            for(int j=0; j<latArray.length; j++){
                LatlngPojo item = new LatlngPojo();
                item.setName(loc_sel.trim());
                item.setLong_(longArray[j]);
                item.setLat(latArray[j]);
                item.setCountryname(country_sel.trim());

                arr_place.add(item);
            }

        }else{
            LatlngPojo item = new LatlngPojo();
            item.setName(loc_sel.trim());
            item.setLong_(long_sel.trim());
            item.setLat(lat_sel.trim());
            item.setCountryname(country_sel.trim());

            arr_place.add(item);
        }
    }


    private void api_Buy(){
        PAGINATION_IDS = "0";
        count_arr= 0;
        try{
            remove_Callback();
            handler_timer.removeCallbacks(runnable_timer);
        }catch (Exception e){}
        AndroidNetworking.enableLogging();
        String hour = "";
        String day = "";
        if(sharedPreferenceLeadr.get_SWITCH_DATE().equalsIgnoreCase("1")) {
            if (sharedPreferenceLeadr.get_HOUR_OR_DAY().equalsIgnoreCase("hour")) {
                if (!sharedPreferenceLeadr.get_MAX_TIME_COUNT().equals("?")) {
                    hour = sharedPreferenceLeadr.get_MAX_TIME_COUNT();
                }
            } else {
                if (!sharedPreferenceLeadr.get_MAX_TIME_COUNT().equals("?")) {
                    day = sharedPreferenceLeadr.get_MAX_TIME_COUNT();
                }
            }
        }
       /* Log.e("userid",     sharedPreferenceLeadr.getUserId());
        Log.e("url_buy: ", NetworkingData.BASE_URL+ NetworkingData.BUY_LOC1);

        Log.e("userid",sharedPreferenceLeadr.getUserId());

        Log.e("hour",hour);
        Log.e("day",day);*/

        String country_name = "0";
        String city_name = "0";
        String lat_ = "0";
        String long_ = "0";
        String radius = "0";
        String city_names = sharedPreferenceLeadr.get_CAT_CAT_FILTER_ID_TEMP();
        if(city_names.equalsIgnoreCase("")){
            city_names = "0";
        }else if(city_names.contains(",")){
            city_names = city_names.replace(",","|");
        }
        String dimension = "0";
//        Log.e("category_id",city_names);

        if(sharedPreferenceLeadr.get_SWITCH_JOB().equalsIgnoreCase("1")) {
            if (arr_place.size() > 0) {
                /*multiple loc*/
                country_name = arr_place.get(0).getCountryname();
                city_name = arr_place.get(0).getName();
                radius = sharedPreferenceLeadr.get_TO_RADIUS();
                if(arr_place.size()==1){
                    lat_ = arr_place.get(0).getLat()+",0,0,0,0";
                    long_ = arr_place.get(0).getLong_()+",0,0,0,0";
                }else if(arr_place.size()==2){
                    lat_ = arr_place.get(0).getLat()+","+arr_place.get(1).getLat()+",0,0,0";
                    long_ = arr_place.get(0).getLong_()+","+arr_place.get(1).getLong_()+",0,0,0";
                }else if(arr_place.size()==3){
                    lat_ = arr_place.get(0).getLat()+","+arr_place.get(1).getLat()+","+arr_place.get(2).getLat()+",0,0";
                    long_ = arr_place.get(0).getLong_()+","+arr_place.get(1).getLong_()+","+arr_place.get(2).getLong_()+",0,0";
                }else if(arr_place.size()==4){
                    lat_ = arr_place.get(0).getLat()+","+arr_place.get(1).getLat()+","+arr_place.get(2).getLat()+","
                            +arr_place.get(3).getLat()+",0";
                    long_ = arr_place.get(0).getLong_()+","+arr_place.get(1).getLong_()+","+arr_place.get(2).getLong_()
                            +","+arr_place.get(3).getLong_()+",0";
                }else if(arr_place.size()==5){
                    lat_ = arr_place.get(0).getLat()+","+arr_place.get(1).getLat()+","+arr_place.get(2).getLat()+","
                            +arr_place.get(3).getLat()+","+arr_place.get(4).getLat();
                    long_ = arr_place.get(0).getLong_()+","+arr_place.get(1).getLong_()+","+arr_place.get(2).getLong_()
                            +","+arr_place.get(3).getLong_()+","+arr_place.get(4).getLong_();
                }
                if (sharedPreferenceLeadr.get_DIMENSION().equalsIgnoreCase("0")) {
                    dimension = "mi";
                } else {
                    dimension = "km";
                }
            }
        }
       /* Log.e("location",city_name);
        Log.e("country",country_name);
        Log.e("lat_",lat_);
        Log.e("long_",long_);
        Log.e("radius",radius);
        Log.e("ids: ",PAGINATION_IDS);
        Log.e("dimension: ",dimension);*/

        String min_budget = "0";
        if(sharedPreferenceLeadr.get_SWITCH_BUD().equalsIgnoreCase("1")) {
            if (!sharedPreferenceLeadr.get_MIN_BUDGET().equalsIgnoreCase("?")) {
                try {
                    float perc = 0.95f * Float.valueOf(sharedPreferenceLeadr.get_MIN_BUDGET());
                    min_budget = String.valueOf(Math.round(perc));
                } catch (Exception e) {
                    min_budget = sharedPreferenceLeadr.get_MIN_BUDGET();
                }
            }
        }
//        Log.e("min_budget",min_budget);
        String from_price = "0";
        String to_price = "0";
        if(sharedPreferenceLeadr.get_SWITCH_PRICE().equalsIgnoreCase("1")){
            from_price = sharedPreferenceLeadr.get_FROM_PRICE();
            to_price = sharedPreferenceLeadr.get_TO_PRICE();
        }
//        Log.e("from_price",from_price);
//        Log.e("to_price",to_price);

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.BUY_LOC1)
                .addBodyParameter("from_price",from_price)
                .addBodyParameter("to_price",to_price)
                .addBodyParameter("category_id",city_names)
                .addBodyParameter("min_budget",min_budget)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
//                .addBodyParameter("location",city_name)
//                .addBodyParameter("country",country_name)
                .addBodyParameter("day",day)
                .addBodyParameter("hour",hour)
                .addBodyParameter("lat",lat_)
                .addBodyParameter("lon",long_)
                .addBodyParameter("radius",radius)
                .addBodyParameter("radius_type",dimension)
                .addBodyParameter("lead_ids",PAGINATION_IDS)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Activity activity = getActivity();
                        if(isAdded() && activity!=null){

                            utils.dismissProgressdialog();
                            arr_buy.clear();
                            arr_ids.clear();
                            try{
                                txt_buy_top.setVisibility(View.VISIBLE);
                                rel_no_show.setVisibility(View.GONE);
                                progresS_load.setVisibility(View.GONE);
                            }catch (Exception e){}

                            if(response!=null) {
                                Log.e("res_buy: ", response + "");
                            }
                            if(response.optString("currency_rate")!=null) {
                                GlobalConstant.CURRENCY = response.optString("currency_rate");
                                Log.e("curr :",GlobalConstant.CURRENCY);
                                sharedPreferenceLeadr.set_PRICE_ILS(GlobalConstant.CURRENCY);
                            }
                            if (response.optString("status").equals("1")) {
                                try {
                                    JSONArray arr = response.getJSONArray("detail");
                                    for (int i = 0; i < arr.length(); i++) {
                                        JSONObject obj = arr.getJSONObject(i);
                                        BuyPojo item = new BuyPojo();
                                        item.setAudio(obj.optString("audio"));
                                        item.setBudget(obj.optString("budget"));
                                        item.setBusiness_fb_page(obj.optString("business_fb_page"));
                                        item.setBusiness_image(obj.optString("business_image"));

                                        String bus_name = obj.optString("business_name");
                                        try {
                                            try {
                                                bus_name = new String(Base64.decode(bus_name.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        String bus_info = obj.optString("business_info");
                                        try {
                                            try {
                                                bus_info = new String(Base64.decode(bus_info.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        item.setBusiness_info(bus_info);
                                        item.setBusiness_name(bus_name);

                                        item.setCategory(obj.optString("category"));
                                        item.setCell_number(obj.optString("cell_number"));
                                        try{
                                            String client_name = obj.optString("client_name");
                                            try {
                                                client_name = new String(Base64.decode(client_name.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (IllegalArgumentException e) {
                                                e.printStackTrace();
                                            }

                                            if(getActivity()!=null) {
                                                if (client_name != null) {
                                                    if (client_name.equalsIgnoreCase(" ")) {
                                                        client_name = getResources().getString(R.string.unknw);
                                                    } else if (client_name.equalsIgnoreCase("")) {
                                                        client_name = getResources().getString(R.string.unknw);
                                                    }
                                                } else {
                                                    client_name = getResources().getString(R.string.unknw);
                                                }
                                            }
                                            item.setClient_name(client_name);
                                        }catch (Exception e){}

                                        item.setId(obj.optString("id"));
//                                    Log.e("ids_buy: ",obj.optString("lead_id"));
                                        //if it 0 then it means lead is in active
                                        item.setIs_delete(obj.optString("is_delete"));
                                        item.setLead_id(obj.optString("lead_id"));
                                        item.setLead_price(obj.optString("lead_price"));
                                        item.setPhone_number(obj.optString("phone_number"));
                                        item.setType(obj.optString("type"));
                                        item.setUser_id(obj.optString("user_id"));
                                        item.setUser_image(obj.optString("user_image"));
                                        if(obj.optString("user_name").equalsIgnoreCase("user deleted account")){
                                            if(getActivity()!=null) {
                                                item.setUser_name(getResources().getString(R.string.usr_del));
                                                item.setJob_title(getResources().getString(R.string.usr_del));
                                                item.setDescription(getResources().getString(R.string.usr_del));
                                            }
                                        }else {
                                            String usrnm = obj.optString("user_name");
                                            try {
                                                usrnm = new String(Base64.decode(usrnm.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }

                                            item.setUser_name(usrnm);

                                            String job_tit = obj.optString("job_title");
                                            try {
                                                try {
                                                    job_tit = new String(Base64.decode(job_tit.getBytes(),Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (IllegalArgumentException e) {
                                                e.printStackTrace();
                                            }

                                            item.setJob_title(job_tit);

                                            String desc = obj.optString("description");

                                            try {
                                                try {
                                                    desc = new String(Base64.decode(desc.getBytes(),Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (IllegalArgumentException e) {
                                                e.printStackTrace();
                                            }
                                            item.setDescription(desc);
                                        }
                                        item.setLat(obj.optString("lat"));
                                        item.setLon(obj.optString("lon"));

                                        String locnm = obj.optString("location_name");
                                        try {
                                            try {
                                                locnm = new String(Base64.decode(locnm.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        item.setLocation_name(locnm);

                                        try{
                                            String full_dat_tym_buy = obj.optString("created_time");
                                            String[] spiltby_T_buy = full_dat_tym_buy.split("T");
                                            String date_buy = spiltby_T_buy[0].trim();
                                            String tym_wid_Z_buy = spiltby_T_buy[1].trim();
                                            String spiltby_Dot1_buy = tym_wid_Z_buy.replace(".000Z","");
                                            String replace_tym_buy = spiltby_Dot1_buy.replace(":","-");
                                            item.setCreated_date(date_buy);
                                            item.setCreated_time(replace_tym_buy);
                                        }catch (Exception e){}

                                        item.setTime(obj.optString("time"));

                                        String addres = obj.optString("address");
                                        try {
                                            try {
                                                addres = new String(Base64.decode(addres.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        item.setAddress(addres);
                                        item.setUser_thum(obj.optString("user_thum"));
                                        item.setCondition(false);
                                        item.setChange(false);

                                        if(PAGINATION_IDS.equalsIgnoreCase("0")) {
                                            PAGINATION_IDS = obj.optString("lead_id");
                                        }else{
                                            PAGINATION_IDS = PAGINATION_IDS + ","+obj.optString("lead_id");
                                        }
                                        arr_ids.add(obj.optString("lead_id"));
                                        arr_buy.add(item);
                                        if (i == arr.length() - 1) {
                                            arr_buy.add(item);
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if(response.optString("message").contains("Suspended account")){
                                    AccountKit.logOut();
                                    sharedPreferenceLeadr.clearPreference();
                                    String languageToLoad  = "en";
                                    Locale locale = new Locale(languageToLoad);
                                    Locale.setDefault(locale);
                                    Configuration config = new Configuration();
                                    config.locale = locale;
                                    getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                    Intent i_nxt = new Intent(getActivity(), GetStartedActivity.class);
                                    i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i_nxt);
                                    getActivity().finish();
                                }else{
                                    BuyPojo item = new BuyPojo();
                                    item.setAudio("");
                                    item.setBudget("");
                                    item.setBusiness_fb_page("");
                                    item.setBusiness_image("");
                                    item.setBusiness_info("");
                                    item.setBusiness_name("");
                                    item.setCategory("");
                                    item.setCell_number("");
                                    item.setClient_name("");
                                    item.setDescription("");
                                    item.setId("");
                                    item.setJob_title("");
                                    //if it 0 then it means lead is in active
                                    item.setIs_delete("");
                                    item.setLead_id("");
                                    item.setLead_price("");
                                    item.setPhone_number("");
                                    item.setType("");
                                    item.setUser_id("");
                                    item.setUser_image("");
                                    item.setUser_name("");
                                    item.setLat("");
                                    item.setLon("");
                                    item.setLocation_name("");
                                    item.setCreated_time("");
                                    item.setCreated_date("");
                                    item.setTime("");
                                    item.setCountry("");
                                    item.setAddress("");
                                    item.setUser_thum("");
                                    item.setCondition(false);
                                    item.setChange(false);

                                    arr_buy.add(item);
                                }

                            }
                            if (getContext() != null) {
                                if (cardStack != null) {
                                    adapter = new BuyAdapter(arr_buy, getContext());
                                    cardStack.setAdapter(adapter);
                                }
                            }

                            String days_passed = method_GetDiffrenceDays();
                            if (!days_passed.equals("0")) {
                                if (Integer.valueOf(days_passed) % 3 == 0) {
                                    String previous_day = sharedPreferenceLeadr.get_DAYPASSED();
                                    if(!previous_day.equals(days_passed)) {
                                        if (getActivity() != null) {
                                            sharedPreferenceLeadr.set_DAYPASSED(days_passed);
                                        /*Intent i = new Intent(getActivity(), RateUsActivity.class);
                                        startActivity(i);*/
                                        }
                                    }
                                }
                            }
                            if(img_undo!=null) {
                                img_undo.setVisibility(View.GONE);
                            }
                            handler_timer.postDelayed(runnable_timer,40000);
                            if(arr_buy.size()==1){
                                try{
                                    handler_refresh.postDelayed(runnable, 4000);
                                }catch (Exception e){}
                            }else{
                                try{
                                    remove_Callback();
                                }catch (Exception e){}
                            }
                            count_arr = arr_buy.size();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Activity activity = getActivity();
                        if(isAdded() && activity != null){
                            if(progresS_load!=null) {
                                progresS_load.setVisibility(View.GONE);
                            }
                            if(img_undo!=null) {
                                img_undo.setVisibility(View.GONE);
                            }
                            utils.dismissProgressdialog();
                            try{
                                handler_refresh.postDelayed(runnable, 4000);
                            }catch (Exception e){}
                        }
                    }
                });
    }


    private void api_Buy_Pagination(){
        count_arr= 0;
        try{
            remove_Callback();
            handler_timer.removeCallbacks(runnable_timer);
        }catch (Exception e){}
        AndroidNetworking.enableLogging();
        String hour = "";
        String day = "";
        if(sharedPreferenceLeadr.get_SWITCH_DATE().equalsIgnoreCase("1")) {
            if (sharedPreferenceLeadr.get_HOUR_OR_DAY().equalsIgnoreCase("hour")) {
                if (!sharedPreferenceLeadr.get_MAX_TIME_COUNT().equals("?")) {
                    hour = sharedPreferenceLeadr.get_MAX_TIME_COUNT();
                }
            } else {
                if (!sharedPreferenceLeadr.get_MAX_TIME_COUNT().equals("?")) {
                    day = sharedPreferenceLeadr.get_MAX_TIME_COUNT();
                }
            }
        }
      /*  Log.e("userid",sharedPreferenceLeadr.getUserId());
        Log.e("url_buy: ", NetworkingData.BASE_URL+ NetworkingData.BUY_LOC1);
        Log.e("from_price",sharedPreferenceLeadr.get_FROM_PRICE());
        Log.e("userid",sharedPreferenceLeadr.getUserId());
        Log.e("to_price",sharedPreferenceLeadr.get_TO_PRICE());

        Log.e("hour",hour);
        Log.e("day",day);*/

        String country_name = "0";
        String city_name = "0";
        String lat_ = "0";
        String long_ = "0";
        String radius = "0";
        String city_names = sharedPreferenceLeadr.get_CAT_CAT_FILTER_ID_TEMP();
        if(city_names.equalsIgnoreCase("")){
            city_names = "0";
        }
        String dimension = "0";
//        Log.e("category_id",city_names);

        if(sharedPreferenceLeadr.get_SWITCH_JOB().equalsIgnoreCase("1")) {
            if (arr_place.size() > 0) {
                /*multiple loc*/
                country_name = arr_place.get(0).getCountryname();
                city_name = arr_place.get(0).getName();
                radius = sharedPreferenceLeadr.get_TO_RADIUS();
                if(arr_place.size()==1){
                    lat_ = arr_place.get(0).getLat()+",0,0,0,0";
                    long_ = arr_place.get(0).getLong_()+",0,0,0,0";
                }else if(arr_place.size()==2){
                    lat_ = arr_place.get(0).getLat()+","+arr_place.get(1).getLat()+",0,0,0";
                    long_ = arr_place.get(0).getLong_()+","+arr_place.get(1).getLong_()+",0,0,0";
                }else if(arr_place.size()==3){
                    lat_ = arr_place.get(0).getLat()+","+arr_place.get(1).getLat()+","+arr_place.get(2).getLat()+",0,0";
                    long_ = arr_place.get(0).getLong_()+","+arr_place.get(1).getLong_()+","+arr_place.get(2).getLong_()+",0,0";
                }else if(arr_place.size()==4){
                    lat_ = arr_place.get(0).getLat()+","+arr_place.get(1).getLat()+","+arr_place.get(2).getLat()+","
                            +arr_place.get(3).getLat()+",0";
                    long_ = arr_place.get(0).getLong_()+","+arr_place.get(1).getLong_()+","+arr_place.get(2).getLong_()
                            +","+arr_place.get(3).getLong_()+",0";
                }else if(arr_place.size()==5){
                    lat_ = arr_place.get(0).getLat()+","+arr_place.get(1).getLat()+","+arr_place.get(2).getLat()+","
                            +arr_place.get(3).getLat()+","+arr_place.get(4).getLat();
                    long_ = arr_place.get(0).getLong_()+","+arr_place.get(1).getLong_()+","+arr_place.get(2).getLong_()
                            +","+arr_place.get(3).getLong_()+","+arr_place.get(4).getLong_();
                }
                if (sharedPreferenceLeadr.get_DIMENSION().equalsIgnoreCase("0")) {
                    dimension = "mi";
                } else {
                    dimension = "km";
                }
            }
        }
      /*  Log.e("location",city_name);
        Log.e("country",country_name);
        Log.e("lat_",lat_);
        Log.e("long_",long_);
        Log.e("radius",radius);
        Log.e("ids: ",PAGINATION_IDS);
        Log.e("dimension: ",dimension);*/

        String min_budget = "0";
        if(sharedPreferenceLeadr.get_SWITCH_BUD().equalsIgnoreCase("1")) {
            if (!sharedPreferenceLeadr.get_MIN_BUDGET().equalsIgnoreCase("?")) {
                try {
                    float perc = 0.95f * Float.valueOf(sharedPreferenceLeadr.get_MIN_BUDGET());
                    min_budget = String.valueOf(Math.round(perc));
                } catch (Exception e) {
                    min_budget = sharedPreferenceLeadr.get_MIN_BUDGET();
                }
            }
        }
//        Log.e("min_budget",min_budget);

        String from_price = "0";
        String to_price = "0";
        if(sharedPreferenceLeadr.get_SWITCH_PRICE().equalsIgnoreCase("1")){
            from_price = sharedPreferenceLeadr.get_FROM_PRICE();
            to_price = sharedPreferenceLeadr.get_TO_PRICE();
        }
      /*  Log.e("from_price",from_price);
        Log.e("to_price",to_price);*/

        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.BUY_LOC1)
                .addBodyParameter("from_price",from_price)
                .addBodyParameter("to_price",to_price)
                .addBodyParameter("category_id",city_names)
                .addBodyParameter("min_budget",min_budget)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
//                .addBodyParameter("location",city_name)
//                .addBodyParameter("country",country_name)
                .addBodyParameter("day",day)
                .addBodyParameter("hour",hour)
                .addBodyParameter("lat",lat_)
                .addBodyParameter("lon",long_)
                .addBodyParameter("radius",radius)
                .addBodyParameter("radius_type",dimension)
                .addBodyParameter("lead_ids",PAGINATION_IDS)
                .setTag("signup").doNotCacheResponse()
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Activity activity = getActivity();
                        if(isAdded() && activity!=null){

                            utils.dismissProgressdialog();
//                        arr_buy.clear();
//                        arr_ids.clear();

                            try{
                                txt_buy_top.setVisibility(View.VISIBLE);
                                rel_no_show.setVisibility(View.GONE);
                                progresS_load.setVisibility(View.GONE);
                            }catch (Exception e){}

                            Log.e("res_buy: ", response + "");
                            if (response.optString("status").equals("1")) {
                                arr_buy.remove(arr_buy.get(arr_buy.size()-1));
                                try {
                                    JSONArray arr = response.getJSONArray("detail");
                                    for (int i = 0; i < arr.length(); i++) {
                                        JSONObject obj = arr.getJSONObject(i);
                                        BuyPojo item = new BuyPojo();
                                        item.setAudio(obj.optString("audio"));
                                        item.setBudget(obj.optString("budget"));
                                        item.setBusiness_fb_page(obj.optString("business_fb_page"));
                                        item.setBusiness_image(obj.optString("business_image"));
                                        String bus_name = obj.optString("business_name");
                                        try {
                                            try {
                                                bus_name = new String(Base64.decode(bus_name.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        String bus_info = obj.optString("business_info");

                                        try {
                                            bus_info = new String(Base64.decode(bus_info.getBytes(),Base64.DEFAULT), "UTF-8");
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }

                                        item.setBusiness_info(bus_info);
                                        item.setBusiness_name(bus_name);
                                        item.setCategory(obj.optString("category"));
                                        item.setCell_number(obj.optString("cell_number"));

                                        String client_name = obj.optString("client_name");
                                        try {
                                            try {
                                                client_name = new String(Base64.decode(client_name.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        if(client_name!=null){
                                            if(client_name.equalsIgnoreCase(" ")){
                                                if(getActivity()!=null) {
                                                    client_name = getResources().getString(R.string.unknw);
                                                }
                                            }else if(client_name.equalsIgnoreCase("")){
                                                if(getActivity()!=null) {
                                                    client_name = getResources().getString(R.string.unknw);
                                                }
                                            }
                                        }else{
                                            if(getActivity()!=null) {
                                                client_name = getResources().getString(R.string.unknw);
                                            }
                                        }
                                        item.setClient_name(client_name);

                                        item.setId(obj.optString("id"));

                                        //if it 0 then it means lead is in active
                                        item.setIs_delete(obj.optString("is_delete"));
                                        item.setLead_id(obj.optString("lead_id"));
                                        item.setLead_price(obj.optString("lead_price"));
                                        item.setPhone_number(obj.optString("phone_number"));
                                        item.setType(obj.optString("type"));
                                        item.setUser_id(obj.optString("user_id"));
                                        item.setUser_image(obj.optString("user_image"));
                                        if(obj.optString("user_name").equalsIgnoreCase("user deleted account")){
                                            if(getActivity()!=null) {
                                                item.setUser_name(getResources().getString(R.string.usr_del));
                                                item.setJob_title(getResources().getString(R.string.usr_del));
                                                item.setDescription(getResources().getString(R.string.usr_del));
                                            }
                                        }else {
                                            String usr_nm = obj.optString("user_name");
                                            try {
                                                try {
                                                    usr_nm = new String(Base64.decode(usr_nm.getBytes(),Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (IllegalArgumentException e) {
                                                e.printStackTrace();
                                            }

                                            item.setUser_name(usr_nm);

                                            String job_tit = obj.optString("job_title");
                                            try {
                                                try {
                                                    job_tit = new String(Base64.decode(job_tit.getBytes(),Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (IllegalArgumentException e) {
                                                e.printStackTrace();
                                            }

                                            item.setJob_title(job_tit);

                                            String desc = obj.optString("description");
                                            try {
                                                try {
                                                    desc = new String(Base64.decode(desc.getBytes(),Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (IllegalArgumentException e) {
                                                e.printStackTrace();
                                            }

                                            item.setDescription(desc);
                                        }
                                        item.setLat(obj.optString("lat"));
                                        item.setLon(obj.optString("lon"));

                                        String locnm = obj.optString("location_name");
                                        try {
                                            try {
                                                locnm = new String(Base64.decode(locnm.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        item.setLocation_name(locnm);

                                        try{
                                            String full_dat_tym_buy = obj.optString("created_time");
                                            String[] spiltby_T_buy = full_dat_tym_buy.split("T");
                                            String date_buy = spiltby_T_buy[0].trim();
                                            String tym_wid_Z_buy = spiltby_T_buy[1].trim();
                                            String spiltby_Dot1_buy = tym_wid_Z_buy.replace(".000Z","");
                                            String replace_tym_buy = spiltby_Dot1_buy.replace(":","-");
                                            item.setCreated_date(date_buy);
                                            item.setCreated_time(replace_tym_buy);

                                        }catch (Exception e){}

                                        item.setTime(obj.optString("time"));

                                        String address = obj.optString("address");
                                        try {
                                            try {
                                                address = new String(Base64.decode(address.getBytes(),Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }

                                        item.setAddress(address);
                                        item.setUser_thum(obj.optString("user_thum"));
                                        item.setCondition(false);
                                        item.setChange(false);

                                        if(PAGINATION_IDS.equalsIgnoreCase("0")) {
                                            PAGINATION_IDS = obj.optString("lead_id");
                                        }else{
                                            PAGINATION_IDS = PAGINATION_IDS + ","+obj.optString("lead_id");
                                        }
                                        arr_ids.add(obj.optString("lead_id"));
                                        arr_buy.add(item);
                                        if (i == arr.length() - 1) {
                                            arr_buy.add(item);
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if(response.optString("message").contains("Suspended account")){
                                    AccountKit.logOut();
                                    sharedPreferenceLeadr.clearPreference();
                                    String languageToLoad  = "en";
                                    Locale locale = new Locale(languageToLoad);
                                    Locale.setDefault(locale);
                                    Configuration config = new Configuration();
                                    config.locale = locale;
                                    getResources().updateConfiguration(config,getResources().getDisplayMetrics());

                                    Intent i_nxt = new Intent(getActivity(), GetStartedActivity.class);
                                    i_nxt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i_nxt);
                                    getActivity().finish();
                                }
                            }
                            if (getContext() != null) {
                                if (cardStack != null) {
                                    adapter.notifyDataSetChanged();
                                }
                            }


                            handler_timer.postDelayed(runnable_timer,40000);
                        /*if(arr_buy.size()==1){
                            try{
                                handler_refresh.postDelayed(runnable, 4000);
                            }catch (Exception e){}
                        }else{
                            try{
                                handler_refresh.removeCallbacks(runnable);
                            }catch (Exception e){}
                        }*/
                            count_arr = arr_buy.size();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Activity activity = getActivity();
                        if(isAdded() && activity!=null){
                            if(progresS_load!=null) {
                                progresS_load.setVisibility(View.GONE);
                            }
                            utils.dismissProgressdialog();
                        }
                    }
                });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_filter:
                if(getActivity()!=null) {
                    if (mMediaPlayer!=null){
                        if(mMediaPlayer.isPlaying()){
                            mMediaPlayer.stop();
                            mMediaPlayer.release();
                        }
                        mMediaPlayer = null;
                    }
                    if(countDownTimer!=null){
                        countDownTimer.cancel();
                        countDownTimer=null;
                    }
                    api_chck_AdminSettings();
                }
                break;

            case R.id.btn_buy:
                if(getActivity()!=null) {
                    Intent i_chang = new Intent(getActivity(), FilterActivity.class);
                    startActivity(i_chang);
                }
                break;
        }
    }

    @Override
    public void networkAvailable() {
        if(dialog_internet!=null){
            if(dialog_internet.isShowing()){
                dialog_internet.dismiss();
            }
            if(arr_buy!=null){
                if(arr_buy.size()<2){
                    OnResumeMethodApiCalled();
                }
            }else{
                OnResumeMethodApiCalled();
            }
        }
       /* try{
            handler_refresh.postDelayed(runnable, 1000);
        }catch (Exception e){}*/
    }

    @Override
    public void networkUnavailable() {
        dialog_msg_show(getActivity(),getResources().getString(R.string.no_internet));
    }


    class BuyAdapter extends BaseAdapter {
        private List<BuyPojo> data;
        private Context       context;
        String                imge_user = "";

        public BuyAdapter(List<BuyPojo> data, Context context) {
            this.data = data;
            this.context = context;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType( int position ) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return getCount();
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if(view==null) {
                if (getContext() != null) {
                    LayoutInflater inflater = LayoutInflater.from(getContext());
                    view = inflater.inflate(R.layout.item_buy, parent, false);
                }
            }

            if(view!=null){
                SimpleDraweeView img_user = (SimpleDraweeView)
                        view.findViewById(R.id.img_user);
                TextView txt_client_name             = (TextView)view.findViewById(R.id.txt_client_name);
                TextView txt_des_client              = (TextView)view.findViewById(R.id.txt_des_client);
                TextView txt_client_phn              = (TextView)view.findViewById(R.id.txt_client_phn);
                final TextView txt_loc               = (TextView)view.findViewById(R.id.txt_loc);
                TextView txt_budget                  = (TextView)view.findViewById(R.id.txt_budget);
                TextView txt_des                     = (TextView)view.findViewById(R.id.txt_des);
                TextView txt_lead_price              = (TextView)view.findViewById(R.id.txt_lead_price);
                TextView txt_ago                     = (TextView)view.findViewById(R.id.txt_ago);
                TextView txt_seller                  = (TextView)view.findViewById(R.id.txt_seller);
                TextView txt_play                    = (TextView)view.findViewById(R.id.txt_play);
                TextView txt_read_more               = (TextView)view.findViewById(R.id.txt_read_more);
                TextView txt_by_now                  = (TextView)view.findViewById(R.id.txt_by_now);
                TextView txt_by_last                 = (TextView)view.findViewById(R.id.txt_by_last);
                final TextView txt_play_pause        = (TextView)view.findViewById(R.id.txt_pause_audio);
                final TextView txt_pause_audio       = (TextView)view.findViewById(R.id.txt_play_pause);
                final TextView txt_secs              = (TextView)view.findViewById(R.id.txt_secs);
                final SeekBar songProgressBar        = (SeekBar) view.findViewById(R.id.songProgressBar);
                RelativeLayout btn_buy               = (RelativeLayout)view.findViewById(R.id.btn_buy);
                final FrameLayout frame_main         = (FrameLayout)view.findViewById(R.id.frame_main);
                final RelativeLayout rel_no_show     = (RelativeLayout)view.findViewById(R.id.rel_no_show);
                RelativeLayout btn_buy_adap          = (RelativeLayout) view.findViewById(R.id.btn_buy_last);
                ImageView img_shadow                 = (ImageView) view.findViewById(R.id.img_shadow);
                View vw_line                         = (View) view.findViewById(R.id.vw_line);
                final  LinearLayout lnr_11           = (LinearLayout) view.findViewById(R.id.lnr_11);
                final  LinearLayout lnr_22           = (LinearLayout) view.findViewById(R.id.lnr_22);
                final  LinearLayout lnr_profile      = (LinearLayout) view.findViewById(R.id.lnr_profile);
                final  RelativeLayout rel_sold       = (RelativeLayout) view.findViewById(R.id.rel_sold);
                final  RelativeLayout rel_start      = (RelativeLayout) view.findViewById(R.id.rel_start);
                final  ImageView img_chat            = (ImageView) view.findViewById(R.id.img_chat);
                final  ImageView img_file_desc       = (ImageView) view.findViewById(R.id.img_file_desc);
                final  Button btn_nxt_sold           = (Button) view.findViewById(R.id.btn_nxt_sold);
                final  ProgressBar progress_one      = (ProgressBar) view.findViewById(R.id.progress_one);
                final  ProgressBar progress_two      = (ProgressBar) view.findViewById(R.id.progress_two);
                final  ImageView img_one             = (ImageView) view.findViewById(R.id.img_one);
                final  ImageView img_two             = (ImageView) view.findViewById(R.id.img_two);
                final  ProgressBar progres_load_user = (ProgressBar) view.findViewById(R.id.progres_load_user);

               /* songProgressBar.setOnTouchListener(new View.OnTouchListener(){
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });
*/
                if(img_user.getTag().equals("0")){
                    if(!data.get(position).getUser_thum().equals("")){
                        if(!data.get(position).getUser_thum().equals("0")){
                            if(data.get(position).getUser_thum().trim().length()>2){
                                progres_load_user.setVisibility(View.VISIBLE);

                                Picasso.with(getActivity())
                                        .load(data.get(position).getUser_thum()).transform(new CircleTransform())
                                        .into(img_user, new com.squareup.picasso.Callback() {
                                            @Override
                                            public void onSuccess() {
                                                progres_load_user.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {
                                                progres_load_user.setVisibility(View.GONE);
                                            }
                                        });
                                img_user.setTag("1");
                                imge_user = data.get(position).getUser_thum();
                            }else{
                                progres_load_user.setVisibility(View.GONE);
                                imge_user = "";
                            }
                        }else{
                            progres_load_user.setVisibility(View.GONE);
                            imge_user = "";
                        }
                    }else{
                        progres_load_user.setVisibility(View.GONE);
                        imge_user = "";
                    }
                }



                txt_seller.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_client_name.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_des_client.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_client_phn.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_budget.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_loc.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_play.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_des.setTypeface(utils.OpenSans_Light(getActivity()));
                txt_lead_price.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_read_more.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_ago.setTypeface(utils.OpenSans_Regular(getActivity()));
                txt_by_now.setTypeface(utils.Dina(getActivity()));
                txt_by_last.setTypeface(utils.Dina(getActivity()));
                btn_nxt_sold.setTypeface(utils.Dina(getActivity()));

                btn_nxt_sold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            handler_timer.removeCallbacks(runnable_timer);
                        }catch (Exception e){}
                        swipeRight();
//                        arr_buy.remove(arr_buy.get(position));
                    }
                });


                if(position!=data.size()-1){
                    if(!data.get(position).getCondition()) {
                        rel_start.setVisibility(View.VISIBLE);
                        rel_sold.setVisibility(View.GONE);
                    }else{
                        rel_start.setVisibility(View.GONE);
                        rel_sold.setVisibility(View.VISIBLE);
                    }
                    frame_main.setVisibility(View.VISIBLE);
                    rel_no_show.setVisibility(View.GONE);


                    lnr_22.setVisibility(View.GONE);
                    if(data.get(position).getAudio()!=null){
                        if(data.get(position).getAudio().trim().equals("")){
                            lnr_11.setVisibility(View.GONE);
                            img_file_desc.setVisibility(View.VISIBLE);
                        }else{
                            lnr_11.setVisibility(View.VISIBLE);
                            img_file_desc.setVisibility(View.INVISIBLE);
                        }
                    }else{
                        lnr_11.setVisibility(View.GONE);
                        img_file_desc.setVisibility(View.INVISIBLE);
                    }
                    try{
                        txt_secs.setText(data.get(position).getTime()+" "+getResources().getString(R.string.secs));
                    }catch (Exception e){}

                    txt_client_name.setText(data.get(position).getUser_name());
                    txt_des.setText(data.get(position).getDescription());
                    txt_client_phn.setText(data.get(position).getCell_number().substring(0,5)+getResources().getString(R.string.xx));
                    txt_des_client.setText(data.get(position).getBusiness_name());
                    if(data.get(position).getLead_price()!=null){
                        if(!data.get(position).getLead_price().equals("0")){
                            if(!data.get(position).getLead_price().equals("")){
                                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                    txt_lead_price.setText(getResources().getString(R.string.dollar)+" "+Math.round(Float.valueOf(data.get(position).getLead_price())*
                                            Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                                }else{
                                    txt_lead_price.setText(getResources().getString(R.string.dollar)+" "+data.get(position).getLead_price());
                                }
                            }else{
                                txt_lead_price.setText(getResources().getString(R.string.free));
                            }
                        }else{
                            txt_lead_price.setText(getResources().getString(R.string.free));
                        }
                    }else{
                        txt_lead_price.setText(getResources().getString(R.string.free));
                    }


                    int screenSize = getResources().getConfiguration().screenLayout &
                            Configuration.SCREENLAYOUT_SIZE_MASK;

                    String gg = data.get(position).getDescription();
                    if(gg.contains("\n")){
                        gg = data.get(position).getDescription().replaceAll("\n","...................");
                    }

                    if(screenSize==Configuration.SCREENLAYOUT_SIZE_LARGE){
                        if(gg.length()>150){
                            txt_read_more.setVisibility(View.VISIBLE);
                            img_shadow.setVisibility(View.VISIBLE);
                            vw_line.setVisibility(View.GONE);
                        }else{
                            txt_read_more.setVisibility(View.INVISIBLE);
                            img_shadow.setVisibility(View.GONE);
                            vw_line.setVisibility(View.VISIBLE);
                        }
                    }
                    else if(screenSize==Configuration.SCREENLAYOUT_SIZE_NORMAL){
                        DisplayMetrics metrics = new DisplayMetrics();
                        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
                        int density = metrics.densityDpi;

                        if (String.valueOf(density).equalsIgnoreCase("640")) {
                            if (gg.length() > 120) {
                                txt_read_more.setVisibility(View.VISIBLE);
                                img_shadow.setVisibility(View.VISIBLE);
                                vw_line.setVisibility(View.GONE);
                            } else {
                                txt_read_more.setVisibility(View.INVISIBLE);
                                img_shadow.setVisibility(View.GONE);
                                vw_line.setVisibility(View.VISIBLE);
                            }

                        }else {
                            if (gg.length() > 80) {
                                txt_read_more.setVisibility(View.VISIBLE);
                                img_shadow.setVisibility(View.VISIBLE);
                                vw_line.setVisibility(View.GONE);
                            } else {
                                txt_read_more.setVisibility(View.INVISIBLE);
                                img_shadow.setVisibility(View.GONE);
                                vw_line.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    else if(screenSize==Configuration.SCREENLAYOUT_SIZE_SMALL){
                        if(gg.length()>40){
                            txt_read_more.setVisibility(View.VISIBLE);
                            img_shadow.setVisibility(View.VISIBLE);
                            vw_line.setVisibility(View.GONE);
                        }else{
                            txt_read_more.setVisibility(View.INVISIBLE);
                            img_shadow.setVisibility(View.GONE);
                            vw_line.setVisibility(View.VISIBLE);
                        }
                    }

                    if(data.get(position).getLat().equals("0")){
                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                    }
                    else if(data.get(position).getAddress()!=null){
                        if(!data.get(position).getAddress().equals("0")){
                            if(!data.get(position).getAddress().equals("")){
                                txt_loc.setText(data.get(position).getAddress());
                                txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                            }else{
                                if(data.get(position).getLocation_name()!=null){
                                    if(!data.get(position).getLocation_name().equals("0")){
                                        if(!data.get(position).getLocation_name().equals("")){
                                            txt_loc.setText(data.get(position).getLocation_name());
                                        }else{
                                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                        }
                                    }else{
                                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                    }

                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }
                            }
                        }else{
                            if(data.get(position).getLocation_name()!=null){
                                if(!data.get(position).getLocation_name().equals("0")){
                                    if(!data.get(position).getLocation_name().equals("")){
                                        txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                        txt_loc.setText(data.get(position).getLocation_name());
                                    }else{
                                        txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                        txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                    }
                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }

                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }
                        }
                    }else{
                        if(data.get(position).getLocation_name()!=null){
                            if(data.get(position).getLocation_name().equals("null")){
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }
                            else if(!data.get(position).getLocation_name().equals("0")){
                                if(!data.get(position).getLocation_name().equals("")){
                                    txt_loc.setTextColor(getResources().getColor(R.color.black_loc));
                                    txt_loc.setText(data.get(position).getLocation_name());
                                }else{
                                    txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                    txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                                }
                            }else{
                                txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                                txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                            }

                        }else{
                            txt_loc.setTextColor(getResources().getColor(R.color.gray_budget));
                            txt_loc.setText(getResources().getString(R.string.loc_unknwn));
                        }
                    }
                    if(data.get(position).getBudget().equals("0")){
                        txt_budget.setTextColor(getResources().getColor(R.color.gray_budget));
                        txt_budget.setText(getResources().getString(R.string.bud_unknw));
                    }else {
                        if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                            txt_budget.setText(getResources().getString(R.string.dollar)+" "+Math.round(Float.valueOf(data.get(position).getBudget())*
                                    Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                        }else{
                            txt_budget.setText(getResources().getString(R.string.dollar)+" " + data.get(position).getBudget() + " "+getResources().getString(R.string.budgt_sel));
                        }
                    }
                }else{
                    frame_main.setVisibility(View.GONE);
                    rel_no_show.setVisibility(View.VISIBLE);
                }

                btn_buy_adap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (getActivity() != null) {
                            if(!arr_buy.get(position).getUser_name().equalsIgnoreCase(context.getResources().getString(R.string.usr_del))){
                                if (mMediaPlayer!=null){
                                    if(mMediaPlayer.isPlaying()){
                                        mMediaPlayer.stop();
                                        mMediaPlayer.release();
                                    }
                                    mMediaPlayer = null;
                                }
                                if(countDownTimer!=null){
                                    countDownTimer.cancel();
                                    countDownTimer=null;
                                }
                                Intent i_chang = new Intent(getActivity(), FilterActivity.class);
                                startActivity(i_chang);
                            }

                        }
                    }
                });

                txt_read_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (getActivity() != null) {
                            if(!arr_buy.get(position).getUser_name().equalsIgnoreCase(context.getResources().getString(R.string.usr_del))) {
                                try {
                                    handler_timer.removeCallbacks(runnable_timer);
                                } catch (Exception e) {
                                }
                                dialog_LongLead(getActivity(), data.get(position).getDescription(),
                                        getResources().getString(R.string.lead_desc));
                            }
                        }
                    }
                });

                img_chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(getActivity()!=null) {
                            if(!arr_buy.get(position).getUser_name().equalsIgnoreCase(context.getResources().getString(R.string.usr_del))) {


                                String img_us = "";
                                if (data.get(position).getUser_thum() != null) {
                                    if (!data.get(position).getUser_thum().equalsIgnoreCase("")) {
                                        if (!data.get(position).getUser_thum().equalsIgnoreCase("0")) {
                                            img_us = data.get(position).getUser_thum();
                                        }
                                    }
                                }
                                Intent i_chang = new Intent(getActivity(), ChatActivity.class);
                                i_chang.putExtra("image_other", img_us);
                                i_chang.putExtra("name_other", data.get(position).getUser_name());
                                i_chang.putExtra("buss_other", data.get(position).getBusiness_name());
                                i_chang.putExtra("info_other", data.get(position).getBuy_userbusinessinfo());
                                i_chang.putExtra("id_other", data.get(position).getUser_id());
                                i_chang.putExtra("leadid_other", data.get(position).getLead_id());
                                i_chang.putExtra("lead_user_id", data.get(position).getUser_id());
                                i_chang.putExtra("other_user_id", sharedPreferenceLeadr.getUserId());
                                i_chang.putExtra("audio", data.get(position).getAudio());
                                i_chang.putExtra("audiotime", data.get(position).getTime());
                                i_chang.putExtra("price_lead", data.get(position).getLead_price());
                                i_chang.putExtra("address", data.get(position).getAddress());
                                i_chang.putExtra("budget", data.get(position).getBudget());
                                i_chang.putExtra("category", data.get(position).getCategory());
                                i_chang.putExtra("desc", data.get(position).getDescription());
                                i_chang.putExtra("typemy", "I Buy");
                                i_chang.putExtra("typeother", "I Sell");
                                startActivity(i_chang);
                            }
                        }
                    }
                });

                txt_ago.setText(method_date_toseT(position));

                btn_buy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!arr_buy.get(position).getUser_name().equalsIgnoreCase(context.getResources().getString(R.string.usr_del))) {
                            if (progresS_load != null) {
                                progresS_load.setVisibility(View.VISIBLE);
                            }

                            if (data.get(position).getLead_price().equals("0")) {
                                api_CheckStatus(data.get(position).getLead_id(), data.get(position).getClient_name(),
                                        data.get(position).getCell_number(), rel_start, rel_sold, data.get(position).getUser_id(), position);
                            } else if (!data.get(position).getLead_price().equals("")) {
                                api_VaidateCard(data.get(position).getLead_id(), data.get(position).getClient_name(),
                                        data.get(position).getCell_number(), rel_start, rel_sold, data.get(position).getUser_id(), position);
                            } else {
                                api_VaidateCard(data.get(position).getLead_id(), data.get(position).getClient_name(),
                                        data.get(position).getCell_number(), rel_start, rel_sold, data.get(position).getUser_id(), position);
                            }
                        }
//                        dialog_msg_Timer();
                    /*dialog_AddCard(data.get(position).getLead_id(), data.get(position).getClient_name(),
                            data.get(position).getPhone_number(), rel_start, rel_sold,data.get(position).getUser_id(),position);*/
                    }

                });

                lnr_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (getActivity() != null) {
                            if(!arr_buy.get(position).getUser_name().equalsIgnoreCase(context.getResources().getString(R.string.usr_del))) {
                                Intent i_pro = new Intent(getActivity(), ProfileOther_Activity.class);
                                i_pro.putExtra("id", data.get(position).getUser_id());
                                startActivity(i_pro);
                            }
                        }
                    }
                });

                txt_play_pause.setTag("0");

                txt_pause_audio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!arr_buy.get(position).getUser_name().equalsIgnoreCase(context.getResources().getString(R.string.usr_del))) {
                            lnr_11.performClick();
                        }
                    }
                });


                lnr_11.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!arr_buy.get(position).getUser_name().equalsIgnoreCase(context.getResources().getString(R.string.usr_del))) {
                            try {
                                handler_timer.removeCallbacks(runnable_timer);
                            } catch (Exception e) {
                            }

                            lnr_11.setVisibility(View.GONE);
                            lnr_11.setClickable(false);
                            lnr_22.setVisibility(View.VISIBLE);
                            if (txt_play_pause.getTag().equals("0")) {
                                if (resume_pos == 0) {
                                    progress_one.setVisibility(View.VISIBLE);
                                    progress_two.setVisibility(View.VISIBLE);
                                    img_one.setVisibility(View.GONE);
                                    img_two.setVisibility(View.GONE);
                                    txt_play_pause.setTag("1");
//                                    onPlay(false);
                                    new Handler().postDelayed(new Runnable() {

                                        @Override
                                        public void run() {
                                            progress_one.setVisibility(View.GONE);
                                            progress_two.setVisibility(View.GONE);
                                            img_one.setVisibility(View.VISIBLE);
                                            img_two.setVisibility(View.VISIBLE);
                                            txt_play_pause.setTag("1");
                                            onPlay(false);
                                        }

                                    }, 300);
                                } else {
                                    txt_play_pause.setTag("1");
                                    onPlay(false);
                                }
                            } else {
                                txt_play_pause.setTag("0");
                                onPlay(true);
                            }
                        }
                    }
                    //
                    private void onPlay(boolean isPlaying2){
                        if (!isPlaying2) {
                            //currently MediaPlayer is not playing audio
                            if(mMediaPlayer == null) {
                                txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
                                startPlaying(); //start from beginning
                                isPlaying = !isPlaying;
                            } else {
                                txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pausse_buy, 0, 0, 0);
                                resumePlaying(); //resume the currently paused MediaPlayer
                            }
                        } else {
                            //pause the MediaPlayer
                            txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pause_sell, 0, 0, 0);
                            pausePlaying();
                        }
                        /*lnr_11.setVisibility(View.GONE);
//                        lnr_11.setClickable(false);
                        lnr_22.setVisibility(View.VISIBLE);*/
                    }

                    private void startPlaying() {
                        mMediaPlayer = new MediaPlayer();
                        try {
                            mMediaPlayer.setDataSource(data.get(position).getAudio());
                            mMediaPlayer.prepare();

                            count_start_pause_add = 0;
                            count_start_pause = Integer.valueOf(data.get(position).getTime())+2;

                            songProgressBar.setMax(mMediaPlayer.getDuration());
                            songProgressBar.setProgress(0);
                            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer mp) {
                                    mMediaPlayer.start();
                                    progress_one.setVisibility(View.GONE);
                                    progress_two.setVisibility(View.GONE);
                                    img_one.setVisibility(View.VISIBLE);
                                    img_two.setVisibility(View.VISIBLE);
                                }
                            });
                        } catch (IOException e) {
                            Log.e("", "prepare() failed");
                        }

                        songProgressBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged( SeekBar seekBar, int progress, boolean fromUser ) {

                            }

                            @Override
                            public void onStartTrackingTouch( SeekBar seekBar ) {
                                if(mHandler_seekbar!=null){
                                    mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                                }
                            }

                            @Override
                            public void onStopTrackingTouch( SeekBar seekBar ) {
                                if(mHandler_seekbar!=null){
                                    mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                                }

                                // forward or backward to certain seconds
                                if(mMediaPlayer!=null) {
                                    mMediaPlayer.seekTo(seekBar.getProgress());
                                }
                                method_countDown_Seekbar();
                                if(countDownTimer!=null){
                                    countDownTimer.cancel();
                                }
//                                Log.e("progress seek: ",seekBar.getProgress()+"");
//                                Log.e("progress seek2: ",mMediaPlayer.getCurrentPosition()+"");
                                count_start_pause = Integer.valueOf(data.get(position).getTime())+2-seekBar.getProgress()/1000;
                                method_countDown(Integer.valueOf(data.get(position).getTime())+1-seekBar.getProgress()/1000);
                            }
                        });

                        count_start_pause_add = 0;
                        method_countDown(Integer.valueOf(data.get(position).getTime())+1);
                        method_countDown_Seekbar();

                        mMediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                            @Override
                            public void onSeekComplete(MediaPlayer mp) {

                            }
                        });
                        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                stopPlaying();
                            }
                        });

                        //keep screen on while playing audio
                        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    }

                    void method_countDown_Seekbar(){
                        mHandler_seekbar = new Handler();
                        mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);

                    }

                    Runnable mRunnabl_seekbar = new Runnable() {
                        @Override
                        public void run() {
                            if(mMediaPlayer != null){
//                Log.e("seejbr: ",mCurrentPosition+"");
//                                Log.e("seejbr2: ",mMediaPlayer.getCurrentPosition()+"");
                                try{
                                    songProgressBar.setProgress(mMediaPlayer.getCurrentPosition());
                                }catch (Exception e){}

                            }
                            mHandler_seekbar.postDelayed(this, 15);
                        }
                    };


                    void method_countDown(int timer){
                        countDownTimer = null;
                        countDownTimer = new CountDownTimer(timer*1000, 1000) {
                            public void onTick(long millisUntilFinished) {
                                long millis = millisUntilFinished;
                                //Convert milliseconds into hour,minute and seconds
                                String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) -
                                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                        TimeUnit.MILLISECONDS.toSeconds(millis) -
                                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

                                txt_play_pause.setText(hms+"");

                                count_start_pause = count_start_pause-1;
                                count_start_pause_add = count_start_pause_add+1;

//                                songProgressBar.setProgress(count_start_pause_add);
                                if(count_start_pause==1){
//                                    songProgressBar.setProgress(count_start_pause_add);
                                    stopPlaying();
                                }
                            }
                            public void onFinish() {
                                countDownTimer = null;//set CountDownTimer to null
                            }
                        }.start();
                    }

                    private void updateSeekBar() {
                        mHandler.postDelayed(mRunnable, 1000);
                    }

                    private void pausePlaying() {
                        mHandler.removeCallbacks(mRunnable);
                        if(mMediaPlayer!=null) {
                            resume_pos = mMediaPlayer.getCurrentPosition();
                        }
                        if(countDownTimer!=null) {
                            countDownTimer.cancel();
                        }
                        if(mMediaPlayer!=null) {
                            mMediaPlayer.pause();
                        }

                        if(mHandler_seekbar!=null){
                            mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                        }
                    }

                    private void resumePlaying() {
//        mMediaPlayer.seekTo(resume_pos);
                        mHandler.removeCallbacks(mRunnable);
                        mMediaPlayer.start();
                        updateSeekBar();
                        method_countDown(count_start_pause-1);
                        mHandler_seekbar.postDelayed(mRunnabl_seekbar, 15);
//        updateSeekBar();
                    }

                    private void stopPlaying() {
                        if(getActivity()!=null) {
                            lnr_11.setClickable(true);
                            mHandler.removeCallbacks(mRunnable);
                            mHandler_seekbar.removeCallbacks(mRunnabl_seekbar);
                            mMediaPlayer.stop();
                            mMediaPlayer.reset();
                            mMediaPlayer.release();
                            mMediaPlayer = null;
                            resume_pos = 0;
                            lnr_11.setVisibility(View.VISIBLE);
                            lnr_22.setVisibility(View.GONE);

                            isPlaying = !isPlaying;
//                        songProgressBar.setProgress(songProgressBar.getMax());

                            if (countDownTimer != null) {
                                countDownTimer.cancel();
                            }
                            txt_play_pause.setTag("0");
                            //allow the screen to turn off again once audio is finished playing
                            if (getActivity() != null) {
                                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                            }

                            try {
                                handler_timer.postDelayed(runnable_timer, 40000);
                            } catch (Exception e) {
                            }
                            songProgressBar.setProgress(0);
                            txt_pause_audio.setCompoundDrawablesWithIntrinsicBounds(R.drawable.pause_sell, 0, 0, 0);
                        }
                    }

                    //updating mSeekBar
                    private Runnable mRunnable = new Runnable() {
                        @Override
                        public void run() {
                            if(mMediaPlayer != null){

                                int mCurrentPosition = mMediaPlayer.getCurrentPosition();
                                long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);
                                long seconds = TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition)
                                        - TimeUnit.MINUTES.toSeconds(minutes);
                                updateSeekBar();
                            }
                        }
                    };
                });
            }


            return view;
        }



        private String method_date_toseT(int position) {
            String date_return = "";
            try {
                //Dates to compare
                String CurrentDate=  CurrentDate();
                String CurrentTime=  CurrentTime();
                String FinalDate=  getOnlyDate(data.get(position).getCreated_date()+ " "+ data.get(position).getCreated_time());
                String get_time =  data.get(position).getCreated_time();
                String FinalTime=  getDate(data.get(position).getCreated_date() + " "+get_time);

                Date date1;
                Date date1_T;
                Date date2;
                Date date2_T;

                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
                SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

                //Setting dates
                date1 = dates.parse(CurrentDate);
                date1_T = times.parse(FinalTime);
                date2 = dates.parse(FinalDate);
                date2_T = times.parse(CurrentTime);

                //Comparing dates
                long difference = Math.abs(date1.getTime() - date2.getTime());
                long differenceDates = difference / (24 * 60 * 60 * 1000);
                long mills =   date2_T.getTime()-date1_T.getTime();

                String api_year = FinalDate.split("-")[0];
                String curr_year = CurrentDate.split("-")[0];
                String sub_year = "0";

                try{
                    sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
                }catch (Exception e){}

                //Convert long to String
                String dayDifference = Long.toString(differenceDates);

                if(sub_year.equals("1")){
                    date_return = getActivity().getResources().getString(R.string.few_mnth);
                }
                else if(sub_year.equals("2")){
                    date_return = getActivity().getResources().getString(R.string.year_ago);
                }
                else if(Integer.valueOf(dayDifference)>0){
                    if(dayDifference.equals("1")){
                        date_return = dayDifference + getActivity().getResources().getString(R.string.day);
                    }else{
                        date_return = dayDifference + getActivity().getResources().getString(R.string.days);
                    }
                }else{
                    int hours = (int) (mills / (1000 * 60 * 60));
                    int minutes = (int) (mills / (1000 * 60));
                    int seconds = (int) (mills / (1000));
                    if(hours>0){
                        date_return = hours+" "+getResources().getString(R.string.hr);
                    }else if(minutes>0){
                        date_return = minutes+" "+getResources().getString(R.string.min);
                    }else{
                        date_return = getResources().getString(R.string.few_sec_full);
                    }
                }

            } catch (Exception exception) {
                date_return = getActivity().getResources().getString(R.string.fewdays);
            }
            return date_return;
        }
    }


    private String getDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);

        }
        catch (Exception e)
        {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }

    private String getOnlyDate(String OurDate_) {
        String chk_date = OurDate_;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate_);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); //this format changeable
//            Log.e("time: ",TimeZone.getDefault()+"");
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate_ = dateFormatter.format(value);
        }
        catch (Exception e) {
            OurDate_ = chk_date;
        }
        return OurDate_;
    }


    String CurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    String CurrentDate_withoutTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    String CurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }



    private void api_VaidateCard(final String lead_id, final String client_name, final String phone, final View vw_start
            , final View vw_sold, final String UserId, final int position){
        AndroidNetworking.enableLogging();
//        utils.showProgressDialog(getActivity(),getResources().getString(R.string.load));
        Log.e("url_chkcard: ", NetworkingData.BASE_URL+ NetworkingData.CHECK_CARD);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_CARD)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chkCard: ",result+"");
                        utils.dismissProgressdialog();
                        //{"status":"0"}
                        if(result.optString("status").equals("0")){
                            if(progresS_load!=null) {
                                progresS_load.setVisibility(View.GONE);
                            }
                            sharedPreferenceLeadr.set_CARD("N/A");
                            dialog_AddCard(lead_id, client_name, phone,vw_start,vw_sold,UserId,position);
                        }else{
//                            Log.e("res== : ",result.optString("stripe_id"));
                            if(result.optString("stripe_id").equalsIgnoreCase("null")){
                                if(progresS_load!=null) {
                                    progresS_load.setVisibility(View.GONE);
                                }
                                sharedPreferenceLeadr.set_CARD("N/A");
                                dialog_AddCard(lead_id, client_name, phone,vw_start,vw_sold,UserId,position);
                            }else{
                                api_CheckStatus(lead_id, client_name, phone,vw_start,vw_sold,UserId, position);
                            }

//                            dialog_AddCard(lead_id, client_name, phone);
                        }
//
                       /**/
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();
                        if(progresS_load!=null) {
                            progresS_load.setVisibility(View.GONE);
                        }
                    }
                });

    }



    private void api_CheckStatus(final String lead_id, final String client_name, final String phone, final View vw_start
            , final View vw_sold, final String userId, final int position){
        AndroidNetworking.enableLogging();

        utils.dismissProgressdialog();
        if(progresS_load!=null){
            if(progresS_load.getVisibility()==View.GONE){
                progresS_load.setVisibility(View.VISIBLE);
            }
        }
        Log.e("url_chkStatus: ", NetworkingData.BASE_URL+ NetworkingData.CHECK_BUY_STATUS);
//        Log.e("lead_id: ", lead_id);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_BUY_STATUS)
                .addBodyParameter("lead_id",lead_id)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chkStatus: ",result+"");
                        if(result.optString("status").equals("1")){
                           /* AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                            logger.logEvent("EVENT_NAME_PURCHASED");*/
                            api_Buy(lead_id, client_name,
                                    phone,vw_start,vw_sold,userId,position);
                        }else{
                            if(result.optString("message").equalsIgnoreCase("Lead deleted!")){
                                try{
                                    handler_timer.removeCallbacks(runnable_timer);
                                }catch (Exception e){}
                                dialog_leadRemoved(getActivity(),getResources().getString(R.string.lead_remv));
                            }
                            else if(result.optString("message").equalsIgnoreCase("Already bought!")){

                                try{
                                    handler_timer.removeCallbacks(runnable_timer);
                                }catch (Exception e){}
                                dialog_leadRemoved(getActivity(),getResources().getString(R.string.lead_alrdy));
                            }
                            else if(result.optString("message").contains("in edit mode!")){
                                try{
                                    handler_timer.removeCallbacks(runnable_timer);
                                }catch (Exception e){}
                                dialog_leadRemoved(getActivity(),getResources().getString(R.string.lead_remvd));
                            }else{
                                vw_start.setVisibility(View.GONE);
                                vw_sold.setVisibility(View.VISIBLE);
                            }
                            if(progresS_load!=null) {
                                progresS_load.setVisibility(View.GONE);
                            }
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}

                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        utils.dismissProgressdialog();
                        if(progresS_load!=null) {
                            progresS_load.setVisibility(View.GONE);
                        }
                    }
                });



    }


    public  void dialog_leadRemoved(Activity activity, String msg){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog.setContentView(bottomSheetView);
        dialog.setCancelable(true);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        img_remove.setVisibility(View.GONE);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                swipeRight();
                img_undo.setVisibility(View.GONE);
            }
        });
        try{
            dialog.show();
        }catch (Exception e){

        }

        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                swipeRight();
            }
        });

    }

    private void api_Buy(final String lead_id, final String client_name, final String phone, final View vw_start
            , final View vw_sold, final String userId,final int position){
        AndroidNetworking.enableLogging();
        Log.e("url_buy: ", NetworkingData.BASE_URL+ NetworkingData.BUY_LEAD);
//        Log.e("lead_id: ", lead_id);
//        Log.e("lead_user_id: ", userId);
//        Log.e("user_id: ", sharedPreferenceLeadr.getUserId());

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.BUY_LEAD)
                .addBodyParameter("lead_id",lead_id)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .addBodyParameter("lead_user_id",userId)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_buy: ",result+"");
                        if(progresS_load!=null) {
                            progresS_load.setVisibility(View.GONE);
                        }
                        if(result.optString("status").equals("1")){
                            sharedPreferenceLeadr.set_SELECTOR_BOUGHT("0");
                            Bundle parameters = new Bundle();
                            parameters.putString("EVENT_PARAM_DESCRIPTION", arr_buy.get(position).getAddress()+","+
                                    arr_buy.get(position).getBudget()+","+arr_buy.get(position).getDescription()+","+
                                    arr_buy.get(position).getCategory());

                            parameters.putString("EVENT_PARAM_LEVEL", method_date_toseT(position));
                            parameters.putString("Value", method_date_toseT(position));

                            AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
                            Double price = 0.0;


                            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                try{
                                    price = Double.valueOf(Math.round(Float.valueOf(arr_buy.get(position).getLead_price())*
                                            Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                                }catch (Exception e){}
                            }else{
                                try{
                                    price = Double.valueOf(Float.valueOf(arr_buy.get(position).getLead_price()));
                                }catch (Exception e){}
                            }
                            parameters.putString("ValueToSUM", price+" "+getResources().getString(R.string.dollar));
                            String value = "";
                            if(arr_buy.get(position).getAudio()!=null){
                                if(!arr_buy.get(position).getAudio().trim().equalsIgnoreCase("")){
                                    value = "voice";
                                }
                            }
                            if(arr_buy.get(position).getDescription()!=null){
                                if(!arr_buy.get(position).getDescription().trim().equalsIgnoreCase("")) {
                                    if (value.equalsIgnoreCase("")) {
                                        value = "text";
                                    } else {
                                        value = value + "text";
                                    }
                                }
                            }
                            parameters.putString("Type_of_lead", value);

                            logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance("USD"));

                            logger.logEvent("EVENT_NAME_PURCHASED",price,
                                    parameters);

                            try{
                                try{
                                    handler_timer.removeCallbacks(runnable_timer);
                                    remove_Callback();
                                }catch (Exception e){}
                                swipeRight();
//                              arr_buy.remove(arr_buy.get(position));

                                dialog_SuccessBuy(lead_id,client_name,phone,vw_start,vw_sold);
                            }catch (Exception e){}

                        }else if(result.optString("msg").contains("You have problem with stripe")){
                            dialog_errorCard(lead_id,client_name,phone,vw_start,vw_sold,userId, position);
                        }else if(result.optString("msg").equalsIgnoreCase("You have bought three leads within one hour..")){
//                            utils.dialog_msg_show(getActivity(),result.optString("msg"));
                            String da_2 = result.optString("time");
//                            utils.dialog_msg_show(getActivity(),result.optString("msg"));
                            String full_time = getDate(CurrentDate_withoutTime()+" "+da_2);

                            try{
                                Date date1;
                                Date date1_T;
                                Date date2;
                                Date date2_T;

                                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

                                //Setting dates
                                date1 = dates.parse(CurrentDate());
                                date1_T = dates.parse(full_time);

                                long difference = Math.abs(date1.getTime() - date1_T.getTime());
                                int diff_int = 3600000 - Integer.valueOf((int) difference);
                                api_GetCount(Long.valueOf(diff_int));
//                                Log.e("full time : ",full_time);
//                                Log.e("difference : ",difference+"");
                            }catch (Exception e){}
                            //"time":"10-31-25"
                        }else{

                            BuyPojo item = new BuyPojo();
                            item.setCreated_time(arr_buy.get(position).getCreated_time());

                            item.setAudio(arr_buy.get(position).getAudio());
                            item.setBudget(arr_buy.get(position).getBudget());
                            item.setBusiness_fb_page(arr_buy.get(position).getBusiness_fb_page());
                            item.setBusiness_image(arr_buy.get(position).getBusiness_image());
                            item.setBusiness_info(arr_buy.get(position).getBusiness_info());
                            item.setBusiness_name(arr_buy.get(position).getBusiness_name());
                            item.setCategory(arr_buy.get(position).getCategory());
                            item.setCell_number(arr_buy.get(position).getCell_number());
                            item.setClient_name(arr_buy.get(position).getClient_name());
                            item.setDescription(arr_buy.get(position).getDescription());
                            item.setId(arr_buy.get(position).getId());
                            item.setJob_title(arr_buy.get(position).getJob_title());
                            //if it 0 then it means lead is in active
                            item.setIs_delete(arr_buy.get(position).getIs_delete());
                            item.setLead_id(arr_buy.get(position).getLead_id());
                            item.setLead_price(arr_buy.get(position).getLead_price());
                            item.setPhone_number(arr_buy.get(position).getPhone_number());
                            item.setType(arr_buy.get(position).getType());
                            item.setUser_id(arr_buy.get(position).getUser_id());
                            item.setUser_image(arr_buy.get(position).getUser_image());
                            item.setUser_name(arr_buy.get(position).getUser_name());
                            item.setLat("");
                            item.setLon("");
                            item.setLocation_name(arr_buy.get(position).getLocation_name());
                            item.setTime(arr_buy.get(position).getTime());
                            item.setCountry(arr_buy.get(position).getCountry());
                            item.setAddress(arr_buy.get(position).getAddress());
                            item.setUser_thum(arr_buy.get(position).getUser_thum());
                            item.setCondition(true);

                            arr_buy.add(position,item);
                            adapter.notifyDataSetChanged();

                            vw_start.setVisibility(View.GONE);
                            vw_sold.setVisibility(View.VISIBLE);

                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.i("", "---> On error  ");
                        if(progresS_load!=null) {
                            progresS_load.setVisibility(View.GONE);
                        }
                        try{
                            try{
                                handler_timer.removeCallbacks(runnable_timer);
                            }catch (Exception e){}

                            swipeRight();
                            arr_buy.remove(arr_buy.get(position));
                            dialog_SuccessBuy(lead_id,client_name,phone,vw_start,vw_sold);
                        }catch (Exception e){}

                        utils.dismissProgressdialog();
                    }

                    private String method_date_toseT(int position) {
                        String date_return = "";
                        try {
                            //Dates to compare
                            String CurrentDate=  CurrentDate();
                            String CurrentTime=  CurrentTime();
                            String FinalDate=  getOnlyDate(arr_buy.get(position).getCreated_date()+ " "+ arr_buy.get(position).getCreated_time());
                            String get_time =  arr_buy.get(position).getCreated_time();
                            String FinalTime=  getDate(arr_buy.get(position).getCreated_date() + " "+get_time);

                            Date date1;
                            Date date1_T;
                            Date date2;
                            Date date2_T;

                            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
                            SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

                            //Setting dates
                            date1 = dates.parse(CurrentDate);
                            date1_T = times.parse(FinalTime);
                            date2 = dates.parse(FinalDate);
                            date2_T = times.parse(CurrentTime);

                            //Comparing dates
                            long difference = Math.abs(date1.getTime() - date2.getTime());
                            long differenceDates = difference / (24 * 60 * 60 * 1000);
                            long mills =   date2_T.getTime()-date1_T.getTime();

                            String api_year = FinalDate.split("-")[0];
                            String curr_year = CurrentDate.split("-")[0];
                            String sub_year = "0";

                            try{
                                sub_year = String.valueOf(Integer.valueOf(curr_year)-Integer.valueOf(api_year));
                            }catch (Exception e){}

                            //Convert long to String
                            String dayDifference = Long.toString(differenceDates);

                            if(sub_year.equals("1")){
                                date_return = getActivity().getResources().getString(R.string.few_mnth);
                            }
                            else if(sub_year.equals("2")){
                                date_return = getActivity().getResources().getString(R.string.year_ago);
                            }
                            else if(Integer.valueOf(dayDifference)>0){
                                if(dayDifference.equals("1")){
                                    date_return = dayDifference+" "+getActivity().getResources().getString(R.string.dyss);
                                }else{
                                    date_return = dayDifference+" "+getActivity().getResources().getString(R.string.dyss);
                                }
                            }else{
                                int hours = (int) (mills / (1000 * 60 * 60));
                                int minutes = (int) (mills / (1000 * 60));
                                int seconds = (int) (mills / (1000));
                                if(hours>0){
                                    date_return = hours+" "+getActivity().getResources().getString(R.string.hr);
                                }else if(minutes>0){
                                    date_return = minutes+" "+getActivity().getResources().getString(R.string.min);
                                }else{
                                    date_return = getActivity().getResources().getString(R.string.few_sec_full);
                                }
                            }

                        } catch (Exception exception) {
                            date_return = getActivity().getResources().getString(R.string.fewdays);
                        }
                        return date_return;
                    }
                    private void api_GetCount(final long diff){
                        AndroidNetworking.enableLogging();
                        utils.showProgressDialog(getActivity(),getContext().getResources().getString(R.string.load));

                        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.CHECK_LEAD_INHOUR)
                                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                                .setTag("msg")
                                .setPriority(Priority.HIGH).doNotCacheResponse()
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject result) {
                                        Log.e("res_count: ",result+"");
                                        utils.dismissProgressdialog();
                                        String lead_count = "3";
                                        lead_count = result.optString("number_of_leads");
                                        dialog_msg_Timer(diff,lead_count);
                                    }
                                    @Override
                                    public void onError(ANError error) {
                                        Log.e("", "---> On error  ");
                                        utils.dismissProgressdialog();
                                        dialog_msg_Timer(Long.valueOf(3),"3");
                                    }
                                });

                    }


                    public  void dialog_msg_Timer(long diff, String lead_count){
                        final BottomSheetDialog dialog = new BottomSheetDialog (getContext());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        View bottomSheetView  = getActivity().getLayoutInflater().inflate(R.layout.dialog_custom_popup_timer, null);
                        dialog.setContentView(bottomSheetView);
                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                BottomSheetDialog d = (BottomSheetDialog) dialog;
                                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                                BottomSheetBehavior.from(bottomSheet)
                                        .setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        });
                        TextView txt_msg            = (TextView) dialog.findViewById(R.id.txt_msg);
                        TextView txt_title          = (TextView) dialog.findViewById(R.id.txt_title);
                        TextView txt_ok             = (TextView) dialog.findViewById(R.id.txt_ok);
                        final  TextView txt_counter = (TextView) dialog.findViewById(R.id.txt_counter);
                        assert txt_msg != null;
                        txt_msg.setText(getActivity().getResources().getString(R.string.u_can_buy)+lead_count+
                                getActivity().getResources().getString(R.string.u_can_buy_nxt));

                        assert txt_title != null;
                        txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
                        txt_ok.setTypeface(utils.OpenSans_Regular(getActivity()));
                        txt_msg.setTypeface(utils.OpenSans_Regular(getActivity()));
                        txt_counter.setTypeface(utils.OpenSans_Regular(getActivity()));
                        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
                        assert rel_vw_save_details != null;
                        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        try{
                            dialog.show();
                        }catch (Exception e){

                        }



                        new CountDownTimer(diff, 1000) {

                            public void onTick(long millisUntilFinished) {
                                int seconds = (int) (millisUntilFinished / 1000) % 60;
                                int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
                                try{
                                    txt_counter.setText( getContext().getResources().getString(R.string.tym_nxt)+twoDigitString(minutes) + " : "
                                            + twoDigitString(seconds));
                                }catch (Exception e){}

                            }

                            public void onFinish() {
                                txt_counter.setText("You can buy it again!");
                            }
                        }.start();

                    }
                    private String twoDigitString(long number) {

                        if (number == 0) {
                            return "00";
                        }

                        if (number / 10 == 0) {
                            return "0" + number;
                        }

                        return String.valueOf(number);
                    }
                });

    }


    private void api_unlyk(final String lead_id, final String check){
        AndroidNetworking.enableLogging();
        Log.e("url lyk: ",NetworkingData.BASE_URL + NetworkingData.SWIPE);
        Log.e("user_id : ",sharedPreferenceLeadr.getUserId());
//        Log.e("lead_id_swipe: ",lead_id);
        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.SWIPE)
                .addBodyParameter("user_id",  sharedPreferenceLeadr.getUserId())
                .addBodyParameter("lead_id",  lead_id)
                .addBodyParameter("check",    check)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("===> Response_lyk: ",result+"");
                        utils.dismissProgressdialog();
                        if(result.optString("status").equalsIgnoreCase("1")){
                            if(adapter!=null) {
                                adapter.notifyDataSetChanged();
                            }
                            if(check.equalsIgnoreCase("0")) {
                                if (arr_buy_lastSwipe.size() > 0) {
                                    arr_buy.add(0, arr_buy_lastSwipe.get(0));
                                    adapter.notifyDataSetChanged();
                                }
//                                cardStack.reverse();
                                if (img_undo != null) {
                                    img_undo.setVisibility(View.GONE);
                                }
                            }
                        }
//                        api_Analytic(lead_id);
                        AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                        logger.logEvent("EVENT_NAME_VIEWED_CONTENT");
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("error",error.toString());
                        utils.dismissProgressdialog();
                    }
                });
    }

    public  void dialog_AddCard(final String lead_id, final String client_name, final String phone, final View vw_start
            , final View vw_sold, final String userId, final int position) {
       /* final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Holo_NoActionBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_payment);

        final Button btnok           = (Button) dialog.findViewById(R.id.btnok);
        final EditText edt_cvv       = (EditText) dialog.findViewById(R.id.edt_cvv);
        final EditText edt_month     = (EditText) dialog.findViewById(R.id.edt_month);
        final EditText edt_year      = (EditText) dialog.findViewById(R.id.edt_year);
        final EditText edt_card_no   = (EditText) dialog.findViewById(R.id.edt_card_no);
        final EditText edt_full_name = (EditText) dialog.findViewById(R.id.edt_full_name);
        final ImageView img_cancel   = (ImageView) dialog.findViewById(R.id.img_cancel);
        final TextView txt_top       = (TextView) dialog.findViewById(R.id.txt_top);*/

        rel_dialog.setVisibility(View.VISIBLE);
        rel_bottom.setVisibility(View.GONE);
        vw_line.setVisibility(View.GONE);

        String text = "";
        if(sharedPreferenceLeadr.get_CARD().equalsIgnoreCase("N/A")){
            img_remove.setVisibility(View.GONE);
            text = "<font color=#227cec>"+getResources().getString(R.string.frst_tym)+
                    "</font> <font color=#071a30>"+getResources().getString(R.string.you_must)+"</font>";
        }else{
            img_remove.setVisibility(View.VISIBLE);
            text =
                    "</font> <font color=#071a30>"+getResources().getString(R.string.ur_card)+"</font>";
        }
        txt_top.setText(Html.fromHtml(text));

        try {
            InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.showSoftInput(edt_full_name, 0);
        } catch (Exception e) {
        }
        edt_full_name.requestFocus();

        btnok.setTypeface(utils.Dina(getActivity()));
        btnok1.setTypeface(utils.Dina(getActivity()));
        edt_cvv.setTypeface(utils.OpenSans_Regular(getActivity()));
        edt_month.setTypeface(utils.OpenSans_Regular(getActivity()));
        edt_year.setTypeface(utils.OpenSans_Regular(getActivity()));
        edt_card_no.setTypeface(utils.OpenSans_Regular(getActivity()));
        edt_full_name.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_top.setTypeface(utils.OpenSans_Regular(getActivity()));


        edt_card_no.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        edt_card_no.setLongClickable(false);
        edt_card_no.setTextIsSelectable(false);


        edt_cvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 3) {
                    utils.hideKeyboard(getActivity());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        edt_card_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 16) {
                    edt_month.requestFocus();
                    edt_month.setSelection(edt_month.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edt_month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    String working = s.toString();
                    Log.e("before: ",working+"");
                }catch (Exception e){}
                if (edt_month.getText().toString().matches("^2")|| edt_month.getText().toString().matches("^3")||
                        edt_month.getText().toString().matches("^4")||edt_month.getText().toString().matches("^5")||
                        edt_month.getText().toString().matches("^6")||edt_month.getText().toString().matches("^7")||
                        edt_month.getText().toString().matches("^8")||edt_month.getText().toString().matches("^9")) {
                    // Not allowed
                    edt_month.setText("");
                }else if(s.length() == 2){
                    if (Integer.valueOf(edt_month.getText().toString())>12){
                        edt_month.setText("1");
                        edt_month.setSelection(edt_month.getText().toString().length());
                    }else  if (s.length() == 2) {
                        edt_year.requestFocus();
                        edt_year.setSelection(edt_year.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edt_year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    edt_month.requestFocus();
                    edt_month.setSelection(edt_month.getText().length());
                }else if(s.length() == 4){
                    edt_cvv.requestFocus();
                    edt_cvv.setSelection(edt_cvv.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        /***Keyboard Listener*****/
        KeyboardVisibilityEvent.setEventListener(
                getActivity(),
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                        if(rel_dialog.getVisibility()==View.VISIBLE) {
                            if (isOpen) {
                                rel_bottm.setVisibility(View.VISIBLE);
                                rel_botm_real.setVisibility(View.GONE);
                            } else {
                                rel_bottm.setVisibility(View.VISIBLE);
                                rel_botm_real.setVisibility(View.GONE);
                            }
                        }
                    }
                });


        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Unregistrar unregistrar = KeyboardVisibilityEvent.registerEventListener(
                        getActivity(),
                        new KeyboardVisibilityEventListener() {
                            @Override
                            public void onVisibilityChanged(boolean isOpen) {
                            }
                        });

                unregistrar.unregister();
                rel_dialog.setVisibility(View.GONE);
                rel_bottom.setVisibility(View.VISIBLE);
                vw_line.setVisibility(View.GONE);
            }
        });


        img_cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_cancel.performClick();
            }
        });

        btnok1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick( View v ) {
                btnok.performClick();
            }
        });

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                method_check_values();
            }

            void method_check_values() {
                utils.hideKeyboard(getActivity());
                if (edt_full_name.getText().toString().trim().length() > 0) {
                    if (edt_card_no.getText().toString().trim().length() > 0) {
                        if (edt_month.getText().toString().trim().length() == 2) {
                            if (edt_year.getText().toString().trim().length() == 4) {
                                if (edt_cvv.getText().toString().trim().length() > 0) {
                                    Card card = new Card(edt_card_no.getText().toString(), Integer.valueOf(edt_month.getText().toString()),
                                            Integer.valueOf(edt_year.getText().toString()),
                                            edt_cvv.getText().toString());
                                    if (!card.validateCard()) {
                                        utils.hideKeyboard(getActivity());
                                        if (!card.validateNumber()) {
                                            dialog_msg_show(getActivity(), getResources().getString(R.string.in_cvv),edt_card_no);

                                        } else if (!card.validateExpMonth()) {
                                            dialog_msg_show(getActivity(), getResources().getString(R.string.exp),edt_month);

                                        } else if (!card.validateExpiryDate()) {
                                            dialog_msg_show(getActivity(), getResources().getString(R.string.expyr),edt_year);

                                        } else if (!card.validateCVC()) {
                                            dialog_msg_show(getActivity(), getResources().getString(R.string.cvv_in),edt_cvv);

                                        }
                                    } else {
                                        String substr = edt_card_no.getText().toString().substring(edt_card_no.getText().toString().
                                                length() - 4);
                                        utils.hideKeyboard(getActivity());
                                        Unregistrar unregistrar = KeyboardVisibilityEvent.registerEventListener(
                                                getActivity(),
                                                new KeyboardVisibilityEventListener() {
                                                    @Override
                                                    public void onVisibilityChanged(boolean isOpen) {
                                                    }
                                                });

                                        unregistrar.unregister();
                                        rel_dialog.setVisibility(View.GONE);
                                        rel_bottom.setVisibility(View.VISIBLE);
                                        vw_line.setVisibility(View.GONE);

                                        if(progresS_load!=null) {
                                            progresS_load.setVisibility(View.VISIBLE);
                                        }
                                        if(dialog_card!=null){
                                            if(dialog_card.isShowing()){
                                                dialog_card.dismiss();
                                            }
                                        }
                                        dialog_CardFrstTYm(getActivity(),getResources().getString(R.string.frst_tym_longr));
                                        method_SendToStripe(card, lead_id,client_name,phone,vw_start,vw_sold,substr,userId,position);
                                    }
                                } else {
                                    dialog_msg_show(getActivity(), getResources().getString(R.string.entr_cvv),edt_cvv);

                                }
                            } else {
                                dialog_msg_show(getActivity(), getResources().getString(R.string.entr_yr),edt_year);

                            }
                        } else {
                            dialog_msg_show(getActivity(), getResources().getString(R.string.entr_mm),edt_month);

                        }
                    } else {
                        dialog_msg_show(getActivity(), getResources().getString(R.string.entr_nmbr),edt_card_no);

                    }
                } else {
                    dialog_msg_show(getActivity(), getResources().getString(R.string.entr_namer),edt_full_name);

                }
            }


            public  void dialog_msg_show(Activity activity, String msg, final EditText edt){
                final BottomSheetDialog dialog = new BottomSheetDialog (activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
                dialog.setContentView(bottomSheetView);
                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        BottomSheetDialog d = (BottomSheetDialog) dialog;
                        FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                        BottomSheetBehavior.from(bottomSheet)
                                .setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                });
                TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
                TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
                TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
                assert txt_msg != null;
                txt_msg.setText(msg);

                assert txt_title != null;
                txt_title.setTypeface(utils.OpenSans_Regular(activity));
                txt_ok.setTypeface(utils.OpenSans_Regular(activity));
                txt_msg.setTypeface(utils.OpenSans_Regular(activity));
                RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
                assert rel_vw_save_details != null;
                rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        try{
                            ((InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                        }catch (Exception e){}
                        edt.requestFocus();
                    }
                });
                try{
                    dialog.show();
                }catch (Exception e){

                }

                dialog.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss( DialogInterface dialog ) {
                        try{
                            ((InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                        }catch (Exception e){}
                        edt.requestFocus();
                    }
                });

            }

            void  method_SendToStripe(Card card, final String lead_id, final String client_name, final String phone, final
            View vw_start, final View vw_sold, final String subcard, final String userId, final int position) {
//                utils.showProgressDialog(getActivity(), getResources().getString(R.string.pls_wait));
                //pk_test_AFG6u0uaLgXBKwMkFmd2QtjP // pk_live_r9TQXzdQ0OeTo6NiRxHJQxft
                //pk_test_AFG6u0uaLgXBKwMkFmd2QtjP // pk_test_AFG6u0uaLgXBKwMkFmd2QtjP
                Stripe stripe = new Stripe(getActivity(), "pk_test_AFG6u0uaLgXBKwMkFmd2QtjP");
                stripe.createToken(
                        card,
                        new TokenCallback() {
                            public void onSuccess(Token token) {
                                utils.dismissProgressdialog();
                                if (utils.isNetworkAvailable(getActivity())) {

//                                    utils.showProgressDialog(getActivity(), getResources().getString(R.string.pls_wait));
                                    api_SaveToken(token.getId(), lead_id,client_name,phone,vw_start,vw_sold,subcard,userId,position);
                                } else {
                                    if(progresS_load!=null) {
                                        progresS_load.setVisibility(View.GONE);
                                    }
                                    if(dialog_card!=null){
                                        if(dialog_card.isShowing()){
                                            dialog_card.dismiss();
                                        }
                                    }
                                    utils.hideKeyboard(getActivity());
                                    utils.dialog_msg_show(getActivity(), getResources().getString(R.string.no_internet));
                                }
                            }

                            public void onError(Exception error) {
                                // Show localized error message
                                utils.dismissProgressdialog();
                                utils.hideKeyboard(getActivity());
                                if(dialog_card!=null){
                                    if(dialog_card.isShowing()){
                                        dialog_card.dismiss();
                                    }
                                }
                                if(progresS_load!=null) {
                                    progresS_load.setVisibility(View.GONE);
                                }
                                utils.dialog_msg_show(getActivity(), "Error in Stripe, please try again!!");
                            }
                        }
                );
            }


            private void api_SaveToken(String token, final String lead_id, final String client_name, final String phone,
                                       final View vw_strat, final View vw_sold, final String subcard, final String userId
                    ,final int position) {
                AndroidNetworking.enableLogging();
                utils.dismissProgressdialog();
//                utils.showProgressDialog(getActivity(), getResources().getString(R.string.load));
                Log.e("url_stripe: ", NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN);
//                Log.e("token: ", token);

                AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.SAVE_TOKEN)
                        .addBodyParameter("token", token)
                        .addBodyParameter("user_id", sharedPreferenceLeadr.getUserId())
                        .addBodyParameter("last_digits", subcard)
                        .setTag("msg")
                        .setPriority(Priority.HIGH).doNotCacheResponse()
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject result) {
                                Log.e("res_stripe: ", result + "");
                                //{"status":"1","customer":{"id":"cus_C4xJzoWD2YfwOO","object":"customer","account_balance":0,"created":1515128721,"currency":null,"default_source":"card_1BgnOOI0GVRrJIpDo8dLjinv","delinquent":false,"description":"Customer for LeadR app","discount":null,"email":null,"livemode":false,"metadata":{},"shipping":null,"sources":{"object":"list","data":[{"id":"card_1BgnOOI0GVRrJIpDo8dLjinv","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"Visa","country":"US","customer":"cus_C4xJzoWD2YfwOO","cvc_check":"pass","dynamic_last4":null,"exp_month":11,"exp_year":2022,"fingerprint":"kXn425uHw3F6DtGT","funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null}],"has_more":false,"total_count":1,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"\/v1\/customers\/cus_C4xJzoWD2YfwOO\/subscriptions"}}}

                                if(dialog_card!=null){
                                    if(dialog_card.isShowing()){
                                        dialog_card.dismiss();
                                    }
                                }
                                if (result.optString("status").equals("1")) {
                                    Bundle parameters = new Bundle();
                                    parameters.putString("app_user_id", sharedPreferenceLeadr.getUserId());
                                    AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                                    logger.logEvent("EVENT_NAME_ADDED_PAYMENT_INFO",parameters);
                                    sharedPreferenceLeadr.set_CARD(subcard);
                                    utils.showProgressDialog(getActivity(), getResources().getString(R.string.load));
                                    api_CheckStatus(lead_id,client_name,phone,vw_start,vw_sold,userId, position);
                                } else {
                                    utils.dismissProgressdialog();
                                    if(progresS_load!=null) {
                                        progresS_load.setVisibility(View.GONE);
                                    }
                                    dialog_errorCard(lead_id,client_name,phone,vw_start,vw_sold,userId,position);
                                }
//                                utils.dismissProgressdialog();
                            }

                            @Override
                            public void onError(ANError error) {
                                Log.i("", "---> On error  ");
                                utils.dismissProgressdialog();
                                if(dialog_card!=null){
                                    if(dialog_card.isShowing()){
                                        dialog_card.dismiss();
                                    }
                                }
                                if(progresS_load!=null) {
                                    progresS_load.setVisibility(View.GONE);
                                }
                            }
                        });

            }
        });

    }


    public  void dialog_errorCard(final String lead_id, final String client_name, final String phone, final View vw_start
            , final View vw_sold, final String userId,final int position){
        final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getActivity().getLayoutInflater().inflate(R.layout.dialog_card_error, null);
        dialog.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        TextView txt_update = (TextView) dialog.findViewById(R.id.txt_update);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView btn_update = (Button) dialog.findViewById(R.id.btn_update);

        txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_ok.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_update.setTypeface(utils.OpenSans_Regular(getActivity()));
        btn_update.setTypeface(utils.OpenSans_Bold(getActivity()));

        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog.findViewById(R.id.rel_vw_save_details);
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sharedPreferenceLeadr.set_CARD("N/A");
                dialog_AddCard(lead_id,client_name,phone,vw_start,vw_sold, userId, position);
            }
        });

        dialog.show();


    }



    public  void dialog_SuccessBuy(final  String lead_id,final  String client_name,final  String phone, final View vw_start,
                                   final View vw_sold){
        dialog_per = new BottomSheetDialog (getActivity(),R.style.Theme_TransparentD);
        dialog_per.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = getActivity().getLayoutInflater().inflate(R.layout.dialog_bought_lead, null);
        dialog_per.setContentView(bottomSheetView);
        dialog_per.setCancelable(true);
        dialog_per.setCanceledOnTouchOutside(true);

        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog_per.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        TextView txt_u_bought     = (TextView)dialog_per.findViewById(R.id.txt_u_bought);
        TextView txt_name         = (TextView)dialog_per.findViewById(R.id.txt_name);
        TextView txt_client_name  = (TextView)dialog_per.findViewById(R.id.txt_client_name);
        TextView txt_phone        = (TextView)dialog_per.findViewById(R.id.txt_phone);
        TextView txt_ur_lead      = (TextView)dialog_per.findViewById(R.id.txt_ur_lead);
        TextView txt_close        = (TextView)dialog_per.findViewById(R.id.txt_close);
        TextView txt_astrik       = (TextView)dialog_per.findViewById(R.id.txt_astrik);
        RelativeLayout rel_close  = (RelativeLayout)dialog_per.findViewById(R.id.rel_close);
        Button btn_call           = (Button)dialog_per.findViewById(R.id.btn_call);

        img_remove.setVisibility(View.GONE);

        txt_u_bought.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_name.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_client_name.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_phone.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_ur_lead.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_close.setTypeface(utils.OpenSans_Regular(getActivity()));
        btn_call.setTypeface(utils.OpenSans_Regular(getActivity()));

        String text = "<font color=#071a30>"+getActivity().getResources().getString(R.string.ur_lead)+
                "</font> <font color=#227cec>"+getActivity().getResources().getString(R.string.my_lead)+"</font>";
        txt_ur_lead.setText(Html.fromHtml(text));

        if(client_name.equalsIgnoreCase("UNKNOWN")){
            if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                txt_client_name.setText(getResources().getString(R.string.unknw));
            }else{
                txt_client_name.setText(client_name);
            }
        }else{
            txt_client_name.setText(client_name);
        }

        txt_phone.setText(phone);

        rel_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vw_sold.setVisibility(View.VISIBLE);
                vw_start.setVisibility(View.GONE);
                dialog_per.dismiss();
                if(sharedPreferenceLeadr.get_BOUGHT_FIRST().equals("0")){
                    sharedPreferenceLeadr.set_BOUGHT_FIRST("1");
                    sharedPreferenceLeadr.set_SELECTOR_BOUGHT("0");
                    try{
                        handler_timer.removeCallbacks(runnable_timer);
                        remove_Callback();
                    }catch (Exception e){}
                    MainActivity.fragment_bought = new BoughtScrollListFragment();
                    MainActivity.fragmentManager.beginTransaction().add(R.id.contentContainer, MainActivity.fragment_bought).commit();

                    if(MainActivity.fragment_inbox!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_inbox).commit();
                    }
                    if(MainActivity.fragment_sell!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_sell).commit();
                    }
                    if(MainActivity.fragment_more!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_more).commit();
                    }
                    if(MainActivity.fragment_buy!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_buy).commit();
                    }

                    MainActivity.vw_buy.setVisibility(View.GONE);
                    vw_bought.setVisibility(View.VISIBLE);
                    vw_sell.setVisibility(View.GONE);
                    vw_inbox.setVisibility(View.GONE);
                    vw_more.setVisibility(View.GONE);

                    MainActivity.vw_buy.setVisibility(View.GONE);
                    vw_bought.setVisibility(View.VISIBLE);
                    vw_sell.setVisibility(View.GONE);
                    vw_inbox.setVisibility(View.GONE);
                    vw_more.setVisibility(View.GONE);
                }else{
                    try{
                        handler_timer.postDelayed(runnable_timer, 40000);
                    }catch (Exception e){}
                }
            }
        });


        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vw_sold.setVisibility(View.VISIBLE);
                vw_start.setVisibility(View.GONE);
                phone_no = phone;
                if (utils.checkReadCallLogPermission(getActivity())) {
                    utils.hideKeyboard(getActivity());
                    dialog_per.dismiss();

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+phone));
                    startActivity(callIntent);
                } else {
                    dialog_per.dismiss();
                    requestPermissions(
                            new String[]{Manifest.permission.CALL_PHONE},
                            1
                    );
                }


            }
        });

        dialog_per.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {
                vw_sold.setVisibility(View.VISIBLE);
                vw_start.setVisibility(View.GONE);
                dialog_per.dismiss();
                if(sharedPreferenceLeadr.get_BOUGHT_FIRST().equals("0")){
                    sharedPreferenceLeadr.set_BOUGHT_FIRST("1");
                    try{
                        handler_timer.removeCallbacks(runnable_timer);
                        remove_Callback();
                    }catch (Exception e){}
                    sharedPreferenceLeadr.set_BOUGHT_FIRST("1");
                    try{
                        handler_timer.removeCallbacks(runnable_timer);
                        remove_Callback();
                    }catch (Exception e){}
                    sharedPreferenceLeadr.set_SELECTOR_BOUGHT("1");
                    MainActivity.fragment_bought = new BoughtScrollListFragment();
                    MainActivity.fragmentManager.beginTransaction().add(R.id.contentContainer, MainActivity.fragment_bought).commit();

                    if(MainActivity.fragment_inbox!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_inbox).commit();
                    }
                    if(MainActivity.fragment_sell!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_sell).commit();
                    }
                    if(MainActivity.fragment_more!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_more).commit();
                    }
                    if(MainActivity.fragment_buy!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_buy).commit();
                    }

                    MainActivity.vw_buy.setVisibility(View.GONE);
                    vw_bought.setVisibility(View.VISIBLE);
                    vw_sell.setVisibility(View.GONE);
                    vw_inbox.setVisibility(View.GONE);
                    vw_more.setVisibility(View.GONE);

                    MainActivity.vw_buy.setVisibility(View.GONE);
                    vw_bought.setVisibility(View.VISIBLE);
                    vw_sell.setVisibility(View.GONE);
                    vw_inbox.setVisibility(View.GONE);
                    vw_more.setVisibility(View.GONE);
                }else{
                    try{
                        handler_timer.postDelayed(runnable_timer, 40000);
                    }catch (Exception e){}
                }
            }
        });

        dialog_per.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                vw_sold.setVisibility(View.VISIBLE);
                vw_start.setVisibility(View.GONE);
                dialog_per.dismiss();
                if(sharedPreferenceLeadr.get_BOUGHT_FIRST().equals("0")){
                    sharedPreferenceLeadr.set_BOUGHT_FIRST("1");
                    try{
                        handler_timer.removeCallbacks(runnable_timer);
                        remove_Callback();
                    }catch (Exception e){}
                    sharedPreferenceLeadr.set_BOUGHT_FIRST("1");
                    try{
                        handler_timer.removeCallbacks(runnable_timer);
                        remove_Callback();
                    }catch (Exception e){}
                    MainActivity.fragment_bought = new BoughtScrollListFragment();
                    MainActivity.fragmentManager.beginTransaction().add(R.id.contentContainer, MainActivity.fragment_bought).commit();

                    if(MainActivity.fragment_inbox!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_inbox).commit();
                    }
                    if(MainActivity.fragment_sell!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_sell).commit();
                    }
                    if(MainActivity.fragment_more!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_more).commit();
                    }
                    if(MainActivity.fragment_buy!=null){
                        MainActivity.fragmentManager.beginTransaction().hide(MainActivity.fragment_buy).commit();
                    }

                    MainActivity.vw_buy.setVisibility(View.GONE);
                    vw_bought.setVisibility(View.VISIBLE);
                    vw_sell.setVisibility(View.GONE);
                    vw_inbox.setVisibility(View.GONE);
                    vw_more.setVisibility(View.GONE);

                    MainActivity.vw_buy.setVisibility(View.GONE);
                    vw_bought.setVisibility(View.VISIBLE);
                    vw_sell.setVisibility(View.GONE);
                    vw_inbox.setVisibility(View.GONE);
                    vw_more.setVisibility(View.GONE);
                }else{
                    try{
                        handler_timer.postDelayed(runnable_timer, 40000);
                    }catch (Exception e){}
                }
            }
        });


        dialog_per.show();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions){
            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)){
                //denied
                utils.hideKeyboard(getActivity());
                dialog_denyOne();
            }else{
                if(ActivityCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED){
                    //allowed
                    utils.hideKeyboard(getActivity());
                    dialog_per.dismiss();
                    AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                    logger.logEvent("EVENT_NAME_BOUGHT_CALLED_CLIENT");
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+phone_no));
                    startActivity(callIntent);
                } else{
                    utils.hideKeyboard(getActivity());
                    dialog_openStoragePer();
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //do something here.
                }
            }
        }
    }

    public  void dialog_denyOne(){
        final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_deny_one, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_ok     = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title  = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);

        txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_ok.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_camera.setTypeface(utils.OpenSans_Regular(getActivity()));

        txt_title.setText(getActivity().getResources().getString(R.string.phone_per));
        txt_camera.setText(getActivity().getResources().getString(R.string.stor_all_phone));

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialog_per.dismiss();
            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }


    }

    public  void dialog_openStoragePer(){
        final BottomSheetDialog dialog = new BottomSheetDialog (getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = this.getLayoutInflater().inflate(R.layout.dialog_per_setting, null);
        dialog.setContentView(bottomSheetView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        Button btn_finish = (Button) dialog.findViewById(R.id.btn_finish);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_camera = (TextView) dialog.findViewById(R.id.txt_camera);
        TextView txt_press = (TextView) dialog.findViewById(R.id.txt_press);
        TextView txt_store = (TextView) dialog.findViewById(R.id.txt_store);

        txt_title.setTypeface(utils.OpenSans_Regular(getActivity()));
        btn_finish.setTypeface(utils.OpenSans_Bold(getActivity()));
        txt_ok.setTypeface(utils.OpenSans_Bold(getActivity()));
        txt_camera.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_press.setTypeface(utils.OpenSans_Regular(getActivity()));
        txt_store.setTypeface(utils.OpenSans_Regular(getActivity()));

        if(sharedPreferenceLeadr.get_LANGUAGE().equals("it")){
            txt_camera.setGravity(Gravity.RIGHT);
            txt_press.setGravity(Gravity.RIGHT);
            txt_store.setGravity(Gravity.RIGHT);
        }

        txt_title.setText(getResources().getString(R.string.phone_per));
        txt_store.setText(getResources().getString(R.string.phone_per_three));

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                GlobalConstant.startInstalledAppDetailsActivity(getActivity());
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialog_per.dismiss();
            }
        });

        try{
            dialog.show();
        }catch (Exception e){

        }


    }


    private String method_GetDiffrenceDays() {
        String date_return = "";
        try {
            //Dates to compare
            String CurrentDate =  CurrentDate();
            String FinalDate   =  sharedPreferenceLeadr.get_DATEINSTALLED();

            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);

            //Comparing dates
            long difference      = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            String dayDifference = Long.toString(differenceDates);
            if(Integer.valueOf(dayDifference)>0){
                date_return = dayDifference;
            }else{
                date_return = "0";
            }
        } catch (Exception exception) {
            date_return = "0";
        }
        return date_return;
    }


    private void api_Analytic(final String lead_id){
        AndroidNetworking.enableLogging();
        Log.e("url lyk: ",NetworkingData.BASE_URL + NetworkingData.ANALYTIC_LEAD);
        Log.e("user_id : ",sharedPreferenceLeadr.getUserId());
//        Log.e("lead_id: ",lead_id);
        AndroidNetworking.post(NetworkingData.BASE_URL + NetworkingData.ANALYTIC_LEAD)
                .addBodyParameter("user_id",  sharedPreferenceLeadr.getUserId())
                .addBodyParameter("lead_id",  lead_id)
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("Res_anlaytic: ",result+"");
                        Bundle parameters = new Bundle();
                        if(arr_buy.size()>0){
                        parameters.putString("EVENT_PARAM_DESCRIPTION", arr_buy.get(0).getDescription());
                        }
                        AppEventsLogger logger = AppEventsLogger.newLogger(getContext());
                        logger.logEvent("EVENT_NAME_VIEWED_CONTENT",
                                parameters);
                        utils.dismissProgressdialog();
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("error",error.toString());
                        utils.dismissProgressdialog();
                    }
                });
    }


    public  void dialog_CardFrstTYm(Activity activity, String msg){
        dialog_card = new BottomSheetDialog (activity);
        dialog_card.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog_card.setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        dialog_card.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog_card.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog_card.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog_card.findViewById(R.id.txt_ok);
        assert txt_msg != null;
        txt_msg.setText(msg);

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        txt_title.setText(getResources().getString(R.string.pls_wait));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog_card.findViewById(R.id.rel_vw_save_details);
        assert rel_vw_save_details != null;
        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_card.dismiss();
            }
        });
        dialog_card.show();


    }


    /***
     * CHeck ADMIN Settings to set price range***/
    private void api_chck_AdminSettings(){
        AndroidNetworking.enableLogging();
        Log.e("url_chkSetting: ", NetworkingData.BASE_URL+ NetworkingData.GET_ADMIN_SETTING);
      try{
          progresS_load.setVisibility(View.VISIBLE);
      }catch (Exception e){}
        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.GET_ADMIN_SETTING)
                .addBodyParameter("user_id",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chkadmin: ",result+"");
                        //{"status":1,"message":"Details..","admin_settings":[{"id":1,"admin_id":1,"number_of_leads":4,"min_amount":"1
                        //  ","max_lead_price":"100","market_fee":"44"}]}

                        try{
                            JSONArray arr = result.getJSONArray("admin_settings");
                            for (int i = 0; i < arr.length(); i++) {
                                JSONObject obj = arr.getJSONObject(i);
                                admin_max_price = obj.optString("max_lead_price");
                                if(sharedPreferenceLeadr.get_LANGUAGE().equalsIgnoreCase("it")){
                                    admin_max_price = String.valueOf(Math.round(Float.valueOf(admin_max_price)*
                                            Float.valueOf(sharedPreferenceLeadr.get_PRICE_ILS())));
                                }
                            }
                        }catch (Exception e){}
                        utils.dismissProgressdialog();
                        try{
                            progresS_load.setVisibility(View.GONE);
                        }catch (Exception e){}
                        Intent i_filter = new Intent(getActivity(), FilterActivity.class);
                        i_filter.putExtra("admin_max_price",admin_max_price);
                        startActivity(i_filter);
//                        getActivity().overridePendingTransition(R.anim.slide_in_down, R.anim.stay);
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                        utils.dismissProgressdialog();
                        try{
                            progresS_load.setVisibility(View.GONE);
                        }catch (Exception e){}
                        Intent i_filter = new Intent(getActivity(), FilterActivity.class);
                        i_filter.putExtra("admin_max_price",admin_max_price);
                        startActivity(i_filter);
//                        getActivity(). overridePendingTransition(R.anim.slide_in_down, R.anim.stay);
                    }
                });

    }


    public  void dialog_LongLead(Activity activity, String msg, String title){
        final BottomSheetDialog dialog = new BottomSheetDialog (activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_long_txt_popup, null);
        dialog.setContentView(bottomSheetView);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(true);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);

                behavior.setPeekHeight(bottomSheet.getHeight());
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);

            }
        });

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey( DialogInterface dialog, int keyCode, KeyEvent event ) {
                dialog.dismiss();
                return false;

            }
        });

        TextView txt_msg   = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog.findViewById(R.id.txt_ok);

        txt_msg.setText(msg);

        txt_msg.setMovementMethod(new ScrollingMovementMethod());

        assert txt_title != null;
        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_title.setText(title);
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));

        txt_msg.setGravity(Gravity.LEFT);
        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss( DialogInterface dialog ) {
                handler_timer.postDelayed(runnable_timer,40000);
            }
        });
        try{
            dialog.show();
        }catch (Exception e){
        }
    }


    /*Dialog for Internet*/
    public  void dialog_msg_show(Activity activity, String msg){
        MainActivity.state_maintain = "1";
        dialog_internet = new BottomSheetDialog (activity);
        dialog_internet.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View bottomSheetView  = activity.getLayoutInflater().inflate(R.layout.dialog_custom_popup, null);
        dialog_internet.setContentView(bottomSheetView);



        dialog_internet.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d     = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = (FrameLayout) d.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet)
                        .setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        TextView txt_msg   = (TextView) dialog_internet.findViewById(R.id.txt_msg);
        TextView txt_title = (TextView) dialog_internet.findViewById(R.id.txt_title);
        TextView txt_ok    = (TextView) dialog_internet.findViewById(R.id.txt_ok);

        txt_msg.setText(msg);

        txt_title.setTypeface(utils.OpenSans_Regular(activity));
        txt_ok.setTypeface(utils.OpenSans_Regular(activity));
        txt_msg.setTypeface(utils.OpenSans_Regular(activity));
        RelativeLayout rel_vw_save_details = (RelativeLayout) dialog_internet.findViewById(R.id.rel_vw_save_details);

        rel_vw_save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_internet.dismiss();
            }
        });
        try{
            if(dialog_internet!=null){
                if(!dialog_internet.isShowing()){
                    dialog_internet.show();

                }
            }
        }catch (Exception e){
        }
    }

}
