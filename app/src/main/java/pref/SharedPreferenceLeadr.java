package pref;

import android.content.Context;
import android.content.SharedPreferences;

interface VariablesPreference {
    String Email = "email";
    String UserName = "username";
    String Password = "pass";
    String PREF_NAME = "leadR";
    String UserId = "userid";
    String UserId_Bfr = "userid_bfr";
    String PROFILE_PIC_URL = "image";
    String DEVICE_TOKEN = "token";
    String PHONE_NO = "phone";
    String JOB_TITLE = "job_title";
    String BUS_NAME = "bus_name";
    String BUS_INFO = "bus_info";
    String BUS_IMG = "bus_img";
    String BUS_SITE = "bus_site";
    String SIGN_UP = "no";
    String NUMBER = "";
    String FILE_NAME = "file_name";
    String TIME_MILLIS = "time_milli";
    String CATEGORY = "cat_id";
    String CATEGORYNAME_ENG_REG = "cat_nm_eng";
    String CATEGORYNAME_HE_REG = "cat_nm_he";
    String CATEGORYNAME_HE_FIL = "cat_nm_fil_he";
    String CATEGORYNAME_ENG_FIL = "cat_nm_fil_eng";
    String CATEGORY_SEL_ID = "cat_id_sel";
    String CATEGORY_SEL_NAME = "cat_name_sel";
    String CATEGORY_SEL_NAME_HE = "cat_name_sel_he";
    String CATEGORY_PROFILE_NAME = "cat_profile_sel";
    String COUNT_START = "start";
    String COUNT_START_PLAY = "play";
    String COUNT_START_PAUSE = "pause";
    String COUNT_START_PAUSE_ADD = "pause_add";
    String SELL_FIRST_TYM = "sell_frst";
    String CAT_FILTER = "cat_filter";
    String CAT_FILTER_NAME = "cat_filter_name";
    String CAT_FILTER_NAME_HE = "cat_filter_namehe";
    String CAT_FILTER_NAME_TEMP = "cat_filter_name_temp";
    String CAT_FILTER_NAME_HE_TEMP = "cat_filterhe_name_temp";
    String CAT_FILTER_ID_TEMP = "cat_filter_id_temp";
    String LOC_NAME = "loc_name";
    String LOC_NAME_COUNTRY = "loc_name_country";
    String LATITUDE = "lat";
    String LONGITUDE = "long";
    String LOC_NAME_TEMP = "loc_name_temp";
    String LOC_NAME_COUNTRY_TEMP = "loc_name_temp_country";
    String LATITUDE_TEMP = "lat_temp";
    String LONGITUDE_TEMP = "long_temp";
    String LOC_COUNTRY_TEMP = "coun_temp";
    String LOC_TYPE = "loc_type";
    String LOC_TYPE_FILTER = "loc_type_filter";
    String TO_PRICE = "to_price";
    String FROM_PRICE = "from_price";
    String MIN_BUDGET = "min_budget";
    String CARD = "card";
    String CHAT_NOTIFY = "chat_notify";
    String CHAT_SOUND = "chat_sound";
    String TICKET_NOTIFY = "ticket_notify";
    String TICKET_SOUND = "ticket_sound";
    String SELL_NOTIFY = "sell_notify";
    String SELL_SOUND = "sell_sound";
    String NEWSELL_SOUND = "newsell_sound";
    String NEWSELL_NOTIFY = "newsell_notify";
    String WIDGET = "widget";
    String PER = "permission";
    String PERPRO = "permission_pro";
    String ADDRESS = "address";
    String ADDRESS_TEMP = "address_temp";
    String BOUGHT_FIRST = "bought_first";
    String PROFILE_THUMB = "pro_thumb";
    String BUSS_THUMB = "buss_thumb";
    String DAYPASSED = "day_passed";
    String DATEINSTALLED = "date_installed";
    String DIMENSION = "dimension_sys";
    String TIME_NOW = "time_now";
    String TIME_HOUR_THEN = "time_then";
    String LANGUAGE = "lang";
    String REG_START = "reg_start";
    String GET_START = "get_start";
    String GET_START_END = "end_start";
    String SELL_START = "sell_start";
    String APP_START = "sell_start";
    String APP_START_SESSION = "session";
    String HOUR_OR_DAY = "hour_or_day";
    String MAX_TIME_COUNT = "count";
    String TO_RADIUS = "50";
    String SELECTOR_BOUGHT = "sel_bought";
    String SELECTOR_SELL = "sel_sell";
    String EMAIL_REG = "eml";
    String LOC_NAME_SELL = "loc_nm_sell";
    String ADDRESS_SELL = "add_sell";
    String LAT_SELL = "lat_sell";
    String LONG_SELL = "long_sell";
    String COUNTRY_SELL = "country_sell";
    String TOOLTIP = "tool";
    String PRICE_ILS = "ils";
    String DAILY_PRICE = "dailyprice";
    String SWITCH_PRICE = "switch_price";
    String SWITCH_BUD = "switch_bud";
    String SWITCH_DATE = "switch_date";
    String SWITCH_JOB = "switch_job";

}

public class SharedPreferenceLeadr implements VariablesPreference {
    private SharedPreferences pref = null;
    private static SharedPreferenceLeadr preferences = null;
    static Context mContext = null;

    public static SharedPreferenceLeadr getInstance(Context context) {
        if (preferences == null) {
            preferences = new SharedPreferenceLeadr(context);
        }
        mContext = context;
        return preferences;
    }

    public void clearPreference() {

        pref.edit().clear().apply();

    }

    public SharedPreferenceLeadr(Context context) {
        mContext = context;
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }


    public String getEmail() {
        return pref.getString(Email, "");
    }

    public void setEmail(String email) {
        pref.edit().putString(Email, email).apply();
    }

    public String getPass() {
        return pref.getString(Password, "");
    }

    public void setPAss(String pass) {
        pref.edit().putString(Password, pass).apply();
    }

    public void setUserId(String userId) {
        pref.edit().putString(UserId, userId).apply();
    }

    public String getUserId() {
        return pref.getString(UserId, "");
    }


    public void setProfilePicUrl(String imageUrl) {
        pref.edit().putString(PROFILE_PIC_URL, imageUrl).apply();
    }

    public String getProfilePicUrl() {
        return pref.getString(PROFILE_PIC_URL, "");
    }


    public void set_username(String phone) {
        pref.edit().putString(UserName, phone).apply();
    }

    public String get_username() {
        return pref.getString(UserName, "");
    }

    public void set_dev_token(String token) {
        pref.edit().putString(DEVICE_TOKEN, token).apply();
    }

    public String get_dev_token() {
        return pref.getString(DEVICE_TOKEN, "0.0");
    }

    public void set_phone(String PHONE_NNO) {
        pref.edit().putString(PHONE_NO, PHONE_NNO).apply();
    }

    public String get_phone() {
        return pref.getString(PHONE_NO, "0");
    }

    public void set_jobTitle(String JOB_eTITLE) {
        pref.edit().putString(JOB_TITLE, JOB_eTITLE).apply();
    }

    public String get_jobTitle() {
        return pref.getString(JOB_TITLE, "");
    }

    public void set_bus_name(String JOB_eTITLE) {
        pref.edit().putString(BUS_NAME, JOB_eTITLE).apply();
    }

    public String get_bus_name() {
        return pref.getString(BUS_NAME, "");
    }

    public void set_bus_info(String JOB_eTITLE) {
        pref.edit().putString(BUS_INFO, JOB_eTITLE).apply();
    }

    public String get_bus_info() {
        return pref.getString(BUS_INFO, "");
    }
    public void set_bus_img(String JOB_eTITLE) {
        pref.edit().putString(BUS_IMG, JOB_eTITLE).apply();
    }

    public String get_bus_img() {
        return pref.getString(BUS_IMG, "");
    }
    public void set_bus_site(String JOB_eTITLE) {
        pref.edit().putString(BUS_SITE, JOB_eTITLE).apply();
    }

    public String get_bus_site() {
        return pref.getString(BUS_SITE, "");
    }


    public void set_signup(String JOB_eTITLE) {
        pref.edit().putString(SIGN_UP, JOB_eTITLE).apply();
    }

    public String get_signup() {
        return pref.getString(SIGN_UP, "");
    }

    public void set_number(String JOB_eTITLE) {
        pref.edit().putString(NUMBER, JOB_eTITLE).apply();
    }

    public String get_number() {
        return pref.getString(NUMBER, "");
    }

    public void set_UserId_Bfr(String JOB_eTITLE) {
        pref.edit().putString(UserId_Bfr, JOB_eTITLE).apply();
    }

    public String get_UserId_Bfr() {
        return pref.getString(UserId_Bfr, "");
    }

    public void set_FILE_NAME(String JOB_eTITLE) {
        pref.edit().putString(FILE_NAME, JOB_eTITLE).apply();
    }

    public String get_FILE_NAME() {
        return pref.getString(FILE_NAME, "");
    }

    public void set_TIME_MILLIS(String JOB_eTITLE) {
        pref.edit().putString(TIME_MILLIS, JOB_eTITLE).apply();
    }

    public String get_TIME_MILLIS() {
        return pref.getString(TIME_MILLIS, "");
    }


    public void set_CATEGORY(String JOB_eTITLE) {
        pref.edit().putString(CATEGORY, JOB_eTITLE).apply();
    }

    public String get_CATEGORY() {
        return pref.getString(CATEGORY, "");
    }


    public void set_CATEGORY_SEL_ID(String JOB_eTITLE) {
        pref.edit().putString(CATEGORY_SEL_ID, JOB_eTITLE).apply();
    }

    public String get_CATEGORY_SEL_ID() {
        return pref.getString(CATEGORY_SEL_ID, "");
    }



    public void set_CATEGORY_SEL_NAME(String JOB_eTITLE) {
        pref.edit().putString(CATEGORY_SEL_NAME, JOB_eTITLE).apply();
    }

    public String get_CATEGORY_SEL_NAME() {
        return pref.getString(CATEGORY_SEL_NAME, "");
    }


    public void set_COUNT_START(String JOB_eTITLE) {
        pref.edit().putString(COUNT_START, JOB_eTITLE).apply();
    }

    public String get_COUNT_START() {
        return pref.getString(COUNT_START, "30");
    }


    public void set_COUNT_START_PLAY(String JOB_eTITLE) {
        pref.edit().putString(COUNT_START_PLAY, JOB_eTITLE).apply();
    }

    public String get_COUNT_START_PLAY() {
        return pref.getString(COUNT_START_PLAY, "");
    }


    public void set_COUNT_START_PAUSE(String JOB_eTITLE) {
        pref.edit().putString(COUNT_START_PAUSE, JOB_eTITLE).apply();
    }

    public String get_COUNT_START_PAUSE() {
        return pref.getString(COUNT_START_PAUSE, "");
    }


    public void set_COUNT_START_PAUSE_ADD(String JOB_eTITLE) {
        pref.edit().putString(COUNT_START_PAUSE_ADD, JOB_eTITLE).apply();
    }

    public String get_COUNT_START_PAUSE_ADD() {
        return pref.getString(COUNT_START_PAUSE_ADD, "");
    }


    public void set_SELL_FIRST_TYM(String JOB_eTITLE) {
        pref.edit().putString(SELL_FIRST_TYM, JOB_eTITLE).apply();
    }

    public String get_SELL_FIRST_TYM() {
        return pref.getString(SELL_FIRST_TYM, "");
    }



    public void set_CAT_FILTER(String JOB_eTITLE) {
        pref.edit().putString(CAT_FILTER, JOB_eTITLE).apply();
    }

    public String get_CAT_FILTER() {
        return pref.getString(CAT_FILTER, "");
    }



    public void set_CAT_FILTER_NAME(String JOB_eTITLE) {
        pref.edit().putString(CAT_FILTER_NAME, JOB_eTITLE).apply();
    }

    public String get_CAT_FILTER_NAME() {
        return pref.getString(CAT_FILTER_NAME, "");
    }



    public void set_CAT_FILTER_NAME_HE(String JOB_eTITLE) {
        pref.edit().putString(CAT_FILTER_NAME_HE, JOB_eTITLE).apply();
    }

    public String get_CAT_FILTER_NAME_HE() {
        return pref.getString(CAT_FILTER_NAME_HE, "");
    }


    public void set_CAT_FILTER_NAME_TEMP(String JOB_eTITLE) {
        pref.edit().putString(CAT_FILTER_NAME_TEMP, JOB_eTITLE).apply();
    }

    public String get_CAT_FILTER_NAME_TEMP() {
        return pref.getString(CAT_FILTER_NAME_TEMP, "");
    }


    public void set_CAT_FILTER_ID_TEMP(String JOB_eTITLE) {
        pref.edit().putString(CAT_FILTER_ID_TEMP, JOB_eTITLE).apply();
    }

    public String get_CAT_CAT_FILTER_ID_TEMP() {
        return pref.getString(CAT_FILTER_ID_TEMP, "");
    }


    public void set_LOC_NAME(String JOB_eTITLE) {
        pref.edit().putString(LOC_NAME, JOB_eTITLE).apply();
    }

    public String get_LOC_NAME() {
        return pref.getString(LOC_NAME, "");
    }


    public void set_LATITUDE(String JOB_eTITLE) {
        pref.edit().putString(LATITUDE, JOB_eTITLE).apply();
    }

    public String get_LATITUDE() {
        return pref.getString(LATITUDE, "0.0");
    }


    public void set_LONGITUDE(String JOB_eTITLE) {
        pref.edit().putString(LONGITUDE, JOB_eTITLE).apply();
    }

    public String get_LONGITUDE() {
        return pref.getString(LONGITUDE, "0.0");
    }


    public void set_LOC_NAME_TEMP(String JOB_eTITLE) {
        pref.edit().putString(LOC_NAME_TEMP, JOB_eTITLE).apply();
    }

    public String get_LOC_NAME_TEMP() {
        return pref.getString(LOC_NAME_TEMP, "");
    }


    public void set_LATITUDE_TEMP(String JOB_eTITLE) {
        pref.edit().putString(LATITUDE_TEMP, JOB_eTITLE).apply();
    }

    public String get_LATITUDE_TEMP() {
        return pref.getString(LATITUDE_TEMP, "0.0");
    }


    public void set_LONGITUDE_TEMP(String JOB_eTITLE) {
        pref.edit().putString(LONGITUDE_TEMP, JOB_eTITLE).apply();
    }

    public String get_LONGITUDE_TEMP() {
        return pref.getString(LONGITUDE_TEMP, "0.0");
    }


    public void set_LOC_COUNTRY_TEMP(String JOB_eTITLE) {
        pref.edit().putString(LOC_COUNTRY_TEMP, JOB_eTITLE).apply();
    }

    public String get_LOC_COUNTRY_TEMP() {
        return pref.getString(LOC_COUNTRY_TEMP, "");
    }


    public void set_LOC_TYPE(String JOB_eTITLE) {
        pref.edit().putString(LOC_TYPE, JOB_eTITLE).apply();
    }

    public String get_LOC_TYPE() {
        return pref.getString(LOC_TYPE, "0");
    }


    public void set_LOC_TYPE_FILTER(String JOB_eTITLE) {
        pref.edit().putString(LOC_TYPE_FILTER, JOB_eTITLE).apply();
    }

    public String get_LOC_TYPE_FILTER() {
        return pref.getString(LOC_TYPE_FILTER, "0");
    }


    public void set_FROM_PRICE(String JOB_eTITLE) {
        pref.edit().putString(FROM_PRICE, JOB_eTITLE).apply();
    }

    public String get_FROM_PRICE() {
        return pref.getString(FROM_PRICE, "0");
    }


    public void set_TO_PRICE(String JOB_eTITLE) {
        pref.edit().putString(TO_PRICE, JOB_eTITLE).apply();
    }

    public String get_TO_PRICE() {
        return pref.getString(TO_PRICE, "");
    }



    public void set_MIN_BUDGET(String JOB_eTITLE) {
        pref.edit().putString(MIN_BUDGET, JOB_eTITLE).apply();
    }

    public String get_MIN_BUDGET() {
        return pref.getString(MIN_BUDGET, "?");
    }



    public void set_LOC_NAME_COUNTRY(String JOB_eTITLE) {
        pref.edit().putString(LOC_NAME_COUNTRY, JOB_eTITLE).apply();
    }

    public String get_LOC_NAME_COUNTRY() {
        return pref.getString(LOC_NAME_COUNTRY, "0");
    }



    public void set_LOC_NAME_COUNTRY_TEMP(String JOB_eTITLE) {
        pref.edit().putString(LOC_NAME_COUNTRY_TEMP, JOB_eTITLE).apply();
    }

    public String get_LOC_NAME_COUNTRY_TEMP() {
        return pref.getString(LOC_NAME_COUNTRY_TEMP, "0");
    }



    public void set_CARD(String JOB_eTITLE) {
        pref.edit().putString(CARD, JOB_eTITLE).apply();
    }

    public String get_CARD() {
        return pref.getString(CARD, "N/A");
    }

    public void set_CHAT_NOTIFY(String JOB_eTITLE) {
        pref.edit().putString(CHAT_NOTIFY, JOB_eTITLE).apply();
    }

    public String get_CHAT_NOTIFY() {
        return pref.getString(CHAT_NOTIFY, "1");
    }


    public void set_CHAT_SOUND(String JOB_eTITLE) {
        pref.edit().putString(CHAT_SOUND, JOB_eTITLE).apply();
    }

    public String get_CHAT_SOUND() {
        return pref.getString(CHAT_SOUND, "1");
    }


    public void set_TICKET_NOTIFY(String JOB_eTITLE) {
        pref.edit().putString(TICKET_NOTIFY, JOB_eTITLE).apply();
    }

    public String get_TICKET_NOTIFY() {
        return pref.getString(TICKET_NOTIFY, "1");
    }

    public void set_TICKET_SOUND(String JOB_eTITLE) {
        pref.edit().putString(TICKET_SOUND, JOB_eTITLE).apply();
    }

    public String get_TICKET_SOUND() {
        return pref.getString(TICKET_SOUND, "1");
    }

    public void set_SELL_NOTIFY(String JOB_eTITLE) {
        pref.edit().putString(SELL_NOTIFY, JOB_eTITLE).apply();
    }

    public String get_SELL_NOTIFY() {
        return pref.getString(SELL_NOTIFY, "1");
    }

    public void set_SELL_SOUND(String JOB_eTITLE) {
        pref.edit().putString(SELL_SOUND, JOB_eTITLE).apply();
    }

    public String get_SELL_SOUND() {
        return pref.getString(SELL_SOUND, "1");
    }


    public void set_NEWSELL_SOUND(String JOB_eTITLE) {
        pref.edit().putString(NEWSELL_SOUND, JOB_eTITLE).apply();
    }

    public String get_NEWSELL_SOUND() {
        return pref.getString(NEWSELL_SOUND, "1");
    }


    public void set_NEWSELL_NOTIFY(String JOB_eTITLE) {
        pref.edit().putString(NEWSELL_NOTIFY, JOB_eTITLE).apply();
    }

    public String get_NEWSELL_NOTIFY() {
        return pref.getString(NEWSELL_NOTIFY, "1");
    }

    public void set_WIDGET(String JOB_eTITLE) {
        pref.edit().putString(WIDGET, JOB_eTITLE).apply();
    }

    public String get_WIDGET() {
        return pref.getString(WIDGET, "0");
    }

    public void set_PER(String JOB_eTITLE) {
        pref.edit().putString(PER, JOB_eTITLE).apply();
    }

    public String get_PER() {
        return pref.getString(PER, "0");
    }

    public void set_PERPRO(String JOB_eTITLE) {
        pref.edit().putString(PERPRO, JOB_eTITLE).apply();
    }

    public String get_PERPRO() {
        return pref.getString(PERPRO, "0");
    }


    public void set_ADDRESS(String JOB_eTITLE) {
        pref.edit().putString(ADDRESS, JOB_eTITLE).apply();
    }

    public String get_ADDRESS() {
        return pref.getString(ADDRESS, "");
    }


    public void set_ADDRESS_TEMP(String JOB_eTITLE) {
        pref.edit().putString(ADDRESS_TEMP, JOB_eTITLE).apply();
    }

    public String get_ADDRESS_TEMP() {
        return pref.getString(ADDRESS_TEMP, "");
    }


    public void set_BOUGHT_FIRST(String JOB_eTITLE) {
        pref.edit().putString(BOUGHT_FIRST, JOB_eTITLE).apply();
    }

    public String get_BOUGHT_FIRST() {
        return pref.getString(BOUGHT_FIRST, "0");
    }


    public void set_PROFILE_THUMB(String JOB_eTITLE) {
        pref.edit().putString(PROFILE_THUMB, JOB_eTITLE).apply();
    }

    public String get_PROFILE_THUMB() {
        return pref.getString(PROFILE_THUMB, "");
    }


    public void set_BUSS_THUMB(String JOB_eTITLE) {
        pref.edit().putString(BUSS_THUMB, JOB_eTITLE).apply();
    }

    public String get_BUSS_THUMB() {
        return pref.getString(BUSS_THUMB, "");
    }


    public void set_DAYPASSED(String JOB_eTITLE) {
        pref.edit().putString(DAYPASSED, JOB_eTITLE).apply();
    }

    public String get_DAYPASSED() {
        return pref.getString(DAYPASSED, "0");
    }


    public void set_DATEINSTALLED(String JOB_eTITLE) {
        pref.edit().putString(DATEINSTALLED, JOB_eTITLE).apply();
    }

    public String get_DATEINSTALLED() {
        return pref.getString(DATEINSTALLED, "");
    }


    public void set_CATEGORY_PROFILE_NAME(String JOB_eTITLE) {
        pref.edit().putString(CATEGORY_PROFILE_NAME, JOB_eTITLE).apply();
    }

    public String get_CATEGORY_PROFILE_NAME() {
        return pref.getString(CATEGORY_PROFILE_NAME, "");
    }


    public void set_DIMENSION(String JOB_eTITLE) {
        pref.edit().putString(DIMENSION, JOB_eTITLE).apply();
    }

    public String get_DIMENSION() {
        return pref.getString(DIMENSION, "1");
    }


    public void set_TIME_NOW(String JOB_eTITLE) {
        pref.edit().putString(TIME_NOW, JOB_eTITLE).apply();
    }

    public String get_TIME_NOW() {
        return pref.getString(TIME_NOW, "0");
    }


    public void set_TIME_HOUR_THEN(String JOB_eTITLE) {
        pref.edit().putString(TIME_HOUR_THEN, JOB_eTITLE).apply();
    }

    public String get_TIME_HOUR_THEN() {
        return pref.getString(TIME_HOUR_THEN, "0");
    }


    public void set_LANGUAGE(String JOB_eTITLE) {
        pref.edit().putString(LANGUAGE, JOB_eTITLE).apply();
    }

    public String get_LANGUAGE() {
        return pref.getString(LANGUAGE, "en");
    }



    public void set_REG_START(String JOB_eTITLE) {
        pref.edit().putString(REG_START, JOB_eTITLE).apply();
    }

    public String get_REG_START() {
        return pref.getString(REG_START, "00:00");
    }

    public void set_GET_STARTT(String JOB_eTITLE) {
        pref.edit().putString(GET_START, JOB_eTITLE).apply();
    }

    public String get_GET_START() {
        return pref.getString(GET_START, "00:00");
    }

    public void set_GET_START_END(String JOB_eTITLE) {
        pref.edit().putString(GET_START_END, JOB_eTITLE).apply();
    }

    public String get_GET_START_END() {
        return pref.getString(GET_START_END, "00:00");
    }

    public void set_SELL_START(String JOB_eTITLE) {
        pref.edit().putString(SELL_START, JOB_eTITLE).apply();
    }

    public String get_SELL_START() {
        return pref.getString(SELL_START, "00:00");
    }

    public void set_APP_START(String JOB_eTITLE) {
        pref.edit().putString(APP_START, JOB_eTITLE).apply();
    }

    public String get_APP_START() {
        return pref.getString(APP_START, "00:00");
    }


    public void set_CATEGORYNAME_ENG_REG(String JOB_eTITLE) {
        pref.edit().putString(CATEGORYNAME_ENG_REG, JOB_eTITLE).apply();
    }

    public String get_CATEGORYNAME_ENG_REG() {
        return pref.getString(CATEGORYNAME_ENG_REG, "");
    }



    public void set_CATEGORYNAME_HE_REG(String JOB_eTITLE) {
        pref.edit().putString(CATEGORYNAME_HE_REG, JOB_eTITLE).apply();
    }

    public String get_CATEGORYNAME_HE_REG() {
        return pref.getString(CATEGORYNAME_HE_REG, "");
    }



    public void set_CATEGORYNAME_HE_FIL(String JOB_eTITLE) {
        pref.edit().putString(CATEGORYNAME_HE_FIL, JOB_eTITLE).apply();
    }

    public String get_CATEGORYNAME_HE_FIL() {
        return pref.getString(CATEGORYNAME_HE_FIL, "");
    }



    public void set_CATEGORYNAME_ENG_FIL(String JOB_eTITLE) {
        pref.edit().putString(CATEGORYNAME_ENG_FIL, JOB_eTITLE).apply();
    }

    public String get_CATEGORYNAME_ENG_FIL() {
        return pref.getString(CATEGORYNAME_ENG_FIL, "");
    }




    public void set_CATEGORY_SEL_NAME_HE(String JOB_eTITLE) {
        pref.edit().putString(CATEGORY_SEL_NAME_HE, JOB_eTITLE).apply();
    }

    public String get_CATEGORY_SEL_NAME_HE() {
        return pref.getString(CATEGORY_SEL_NAME_HE, "");
    }


    public void set_HOUR_OR_DAY(String JOB_eTITLE) {
        pref.edit().putString(HOUR_OR_DAY, JOB_eTITLE).apply();
    }

    public String get_HOUR_OR_DAY() {
        return pref.getString(HOUR_OR_DAY, "hour");
    }


    public void set_MAX_TIME_COUNT(String JOB_eTITLE) {
        pref.edit().putString(MAX_TIME_COUNT, JOB_eTITLE).apply();
    }

    public String get_MAX_TIME_COUNT() {
        return pref.getString(MAX_TIME_COUNT, "?");
    }



    public void set_TO_RADIUS(String JOB_eTITLE) {
        pref.edit().putString(TO_RADIUS, JOB_eTITLE).apply();
    }

    public String get_TO_RADIUS() {
        return pref.getString(TO_RADIUS, "150");
    }



    public void set_CAT_FILTER_NAME_HE_TEMP(String JOB_eTITLE) {
        pref.edit().putString(CAT_FILTER_NAME_HE_TEMP, JOB_eTITLE).apply();
    }

    public String get_CAT_FILTER_NAME_HE_TEMP() {
        return pref.getString(CAT_FILTER_NAME_HE_TEMP, "");
    }


    public void set_APP_START_SESSION(String JOB_eTITLE) {
        pref.edit().putString(APP_START_SESSION, JOB_eTITLE).apply();
    }

    public String get_APP_START_SESSION() {
        return pref.getString(APP_START_SESSION, "00:00");
    }


    public void set_SELECTOR_BOUGHT(String JOB_eTITLE) {
        pref.edit().putString(SELECTOR_BOUGHT, JOB_eTITLE).apply();
    }

    public String get_SELECTOR_BOUGHT() {
        return pref.getString(SELECTOR_BOUGHT, "0");
    }


    public void set_SELECTOR_SELL(String JOB_eTITLE) {
        pref.edit().putString(SELECTOR_SELL, JOB_eTITLE).apply();
    }

    public String get_SELECTOR_SELL() {
        return pref.getString(SELECTOR_SELL, "0");
    }


    public void set_EMAIL_REG(String JOB_eTITLE) {
        pref.edit().putString(EMAIL_REG, JOB_eTITLE).apply();
    }

    public String get_EMAIL_REG() {
        return pref.getString(EMAIL_REG, "");
    }


    public void set_LOC_NAME_SELL(String JOB_eTITLE) {
        pref.edit().putString(LOC_NAME_SELL, JOB_eTITLE).apply();
    }

    public String get_LOC_NAME_SELL() {
        return pref.getString(LOC_NAME_SELL, "");
    }


    public void set_LOC_ADDRESS_SELL(String JOB_eTITLE) {
        pref.edit().putString(ADDRESS_SELL, JOB_eTITLE).apply();
    }

    public String get_ADDRESS_SELL() {
        return pref.getString(ADDRESS_SELL, "");
    }


    public void set_LAT_SELL(String JOB_eTITLE) {
        pref.edit().putString(LAT_SELL, JOB_eTITLE).apply();
    }

    public String get_LAT_SELL() {
        return pref.getString(LAT_SELL, "0");
    }


    public void set_LONG_SELL(String JOB_eTITLE) {
        pref.edit().putString(LONG_SELL, JOB_eTITLE).apply();
    }

    public String get_LONG_SELL() {
        return pref.getString(LONG_SELL, "");
    }


    public void set_COUNTRY_SELL(String JOB_eTITLE) {
        pref.edit().putString(COUNTRY_SELL, JOB_eTITLE).apply();
    }

    public String get_COUNTRY_SELL() {
        return pref.getString(COUNTRY_SELL, "");
    }


    public void set_TOOLTIP(String JOB_eTITLE) {
        pref.edit().putString(TOOLTIP, JOB_eTITLE).apply();
    }

    public String get_TOOLTIP() {
        return pref.getString(TOOLTIP, "");
    }


    public void set_PRICE_ILS(String JOB_eTITLE) {
        pref.edit().putString(PRICE_ILS, JOB_eTITLE).apply();
    }

    public String get_PRICE_ILS() {
        return pref.getString(PRICE_ILS, "5");
    }


    public void set_DAILY_PRICE(String JOB_eTITLE) {
        pref.edit().putString(DAILY_PRICE, JOB_eTITLE).apply();
    }

    public String get_DAILY_PRICE() {
        return pref.getString(DAILY_PRICE, "");
    }


    public void set_SWITCH_PRICE(String JOB_eTITLE) {
        pref.edit().putString(SWITCH_PRICE, JOB_eTITLE).apply();
    }

    public String get_SWITCH_PRICE() {
        return pref.getString(SWITCH_PRICE, "0");
    }


    public void set_SWITCH_BUD(String JOB_eTITLE) {
        pref.edit().putString(SWITCH_BUD, JOB_eTITLE).apply();
    }

    public String get_SWITCH_BUD() {
        return pref.getString(SWITCH_BUD, "0");
    }


    public void set_SWITCH_DATE(String JOB_eTITLE) {
        pref.edit().putString(SWITCH_DATE, JOB_eTITLE).apply();
    }

    public String get_SWITCH_DATE() {
        return pref.getString(SWITCH_DATE, "0");
    }



    public void set_SWITCH_JOB(String JOB_eTITLE) {
        pref.edit().putString(SWITCH_JOB, JOB_eTITLE).apply();
    }

    public String get_SWITCH_JOB() {
        return pref.getString(SWITCH_JOB, "0");
    }


}
