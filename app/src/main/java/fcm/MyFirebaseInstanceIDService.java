package fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import pref.SharedPreferenceLeadr;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    SharedPreferenceLeadr sharedPreferences;


    @Override
    public void onTokenRefresh() {
        sharedPreferences = SharedPreferenceLeadr.getInstance(this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        sharedPreferences.set_dev_token(refreshedToken);
        Log.e(TAG, "Refreshed token2: " + sharedPreferences.get_dev_token());

        sendRegistrationToServer(refreshedToken);
    }


    private void sendRegistrationToServer(String token) {

        sharedPreferences.set_dev_token(token);
        // TODO: Implement this method to send token to your app server.
    }
}