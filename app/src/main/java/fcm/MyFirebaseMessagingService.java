package fcm;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.RemoteViews;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import leadr.com.leadr.R;
import leadr.com.leadr.activity.ChatActivity;
import leadr.com.leadr.activity.MainActivity;
import leadr.com.leadr.activity.MySellActivity;
import leadr.com.leadr.activity.MySell_ListActivity;
import pref.SharedPreferenceLeadr;
import utils.GlobalConstant;
import utils.NetworkingData;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SharedPreferenceLeadr sharedPreferenceLeadr;
    String lead_id ="";
    String user_id ="";
    String user_otherid ="";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sharedPreferenceLeadr = SharedPreferenceLeadr.getInstance(this);

        Log.e("hhh0", "hjghj");
        //s1: message,123,1,3
        //var cat = 'message' + ','+ lead_id+ ','+ lead_user_id+ ','+ other_user_id;
        String s1 = remoteMessage.getData().get("category") + "";
        String s3 = remoteMessage.getData().get("message") + "";
        String s2 = remoteMessage.getMessageType();
//s3: You have recieved a refund request
        //cate = rating, message = Please Rate this app!

        try{
            Log.e("s1", s1);
        }catch (Exception e){}
        try{
            Log.e("s3", s3);
        }catch (Exception e){}
        if (s3.equalsIgnoreCase("You have approved a refund to the buyer")) {
            /*sharedPreferenceLeadr.set_SELECTOR_SELL("0");
            if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                sharedPreferenceLeadr.set_SELECTOR_SELL("2");
                if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.u_sold), "sell","1");
                }else{
                    sendNotification(getResources().getString(R.string.u_sold), "sell","0");
                }
            }*/
        }else if (s3.equalsIgnoreCase("You have sold a new lead")) {
            if(sharedPreferenceLeadr.get_SELL_NOTIFY().equalsIgnoreCase("1")){
                if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.u_sold), "sold","1");
                }else{
                    sendNotification(getResources().getString(R.string.u_sold), "sold","0");
                }
            }
            Boolean bo = isAppIsInBackground(this);
            if(!bo) {
                if (MySell_ListActivity.SCREEN_TYPE != null) {
                    if (MySell_ListActivity.SCREEN_TYPE.equalsIgnoreCase("sell")) {
                        String filter = "thisIsForMyActivitySell";
                        Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                }
            }
        }
        /*.....*/
        else if (s3.equalsIgnoreCase("You have recieved a refund request")) {
            if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")){
                if(sharedPreferenceLeadr.get_TICKET_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.buyr_askd_ref), "refund_ask_sell","1");
                }else{
                    sendNotification(getResources().getString(R.string.buyr_askd_ref), "refund_ask_sell","0");
                }
            }
            Boolean bo = isAppIsInBackground(this);
            if(!bo) {
                if (MySell_ListActivity.SCREEN_TYPE != null) {
                    if (MySell_ListActivity.SCREEN_TYPE.equalsIgnoreCase("sell")) {
                        String filter = "thisIsForMyActivitySell";
                        Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                }
            }
        }
        else if (s3.equalsIgnoreCase("Buyer opened a ticket")) {
            if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")){
                if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.buyr_opn_tckt), "refund_ask_sell","1");
                }else{
                    sendNotification(getResources().getString(R.string.buyr_opn_tckt), "refund_ask_sell","0");
                }
            }
            Boolean bo = isAppIsInBackground(this);
            if(!bo) {
                if (MySell_ListActivity.SCREEN_TYPE != null) {
                    if (MySell_ListActivity.SCREEN_TYPE.equalsIgnoreCase("sell")) {
                        String filter = "thisIsForMyActivitySell";
                        Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                }
            }
        }
        else if (s3.equalsIgnoreCase("Your refund request has been approved by seller")) {
            if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")){
                if(sharedPreferenceLeadr.get_TICKET_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.sell_app_noti), "bought","1");
                }else{
                    sendNotification(getResources().getString(R.string.sell_app_noti), "bought","0");
                }
            }
            Boolean bo = isAppIsInBackground(this);
            if(!bo) {
                if (MainActivity.Screen_Type != null) {
                    if (MainActivity.Screen_Type.equalsIgnoreCase("bought")) {
                        String filter = "thisIsForMyFragmentBought";
                        Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                }
            }
        }
        else if (s3.equalsIgnoreCase("Your refund request has been approved by seller")) {
            if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.ref_dec), "bought","1");
                }else{
                    sendNotification(getResources().getString(R.string.ref_dec), "bought","0");
                }
            }
            Boolean bo = isAppIsInBackground(this);
            if(!bo) {
                if (MainActivity.Screen_Type != null) {
                    if (MainActivity.Screen_Type.equalsIgnoreCase("bought")) {
                        String filter = "thisIsForMyFragmentBought";
                        Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                }
            }
        }
        /*else if (s3.equalsIgnoreCase("Your refund request has been decline by seller")) {
            if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                sharedPreferenceLeadr.set_SELECTOR_BOUGHT("1");
                if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.ref_dec), "bought","1");
                }else{
                    sendNotification(getResources().getString(R.string.ref_dec), "bought","0");
                }
            }
        }*/
        else if (s1.equalsIgnoreCase("sell alert")) {
            if(sharedPreferenceLeadr.get_SELL_NOTIFY().equalsIgnoreCase("1")){
                if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.lead_sld), "no sell","1");
                }else{
                    sendNotification(getResources().getString(R.string.lead_sld), "no sell","0");
                }
            }
        }else if (s1.equalsIgnoreCase("refund_approved")) {
            if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")){
                if(sharedPreferenceLeadr.get_TICKET_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.ref_req_ap), "bought","1");
                }else{
                    sendNotification(getResources().getString(R.string.ref_req_ap), "bought","0");
                }
            }
            Boolean bo = isAppIsInBackground(this);
            if(!bo) {
                if (MainActivity.Screen_Type != null) {
                    if (MainActivity.Screen_Type.equalsIgnoreCase("bought")) {
                        String filter = "thisIsForMyFragmentBought";
                        Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                }
            }
        }else if (s1.equalsIgnoreCase("refund_rejected")) {
            if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")){
                if(sharedPreferenceLeadr.get_TICKET_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.ref_dec), "bought","1");
                }else{
                    sendNotification(getResources().getString(R.string.ref_dec), "bought","0");
                }
            }
            Boolean bo = isAppIsInBackground(this);
            if(!bo) {
                if (MainActivity.Screen_Type != null) {
                    if (MainActivity.Screen_Type.equalsIgnoreCase("bought")) {
                        String filter = "thisIsForMyFragmentBought";
                        Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                }
            }
        }else if (s1.equalsIgnoreCase("ticket_approved")) {
                  method_TicketApp(s3,s1);
        }

        else if (s1.equalsIgnoreCase("ticket_rejected")) {
                if(!sharedPreferenceLeadr.get_TICKET_SOUND().equalsIgnoreCase("1")){
                    if(s3.equalsIgnoreCase("Ticket has been declined on your lead")){
                        if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")) {
                            sendNotification(getResources().getString(R.string.tckt_dec_sell_noti), "refund_ask_sell", "1");
                        }
                    }else{
                        if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")) {
                            sendNotification(getResources().getString(R.string.tckt_dec_noti), "bought", "0");
                        }
                    }
                    if(s3.equalsIgnoreCase("Ticket has been declined on your lead")){
                        //sell activity notify
                        Boolean bo = isAppIsInBackground(this);
                        if(!bo) {
                            if (MySell_ListActivity.SCREEN_TYPE != null) {
                                if (MySell_ListActivity.SCREEN_TYPE.equalsIgnoreCase("sell")) {
                                    String filter = "thisIsForMyActivitySell";
                                    Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                                }
                            }
                        }
                    }else{
                        //bought frag notify
                        Boolean bo = isAppIsInBackground(this);
                        if(!bo) {
                            if (MainActivity.Screen_Type != null) {
                                if (MainActivity.Screen_Type.equalsIgnoreCase("bought")) {
                                    String filter = "thisIsForMyFragmentBought";
                                    Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                                }
                            }
                        }
                    }
                }else{
                    if(s3.equalsIgnoreCase("Ticket has been declined on your lead")){
                        if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")) {
                            sendNotification(getResources().getString(R.string.tckt_dec_sell_noti), "refund_ask_sell", "1");
                        }
                    }else{
                        if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")) {
                            sendNotification(getResources().getString(R.string.tckt_dec_noti), "bought", "1");
                        }
                    }
                    if(s3.equalsIgnoreCase("Ticket has been declined on your lead")){
                        //decline move to my sell
                        Boolean bo = isAppIsInBackground(this);
                        if(!bo) {
                            if (MySell_ListActivity.SCREEN_TYPE != null) {
                                if (MySell_ListActivity.SCREEN_TYPE.equalsIgnoreCase("sell")) {
                                    String filter = "thisIsForMyActivitySell";
                                    Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                                }
                            }
                        }
                    }else{
                        //bought
                        Boolean bo = isAppIsInBackground(this);
                        if(!bo) {
                            if (MainActivity.Screen_Type != null) {
                                if (MainActivity.Screen_Type.equalsIgnoreCase("bought")) {
                                    String filter = "thisIsForMyFragmentBought";
                                    Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                                }
                            }
                        }
                    }
                }

        }else if (s1.equalsIgnoreCase("all")) {
            //push notification
            sendNotification(s3, "all","1");
        }else if (s3.contains("Lead wasn")) {
            sharedPreferenceLeadr.set_SELECTOR_SELL("0");
            if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                sharedPreferenceLeadr.set_SELECTOR_SELL("0");
                if(sharedPreferenceLeadr.get_SELL_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.lead_sld), "sell_alert","1");
                }else{
                    sendNotification(getResources().getString(R.string.lead_sld), "sell_alert","0");
                }
            }
        }else if (s1.contains("refund")) {
            sendNotification(s3, "all","1");
        }else if (s3.contains("rating")) {
            //Rating
            sendNotification(s3, "all","1");
        } else if(s1.contains("message")) {
            ////var cat = 'message' + ','+ lead_id+ ','+ lead_user_id+ ','+ other_user_id;
            if(sharedPreferenceLeadr.get_CHAT_NOTIFY().equalsIgnoreCase("1")){

                String[] arr_split = s1.split(",");
                lead_id = arr_split[1].trim();
                user_id = arr_split[2].trim();
                user_otherid = arr_split[3].trim();
                if(sharedPreferenceLeadr.get_CHAT_SOUND().equalsIgnoreCase("1")){
                    sendNotification(getResources().getString(R.string.nw_msg), "msg","1");
                }else{
                    sendNotification(getResources().getString(R.string.nw_msg), "msg","0");
                }
            }
            //notify inbox screen
            Boolean bo = isAppIsInBackground(this);
            if(!bo) {
                if (MainActivity.Screen_Type != null) {
                    if (MainActivity.Screen_Type.equalsIgnoreCase("inbox")) {
                        String filter = "thisIsForMyFragment";
                        Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

                    }
                }
                String filter2 = "thisIsForMyActivity";
                Intent intent2 = new Intent(filter2); //If you need extra, add: intent.putExtra("extra","something");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent2);
            }
        }
    }

    private void method_TicketApp(String s3, String s1) {
        if(!sharedPreferenceLeadr.get_TICKET_SOUND().equalsIgnoreCase("1")){
            if(s3.equalsIgnoreCase("Ticket has been approved on your lead")){
                if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")) {
                    sendNotification(getResources().getString(R.string.tckt_app_sell_noti), "refund_ask_sell", "0");
                }
            }else{
                if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")) {
                    sendNotification(getResources().getString(R.string.tckt_app_noti), "bought", "0");
                }
            }
            if(s3.equalsIgnoreCase("Ticket has been approved on your lead")){
                Boolean bo = isAppIsInBackground(this);
                if(!bo) {
                    if (MySell_ListActivity.SCREEN_TYPE != null) {
                        if (MySell_ListActivity.SCREEN_TYPE.equalsIgnoreCase("sell")) {
                            String filter = "thisIsForMyActivitySell";
                            Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        }
                    }
                }
            }else{
                Boolean bo = isAppIsInBackground(this);
                if(!bo) {
                    if (MainActivity.Screen_Type != null) {
                        if (MainActivity.Screen_Type.equalsIgnoreCase("bought")) {
                            String filter = "thisIsForMyFragmentBought";
                            Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        }
                    }
                }
            }
        }
        else{
            if(s3.equalsIgnoreCase("Ticket has been approved on your lead")){
                if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")) {
                    sendNotification(getResources().getString(R.string.tckt_app_sell_noti), "refund_ask_sell", "1");
                }
            }else{
                if(sharedPreferenceLeadr.get_TICKET_NOTIFY().equalsIgnoreCase("1")) {
                    sendNotification(getResources().getString(R.string.tckt_app_noti), "bought", "1");
                }
            }
            if(s3.equalsIgnoreCase("Ticket has been approved on your lead")){
                Boolean bo = isAppIsInBackground(this);
                if(!bo) {
                    if (MySell_ListActivity.SCREEN_TYPE != null) {
                        if (MySell_ListActivity.SCREEN_TYPE.equalsIgnoreCase("sell")) {
                            String filter = "thisIsForMyActivitySell";
                            Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        }
                    }
                }
            }else{
                Boolean bo = isAppIsInBackground(this);
                if(!bo) {
                    if (MainActivity.Screen_Type != null) {
                        if (MainActivity.Screen_Type.equalsIgnoreCase("bought")) {
                            String filter = "thisIsForMyFragmentBought";
                            Intent intent = new Intent(filter); //If you need extra, add: intent.putExtra("extra","something");
                            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        }
                    }
                }
            }

        }


    }


    private void sendNotification(String messageBody, String order_id, String sound) {
        Intent intent = null;
        Boolean is_Foreground = false;
        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.custom_notification);

        if(order_id.equalsIgnoreCase("msg")){
            intent = new Intent(this, ChatActivity.class);
            intent.putExtra("noti","noti");
            intent.putExtra("user_id",user_id);
            intent.putExtra("lead_id",lead_id);
            intent.putExtra("user_otherid",user_otherid);
            is_Foreground = isAppIsInBackground(this);
            if(!is_Foreground){
//                api_GetInboxCount();
            }
        }else if(order_id.equalsIgnoreCase("sell_alert")){
            intent = new Intent(this, MySell_ListActivity.class);
            intent.putExtra("noti","noti");
        }else if(order_id.equalsIgnoreCase("refund_ask_sell")){
            //when buyer asked for refund
            sharedPreferenceLeadr.set_SELECTOR_SELL("1");
            intent = new Intent(this, MySell_ListActivity.class);
            intent.putExtra("noti","noti");
        }else if(order_id.equalsIgnoreCase("sold")){
            //seller sold a new lead
            sharedPreferenceLeadr.set_SELECTOR_SELL("2");
            intent = new Intent(this, MySell_ListActivity.class);
            intent.putExtra("noti","noti");
        }else if(order_id.equalsIgnoreCase("no sell")){
            //when lead is not sold
            sharedPreferenceLeadr.set_SELECTOR_SELL("0");
            intent = new Intent(this, MySell_ListActivity.class);
            intent.putExtra("noti","noti");
        }else if(order_id.equalsIgnoreCase("refund_ask")){
            sharedPreferenceLeadr.set_SELECTOR_SELL("1");
            GlobalConstant.PAGINATION_IDS_SELL = "0";
            intent = new Intent(this, MySell_ListActivity.class);
            intent.putExtra("noti","refund_ask");
        }else if(order_id.equalsIgnoreCase("bought")){
            GlobalConstant.PAGINATION_IDS_BOUGHT = "0";
            sharedPreferenceLeadr.set_SELECTOR_BOUGHT("1");
            intent = new Intent(this, MainActivity.class);
            intent.putExtra("noti","bought");
        }else if(order_id.equalsIgnoreCase("all")){
            intent = new Intent(this, MainActivity.class);
            intent.putExtra("noti","all");
        }else{
            intent = new Intent(this, MySell_ListActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        RemoteViews contentView = new RemoteViews
                (getPackageName(), R.layout.custom_notification);
        contentView.setTextViewText(R.id.textAlbumName,
                messageBody);


        Uri defaultSoundUri = null;
        Log.e("notii_sound : ",sound);
        if(sound.equalsIgnoreCase("1")){
            defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }

        notificationLayout.setTextViewText(R.id.textAlbumName, messageBody);
        notificationLayout.setTextViewText(R.id.textDate,
                CurrentTimeStart());

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)

//                .setSmallIcon(R.drawable.logo_white)
                .setBadgeIconType(R.drawable.logo_white)
                .setContentTitle("LeadR")
//        .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(notificationLayout)
//                .setContentText(messageBody)
                .setAutoCancel(true)
                .setShowWhen(true)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                        R.drawable.notificationicon))
                .setSound(defaultSoundUri)
                .setContent(contentView)
                .setWhen(Calendar.getInstance().getTimeInMillis())
                .setContentIntent(pendingIntent);


        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(messageBody);
        bigText.setBigContentTitle("LeadR");


//        notificationBuilder.setStyle(bigText);
        notificationBuilder.setPriority(NotificationCompat.PRIORITY_MAX);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.logo_white);
            notificationBuilder.setColor(getResources().getColor(R.color.colorPrimary));
        } else {
            notificationBuilder.setSmallIcon(R.drawable.notificationicon);
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("10001", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.canShowBadge();
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert notificationManager != null;
            notificationBuilder.setChannelId("10001");
            notificationManager.createNotificationChannel(notificationChannel);
        }
        assert notificationManager != null;




//        notificationBuilder.build().contentView = contentView;
//        notificationBuilder.build().contentIntent = pendingIntent;


        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }



    String CurrentTimeStart(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm");
        String formattedDate = df.format(c.getTime());
        Calendar now = Calendar.getInstance();
        int a = now.get(Calendar.AM_PM);
        String am_or_pm = "am";
        if(a == Calendar.AM){
            am_or_pm = "am";
        }else{
            am_or_pm = "pm";
        }
        return formattedDate+" "+am_or_pm;
    }

    /****Check if app is in background or NOT****/
    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    /****GET Inbox Cpunt****/
    private void api_GetInboxCount(){
        sharedPreferenceLeadr = new SharedPreferenceLeadr(this);
        AndroidNetworking.enableLogging();
        Log.e("url_InboxCont: ", NetworkingData.BASE_URL+ NetworkingData.INBOX_TOTAL_COUNT);

        AndroidNetworking.post(NetworkingData.BASE_URL+ NetworkingData.INBOX_TOTAL_COUNT)
                .addBodyParameter("userId",sharedPreferenceLeadr.getUserId())
                .setTag("msg")
                .setPriority(Priority.HIGH).doNotCacheResponse()
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject result) {
                        Log.e("res_chkadmin: ",result+"");
                        if(result.optString("status").equalsIgnoreCase("1")){
                            if(MainActivity.textInboxCount!=null) {
                                MainActivity.textInboxCount.setText(result.optString("detail"));
                            }
                        }else{
                            if(MainActivity.textInboxCount!=null) {
                                MainActivity.textInboxCount.setText("0");
                            }
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("", "---> On error  ");
                    }
                });

    }
}