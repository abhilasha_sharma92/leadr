package modal;

/**
 * Created by Abhilasha on 6/12/2017.
 */

public class BuyPojo {
   String audio;
   String budget;
   String business_fb_page;
   String business_image;
   String business_info;
   String business_name;
   String category;
   String cell_number;
   String client_name;
   String description;
   String device_token;
   String device_type;
   String id;
   String is_delete;
   String job_title;
   String lead_id;
   String lead_price;
   String phone_number;
   String type;
   String user_id;
   String user_image;
   String lon;
   String lat;
   String created_date;
   String refund_req;
   String buy_time;
   String refund_date;

    public String getArchive_status() {
        return archive_status;
    }

    public void setArchive_status( String archive_status ) {
        this.archive_status = archive_status;
    }

    String archive_status;

    public String getRefund_user_id() {
        return refund_user_id;
    }

    public void setRefund_user_id( String refund_user_id ) {
        this.refund_user_id = refund_user_id;
    }

    String refund_user_id;

    public String getRefund_date() {
        return refund_date;
    }

    public void setRefund_date( String refund_date ) {
        this.refund_date = refund_date;
    }

    public String getRefund_time() {
        return refund_time;
    }

    public void setRefund_time( String refund_time ) {
        this.refund_time = refund_time;
    }

    String refund_time;

    public String getRefund_req_for() {
        return refund_req_for;
    }

    public void setRefund_req_for( String refund_req_for ) {
        this.refund_req_for = refund_req_for;
    }

    String refund_req_for;

    public String getBuy_time() {
        return buy_time;
    }

    public void setBuy_time( String buy_time ) {
        this.buy_time = buy_time;
    }

    public String getBuy_date() {
        return buy_date;
    }

    public void setBuy_date( String buy_date ) {
        this.buy_date = buy_date;
    }

    String buy_date;

    public Boolean getChange() {
        return change;
    }

    public void setChange(Boolean change) {
        this.change = change;
    }

    Boolean change;

    public String getReject_refund_req() {
        return reject_refund_req;
    }

    public void setReject_refund_req(String reject_refund_req) {
        this.reject_refund_req = reject_refund_req;
    }

    String reject_refund_req;

    public String getRefund_req() {
        return refund_req;
    }

    public void setRefund_req(String refund_req) {
        this.refund_req = refund_req;
    }

    public String getRefund_approved() {
        return refund_approved;
    }

    public void setRefund_approved(String refund_approved) {
        this.refund_approved = refund_approved;
    }

    String refund_approved;

    public String getBuy_userbusinessinfo() {
        return buy_userbusinessinfo;
    }

    public void setBuy_userbusinessinfo(String buy_userbusinessinfo) {
        this.buy_userbusinessinfo = buy_userbusinessinfo;
    }

    public String getBuy_userbusinessname() {
        return buy_userbusinessname;
    }

    public void setBuy_userbusinessname(String buy_userbusinessname) {
        this.buy_userbusinessname = buy_userbusinessname;
    }

    public String getBuy_userimage() {
        return buy_userimage;
    }

    public void setBuy_userimage(String buy_userimage) {
        this.buy_userimage = buy_userimage;
    }

    public String getBuy_username() {
        return buy_username;
    }

    public void setBuy_username(String buy_username) {
        this.buy_username = buy_username;
    }

    String buy_userbusinessinfo;
   String buy_userbusinessname;
   String buy_userimage;
   String buy_username;

    public String getLead_swipe_by() {
        return lead_swipe_by;
    }

    public void setLead_swipe_by( String lead_swipe_by ) {
        this.lead_swipe_by = lead_swipe_by;
    }

    String lead_swipe_by;

    public String getBuy_userid() {
        return buy_userid;
    }

    public void setBuy_userid(String buy_userid) {
        this.buy_userid = buy_userid;
    }

    String buy_userid;

    public String getUser_thum() {
        return user_thum;
    }

    public void setUser_thum(String user_thum) {
        this.user_thum = user_thum;
    }

    String user_thum;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    String address;

    public String getBuy_status() {
        return buy_status;
    }

    public void setBuy_status(String buy_status) {
        this.buy_status = buy_status;
    }

    String buy_status;

    public String getView_by() {
        return view_by;
    }

    public void setView_by(String view_by) {
        this.view_by = view_by;
    }

    String view_by;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    String country;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    String time;

    public Boolean getCondition() {
        return condition;
    }

    public void setCondition(Boolean condition) {
        this.condition = condition;
    }

    Boolean condition;

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    String created_time;

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    String location_name;

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getBusiness_fb_page() {
        return business_fb_page;
    }

    public void setBusiness_fb_page(String business_fb_page) {
        this.business_fb_page = business_fb_page;
    }

    public String getBusiness_image() {
        return business_image;
    }

    public void setBusiness_image(String business_image) {
        this.business_image = business_image;
    }

    public String getBusiness_info() {
        return business_info;
    }

    public void setBusiness_info(String business_info) {
        this.business_info = business_info;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCell_number() {
        return cell_number;
    }

    public void setCell_number(String cell_number) {
        this.cell_number = cell_number;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public String getLead_price() {
        return lead_price;
    }

    public void setLead_price(String lead_price) {
        this.lead_price = lead_price;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    String user_name;
}
