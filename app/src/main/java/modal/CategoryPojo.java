package modal;

/**
 * Created by Abhilasha on 2/12/2017.
 */

public class CategoryPojo {

    String id;
    String category;

    public String getHebrew() {
        return hebrew;
    }

    public void setHebrew( String hebrew ) {
        this.hebrew = hebrew;
    }

    String hebrew;

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    Boolean checked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
