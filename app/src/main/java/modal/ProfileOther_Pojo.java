package modal;

/**
 * Created by Abhilasha on 21/12/2017.
 */

public class ProfileOther_Pojo {
    String id;
    String phone_number;
    String user_name;
    String user_image;
    String job_title;
    String business_name;
    String business_info;
    String business_image;
    String business_fb_page;
    String category;
    String location_name;
    String bought;
    String refund;
    String buy_leads;
    String cate_en;

    public String getI_sold_to() {
        return i_sold_to;
    }

    public void setI_sold_to( String i_sold_to ) {
        this.i_sold_to = i_sold_to;
    }

    String i_sold_to;

    public String getNo_of_refund() {
        return no_of_refund;
    }

    public void setNo_of_refund( String no_of_refund ) {
        this.no_of_refund = no_of_refund;
    }

    String no_of_refund;

    public String getNo_buyers() {
        return no_buyers;
    }

    public void setNo_buyers( String no_buyers ) {
        this.no_buyers = no_buyers;
    }

    String no_buyers;

    public String getCate_en() {
        return cate_en;
    }

    public void setCate_en( String cate_en ) {
        this.cate_en = cate_en;
    }

    public String getCate_heb() {
        return cate_heb;
    }

    public void setCate_heb( String cate_heb ) {
        this.cate_heb = cate_heb;
    }

    String cate_heb;

    public String getBusi_thum() {
        return busi_thum;
    }

    public void setBusi_thum(String busi_thum) {
        this.busi_thum = busi_thum;
    }

    String busi_thum;

    public String getUser_thum() {
        return user_thum;
    }

    public void setUser_thum(String user_thum) {
        this.user_thum = user_thum;
    }

    String user_thum;

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getBought() {
        return bought;
    }

    public void setBought(String bought) {
        this.bought = bought;
    }

    public String getRefund() {
        return refund;
    }

    public void setRefund(String refund) {
        this.refund = refund;
    }

    public String getBuy_leads() {
        return buy_leads;
    }

    public void setBuy_leads(String buy_leads) {
        this.buy_leads = buy_leads;
    }

    public String getSold_leads() {
        return sold_leads;
    }

    public void setSold_leads(String sold_leads) {
        this.sold_leads = sold_leads;
    }

    public String getNo_of_leads() {
        return no_of_leads;
    }

    public void setNo_of_leads(String no_of_leads) {
        this.no_of_leads = no_of_leads;
    }

    String sold_leads;
    String no_of_leads;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getBusiness_info() {
        return business_info;
    }

    public void setBusiness_info(String business_info) {
        this.business_info = business_info;
    }

    public String getBusiness_image() {
        return business_image;
    }

    public void setBusiness_image(String business_image) {
        this.business_image = business_image;
    }

    public String getBusiness_fb_page() {
        return business_fb_page;
    }

    public void setBusiness_fb_page(String business_fb_page) {
        this.business_fb_page = business_fb_page;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }

    String is_delete;
}
