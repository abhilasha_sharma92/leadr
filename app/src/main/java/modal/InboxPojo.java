package modal;

/**
 * Created by Abhilasha on 2/7/2018.
 */

public class InboxPojo {




    String from_user_image;
    String from_user_name;
    String from_user_thum;
    String lead_description;
    String lead_id;
    String lead_user_id;
    String msg_from;
    String msg_from_type;
    String msg_to;
    String msg_to_type;
    String other_user_id;
    String to_user_image;
    String to_user_name;
    String to_business_info;
    String sold_status;

    public String getSold_status() {
        return sold_status;
    }

    public void setSold_status( String sold_status ) {
        this.sold_status = sold_status;
    }

    public String getSold_user_id() {
        return sold_user_id;
    }

    public void setSold_user_id( String sold_user_id ) {
        this.sold_user_id = sold_user_id;
    }

    String sold_user_id;


    String time;
    String audiotime;
    String chat_cnt;

    public String getChat_cnt() {
        return chat_cnt;
    }

    public void setChat_cnt( String chat_cnt ) {
        this.chat_cnt = chat_cnt;
    }

    public String getLead_id_count() {
        return lead_id_count;
    }

    public void setLead_id_count( String lead_id_count ) {
        this.lead_id_count = lead_id_count;
    }

    String lead_id_count;

    public String getAudiotime() {
        return audiotime;
    }

    public void setAudiotime( String audiotime ) {
        this.audiotime = audiotime;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio( String audio ) {
        this.audio = audio;
    }

    String audio;

    public String getTime() {
        return time;
    }

    public void setTime( String time ) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate( String date ) {
        this.date = date;
    }

    String date;

    public String getMsg() {
        return msg;
    }

    public void setMsg( String msg ) {
        this.msg = msg;
    }

    String msg;

    public String getTo_business_info() {
        return to_business_info;
    }

    public void setTo_business_info( String to_business_info ) {
        this.to_business_info = to_business_info;
    }

    public String getTo_business_name() {
        return to_business_name;
    }

    public void setTo_business_name( String to_business_name ) {
        this.to_business_name = to_business_name;
    }

    String to_business_name;

    public String getFrom_business_info() {
        return from_business_info;
    }

    public void setFrom_business_info( String from_business_info ) {
        this.from_business_info = from_business_info;
    }

    public String getFrom_business_name() {
        return from_business_name;
    }

    public void setFrom_business_name( String from_business_name ) {
        this.from_business_name = from_business_name;
    }

    String from_business_info;
    String from_business_name;

    public String getFrom_user_image() {
        return from_user_image;
    }

    public void setFrom_user_image( String from_user_image ) {
        this.from_user_image = from_user_image;
    }

    public String getFrom_user_name() {
        return from_user_name;
    }

    public void setFrom_user_name( String from_user_name ) {
        this.from_user_name = from_user_name;
    }

    public String getFrom_user_thum() {
        return from_user_thum;
    }

    public void setFrom_user_thum( String from_user_thum ) {
        this.from_user_thum = from_user_thum;
    }

    public String getLead_description() {
        return lead_description;
    }

    public void setLead_description( String lead_description ) {
        this.lead_description = lead_description;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id( String lead_id ) {
        this.lead_id = lead_id;
    }

    public String getLead_user_id() {
        return lead_user_id;
    }

    public void setLead_user_id( String lead_user_id ) {
        this.lead_user_id = lead_user_id;
    }

    public String getMsg_from() {
        return msg_from;
    }

    public void setMsg_from( String msg_from ) {
        this.msg_from = msg_from;
    }

    public String getMsg_from_type() {
        return msg_from_type;
    }

    public void setMsg_from_type( String msg_from_type ) {
        this.msg_from_type = msg_from_type;
    }

    public String getMsg_to() {
        return msg_to;
    }

    public void setMsg_to( String msg_to ) {
        this.msg_to = msg_to;
    }

    public String getMsg_to_type() {
        return msg_to_type;
    }

    public void setMsg_to_type( String msg_to_type ) {
        this.msg_to_type = msg_to_type;
    }

    public String getOther_user_id() {
        return other_user_id;
    }

    public void setOther_user_id( String other_user_id ) {
        this.other_user_id = other_user_id;
    }

    public String getTo_user_image() {
        return to_user_image;
    }

    public void setTo_user_image( String to_user_image ) {
        this.to_user_image = to_user_image;
    }

    public String getTo_user_name() {
        return to_user_name;
    }

    public void setTo_user_name( String to_user_name ) {
        this.to_user_name = to_user_name;
    }

    public String getTo_user_thum() {
        return to_user_thum;
    }

    public void setTo_user_thum( String to_user_thum ) {
        this.to_user_thum = to_user_thum;
    }

    String to_user_thum;



}
